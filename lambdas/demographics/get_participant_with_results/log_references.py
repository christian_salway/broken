import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    GPWR0001 = (logging.INFO,  'Received request to get participant with results')
    GPWR0002 = (logging.INFO,  'No participants or test results found for requested participant_id')
    GPWR0003 = (logging.INFO,  'Successfully queried DynamoDB for participant and test results')
    GPWR0004 = (logging.INFO,  'No test results found for requested participant_id')
    GPWR0005 = (logging.INFO,  'Successfully converted participant and test results into response type')
    GPWR0006 = (logging.ERROR, 'Can not find participant id in query parameter for incoming request')
    GPWR0007 = (logging.INFO,  'No participant found for the given participant id')
    GPWR0008 = (logging.INFO,  'Requesting details of registered gp practice')
    GPWR0009 = (logging.INFO,  'Unable to get latest episode for participant')
    GPWR0010 = (logging.ERROR, 'Unable to find exactly one deferral record for deferred episode')
    GPWR0011 = (logging.INFO,  'Returning notification status for suppressed results letter')
    GPWR0012 = (logging.INFO,  'Returning notification status for results letter')
