from common.log import log
from get_participant_with_results.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_message, json_return_object
from common.utils.audit_utils import AuditActions, audit
from common.utils.session_utils import get_session_from_lambda_event
from get_participant_with_results.get_participant_service import get_participants_and_results_response_by_participant_id


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    path_parameters = event.get('pathParameters')
    participant_id = path_parameters.get('participant_id') if path_parameters else None
    if not participant_id:
        log({'log_reference': LogReference.GPWR0006})
        return json_return_message(400, 'Required parameter participant_id not supplied')

    log({'log_reference': LogReference.GPWR0001})

    query_parameters = event.get('queryStringParameters')
    gp_practice_info_required = query_parameters.get('gp_practice_info') == 'true' if query_parameters else False

    participants_and_results = get_participants_and_results_response_by_participant_id(participant_id,
                                                                                       gp_practice_info_required)

    log({'log_reference': LogReference.GPWR0003})

    if not participants_and_results:
        log({'log_reference': LogReference.GPWR0002})
        return json_return_message(404, 'No participant or results found for requested participant_id')

    session = get_session_from_lambda_event(event)
    audit(session=session,
          action=AuditActions.VIEW_PARTICIPANT,
          participant_ids=[participant_id],
          nhs_numbers=[participants_and_results['data']['nhs_number']])

    return json_return_object(200, participants_and_results)
