import copy
from common.utils.participant_utils import \
    (get_participant_and_test_results, convert_dynamodb_entity_to_participant_response)
from common.utils.participant_episode_utils import \
    query_defer_record_by_participant_id_and_date, query_defer_records_by_participant_id, \
    query_episodes_by_participant_id, EpisodeStatus
from common.log import log
from get_participant_with_results.log_references import LogReference
from common.utils.organisation_directory_utils import \
    get_organisation_information_by_organisation_code
from common.utils.notification_utils import get_notifications_from_result
from common.models.letters import NotificationStatus, SupressionReason

PARTICIPANTS_SORT_KEY = 'PARTICIPANT'
TEST_SORT_KEY = 'RESULT'
CEASE_SORT_KEY = 'CEASE'
FRONTEND_NOTIFICATION_STATUS = {
    NotificationStatus.PENDING_PRINT_FILE: 'PROCESSING',
    NotificationStatus.IN_PRINT_FILE: 'SENT',
    NotificationStatus.WITH_PRINTER: 'SENT',
    NotificationStatus.SENT: 'SENT',
    NotificationStatus.SUPPRESSED: 'CANCELLED',
    SupressionReason.OLD_TEST: 'NOT SENT - OLDER THAN 3 MONTHS',
    SupressionReason.SOURCE_TYPE_7: 'NOT SENT - LAB SENT OWN LETTER',
    SupressionReason.HISTORIC_RESULT: 'NOT SENT - HISTORIC RESULT'
}


def get_participants_and_results_response_by_participant_id(participant_id: str, gp_practice_info_required: bool):
    participants_and_results = get_participant_and_test_results(participant_id, True)

    if not participants_and_results:
        return None

    test_results = _extract_test_results(participants_and_results)

    if len(test_results) == 0:
        log({'log_reference': LogReference.GPWR0004})

    participant = _extract_participant(participants_and_results)

    if not participant:
        log({'log_reference': LogReference.GPWR0007})
        return None

    registered_gp_practice_details = None
    registered_gp_practice_code = participant.get('registered_gp_practice_code')
    if registered_gp_practice_code and gp_practice_info_required:
        log({'log_reference': LogReference.GPWR0008})
        registered_gp_practice_details = get_organisation_information_by_organisation_code(registered_gp_practice_code)

    converted_participant = convert_dynamodb_entity_to_participant_response(participant, test_results,
                                                                            registered_gp_practice_details)

    ceased_result = _extract_ceased_result(participants_and_results)
    if ceased_result:
        converted_participant.update({'ceased_reason': ceased_result[0].get('reason', '')})
        converted_participant.update({'date_from': ceased_result[0].get('date_from', '')})

    latest_episode = get_latest_episode(participant_id)
    if latest_episode and latest_episode['status']:
        episode_status = latest_episode['status']
        converted_participant.update({'episode_status': episode_status})
        if episode_status == EpisodeStatus.DEFERRED.value:
            deferred_record = get_defer_record(participant_id, latest_episode.get('deferred_date'))
            defer_reason = deferred_record['reason']
            converted_participant.update({'defer_reason': defer_reason})
    else:
        # Participant may have been deferred with no previous episode
        defer_records = get_defer_records(participant_id)
        if len(defer_records):
            latest_defer_record = sorted(defer_records, key=lambda r: r['date_from'], reverse=True)[0]
            defer_reason = latest_defer_record['reason']
            converted_participant.update({'episode_status': EpisodeStatus.DEFERRED.value})
            converted_participant.update({'defer_reason': defer_reason})

    log({'log_reference': LogReference.GPWR0005})

    return {
        'data': converted_participant,
        'links': {
            'self': ''
        }
    }


def get_defer_record(participant_id, date_of_deferral):
    records = query_defer_record_by_participant_id_and_date(participant_id, date_of_deferral)
    if len(records) == 1:
        return records[0]

    log({
        'log_reference': LogReference.GPWR0010,
        'expected_num_records': 1,
        'actual_num_records': len(records),
        'participant_id': participant_id,
        'date_of_deferral': date_of_deferral})

    return None


def get_defer_records(participant_id):
    return query_defer_records_by_participant_id(participant_id)


def _extract_participant(participants_and_results):
    for entry in participants_and_results:
        if entry['sort_key'] == PARTICIPANTS_SORT_KEY:
            return entry
    return None


def _extract_test_results(participants_and_results):
    results = _extract_results(participants_and_results, TEST_SORT_KEY)
    updated_results = [
        {
            **result,
            **{'notification_status': _get_and_convert_notification_status(result)}
            } for result in results
    ]
    return updated_results


def _extract_ceased_result(participants_and_results):
    ceased_results = _extract_results(participants_and_results, CEASE_SORT_KEY)
    return sorted(ceased_results, key=lambda r: r['date_from'], reverse=True)


def get_latest_episode(participant_id):
    episodes = query_episodes_by_participant_id(participant_id)
    if not episodes:
        log({'log_reference': LogReference.GPWR0009})
        return None
    sorted_episodes = sorted(episodes, key=lambda r: r.get('created', ''), reverse=True)
    return sorted_episodes[0]


def _extract_results(participants_and_results, sort_key):
    results = []
    participants_and_results_list = copy.deepcopy(participants_and_results)
    for entry in participants_and_results_list:
        if entry['sort_key'].startswith(sort_key):
            participants_and_results.remove(entry)
            results.append(entry)
    return results


def _get_and_convert_notification_status(result):
    if result:
        notification_items = get_notifications_from_result(result['participant_id'], result['sort_key'])
        if notification_items and len(notification_items) > 0:
            notification_status = notification_items[0]['status']
            log({
                'log_reference': LogReference.GPWR0012,
                'notification_status': notification_status})
            return FRONTEND_NOTIFICATION_STATUS[notification_status]
        suppression_reason = result.get('suppression_reason')
        if suppression_reason:
            log({
                'log_reference': LogReference.GPWR0011,
                'suppression_reason': suppression_reason})
            return FRONTEND_NOTIFICATION_STATUS[suppression_reason]
