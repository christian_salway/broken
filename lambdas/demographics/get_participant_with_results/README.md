This lambda is to sit behind the screening_api API Gateway and serve responses for the front end.
It returns information about a participant given their participant ID, along with their test results.

## Request
``` GET /participants/{participant_id}/results ```

#### Required Params
``` participant_id=UUID ```


## Response
* Scenario: Result found

    Status code: 200 OK 

    Body: 
    ```
    {
      links: { self: '' },
      data: {
        "address": {
          "address_line_1": "Floor 8",
          "address_line_2": "Bridgewater Place",
          "address_line_3": "Water Lane",
          "address_line_4": "Leeds",
          "address_line_5": "West Yorkshire",
          "postcode": "LS11 5BZ"
        },
        "test_results": [
          {
            "test_date": "2020-01-14",
            "infection_code": "0",
            "result": "No Cytology test undertaken",
            "action": "Routine",
            "recall_months": "36",
            "sender_code": "714522",
            "source_code": "000001",
            "lab_code": "xxx",
            "slide_number": "654254123"
          }
        ]
        "address_effective_from_date": null,
        "date_of_birth": "1994-10-17",
        "date_of_death": null,
        "first_name": "Katherine",
        "gender": "2",
        "is_ceased": true,
        "is_invalid_patient": false,
        "last_name": "smith",
        "middle_names": "sophia Jane",
        "next_test_due_date": "2023-07-31",
        "nhs_number": "9876543210",
        "participant_id": "37f4524d-e59f-41d3-9071-97eec87955c8",
        "reason_for_removal_code": null,
        "reason_for_removal_effective_from_date": null,
        "registered_gp_practice_code": "P12345",
        "serial_change_number": null,
        "superseded_nhs_number": null,
        "title": "Miss"
      }
    }
    ```

* Scenario: No results found

    Status code: 200 OK 

    Body: 
     ```
    {
        "message": "No participant found for requested NHS Number"
    }
     ```
