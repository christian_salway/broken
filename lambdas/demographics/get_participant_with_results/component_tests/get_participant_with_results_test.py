import json
from unittest.case import TestCase
from boto3.dynamodb.conditions import Key
from mock import Mock, patch
from common.models.participant import ParticipantSortKey
from common.utils.data_segregation.nhais_ciphers import Cohorts
from get_participant_with_results import get_participant_with_results

mock_env_vars = {
    'AUDIT_QUEUE_URL': 'audit-queue',
    'DYNAMODB_PARTICIPANTS': 'participants-table',
}

participants_table_mock = Mock()
sqs_client_mock = Mock()

SESSION = {
    "session_id": "12345678",
    "user_data": {'nhsid_useruid': 'NHS12345', 'first_name': 'firsty', 'last_name': 'lasty'},
    'selected_role': {
        'organisation_code': 'NHSE',
        'organisation_name': 'YORKSHIRE AND THE HUMBER',
        'role_id': 'R8001',
        'role_name': 'Nurse Access'
    },
    'access_groups': {
        'pnl': True,
        'sample_taker': False,
        'lab': False,
        'colposcopy': False,
        'csas': False,
        'view_participant': True
    }
}

PARTICIPANTS = {
    "Items": [
        {
            "nhs_number": "71349462",
            "participant_id": "001",
            "sort_key": ParticipantSortKey.PARTICIPANT,
            "title": "dummy_title",
            "first_name": "correct",
            "middle_names": "participant",
            "last_name": "last",
            "nhais_cipher": "LDS",
            'date_of_birth': '1994-01-02',
        },
        {
            "participant_id": "001",
            "slide_number": "249874",
            "sort_key": "RESULT#2018-01-12#2020-11-29T00:00:00.000000",
            "nhais_cipher": "LDS"
        },
        {
            "participant_id": "001",
            "slide_number": "249797",
            "sort_key": "RESULT#2020-04-01#2020-11-29T00:00:00.000000",
            "nhais_cipher": "DMS"
        },
        {
            "nhs_number": "46879751",
            "participant_id": "002",
            "sort_key": ParticipantSortKey.PARTICIPANT,
            "title": "dummy_title",
            "first_name": "incorrect",
            "middle_names": "participant",
            "last_name": "last",
            "nhais_cipher": "DMS",
            'date_of_birth': '1994-01-02',
        },
    ]
}

PARTICIPANT_AND_RESULTS_PROJECTION = ', '.join([
    'nhs_number',
    'sort_key',
    'participant_id',
    'title',
    'first_name',
    'middle_names',
    'last_name',
    'gender',
    'address',
    'date_of_birth',
    'date_of_death',
    'is_ceased',
    'is_invalid_patient',
    'next_test_due_date',
    'registered_gp_practice_code',
    'infection_result',
    'infection_code',
    '#result',
    '#action',
    'action_code',
    'test_date',
    'recall_months',
    'result_code',
    'sender_code',
    'source_code',
    'sender_source_type',
    'slide_number',
    'sending_lab',
    '#status',
    'suppression_reason',
    'invited_date',
    'reason',
    'date_from',
    'is_fp69',
    'event_text',
    'health_authority',
    'crm',
    'active',
    'reason_for_removal_code',
    'reason_for_removal_effective_from_date',
    'hpv_primary',
    'self_sample',
    'practice_code',
    'nhais_cipher',
    'is_paused',
    'is_paused_received_time'
])

EXPECTED_BODY = {
   "data": {
      "nhs_number": "71349462",
      "date_of_birth": "1994-01-02",
      "gender": None,
      "title": "dummy_title",
      "first_name": "correct",
      "middle_names": "participant",
      "last_name": "last",
      "next_test_due_date": None,
      "date_of_death": None,
      "is_invalid_patient": None,
      "status": None,
      "is_fp69": None,
      "event_text": None,
      "test_results": [
         {
            "sort_key": "RESULT#2018-01-12#2020-11-29T00:00:00.000000",
            "title": None,
            "first_name": None,
            "last_name": None,
            "date_of_birth": None,
            "gender": None,
            "health_authority": None,
            "sending_lab": None,
            "source_code": None,
            "sender_code": None,
            "sender_source_type": None,
            "slide_number": "249874",
            "test_date": None,
            "infection_result": None,
            "infection_code": None,
            "result": None,
            "result_code": None,
            "action": None,
            "action_code": None,
            "hpv_primary": None,
            "self_sample": None,
            "recall_months": None,
            "crm": [],
            "notification_status": None,
            "nhais_cipher": "LDS"
         },
         {
            "sort_key": "RESULT#2020-04-01#2020-11-29T00:00:00.000000",
            "title": None,
            "first_name": None,
            "last_name": None,
            "date_of_birth": None,
            "gender": None,
            "health_authority": None,
            "sending_lab": None,
            "source_code": None,
            "sender_code": None,
            "sender_source_type": None,
            "slide_number": "249797",
            "test_date": None,
            "infection_result": None,
            "infection_code": None,
            "result": None,
            "result_code": None,
            "action": None,
            "action_code": None,
            "hpv_primary": None,
            "self_sample": None,
            "recall_months": None,
            "crm": [],
            "notification_status": None,
            "nhais_cipher": "DMS"
         }
      ],
      "registered_gp_practice": None,
      "address": None,
      "participant_id": "001",
      "active": True,
      "reason_for_removal_code": None,
      "reason_for_removal_effective_from_date": None,
      "is_ceased": None,
      "serial_change_number": None,
      "superseded_nhs_number": None,
      "address_effective_from_date": None,
      "invited_date": None,
      "is_paused": None,
      "is_paused_received_time": None
   },
   "links": {"self": ""}
}

EXPECTED_RESPONSE = {
    'statusCode': 200,
    'headers': {
        'Content-Type': 'application/json',
        'X-Content-Type-Options': 'nosniff',
        'Strict-Transport-Security': 'max-age=1576800'
    },
    'body': json.dumps(EXPECTED_BODY),
    'isBase64Encoded': False
}

NOT_FOUND_MESSAGE = {"message": "No participant or results found for requested participant_id"}

EXPECTED_NOT_FOUND_RESPONSE = {
    'statusCode': 404,
    'headers': {
        'Content-Type': 'application/json',
        'X-Content-Type-Options': 'nosniff',
        'Strict-Transport-Security': 'max-age=1576800'
    },
    'body': json.dumps(NOT_FOUND_MESSAGE),
    'isBase64Encoded': False
}


@patch('os.environ', mock_env_vars)
class TestGetParticipants(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        participants_table_mock.reset_mock()
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        retrieved_participants_list = PARTICIPANTS.copy()

        participants_table_mock.query.return_value = retrieved_participants_list
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = ''

        self.maxDiff = None

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('get_participant_with_results.get_participant_service.query_episodes_by_participant_id',
           Mock(return_value=None))
    @patch('get_participant_with_results.get_participant_service.query_defer_records_by_participant_id',
           Mock(return_value=[]))
    @patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
           Mock(return_value=participants_table_mock))
    @patch('common.utils.notification_utils.dynamodb_query', Mock(return_value=None))
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    def get_participant_in_cohort_test(self):
        participant_id = '001'

        event = {
            'pathParameters': {'participant_id': participant_id},
            'requestContext': {'authorizer': {
                'session': json.dumps(SESSION), 'principalId': 'blah'
            }},
            'headers': {'request_id': '1'},
            'queryStringParameters': {'gp_practice_info': 'yes'}
        }

        actual_response = get_participant_with_results.lambda_handler(event, self.context)

        self.assertEqual(actual_response, EXPECTED_RESPONSE)

        participants_table_mock.query.assert_called_once_with(
            KeyConditionExpression=Key('participant_id').eq(participant_id),
            ProjectionExpression=PARTICIPANT_AND_RESULTS_PROJECTION,
            ExpressionAttributeNames={'#result': 'result', '#action': 'action', '#status': 'status'},
            ScanIndexForward=False
        )

    @patch('get_participant_with_results.get_participant_service.query_episodes_by_participant_id',
           Mock(return_value=None))
    @patch('get_participant_with_results.get_participant_service.query_defer_records_by_participant_id',
           Mock(return_value=[]))
    @patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
           Mock(return_value=participants_table_mock))
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.IOM]))
    def fail_to_get_participants_not_in_cohort_test(self):
        participant_id = '002'

        event = {
            'pathParameters': {'participant_id': participant_id},
            'requestContext': {'authorizer': {
                'session': json.dumps(SESSION), 'principalId': 'blah'
            }},
            'headers': {'request_id': '1'},
            'queryStringParameters': {'gp_practice_info': 'yes'}
        }

        actual_response = get_participant_with_results.lambda_handler(event, self.context)

        self.assertEqual(actual_response, EXPECTED_NOT_FOUND_RESPONSE)

        participants_table_mock.query.assert_called_once_with(
            KeyConditionExpression=Key('participant_id').eq(participant_id),
            ProjectionExpression=PARTICIPANT_AND_RESULTS_PROJECTION,
            ExpressionAttributeNames={'#result': 'result', '#action': 'action', '#status': 'status'},
            ScanIndexForward=False
        )
