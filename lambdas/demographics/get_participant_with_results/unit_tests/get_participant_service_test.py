from unittest.case import TestCase
from mock import patch, call
from ddt import ddt, data, unpack
from common.models.participant import EpisodeStatus, DeferReason
from common.models.letters import NotificationStatus, SupressionReason

module = 'get_participant_with_results.get_participant_service'


class TestGetParticipantsService(TestCase):

    @patch('boto3.resource')
    def setUp(self, *args):
        global get_participants_and_results_response_by_participant_id, LogReference, EpisodeStatus, DeferReason
        from get_participant_with_results.get_participant_service import (
            get_participants_and_results_response_by_participant_id, LogReference)

        self.log_patcher = patch(f'{module}.log').start()
        self.query_episodes_by_participant_id_patcher = patch(f'{module}.query_episodes_by_participant_id').start()
        self.query_defer_records_by_participant_id_patcher = patch(
            f'{module}.query_defer_records_by_participant_id').start()
        self.converter_patcher = patch(f'{module}.convert_dynamodb_entity_to_participant_response').start()
        self.get_organisation_information_by_organisation_code_patcher = patch(
            f'{module}.get_organisation_information_by_organisation_code').start()
        self.get_participant_and_test_results_patcher = patch(f'{module}.get_participant_and_test_results').start()
        self.query_defer_record_by_participant_id_and_date_patcher = patch(
            f'{module}.query_defer_record_by_participant_id_and_date').start()
        self.get_notifications_from_result_patcher = patch(f'{module}.get_notifications_from_result').start()

        # Test data
        self.participant_id = 'a1b2c3'
        self.gp_practice_info_required = False
        self.expected_response = {
            'data': {
                'participant_id': 'converted',
                'episode_status': EpisodeStatus.DEFERRED.value,
                'defer_reason': DeferReason.CURRENT_PREGNANCY,
            }, 'links': {'self': ''}}
        self.notification_status = 'SENT'

        # Mock return
        self.converter_patcher.return_value = {'participant_id': 'converted'}
        self.query_episodes_by_participant_id_patcher.return_value = [
            {'created': '2020-01-01T12:15:08.132', 'sort_key': 'EPISODE#', 'status': EpisodeStatus.PNL.value},
            {'created': '2020-01-03T12:15:08.132', 'sort_key': 'EPISODE#',
                'status': EpisodeStatus.DEFERRED.value, 'deferred_date': '2020-01-03T12:15:08.132'},
            {'created': '2020-01-02T12:15:08.132', 'sort_key': 'EPISODE#', 'status': EpisodeStatus.INVITED.value}]
        self.query_defer_record_by_participant_id_and_date_patcher.return_value = [
            {'reason': DeferReason.CURRENT_PREGNANCY}]
        self.query_defer_records_by_participant_id_patcher.return_value = [
            {'reason': DeferReason.RECENT_TEST, 'date_from': '2020-01-01'},
            {'reason': DeferReason.CURRENT_PREGNANCY, 'date_from': '2020-01-03'},
            {'reason': DeferReason.UNDER_COLPOSCOPY, 'date_from': '2020-01-02'},
        ]
        self.get_notifications_from_result_patcher.return_value = [{'status': self.notification_status}]

    def act(self):
        return get_participants_and_results_response_by_participant_id(
            self.participant_id, self.gp_practice_info_required)

    def test_none_is_returned_given_no_participant_found_for_given_participant_id(self):
        self.get_participant_and_test_results_patcher.return_value = []

        response = self.act()

        self.assertIsNone(response)

    def test_participant_response_returned_given_no_test_results_found_and_gp_practice_code_not_required(self):
        self.get_participant_and_test_results_patcher.return_value = [{'sort_key': 'PARTICIPANT', 'nhs_number': '123'}]

        response = self.act()

        self.get_organisation_information_by_organisation_code_patcher.assert_not_called()
        self.converter_patcher.assert_called_with({'sort_key': 'PARTICIPANT', 'nhs_number': '123'}, [], None)
        self.assertDictEqual(self.expected_response, response)

    def test_returns_deferred_reason_if_deferred_with_no_episode(self):
        self.get_participant_and_test_results_patcher.return_value = [{'sort_key': 'PARTICIPANT', 'nhs_number': '123'}]
        self.query_episodes_by_participant_id_patcher.return_value = []

        response = self.act()

        self.get_organisation_information_by_organisation_code_patcher.assert_not_called()
        self.converter_patcher.assert_called_with({'sort_key': 'PARTICIPANT', 'nhs_number': '123'}, [], None)
        self.assertDictEqual(self.expected_response, response)

    def test_participant_response_returned_given_test_results_found_and_gp_practice_code_not_required(self):
        self.get_participant_and_test_results_patcher.return_value = [
            {'sort_key': 'PARTICIPANT', 'nhs_number': '123'},
            {'sort_key': 'RESULT#2020-04-27', 'id': 'test 1', 'participant_id': '5'},
            {'sort_key': 'RESULT#2020-05-03', 'id': 'test 2', 'participant_id': '5'}
        ]

        response = self.act()

        self.get_organisation_information_by_organisation_code_patcher.assert_not_called()
        self.converter_patcher.assert_called_with(
            {'sort_key': 'PARTICIPANT', 'nhs_number': '123'},
            [{'sort_key': 'RESULT#2020-04-27', 'id': 'test 1', 'participant_id': '5', 'notification_status': 'SENT'},
             {'sort_key': 'RESULT#2020-05-03', 'id': 'test 2', 'participant_id': '5', 'notification_status': 'SENT'}],
            None)
        self.assertDictEqual(self.expected_response, response)

    def test_participant_response_returned_given_gp_practice_code_required_but_no_gp_practice_code(self):
        self.gp_practice_info_required = True
        self.get_participant_and_test_results_patcher.return_value = [{'sort_key': 'PARTICIPANT', 'nhs_number': '123'}]

        response = self.act()

        self.get_organisation_information_by_organisation_code_patcher.assert_not_called()
        self.converter_patcher.assert_called_with({'sort_key': 'PARTICIPANT', 'nhs_number': '123'}, [], None)
        self.assertDictEqual(self.expected_response, response)

    def test_participant_response_returned_given_gp_practice_code_required_and_gp_practice_code(self):
        self.gp_practice_info_required = True
        self.get_participant_and_test_results_patcher.return_value = [
            {'sort_key': 'PARTICIPANT', 'registered_gp_practice_code': '123'}]
        self.get_organisation_information_by_organisation_code_patcher.return_value = {
            'name': 'St James', 'type': 'Hospital'}

        response = self.act()

        self.converter_patcher.assert_called_with({'sort_key': 'PARTICIPANT', 'registered_gp_practice_code': '123'},
                                                  [],
                                                  {'name': 'St James', 'type': 'Hospital'})
        self.assertDictEqual(self.expected_response, response)

    def test_returns_gp_info_and_test_results(self):
        self.gp_practice_info_required = True
        self.get_participant_and_test_results_patcher.return_value = [
            {'sort_key': 'PARTICIPANT', 'registered_gp_practice_code': '123'},
            {'sort_key': 'RESULT#2020-04-27', 'id': 'test 1', 'participant_id': '5'},
            {'sort_key': 'RESULT#2020-05-03', 'id': 'test 2', 'participant_id': '5'}
        ]

        self.get_organisation_information_by_organisation_code_patcher.return_value = {
            'name': 'St James', 'type': 'Hospital'}

        response = self.act()

        self.converter_patcher.assert_called_with(
            {'sort_key': 'PARTICIPANT', 'registered_gp_practice_code': '123'},
            [{'sort_key': 'RESULT#2020-04-27', 'id': 'test 1', 'participant_id': '5', 'notification_status': 'SENT'},
             {'sort_key': 'RESULT#2020-05-03', 'id': 'test 2', 'participant_id': '5', 'notification_status': 'SENT'}],
            {'name': 'St James', 'type': 'Hospital'})
        self.assertDictEqual(self.expected_response, response)

    def test_participant_response_if_no_live_episode(self):
        self.gp_practice_info_required = True
        self.get_participant_and_test_results_patcher.return_value = [
            {'sort_key': 'PARTICIPANT', 'registered_gp_practice_code': '123'},
            {'sort_key': 'RESULT#2020-04-27', 'id': 'test 1', 'participant_id': '5'},
            {'sort_key': 'RESULT#2020-05-03', 'id': 'test 2', 'participant_id': '5'}
        ]

        self.get_organisation_information_by_organisation_code_patcher.return_value = {
            'name': 'St James', 'type': 'Hospital'}
        self.query_episodes_by_participant_id_patcher.return_value = []

        response = self.act()

        self.converter_patcher.assert_called_with(
            {'sort_key': 'PARTICIPANT', 'registered_gp_practice_code': '123'},
            [{'sort_key': 'RESULT#2020-04-27', 'id': 'test 1', 'participant_id': '5', 'notification_status': 'SENT'},
             {'sort_key': 'RESULT#2020-05-03', 'id': 'test 2', 'participant_id': '5', 'notification_status': 'SENT'}],
            {'name': 'St James', 'type': 'Hospital'})
        self.assertDictEqual(self.expected_response, response)
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.GPWR0008}),
            call({'log_reference': LogReference.GPWR0009}),
            call({'log_reference': LogReference.GPWR0005})])

    def test_participant_response_returned_given_test_results_found_and_multiple_ceased_records_exist(self):
        self.gp_practice_info_required = True
        self.get_participant_and_test_results_patcher.return_value = [
            {'sort_key': 'PARTICIPANT', 'registered_gp_practice_code': '123'},
            {'sort_key': 'RESULT#2020-04-27', 'id': 'test 1', 'participant_id': '1'},
            {'sort_key': 'RESULT#2020-05-03', 'id': 'test 2', 'participant_id': '1'},
            {'sort_key': 'CEASE#2020-09-02#1', 'id': 'test 3', 'date_from': '2020-09-01T08:58:48.324',
                'reason': 'Reason 01', 'participant_id': '1'},
            {'sort_key': 'CEASE#2020-09-02#3', 'id': 'test 3', 'date_from': '2020-09-02T07:54:48.324',
                'reason': 'Reason 03', 'participant_id': '1'},
            {'sort_key': 'CEASE#2020-09-02#2', 'id': 'test 3', 'date_from': '2020-08-01T09:31:48.324',
                'reason': 'Reason 02', 'participant_id': '1'}
        ]

        self.get_organisation_information_by_organisation_code_patcher.return_value = {
            'name': 'St James', 'type': 'Hospital'}

        expected_response = {
            'data': {
                'participant_id': 'converted',
                'episode_status': EpisodeStatus.DEFERRED.value,
                'defer_reason': DeferReason.CURRENT_PREGNANCY,
                'ceased_reason': 'Reason 03',
                'date_from': '2020-09-02T07:54:48.324'
            },
            'links': {'self': ''}}

        response = self.act()

        self.converter_patcher.assert_called_with(
            {'sort_key': 'PARTICIPANT', 'registered_gp_practice_code': '123'},
            [{'sort_key': 'RESULT#2020-04-27', 'id': 'test 1', 'participant_id': '1', 'notification_status': 'SENT'},
             {'sort_key': 'RESULT#2020-05-03', 'id': 'test 2', 'participant_id': '1', 'notification_status': 'SENT'}],
            {'name': 'St James', 'type': 'Hospital'})
        self.assertDictEqual(expected_response, response)


@ddt
class TestGetParticipantsServiceFunctions(TestCase):

    def setUp(self, *args):
        import get_participant_with_results.get_participant_service as _get_participant_service
        self.get_participant_service_module = _get_participant_service

    @unpack
    @data(
        (None, [], None),
        ({'participant_id': '1', 'sort_key': '2'}, [], None),
        ({'participant_id': '1', 'sort_key': '2'}, [{'status': NotificationStatus.PENDING_PRINT_FILE}], 'PROCESSING'),
        ({'participant_id': '1', 'sort_key': '2'}, [{'status': NotificationStatus.IN_PRINT_FILE}], 'SENT'),
        ({'participant_id': '1', 'sort_key': '2'}, [{'status': NotificationStatus.WITH_PRINTER}], 'SENT'),
        ({'participant_id': '1', 'sort_key': '2'}, [{'status': NotificationStatus.SENT}], 'SENT'),
        ({'participant_id': '1', 'sort_key': '2'}, [{'status': NotificationStatus.SUPPRESSED}], 'CANCELLED'),
        ({'participant_id': '1', 'sort_key': '2', 'suppression_reason': SupressionReason.OLD_TEST},
         [], 'NOT SENT - OLDER THAN 3 MONTHS'),
        ({'participant_id': '1', 'sort_key': '2', 'suppression_reason': SupressionReason.SOURCE_TYPE_7},
         [], 'NOT SENT - LAB SENT OWN LETTER'),
        ({'participant_id': '1', 'sort_key': '2', 'suppression_reason': SupressionReason.HISTORIC_RESULT},
         [], 'NOT SENT - HISTORIC RESULT'),
        ({'participant_id': '1', 'sort_key': '2', 'suppression_reason': 'Reason overridden'},
         [{'status': NotificationStatus.SENT}], 'SENT')
    )
    @patch(f'{module}.get_notifications_from_result')
    def test_get_and_convert_notification_status(self,
                                                 result_record, notification_items, expected_result,
                                                 get_notifications_from_result_mock):
        get_notifications_from_result_mock.return_value = notification_items
        actual_result = self.get_participant_service_module._get_and_convert_notification_status(result_record)
        self.assertEqual(actual_result, expected_result)
