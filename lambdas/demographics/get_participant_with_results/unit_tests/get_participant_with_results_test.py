from unittest.case import TestCase
from mock import patch, Mock

from common.test_assertions.api_assertions import assertApiResponse

with patch('boto3.resource') as resource_mock:
    with patch('botocore.client') as client_mock:
        from common.utils.audit_utils import AuditActions
        from get_participant_with_results.get_participant_with_results import lambda_handler


module = 'get_participant_with_results.get_participant_with_results'


@patch(f'{module}.audit', Mock())
@patch(f'{module}.get_session_from_lambda_event', Mock())
@patch(f'{module}.log', Mock())
class TestGetParticipantByParticipantId(TestCase):

    def setUp(self):
        super(TestGetParticipantByParticipantId, self).setUp()
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    @patch(f'{module}.get_participants_and_results_response_by_participant_id')
    def test_bad_request_returned_given_no_participant_id_in_event(self, service_mock):
        event = {'pathParameters': None}

        expected_response_body = {'message': 'Required parameter participant_id not supplied'}

        actual_response = self.unwrapped_handler(event, {})

        assertApiResponse(self, 400, expected_response_body, actual_response)

        service_mock.assert_not_called()

    @patch(f'{module}.get_participants_and_results_response_by_participant_id')
    def test_not_found_returned_when_no_participant_found(self, service_mock):
        participant_id = '12345678'
        event = {'pathParameters': {'participant_id': participant_id}, 'headers': None, 'queryStringParameters': None}
        service_mock.return_value = None
        expected_response_body = {'message': 'No participant or results found for requested participant_id'}

        actual_response = self.unwrapped_handler(event, {})

        assertApiResponse(self, 404, expected_response_body, actual_response)

        service_mock.assert_called_with(participant_id, False)

    @patch(f'{module}.get_participants_and_results_response_by_participant_id')
    def test_ok_and_expected_participant_with_results_returned_given_gp_practice_query_parameter_not_true(
            self, service_mock):
        participant_id = '12345678'

        event = {'pathParameters': {'participant_id': participant_id}, 'headers': None,
                 'queryStringParameters': {'gp_practice_info': 'yes'}}

        service_mock.return_value = {
            "data": {
                "participant_id": participant_id,
                "nhs_number": "1",
                "first_name": "Jane",
                "last_name": "Doe",
                "test_results": [
                    {
                        "slide_number": "19035247",
                    }
                ]
            }
        }

        expected_response_body = {
            "data": {
                'participant_id': '12345678',
                'nhs_number': '1',
                'first_name': 'Jane',
                'last_name': 'Doe',
                'test_results': [{'slide_number': '19035247'}]
            }
        }

        actual_response = self.unwrapped_handler(event, {})

        assertApiResponse(self, 200, expected_response_body, actual_response)

        service_mock.assert_called_with(participant_id, False)

    @patch(f'{module}.get_participants_and_results_response_by_participant_id')
    def test_ok_and_expected_participant_with_results_returned_given_gp_practice_query_parameter_true(
            self, service_mock):
        participant_id = '12345678'

        event = {'pathParameters': {'participant_id': participant_id}, 'headers': None,
                 'queryStringParameters': {'gp_practice_info': 'true'}}

        service_mock.return_value = {
            "data": {
                "participant_id": participant_id,
                "nhs_number": "1",
                "first_name": "Jane",
                "last_name": "Doe",
                "test_results": [
                    {
                        "slide_number": "19035247",
                    }
                ]
            }
        }

        expected_response_body = {
            "data": {
                'participant_id': '12345678',
                'nhs_number': '1',
                'first_name': 'Jane',
                'last_name': 'Doe',
                'test_results': [{'slide_number': '19035247'}]
            }
        }

        actual_response = self.unwrapped_handler(event, {})

        assertApiResponse(self, 200, expected_response_body, actual_response)

        service_mock.assert_called_with(participant_id, True)


@patch('get_participant_with_results.get_participant_with_results.log', Mock())
class TestAudit(TestCase):

    def setUp(self):
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    @patch(f'{module}.get_session_from_lambda_event', Mock())
    @patch(f'{module}.audit')
    @patch(f'{module}.get_participants_and_results_response_by_participant_id')
    def test_lambda_handler_calls_audit_with_participant_id_if_successfully_found(self, service_mock, audit_mock):
        service_mock.return_value = {
            "data": {
                "participant_id": "test_participant_id",
                "nhs_number": "1",
                "first_name": "Jane",
                "last_name": "Doe"
            }
        }
        event = {'pathParameters': {'participant_id': 'test_participant_id'}, 'headers': None}

        self.unwrapped_handler(event, {})
        _, kwargs = audit_mock.call_args

        self.assertEqual(kwargs['participant_ids'], ['test_participant_id'])

    @patch(f'{module}.get_session_from_lambda_event', Mock())
    @patch(f'{module}.audit')
    @patch(f'{module}.get_participants_and_results_response_by_participant_id')
    def test_lambda_handler_calls_audit_with_correct_action(self, service_mock, audit_mock):
        service_mock.return_value = {
            "data": {
                "participant_id": "test_participant_id",
                "nhs_number": "1",
                "first_name": "Jane",
                "last_name": "Doe"
            }
        }
        event = {'pathParameters': {'participant_id': 'test_participant_id'}, 'headers': None}

        self.unwrapped_handler(event, {})
        _, kwargs = audit_mock.call_args

        self.assertEqual(kwargs['action'], AuditActions.VIEW_PARTICIPANT)

    @patch(f'{module}.get_session_from_lambda_event')
    @patch(f'{module}.audit')
    @patch(f'{module}.get_participants_and_results_response_by_participant_id')
    def test_lambda_handler_calls_passes_session_correctly_to_audit(self, service_mock, audit_mock, get_session_mock):
        session = {'session_token': 'test_session_token', 'user_data': 'some_data'}
        get_session_mock.return_value = session

        service_mock.return_value = {
            "data": {
                "participant_id": "test_participant_id",
                "nhs_number": "1",
                "first_name": "Jane",
                "last_name": "Doe"
            }
        }
        event = {'pathParameters': {'participant_id': 'test_participant_id'}, 'headers': None,
                 'requestContext': {'authorizer': {'session': '{"session_key": "session_value"}'}}}

        self.unwrapped_handler(event, {})
        get_session_mock.assert_called_with(event)
        _, kwargs = audit_mock.call_args

        self.assertEqual(kwargs['session'], session)

    @patch(f'{module}.get_session_from_lambda_event')
    @patch(f'{module}.audit')
    @patch(f'{module}.get_participants_and_results_response_by_participant_id')
    def test_lambda_handler_does_not_call_audit_with_participant_id_if_not_found(
            self, service_mock, audit_mock, get_session_mock):
        service_mock.return_value = {}
        event = {'pathParameters': {'participant_id': 'test_participant_id'}, 'headers': None}

        self.unwrapped_handler(event, {})

        get_session_mock.assert_not_called()
        audit_mock.assert_not_called()
