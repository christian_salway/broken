from unittest.case import TestCase
import json
import os
import common
from mock import call, patch, Mock
from datetime import datetime, date, timezone
from ddt import ddt, data, unpack
from load_demographics.log_references import LogReference
from common.models.participant import ParticipantStatus

DIRECTORY_NAME = os.path.dirname(__file__)


def get_record(fields=None):
    if fields is None:
        fields = {}
    record = {
        'active': True,
        'nhs_number': '12345678',
        'next_test_due_date': '2020-02-13',
        'gender': '0',
        'sanitised_nhs_number': '12345678',
        'is_ceased': False, 'is_invalid_patient': False, 'is_fp69': False,
        'participant_id': '12345', 'sort_key': 'PARTICIPANT',
        'cohort_raw_data': {'received': '2019-12-05T09:45:23.123456+00:00', 'value': 'raw'}
    }
    record.update(fields)
    filtered = {k: v for k, v in record.items() if v is not None}
    return filtered


@ddt
@patch('load_demographics.participant_record_updater.generate_participant_id')
class TestParticipantRecordUpdater(TestCase):

    FIXED_NOW_TIME = datetime(2019, 12, 5, 9, 45, 23, 123456, tzinfo=timezone.utc)
    maxDiff = None

    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    def setUp(self):
        self.log_patcher = patch('load_demographics.participant_record_updater.log').start()

        import load_demographics.participant_record_updater as participant_record_updater_module
        import load_demographics as load_demographics_module
        self.participant_record_updater_module = participant_record_updater_module
        self.load_demographics_module = load_demographics_module

    @patch('load_demographics.participant_record_updater.calculate_next_test_due_date_for_new_participant')
    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    @patch('common.utils.date_utils.datetime', Mock(wraps=datetime))
    def test_next_test_due_date_calculator_is_used_if_next_test_due_date_not_present_in_existing_record(
            self, calculator_mock, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        common.utils.date_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        calculator_mock.return_value = date(2023, 5, 17)
        participant_mock.return_value = '12345'

        nhs_number = '12345678'
        existing_record = {'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {
            'gender': '2',
            'raw_data': 'raw',
            'data_record_type': 'AMENDED',
            'date_of_birth': '19950704',
            'is_fp69': 'Y'
        }
        expected_updated_record = get_record({
            'gender': '2',
            'is_fp69': True,
            'date_of_birth': '1995-07-04',
            'next_test_due_date': '2023-05-17'
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)

        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_gender_is_set_to_unspecified_if_not_supplied_in_demographics_data(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'raw_data': 'raw', 'data_record_type': 'AMENDED'}
        participant_mock.return_value = '12345'
        expected_updated_record = get_record({'next_test_due_date': '2019-12-06'})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)

        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_is_invalid_patient_flag_is_set_to_true_if_demographics_data_is_invalid_flag_is_y(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'is_invalid': 'Y', 'raw_data': 'raw', 'data_record_type': 'AMENDED'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({'is_invalid_patient': True, 'next_test_due_date': '2019-12-06'})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)

        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_is_ceased_flag_is_set_to_false_if_data_record_type_is_removed(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'data_record_type': 'REMOVED', 'raw_data': 'raw'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({'active': False, 'is_ceased': False, 'next_test_due_date': None})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_is_fp9_flag_is_set_to_false_if_record_is_false(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw', 'is_fp69': 'false'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({'is_fp69': False, 'next_test_due_date': '2019-12-06'})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_is_fp9_flag_is_set_to_true_if_record_is_true(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw', 'is_fp69': 'true'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({'is_fp69': True, 'next_test_due_date': '2019-12-06'})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_is_ceased_flag_is_set_to_false_and_active_set_to_false_if_participant_is_deceased(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw', 'date_of_death': '20200503',
                             'reason_for_removal_code': 'DEA'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({'active': False, 'is_ceased': False, 'next_test_due_date': None,
                                              'date_of_death': '2020-05-03', 'reason_for_removal_code': 'DEA'})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @unpack
    @data(
        ('CGA', False, None),
        ('MB', False, None),
        ('LDN', False, None),
        ('NIT', False, None),
        ('ORR', False, None),
        ('RFI', False, None),
        ('SCT', False, None),
        ('TRA', False, None),
        ('RPR', True, '2019-12-06'),
        ('RDI', True, '2019-12-06'),
        ('RDR', True, '2019-12-06'),
        ('DIS', True, '2019-12-06'),
        ('OPA', True, '2019-12-06'),
        ('AFL', True, '2019-12-06'),
        ('AFN', True, '2019-12-06'),
        ('SDL', True, '2019-12-06'),
        ('SDN', True, '2019-12-06'),
    )
    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_active_is_set_if_participant_has_reason_for_removal(
        self, removal_reason, expected_active,
        expected_ntdd, participant_mock
    ):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw',
                             'reason_for_removal_code': removal_reason}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({
            'active': expected_active,
            'is_ceased': False,
            'next_test_due_date': expected_ntdd,
            'reason_for_removal_code': removal_reason
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @unpack
    @data(
        ('1', '20191130', None, None, False),
        ('1', '20191130', 'ABC123', '2019-12-06', True),
        (None, '20191130', 'ABC123', '2019-12-06', True),
        (None, '20191130', None, '2019-12-06', True)
    )
    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_active_is_set_if_participant_has_informal_death_status(
        self, status_of_death, date_of_death, gp_provider_code,
        expected_next_test_due_date, expected_active, participant_mock
    ):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT',
                           'registered_gp_practice_code': 'ABC123'}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw',
                             'status_of_death_notification': status_of_death,
                             'date_of_death': date_of_death, 'gp_provider_code': gp_provider_code}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({
            'active': expected_active,
            'date_of_death': '2019-11-30',
            'next_test_due_date': expected_next_test_due_date,
            'registered_gp_practice_code': gp_provider_code,
            'status_of_death_notification': status_of_death
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_active_is_set_to_false_if_participant_has_moved_from_england_to_scot(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw',
                             'reason_for_removal_code': 'SCT'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({
            'active': False,
            'is_ceased': False,
            'next_test_due_date': None,
            'reason_for_removal_code': 'SCT'
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_active_is_set_to_false_if_participant_has_moved_from_england_to_ni(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw',
                             'reason_for_removal_code': 'NIT'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({
            'active': False,
            'is_ceased': False,
            'next_test_due_date': None,
            'reason_for_removal_code': 'NIT'
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_active_is_set_to_false_if_participant_has_moved_from_england_to_wales(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw',
                             'previous_nhais_posting': 'TEST', 'nhais_posting': 'CYM'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({
            'active': False,
            'is_ceased': False,
            'next_test_due_date': None,
            'nhais_cipher': 'CYM',
            'reason_for_removal_code': 'CYM',
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_active_is_set_to_active_and_ntdd_set_if_existing_participant_has_moved_from_rogb_to_eng(
            self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': None, 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw',
                             'previous_nhais_posting': 'CYM', 'nhais_posting': 'LND'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({
            'active': True,
            'is_ceased': False,
            'next_test_due_date': '2020-03-05',
            'nhais_cipher': 'LND'
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_active_is_set_to_active_and_ntdd_set_if_new_participant_has_moved_from_rogb_to_eng(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw',
                             'previous_nhais_posting': 'CYM', 'nhais_posting': 'LND'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({
            'active': True,
            'is_ceased': False,
            'next_test_due_date': '2020-03-05',
            'nhais_cipher': 'LND',
            'status': ParticipantStatus.CALLED
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.calculate_next_test_due_date_for_new_participant')
    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    @patch('common.utils.date_utils.datetime', Mock(wraps=datetime))
    def test_active_is_set_ntdd_set_default_cipher_set_if_participant_has_invalid_nhais_posting(
            self, calculator_mock, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        common.utils.date_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        calculator_mock.return_value = date(2023, 5, 17)
        nhs_number = '12345678'
        existing_record = {}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw', 'date_of_birth': '1990-01-01',
                             'previous_nhais_posting': 'CYM', 'nhais_posting': 'INVALID'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({
            'active': True,
            'date_of_birth': '1990-01-01',
            'is_ceased': False,
            'next_test_due_date': '2023-05-17',
            'nhais_cipher': 'ENG',
            'status': ParticipantStatus.CALLED
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PART0028}),
            call({
                'log_reference': LogReference.PART0012,
                'field_name': 'is_fp69',
                'field_index': 39,
                'value': None,
                'expected_value': 'true | false'
            })
        ])

        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.calculate_next_test_due_date_for_new_participant')
    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    @patch('common.utils.date_utils.datetime', Mock(wraps=datetime))
    def test_active_is_set_default_cipher_set_if_participant_has_invalid_nhais_posting1(
            self, calculator_mock, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        common.utils.date_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        calculator_mock.return_value = date(2023, 5, 17)
        nhs_number = '12345678'
        existing_record = {
            'next_test_due_date': None,
            'participant_id': '12345',
            'nhais_cipher': 'LND',
            'sort_key': 'PARTICIPANT'
        }
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw', 'date_of_birth': '1990-01-01',
                             'previous_nhais_posting': 'CYM', 'nhais_posting': 'INVALID'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({
            'active': True,
            'date_of_birth': '1990-01-01',
            'is_ceased': False,
            'next_test_due_date': '2023-05-17',
            'nhais_cipher': 'LND'
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_active_is_set_to_true_if_ceased_participant_has_moved_from_rogb_to_eng(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {
            'next_test_due_date': None,
            'participant_id': '12345',
            'sort_key': 'PARTICIPANT',
            'is_ceased': True
        }
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw',
                             'previous_nhais_posting': 'CYM', 'nhais_posting': 'LND'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({
            'active': True,
            'is_ceased': True,
            'next_test_due_date': None,
            'nhais_cipher': 'LND',
            'status': ParticipantStatus.CEASED,
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @unpack
    @data(
        ('DMS', 'DMS', True, True),
        ('DMS', 'DMS', False, False),
        ('DMS', 'ENG', True, True),
        ('DMS', 'SCT', True, True)
    )
    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    @patch('load_demographics.participant_record_updater.organisation_is_dms_valid')
    def test_active_is_set_for_dms_participants(
        self,
        previous_pds_posting,
        current_pds_posting,
        org_valid,
        expected_active,
        org_valid_mock,
        participant_mock
    ):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        org_valid_mock.return_value = org_valid

        existing_record = {
            'participant_id': 'PART1',
            'sort_key': 'PARTICIPANT',
            'date_of_birth': '1990-01-01',
            'is_ceased': False,
            'next_test_due_date': '2023-05-17',
        }

        demographics_data = {
            'data_record_type': 'AMENDED',
            'raw_data': 'raw',
            'previous_nhais_posting': previous_pds_posting,
            'nhais_posting': current_pds_posting,
            'gp_provider_code': 'CODE1'
        }

        nhs_number = '1234567890'

        expected_updated_record = {
            'participant_id': 'PART1',
            'sort_key': 'PARTICIPANT',
            'nhs_number': nhs_number,
            'sanitised_nhs_number': nhs_number,
            'gender': '0',
            'active': expected_active,
            'is_ceased': False,
            'nhais_cipher': current_pds_posting,
            'is_invalid_patient': False,
            'is_fp69': False,
            'registered_gp_practice_code': 'CODE1',
            'cohort_raw_data': {
                'value': 'raw',
                'received': '2019-12-05T09:45:23.123456+00:00'
            }
        }

        if (expected_active):
            expected_updated_record['next_test_due_date'] = '2023-05-17'

        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number,
            existing_record,
            demographics_data
        )

        self.assertDictEqual(expected_updated_record, actual_updated_record)

        if (current_pds_posting == 'DMS'):
            org_valid_mock.assert_called_with('CODE1')
        else:
            org_valid_mock.assert_not_called()

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_is_ceased_flag_is_set_to_false_if_data_record_type_is_amend_and_participant_is_not_deceased(
            self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'data_record_type': 'AMENDED', 'raw_data': 'raw'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({'is_ceased': False, 'next_test_due_date': '2019-12-06'})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_fields_in_existing_record_that_cannot_be_changed_by_message_are_left_unchanged(self,
                                                                                            participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME

        nhs_number = '12345678'
        existing_record = {'nhs_number': '12345678', 'sanitised_nhs_number': '12345678',
                           'favourite_colour': 'blue', 'next_test_due_date': '2019-12-06',
                           'participant_id': '12345', 'sort_key': 'PARTICIPANT'}
        demographics_data = {'raw_data': 'raw', 'data_record_type': 'AMENDED'}
        participant_mock.return_value = '12345'

        expected_updated_record = get_record({'favourite_colour': 'blue', 'next_test_due_date': '2019-12-06'})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)

        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.calculate_next_test_due_date_for_new_participant')
    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_record_created_correctly_when_demographics_data_contains_all_fields(
            self, calculator_mock, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME

        nhs_number = '12345678'
        existing_record = {}
        calculator_mock.return_value = date(2020, 2, 13)
        participant_mock.return_value = '12345'
        demographics_data = json.load(open(
            os.path.join(DIRECTORY_NAME, 'fixtures/updater/new_participant_message.json')))

        expected_updated_record = json.load(open(os.path.join(DIRECTORY_NAME,
                                                              'expected/updater/new_participant_record.json')))

        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)

        self.assertDictEqual(expected_updated_record, actual_updated_record)
        calculator_mock.assert_called_with(date(1993, 5, 7))

    @patch('load_demographics.participant_record_updater.calculate_next_test_due_date_for_new_participant')
    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_record_updated_correctly_when_demographics_data_and_existing_record_both_contain_all_fields(
            self, calculator_mock, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = json.load(open
                                    (os.path.join(DIRECTORY_NAME,
                                                  'fixtures/updater/existing_participant_to_be_removed.json')))

        demographics_data = json.load(open(os.path.join(DIRECTORY_NAME,
                                                        'fixtures/updater/removed_participant_message.json')))

        expected_updated_record = json.load(
            open(os.path.join(DIRECTORY_NAME, 'expected/updater/removed_participant_record.json')))

        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)

        self.assertDictEqual(expected_updated_record, actual_updated_record)
        participant_mock.assert_not_called()
        calculator_mock.assert_not_called()

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_fields_not_provided_in_demographics_data_are_removed_from_existing_record(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        existing_record = json.load(
            open(os.path.join(DIRECTORY_NAME, 'fixtures/updater/existing_participant_to_be_amended.json')))
        demographics_data = {'raw_data': 'raw', 'data_record_type': 'AMENDED'}

        expected_updated_record = get_record({'next_test_due_date': '2018-01-17'})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)

        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_new_participant_id_new_status_generated_new_records(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        participant_id = '123456789'
        existing_record = {'next_test_due_date': '2019-12-06'}
        demographics_data = {'raw_data': 'raw', 'data_record_type': 'AMENDED'}
        participant_mock.return_value = participant_id
        expected_updated_record = get_record({
            'is_fp69': False,
            'next_test_due_date': '2019-12-06',
            'participant_id': '123456789',
            'status': ParticipantStatus.CALLED
        })
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)

        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_should_not_call_generate_participant_id_for_existing_records(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        participant_id = '123456789'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': participant_id,
                           'sort_key': 'PARTICIPANT'}
        demographics_data = {'raw_data': 'raw', 'data_record_type': 'AMENDED'}
        participant_mock.return_value = participant_id
        expected_updated_record = get_record({
            'is_fp69': False,
            'next_test_due_date': '2019-12-06',
            'participant_id': '123456789'})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        participant_mock.assert_not_called()
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    @patch('load_demographics.participant_record_updater.datetime', Mock(wraps=datetime))
    def test_should_replace_cohort_raw_data_in_existing_participant_with_new_raw_data(self, participant_mock):
        self.participant_record_updater_module.datetime.now.return_value = self.FIXED_NOW_TIME
        nhs_number = '12345678'
        participant_id = '123456789'
        existing_record = {'next_test_due_date': '2019-12-06', 'participant_id': participant_id,
                           'sort_key': 'PARTICIPANT',
                           'cohort_raw_data': {'received': '2018-04-27T15:37:08.012+00:00', 'value': 'old|raw|data'}}
        demographics_data = {'raw_data': 'raw', 'data_record_type': 'AMENDED'}
        participant_mock.return_value = participant_id
        expected_updated_record = get_record({
            'active': True,
            'is_fp69': False,
            'next_test_due_date': '2019-12-06',
            'participant_id': '123456789'})
        actual_updated_record = self.participant_record_updater_module.update_existing_record_with_demographics_data(
            nhs_number, existing_record, demographics_data)
        self.assertDictEqual(expected_updated_record, actual_updated_record)

    def test_previous_address_record_is_created_with_correct_information(self, participant_mock):
        existing_record = json.load(
            open(os.path.join(DIRECTORY_NAME, 'expected/updater/new_participant_record.json')))

        current_date = date.today().isoformat()

        previous_address_item = {
            'participant_id': '12345',
            'sort_key': 'PREVADDR#' + current_date,
            'date_to': current_date,
            'address': {
                'address_line_1': '1 TEST FLAT',
                'address_line_2': '2 TEST BUILDING',
                'address_line_3': '3 TEST ROAD',
                'address_line_4': 'TEST TOWN',
                'address_line_5': 'TESTSHIRE',
                'postcode': 'LS11 5DR'
            },
            'date_from': '2018-07-01'
        }
        return_value = self.participant_record_updater_module.create_previous_address_record_from_existing_record(
            existing_record)

        self.assertEqual(return_value, previous_address_item)

    def test_set_fp69_flag_defaults_to_false(self, participant_mock):
        # Arrange
        value = {'is_fp69': 'invalid value'}
        existing_record = {}
        # Act
        self.participant_record_updater_module._set_is_fp69_flag(existing_record, value)
        # Assert
        self.assertEqual(False, existing_record['is_fp69'])

    def test_set_fp69_flag_parses_truthy_values(self, participant_mock):
        # Arrange
        values = ['Y', 'true', True, 'True']
        # Act
        for value in values:
            existing_record = {}
            self.participant_record_updater_module._set_is_fp69_flag(existing_record, {'is_fp69': value})
            # Assert
            self.assertEqual(True, existing_record['is_fp69'])

    def test_set_fp69_flag_parses_falsy_values(self, participant_mock):
        # Arrange
        values = ['N', 'false', False, None, 'False']
        # Act
        for value in values:
            existing_record = {}
            self.participant_record_updater_module._set_is_fp69_flag(existing_record, {'is_fp69': value})
            # Assert
            self.assertEqual(False, existing_record['is_fp69'])

    def test_logs_error_if_invalid_value(self, participant_mock):
        # Arrange
        value = {'is_fp69': 'invalid value'}
        # Act
        self.participant_record_updater_module._set_is_fp69_flag({}, value)
        # Assert
        self.log_patcher.assert_called_with({
            'log_reference': LogReference.PART0012,
            'field_name': 'is_fp69',
            'field_index': 39,
            'value': 'invalid value',
            'expected_value': 'true | false'})
