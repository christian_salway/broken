import json
import os
from unittest.case import TestCase
from mock import patch, call
from load_demographics.demographics_record_converter import convert_demographics_message_to_participant_schema_format
from load_demographics.log_references import LogReference


DIRECTORY_NAME = os.path.dirname(__file__)


class TestDemographicsRecordConverter(TestCase):

    @patch('load_demographics.demographics_record_converter.log')
    def test_converter_raises_exception_given_incorrect_number_of_fields(self, log_mock):
        message = {
            'body': '{"internal_id": "the_id", "file_name": "the_file", "line_index": "the_index", '
                    '"line": "just one field"}'
        }

        with self.assertRaises(Exception) as context:
            convert_demographics_message_to_participant_schema_format(message)

        expected_exception_message = 'Unexpected number of fields in raw data. Expected 42 fields. Received 1 fields.'
        actual_exception_message = context.exception.args[0]

        self.assertEqual(expected_exception_message, actual_exception_message)

        expected_log_calls = [
            call({'log_reference': LogReference.PART0002, 'file_name': 'the_file', 'line_index': 'the_index'})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('load_demographics.demographics_record_converter.log')
    def test_converter_raises_exception_given_unknown_record_type(self, log_mock):
        message = {
            'body': '{"internal_id": "the_id", "file_name": "the_file", "line_index": "the_index", '
                    '"line": "INVALID|||||||||||||||||||||||||||||||||||||||||"}'
        }

        with self.assertRaises(Exception) as context:
            convert_demographics_message_to_participant_schema_format(message)

        expected_exception_message = "Data record type is not one of the allowed types: ['NEW', 'REMOVED', 'AMENDED']"
        actual_exception_message = context.exception.args[0]

        self.assertEqual(expected_exception_message, actual_exception_message)

        expected_log_calls = [
            call({'log_reference': LogReference.PART0002, 'file_name': 'the_file', 'line_index': 'the_index'})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('load_demographics.demographics_record_converter.log')
    def test_converter_successfully_converts_record_with_only_record_type_populated(self, log_mock):
        message = {
            'body': '{"internal_id": "the_id", "file_name": "the_file", "line_index": "the_index", '
                    '"line": "NEW||1|||||||||||||||||||||||||||||||||||||||"}'
        }

        expected_record = {
            'data_record_type': 'NEW',
            'raw_data': 'NEW||1|||||||||||||||||||||||||||||||||||||||',
            'serial_change_number': 1
        }

        actual_record = convert_demographics_message_to_participant_schema_format(message)

        self.assertDictEqual(expected_record, actual_record)

        expected_log_calls = [
            call({'log_reference': LogReference.PART0002, 'file_name': 'the_file', 'line_index': 'the_index'})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('load_demographics.demographics_record_converter.log')
    def test_converter_successfully_converts_multiple_middle_names_in_record(self, log_mock):
        message = {
            'body': '{"internal_id": "the_id", "file_name": "the_file", "line_index": "the_index", '
                    '"line": "NEW||1||||||||||||three,middle,names|||||||||||||||||||||||||||"}'
        }

        expected_record = {
            'data_record_type': 'NEW',
            'other_given_names': 'three middle names',
            'raw_data': 'NEW||1||||||||||||three,middle,names|||||||||||||||||||||||||||',
            'serial_change_number': 1
        }

        actual_record = convert_demographics_message_to_participant_schema_format(message)

        self.assertDictEqual(expected_record, actual_record)

        expected_log_calls = [
            call({'log_reference': LogReference.PART0002, 'file_name': 'the_file', 'line_index': 'the_index'})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('load_demographics.demographics_record_converter.log')
    def test_converter_successfully_converts_record_with_all_fields_populated(self, log_mock):
        self.maxDiff = None
        message = json.load(open(
            os.path.join(DIRECTORY_NAME, 'fixtures/converter/message_with_all_fields_in_line.json')))

        expected_record = json.load(open(
            os.path.join(DIRECTORY_NAME, 'expected/converter/converted_message_with_all_fields_in_line.json')))

        actual_record = convert_demographics_message_to_participant_schema_format(message)

        self.assertDictEqual(expected_record, actual_record)

        expected_log_calls = [
            call({'log_reference': LogReference.PART0002, 'file_name': 'the_file', 'line_index': 'the_index'})
        ]

        log_mock.assert_has_calls(expected_log_calls)
