from datetime import date
import os
from unittest import TestCase
from unittest.mock import Mock
from mock import patch, call
from ddt import ddt, data, unpack
from datetime import datetime, timezone
from common.utils.audit_utils import AuditActions, AuditUsers

with patch('boto3.resource'):
    import load_demographics.load_demographics as demographics_module
    import common.utils.audit_utils as audit_utils
from load_demographics.log_references import LogReference
from load_demographics.validate_pds import PdsValidationContext
from common.models.participant import CeaseReason


DIRECTORY_NAME = os.path.dirname(__file__)


@ddt
@patch('load_demographics.load_demographics.GP_NOTIFICATION_QUEUE_URL', 'the_gp_notification_queue_url')
@patch('load_demographics.load_demographics.RESEND_NOTIFICATION_QUEUE_URL', 'the_resend_notification_queue_url')
@patch('load_demographics.load_demographics.get_internal_id', Mock(return_value='123'))
@patch('load_demographics.load_demographics.audit')
@patch('load_demographics.load_demographics.get_changed_fields_for_amended_records')
@patch('load_demographics.load_demographics.resend_notification')
@patch('load_demographics.load_demographics.create_or_update_participant_record_with_demographic_information')
@patch('load_demographics.load_demographics.create_previous_details_records')
@patch('load_demographics.load_demographics.get_existing_record_else_default_to_empty_dictionary')
@patch('load_demographics.load_demographics.convert_demographics_message_to_participant_schema_format')
@patch('load_demographics.load_demographics.log')
@patch('load_demographics.load_demographics.validate_message_only_contains_one_record')
@patch('load_demographics.load_demographics.update_log_metadata_from_record_event')
class TestLoadDemographicsLambdaHandler(TestCase):

    @classmethod
    def setUpClass(self):
        self.log_patcher = patch('common.log.logging')

    @classmethod
    def tearDownClass(self):
        self.log_patcher.stop()

    def setUp(self):
        self.log_patcher.start()

    @patch('load_demographics.load_demographics.cease_participant_and_send_notification_messages')
    @patch('load_demographics.load_demographics.participant_eligible_for_auto_cease')
    @patch('load_demographics.load_demographics.send_new_message')
    def test_new_participant_record_is_created_given_no_existing_record(
        self, send_message_mock, eligible_autocease_mock, cease_mock, update_log_metadata_mock,
        validate_message_mock, log_mock, converter_mock,
        get_participant_mock, create_previous_details_records_mock,
        create_update_participant_mock,
        resend_notification_mock, changed_fields_mock, audit_mock
    ):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'given_name': 'Tessa',
            'family_name': 'Person',
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'NEW',
            'date_of_death': '20210202',
            'some_data_key': 'some_data_value',
            'gender': 2,
            'nhais_posting': 'SUN'

            }
        updated_participant = {
            'participant_id': 'participant_id',
            'updated_key': 'updated_value',
            'registered_gp_practice_code': 'X09',
            'gender': 2
        }
        converter_mock.return_value = demographics_data
        get_participant_mock.return_value = {}
        create_update_participant_mock.return_value = updated_participant
        eligible_autocease_mock.return_value = False

        demographics_module.lambda_handler.__wrapped__(event, {})

        update_log_metadata_mock.assert_called_with(event)
        validate_message_mock.assert_called_with([{'messageId': 'the_message'}])
        converter_mock.assert_called_with({'messageId': 'the_message'})
        get_participant_mock.assert_called_with('7147161505')
        create_previous_details_records_mock.assert_not_called()
        create_update_participant_mock.assert_called_with('7147161505', {}, demographics_data)
        resend_notification_mock.assert_not_called()
        changed_fields_mock.assert_not_called()
        cease_mock.assert_not_called()
        send_message_mock.assert_called_with(
            'the_gp_notification_queue_url',
            {
                'participant_id': 'participant_id',
                'registered_gp_practice_code': 'X09',
                'notification_origin': 'GP_REGISTRATION',
                'internal_id': '123'
            }
        )
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.COHORT_ADD,
            additional_information=None,
            participant_ids=['participant_id'],
            user=audit_utils.AuditUsers.DEMOGRAPHICS
        )

        expected_log_calls = [
            call({'log_reference': LogReference.PART0001, 'message_id': 'the_message'}),
            call({'log_reference': LogReference.PART0018, 'age_group': 'unknown age'}),
            call({'log_reference': LogReference.PART0022,
                  'existing_category': 'No matching participant record', 'new_category':  'a', 'type': 'NEW', 'match': False}),  
            call({
                'log_reference': LogReference.PART0019, 'same_date_of_birth': False,
                'date_of_birth_status': "Incomplete", 'data_record_type': 'NEW',
                'matched_to_existing_record': False
            }),
            call({'log_reference': LogReference.PART0021, 'PDS_record_type': 'NEW', 'matched': False, 'is_invalid_patient_flag': False, 'age_group': 'NODATA', 'LDN': 'NODATA'}),  
            call({
                'log_reference': LogReference.PART0023, 'matched_to_existing_record': False,
                'PDS_first_forename': True, 'PDS_surname': True, 'NHAIS_first_forename': False,
                'NHAIS_surname': False, 'data_record_type': 'NEW'
            }),
            call({'log_reference': LogReference.PART0025, 'match_record': False, 'record_type': 'NEW'}),
            call({'log_reference': LogReference.PART0014, 'gender': 2, 'existing_gender': '', 'data_record_type': 'NEW'}),  
            call({'log_reference': LogReference.PART0011}),
            call({'log_reference': LogReference.PART0008, 'message_id': 'the_message'})
        ]
        log_mock.assert_has_calls(expected_log_calls)

    @patch('load_demographics.load_demographics.cease_participant_and_send_notification_messages')
    @patch('load_demographics.load_demographics.participant_eligible_for_auto_cease')
    def test_new_participant_record_is_created_given_no_existing_record_with_autocease(
            self, eligible_autocease_mock, cease_mock, update_log_metadata_mock, validate_message_mock, log_mock,
            converter_mock, get_participant_mock, create_previous_details_records_mock, create_update_participant_mock,
            resend_notification_mock, changed_fields_mock, audit_mock):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'given_name': 'Tessa',
            'family_name': 'Person',
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'NEW',
            'date_of_death': '20210201',
            'some_data_key': 'some_data_value',
            'gender': 2,
            'nhais_posting': 'ENG'
        }
        updated_participant = {
            'participant_id': 'participant_id',
            'updated_key': 'updated_value',
            'date_of_birth': '1890-01-01',
            'next_test_due_date': '2020-01-01',
            'gender': 2
        }
        converter_mock.return_value = demographics_data
        get_participant_mock.return_value = {}
        create_update_participant_mock.return_value = updated_participant
        eligible_autocease_mock.return_value = True

        demographics_module.lambda_handler.__wrapped__(event, {})

        update_log_metadata_mock.assert_called_with(event)
        validate_message_mock.assert_called_with([{'messageId': 'the_message'}])
        converter_mock.assert_called_with({'messageId': 'the_message'})
        get_participant_mock.assert_called_with('7147161505')
        cease_mock.assert_called_with(
            updated_participant, 'SYSTEM', {'reason': CeaseReason.AUTOCEASE_DUE_TO_AGE})
        create_previous_details_records_mock.assert_not_called()
        create_update_participant_mock.assert_called_with('7147161505', {}, demographics_data)
        resend_notification_mock.assert_not_called()
        changed_fields_mock.assert_not_called()
        audit_mock.assert_has_calls(
            [call(action=audit_utils.AuditActions.CEASE_EPISODE,
                  additional_information={'reason': CeaseReason.AUTOCEASE_DUE_TO_AGE},
                  participant_ids=['participant_id'],
                  user=audit_utils.AuditUsers.SYSTEM),
             call(action=audit_utils.AuditActions.COHORT_ADD,
                  additional_information=None,
                  participant_ids=['participant_id'],
                  user=audit_utils.AuditUsers.DEMOGRAPHICS)]
        )

        expected_log_calls = [
            call({'log_reference': LogReference.PART0001, 'message_id': 'the_message'}),
            call({'log_reference': LogReference.PART0018, 'age_group': 'unknown age'}),
            call({'log_reference': LogReference.PART0022, 'existing_category': 'No matching participant record', 'new_category':  'a', 'type': 'NEW', 'match': False}), 
            call({
                'log_reference': LogReference.PART0019, 'same_date_of_birth': False,
                'date_of_birth_status': "Incomplete", 'data_record_type': 'NEW',
                'matched_to_existing_record': False
            }),
            call({'log_reference': LogReference.PART0021, 'PDS_record_type': 'NEW', 'matched': False, 'is_invalid_patient_flag': False, 'age_group': 'NODATA', 'LDN': 'NODATA'}),   
            call({
                'log_reference': LogReference.PART0023, 'matched_to_existing_record': False,
                'PDS_first_forename': True, 'PDS_surname': True, 'NHAIS_first_forename': False,
                'NHAIS_surname': False, 'data_record_type': 'NEW'
            }),
            call({'log_reference': LogReference.PART0025, 'match_record': False, 'record_type': 'NEW'}),
            call({'log_reference': LogReference.PART0014, 'gender': 2, 'existing_gender': '', 'data_record_type': 'NEW'}),  
            call({'log_reference': LogReference.PART0008, 'message_id': 'the_message'})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('load_demographics.load_demographics.cease_participant_and_send_notification_messages')
    @patch('load_demographics.load_demographics.participant_eligible_for_auto_cease')
    def test_existing_participant_record_does_not_have_ntdd_made_active_while_ceased(
            self, eligible_autocease_mock, cease_mock, update_log_metadata_mock, validate_message_mock, log_mock,
            converter_mock, get_participant_mock, create_previous_details_records_mock, create_update_participant_mock,
            resend_notification_mock, changed_fields_mock, audit_mock):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'given_name': 'Tessa',
            'family_name': 'Person',
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'NEW',
            'some_data_key': 'some_data_value',
            'gender': 2,
            'nhais_posting': 'ENG'
        }
        existing_record = {
            'given_name': 'Tessa',
            'family_name': 'Person',
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'NEW',
            'some_data_key': 'some_data_value',
            'gender': 2,
            'nhais_posting': 'ENG',
            'is_ceased': True,
            'active': False
        }
        updated_participant = {
            'participant_id': 'participant_id',
            'updated_key': 'updated_value',
            'date_of_birth': '1890-01-01',
            'gender': 2,
            'active': True
        }
        converter_mock.return_value = demographics_data
        get_participant_mock.return_value = existing_record
        create_previous_details_records_mock.return_value = False
        create_update_participant_mock.return_value = updated_participant
        eligible_autocease_mock.return_value = True

        demographics_module.lambda_handler.__wrapped__(event, {})

        update_log_metadata_mock.assert_called_with(event)
        validate_message_mock.assert_called_with([{'messageId': 'the_message'}])
        converter_mock.assert_called_with({'messageId': 'the_message'})
        get_participant_mock.assert_called_with('7147161505')
        create_previous_details_records_mock.assert_called()
        create_update_participant_mock.assert_called_with('7147161505', existing_record, demographics_data)
        resend_notification_mock.assert_not_called()
        changed_fields_mock.assert_not_called()
        audit_mock.assert_has_calls(
            [call(action=audit_utils.AuditActions.COHORT_ADD,
                  additional_information=None,
                  participant_ids=['participant_id'],
                  user=audit_utils.AuditUsers.DEMOGRAPHICS)]
        )

        expected_log_calls = [
            call({'log_reference': LogReference.PART0001, 'message_id': 'the_message'}),
            call({'log_reference': LogReference.PART0024, 'existing_category': False,
                  'new_category': False, 'participant_age_bracket': 'of unknown age'}),
            call({'log_reference': LogReference.PART0017, 'age_group': 'unknown age'}),
            call({'log_reference': LogReference.PART0022, 'existing_category': 'NO',
                  'new_category':  'a', 'type': 'NEW', 'match': False}),
            call({'log_reference': LogReference.PART0019, 'same_date_of_birth': False,
                  'date_of_birth_status': "Incomplete", 'data_record_type': 'NEW', 'matched_to_existing_record': True}),
            call({'log_reference': LogReference.PART0021, 'PDS_record_type': 'NEW', 'matched': True,
                  'is_invalid_patient_flag': False, 'age_group': 'NODATA', 'LDN': 'NODATA'}),
            call({'log_reference': LogReference.PART0023, 'matched_to_existing_record': True,
                  'PDS_first_forename': True, 'PDS_surname': True, 'NHAIS_first_forename': False,
                  'NHAIS_surname': False, 'data_record_type': 'NEW'}),
            call({'log_reference': LogReference.PART0025, 'match_record': True, 'record_type': 'NEW',
                  'match_family': False, 'match_dob': False, 'match_gender': True}),
            call({'log_reference': LogReference.PART0014, 'gender': 2, 'existing_gender': 2,
                  'data_record_type': 'NEW'}),
            call({'log_reference': LogReference.PART0008, 'message_id': 'the_message'})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    def test_existing_participant_record_is_updated_and_previous_details_records_created_given_existing_record(
            self, update_log_metadata_mock, validate_message_mock, log_mock, converter_mock, get_participant_mock,
            create_previous_details_records_mock, create_update_participant_mock,
            resend_notification_mock, changed_fields_mock, audit_mock):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'given_name': 'Tessa',
            'family_name': 'Person',
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'NEW',
            'some_data_key': 'some_data_value',
            'gender': 2,
            'nhais_posting': 'SUN'
            }
        converter_mock.return_value = demographics_data
        participant = {'existing_key': 'existing_value', 'date_of_death': '2021-02-01', 'gender': 2}
        get_participant_mock.return_value = participant
        updated_participant = {'participant_id': 'participant_id', 'updated_key': 'updated_value', 'gender': 2}
        create_previous_details_records_mock.return_value = False
        create_update_participant_mock.return_value = updated_participant

        demographics_module.lambda_handler.__wrapped__(event, {})

        update_log_metadata_mock.assert_called_with(event)
        validate_message_mock.assert_called_with([{'messageId': 'the_message'}])
        converter_mock.assert_called_with({'messageId': 'the_message'})
        get_participant_mock.assert_called_with('7147161505')
        create_previous_details_records_mock.assert_called_with(participant, demographics_data)
        create_update_participant_mock.assert_called_with('7147161505', participant, demographics_data)
        resend_notification_mock.assert_not_called()
        changed_fields_mock.assert_not_called()
        audit_mock.assert_called_once_with(action=audit_utils.AuditActions.COHORT_ADD,
                                           additional_information=None,
                                           participant_ids=['participant_id'],
                                           user=audit_utils.AuditUsers.DEMOGRAPHICS)

        expected_log_calls = [
            call({'log_reference': LogReference.PART0001, 'message_id': 'the_message'}),
            call({'log_reference': LogReference.PART0024, 'existing_category': True, 'new_category': False,
                 'participant_age_bracket': 'of unknown age'}),
            call({'log_reference': LogReference.PART0017, 'age_group': 'unknown age'}),
            call({'log_reference': LogReference.PART0022,
                  'existing_category': 'NO', 'new_category':  'a', 'type': 'NEW', 'match': False}),
            call({
                'log_reference': LogReference.PART0019, 'same_date_of_birth': False,
                'date_of_birth_status': "Incomplete", 'data_record_type': 'NEW',
                'matched_to_existing_record': True
            }),
            call({'log_reference': LogReference.PART0021, 'PDS_record_type': 'NEW', 'matched': True, 'is_invalid_patient_flag': False, 'age_group': 'NODATA', 'LDN': 'NODATA'}), 
            call({
                'log_reference': LogReference.PART0023, 'matched_to_existing_record': True, 'PDS_first_forename': True,
                'PDS_surname': True, 'NHAIS_first_forename': False, 'NHAIS_surname': False, 'data_record_type': 'NEW'
            }),
            call({
                'log_reference': LogReference.PART0025,
                'match_record': True,
                'record_type': 'NEW',
                'match_family': False,
                'match_dob': False,
                'match_gender': True
            }),
            call({'log_reference': LogReference.PART0014, 'gender': 2, 'existing_gender': 2, 'data_record_type': 'NEW'}),  
            call({'log_reference': LogReference.PART0008, 'message_id': 'the_message'})
        ]
        log_mock.assert_has_calls(expected_log_calls)

    def test_audit_called_with_new_action_for_new_record(
            self, update_log_metadata_mock, validate_message_mock, log_mock, converter_mock, get_participant_mock,
            create_previous_details_records_mock, create_update_participant_mock,
            resend_notification_mock, changed_fields_mock, audit_mock):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'given_name': 'Tessa',
            'family_name': 'Person',
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'NEW',
            'some_data_key': 'some_data_value',
            'gender': 2,
            'nhais_posting': 'SUN'
            }
        converter_mock.return_value = demographics_data
        get_participant_mock.return_value = {}
        updated_participant = {'participant_id': 'participant_id', 'updated_key': 'updated_value'}
        create_update_participant_mock.return_value = updated_participant

        demographics_module.lambda_handler.__wrapped__(event, {})
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.COHORT_ADD,
            user=audit_utils.AuditUsers.DEMOGRAPHICS,
            participant_ids=['participant_id'],
            additional_information=None
        )
        resend_notification_mock.assert_not_called()
        changed_fields_mock.assert_not_called()

    @patch('load_demographics.load_demographics.pds_record_validation')
    def test_audit_called_with_new_action_for_fp_69(
            self,
            pds_record_validation_mock,
            update_log_metadata_mock,
            validate_message_mock,
            log_mock,
            converter_mock,
            get_participant_mock,
            create_previous_details_records_mock,
            create_update_participant_mock,
            resend_notification_mock,
            changed_fields_mock,
            audit_mock
    ):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'AMENDED',
            'some_data_key': 'some_data_value',
            'gender': 2,
            'nhais_posting': 'SUN'
        }
        converter_mock.return_value = demographics_data
        participant = {'existing_key': 'existing_value'}
        get_participant_mock.return_value = participant
        updated_participant = {'is_fp69': True, 'participant_id': 'participant_id', 'updated_key': 'updated_value'}
        create_update_participant_mock.return_value = updated_participant
        create_previous_details_records_mock.return_value = False
        changed_fields_mock.return_value = ['a_key']
        pds_record_validation_mock.return_value = PdsValidationContext(None, None, [], [], False, {})

        demographics_module.lambda_handler.__wrapped__(event, {})
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.FP69_SET,
                user=audit_utils.AuditUsers.DEMOGRAPHICS,
                participant_ids=['participant_id']
            ),
            call(
                action=audit_utils.AuditActions.COHORT_AMEND,
                user=audit_utils.AuditUsers.DEMOGRAPHICS,
                participant_ids=['participant_id'],
                additional_information={'changed_fields': ['a_key']}
            ),
        ])
        resend_notification_mock.assert_not_called()
        changed_fields_mock.assert_called_with(participant, updated_participant)

    @patch('load_demographics.load_demographics.pds_record_validation')
    def test_audit_not_called_with_fp69_if_set_to_false_from_none(
            self,
            pds_record_validation_mock,
            update_log_metadata_mock,
            validate_message_mock,
            log_mock,
            converter_mock,
            get_participant_mock,
            create_previous_details_records_mock,
            create_update_participant_mock,
            resend_notification_mock,
            changed_fields_mock,
            audit_mock
    ):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'NEW',
            'some_data_key': 'some_data_value',
            'gender': 2,
            'nhais_posting': 'SUN'
        }
        converter_mock.return_value = demographics_data
        participant = {'existing_key': 'existing_value'}
        get_participant_mock.return_value = participant
        updated_participant = {'is_fp69': False, 'participant_id': 'participant_id', 'updated_key': 'updated_value'}
        create_update_participant_mock.return_value = updated_participant
        create_previous_details_records_mock.return_value = False
        changed_fields_mock.return_value = []
        pds_record_validation_mock.return_value = PdsValidationContext(None, None, [], [], False, {})

        demographics_module.lambda_handler.__wrapped__(event, {})
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.COHORT_ADD,
            user=audit_utils.AuditUsers.DEMOGRAPHICS,
            participant_ids=['participant_id'],
            additional_information=None
        )
        resend_notification_mock.assert_not_called()

    @patch('load_demographics.load_demographics.pds_record_validation')
    def test_audit_called_with_amend_action_for_amended_record(
            self,
            pds_record_validation_mock,
            update_log_metadata_mock,
            validate_message_mock,
            log_mock,
            converter_mock,
            get_participant_mock,
            create_previous_details_records_mock,
            create_update_participant_mock,
            resend_notification_mock,
            changed_fields_mock,
            audit_mock
    ):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'AMENDED',
            'some_data_key': 'some_data_value', 'gender': 2
        }
        converter_mock.return_value = demographics_data
        participant = {'existing_key': 'existing_value'}
        get_participant_mock.return_value = participant
        updated_participant = {'participant_id': 'participant_id', 'updated_key': 'updated_value'}
        create_update_participant_mock.return_value = updated_participant
        create_previous_details_records_mock.return_value = False
        changed_fields_mock.return_value = []
        pds_record_validation_mock.return_value = PdsValidationContext(None, None, [], [], False, {})

        demographics_module.lambda_handler.__wrapped__(event, {})
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.COHORT_AMEND,
            user=audit_utils.AuditUsers.DEMOGRAPHICS,
            participant_ids=['participant_id'],
            additional_information=None
        )
        changed_fields_mock.assert_called_with(participant, updated_participant)

    def test_audit_called_with_remove_action_for_removed_record(
            self, update_log_metadata_mock, validate_message_mock, log_mock, converter_mock, get_participant_mock,
            create_previous_details_records_mock, create_update_participant_mock,
            resend_notification_mock, changed_fields_mock, audit_mock):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'nhs_number': '7147161505', 'data_record_type': 'REMOVED',
            'some_data_key': 'some_data_value', 'gender': 2
        }
        converter_mock.return_value = demographics_data
        participant = {'existing_key': 'existing_value'}
        get_participant_mock.return_value = participant
        updated_participant = {'participant_id': 'participant_id', 'updated_key': 'updated_value'}
        create_previous_details_records_mock.return_value = True
        create_update_participant_mock.return_value = updated_participant

        demographics_module.lambda_handler.__wrapped__(event, {})
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.COHORT_REMOVE,
            user=audit_utils.AuditUsers.DEMOGRAPHICS,
            participant_ids=['participant_id'],
            additional_information=None
        )
        resend_notification_mock.assert_not_called()
        changed_fields_mock.assert_not_called()

    @patch('load_demographics.load_demographics.pds_record_validation')
    def test_resend_notification_called_when_address_has_changed(
            self,
            pds_record_validation_mock,
            update_log_metadata_mock,
            validate_message_mock,
            log_mock,
            converter_mock,
            get_participant_mock,
            create_previous_details_records_mock,
            create_update_participant_mock,
            resend_notification_mock,
            changed_fields_mock,
            audit_mock
    ):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'AMENDED',
            'some_data_key': 'some_data_value',
            'gender': 2
        }
        converter_mock.return_value = demographics_data
        participant = {'existing_key': 'existing_value'}
        get_participant_mock.return_value = participant
        updated_participant = {'participant_id': 'participant_id', 'updated_key': 'updated_value'}
        create_update_participant_mock.return_value = updated_participant
        create_previous_details_records_mock.return_value = True
        changed_fields_mock.return_value = []
        pds_record_validation_mock.return_value = PdsValidationContext(None, None, [], [], False, {})

        demographics_module.lambda_handler.__wrapped__(event, {})
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.COHORT_AMEND,
            user=audit_utils.AuditUsers.DEMOGRAPHICS,
            participant_ids=['participant_id'],
            additional_information=None
        )
        resend_notification_mock.assert_called_with('participant_id', False, True)

    @patch('load_demographics.load_demographics.pds_record_validation')
    def test_resend_notification_called_when_fp69_unset(
            self,
            pds_record_validation_mock,
            update_log_metadata_mock,
            validate_message_mock,
            log_mock,
            converter_mock,
            get_participant_mock,
            create_previous_details_records_mock,
            create_update_participant_mock,
            resend_notification_mock,
            changed_fields_mock,
            audit_mock
    ):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'postcode': 'AB44 1XF',
            'nhs_number': '7147161505',
            'address_line_1': 'address line 1',
            'data_record_type': 'NEW',
            'some_data_key': 'some_data_value',
            'is_fp69': False, 'gender': 2
        }
        converter_mock.return_value = demographics_data
        participant = {'existing_key': 'existing_value', 'is_fp69': True, 'gender': 2}
        get_participant_mock.return_value = participant
        updated_participant = {
            'participant_id': 'participant_id', 'updated_key': 'updated_value', 'is_fp69': False, 'gender': 2
        }
        create_update_participant_mock.return_value = updated_participant
        create_previous_details_records_mock.return_value = False
        pds_record_validation_mock.return_value = PdsValidationContext(None, None, [], [], False, {})

        demographics_module.lambda_handler.__wrapped__(event, {})
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.FP69_UNSET,
                user=audit_utils.AuditUsers.DEMOGRAPHICS,
                participant_ids=['participant_id']
            ),
            call(
                action=audit_utils.AuditActions.COHORT_ADD,
                user=audit_utils.AuditUsers.DEMOGRAPHICS,
                participant_ids=['participant_id'],
                additional_information=None
            ),
        ])
        resend_notification_mock.assert_called_with('participant_id', True, False)
        changed_fields_mock.assert_not_called()

    @unpack
    @data(
        (
            {
                'nhs_number': '714716150',
                'data_record_type': 'NEW',
                'date_of_death': '20210201',
                'some_data_key': 'some_data_value',
                'gender': 2
            },
            ['CSMS-1'],
            True
        ),
        (
            {
                'nhs_number': '8781832052',
                'data_record_type': 'NEW',
                'date_of_death': '20210201',
                'some_data_key': 'some_data_value',
                'gender': 2
            },
            ['CSMS-7'],
            True
        ),
        (
            {
                'nhs_number': '8781832052',
                'postcode': 'ZZ99 1AB',
                'data_record_type': 'NEW',
                'date_of_death': '20210201',
                'some_data_key': 'some_data_value',
                'gender': 2
            },
            ['CSMS-6'],
            True
        ),
        (
            {
                'nhs_number': '8781832052',
                'postcode': 'ZZ99 1AB',
                'address_line_1': '1 House Street',
                'data_record_type': 'NEW',
                'date_of_death': '20210201',
                'some_data_key': 'some_data_value',
                'gender': 2
            },
            ['CSMS-5'],
            True
        )
    )
    @patch('load_demographics.load_demographics.send_new_message')
    @patch('load_demographics.load_demographics.get_message_body_from_event')
    @patch('load_demographics.load_demographics.participant_eligible_for_auto_cease')
    @patch('load_demographics.load_demographics.create_or_replace_rejected_record')
    @patch('load_demographics.load_demographics.datetime')
    def test_pds_validation_failure(
            self,
            demographics_data,
            expected_failed_rules,
            expected_hard_fail,
            datetime_mock,
            create_or_replace_rejected_record_mock,
            participant_eligible_for_auto_cease_mock,
            get_message_body_mock,
            send_message_mock,
            update_log_metadata_mock,
            validate_message_mock,
            log_mock,
            converter_mock,
            get_participant_mock,
            create_previous_details_records_mock,
            create_update_participant_mock,
            resend_notification_mock,
            changed_fields_mock,
            audit_mock
    ):
        event = {'Records': [{'messageId': 'the_message'}]}
        updated_participant = {
            'participant_id': 'participant_id',
            'updated_key': 'updated_value',
            'date_of_birth': '1890-01-01',
            'next_test_due_date': '2020-01-01',
            'gender': 2
        }
        converter_mock.return_value = demographics_data
        get_participant_mock.return_value = {}
        create_update_participant_mock.return_value = updated_participant
        get_message_body_mock.return_value = {'message': 'decoded'}
        datetime_mock.now.return_value = datetime(2020, 1, 1, tzinfo=timezone.utc)

        expected_log_calls = [
            call(
                {
                    'log_reference': LogReference.PART0001,
                    'message_id': 'the_message'
                }
            ),
            call(
                {
                    'log_reference': LogReference.PART0026,
                    'hard_fail': expected_hard_fail,
                    'failed_rules': expected_failed_rules
                }
            ),
        ]

        expected_send_message_call = (
            None,
            {
                'message': 'decoded',
                'metadata': {
                    'hard_fail': expected_hard_fail,
                    'failed_rules': expected_failed_rules
                }
            }
        )

        expected_dynamo_call = {
            'uuid': 'the_message',
            'nhs_number': demographics_data['nhs_number'],
            'rejection_type': 'PDS#2020-01-01',
            'message': 'decoded',
            'rejected_status': 'UNPROCESSED',
            'created': '2020-01-01T00:00:00+00:00',
            'metadata': {
                'hard_fail': expected_hard_fail,
                'failed_rules': expected_failed_rules,
            }
        }

        demographics_module.lambda_handler.__wrapped__(event, {})

        log_mock.assert_has_calls(expected_log_calls)
        send_message_mock.assert_called_with(*expected_send_message_call)
        create_or_replace_rejected_record_mock.assert_called_with(expected_dynamo_call)


@ddt
@patch('load_demographics.load_demographics.GP_NOTIFICATION_QUEUE_URL', 'the_gp_notification_queue_url')
@patch('load_demographics.load_demographics.RESEND_NOTIFICATION_QUEUE_URL', 'the_resend_notification_queue_url')
class TestLoadDemographicsHelperFunctions(TestCase):

    @classmethod
    def setUpClass(self):
        self.log_patcher = patch('common.log.logging')
        self.date_time_patcher = patch.object(demographics_module, 'datetime', Mock(wraps=datetime))
        self.date_time_patcher.start()

    @classmethod
    def tearDownClass(self):
        self.log_patcher.stop()
        self.date_time_patcher.stop()

    FIXED_NOW_TIME = datetime(2021, 3, 1, 12, 14, 27, 34, tzinfo=timezone.utc)
    FIXED_TODAY = datetime(2021, 3, 1)

    def setUp(self):
        self.log_patcher.start()

    @patch('load_demographics.load_demographics.query_participants_by_nhs_number_all_fields')
    def test_existing_record_is_returned_when_found(self, nhs_number_query_mock):
        nhs_number = "12345678"
        mock_uuid = "56890b5b-ad35-41a1-9052-19b6da39a774"
        existing_record = {"nhsNumber": "12345678", "date_of_birth": "1993-02-21",
                           "participant_id": mock_uuid, "sort_key": "PARTICIPANT", "address": {"postcode": "LS11 5DR"}}
        nhs_number_query_mock.return_value = [existing_record]

        returned_value = demographics_module.get_existing_record_else_default_to_empty_dictionary(nhs_number)

        nhs_number_query_mock.assert_called_with(nhs_number)
        self.assertEqual(returned_value, existing_record)

    @patch('load_demographics.load_demographics.log')
    @patch('load_demographics.load_demographics.query_participants_by_nhs_number_all_fields')
    def test_empty_dictionary_record_is_returned_when_no_existing_record_is_found(
            self, nhs_number_query_mock, log_mock):
        nhs_number = "12345678"
        nhs_number_query_mock.return_value = []

        returned_value = demographics_module.get_existing_record_else_default_to_empty_dictionary(nhs_number)

        nhs_number_query_mock.assert_called_with(nhs_number)
        self.assertEqual(returned_value, {})

        expected_log_calls = [call({'log_reference': LogReference.PART0003})]
        log_mock.assert_has_calls(expected_log_calls)

    @patch('load_demographics.load_demographics.create_previous_name_record')
    @patch('load_demographics.load_demographics.name_has_changed')
    @patch('load_demographics.load_demographics.create_previous_address_record')
    @patch('load_demographics.load_demographics.address_has_changed')
    @patch('load_demographics.load_demographics.log')
    def test_create_previous_address_and_name_records_creates_no_records_given_neither_name_or_address_changed(
            self, log_mock, address_changed_mock, create_address_record_mock,
            name_changed_mock, create_name_record_mock):
        existing_record = {'address': 'the_existing_address'}
        demographics_data = 'the_demographics_data'
        address_changed_mock.return_value = False
        name_changed_mock.return_value = False

        demographics_module.create_previous_details_records(existing_record, demographics_data)

        address_changed_mock.assert_called_with('the_existing_address', demographics_data)
        name_changed_mock.assert_called_with(existing_record, demographics_data)
        create_address_record_mock.assert_not_called()
        create_name_record_mock.assert_not_called()
        log_mock.assert_not_called()

    @patch('load_demographics.load_demographics.create_previous_name_record')
    @patch('load_demographics.load_demographics.name_has_changed')
    @patch('load_demographics.load_demographics.create_previous_address_record')
    @patch('load_demographics.load_demographics.address_has_changed')
    @patch('load_demographics.load_demographics.log')
    def test_create_previous_address_and_name_records_creates_previous_address_record_given_address_changed(
            self, log_mock, address_changed_mock, create_address_record_mock,
            name_changed_mock, create_name_record_mock):
        existing_record = {'address': 'the_existing_address'}
        demographics_data = 'the_demographics_data'
        address_changed_mock.return_value = True
        name_changed_mock.return_value = False

        demographics_module.create_previous_details_records(existing_record, demographics_data)

        address_changed_mock.assert_called_with('the_existing_address', demographics_data)
        name_changed_mock.assert_called_with(existing_record, demographics_data)
        create_address_record_mock.assert_called_with(existing_record)
        create_name_record_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.PART0004})]
        log_mock.assert_has_calls(expected_log_calls)

    @patch('load_demographics.load_demographics.create_previous_name_record')
    @patch('load_demographics.load_demographics.name_has_changed')
    @patch('load_demographics.load_demographics.create_previous_address_record')
    @patch('load_demographics.load_demographics.address_has_changed')
    @patch('load_demographics.load_demographics.log')
    def test_create_previous_address_and_name_records_creates_previous_name_record_given_name_changed(
            self, log_mock, address_changed_mock, create_address_record_mock,
            name_changed_mock, create_name_record_mock):
        existing_record = {'address': 'the_existing_address'}
        demographics_data = 'the_demographics_data'
        address_changed_mock.return_value = False
        name_changed_mock.return_value = True

        demographics_module.create_previous_details_records(existing_record, demographics_data)

        address_changed_mock.assert_called_with('the_existing_address', demographics_data)
        name_changed_mock.assert_called_with(existing_record, demographics_data)
        create_address_record_mock.assert_not_called()
        create_name_record_mock.assert_called_with(existing_record)

        expected_log_calls = [call({'log_reference': LogReference.PART0005})]
        log_mock.assert_has_calls(expected_log_calls)

    @patch('load_demographics.load_demographics.create_previous_name_record')
    @patch('load_demographics.load_demographics.name_has_changed')
    @patch('load_demographics.load_demographics.create_previous_address_record')
    @patch('load_demographics.load_demographics.address_has_changed')
    @patch('load_demographics.load_demographics.log')
    def test_create_previous_address_and_name_records_creates_previous_name_and_address_records_given_both_changed(
            self, log_mock, address_changed_mock, create_address_record_mock,
            name_changed_mock, create_name_record_mock):
        existing_record = {'address': 'the_existing_address'}
        demographics_data = 'the_demographics_data'
        address_changed_mock.return_value = True
        name_changed_mock.return_value = True

        demographics_module.create_previous_details_records(existing_record, demographics_data)

        address_changed_mock.assert_called_with('the_existing_address', demographics_data)
        name_changed_mock.assert_called_with(existing_record, demographics_data)
        create_address_record_mock.assert_called_with(existing_record)
        create_name_record_mock.assert_called_with(existing_record)

        expected_log_calls = [
            call({'log_reference': LogReference.PART0004}),
            call({'log_reference': LogReference.PART0005})
        ]
        log_mock.assert_has_calls(expected_log_calls)

    def test_address_has_changed_returns_true_when_postcode_has_changed(self):
        existing_address = {'postcode': 'old_postcode'}
        new_address = {'postcode': 'new_postcode'}
        is_changed = demographics_module.address_has_changed(existing_address, new_address)
        self.assertTrue(is_changed)

    def test_address_has_changed_returns_true_when_address_line_1_has_changed(self):
        existing_address = {'postcode': 'postcode', 'address_line_1': 'old_line_1'}
        new_address = {'postcode': 'postcode', 'address_line_1': 'new_line_1'}
        is_changed = demographics_module.address_has_changed(existing_address, new_address)
        self.assertTrue(is_changed)

    def test_address_has_changed_returns_true_when_address_line_2_has_changed(self):
        existing_address = {'postcode': 'postcode', 'address_line_2': 'old_line_2'}
        new_address = {'postcode': 'postcode', 'address_line_2': 'new_line_2'}
        is_changed = demographics_module.address_has_changed(existing_address, new_address)
        self.assertTrue(is_changed)

    def test_address_has_changed_returns_true_when_address_line_3_has_changed(self):
        existing_address = {'postcode': 'postcode', 'address_line_3': 'old_line_3'}
        new_address = {'postcode': 'postcode', 'address_line_3': 'new_line_3'}
        is_changed = demographics_module.address_has_changed(existing_address, new_address)
        self.assertTrue(is_changed)

    def test_address_has_changed_returns_true_when_address_line_4_has_changed(self):
        existing_address = {'postcode': 'postcode', 'address_line_4': 'old_line_4'}
        new_address = {'postcode': 'postcode', 'address_line_4': 'new_line_4'}
        is_changed = demographics_module.address_has_changed(existing_address, new_address)
        self.assertTrue(is_changed)

    def test_address_has_changed_returns_true_when_address_line_5_has_changed(self):
        existing_address = {'postcode': 'postcode', 'address_line_5': 'old_line_5'}
        new_address = {'postcode': 'postcode', 'address_line_5': 'new_line_5'}
        is_changed = demographics_module.address_has_changed(existing_address, new_address)
        self.assertTrue(is_changed)

    def test_address_has_changed_returns_false_when_addresses_both_empty(self):
        is_changed = demographics_module.address_has_changed({}, {})
        self.assertFalse(is_changed)

    def test_address_has_changed_returns_false_when_addresses_the_same(self):
        address = {
            'address_line_1': 'line 1',
            'address_line_2': 'line 2',
            'address_line_3': 'line 3',
            'address_line_4': 'line 4',
            'address_line_5': 'line 5',
            'postcode': 'postcode',
        }
        is_changed = demographics_module.address_has_changed(address, address)
        self.assertFalse(is_changed)

    def test_name_has_changed_returns_true_when_title_has_changed(self):
        existing_details = {'title': 'old_title'}
        new_details = {'title': 'new_title'}
        is_changed = demographics_module.name_has_changed(existing_details, new_details)
        self.assertTrue(is_changed)

    def test_name_has_changed_returns_true_when_first_name_has_changed(self):
        existing_details = {'first_name': 'old name'}
        new_details = {'given_name': 'new name'}
        is_changed = demographics_module.name_has_changed(existing_details, new_details)
        self.assertTrue(is_changed)

    def test_name_has_changed_returns_true_when_surname_has_changed(self):
        existing_details = {'last_name': 'old name'}
        new_details = {'given_name': 'new name'}
        is_changed = demographics_module.name_has_changed(existing_details, new_details)
        self.assertTrue(is_changed)

    def test_name_has_changed_returns_true_when_middle_names_have_changed(self):
        existing_details = {'middle_names': 'old_title'}
        new_details = {'other_given_names': 'new_title'}
        is_changed = demographics_module.name_has_changed(existing_details, new_details)
        self.assertTrue(is_changed)

    def test_name_has_changed_returns_false_when_names_both_empty(self):
        is_changed = demographics_module.name_has_changed({}, {})
        self.assertFalse(is_changed)

    def test_name_has_changed_returns_false_when_names_the_same(self):
        existing_name = {
            'title': 'sir',
            'first_name': 'test',
            'middle_names': 'with middle names',
            'last_name': 'person'
        }
        new_name = {
            'title': 'sir',
            'given_name': 'test',
            'other_given_names': 'with middle names',
            'family_name': 'person'
        }
        is_changed = demographics_module.name_has_changed(existing_name, new_name)
        self.assertFalse(is_changed)

    @patch('load_demographics.load_demographics.create_replace_record_in_participant_table')
    @patch('load_demographics.load_demographics.create_previous_address_record_from_existing_record')
    def test_create_previous_address_record_builds_and_saves_record(self, builder_mock, save_mock):
        existing_record = {'some_key': 'some_value'}
        address_record = {'sort_key': 'ADDRESS'}
        builder_mock.return_value = address_record

        demographics_module.create_previous_address_record(existing_record)

        builder_mock.assert_called_with(existing_record)
        save_mock.assert_called_with(address_record)

    @patch('load_demographics.load_demographics.get_internal_id', Mock(return_value='123'))
    @patch('load_demographics.load_demographics.log')
    @patch('load_demographics.load_demographics.create_replace_record_in_participant_table')
    @patch('load_demographics.load_demographics.update_existing_record_with_demographics_data')
    def test_create_or_update_participant_record_with_demographic_information_builds_and_saves_and_returns_record(
            self, builder_mock, save_mock, log_mock):
        nhs_number = '7147161505'
        existing_record = {'some_key': 'some_value'}
        demographics_data = {'cohort_key': 'cohort_value', 'serial_change_number': "2"}
        updated_record = {
            'participant_id': 'test_id',
            'updated_key': 'updated_value',
            'serial_change_number': "2",
            "registered_gp_practice_code": "test_code"
        }
        builder_mock.return_value = updated_record

        updated_participant = \
            demographics_module.create_or_update_participant_record_with_demographic_information(
                nhs_number, existing_record, demographics_data)

        builder_mock.assert_called_with(nhs_number, existing_record, demographics_data)
        save_mock.assert_called_with(updated_record, '', {})
        self.assertDictEqual(updated_record, updated_participant)
        expected_log_calls = [
            call({'log_reference': LogReference.PART0006}),
        ]
        log_mock.assert_has_calls(expected_log_calls)

    @patch('load_demographics.load_demographics.log')
    @patch('load_demographics.load_demographics.create_replace_record_in_participant_table')
    @patch('load_demographics.load_demographics.update_existing_record_with_demographics_data')
    def test_create_or_update_participant_demographic_information_builds_and_saves_record_with_conditional_check(
            self, builder_mock, save_mock, log_mock):
        nhs_number = '7147161505'
        existing_record = {'some_key': 'some_value', 'serial_change_number': "1"}
        demographics_data = {'cohort_key': 'cohort_value', 'serial_change_number': "2"}
        updated_record = {'updated_key': 'updated_value', 'serial_change_number': "2"}

        conditional_string = ' (serial_change_number < :scn) '
        conditional_string_values = {":scn": "2"}
        builder_mock.return_value = updated_record

        demographics_module.create_or_update_participant_record_with_demographic_information(nhs_number,
                                                                                             existing_record,
                                                                                             demographics_data)

        builder_mock.assert_called_with(nhs_number, existing_record, demographics_data)
        save_mock.assert_called_with(updated_record, conditional_string, conditional_string_values)

        expected_log_calls = [call({'log_reference': LogReference.PART0006})]
        log_mock.assert_has_calls(expected_log_calls)

    def test_get_changed_fields_for_amended_records_returns_empty_list_if_no_changes(self):
        existing_record = {'last_name': 'value_1', 'other_key': 'value_1'}
        updated_record = {'last_name': 'value_1', 'other_key': 'value_2'}

        changed_fields = demographics_module.get_changed_fields_for_amended_records(
            existing_record, updated_record
        )
        self.assertListEqual([], changed_fields)

    def test_get_changed_fields_for_amended_records_returns_list_of_changed_fields(self):
        existing_record = {'address': {'address_line_1': '1', 'address_line_2': '1', 'address_line_3': '1',
                                       'address_line_4': '1', 'address_line_5': '1', 'postcode': '1'},
                           'address_effective_from_date': '1',
                           'date_of_birth': '1', 'date_of_death': '1', 'first_name': '1', 'gender': '1',
                           'is_ceased': '1', 'is_invalid_patient': '1', 'last_name': '1', 'middle_names': '1',
                           'next_test_due_date': '1', 'nhais_cipher': '1', 'status': '1',
                           'reason_for_removal_code': '1', 'reason_for_removal_effective_from_date': '1',
                           'registered_gp_practice_code': '1', 'superseded_nhs_number': '1', 'title': '1',
                           'ignored_key': 'ignored_value_1'}
        updated_record = {'address': {'address_line_1': '2', 'address_line_2': '2', 'address_line_3': '2',
                                      'address_line_4': '2', 'address_line_5': '2', 'postcode': '2'},
                          'address_effective_from_date': '2',
                          'date_of_birth': '2', 'date_of_death': '2', 'first_name': '2', 'gender': '2',
                          'is_ceased': '2', 'is_invalid_patient': '2', 'last_name': '2', 'middle_names': '2',
                          'next_test_due_date': '2', 'nhais_cipher': '2', 'status': '2',
                          'reason_for_removal_code': '2', 'reason_for_removal_effective_from_date': '2',
                          'registered_gp_practice_code': '2', 'superseded_nhs_number': '2', 'title': '2',
                          'ignored_key': 'ignored_value_2'}

        expected_changed_fields = ['address_effective_from_date', 'date_of_birth',
                                   'date_of_death', 'first_name', 'gender', 'is_ceased', 'is_invalid_patient',
                                   'last_name', 'middle_names', 'nhais_cipher', 'status',
                                   'reason_for_removal_code', 'reason_for_removal_effective_from_date',
                                   'registered_gp_practice_code', 'superseded_nhs_number', 'title']

        actual_changed_fields = demographics_module.get_changed_fields_for_amended_records(
            existing_record, updated_record
        )
        self.maxDiff = None
        self.assertListEqual(expected_changed_fields, actual_changed_fields)

    @patch('load_demographics.load_demographics.get_internal_id', Mock(return_value='123'))
    @patch('load_demographics.load_demographics.send_new_message')
    @patch('load_demographics.load_demographics.log')
    def test_resend_notification_sends_correct_message_to_the_notifications_to_resend_queue(
            self, log_mock, send_message_mock):
        demographics_module.resend_notification('test_id', True, False)

        send_message_mock.assert_called_with('the_resend_notification_queue_url', {
            'participant_id': 'test_id',
            'internal_id': '123'
        })
        log_mock.assert_called_with({
            'log_reference': LogReference.PART0013,
            'is_fp69_unset': True,
            'has_address_changed': False
        })

    def test_previous_and_current_address_for_audit_log_with_changes(self):
        existing_record = {
            'address': {
                'address_line_1': '1',
                'address_line_2': 'Hope Street',
                'address_line_3': 'St Augusta',
                'address_line_4': 'Edinburgh',
                'address_line_5': 'West Lothian',
                'postcode': 'EH11 D12'
            },
            'address_effective_from_date': '1',
            'date_of_birth': '1', 'date_of_death': '1', 'first_name': '1', 'gender': '1',
            'is_ceased': '1', 'is_invalid_patient': '1', 'last_name': '1', 'middle_names': '1',
            'next_test_due_date': '1', 'nhais_cipher': '1', 'status': '1',
            'reason_for_removal_code': '1', 'reason_for_removal_effective_from_date': '1',
            'registered_gp_practice_code': '1', 'superseded_nhs_number': '1', 'title': '1',
            'ignored_key': 'ignored_value_1'}
        updated_record = {
            'address': {
                'address_line_1': '2',
                'address_line_2': 'Happy Street',
                'address_line_3': 'St Mirren',
                'address_line_4': 'Cardiff',
                'address_line_5': 'South Wales',
                'postcode': 'CF5 3DE'
            },
            'address_effective_from_date': '2',
            'date_of_birth': '2', 'date_of_death': '2', 'first_name': '2', 'gender': '2',
            'is_ceased': '2', 'is_invalid_patient': '2', 'last_name': '2', 'middle_names': '2',
            'next_test_due_date': '2', 'nhais_cipher': '2', 'status': '2',
            'reason_for_removal_code': '2', 'reason_for_removal_effective_from_date': '2',
            'registered_gp_practice_code': '2', 'superseded_nhs_number': '2', 'title': '2',
            'ignored_key': 'ignored_value_2'}

        expected_changed_fields = {
            'address_line_1': {
                'previous': '1',
                'current': '2',
            },
            'address_line_2': {
                'previous': 'Hope Street',
                'current': 'Happy Street',
            },
            'address_line_3': {
                'previous': 'St Augusta',
                'current': 'St Mirren',
            },
            'address_line_4': {
                'previous': 'Edinburgh',
                'current': 'Cardiff',
            },
            'address_line_5': {
                'previous': 'West Lothian',
                'current': 'South Wales',
            },
            'postcode': {
                'previous': 'EH11 D12',
                'current': 'CF5 3DE',
            }
        }

        actual_changed_fields = demographics_module.previous_and_current_address_for_audit_log(
            existing_record, updated_record
        )
        self.maxDiff = None
        self.assertDictEqual(expected_changed_fields, actual_changed_fields)

    def test_previous_and_current_address_for_audit_log_with_no_changes(self):
        existing_record = {'address': {
            'address_line_1': '1',
            'address_line_2': 'Hope Street',
            'address_line_3': 'St Augusta',
            'address_line_4': 'Edinburgh',
            'address_line_5': 'West Lothian',
            'postcode': 'EH11 D12'
        },
            'address_effective_from_date': '1',
            'date_of_birth': '1', 'date_of_death': '1', 'first_name': '1', 'gender': '1',
            'is_ceased': '1', 'is_invalid_patient': '1', 'last_name': '1', 'middle_names': '1',
            'next_test_due_date': '1', 'nhais_cipher': '1', 'status': '1',
            'reason_for_removal_code': '1', 'reason_for_removal_effective_from_date': '1',
            'registered_gp_practice_code': '1', 'superseded_nhs_number': '1', 'title': '1',
            'ignored_key': 'ignored_value_1'}
        updated_record = {'address': {
            'address_line_1': '1',
            'address_line_2': 'Hope Street',
            'address_line_3': 'St Augusta',
            'address_line_4': 'Edinburgh',
            'address_line_5': 'West Lothian',
            'postcode': 'EH11 D12'
        },
            'address_effective_from_date': '2',
            'date_of_birth': '2', 'date_of_death': '2', 'first_name': '2', 'gender': '2',
            'is_ceased': '2', 'is_invalid_patient': '2', 'last_name': '2', 'middle_names': '2',
            'next_test_due_date': '2', 'nhais_cipher': '2', 'status': '2',
            'reason_for_removal_code': '2', 'reason_for_removal_effective_from_date': '2',
            'registered_gp_practice_code': '2', 'superseded_nhs_number': '2', 'title': '2',
            'ignored_key': 'ignored_value_2'}

        expected_changed_fields = {}

        actual_changed_fields = demographics_module.previous_and_current_address_for_audit_log(
            existing_record, updated_record
        )
        self.maxDiff = None
        self.assertDictEqual(expected_changed_fields, actual_changed_fields)

    @patch('load_demographics.load_demographics.audit')
    @patch('load_demographics.load_demographics.close_existing_episode')
    def test_check_and_audit_active_change_no_change_to_active(self, close_existing_episode_mock, audit_mock):
        existing_record = {'active': True}
        updated_record = {'active': True}
        demographics_module.check_and_audit_active_change(updated_record, existing_record)
        audit_mock.assert_not_called()
        close_existing_episode_mock.assert_not_called()

    @unpack
    @data(({
        'active': True,
        'next_test_due_date': '2020-12-25'
    }, {'reason': 'ORR', 'updating_next_test_due_date_from': '2020-12-25'},), ({
        'active': True,
    }, {'reason': 'ORR'})
    )
    @patch('load_demographics.load_demographics.audit')
    @patch('load_demographics.load_demographics.close_existing_episode')
    def test_check_and_audit_active_change_change_active_to_false(self, existing_record, additional,
                                                                  close_existing_episode_mock, audit_mock):
        updated_record = {'active': False, 'participant_id': 'TEST'}
        demographics_module.check_and_audit_active_change(updated_record, existing_record)
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.DEACTIVATE_PARTICIPANT,
            user=audit_utils.AuditUsers.DEMOGRAPHICS,
            participant_ids=['TEST'],
            additional_information=additional
        )
        close_existing_episode_mock.assert_called_with('TEST', 'SYSTEM')

    @patch('load_demographics.load_demographics.audit')
    @patch('load_demographics.load_demographics.close_existing_episode')
    def test_check_and_audit_active_change_change_active_to_false_nit(self, close_existing_episode_mock, audit_mock):
        existing_record = {'active': True, 'next_test_due_date': '2020-12-25'}
        updated_record = {'active': False, 'participant_id': 'TEST', 'reason_for_removal_code': 'NIT'}
        demographics_module.check_and_audit_active_change(updated_record, existing_record)
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.DEACTIVATE_PARTICIPANT,
            user=audit_utils.AuditUsers.DEMOGRAPHICS,
            participant_ids=['TEST'],
            additional_information={'reason': 'NIT', 'updating_next_test_due_date_from': '2020-12-25'}
        )
        close_existing_episode_mock.assert_called_with('TEST', 'SYSTEM')

    @patch('load_demographics.load_demographics.audit')
    @patch('load_demographics.load_demographics.close_existing_episode')
    def test_check_and_audit_active_change_change_active_to_false_cym(self, close_existing_episode_mock, audit_mock):
        existing_record = {'active': True, 'next_test_due_date': '2020-12-25'}
        updated_record = {
            'active': False,
            'participant_id': 'TEST',
            'nhais_cipher': 'CYM',
            'reason_for_removal_code': 'CYM'
        }
        demographics_module.check_and_audit_active_change(updated_record, existing_record)
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.DEACTIVATE_PARTICIPANT,
            user=audit_utils.AuditUsers.DEMOGRAPHICS,
            participant_ids=['TEST'],
            additional_information={'reason': 'CYM', 'updating_next_test_due_date_from': '2020-12-25'}
        )
        close_existing_episode_mock.assert_called_with('TEST', 'SYSTEM')

    @patch('load_demographics.load_demographics.audit')
    @patch('load_demographics.load_demographics.close_existing_episode')
    def test_check_and_audit_active_change_change_active_to_true(self, close_existing_episode_mock, audit_mock):
        existing_record = {'active': False}
        updated_record = {'active': True, 'participant_id': 'TEST', 'next_test_due_date': '2020-12-26'}
        demographics_module.check_and_audit_active_change(updated_record, existing_record)
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.ACTIVATE_PARTICIPANT,
            user=audit_utils.AuditUsers.DEMOGRAPHICS,
            participant_ids=['TEST'],
            additional_information={'reason': 'Transferred in', 'updating_next_test_due_date_to': '2020-12-26'}
        )
        close_existing_episode_mock.assert_not_called()

    @patch('load_demographics.load_demographics.audit')
    @patch('load_demographics.load_demographics.close_existing_episode')
    def test_check_and_audit_active_change_ntdd_not_audited(self, close_existing_episode_mock, audit_mock):
        existing_record = {'active': False}
        updated_record = {'active': True, 'participant_id': 'TEST'}
        demographics_module.check_and_audit_active_change(updated_record, existing_record)
        audit_mock.assert_called_once_with(
            action=audit_utils.AuditActions.ACTIVATE_PARTICIPANT,
            user=audit_utils.AuditUsers.DEMOGRAPHICS,
            participant_ids=['TEST'],
            additional_information={'reason': 'Transferred in'}
        )
        close_existing_episode_mock.assert_not_called()

    @unpack
    @data(
        (
            {
                'date_of_birth': '1950-01-01',
                'date_of_death': '2020-01-01'
            },
            {},
            True,
            False,
            'over 65'
        ),
        (
            {
                'date_of_birth': 'aaa',
                'date_of_death': 'aaa'
            },
            {
                'date_of_birth': 'aaa',
                'date_of_death': 'aaa'
            },
            False,
            False,
            'of unknown age'
        ),
        (
            {
                'date_of_birth': '1950-01-01',
                'date_of_death': 'aaa'
            },
            {},
            False,
            False,
            'over 65'
        ),
        (
            {
                'date_of_birth': '2000-01-01'
            },
            {
                'date_of_death': '20200101'
            },
            False,
            True,
            'under 24.5'
        ),
        (
            {
                'date_of_death': '2020-01-01'
            },
            {
                'date_of_death': '20200101'
            },
            True,
            True,
            'of unknown age'
        )
    )
    @patch('load_demographics.load_demographics.log')
    def test_log_PDS_record_date_of_death_information_called(self,
                                                             existing_record,
                                                             demographics_data,
                                                             exsiting_category,
                                                             new_category,
                                                             age_bracket,
                                                             log_mock):

        demographics_module.datetime.today.return_value = self.FIXED_TODAY
        demographics_module._log_PDS_record_date_of_death_information(existing_record, demographics_data)

        expected_log_calls = [
            call({'log_reference': LogReference.PART0024,
                  'existing_category': exsiting_category,
                  'new_category': new_category,
                  'participant_age_bracket': age_bracket})
        ]
        log_mock.assert_has_calls(expected_log_calls)

    @unpack
    @data(
        (
            {},
            {
                'date_of_birth': '1980-01-01',
                'date_of_death': '2020-01-01'
            },
            False,
            True,
            'between 24.5 and 65'
        ),
        (
            {},
            {},
            False,
            False,
            'of unknown age'
        )
    )
    @patch('load_demographics.load_demographics.log')
    def test_log_PDS_record_date_of_death_information_not_called(self,
                                                                 existing_record,
                                                                 demographics_data,
                                                                 exsiting_category,
                                                                 new_category,
                                                                 age_bracket,
                                                                 log_mock):

        demographics_module.datetime.today.return_value = self.FIXED_TODAY
        demographics_module._log_PDS_record_date_of_death_information(existing_record, demographics_data)

        log_mock.assert_not_called()

    @patch('load_demographics.load_demographics.date')
    def test_age_groups_under_24_and_a_half(self, date_mock):
        date_mock.today.return_value = date(2024, 6, 30)
        test_date = '20000101'
        expected_result = 'under 24.5'
        actual_result = demographics_module.assign_to_age_group(test_date)
        self.assertEqual(expected_result, actual_result)

    @patch('load_demographics.load_demographics.date')
    def test_age_groups_over_24_and_a_half_to_49(self, date_mock):
        date_mock.today.return_value = date(2024, 7, 1)
        test_date = '20000101'
        expected_result = '24.5 to 49'
        actual_result = demographics_module.assign_to_age_group(test_date)
        self.assertEqual(expected_result, actual_result)

    @patch('load_demographics.load_demographics.date')
    def test_age_groups_50_to_65(self, date_mock):
        date_mock.today.return_value = date(2021, 3, 1)
        test_date = '19600101'
        expected_result = '50 to 65'
        actual_result = demographics_module.assign_to_age_group(test_date)
        self.assertEqual(expected_result, actual_result)

    @patch('load_demographics.load_demographics.date')
    def test_age_groups_over_65(self, date_mock):
        date_mock.today.return_value = date(2021, 3, 1)
        test_date = '19400101'
        expected_result = 'over 65'
        actual_result = demographics_module.assign_to_age_group(test_date)
        self.assertEqual(expected_result, actual_result)

    @patch('load_demographics.load_demographics.date')
    def test_age_groups_over_65_dash_format(self, date_mock):
        date_mock.today.return_value = date(2021, 3, 1)
        test_date = '1940-01-01'
        expected_result = 'over 65'
        actual_result = demographics_module.assign_to_age_group(test_date)
        self.assertEqual(expected_result, actual_result)

    @patch('load_demographics.load_demographics.date')
    def test_age_groups_over_65_slash_format(self, date_mock):
        date_mock.today.return_value = date(2021, 3, 1)
        test_date = '1940/01/01'
        expected_result = 'over 65'
        actual_result = demographics_module.assign_to_age_group(test_date)
        self.assertEqual(expected_result, actual_result)

    @patch('load_demographics.load_demographics.log')
    def test_date_of_birth_logging(self, log_mock):
        existing_record = {'date_of_birth': '1993-05-07'}

        demographics_data = {'date_of_birth': '19930507', 'data_record_type': 'NEW'}
        demographics_module.log_PDS_record_date_of_birth_information(existing_record, demographics_data)
        expected_log_call = {
            'log_reference': LogReference.PART0019, 'same_date_of_birth': True,
            'date_of_birth_status': "Complete", 'data_record_type': 'NEW',
            'matched_to_existing_record': True
        }
        log_mock.assert_called_with(expected_log_call)

        demographics_data = {'date_of_birth': '199305', 'data_record_type': 'AMEND'}
        demographics_module.log_PDS_record_date_of_birth_information(existing_record, demographics_data)
        expected_log_call = {
            'log_reference': LogReference.PART0019, 'same_date_of_birth': False,
            'date_of_birth_status': "Incomplete", 'data_record_type': 'AMEND',
            'matched_to_existing_record': True
        }
        log_mock.assert_called_with(expected_log_call)

        demographics_data = {'date_of_birth': '19934050', 'data_record_type': 'AMEND'}
        demographics_module.log_PDS_record_date_of_birth_information(existing_record, demographics_data)
        expected_log_call = {
            'log_reference': LogReference.PART0019, 'same_date_of_birth': False,
            'date_of_birth_status': "Can't be converted", 'data_record_type': 'AMEND',
            'matched_to_existing_record': True
        }
        log_mock.assert_called_with(expected_log_call)

    @patch('load_demographics.load_demographics.log')
    def test_date_of_birth_boundry_difference_log(self, log_mock):
        demographics_module.datetime.now.return_value = self.FIXED_NOW_TIME

        existing_date_of_birth = '2000-05-07'
        new_date_of_birth = '19930507'
        demographics_module.log_date_of_birth_boundry_difference(existing_date_of_birth, new_date_of_birth)
        log_mock.assert_called_with({
            'log_reference': LogReference.PART0020,
            'date_of_birth_boundry_difference': 'Date of Birth difference is over a 24.5 year old boundary i.e. PDS is over 24.5 (eligible) NHAIS says under 24.5 (not eligible).'  
        })

        existing_date_of_birth = '1993-05-07'
        new_date_of_birth = '20000507'
        demographics_module.log_date_of_birth_boundry_difference(existing_date_of_birth, new_date_of_birth)
        log_mock.assert_called_with({
            'log_reference': LogReference.PART0020,
            'date_of_birth_boundry_difference': 'Date of Birth difference is over a 24.5 year old boundary i.e. NHAIS is over 24.5 (eligible) PDS says under 24.5 (not eligible).'  
        })

        existing_date_of_birth = '1993-05-07'
        new_date_of_birth = '19700507'
        demographics_module.log_date_of_birth_boundry_difference(existing_date_of_birth, new_date_of_birth)
        log_mock.assert_called_with({
            'log_reference': LogReference.PART0020,
            'date_of_birth_boundry_difference': 'Date of Birth difference is over a 50 year old boundary i.e. PDS is over 50 (5 yr recall) NHAIS says under 50 (3 yr).'  
        })

        existing_date_of_birth = '1970-05-07'
        new_date_of_birth = '19930507'
        demographics_module.log_date_of_birth_boundry_difference(existing_date_of_birth, new_date_of_birth)
        log_mock.assert_called_with({
            'log_reference': LogReference.PART0020,
            'date_of_birth_boundry_difference': 'Date of Birth difference is over a 50 year old boundary i.e. NHAIS is over 50 (5 yr recall) PDS says under 50 (3 yr).'  
        })

        existing_date_of_birth = '1993-05-07'
        new_date_of_birth = '19550507'
        demographics_module.log_date_of_birth_boundry_difference(existing_date_of_birth, new_date_of_birth)
        log_mock.assert_called_with({
            'log_reference': LogReference.PART0020,
            'date_of_birth_boundry_difference': 'Date of Birth difference is over a 65 year old boundary i.e. PDS is over 65 (not eligible) NHAIS says under 65 (eligible).'  
        })

        existing_date_of_birth = '1955-05-07'
        new_date_of_birth = '19930507'
        demographics_module.log_date_of_birth_boundry_difference(existing_date_of_birth, new_date_of_birth)
        log_mock.assert_called_with({
            'log_reference': LogReference.PART0020,
            'date_of_birth_boundry_difference': 'Date of Birth difference is over a 65 year old boundary i.e. NHAIS is over 65 (not eligible) PDS says under 65 (eligible).'  
        })

    @unpack
    @data(({
        'address_line_1': '12 high street',
        'address_line_2': 'Leeds',
        'postcode': 'LS12 4SN'
    }, ('', '12LS124SN')), ({
        'address_line_1': 'The Laurels',
        'address_line_2': 'Leeds',
        'postcode': 'LS12 4SN'
    }, ('THELAURELS', 'LS124SN')), ({
        'address_line_1': 'Flat1 12 high street',
        'address_line_2': 'Leeds',
        'postcode': 'LS12 4SN'
    }, ('', '112LS124SN')), ({
        'address_line_1': 'Flat1 ',
        'address_line_2': '12 high street',
        'postcode': ''
    }, ('', '112')), ({
        'address_line_1': 'GROUND FLOOR ',
        'address_line_2': '12 high street',
        'postcode': 'ls12 4sn'
    }, ('', '12LS124SN')))
    def test_get_address_match_token(self, address, expected_result):

        actual_result = demographics_module._get_address_match_token(address)
        self.assertEqual(expected_result, actual_result)

    @unpack
    @data(({
        'address': {
            'address_line_1': '12 high street',
            'address_line_2': 'Leeds',
            'address_line_3': '',
            'postcode': 'LS12 4SN'
        }
    }, {
        'data_record_type': 'NEW',
        'address_line_1': '',
        'address_line_2': 'Leeds',
        'postcode': ''
    }, 'a', 'NO', 'NEW', False), ({
        'address': {
            'address_line_1': '12 high street',
            'address_line_2': 'Leeds',
            'address_line_3': '',
            'postcode': 'LS12 4SN'
        }
    }, {
        'data_record_type': 'AMENDED',
        'address_line_1': '12 high street',
        'address_line_2': 'Leeds',
        'postcode': 'LS12 4SN'
    }, 'a', 'a', 'AMENDED', True), ({
        'address': {
            'address_line_1': 'The Laurels',
            'address_line_2': 'Leeds',
            'address_line_3': '',
            'postcode': 'Ls12 4sn'
        }
    }, {
        'data_record_type': 'AMENDED',
        'address_line_1': 'The laurels',
        'address_line_2': 'Leeds',
        'postcode': 'LS12 4SN'
    }, 'a', 'a', 'AMENDED', True), ({}, {
        'data_record_type': 'NEW',
        'address_line_1': '12 high street',
        'address_line_2': 'Leeds',
        'postcode': 'LS12 5SN'
    }, 'No matching participant record', 'a', 'NEW', False), ({
        'address': {
            'address_line_1': 'Middle of nowhere',
            'address_line_2': 'Leeds',
            'address_line_3': '',
            'postcode': ''
        }
    }, {
        'data_record_type': 'AMENDED',
        'address_line_1': 'Middle of nowhere',
        'address_line_2': 'Leeds',
        'postcode': ''
    }, 'NO', 'NO', 'AMENDED', False))
    @patch('load_demographics.load_demographics.log')
    def test_log_PDS_record_address_information(self, existing_record, demographics, exist_cat,
                                                new_cat, record_type, match, log_mock):

        demographics_module._log_PDS_record_address_information(existing_record, demographics)
        log_mock.assert_called_with({
            'log_reference': LogReference.PART0022,
            'existing_category': exist_cat, 'new_category': new_cat, 'type': record_type, 'match': match  
        })

    @unpack
    @data(
        ('', 'NODATA'),
        ('Yesterday', 'NODATA'),
        ('20190101', '0-24.5'),
        ('19960830', '24.5-49'),
        ('19710301', '50-65'),
        ('19560301', 'Over 65')
    )
    def test_age_group_from_demographics(self, date_of_birth, expected_bucket):
        actual_bucket = demographics_module.age_group_from_demographics({'date_of_birth': date_of_birth})
        self.assertEqual(actual_bucket, expected_bucket)

    @unpack
    @data(
        (
            {
                'migrated_1C_data': {
                    'value': '||||||||||||L1|||||'
                }
            },
            'L1'
        ),
        (
            {
                'migrated_1R_data': {
                    'value': '|||||L2||||||||||||||||'
                }
            },
            'L2'
        ),
        (
            {},
            'NODATA'
        )
    )
    def test_ldn_from_existing_record(self, existing_record, expected_ldn):
        actual_ldn = demographics_module.ldn_from_existing_record(existing_record)
        self.assertEqual(actual_ldn, expected_ldn)

    @unpack
    @data(
        (
            {
                'migrated_1C_data': {
                    'value': '||||||||||||L1|||||'
                }
            },
            {
                'data_record_type': 'NEW',
                'is_invalid': 'Y',
                'date_of_birth': '19960830'
            },
            {
                'log_reference': LogReference.PART0021,
                'PDS_record_type': 'NEW',
                'matched': True,
                'is_invalid_patient_flag': True,
                'LDN': 'L1',
                'age_group': '24.5-49'
            }
        ),
        (
            {
                'migrated_1C_data': {
                    'value': '||||||||||||L1|||||'
                }
            },
            {
                'data_record_type': 'AMENDED',
                'is_invalid': '',
                'date_of_birth': '19960830'
            },
            {
                'log_reference': LogReference.PART0021,
                'PDS_record_type': 'AMENDED',
                'matched': True,
                'is_invalid_patient_flag': False,
                'LDN': 'L1',
                'age_group': '24.5-49'
            }
        ),
        (
            {
                'migrated_1C_data': {
                    'value': '||||||||||||L1|||||'
                }
            },
            {
                'data_record_type': 'REMOVED',
                'is_invalid': 'Y',
                'date_of_birth': '19710301'
            },
            {
                'log_reference': LogReference.PART0021,
                'PDS_record_type': 'REMOVED',
                'matched': True,
                'is_invalid_patient_flag': True,
                'LDN': 'L1',
                'age_group': '50-65'
            }
        ),
        (
            {},
            {
                'data_record_type': 'NEW',
                'is_invalid': '',
                'date_of_birth': '19710301'
            },
            {
                'log_reference': LogReference.PART0021,
                'PDS_record_type': 'NEW',
                'matched': False,
                'is_invalid_patient_flag': False,
                'age_group': '50-65',
                'LDN': 'NODATA'
            }
        ),
        (
            {},
            {
                'data_record_type': 'AMENDED',
                'is_invalid': '',
                'date_of_birth': '19710301'
            },
            {
                'log_reference': LogReference.PART0021,
                'PDS_record_type': 'AMENDED',
                'matched': False,
                'is_invalid_patient_flag': False,
                'age_group': '50-65',
                'LDN': 'NODATA'
            }
        ),
        (
            {},
            {
                'data_record_type': 'REMOVED',
                'is_invalid': 'Y',
                'date_of_birth': '19710301'
            },
            {
                'log_reference': LogReference.PART0021,
                'PDS_record_type': 'REMOVED',
                'matched': False,
                'is_invalid_patient_flag': True,
                'age_group': '50-65',
                'LDN': 'NODATA'
            }
        )
    )
    @patch('load_demographics.load_demographics.log')
    def test_log_pds_invalid(self, existing_record, demographics_data, expected_log_call, log_mock):
        demographics_module.log_pds_invalid(existing_record, demographics_data)
        log_mock.assert_called_with(expected_log_call)

    @patch('load_demographics.load_demographics.log')
    def test_name_information_logging_when_names_are_empty_and_not_matched(self, log_mock):
        existing_record = {}
        demographics_data = {'data_record_type': 'NEW', 'nhs_number': '123'}

        demographics_module.log_PDS_name_information(existing_record, demographics_data)

        log_mock.assert_called_with({
            'log_reference': LogReference.PART0023,
            'matched_to_existing_record': False,
            'PDS_first_forename': False,
            'PDS_surname': False,
            'NHAIS_first_forename': False,
            'NHAIS_surname': False,
            'data_record_type': 'NEW'
        })

    @patch('load_demographics.load_demographics.log')
    def test_name_information_logging_when_names_are_empty_and_matched(self, log_mock):
        existing_record = {'nhs_number': '123'}
        demographics_data = {'data_record_type': 'NEW', 'nhs_number': '123'}

        demographics_module.log_PDS_name_information(existing_record, demographics_data)

        log_mock.assert_called_with({
            'log_reference': LogReference.PART0023,
            'matched_to_existing_record': True,
            'PDS_first_forename': False,
            'PDS_surname': False,
            'NHAIS_first_forename': False,
            'NHAIS_surname': False,
            'data_record_type': 'NEW'
        })

    @patch('load_demographics.load_demographics.log')
    def test_name_information_logging_when_names_are_present_but_different(self, log_mock):
        existing_record = {'first_name': 'Jolyne', 'last_name': 'Knight', 'nhs_number': '123'}
        demographics_data = {'data_record_type': 'AMENDED',
                             'given_name': 'Samantha', 'family_name': 'Jaeger', 'nhs_number': '123'}

        demographics_module.log_PDS_name_information(existing_record, demographics_data)

        log_mock.assert_called_with({
            'log_reference': LogReference.PART0023,
            'matched_to_existing_record': True,
            'same_first_forename': False,
            'same_surname': False,
            'PDS_first_forename': True,
            'PDS_surname': True,
            'NHAIS_first_forename': True,
            'NHAIS_surname': True,
            'data_record_type': 'AMENDED'
        })

    @patch('load_demographics.load_demographics.log')
    def test_name_information_logging_when_names_are_present_and_the_same(self, log_mock):
        existing_record = {'first_name': 'Jolyne', 'last_name': 'Knight', 'nhs_number': '123'}
        demographics_data = {'data_record_type': 'AMENDED',
                             'given_name': 'Jolyne', 'family_name': 'Knight', 'nhs_number': '123'}

        demographics_module.log_PDS_name_information(existing_record, demographics_data)

        log_mock.assert_called_with({
            'log_reference': LogReference.PART0023,
            'matched_to_existing_record': True,
            'same_first_forename': True,
            'same_surname': True,
            'PDS_first_forename': True,
            'PDS_surname': True,
            'NHAIS_first_forename': True,
            'NHAIS_surname': True,
            'data_record_type': 'AMENDED'
        })

    @unpack
    @data(
        (
            {},
            {
                'data_record_type': 'NEW'
            },
            {
                'match_record': False,
                'record_type': 'NEW'
            }
        ),
        (
            {
                'last_name': 'Person',
                'date_of_birth': '2021-05-27',
                'gender': 2
            },
            {
                'data_record_type': 'NEW',
                'family_name': 'Person',
                'date_of_birth': '20210527',
                'gender': '2'
            },
            {
                'match_record': True,
                'record_type': 'NEW',
                'match_family': True,
                'match_dob': True,
                'match_gender': True
            }
        ),
        (
            {
                'last_name': 'Person-Person',
                'date_of_birth': '2021-05-26',
                'gender': 1
            },
            {
                'data_record_type': 'NEW',
                'family_name': 'Person',
                'date_of_birth': '20210527',
                'gender': '2'
            },
            {
                'match_record': True,
                'record_type': 'NEW',
                'match_family': False,
                'match_dob': False,
                'match_gender': False
            }
        ),
        (
            {
                'last_name': 'Person-Person'
            },
            {
                'data_record_type': 'NEW',
                'family_name': 'Person',
                'date_of_birth': '20210527',
                'gender': '2'
            },
            {
                'match_record': True,
                'record_type': 'NEW',
                'match_family': False,
                'match_dob': False,
                'match_gender': False
            }
        )
    )
    @patch('load_demographics.load_demographics.log')
    def test_log_PDS_significant_changes(self, existing_record, demographics_data, expected_log_entry, log_mock):
        demographics_module.log_PDS_significant_changes(existing_record, demographics_data)
        log_mock.assert_called_with({
            'log_reference': LogReference.PART0025,
            **expected_log_entry
        })

    @data(1, 2)
    @patch('load_demographics.load_demographics.LAMBDA_DLQ_URL', 'the_lambda_dlq_queue_url')
    @patch('load_demographics.load_demographics.update_log_metadata_from_record_event')
    @patch('load_demographics.load_demographics.convert_demographics_message_to_participant_schema_format')
    @patch('load_demographics.load_demographics.get_existing_record_else_default_to_empty_dictionary')
    @patch('load_demographics.load_demographics.log')
    @patch('load_demographics.load_demographics.send_new_message')
    @patch('load_demographics.load_demographics.get_message_body_from_event')
    @patch('load_demographics.load_demographics.audit')
    @patch('load_demographics.load_demographics._log_PDS_record_date_of_death_information')
    def test_processing_stopped_when_scn_not_greater_than_existing_scn(
            self, scn, _log_PDS_record_date_of_death_information_mock, audit_mock,
            get_message_body_from_event_mock, send_message_mock, log_mock,
            get_participant_mock, converter_mock, update_log_metadata_mock):
        event = {'Records': [{'messageId': 'the_message'}]}
        demographics_data = {
            'nhs_number': 'the_nhs_number', 'data_record_type': 'NEW',
            'some_data_key': 'some_data_value', 'gender': 2,
            'serial_change_number': scn}
        converter_mock.return_value = demographics_data
        participant = {
            'existing_key': 'existing_value', 'date_of_death': '2021-02-01',
            'gender': 2, 'serial_change_number': '2'}
        get_participant_mock.return_value = participant
        get_message_body_from_event_mock.return_value = 'message body'

        demographics_module.lambda_handler.__wrapped__(event, {})

        expected_log_calls = [call({'log_reference': LogReference.PART0027})]
        log_mock.assert_has_calls(expected_log_calls)
        send_message_mock.assert_called_with('the_lambda_dlq_queue_url', 'message body')
        audit_mock.assert_not_called()

        # Make sure lambda has returned by asserting the next function call is not made
        _log_PDS_record_date_of_death_information_mock.assert_not_called()

    @unpack
    @data(
        (
            {
                'address_line_1': 'nfa'
            },
            PdsValidationContext(
                None,
                'CSMS-8',
                [],
                [],
                False,
                {
                        'address_line_1': 'No Fixed Abode',
                        'postcode': 'ZZ99 3VZ'
                }
            ),
            {
                'address_line_1': 'No Fixed Abode',
                'postcode': 'ZZ99 3VZ'
            },
            False,
            False,
            []
        ),
        (
            {
                'nhs_number': '1234'
            },
            PdsValidationContext(
                None,
                None,
                ['nhs_number'],
                ['CSMS-1'],
                True,
                {}
            ),
            {},
            True,
            True,
            ['CSMS-1']
        )
    )
    @patch('load_demographics.load_demographics.log')
    @patch('load_demographics.load_demographics.send_new_message')
    @patch('load_demographics.load_demographics.create_or_replace_rejected_record')
    @patch('load_demographics.load_demographics.datetime')
    def test_handle_validation_result(
        self,
        demographics_data,
        validation_result,
        expected_demographics_data,
        expected_log_call,
        expected_hard_fail,
        expected_failed_rules,
        datetime_mock,
        create_rejected_mock,
        send_new_message_mock,
        log_mock
    ):

        dummy_event = {
            'Records': [
                {
                    'body': {},
                    'messageId': 'ABC123'
                }
            ]
        }

        datetime_mock.now = Mock(return_value=datetime(2020, 1, 1, 11, 22, 33, 444, tzinfo=timezone.utc))

        updated_demographics_data = demographics_module.handle_validation_result(
            demographics_data,
            validation_result,
            dummy_event
        )

        self.assertEqual(updated_demographics_data, expected_demographics_data)
        if expected_log_call:
            log_mock.assert_called_with({
                'log_reference': LogReference.PART0026,
                'hard_fail': expected_hard_fail,
                'failed_rules': expected_failed_rules
            })
            send_new_message_mock.assert_called_with(
                None,
                {
                    'metadata': {
                        'hard_fail': expected_hard_fail,
                        'failed_rules': expected_failed_rules
                    }
                }
            )
            create_rejected_mock.assert_called_with({
                'metadata': {
                    'hard_fail': expected_hard_fail,
                    'failed_rules': expected_failed_rules
                },
                'uuid': 'ABC123',
                'rejection_type': 'PDS#2020-01-01',
                'rejected_status': 'UNPROCESSED',
                'created': '2020-01-01T11:22:33.000444+00:00',
                'nhs_number': '1234'
            })
            send_new_message_mock.assert_called_with(
                None,
                {
                    'metadata': {
                        'hard_fail': expected_hard_fail,
                        'failed_rules': expected_failed_rules
                    }
                }
            )
        else:
            log_mock.assert_not_called()
            send_new_message_mock.assert_not_called()

    @unpack
    @data(
       ({'status_of_death_notification': '1'}, {'status_of_death_notification': '1'}, False),
       ({'status_of_death_notification': ''}, {'status_of_death_notification': '1'}, False),
       ({'participant_id': '123', 'status_of_death_notification': '1'}, {'status_of_death_notification': ''}, True)
    )
    @patch('load_demographics.load_demographics.audit')
    @patch('load_demographics.load_demographics.log')
    def test_audit_is_created_when_participant_is_no_longer_dead(
        self, existing_record, demographics_data, expect_audit, log_mock, audit_mock
    ):
        demographics_module.audit_if_pariticpant_is_no_longer_dead(existing_record, demographics_data)
        if expect_audit:
            log_mock.assert_called_with({'log_reference': LogReference.PART0029})
            audit_mock.assert_called_with(
                action=audit_utils.AuditActions.DECEASED_PARTICIPANT_MARKED_AS_ALIVE,
                user=audit_utils.AuditUsers.DEMOGRAPHICS,
                participant_ids=[existing_record.get('participant_id')]
            )
        else:
            log_mock.assert_not_called()
            audit_mock.assert_not_called()

    @unpack
    @data(
        (
            [
                {
                    'participant_id': '614ac061-d701-44a3-a300-0bc3608a14c1'
                }
            ],
            {},
            {},
            {}
        ),
        (
            [],
            {
                'participant_id': 'REPLACEME',
                'active': True,
                'status': 'Routine',
                'nhais_cipher': 'LDN',
                'next_test_due_date': '2023-02-01'
            },
            {
                'participant_id': 'REPLACEME',
                'active': False,
                'is_ceased': True,
                'status': 'Suspended',
                'reason_for_removal_code': 'CYM',
                'reason_for_removal_effective_from_date': '2020-01-01',
                'migrated_1R_data': 'stuff',
                'migrated_1C_data': 'stuff',
                'cohort_update': 'stuff',
                'serial_change_number': 1234,
                'address': 'Our House',
                'superseded_nhs_number': '1234567890',
                'nhs_number': '1111111111',
                'sanitised_nhs_number': '1111111111'
            },
            {
                'participant_id': '614ac061-d701-44a3-a300-0bc3608a14c1',
                'sort_key': 'PARTICIPANT',
                'active': True,
                'status': 'Routine',
                'address': 'Our House',
                'nhs_number': '9999999999',
                'sanitised_nhs_number': '9999999999',
                'nhais_cipher': 'LDN',
                'next_test_due_date': '2023-02-01'
            }
        )
    )
    @patch('load_demographics.load_demographics.query_participants_by_nhs_number')
    @patch('load_demographics.load_demographics.create_replace_record_in_participant_table')
    @patch('load_demographics.load_demographics.cease_participant')
    @patch('load_demographics.load_demographics.audit')
    @patch('load_demographics.load_demographics.generate_participant_id')
    def test_supersede_participant(
        self,
        existing_participants,
        existing_record,
        updated_record,
        expected_create_replace,
        mock_generate_participant_id,
        mock_audit,
        mock_cease_participant,
        mock_create_participant,
        mock_query_participants
    ):

        nhs_number = '1111111111'
        superseded_nhs_number = '9999999999'
        participant_id = '614ac061-d701-44a3-a300-0bc3608a14c1'
        additional_information = {}

        mock_query_participants.return_value = existing_participants
        mock_generate_participant_id.return_value = participant_id

        demographics_module.supersede_participant(
            nhs_number,
            superseded_nhs_number,
            additional_information,
            existing_record,
            updated_record
        )

        mock_query_participants.assert_called_with('9999999999')
        mock_audit.assert_called_with(
            action=AuditActions.SUPERSEDING,
            user=AuditUsers.SYSTEM,
            participant_ids=[participant_id],
            additional_information={'supersedes': '1111111111'}
        )
        mock_cease_participant.assert_called_with(
            updated_record,
            additional_information,
            CeaseReason.MERGED
        )

        if expected_create_replace:
            mock_create_participant.assert_called_with(expected_create_replace)
        else:
            mock_create_participant.assert_not_called()
