from ddt import ddt, data, unpack
from load_demographics.validate_pds import (
    _strategy_address_lines_1_2_4,
    _strategy_blank_name,
    _strategy_nhs_number,
    PdsValidationContext,
    _strategy_removal,
    _strategy_zz99_check,
    _strategy_address_paf_key,
    _strategy_no_fixed_abode,
    _strategy_postcode_null,
    _strategy_nhais_posting,
    pds_record_validation
)
from unittest import TestCase


@ddt
class TestValidatePds(TestCase):
    @unpack
    @data(
        ('8611195205',),
        ('6365319851',)
    )
    def test_strategy_nhs_number_valid(self, nhs_number):
        input_context = PdsValidationContext(None, 'CSMS-1', [], [], False, {})
        output_context = _strategy_nhs_number(input_context, {'nhs_number': nhs_number})
        self.assertIsNotNone(output_context)
        self.assertEqual(output_context.next_rule, 'CSMS-2')
        self.assertEqual(output_context.rejected_fields, [])
        self.assertEqual(output_context.failed_rules, [])
        self.assertEqual(output_context.hard_fail, False)

    @unpack
    @data(
        ('6365319852',),
        ('123456789',)
    )
    def test_strategy_nhs_number_invalid(self, nhs_number):
        input_context = PdsValidationContext(None, 'CSMS-1', [], [], False, {})
        output_context = _strategy_nhs_number(input_context, {'nhs_number': nhs_number})
        self.assertIsNotNone(output_context)
        self.assertEqual(output_context.next_rule, None)
        self.assertEqual(output_context.rejected_fields, ['nhs_number'])
        self.assertEqual(output_context.failed_rules, ['CSMS-1'])
        self.assertEqual(output_context.hard_fail, True)

    @unpack
    @data(
        ('REMOVED', None),
        ('NEW', 'CSMS-3'),
        ('AMENDED', 'CSMS-3')
    )
    def test_strategy_removal(self, data_record_type, expected_next_rule):
        input_context = PdsValidationContext(None, 'CSMS-2', [], [], False, {})
        output_context = _strategy_removal(input_context, {'data_record_type': data_record_type})
        self.assertIsNotNone(output_context)
        self.assertEqual(output_context.next_rule, expected_next_rule)
        self.assertEqual(output_context.rejected_fields, [])
        self.assertEqual(output_context.failed_rules, [])
        self.assertEqual(output_context.hard_fail, False)

    @unpack
    @data(
        ('iv30 1hu', 'CSMS-8', False, []),
        ('  ...z.Z.99.1@B', None, True, ['postcode'])
    )
    def test_strategy_zz99_check(
        self,
        postcode,
        expected_next_rule,
        expected_hard_fail,
        expected_rejected_fields
    ):
        input_context = PdsValidationContext(None, 'CSMS-5', [], [], False, {})
        output_context = _strategy_zz99_check(input_context, {'postcode': postcode})
        self.assertIsNotNone(output_context)
        self.assertEqual(output_context.next_rule, expected_next_rule)
        self.assertEqual(output_context.rejected_fields, expected_rejected_fields)
        self.assertEqual(output_context.hard_fail, expected_hard_fail)

    @unpack
    @data(
        ({}, 'CSMS-4'),
        ({'paf_key': 'some value'}, 'CSMS-8')
    )
    def test_address_paf_key(self, pds_record, expected_next_rule):
        input_context = PdsValidationContext(None, 'CSMS-3', [], [], False, {})
        output_context = _strategy_address_paf_key(input_context, pds_record)
        self.assertIsNotNone(output_context)
        self.assertEqual(output_context.next_rule, expected_next_rule)
        self.assertEqual(output_context.rejected_fields, [])
        self.assertEqual(output_context.failed_rules, [])
        self.assertEqual(output_context.hard_fail, False)

    @unpack
    @data(
        ({
            'address_line_1': 'line 1',
            'address_line_2': 'line 2',
            'address_line_4': 'line 4',
        },
            'CSMS-5', [], [], False
        ),
        ({
            'address_line_2': 'line 2',
            'address_line_4': 'line 4',
        },
            'CSMS-5', [], [], False
        ),
        ({
            'address_line_1': 'line 1',
            'address_line_4': 'line 4',
        },
            'CSMS-5', [], [], False
        ),
        ({
            'address_line_1': 'line 1',
            'address_line_2': 'line 2',
        },
            'CSMS-5', [], [], False
        ),
        ({
            'address_line_1': 'line 1',
        },
            'CSMS-5', [], [], False
        ),
        ({
            'address_line_2': 'line 2',
        },
            'CSMS-5', [], [], False
        ),
        ({
            'address_line_4': 'line 4',
        },
            'CSMS-5', [], [], False
        ),
        ({},
         None, [], ['CSMS-6'], True
         ),
    )
    def test_address_lines_1_2_4(self,
                                 pds_record,
                                 expected_next_rule,
                                 expected_rejected_fields,
                                 expected_failed_rules,
                                 expected_hard_fail):
        input_context = PdsValidationContext(None, 'CSMS-6', [], [], False, {})
        output_context = _strategy_address_lines_1_2_4(input_context, pds_record)

        self.assertIsNotNone(output_context)
        self.assertEqual(output_context.next_rule, expected_next_rule)
        self.assertEqual(output_context.rejected_fields, expected_rejected_fields)
        self.assertEqual(output_context.failed_rules, expected_failed_rules)
        self.assertEqual(output_context.hard_fail, expected_hard_fail)

    @unpack
    @data(
        ({'postcode': 'AB44 1XF'}, 'CSMS-7', {}),
        ({'address_line_1': 'ukn', 'postcode': 'AB44 1XF'}, 'CSMS-7', {}),
        ({'postcode': 'ZZ99 3VZ'}, 'CSMS-8', {'address_line_1': 'No Fixed Abode', 'postcode': 'ZZ99 3VZ'}),
        ({'address_line_1': 'nfa'}, 'CSMS-8', {'address_line_1': 'No Fixed Abode', 'postcode': 'ZZ99 3VZ'}),
        ({'address_line_2': 'no fixed abode'}, 'CSMS-8', {'address_line_1': 'No Fixed Abode', 'postcode': 'ZZ99 3VZ'}),
        ({'address_line_3': 'zz99 3vz'}, 'CSMS-8', {'address_line_1': 'No Fixed Abode', 'postcode': 'ZZ99 3VZ'}),
        ({'address_line_4': 'ukn'}, 'CSMS-8', {'address_line_1': 'No Fixed Abode', 'postcode': 'ZZ99 3VZ'}),
        ({'address_line_5': 'unknown'}, 'CSMS-8', {'address_line_1': 'No Fixed Abode', 'postcode': 'ZZ99 3VZ'})
    )
    def test_strategy_no_fixed_abode(self, demographics_data, expected_next_rule, expected_replace_fields):
        input_context = PdsValidationContext(None, 'CSMS-3', [], [], False, {})
        output_context = _strategy_no_fixed_abode(input_context, demographics_data)
        self.assertIsNotNone(output_context)
        self.assertEqual(output_context.next_rule, expected_next_rule)
        self.assertEqual(output_context.rejected_fields, [])
        self.assertEqual(output_context.failed_rules, [])
        self.assertEqual(output_context.hard_fail, False)
        self.assertEqual(output_context.replace_fields, expected_replace_fields)

    @unpack
    @data(
        ('AB44 1XF', 'CSMS-6', [], [], False),
        ('ZZ99 3VZ', 'CSMS-6', [], [], False),
        (None, None, ['postcode'], ['CSMS-7'], True),
        ('', None, ['postcode'], ['CSMS-7'], True)
    )
    def test_strategy_postcode_null(self, postcode, expected_next_rule, expected_rejected_fields, expected_failed_rules,
                                    expected_hard_fail):
        input_context = PdsValidationContext(None, 'CSMS-7', [], [], False, {})
        output_context = _strategy_postcode_null(input_context, {'postcode': postcode})

        self.assertEqual(output_context.next_rule, expected_next_rule)
        self.assertEqual(output_context.rejected_fields, expected_rejected_fields)
        self.assertEqual(output_context.failed_rules, expected_failed_rules)
        self.assertEqual(output_context.hard_fail, expected_hard_fail)
        self.assertEqual(output_context.replace_fields, {})

    @unpack
    @data(
        (
            {
                'given_name': 'Tessa',
                'family_name': 'Person'
            },
            [],
            []
        ),
        (
            {
                'given_name': 'Tessa',
                'family_name': '    '
            },
            ['given_name', 'family_name'],
            ['CSMS-8']
        ),
        (
            {
                'given_name': '     ',
                'family_name': 'Person'
            },
            ['given_name', 'family_name'],
            ['CSMS-8']
        ),
        (
            {
                'given_name': '',
                'family_name': ''
            },
            ['given_name', 'family_name'],
            ['CSMS-8']
        ),
        (
            {},
            ['given_name', 'family_name'],
            ['CSMS-8']
        )
    )
    def test_strategy_blank_name(self, pds_record, expected_rejected_fields, expected_failed_rules):
        input_context = PdsValidationContext(None, 'CSMS-8', [], [], False, {})
        output_context = _strategy_blank_name(input_context, pds_record)
        self.assertIsNotNone(output_context)
        self.assertEqual(output_context.next_rule, 'CSMS-9')
        self.assertEqual(output_context.rejected_fields, expected_rejected_fields)
        self.assertEqual(output_context.failed_rules, expected_failed_rules)
        self.assertEqual(output_context.hard_fail, False)

    @unpack
    @data(
        (
            {
                'nhais_posting': 'SUN'
            },
            [],
            [],
            {}
        ),
        (
            {
                'nhais_posting': 'AAAAA'
            },
            [],
            ['CSMS-9'],
            {'nhais_posting': 'INVALID'}
        )
    )
    def test_strategy_nhais_posting_name(self, pds_record,
                                         expected_rejected_fields,
                                         expected_failed_rules,
                                         expected_replace_fields):
        input_context = PdsValidationContext(None, 'CSMS-9', [], [], False, {})
        output_context = _strategy_nhais_posting(input_context, pds_record)
        self.assertIsNotNone(output_context)
        self.assertEqual(output_context.next_rule, None)
        self.assertEqual(output_context.rejected_fields, expected_rejected_fields)
        self.assertEqual(output_context.failed_rules, expected_failed_rules)
        self.assertEqual(output_context.hard_fail, False)
        self.assertEqual(output_context.replace_fields, expected_replace_fields)

    @unpack
    @data(
        (
            {
                'given_name': 'Tessa',
                'family_name': 'Person',
                'postcode': 'AB44 1XF',
                'nhs_number': '8611195205',
                'address_line_1': 'address line 1',
                'nhais_posting': 'SUN'
            },
            [],
            [],
            False,
            {}
        ),
        (
            {
                'nhs_number': '6365319852'
            },
            ['nhs_number'],
            ['CSMS-1'],
            True,
            {}
        ),
        (
            {
                'nhs_number': '8611195205',
                'data_record_type': 'REMOVED'
            },
            [],
            [],
            False,
            {}
        ),
        (
            {
                'nhs_number': '8611195205',
                'data_record_type': 'NEW',
                'postcode': 'ZZ991AB',
                'address_line_1': 'Somewhere'
            },
            ['postcode'],
            ['CSMS-5'],
            True,
            {}
        ),
        (
            {
                'given_name': 'Tessa',
                'family_name': 'Patterson',
                'nhs_number': '8611195205',
                'data_record_type': 'NEW',
                'postcode': 'ls124sn',
                'address_line_1': 'Somewhere'

            },
            [],
            ['CSMS-9'],
            False,
            {'nhais_posting': 'INVALID'}
        )
    )
    def test_pds_record_validation(
        self,
        pds_record,
        expected_rejected_fields,
        expected_failed_rules,
        expected_hard_fail,
        expected_replace_fields
    ):
        context = pds_record_validation(pds_record)
        self.assertIsNotNone(context)
        self.assertEqual(context.rejected_fields, expected_rejected_fields)
        self.assertEqual(context.failed_rules, expected_failed_rules)
        self.assertEqual(context.hard_fail, expected_hard_fail)
        self.assertEqual(context.replace_fields, expected_replace_fields)
