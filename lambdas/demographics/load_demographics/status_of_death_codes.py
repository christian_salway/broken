from enum import Enum


class StatusOfDeath(Enum):

    INFORMAL = '1'
    FORMAL = '2'
