import json
from common.log import log
from load_demographics.log_references import LogReference


DATA_RECORD_TYPES = ['NEW', 'REMOVED', 'AMENDED']
EXPECTED_NUMBER_OF_FIELDS = 42
FIELD_DELIMITER = '|'


def convert_demographics_message_to_participant_schema_format(message):
    message_body = json.loads(message.get('body'))

    log({'log_reference': LogReference.PART0002, 'file_name': message_body['file_name'],
         'line_index': message_body['line_index']})

    raw_data = message_body['line']
    fields = raw_data.split(FIELD_DELIMITER)
    number_of_fields = len(fields)

    if number_of_fields != EXPECTED_NUMBER_OF_FIELDS:
        raise Exception(f'Unexpected number of fields in raw data. '
                        f'Expected {EXPECTED_NUMBER_OF_FIELDS} fields. '
                        f'Received {number_of_fields} fields.')

    data_record_type = fields[0]
    if data_record_type not in DATA_RECORD_TYPES:
        raise Exception(f'Data record type is not one of the allowed types: {DATA_RECORD_TYPES}')

    converted_record = {}

    converted_record['raw_data'] = raw_data
    converted_record['data_record_type'] = data_record_type

    set_if_not_empty(converted_record, 'pds_change_timestamp', fields[1])
    set_if_not_empty(converted_record, 'serial_change_number', int(fields[2]))
    set_if_not_empty(converted_record, 'nhs_number', fields[3])
    set_if_not_empty(converted_record, 'superseded_nhs_number', fields[4])
    set_if_not_empty(converted_record, 'gp_provider_code', fields[5])
    set_if_not_empty(converted_record, 'gp_provider_start_date', fields[6])
    set_if_not_empty(converted_record, 'nhais_posting', fields[7])
    set_if_not_empty(converted_record, 'nhais_posting_start_date', fields[8])
    set_if_not_empty(converted_record, 'previous_nhais_posting', fields[9])
    set_if_not_empty(converted_record, 'previous_nhais_posting_start_date', fields[10])
    set_if_not_empty(converted_record, 'registration_type', fields[11])
    set_if_not_empty(converted_record, 'title', fields[12])
    set_if_not_empty(converted_record, 'given_name', fields[13])
    set_if_not_empty(converted_record, 'other_given_names', fields[14].replace(',', ' '))
    set_if_not_empty(converted_record, 'family_name', fields[15])
    set_if_not_empty(converted_record, 'previous_family_name', fields[16])
    set_if_not_empty(converted_record, 'date_of_birth', fields[17])
    set_if_not_empty(converted_record, 'gender', fields[18])
    set_if_not_empty(converted_record, 'address_line_1', fields[19])
    set_if_not_empty(converted_record, 'address_line_2', fields[20])
    set_if_not_empty(converted_record, 'address_line_3', fields[21])
    set_if_not_empty(converted_record, 'address_line_4', fields[22])
    set_if_not_empty(converted_record, 'address_line_5', fields[23])
    set_if_not_empty(converted_record, 'postcode', fields[24])
    set_if_not_empty(converted_record, 'paf_key', fields[25])
    set_if_not_empty(converted_record, 'address_start_date', fields[26])
    set_if_not_empty(converted_record, 'reason_for_removal_code', fields[27])
    set_if_not_empty(converted_record, 'reason_for_removal_start_date', fields[28])
    set_if_not_empty(converted_record, 'date_of_death', fields[29])
    set_if_not_empty(converted_record, 'status_of_death_notification', fields[30])
    set_if_not_empty(converted_record, 'telephone', fields[31])
    set_if_not_empty(converted_record, 'telephone_start_date', fields[32])
    set_if_not_empty(converted_record, 'mobile', fields[33])
    set_if_not_empty(converted_record, 'mobile_start_date', fields[34])
    set_if_not_empty(converted_record, 'email', fields[35])
    set_if_not_empty(converted_record, 'email_start_date', fields[36])
    set_if_not_empty(converted_record, 'language', fields[37])
    set_if_not_empty(converted_record, 'interpreter', fields[38])
    set_if_not_empty(converted_record, 'is_invalid', fields[39])
    set_if_not_empty(converted_record, 'is_fp69', fields[40])
    set_if_not_empty(converted_record, 'local_authority_area_code', fields[41])

    return converted_record


def set_if_not_empty(record, field_name, value):
    if value:
        record[field_name] = value
