from datetime import datetime, date, timedelta, timezone
from common.models.participant import ParticipantStatus
from common.utils.data_segregation.nhais_ciphers import (
    DMS_COHORT_CODES,
    NO_COHORT_CODES,
    WELSH_COHORT_CODES,
    DEFAULT_COUNTRY_CODE
)

from common.utils.next_test_due_date_calculators.next_test_due_date_calculator import \
    calculate_next_test_due_date_for_new_participant
from common.utils.date_utils import (
    pad_date_string, is_valid_date_format
)
from common.utils.organisation_directory_utils import organisation_is_dms_valid
from common.log import log
from load_demographics.log_references import LogReference
from common.gender_codes import GenderCode
from common.utils.participant_utils import (
    generate_participant_id,
    set_search_indexes
)
from common.utils.nhs_number_utils import sanitise_nhs_number
from distutils.util import strtobool
from common.utils import _remove_empty_items_from_dictionary
from load_demographics.status_of_death_codes import StatusOfDeath

TWENTY_TWO_WEEKS_IN_DAYS = 154
REMOVED_DATA_RECORD_TYPE = 'REMOVED'
NEW_DATA_RECORD_TYPE = 'NEW'
AMENDED_DATA_RECORD_TYPE = 'AMENDED'
DECEASED_CODE = 'DEA'
IS_TRUE_FLAG = 'Y'
DEACTIVATE_REMOVAL_REASONS = ['DEA', 'CGA', 'MB', 'LDN', 'NIT', 'ORR', 'RFI', 'SCT', 'TRA']


def update_existing_record_with_demographics_data(nhs_number, existing_record, demographics_data):

    existing_record['nhs_number'] = nhs_number
    existing_record['superseded_nhs_number'] = demographics_data.get('superseded_nhs_number', '')

    existing_record['gender'] = demographics_data.get('gender', GenderCode.NOT_KNOWN.value)

    existing_record['date_of_birth'] = convert_date_to_iso_date_format(demographics_data.get('date_of_birth', ''))
    existing_record['date_of_death'] = convert_date_to_iso_date_format(demographics_data.get('date_of_death', ''))
    existing_record['status_of_death_notification'] = demographics_data.get('status_of_death_notification', '')
    existing_record['serial_change_number'] = demographics_data.get('serial_change_number', '')

    _set_active_status(existing_record, demographics_data)

    _set_next_test_due_date_if_not_present_and_is_active(existing_record)

    _set_is_invalid_flag(existing_record, demographics_data)

    _set_is_ceased(existing_record, demographics_data)

    _set_name(existing_record, demographics_data)

    _set_address(existing_record, demographics_data)

    _set_reason_for_removal(existing_record, demographics_data)

    _set_gp_provider_information(existing_record, demographics_data)

    _set_nhais_posting_information(existing_record, demographics_data)

    _set_sanitised_nhs_number(existing_record)

    sanitised_record = _remove_empty_items_from_dictionary(existing_record)

    _set_participant_id_and_status_for_new_records(sanitised_record)

    _set_cohort_raw_data(sanitised_record, demographics_data)

    _set_is_fp69_flag(sanitised_record, demographics_data)

    set_search_indexes(sanitised_record)

    return sanitised_record


def _set_address(existing_record, demographics_data):
    new_address_start_date = demographics_data.get('address_start_date', '')
    existing_record['address_effective_from_date'] = convert_date_to_iso_date_format(new_address_start_date)

    updated_address = {}
    updated_address['address_line_1'] = demographics_data.get('address_line_1', '')
    updated_address['address_line_2'] = demographics_data.get('address_line_2', '')
    updated_address['address_line_3'] = demographics_data.get('address_line_3', '')
    updated_address['address_line_4'] = demographics_data.get('address_line_4', '')
    updated_address['address_line_5'] = demographics_data.get('address_line_5', '')
    updated_address['postcode'] = demographics_data.get('postcode', '')

    existing_record['address'] = _remove_empty_items_from_dictionary(updated_address)


def _set_name(existing_record, demographics_data):
    existing_record['title'] = demographics_data.get('title', '')
    existing_record['first_name'] = demographics_data.get('given_name', '')
    existing_record['middle_names'] = demographics_data.get('other_given_names', '')
    existing_record['last_name'] = demographics_data.get('family_name', '')


def _set_next_test_due_date_if_not_present_and_is_active(existing_record):
    existing_next_test_due_date = existing_record.get('next_test_due_date', '')

    if not existing_record.get('active', True):
        existing_record['next_test_due_date'] = ''
        return

    if existing_next_test_due_date:
        return

    if existing_record.get('is_ceased', False):
        return

    else:
        date_of_birth = datetime.strptime(existing_record['date_of_birth'], '%Y-%m-%d').date()
        existing_record['next_test_due_date'] = \
            calculate_next_test_due_date_for_new_participant(date_of_birth).isoformat()


def _set_is_invalid_flag(existing_record, demographics_data):
    is_invalid_flag = demographics_data.get('is_invalid', '')

    if is_invalid_flag == IS_TRUE_FLAG:
        existing_record['is_invalid_patient'] = True
    else:
        existing_record['is_invalid_patient'] = False


def _set_is_fp69_flag(existing_record, demographics_data):
    value = demographics_data.get('is_fp69')
    try:
        existing_record['is_fp69'] = bool(strtobool(str(value)))
    except Exception:
        log({
            'log_reference': LogReference.PART0012,
            'field_name': 'is_fp69',
            'field_index': 39,
            'value': value,
            'expected_value': 'true | false'})
        existing_record['is_fp69'] = False


def _set_is_ceased(existing_record, demographics_data):
    superseded_nhs_number = demographics_data.get('superseded_nhs_number')
    if superseded_nhs_number:
        existing_record['is_ceased'] = True

    is_ceased = existing_record.get('is_ceased', False)

    existing_record['is_ceased'] = is_ceased
    if is_ceased:
        existing_record['status'] = ParticipantStatus.CEASED


def _set_active_status(existing_record, demographics_data):
    record_data_type = demographics_data['data_record_type']
    reason_for_removal = demographics_data.get('reason_for_removal_code', '')

    if is_removed(record_data_type) or (reason_for_removal in DEACTIVATE_REMOVAL_REASONS):
        existing_record['active'] = False
        return

    if is_new(record_data_type):
        existing_record['active'] = True
        return

    status_of_death = demographics_data.get('status_of_death_notification')

    if status_of_death == StatusOfDeath.INFORMAL.value and demographics_data.get('date_of_death') \
            and existing_record['registered_gp_practice_code'] and not demographics_data.get('gp_provider_code'):
        existing_record['active'] = False
        return

    existing_record['active'] = _get_is_active_and_set_ntdd_from_nhais_posting(existing_record, demographics_data)


def _set_reason_for_removal(existing_record, demographics_data):
    if not demographics_data.get('reason_for_removal_code', '') and \
         demographics_data.get('nhais_posting', '') in WELSH_COHORT_CODES:
        existing_record['reason_for_removal_code'] = 'CYM'
        existing_record['reason_for_removal_effective_from_date'] =\
            demographics_data.get('nhais_posting_start_date', '')
    else:
        existing_record['reason_for_removal_code'] = demographics_data.get('reason_for_removal_code', '')
        existing_record['reason_for_removal_effective_from_date'] = convert_date_to_iso_date_format(
            demographics_data.get('reason_for_removal_start_date', ''))


def _set_gp_provider_information(existing_record, demographics_data):
    existing_record['registered_gp_practice_code'] = demographics_data.get('gp_provider_code', '')


def _set_nhais_posting_information(existing_record, demographics_data):
    if demographics_data.get('nhais_posting', '') == 'INVALID':
        if existing_record.get('nhais_cipher', '') == '':
            log({'log_reference': LogReference.PART0028})
            existing_record['nhais_cipher'] = DEFAULT_COUNTRY_CODE
        return

    existing_record['nhais_cipher'] = demographics_data.get('nhais_posting', '')
    existing_record['record_creation_date'] = convert_date_to_iso_date_format(
        demographics_data.get('nhais_posting_start_date', ''))


def _set_participant_id_and_status_for_new_records(existing_record):
    participant_id = existing_record.get('participant_id')

    if not participant_id:
        existing_record['participant_id'] = generate_participant_id()
        existing_record['sort_key'] = 'PARTICIPANT'
        existing_record['status'] = ParticipantStatus.CALLED


def _set_sanitised_nhs_number(existing_record):
    existing_record['sanitised_nhs_number'] = sanitise_nhs_number(existing_record['nhs_number'])


def _set_cohort_raw_data(existing_record, demographics_data):
    cohort_raw_data = {}
    cohort_raw_data['value'] = demographics_data['raw_data']
    cohort_raw_data['received'] = datetime.now(timezone.utc).isoformat()
    existing_record['cohort_raw_data'] = cohort_raw_data


def convert_date_to_iso_date_format(date):
    if is_valid_date_format(date):
        return datetime.strptime(pad_date_string(date), '%Y%m%d').date().isoformat()
    return date


def convert_timestamp_to_iso_datetime_format(timestamp):
    return datetime.strptime(timestamp, '%Y%m%d%H%M%S').isoformat() if timestamp else timestamp


def is_removed(record_data_type):
    return record_data_type == REMOVED_DATA_RECORD_TYPE


def is_new(record_data_type):
    return record_data_type == NEW_DATA_RECORD_TYPE


def is_deceased(record, record_data_type):
    date_of_death = record.get('date_of_death')
    reason_for_removal_code = record.get('reason_for_removal_code')
    return record_data_type == AMENDED_DATA_RECORD_TYPE and reason_for_removal_code == DECEASED_CODE and date_of_death


def create_previous_address_record_from_existing_record(existing_record):
    current_date = date.today()
    previous_address_record = {
        'participant_id': existing_record['participant_id'],
        'sort_key': "PREVADDR#" + current_date.isoformat(),
        'date_to': current_date.isoformat(),
        'date_from': existing_record.get('address_effective_from_date', ''),
        'address':  existing_record.get('address', {})
    }

    return _remove_empty_items_from_dictionary(previous_address_record)


def _get_is_active_and_set_ntdd_from_nhais_posting(existing_record, demographics_data):

    nhais_posting = demographics_data.get('nhais_posting', '')
    previous_nhais_posting = demographics_data.get('previous_nhais_posting', '')

    if nhais_posting == 'INVALID':
        return existing_record.get('is_active', True)

    if (nhais_posting in WELSH_COHORT_CODES and
            demographics_data.get('previous_nhais_posting', '') not in NO_COHORT_CODES):
        return False

    if nhais_posting in DMS_COHORT_CODES:
        return organisation_is_dms_valid(demographics_data.get('gp_provider_code'))

    if (previous_nhais_posting in NO_COHORT_CODES.union(WELSH_COHORT_CODES) and
            nhais_posting not in NO_COHORT_CODES.union(WELSH_COHORT_CODES)):
        # NTDD defaults to be 13 weeks when transferred and participant not ceased
        if not existing_record.get('is_ceased', False):
            now = datetime.now(timezone.utc)
            due_date = now + timedelta(weeks=13)
            existing_record['next_test_due_date'] = (
               due_date.date().isoformat())
        return True

    return True
