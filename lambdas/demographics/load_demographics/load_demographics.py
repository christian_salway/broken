import os
from datetime import date, datetime, timezone
from typing import Dict, List
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from common.log import log, get_internal_id
from common.utils.nhs_number_utils import sanitise_nhs_number
from load_demographics.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils import validate_message_only_contains_one_record
from common.utils.transform_and_store_utils import (
    get_message_body_from_event,
    get_message_uuid_from_event,
    update_log_metadata_from_record_event
)
from load_demographics.participant_record_updater import (
    convert_date_to_iso_date_format,
    update_existing_record_with_demographics_data,
    create_previous_address_record_from_existing_record,
    AMENDED_DATA_RECORD_TYPE,
    REMOVED_DATA_RECORD_TYPE,
)
from load_demographics.demographics_record_converter import convert_demographics_message_to_participant_schema_format
from common.utils.participant_utils import (
    create_previous_name_record,
    create_replace_record_in_participant_table,
    generate_participant_id,
    query_participants_by_nhs_number,
    query_participants_by_nhs_number_all_fields
)
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.utils.sqs_utils import send_new_message
from common.models.participant import CeaseReason, EpisodeStatus
from common.models.rejected import RejectedStatus
from common.utils.cease_utils import (
    participant_eligible_for_auto_cease, cease_participant_and_send_notification_messages
)
from common.utils.participant_episode_utils import (
    update_participant_episode_record, query_for_live_episode
)

from load_demographics.validate_pds import PdsValidationContext, pds_record_validation
from common.utils.rejected_utils import create_or_replace_rejected_record

audit_actions = {
    'NEW': AuditActions.COHORT_ADD,
    'AMENDED': AuditActions.COHORT_AMEND,
    'REMOVED': AuditActions.COHORT_REMOVE,
    'FP69_SET': AuditActions.FP69_SET,
    'FP69_UNSET': AuditActions.FP69_UNSET,
    'ACTIVATE_PARTICIPANT': AuditActions.ACTIVATE_PARTICIPANT,
    'DEACTIVATE_PARTICIPANT': AuditActions.DEACTIVATE_PARTICIPANT,
}
SUPERSEDE_FIELDS_EXISTING = [
    'active',
    'is_ceased',
    'status',
    'reason_for_removal_code',
    'reason_for_removal_effective_from_date',
    'nhais_cipher',
    'next_test_due_date'
]

SUPERSEDE_FIELDS_DROP = [
    'participant_id',
    'migrated_1R_data',
    'migrated_1C_data',
    'cohort_update',
    'serial_change_number',
    'superseded_nhs_number',
    'nhs_number',
    'sanitised_nhs_number'
]

SUPSERSED_FIELDS_NOT_UPDATED = [*SUPERSEDE_FIELDS_EXISTING, *SUPERSEDE_FIELDS_DROP]

GP_NOTIFICATION_QUEUE_URL = os.environ.get('GP_NOTIFICATION_QUEUE_URL')
RESEND_NOTIFICATION_QUEUE_URL = os.environ.get('RESEND_NOTIFICATION_QUEUE_URL')
LAMBDA_DLQ_URL = os.environ.get('LAMBDA_DLQ_URL')


@lambda_entry_point
def lambda_handler(event, context):
    update_log_metadata_from_record_event(event)

    message_records = event.get('Records', [{}])
    validate_message_only_contains_one_record(message_records)
    message_record = message_records[0]
    message_id = str(message_record.get('messageId', 'NotProvided'))

    log({'log_reference': LogReference.PART0001, 'message_id': message_id})

    demographics_data = convert_demographics_message_to_participant_schema_format(message_record)

    nhs_number = demographics_data['nhs_number']
    existing_record = get_existing_record_else_default_to_empty_dictionary(nhs_number)

    new_serial_change_number = demographics_data.get('serial_change_number', '')
    old_serial_change_number = existing_record.get('serial_change_number', '')

    if new_serial_change_number and old_serial_change_number \
            and int(new_serial_change_number) <= int(old_serial_change_number):
        log({'log_reference': LogReference.PART0027})
        send_new_message(LAMBDA_DLQ_URL, get_message_body_from_event(event))
        return

    validation_result = pds_record_validation(demographics_data)

    demographics_data = handle_validation_result(demographics_data, validation_result, event)
    if (validation_result.hard_fail):
        return

    _log_PDS_record_date_of_death_information(existing_record, demographics_data)

    if demographics_data.get('date_of_birth'):
        age_group = assign_to_age_group(demographics_data.get('date_of_birth'))
    else:
        age_group = 'unknown age'

    if existing_record:
        log({'log_reference': LogReference.PART0017, 'age_group': age_group})
    else:
        log({'log_reference': LogReference.PART0018, 'age_group': age_group})

    has_address_changed = create_previous_details_records(
        existing_record, demographics_data) if existing_record else False

    updated_record = create_or_update_participant_record_with_demographic_information(
        nhs_number, dict(existing_record), demographics_data)

    _log_PDS_record_address_information(existing_record, demographics_data)
    log_PDS_record_date_of_birth_information(existing_record, demographics_data)
    log_pds_invalid(existing_record, demographics_data)
    log_PDS_name_information(existing_record, demographics_data)
    log_PDS_significant_changes(existing_record, demographics_data)

    existing_gender = existing_record.get('gender', '')
    gender = demographics_data['gender']
    data_record_type = demographics_data['data_record_type']
    log({
        'log_reference': LogReference.PART0014,
        'gender': gender,
        'existing_gender': existing_gender,
        'data_record_type': data_record_type
    })

    record_type = demographics_data['data_record_type']
    participant_id = updated_record['participant_id']

    active_flag_changed = check_and_audit_active_change(updated_record, existing_record)

    is_fp69_unset = check_and_audit_fp69_change(updated_record, existing_record)
    if (is_fp69_unset or has_address_changed) and record_type != REMOVED_DATA_RECORD_TYPE:
        resend_notification(participant_id, is_fp69_unset, has_address_changed)

    can_autocease = eligible_for_autocease(existing_record, updated_record)
    additional_information = {}
    if can_autocease:
        additional_information.update({'updating_next_test_due_date_from': existing_record['next_test_due_date']}) \
            if existing_record.get('next_test_due_date') else None
        cease_participant(updated_record, additional_information, CeaseReason.AUTOCEASE_DUE_TO_AGE)

    superseded_nhs_number = demographics_data.get('superseded_nhs_number')
    if superseded_nhs_number:
        supersede_participant(
            nhs_number,
            superseded_nhs_number,
            additional_information,
            existing_record,
            updated_record
        )

    if is_gp_notification_required(existing_record, updated_record):
        send_gp_notification(updated_record)

    next_test_due_date_changes_already_audited = active_flag_changed or can_autocease
    audit_for_record_type(record_type, existing_record, updated_record, next_test_due_date_changes_already_audited)
    audit_if_pariticpant_is_no_longer_dead(existing_record, demographics_data)

    log({'log_reference': LogReference.PART0008, 'message_id': message_id})


def get_existing_record_else_default_to_empty_dictionary(nhs_number):
    participants = query_participants_by_nhs_number_all_fields(nhs_number)

    if len(participants) == 0:
        log({'log_reference': LogReference.PART0003})
        return {}
    return participants[0]


def check_and_audit_fp69_change(updated_record, existing_record):
    """
    Returns if the fp69 status has been unset by a demographic import
    """
    new = updated_record.get('is_fp69')
    old = existing_record.get('is_fp69')
    if new == old:
        return False

    if old is None and new is False:
        return False

    participant_id = updated_record['participant_id']

    is_fp69_unset = not updated_record['is_fp69']
    audit_action = audit_actions['FP69_SET'] if updated_record['is_fp69'] else audit_actions['FP69_UNSET']

    audit(action=audit_action, user=AuditUsers.DEMOGRAPHICS, participant_ids=[participant_id])
    return is_fp69_unset


def check_and_audit_active_change(updated_record, existing_record):
    """
    Checks if the Audit status has been changed by a demographic import
    Returns True/False to indicate if audit was required
    """
    new = updated_record.get('active')
    old = existing_record.get('active')
    if new == old:
        return False

    if old is None and new is True:
        return False

    audit_action = audit_actions['ACTIVATE_PARTICIPANT'] if new else audit_actions['DEACTIVATE_PARTICIPANT']
    participant_id = updated_record['participant_id']

    if new:
        additional_information = {
            'reason': 'Transferred in'
        }
        if 'next_test_due_date' in updated_record:
            additional_information['updating_next_test_due_date_to'] = updated_record['next_test_due_date']
    else:
        close_existing_episode(participant_id, AuditUsers.SYSTEM.value)
        additional_information = {
            'reason': updated_record.get('reason_for_removal_code', 'ORR')
        }
        if 'next_test_due_date' in existing_record:
            additional_information['updating_next_test_due_date_from'] = existing_record['next_test_due_date']

    audit(action=audit_action,
          user=AuditUsers.DEMOGRAPHICS,
          participant_ids=[participant_id],
          additional_information=additional_information)

    return True


def create_previous_details_records(existing_record, demographics_data):
    has_address_changed = address_has_changed(existing_record['address'], demographics_data)
    if has_address_changed:
        log({'log_reference': LogReference.PART0004})
        create_previous_address_record(existing_record)
    if name_has_changed(existing_record, demographics_data):
        log({'log_reference': LogReference.PART0005})
        create_previous_name_record(existing_record)
    return has_address_changed


def address_has_changed(existing_address, demographics_data):
    if existing_address.get('postcode') != demographics_data.get('postcode'):
        return True

    for i in range(1, 6):
        address_line = f'address_line_{i}'
        if existing_address.get(address_line, '') != demographics_data.get(address_line, ''):
            return True

    return False


def name_has_changed(existing_record, demographics_data):
    if existing_record.get('title') != demographics_data.get('title'):
        return True
    if existing_record.get('first_name') != demographics_data.get('given_name'):
        return True
    if existing_record.get('last_name') != demographics_data.get('family_name'):
        return True
    if existing_record.get('middle_names') != demographics_data.get('other_given_names'):
        return True

    return False


def create_previous_address_record(existing_record):
    previous_address_record = create_previous_address_record_from_existing_record(existing_record)
    create_replace_record_in_participant_table(previous_address_record)


def create_or_update_participant_record_with_demographic_information(nhs_number, existing_record, demographics_data):
    log({'log_reference': LogReference.PART0006})
    conditional_string = ''
    conditional_string_values = {}
    if 'serial_change_number' in existing_record.keys():
        serial_change_number = demographics_data.get('serial_change_number', '')
        conditional_string = ' (serial_change_number < :scn) '
        conditional_string_values = {":scn": serial_change_number}
        log({'log_reference': LogReference.PART0010})

    updated_record = update_existing_record_with_demographics_data(nhs_number, existing_record, demographics_data)

    create_replace_record_in_participant_table(updated_record, conditional_string, conditional_string_values)

    return updated_record


def previous_and_current_address_for_audit_log(existing_record: Dict, updated_record: Dict) -> Dict:
    changed_address = {}

    address_fields = [
        'address_line_1', 'address_line_2', 'address_line_3', 'address_line_4', 'address_line_5', 'postcode'
    ]
    old_address = existing_record.get('address', {})
    new_address = updated_record.get('address', {})

    # If any address field has changed we need to store the whole address
    if address_has_changed(old_address, new_address):
        for address_key in address_fields:
            changed_address[address_key] = {
                'previous': old_address.get(address_key),
                'current': new_address.get(address_key)
            }

    return changed_address


def _get_age_bracket_from_lifespan_for_logging(lifespan):
    if (lifespan >= 65.0):
        return 'over 65'
    if (lifespan >= 24.5):
        return 'between 24.5 and 65'
    return 'under 24.5'


def _convert_to_date(raw_input):
    try:
        return datetime.strptime(raw_input, '%Y-%m-%d')
    except ValueError:
        return None


def _log_PDS_record_date_of_death_information(existing_record, demographics_data):
    if existing_record:
        existing_date_of_birth = _convert_to_date(existing_record.get('date_of_birth', ''))
        existing_date_of_death = _convert_to_date(existing_record.get('date_of_death', ''))
        existing_category = bool(existing_date_of_death)
        new_date_of_birth = _convert_to_date(
            convert_date_to_iso_date_format(
                demographics_data.get('date_of_birth', '')))
        new_date_of_death = _convert_to_date(
            convert_date_to_iso_date_format(
                demographics_data.get('date_of_death', '')))
        new_category = bool(new_date_of_death)

        lifespan_begin = new_date_of_birth or existing_date_of_birth
        lifespan_end = new_date_of_death or existing_date_of_death or datetime.today()
        if lifespan_begin and lifespan_end:
            difference = relativedelta(lifespan_end, lifespan_begin)
            lifespan = difference.years + (difference.months / 12)
            age_bracket = _get_age_bracket_from_lifespan_for_logging(lifespan)
        else:
            age_bracket = 'of unknown age'

        log({
            'log_reference': LogReference.PART0024,
            'existing_category': existing_category,
            'new_category': new_category,
            'participant_age_bracket': age_bracket
        })


def get_changed_fields_for_amended_records(existing_record: Dict, updated_record: Dict) -> List:
    changed_fields = []

    fields_to_track = ['address_effective_from_date', 'date_of_birth', 'date_of_death', 'first_name', 'gender',
                       'is_ceased', 'is_invalid_patient', 'last_name', 'middle_names', 'nhais_cipher', 'status',
                       'reason_for_removal_code', 'reason_for_removal_effective_from_date',
                       'registered_gp_practice_code', 'superseded_nhs_number', 'title']

    other_changed_fields = [field_name for field_name in fields_to_track
                            if updated_record.get(field_name) != existing_record.get(field_name)]

    changed_fields.extend(other_changed_fields)

    return changed_fields


def resend_notification(participant_id, is_fp69_unset, has_address_changed):
    log({'log_reference': LogReference.PART0013, 'is_fp69_unset': is_fp69_unset,
         'has_address_changed': has_address_changed})
    notification_message = {
        'participant_id': participant_id,
        'internal_id': get_internal_id()
    }
    send_new_message(RESEND_NOTIFICATION_QUEUE_URL, notification_message)


def send_gp_notification(updated_record):
    notification_message = {
        'participant_id': updated_record['participant_id'],
        'registered_gp_practice_code': updated_record['registered_gp_practice_code'],
        'notification_origin': 'GP_REGISTRATION',
        'internal_id': get_internal_id()
    }
    log({'log_reference': LogReference.PART0011})
    send_new_message(GP_NOTIFICATION_QUEUE_URL, notification_message)


def eligible_for_autocease(existing_record, updated_record):
    if not updated_record.get('next_test_due_date', ''):
        return False

    return not existing_record and participant_eligible_for_auto_cease(
        updated_record, date.fromisoformat(updated_record['next_test_due_date'])
    )


def audit_for_record_type(record_type, existing_record, updated_record, next_test_due_date_changes_already_audited):
    additional_information = audit_additional_information(existing_record,
                                                          updated_record,
                                                          next_test_due_date_changes_already_audited) \
        if record_type == AMENDED_DATA_RECORD_TYPE else None
    audit(action=audit_actions[record_type],
          user=AuditUsers.DEMOGRAPHICS,
          participant_ids=[updated_record['participant_id']],
          additional_information=additional_information)


def audit_additional_information(existing_record, updated_record, next_test_due_date_changes_already_audited):
    additional_information = {}

    changed_fields = get_changed_fields_for_amended_records(existing_record, updated_record)
    changed_address = previous_and_current_address_for_audit_log(existing_record, updated_record)
    changed_names = previous_vs_current(
        existing_record, updated_record,
        ('title', 'first_name', 'last_name', 'middle_names'))
    changed_gp_practice_code = previous_vs_current(existing_record, updated_record, ('registered_gp_practice_code',))
    changed_date_of_birth = previous_vs_current(existing_record, updated_record, ('date_of_birth',))

    if changed_fields:
        additional_information.update({'changed_fields': changed_fields})
    if changed_address:
        additional_information.update({'changed_address': changed_address})
    if changed_names:
        additional_information.update({'changed_names': changed_names})
    if changed_gp_practice_code:
        additional_information.update({
            'changed_gp_practice_code':
            changed_gp_practice_code['registered_gp_practice_code']
        })
    if changed_date_of_birth:
        additional_information.update({'changed_date_of_birth': changed_date_of_birth['date_of_birth']})

    if not next_test_due_date_changes_already_audited:
        old_next_test_due_date = existing_record.get('next_test_due_date')
        new_next_test_due_date = updated_record.get('next_test_due_date')
        if old_next_test_due_date != new_next_test_due_date:
            additional_information.update({'updating_next_test_due_date_from': old_next_test_due_date}) \
                if old_next_test_due_date else None
            additional_information.update({'updating_next_test_due_date_to': new_next_test_due_date}) \
                if new_next_test_due_date else None

    return additional_information if additional_information else None


def previous_vs_current(existing_record, updated_record, fields):
    changed_fields = {}
    for item in fields:
        if existing_record.get(item) != updated_record.get(item):
            changed_fields[item] = {
             'previous': existing_record.get(item),
             'current': updated_record.get(item)
            }
    return changed_fields


def audit_if_pariticpant_is_no_longer_dead(existing_record, demographics_data):
    status_of_death = demographics_data.get('status_of_death_notification')
    existing_status_of_death = existing_record.get('status_of_death_notification')

    if not status_of_death and existing_status_of_death:
        log({'log_reference': LogReference.PART0029})
        audit(
            action=AuditActions.DECEASED_PARTICIPANT_MARKED_AS_ALIVE,
            user=AuditUsers.DEMOGRAPHICS,
            participant_ids=[existing_record.get('participant_id')]
        )


def is_gp_notification_required(existing_record, updated_record):
    existing_gp_practice_code = existing_record.get('registered_gp_practice_code')
    updated_gp_practice_code = updated_record.get('registered_gp_practice_code')
    return updated_gp_practice_code and existing_gp_practice_code != updated_gp_practice_code


def close_existing_episode(participant_id, user_id):
    now = datetime.now(timezone.utc)
    live_episodes = query_for_live_episode(participant_id)

    if not live_episodes:
        log({
            'log_reference': LogReference.PART0015,
            'participant_id': participant_id
        })
        return

    for live_episode in live_episodes:

        key = {
            'participant_id': participant_id,
            'sort_key': live_episode['sort_key']
        }

        update_participant_episode_record(
            key,
            update_attributes={
                'status': EpisodeStatus.CLOSED,
                'closed_date': now.date().isoformat(),
                'user_id': user_id,
            },
            delete_attributes=['live_record_status'])
        log({
            'log_reference': LogReference.PART0016,
            'participant_id': participant_id
        })


def assign_to_age_group(date_of_birth):
    age = calculate_age(date_of_birth)
    if age < 24.5:
        age_group = 'under 24.5'
    if 24.5 <= age <= 49:
        age_group = '24.5 to 49'
    if 49 < age <= 65:
        age_group = '50 to 65'
    if age > 65:
        age_group = 'over 65'
    return age_group


def calculate_age(date_of_birth):
    date_of_birth_as_date = parse(date_of_birth, yearfirst=True, dayfirst=False).date()
    date_today = date.today()
    age = relativedelta(date_today, date_of_birth_as_date)

    if age.months >= 6:
        return age.years + 0.5

    return age.years


def log_PDS_record_date_of_birth_information(existing_record, demographics_data):
    existing_date_of_birth = existing_record.get('date_of_birth')
    new_date_of_birth = demographics_data.get('date_of_birth')
    data_record_type = demographics_data['data_record_type']
    date_of_birth_status = get_date_of_birth_status(new_date_of_birth)
    same_date_of_birth = False
    if date_of_birth_status == 'Complete' and existing_date_of_birth and new_date_of_birth:
        same_date_of_birth = (datetime.strptime(existing_date_of_birth, '%Y-%m-%d').date().isoformat()
                              == datetime.strptime(new_date_of_birth, '%Y%m%d').date().isoformat())
        log_date_of_birth_boundry_difference(existing_date_of_birth, new_date_of_birth)

    log({
        'log_reference': LogReference.PART0019,
        'same_date_of_birth': same_date_of_birth,
        'date_of_birth_status': date_of_birth_status,
        'data_record_type': data_record_type,
        'matched_to_existing_record': existing_record != {}
    })


def get_date_of_birth_status(new_date_of_birth):
    if len(str(new_date_of_birth)) != 8:
        return 'Incomplete'
    try:
        datetime.strptime(new_date_of_birth, '%Y%m%d').date().isoformat()
        return 'Complete'
    except Exception:
        return "Can't be converted"


def log_date_of_birth_boundry_difference(existing_date_of_birth, new_date_of_birth):
    now = datetime.now(timezone.utc).date()

    difference = relativedelta(now, datetime.strptime(existing_date_of_birth, '%Y-%m-%d'))
    existing_age = difference.years + (difference.months / 12)
    difference = relativedelta(now, datetime.strptime(new_date_of_birth, '%Y%m%d'))
    new_age = difference.years + (difference.months / 12)

    if new_age > 24.5 and existing_age < 24.5:
        boundry_difference = 'Date of Birth difference is over a 24.5 year old boundary i.e. PDS is over 24.5 (eligible) NHAIS says under 24.5 (not eligible).' 
        log({'log_reference': LogReference.PART0020, 'date_of_birth_boundry_difference': boundry_difference})
    elif new_age < 24.5 and existing_age > 24.5:
        boundry_difference = 'Date of Birth difference is over a 24.5 year old boundary i.e. NHAIS is over 24.5 (eligible) PDS says under 24.5 (not eligible).' 
        log({'log_reference': LogReference.PART0020, 'date_of_birth_boundry_difference': boundry_difference})

    if new_age > 50 and existing_age < 50:
        boundry_difference = 'Date of Birth difference is over a 50 year old boundary i.e. PDS is over 50 (5 yr recall) NHAIS says under 50 (3 yr).' 
        log({'log_reference': LogReference.PART0020, 'date_of_birth_boundry_difference': boundry_difference})
    elif new_age < 50 and existing_age > 50:
        boundry_difference = 'Date of Birth difference is over a 50 year old boundary i.e. NHAIS is over 50 (5 yr recall) PDS says under 50 (3 yr).' 
        log({'log_reference': LogReference.PART0020, 'date_of_birth_boundry_difference': boundry_difference})

    if new_age > 65 and existing_age < 65:
        boundry_difference = 'Date of Birth difference is over a 65 year old boundary i.e. PDS is over 65 (not eligible) NHAIS says under 65 (eligible).'  
        log({'log_reference': LogReference.PART0020, 'date_of_birth_boundry_difference': boundry_difference})
    elif new_age < 65 and existing_age > 65:
        boundry_difference = 'Date of Birth difference is over a 65 year old boundary i.e. NHAIS is over 65 (not eligible) PDS says under 65 (eligible).'  
        log({'log_reference': LogReference.PART0020, 'date_of_birth_boundry_difference': boundry_difference})


def _get_address_match_token(address):
    house_number = ''
    house_name = ''
    for i in range(1, 6):
        address_line = f'address_line_{i}'
        address_str = address.get(address_line)
        if address_str:
            newstr = ''.join((ch if ch.isdigit() else '') for ch in address_str)
            house_number += newstr
    if house_number == '':
        house_name = address.get('address_line_1', '').upper().replace(' ', '')
    return house_name, house_number + address.get('postcode', '').upper().replace(' ', '')


def _log_PDS_record_address_information(existing_record, demographics_data):

    existing_house_number_postcode = ''
    if not existing_record:
        existing_category = 'No matching participant record'
    else:
        existing_house_name, existing_house_number_postcode = _get_address_match_token(
            existing_record.get('address', {}))
        existing_category = 'a' if existing_house_number_postcode else 'NO'

    match = False
    new_house_name, new_house_number_postcode = _get_address_match_token(demographics_data)
    new_category = 'a' if new_house_number_postcode else 'NO'
    if existing_house_number_postcode and new_house_number_postcode:
        match = existing_house_name + existing_house_number_postcode == new_house_name + new_house_number_postcode

    log({
        'log_reference': LogReference.PART0022,
        'existing_category': existing_category,
        'new_category': new_category,
        'type': demographics_data['data_record_type'],
        'match': match
    })


def log_pds_invalid(existing_record, demographics_data):
    record_type = demographics_data['data_record_type']
    invalid_patient = demographics_data.get('is_invalid') == 'Y'
    ldn = ldn_from_existing_record(existing_record)
    age_group = age_group_from_demographics(demographics_data)

    log({
        'log_reference': LogReference.PART0021,
        'PDS_record_type': record_type,
        'matched': bool(existing_record),
        'is_invalid_patient_flag': invalid_patient,
        'age_group': age_group,
        'LDN': ldn
    })


def ldn_from_existing_record(existing_record):
    if 'migrated_1C_data' in existing_record:
        migrated_parts = existing_record['migrated_1C_data']['value'].split('|')
        return migrated_parts[12]
    elif 'migrated_1R_data' in existing_record:
        migrated_parts = existing_record['migrated_1R_data']['value'].split('|')
        return migrated_parts[5]
    return 'NODATA'


def age_group_from_demographics(demographics_data):
    date_of_birth = demographics_data.get('date_of_birth')
    date_of_birth_status = get_date_of_birth_status(date_of_birth)

    if date_of_birth_status == 'Complete':
        now = datetime.now(timezone.utc).date()
        difference = relativedelta(now, datetime.strptime(date_of_birth, '%Y%m%d'))
        age = difference.years + (difference.months / 12)
        if age < 24.5:
            return '0-24.5'
        if age < 50:
            return '24.5-49'
        if age < 65:
            return '50-65'
        return 'Over 65'

    return 'NODATA'


def log_PDS_name_information(existing_record, demographics_data):
    log_parameters = {}

    new_first_forename = demographics_data.get('given_name', '')
    new_surname = demographics_data.get('family_name', '')

    existing_first_forename = existing_record.get('first_name', '')
    existing_surname = existing_record.get('last_name', '')
    if existing_first_forename:
        log_parameters['same_first_forename'] = new_first_forename == existing_first_forename
    if existing_surname:
        log_parameters['same_surname'] = new_surname == existing_surname

    log_parameters['matched_to_existing_record'] = existing_record != {}
    log_parameters['PDS_first_forename'] = new_first_forename != ''
    log_parameters['PDS_surname'] = new_surname != ''
    log_parameters['NHAIS_first_forename'] = existing_first_forename != ''
    log_parameters['NHAIS_surname'] = existing_surname != ''
    log_parameters['data_record_type'] = demographics_data['data_record_type']

    log({'log_reference': LogReference.PART0023, **log_parameters})


def log_PDS_significant_changes(existing_record, demographics_data):
    log_entry = {
        'log_reference': LogReference.PART0025,
        'match_record': bool(existing_record),
        'record_type': demographics_data['data_record_type']
    }

    if existing_record:
        log_entry['match_family'] = demographics_data.get('family_name') == existing_record.get('last_name')
        log_entry['match_dob'] = (
            demographics_data.get('date_of_birth') == existing_record.get('date_of_birth', '').replace('-', '')
        )
        log_entry['match_gender'] = int(demographics_data['gender']) == int(existing_record.get('gender', '-1'))

    log(log_entry)


def handle_validation_result(demographics_data: Dict, validation_result: PdsValidationContext, event: Dict):
    if (validation_result.failed_rules):
        log(
            {
                'log_reference': LogReference.PART0026,
                'hard_fail': validation_result.hard_fail,
                'failed_rules': validation_result.failed_rules
            }
        )
        message = get_message_body_from_event(event)
        message['metadata'] = {
            'hard_fail': validation_result.hard_fail,
            'failed_rules': validation_result.failed_rules
        }
        log_rejected_message(event, message, demographics_data['nhs_number'])

        if validation_result.hard_fail:
            send_new_message(LAMBDA_DLQ_URL, message)

    for rejected_field in validation_result.rejected_fields:
        demographics_data.pop(rejected_field, None)

    if validation_result.replace_fields:
        demographics_data = {**demographics_data, **validation_result.replace_fields}

    return demographics_data


def log_rejected_message(event: Dict, message: Dict, nhs_number):
    uuid = get_message_uuid_from_event(event)
    now = datetime.now(timezone.utc)

    record = {**message}
    record['uuid'] = uuid
    record['nhs_number'] = nhs_number
    record['rejection_type'] = f'PDS#{now:%Y-%m-%d}'
    record['rejected_status'] = RejectedStatus.UNPROCESSED
    record['created'] = now.isoformat()

    create_or_replace_rejected_record(record)


def cease_participant(updated_record, additional_information, cease_reason):
    cease_participant_and_send_notification_messages(
        updated_record, AuditUsers.SYSTEM.value, {'reason': cease_reason}
    )
    additional_information['reason'] = cease_reason
    audit(action=AuditActions.CEASE_EPISODE,
          user=AuditUsers.SYSTEM,
          participant_ids=[updated_record['participant_id']],
          additional_information=additional_information
          )


def supersede_participant(
    nhs_number: str,
    superseded_nhs_number: str,
    additional_information: Dict,
    existing_record: Dict,
    updated_record: Dict
):
    existing_participants = query_participants_by_nhs_number(superseded_nhs_number)
    if existing_participants:
        superseding_participant = existing_participants[0]
    else:
        superseding_participant = merge_records_for_superseded(superseded_nhs_number, existing_record, updated_record)
        create_replace_record_in_participant_table(superseding_participant)
    audit(
        action=AuditActions.SUPERSEDING,
        user=AuditUsers.SYSTEM,
        participant_ids=[superseding_participant['participant_id']],
        additional_information={'supersedes': nhs_number}
    )
    additional_information['superseded_nhs_number'] = superseded_nhs_number
    cease_participant(updated_record, additional_information, CeaseReason.MERGED)


def merge_records_for_superseded(nhs_number: str, existing_record: Dict, updated_record: Dict) -> Dict:
    merged_record = {
        'participant_id': generate_participant_id(),
        'sort_key': 'PARTICIPANT',
        'nhs_number': nhs_number,
        'sanitised_nhs_number': sanitise_nhs_number(nhs_number)
    }

    for field in SUPERSEDE_FIELDS_EXISTING:
        if existing_record and field in existing_record:
            merged_record[field] = existing_record[field]

    for field, value in updated_record.items():
        if field not in SUPSERSED_FIELDS_NOT_UPDATED:
            merged_record[field] = value

    return merged_record
