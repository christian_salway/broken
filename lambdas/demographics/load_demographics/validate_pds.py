from common.utils.data_segregation.nhais_ciphers import ALL_CODES
from common.utils.participant_utils import nhs_checksum_valid
import re
from typing import Dict, List


RULE_CSMS_1 = 'CSMS-1'
RULE_CSMS_2 = 'CSMS-2'
RULE_CSMS_3 = 'CSMS-3'
RULE_CSMS_4 = 'CSMS-4'
RULE_CSMS_5 = 'CSMS-5'
RULE_CSMS_6 = 'CSMS-6'
RULE_CSMS_7 = 'CSMS-7'
RULE_CSMS_8 = 'CSMS-8'
RULE_CSMS_9 = 'CSMS-9'

NO_FIXED_ABODE_POSTCODE = 'zz993vz'
NO_FIXED_ABODE_STRING_LIST = [
    'nfa',
    'nofixedabode',
    'zz993vz',
    'ukn',
    'unknown'
]


class PdsValidationContext:
    next_rule: str
    rejected_fields: List[str] = []
    failed_rules: List[str] = []
    hard_fail: bool = False
    replace_fields: Dict = {}

    def __init__(
        self,
        previous_context,
        next_rule: str,
        rejected_fields: List[str],
        failed_rules: List[str],
        hard_fail: bool,
        replace_fields: Dict
    ):
        if (previous_context):
            self.rejected_fields = previous_context.rejected_fields + rejected_fields
            self.failed_rules = previous_context.failed_rules + failed_rules
            self.hard_fail = previous_context.hard_fail or hard_fail
            self.replace_fields = {**previous_context.replace_fields, **replace_fields}
        else:
            self.rejected_fields = rejected_fields
            self.failed_rules = failed_rules
            self.hard_fail = hard_fail
            self.replace_fields = replace_fields

        self.next_rule = next_rule


def _strategy_nhs_number(context: PdsValidationContext, pds_record: Dict) -> PdsValidationContext:
    nhs_number = pds_record.get('nhs_number', '')
    if len(nhs_number) < 10 or not nhs_checksum_valid(nhs_number):
        return PdsValidationContext(context, None, ['nhs_number'], [RULE_CSMS_1], True, {})
    return PdsValidationContext(context, RULE_CSMS_2, [], [], False, {})


def _strategy_removal(context: PdsValidationContext, pds_record: Dict) -> PdsValidationContext:
    if pds_record.get('data_record_type') == 'REMOVED':
        return PdsValidationContext(context, None, [], [], False, {})
    return PdsValidationContext(context, RULE_CSMS_3, [], [], False, {})


def _strategy_address_paf_key(context: PdsValidationContext, pds_record: Dict) -> PdsValidationContext:
    if pds_record.get('paf_key'):
        return PdsValidationContext(context, RULE_CSMS_8, [], [], False, {})

    return PdsValidationContext(context, RULE_CSMS_4, [], [], False, {})


def _strategy_blank_name(context: PdsValidationContext, pds_record: Dict) -> PdsValidationContext:
    given_name = pds_record.get('given_name', '')
    family_name = pds_record.get('family_name', '')
    if not given_name.strip() or not family_name.strip():
        return PdsValidationContext(context, RULE_CSMS_9, ['given_name', 'family_name'], [RULE_CSMS_8], False, {})
    return PdsValidationContext(context, RULE_CSMS_9, [], [], False, {})


def _strategy_no_fixed_abode(context: PdsValidationContext, pds_record: Dict) -> PdsValidationContext:
    postcode = pds_record.get('postcode', '').lower().replace(' ', '')
    addressLines = [pds_record[field].lower().replace(' ', '')
                    for field in pds_record if field.startswith('address_line_')]
    replace_fields = {'address_line_1': 'No Fixed Abode', 'postcode': 'ZZ99 3VZ'}
    if (postcode == NO_FIXED_ABODE_POSTCODE or
            (any(nfa_string in addressLines for nfa_string in NO_FIXED_ABODE_STRING_LIST) and not postcode)):
        return PdsValidationContext(context, RULE_CSMS_8, [], [], False, replace_fields)
    else:
        return PdsValidationContext(context, RULE_CSMS_7, [], [], False, {})


def _strategy_zz99_check(context: PdsValidationContext, pds_record: Dict) -> PdsValidationContext:
    postcode = pds_record.get('postcode', '').upper()
    clean_postcode = re.sub(r'[^A-Z0-9]', '', postcode)
    if clean_postcode.startswith('ZZ99'):
        return PdsValidationContext(context, None, ['postcode'], [RULE_CSMS_5], True, {})
    return PdsValidationContext(context, RULE_CSMS_8, [], [], False, {})


def _strategy_address_lines_1_2_4(context: PdsValidationContext, pds_record: Dict) -> PdsValidationContext:
    if not pds_record.get('address_line_1') \
       and not pds_record.get('address_line_2') \
       and not pds_record.get('address_line_4'):
        return PdsValidationContext(context,
                                    None,
                                    [],
                                    [RULE_CSMS_6],
                                    True,
                                    {})

    return PdsValidationContext(context, RULE_CSMS_5, [], [], False, {})


def _strategy_postcode_null(context: PdsValidationContext, pds_record: Dict) -> PdsValidationContext:
    if not pds_record.get('postcode'):
        return PdsValidationContext(context, None, ['postcode'], [RULE_CSMS_7], True, {})
    return PdsValidationContext(context, RULE_CSMS_6, [], [], False, {})


def _strategy_nhais_posting(context: PdsValidationContext, pds_record: Dict) -> PdsValidationContext:
    if pds_record.get('nhais_posting') not in ALL_CODES:
        replace_fields = {'nhais_posting': 'INVALID'}
        return PdsValidationContext(context, None, [], [RULE_CSMS_9], False, replace_fields)
    return PdsValidationContext(context, None, [], [], False, {})


RULE_STRATEGY_MAP = {
    RULE_CSMS_1: _strategy_nhs_number,
    RULE_CSMS_2: _strategy_removal,
    RULE_CSMS_3: _strategy_address_paf_key,
    RULE_CSMS_4: _strategy_no_fixed_abode,
    RULE_CSMS_5: _strategy_zz99_check,
    RULE_CSMS_6: _strategy_address_lines_1_2_4,
    RULE_CSMS_7: _strategy_postcode_null,
    RULE_CSMS_8: _strategy_blank_name,
    RULE_CSMS_9: _strategy_nhais_posting
}


def _pds_record_validation(context: PdsValidationContext, pds_record: Dict) -> PdsValidationContext:
    while context.next_rule in RULE_STRATEGY_MAP:
        context = RULE_STRATEGY_MAP[context.next_rule](context, pds_record)
    return context


def pds_record_validation(pds_record: Dict) -> PdsValidationContext:
    context = PdsValidationContext(None, RULE_CSMS_1, [], [], False, {})
    return _pds_record_validation(context, pds_record)
