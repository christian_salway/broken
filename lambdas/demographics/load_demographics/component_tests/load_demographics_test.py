from datetime import datetime, timezone
from unittest import TestCase
from unittest.mock import patch, Mock, call

from boto3.dynamodb.conditions import Key

from common.models.participant import ParticipantSortKey, ParticipantStatus
from common.test_mocks.mock_events import sqs_mock_event

datetime_mock = Mock()
datetime_mock.now.return_value = datetime(2020, 1, 1, tzinfo=timezone.utc)

amend_record_row = '''AMENDED|20200527012038|3|9413856990||B87614|20170927|GG|20081010|SUN|20081010||MR|JONAH|ABAYOMI|RAHMAN|
              |19750611|0|AutoFirst|1 WALESBY LANE COTTAGES||NEWARK||NG22 9RD||20200527|||||||07700900682|20170811||
              |en|0||True|E07000175'''

new_record_row = '''NEW|20200527012038|3|9413856990||A60|20170927|GG|20081010|SUN|20081010||MR|JONAH|ABAYOMI|RAHMAN|
                |19250611|0|AutoFirst|1 WALESBY LANE COTTAGES||NEWARK||NG22 9RD||20200527|||||||07700900682|20170811||
                |en|0||False|E07000175'''

participants_table_mock = Mock()
sqs_client_mock = Mock()
dynamodb_resource_mock = Mock()


mock_env_vars = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE_NAME',
    'GP_NOTIFICATION_QUEUE_URL': 'the_gp_notification_queue_url',
    'NOTIFICATION_QUEUE_URL': 'the_notification_queue_url',
    'RESEND_NOTIFICATION_QUEUE_URL': 'the_resend_notification_queue_url',
    'AUDIT_QUEUE_URL': 'the_audit_queue_url'
}


with patch('os.environ', mock_env_vars):
    from load_demographics import load_demographics


def _build_event(update_row):
    body_dict = {
        'internal_id': 'internal_id',
        'file_name': 'file_name_before_part',
        'file_part_name': 'file_key',
        'line_index': 1,
        'line': update_row
    }

    return sqs_mock_event(body_dict)


@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_resource',
       Mock(return_value=dynamodb_resource_mock))
@patch('common.utils.cease_utils.get_internal_id', Mock(return_value='1'))
@patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
@patch('load_demographics.load_demographics.get_internal_id', Mock(return_value='1'))
@patch('common.utils.next_test_due_date_calculators.next_test_due_date_calculator.datetime', datetime_mock) 
@patch('common.utils.audit_utils.datetime', datetime_mock)
@patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(return_value=participants_table_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('os.environ', mock_env_vars)
class TestLoadDemographics(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        participants_table_mock.query.return_value = []
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        dynamodb_resource_mock.reset_mock()
        self.log_patcher.start()
        dynamodb_resource_mock.batch_get_item.return_value = {
            'Responses': {
                'PARTICIPANTS_TABLE_NAME': []
            }
        }

    def tearDown(self):
        self.log_patcher.stop()

    def test_load_demographics_with_amend_record(self):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '9413856990',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG',
                                   'address': {
                                       'address_line_1': '1 WHITEHALL QUAY',
                                       'address_line_2': 'WHITEHALL ROAD',
                                       'address_line_3': 'CITY CENTRE',
                                       'address_line_4': 'LEEDS',
                                       'address_line_5': 'WEST YORKSHIRE',
                                       'postcode': 'LS1 4HR'
                                   },
                                   'title': 'MRS',
                                   'first_name': 'SARAH',
                                   'middle_names': 'JANE',
                                   'last_name': 'TAYLOR',
                                   'serial_change_number': '2'
                                   }]}

        participants_table_mock.query.side_effect = _query_mock

        event = _build_event(amend_record_row)

        context = Mock()
        context.function_name = ''

        conditional_string = ' (serial_change_number < :scn) '
        conditional_string_values = {':scn': 3}

        load_demographics.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant
        participants_table_mock.query.assert_called_once()
        participants_table_mock.query.assert_called_with(
            IndexName='sanitised-nhs-number-sort-key',
            KeyConditionExpression=Key('sanitised_nhs_number').eq('9413856990') & Key('sort_key').eq('PARTICIPANT')
        )

        # Asserts it stores the record with the correct reason
        participants_table_mock.put_item.assert_called()
        _, kwargs = participants_table_mock.put_item.call_args
        self.assertEqual(conditional_string, kwargs['ConditionExpression'])
        self.assertEqual(conditional_string_values, kwargs['ExpressionAttributeValues'])
        self.assertEqual('9413856990', kwargs['Item'].get('participant_id'))
        self.assertEqual(3, kwargs['Item'].get('serial_change_number'))

        # Assert it creates the expected SQS messages for the audit actions, gp notification and notification to resend
        expected_gp_notification_body = '{\n    "internal_id": "1",\n    "notification_origin": "GP_REGISTRATION",\n    "participant_id": "9413856990",\n    "registered_gp_practice_code": "B87614"\n}'  
        expected_fp69_audit_body = '{"action": "FP69_SET", "timestamp": "2020-01-01T00:00:00+00:00", \
"internal_id": "1", "nhsid_useruid": "LOAD_DEMOGRAPHICS", "session_id": "NONE", "participant_ids": ["9413856990"]}'
        expected_resend_notification_body = '{\n    "internal_id": "1",\n    "participant_id": "9413856990"\n}'
        expected_amend_audit_body = '{"action": "COHORT_AMEND", "timestamp": "2020-01-01T00:00:00+00:00", \
"internal_id": "1", "nhsid_useruid": "LOAD_DEMOGRAPHICS", "session_id": "NONE", "participant_ids": ["9413856990"], \
"additional_information": {"changed_fields": ["address_effective_from_date", "date_of_birth", "first_name", "gender", \
"is_ceased", "is_invalid_patient", "last_name", "middle_names", "nhais_cipher", \
"registered_gp_practice_code", "title"], "changed_address": \
{"address_line_1": {"previous": "1 WHITEHALL QUAY", "current": "AutoFirst"}, \
"address_line_2": {"previous": "WHITEHALL ROAD", "current": "1 WALESBY LANE COTTAGES"}, \
"address_line_3": {"previous": "CITY CENTRE", "current": null}, \
"address_line_4": {"previous": "LEEDS", "current": "NEWARK"}, \
"address_line_5": {"previous": "WEST YORKSHIRE", "current": null}, \
"postcode": {"previous": "LS1 4HR", "current": "NG22 9RD"}}, \
"changed_names": {"title": {"previous": "MRS", "current": "MR"}, \
"first_name": {"previous": "SARAH", "current": "JONAH"}, \
"last_name": {"previous": "TAYLOR", "current": "RAHMAN"}, "middle_names": {"previous": "JANE", "current": "ABAYOMI"}}, \
"changed_gp_practice_code": {"previous": null, "current": "B87614"}, \
"changed_date_of_birth": {"previous": null, "current": "1975-06-11"}, "updating_next_test_due_date_to": "2020-03-12"}}'
        expected_log = [
            call(QueueUrl='the_audit_queue_url', MessageBody=expected_fp69_audit_body),
            call(QueueUrl='the_resend_notification_queue_url', MessageBody=expected_resend_notification_body),
            call(QueueUrl='the_gp_notification_queue_url', MessageBody=expected_gp_notification_body),
            call(QueueUrl='the_audit_queue_url', MessageBody=expected_amend_audit_body)
        ]
        sqs_client_mock.send_message.assert_has_calls(expected_log)

    @patch('load_demographics.participant_record_updater.generate_participant_id', Mock(return_value='1'))
    def test_load_demographics_with_new_record(self):
        def _query_mock(*args, **kwargs):
            return {'Items': []}

        participants_table_mock.query.side_effect = _query_mock

        event = _build_event(new_record_row)

        context = Mock()
        context.function_name = ''

        load_demographics.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant
        participants_table_mock.return_value = {'Items': {}}
        participants_table_mock.query.assert_has_calls([
            call(IndexName='sanitised-nhs-number-sort-key', KeyConditionExpression=Key('sanitised_nhs_number').eq('9413856990') & Key('sort_key').eq('PARTICIPANT'))  
        ])

        # Asserts it stores the record with the correct reason
        participants_table_mock.update_item.assert_has_calls([
            call(
                Key={'participant_id': '1', 'sort_key': 'PARTICIPANT'},
                UpdateExpression='SET #is_ceased = :is_ceased, #status = :status REMOVE #next_test_due_date',
                ExpressionAttributeValues={':is_ceased': True, ':status': ParticipantStatus.CEASED},
                ExpressionAttributeNames={
                    '#is_ceased': 'is_ceased', '#status': 'status', '#next_test_due_date': 'next_test_due_date'},
                ReturnValues='NONE'
            )
        ])

        # Assert it creates the expected SQS messages for the audit actions, gp notification and notification to resend
        expected_gp_notification_body = '{\n    "internal_id": "1",\n    "notification_origin": "GP_REGISTRATION",\n    "participant_id": "1",\n    "registered_gp_practice_code": "A60"\n}'  
        expected_cease_gp_notification_body = '{\n    "internal_id": "1",\n    "notification_origin": "CEASED",\n    "participant_id": "1",\n    "registered_gp_practice_code": "A60"\n}'  
        expected_add_audit_body = '{"action": "COHORT_ADD", "timestamp": "2020-01-01T00:00:00+00:00", \
"internal_id": "1", "nhsid_useruid": "LOAD_DEMOGRAPHICS", "session_id": "NONE", "participant_ids": ["1"]}'
        expected_cease_audit_body = '{"action": "CEASE_EPISODE", "timestamp": "2020-01-01T00:00:00+00:00", "internal_id": "1", "nhsid_useruid": "SYSTEM", "session_id": "NONE", "participant_ids": ["1"], "additional_information": {"reason": "AUTOCEASE_DUE_TO_AGE"}}' 
        expected_notification_body = '{\n    "participant_id": "1",\n    "template": "CUA",\n    "type": "Cease letter"\n}' 
        expected_log = [
            call(QueueUrl='the_gp_notification_queue_url', MessageBody=expected_cease_gp_notification_body),
            call(QueueUrl='the_notification_queue_url', MessageBody=expected_notification_body),
            call(QueueUrl='the_audit_queue_url', MessageBody=expected_cease_audit_body),
            call(QueueUrl='the_gp_notification_queue_url', MessageBody=expected_gp_notification_body),
            call(QueueUrl='the_audit_queue_url', MessageBody=expected_add_audit_body),
        ]
        sqs_client_mock.send_message.assert_has_calls(expected_log)
