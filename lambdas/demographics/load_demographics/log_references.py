import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    PART0001 = (logging.INFO, 'Consumed message from the SQS Queue.')
    PART0002 = (logging.INFO, 'Decoding message and transforming to participant schema format.')
    PART0003 = (logging.INFO, 'No existing participant for given NHS number.')
    PART0004 = (logging.INFO, 'Address has changed. Creating previous address record.')
    PART0005 = (logging.INFO, 'Name has changed. Creating previous name record')
    PART0006 = (logging.INFO, 'Updating participant.')
    PART0007 = (logging.INFO, 'Participant has been removed from cohort. Creating cease record.')
    PART0008 = (logging.INFO, 'Message processed successfully.')
    PART0009 = (logging.ERROR, 'Record in database is newer')
    PART0010 = (logging.INFO, 'Sending conditional update for participant.')
    PART0011 = (logging.INFO, 'registered_gp_practice_code has changed, sending gp notification')
    PART0012 = (logging.ERROR, 'Unable to parse value')
    PART0013 = (logging.INFO, 'address has changed or fp69 flag unset, resending notification')
    PART0014 = (logging.INFO, 'Logging gender information for PDS record')
    PART0015 = (logging.INFO, 'No live episodes found')
    PART0016 = (logging.INFO, 'Closing episode')
    PART0017 = (logging.INFO, 'Record was a match')
    PART0018 = (logging.INFO, 'Record was not a match')
    PART0019 = (logging.INFO, 'Logging date of birth information for PDS Record')
    PART0020 = (logging.INFO, 'Logging date of birth boundry difference information for PDS Record')
    PART0021 = (logging.INFO, 'Logging invalid status for PDS Record')
    PART0022 = (logging.INFO, 'Logging address difference information for PDS Record')
    PART0023 = (logging.INFO, 'Logging name information for PDS record')
    PART0024 = (logging.INFO, 'Logging date of death information for PDS record')
    PART0025 = (logging.INFO, 'Logging significant differences')
    PART0026 = (logging.INFO, 'PDS record failed at least one validation rule')
    PART0027 = (logging.WARNING, "Serial Change Number is lower than existing record."
                " Aborting further processing and moving the record to the DLQ.")
    PART0028 = (logging.WARNING, "Invalid nhais_posting defaulting to 'ENG'")
    PART0029 = (logging.INFO, 'Participant status of death has changed to be no longer dead')
