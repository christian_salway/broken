This lambda is triggered by messages placed on the import_demographics_raw sqs queue by the import_demographics lambda.
The messages contain a raw line from a cohort file along with some metadata about that line.
If the message is of type REMOVED then the participant will be marked as "is_ceased" and a CEASED record will be created.
If a record in the participants dynamodb table already exists for this participant then this record will be updated, else a new record will be created.
If there is an existing record and the participant has changed name and/or address then previous name and/or previous address records are created.
If the record is to be updated this lambda will supply a serial change number (SCN) to compare against the existing record.
If the SCN value supplied by the lambda is greater than what is stored, then the record will be updated. This is to stop out dated information being stored against the participant.