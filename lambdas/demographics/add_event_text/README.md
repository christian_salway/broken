This lambda is being triggered by a PUT request to the endpoint /participants/{participant_id}/event

it accepts body like
```
{
  "event_text": "Some event text"
}
```
or
```
{
  "event_text": ""
}
```

and it adds, edits or removes [event_text] from a participant record along with auditing.