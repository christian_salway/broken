import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    EVENTTEXT001 = (logging.INFO, 'Received request to modify event text')
    EVENTTEXT002 = (logging.ERROR, 'Participant does not exist')
    EVENTTEXT003 = (logging.ERROR, 'Request body does not contain event text')
    EVENTTEXT004 = (logging.INFO, 'Added Event Text to participant')
    EVENTTEXT005 = (logging.INFO, 'Removed Event Text from participant')
    EVENTTEXT006 = (logging.ERROR, 'Event text greater than 1000 characters')
