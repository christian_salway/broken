from common.log import log
from common.utils import json_return_object, json_return_message
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils.audit_utils import audit, AuditActions
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.participant_utils import (
    get_participant_by_participant_id,
    update_participant_record
)
from add_event_text.log_references import LogReference


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    event_data, *_ = get_api_request_info_from_event(event)
    request_body = event_data.get('body')

    log({
        'log_reference': LogReference.EVENTTEXT001,
        'request_keys': list(request_body.keys())
    })

    participant_id = event_data['path_parameters']['participant_id']
    participant = get_participant_by_participant_id(participant_id, segregate=True)

    if not participant:
        log(LogReference.EVENTTEXT002)
        return json_return_message(404, 'Participant does not exist')

    if not request_body or 'event_text' not in request_body.keys():
        log(LogReference.EVENTTEXT003)
        return json_return_message(400, 'Request body does not contain event text')

    event_text = request_body['event_text']
    old_event_text = participant.get('event_text')

    if event_text and len(event_text) > 1000:
        log(LogReference.EVENTTEXT006)
        return json_return_message(400, 'Event information text is greater than 1000 characters')

    key, update_attributes, delete_attributes = generate_update_event_record(event_text, participant)
    update_participant_record(key, update_attributes, delete_attributes=delete_attributes)

    additional_information = {}
    if update_attributes:
        additional_information = {'new_event_text': event_text}
        if old_event_text is not None:
            additional_information['old_event_text'] = old_event_text
        log(LogReference.EVENTTEXT004)
        audit(action=AuditActions.EVENT_TEXT_ADDED,
              session=event_data.get('session'),
              participant_ids=[participant_id],
              nhs_numbers=[participant['nhs_number']],
              additional_information=additional_information)
    if delete_attributes:
        log(LogReference.EVENTTEXT005)
        audit(action=AuditActions.EVENT_TEXT_REMOVED,
              session=event_data.get('session'),
              participant_ids=[participant_id],
              nhs_numbers=[participant['nhs_number']],
              additional_information={'old_event_text': old_event_text})

    return json_return_object(200, {})


def generate_update_event_record(event_text, participant):
    key = {
        'participant_id': participant['participant_id'],
        'sort_key': 'PARTICIPANT'
    }
    update_attributes = {}
    delete_attributes = None
    if event_text:
        update_attributes = {'event_text': event_text}
    else:
        delete_attributes = ['event_text']

    return key, update_attributes, delete_attributes
