from mock import Mock, call, patch

from datetime import datetime, timezone
import json
from unittest.case import TestCase

log_mock = Mock()
with patch('common.log.log', log_mock):
    from add_event_text import add_event_text

from common.models.participant import ParticipantSortKey 
from common.utils.audit_utils import AuditActions 
from common.utils.data_segregation.nhais_ciphers import Cohorts 


SESSION = {
    "session_id": "12345678",
    "user_data": {'nhsid_useruid': 'NHS12345', 'first_name': 'firsty', 'last_name': 'lasty'},
    'selected_role': {
        'organisation_code': 'NHSE',
        'organisation_name': 'YORKSHIRE AND THE HUMBER',
        'role_id': 'R8010',
        'role_name': 'Clerical Role',
        'workgroups': [str(Cohorts.ENGLISH)]
    },
    'access_groups': {
        'pnl': True,
        'sample_taker': False,
        'lab': False,
        'colposcopy': False,
        'csas': True,
        'view_participant': True
    }
}

boto3_client = Mock()
boto3_resource = Mock()
participants_table_mock = Mock()
sqs_client_mock = Mock()

example_date = datetime(2020, 1, 23, 13, 48, 8, 123456, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.today.return_value = example_date
datetime_mock.now.return_value = example_date


@patch('common.utils.audit_utils.datetime', datetime_mock)
@patch('common.utils.audit_utils.client', boto3_client)
@patch('common.utils.dynamodb_access.dynamodb_boto3.resource', boto3_resource)
@patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'participants-table', 'AUDIT_QUEUE_URL': 'audit-queue-url'})
class TestAddEventText(TestCase):

    def setUp(self):
        boto3_client.reset_mock()
        boto3_resource.reset_mock()
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()

        def database_table_mocks(table_name):
            if table_name == 'participants-table':
                return participants_table_mock
        boto3_table_mock = Mock()
        boto3_table_mock.Table.side_effect = database_table_mocks
        boto3_resource.return_value = boto3_table_mock
        boto3_client.side_effect = lambda *args, **kwargs: sqs_client_mock if args and args[0] == 'sqs' else Mock()

    def test_adding_participant_text(self):
        # Arrange
        event = {
            'requestContext': {'authorizer': {
                'session': json.dumps(SESSION), 'principalId': 'blah'
            }},
            'body': json.dumps({'event_text': 'Test event text'}),
            'pathParameters': {'participant_id': 'event_text_participant'},
            'headers': {'request_id': '1'}
        }
        context = Mock()
        context.function_name = ''
        participants_table_mock.get_item.return_value = {
            'Item': {
                'participant_id': 'event_text_participant',
                'sort_key': ParticipantSortKey.PARTICIPANT,
                'nhais_cipher': 'ENG',
                'nhs_number': '9999999999',
            }
        }

        # Act
        response = add_event_text.lambda_handler(event, context)

        # Assert
        participants_table_mock.get_item.assert_called_once_with(Key={
            'participant_id': 'event_text_participant',
            'sort_key': ParticipantSortKey.PARTICIPANT
        })
        participants_table_mock.update_item.assert_has_calls([call(
            Key={'participant_id': 'event_text_participant', 'sort_key': 'PARTICIPANT'},
            UpdateExpression='SET #event_text = :event_text',
            ExpressionAttributeValues={':event_text': 'Test event text'},
            ExpressionAttributeNames={'#event_text': 'event_text'},
            ReturnValues='NONE'
        )])

        sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='audit-queue-url',
            MessageBody=json.dumps(dict(
                action=AuditActions.EVENT_TEXT_ADDED.value,
                timestamp="2020-01-23T13:48:08.123456+00:00",
                internal_id="1",
                nhsid_useruid="NHS12345",
                session_id="12345678",
                first_name="firsty",
                last_name="lasty",
                role_id="R8010",
                user_organisation_code="NHSE",
                participant_ids=['event_text_participant'],
                nhs_numbers=['9999999999'],
                additional_information={'new_event_text': 'Test event text'}
            ))
        )

        self.assertEqual({
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
                'X-Content-Type-Options': 'nosniff',
                'Strict-Transport-Security': 'max-age=1576800'
            },
            'body': "{}",
            'isBase64Encoded': False
        }, response)

    def test_adding_participant_text_incorrect_participant_cohort(self):
        # Arrange
        event = {
            'requestContext': {'authorizer': {
                'session': json.dumps(SESSION), 'principalId': 'blah'
            }},
            'body': json.dumps({'event_text': 'Test event text'}),
            'pathParameters': {'participant_id': 'event_text_participant'},
            'headers': {'request_id': '1'}
        }
        context = Mock()
        context.function_name = ''
        participants_table_mock.get_item.return_value = {
            'Item': {
                'participant_id': 'event_text_participant',
                'sort_key': ParticipantSortKey.PARTICIPANT,
                'nhais_cipher': 'IM',
                'nhs_number': '9999999999',
            }
        }

        # Act
        response = add_event_text.lambda_handler(event, context)

        # Assert
        participants_table_mock.get_item.assert_called_once_with(Key={
            'participant_id': 'event_text_participant',
            'sort_key': ParticipantSortKey.PARTICIPANT
        })
        participants_table_mock.update_item.assert_not_called()

        sqs_client_mock.send_message.assert_not_called()
        self.assertEqual({
            'statusCode': 404,
            'headers': {
                'Content-Type': 'application/json',
                'X-Content-Type-Options': 'nosniff',
                'Strict-Transport-Security': 'max-age=1576800'
            },
            'body': '{"message": "Participant does not exist"}',
            'isBase64Encoded': False
        }, response)
