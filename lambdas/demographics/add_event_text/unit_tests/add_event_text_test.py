from unittest import TestCase
from mock import patch, call
from ddt import ddt, data, unpack
from common.utils import audit_utils
from common.test_assertions.api_assertions import assertApiResponse
from add_event_text.log_references import LogReference
from add_event_text.add_event_text import (lambda_handler, generate_update_event_record)

PARTICIPANT = {
    'participant_id': '123',
    'nhs_number': '9999999999',
    'event_text': 'Old event test'
}

SESSION = {'session_key': '1234567890'}


@ddt
class TestAddEventText(TestCase):

    module = 'add_event_text.add_event_text'

    @unpack
    @data(
        ('', {}, ['event_text']),
        (None, {}, ['event_text']),
        ('Test Event', {'event_text': 'Test Event'}, None)
    )
    def test_event_record_update_generated_successfully(
            self,
            event_text,
            expected_update_attributes,
            expected_delete_attributes):
        expected_key = {
            'participant_id': PARTICIPANT['participant_id'],
            'sort_key': 'PARTICIPANT'
        }

        actual_key, actual_update_attributes, actual_delete_attributes = generate_update_event_record(
            event_text, PARTICIPANT)

        self.assertDictEqual(expected_key, actual_key)
        self.assertDictEqual(expected_update_attributes, actual_update_attributes)
        self.assertEqual(expected_delete_attributes, actual_delete_attributes)

    @unpack
    @data(
        ('', {}, ['event_text'], 'EVENTTEXT005', 'EVENT_TEXT_REMOVED',
            {'old_event_text': 'Old event test'}),
        (None, {}, ['event_text'], 'EVENTTEXT005', 'EVENT_TEXT_REMOVED',
            {'old_event_text': 'Old event test'}),
        ('Test Event', {'event_text': 'Test Event'}, None, 'EVENTTEXT004', 'EVENT_TEXT_ADDED',
            {'old_event_text': 'Old event test', 'new_event_text': 'Test Event'})
    )
    @patch(f'{module}.audit')
    @patch(f'{module}.log')
    @patch(f'{module}.update_participant_record')
    @patch(f'{module}.generate_update_event_record')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_add_event(
            self,
            event_text,
            update_attributes,
            delete_attributes,
            log_reference,
            audit_reference,
            additional_information,
            request_info_mock,
            get_participant_mock,
            generate_update_event_record_mock,
            update_participant_mock,
            log_mock,
            audit_mock):

        request_body = {'event_text': event_text}

        key = {
            'participant_id': PARTICIPANT['participant_id'],
            'sort_key': 'PARTICIPANT'
        }
        request_info_mock.return_value = request_info(request_body)
        get_participant_mock.return_value = PARTICIPANT
        generate_update_event_record_mock.return_value = key, update_attributes, delete_attributes
        actual_response = lambda_handler.__wrapped__.__wrapped__({}, {})

        assertApiResponse(self, 200, {}, actual_response)
        get_participant_mock.assert_called_with('123', segregate=True)

        update_participant_mock.assert_called_with(
            key,
            update_attributes,
            delete_attributes=delete_attributes
        )

        expected_log_calls = [
            call({'log_reference': LogReference.EVENTTEXT001, 'request_keys': list(request_body.keys())}),
            call(LogReference[log_reference]),
        ]

        log_mock.assert_has_calls(expected_log_calls)

        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions[audit_reference],
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information=additional_information
            )
        ])

    @patch(f'{module}.audit')
    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_add_event_text_unknown_participant(self, request_info_mock, get_participant_mock, log_mock, audit_mock):
        request_body = {'event_text': 'This is Event Text'}
        request_info_mock.return_value = request_info(request_body)
        get_participant_mock.return_value = None

        actual_response = lambda_handler.__wrapped__.__wrapped__({}, {})
        assertApiResponse(self, 404, {'message': 'Participant does not exist'}, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.EVENTTEXT001, 'request_keys': list(request_body.keys())}),
            call(LogReference.EVENTTEXT002),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_not_called()

    @patch(f'{module}.audit')
    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_add_event_text_no_event_text_in_body(
            self,
            request_info_mock,
            get_participant_mock,
            log_mock,
            audit_mock):
        request_body = {}
        request_info_mock.return_value = request_info(request_body)
        get_participant_mock.return_value = {'Item': PARTICIPANT}

        actual_response = lambda_handler.__wrapped__.__wrapped__({}, {})
        assertApiResponse(self, 400, {'message': 'Request body does not contain event text'}, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.EVENTTEXT001, 'request_keys': list(request_body.keys())}),
            call(LogReference.EVENTTEXT003),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_not_called()

    @patch(f'{module}.audit')
    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_add_event_text_event_text_greater_than_1000_chars(
            self,
            request_info_mock,
            get_participant_mock,
            log_mock,
            audit_mock):
        request_body = {'event_text': 'This is Event Text'*1000}
        request_info_mock.return_value = request_info(request_body)
        get_participant_mock.return_value = {'Item': PARTICIPANT}

        actual_response = lambda_handler.__wrapped__.__wrapped__({}, {})
        assertApiResponse(self, 400, {'message': 'Event information text is greater than 1000 characters'},
                          actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.EVENTTEXT001, 'request_keys': list(request_body.keys())}),
            call(LogReference.EVENTTEXT006),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_not_called()


def request_info(request_body):
    return {
        'path_parameters': {'participant_id': '123'},
        'body': request_body,
        'session': SESSION
    }, 'POST', ''
