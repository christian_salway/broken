This lambda retrieves unprocessed pds records from the rejected table and writes them to a csv file.
This lambda should be triggered by cloudwatch event rule `process-rejected-records-trigger` that should run daily when enabled  
The lambda searches for records in the rejected dynamodb table which have the `PDS#` type and have the status `unprocessed`.  
It then formats those records into a csv file format and writes it to an S3 bucket.
Afterwards it updates the status of the records in the rejected dynamodb table.