from unittest.case import TestCase
from datetime import datetime, timezone
from mock import patch, call, Mock
from process_rejected_pds_records.log_references import LogReference

module = 'process_rejected_pds_records.process_rejected_pds_records'


class TestProcessRejectedPDSRecords(TestCase):
    mock_env_vars = {
        'REJECTED_PDS_RECORDS_BUCKET': 'rejected-pds-records-bucket',
    }

    @patch('os.environ', mock_env_vars)
    def setUp(self):
        import process_rejected_pds_records.process_rejected_pds_records as _process_rejected_pds_records
        self.process_rejected_pds_records = _process_rejected_pds_records

    @patch(f'{module}.get_unprocessed_rejected_records')
    @patch(f'{module}.create_csv_file_in_s3_bucket')
    @patch(f'{module}.update_rejected_record_statuses')
    def test_lambda(self, update_rejected_mock, create_csv_mock, get_rejected_mock):

        get_rejected_mock.return_value = ([
            '9565512593,"Failed PDS validation rules: CSMS-1",CSS_Update_20200528.dat,2021-07-02T10:12:28.516191+00:00',
            '9999999999,"Failed PDS validation rules: CSMS-1",CSS_Update_20200528.dat,2021-07-02T10:13:28.516191+00:00',
        ],
         [
            '48360fb1-9de8-43a7-be9a-9ce71354d6df',
            '1d163e6d-c5fe-429c-8ef8-ef4b1b991186',
        ])

        self.process_rejected_pds_records.lambda_handler.__wrapped__({}, {})
        get_rejected_mock.assert_called()
        create_csv_mock.assert_called_with([
            '9565512593,"Failed PDS validation rules: CSMS-1",CSS_Update_20200528.dat,2021-07-02T10:12:28.516191+00:00',
            '9999999999,"Failed PDS validation rules: CSMS-1",CSS_Update_20200528.dat,2021-07-02T10:13:28.516191+00:00',
        ])
        update_rejected_mock.assert_called_with([
            '48360fb1-9de8-43a7-be9a-9ce71354d6df',
            '1d163e6d-c5fe-429c-8ef8-ef4b1b991186',
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.query_rejected_records_by_status')
    @patch(f'{module}.get_rejection_reason')
    def test_getting_unprocessed_rejected(self, rejection_reason_mock, query_rejected_mock, log_mock):
        query_rejected_mock.return_value = [
            {
                'uuid': '48360fb1-9de8-43a7-be9a-9ce71354d6df', 'nhs_number': '9565512593',
                'rejection_type': 'PDS#2021-07-10', 'file_name': 'CSS_Update_20200528.dat',
                'created': '2021-07-02T10:12:28.516191+00:00',
                'metadata': {'failed_rules': ['CSMS-1'], 'hard_fail': True},
            },
            {
                'uuid': '1d163e6d-c5fe-429c-8ef8-ef4b1b991186', 'nhs_number': '9999999999',
                'rejection_type': 'PDS#2021-07-10', 'file_name': 'CSS_Update_20200528.dat',
                'created': '2021-07-02T10:13:28.516191+00:00',
                'metadata': {'failed_rules': ['CSMS-1'], 'hard_fail': True},
            }
        ]
        rejection_reason_mock.return_value = 'Failed PDS validation rules: CSMS-1'

        rejected_lines, rejected_keys = self.process_rejected_pds_records.get_unprocessed_rejected_records()
        expected_rejected_lines = [
            '9565512593,"Failed PDS validation rules: CSMS-1",CSS_Update_20200528.dat,2021-07-02T10:12:28.516191+00:00',
            '9999999999,"Failed PDS validation rules: CSMS-1",CSS_Update_20200528.dat,2021-07-02T10:13:28.516191+00:00',
        ]
        expected_rejected_keys = [
            {'uuid': '48360fb1-9de8-43a7-be9a-9ce71354d6df', 'rejection_type': 'PDS#2021-07-10'},
            {'uuid': '1d163e6d-c5fe-429c-8ef8-ef4b1b991186', 'rejection_type': 'PDS#2021-07-10'}
        ]

        query_rejected_mock.assert_called_with('UNPROCESSED', 'PDS')
        log_mock.assert_called_with({'log_reference': LogReference.PRCREJECTEDPDS0001})
        self.assertEqual(rejected_lines, expected_rejected_lines)
        self.assertEqual(rejected_keys, expected_rejected_keys)

    @patch(f'{module}.log')
    @patch(f'{module}.datetime')
    @patch(f'{module}.put_object')
    def test_writing_to_s3(self, put_object_mock, datetime_mock, log_mock):
        datetime_mock.now = Mock(return_value=datetime(2020, 7, 3, 6, 22, 23, 233, tzinfo=timezone.utc))
        rejected_lines = [
            '9565512593,"Failed PDS validation rules: CSMS-1",CSS_Update_20200528.dat,2021-07-02T10:12:28.516191+00:00',
            '9999999999,"Failed PDS validation rules: CSMS-1",CSS_Update_20200528.dat,2021-07-02T10:13:28.516191+00:00',
        ]
        self.process_rejected_pds_records.create_csv_file_in_s3_bucket(rejected_lines)

        expected_file_body = (
         'NHS Number,Rejection Reason,Origin Filename,Date\n'
         '9565512593,"Failed PDS validation rules: CSMS-1",CSS_Update_20200528.dat,2021-07-02T10:12:28.516191+00:00\n'
         '9999999999,"Failed PDS validation rules: CSMS-1",CSS_Update_20200528.dat,2021-07-02T10:13:28.516191+00:00'
        )
        expected_file_name = 'rejected_records/rejected_pds_records_20200703062223.csv'
        put_object_mock.assert_called_with('rejected-pds-records-bucket', expected_file_body, expected_file_name)
        log_mock.assert_called_with({'log_reference': LogReference.PRCREJECTEDPDS0003, 'file_name': expected_file_name})

    @patch(f'{module}.log')
    @patch(f'{module}.datetime')
    @patch(f'{module}.update_rejected_record')
    def test_updating_status(self, update_rejected_mock, datetime_mock, log_mock):
        datetime_mock.now = Mock(return_value=datetime(2020, 7, 3, 6, 22, 23, 233, tzinfo=timezone.utc))

        rejected_keys = [
            {'uuid': '48360fb1-9de8-43a7-be9a-9ce71354d6df', 'rejection_type': 'PDS#2021-07-10'},
            {'uuid': '1d163e6d-c5fe-429c-8ef8-ef4b1b991186', 'rejection_type': 'PDS#2021-07-10'},
        ]
        self.process_rejected_pds_records.update_rejected_record_statuses(rejected_keys)

        update_calls = [
            call(
                {'uuid': '48360fb1-9de8-43a7-be9a-9ce71354d6df', 'rejection_type': 'PDS#2021-07-10'},
                {'rejected_status': 'WRITTEN_TO_CSV', 'date_written_to_csv': '2020-07-03T06:22:23.000233+00:00'}
            ),
            call(
                {'uuid': '1d163e6d-c5fe-429c-8ef8-ef4b1b991186', 'rejection_type': 'PDS#2021-07-10'},
                {'rejected_status': 'WRITTEN_TO_CSV', 'date_written_to_csv': '2020-07-03T06:22:23.000233+00:00'}
            ),
        ]
        log_calls = [
            call({'log_reference': LogReference.PRCREJECTEDPDS0004}),
            call({'log_reference': LogReference.PRCREJECTEDPDS0004}),
        ]

        update_rejected_mock.assert_has_calls(update_calls)
        log_mock.assert_has_calls(log_calls)
