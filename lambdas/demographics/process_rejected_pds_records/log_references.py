import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    PRCREJECTEDPDS0001 = (logging.INFO, 'Retrieved rejected pds records')
    PRCREJECTEDPDS0002 = (logging.WARNING, 'PDS record does not have sufficient details')
    PRCREJECTEDPDS0003 = (logging.INFO, 'Written rejected PDS records CSV file to S3')
    PRCREJECTEDPDS0004 = (logging.INFO, 'Updated Status for a rejected record')
