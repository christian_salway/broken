from os import environ
from datetime import datetime, timezone

from common.log import log
from common.models.rejected import RejectedStatus
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.s3_utils import put_object
from common.utils.rejected_utils import (
    query_rejected_records_by_status, update_rejected_record,
    get_rejection_reason
)
from process_rejected_pds_records.log_references import LogReference

REJECTED_PDS_RECORDS_BUCKET = environ.get('REJECTED_PDS_RECORDS_BUCKET')
CSV_FIELD_DELIMITER = ','
CSV_RECORD_DELIMITER = '\n'


@lambda_entry_point
def lambda_handler(event, context):

    rejected_lines, rejected_keys = get_unprocessed_rejected_records()
    create_csv_file_in_s3_bucket(rejected_lines)
    update_rejected_record_statuses(rejected_keys)


def get_unprocessed_rejected_records():

    rejected = query_rejected_records_by_status(RejectedStatus.UNPROCESSED, 'PDS')
    log({'log_reference': LogReference.PRCREJECTEDPDS0001})
    rejected_lines = []
    rejected_keys = []
    for record in rejected:
        nhs_number = record.get('nhs_number')
        rejection_reason = get_rejection_reason(record)
        file_name = record.get('file_name')
        created = record.get('created')
        if not nhs_number or not rejection_reason or not file_name or not created:
            log({'log_reference': LogReference.PRCREJECTEDPDS0002})
            continue
        line = CSV_FIELD_DELIMITER.join([nhs_number, f'"{rejection_reason}"', file_name, created])
        rejected_lines.append(line)
        rejected_keys.append({'uuid': record['uuid'], 'rejection_type': record['rejection_type']})

    return rejected_lines, rejected_keys


def create_csv_file_in_s3_bucket(rejected_lines):

    timestamp = datetime.now(timezone.utc).strftime('%Y%m%d%H%M%S')
    header = CSV_FIELD_DELIMITER.join(['NHS Number', 'Rejection Reason', 'Origin Filename', 'Date'])
    file_body = CSV_RECORD_DELIMITER.join([header] + rejected_lines)

    file_name = f'rejected_records/rejected_pds_records_{timestamp}.csv'
    put_object(REJECTED_PDS_RECORDS_BUCKET, file_body, file_name)
    log({'log_reference': LogReference.PRCREJECTEDPDS0003, 'file_name': file_name})


def update_rejected_record_statuses(rejected_keys):
    timestamp = datetime.now(timezone.utc).isoformat()
    for key in rejected_keys:
        rejected_status = RejectedStatus.WRITTEN_TO_CSV
        date_written_to_csv = timestamp
        attributes_to_update = {'rejected_status': rejected_status, 'date_written_to_csv': date_written_to_csv}
        update_rejected_record(key, attributes_to_update)
        log({'log_reference': LogReference.PRCREJECTEDPDS0004})
