This lambda reads an array of filenames from the demographics-files-to-process SQS queue. 
It processes the first of these filenames and retrieves the actual file from the demographics-data bucket.
It splits each file line by line, and for each line sends a message to the import_demographics_raw sqs queue in batches of 10 messages.
Once a file has been fully processed it is moved to the demographics-data-completed bucket and removed from the demographics-data bucket.
If an error occurs during file processing then no more rows are processed, the file is moved to the demographics-data-failed bucket and removed from the demographics-data bucket.