import json
import os
from botocore.exceptions import ClientError
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils import chunk_list
from common.log import log, get_internal_id
from import_demographics.log_references import LogReference
from common.utils.s3_utils import (
    move_object_to_bucket_and_transition_to_infrequent_access_class, get_object_from_bucket
)
from common.utils.sqs_utils import (
    read_message_from_queue,
    send_new_message_batch,
)

BATCH_SIZE = 10
EXPECTED_NUMBER_OF_HEADER_KEYS = 7
FIELD_DELIMITER = '|'
SCREENING_COHORT = 'CSS'
FILE_TYPE = 'Update'


COMPLETED_BUCKET = os.environ.get('COMPLETED_BUCKET')
FAILED_BUCKET = os.environ.get('FAILED_BUCKET')
SQS_QUEUE_URL = os.environ.get('SQS_QUEUE_URL')
DATA_BUCKET = os.environ.get('DATA_BUCKET')


@lambda_entry_point
def lambda_handler(event, context):
    event_json = json.loads(read_message_from_queue(event))
    file_name = event_json.get('filename')
    if not file_name:
        log({'log_reference': LogReference.IMPORTDEMOG11})
        raise Exception(LogReference.IMPORTDEMOG11.message)

    process_file(file_name)


def process_file(file_name):
    log({'log_reference': LogReference.IMPORTDEMOG1})
    try:
        log({'log_reference': LogReference.IMPORTDEMOG2})

        demographics_file, _ = get_file_content(file_name)
        log({'log_reference': LogReference.IMPORTDEMOG3})

        send_messages_in_batches(demographics_file, file_name)

        log({'log_reference': LogReference.IMPORTDEMOG9, 'from_bucket': DATA_BUCKET, 'to_bucket': COMPLETED_BUCKET})
        destination_key = f"pds_updates/{file_name}"
        move_object_to_bucket_and_transition_to_infrequent_access_class(
            DATA_BUCKET,
            COMPLETED_BUCKET,
            file_name, destination_key)
        log({'log_reference': LogReference.IMPORTDEMOG10})
    except ClientError as e:
        exception_message = str(e)
        log({
            'log_reference': LogReference.IMPORTDEMOG8,
            'error': exception_message,
            'bucket': FAILED_BUCKET
        })

        if 'NoSuchKey' not in exception_message:
            move_object_to_bucket_and_transition_to_infrequent_access_class(
                DATA_BUCKET,
                FAILED_BUCKET,
                file_name
            )

    except Exception as e:
        log({
            'log_reference': LogReference.IMPORTDEMOG8,
            'error': str(e),
            'bucket': FAILED_BUCKET
        })

        move_object_to_bucket_and_transition_to_infrequent_access_class(
            DATA_BUCKET,
            FAILED_BUCKET,
            file_name)


def get_file_content(file_name):
    object_data = get_object_from_bucket(DATA_BUCKET, file_name)
    return [object_data.get('Body'), object_data.get('Metadata')]


def build_sqs_messages_from_file_lines(record_chunk, file_name, internal_id):

    messages = []
    for index, line in enumerate(record_chunk):
        message = {
            'Id': str(index),
            'MessageBody': json.dumps(
                {
                    'internal_id': internal_id,
                    'file_name': file_name,
                    'line_index': index,
                    'line': line
                }
            )
        }
        messages.append(message)

    return messages


def get_unique_records(demographics_file):
    unique_lines = {}
    demographics_file_stream = demographics_file.iter_lines()
    validate_demographics_file_header(next(demographics_file_stream))
    for line in demographics_file_stream:
        message_body = line.decode('UTF-8').split(FIELD_DELIMITER)
        nhs_number = message_body[3]
        unique_lines[nhs_number] = line.decode('UTF-8')

    return list(unique_lines.values())


def validate_demographics_file_header(header):
    header_keys = header.decode('utf-8').split(FIELD_DELIMITER)
    number_of_keys = len(header_keys)
    if number_of_keys != EXPECTED_NUMBER_OF_HEADER_KEYS:
        raise Exception('Unexpected number of keys in header')
    cohort = header_keys[1]
    file_type = header_keys[2]
    if cohort != SCREENING_COHORT or file_type != FILE_TYPE:
        raise Exception('Unrecognised format of header')


def send_messages_in_batches(demographics_file, file_name):

    unique_records = get_unique_records(demographics_file)
    internal_id = get_internal_id()
    records_chunks = chunk_list(unique_records, BATCH_SIZE)
    for chunk in records_chunks:
        message_chunk = build_sqs_messages_from_file_lines(chunk, file_name, internal_id)

        log({'log_reference': LogReference.IMPORTDEMOG4})
        response = send_new_message_batch(SQS_QUEUE_URL, message_chunk)

        sent_messages = response.get('Successful', [])

        for item in sent_messages:
            log({'log_reference': LogReference.IMPORTDEMOG5, 'index': item['Id']})

        failed_messages = response.get('Failed', [])

        if failed_messages:
            for item in failed_messages:
                log({'log_reference': LogReference.IMPORTDEMOG6, 'index': item['Id']})
            raise Exception('Unable to send batch to SQS')

    log({'log_reference': LogReference.IMPORTDEMOG7})
