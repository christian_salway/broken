import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    IMPORTDEMOG1 = (logging.INFO, 'Received event from queue')
    IMPORTDEMOG2 = (logging.INFO, 'Retrieving file from S3')
    IMPORTDEMOG3 = (logging.INFO, 'Retrieved file from S3')
    IMPORTDEMOG4 = (logging.INFO, 'Sending batch of messages to SQS')
    IMPORTDEMOG5 = (logging.INFO, 'Successfully sent message to SQS')
    IMPORTDEMOG6 = (logging.INFO, 'Failed to send message to SQS')
    IMPORTDEMOG7 = (logging.INFO, 'Successfully sent all messages to SQS')
    IMPORTDEMOG8 = (logging.ERROR, 'Failed to process file')
    IMPORTDEMOG9 = (logging.INFO, 'Copying file to bucket')
    IMPORTDEMOG10 = (logging.INFO, 'Copied file to bucket')
    IMPORTDEMOG11 = (logging.ERROR, 'Event does not contain a filename')
