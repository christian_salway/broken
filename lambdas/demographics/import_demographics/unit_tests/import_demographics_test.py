import json
from botocore.errorfactory import ClientError
from unittest import TestCase
from mock import patch, Mock, call, MagicMock
from import_demographics.log_references import LogReference
from common.test_mocks.mock_events import sqs_mock_event


@patch('import_demographics.import_demographics.validate_demographics_file_header', Mock())
class TestImportDemographicsBuildSQSMessages(TestCase):
    mock_env_vars = {
        'COMPLETED_BUCKET': 'completed_bucket',
        'FAILED_BUCKET': 'failed_bucket',
        'SQS_QUEUE_URL': 'sqs_queue_url',
        'DATA_BUCKET': 'data_bucket'
    }

    @patch('os.environ', mock_env_vars)
    @patch('boto3.client', MagicMock())
    @patch('boto3.resource', MagicMock())
    @patch('common.log.get_internal_id', Mock(return_value='internal_id'))
    def setUp(self):
        import import_demographics.import_demographics as _import_demographics
        self.import_demographics = _import_demographics
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = 'import_demographics_test'
        self.file_contents = 'file contents'.encode('utf-8')

    def test_build_messages_formats_the_messages_correctly(self):
        internal_id = 'internal_id'
        file_name = 'my_file_name'
        mock_demographics_file = ['message1', 'message2']
        expected_messages = [
            {
                'Id': '0',
                'MessageBody': json.dumps(
                    {
                        'internal_id': internal_id,
                        'file_name': file_name,
                        'line_index': 0,
                        'line': 'message1'
                    }
                )
            },
            {
                'Id': '1',
                'MessageBody': json.dumps(
                    {
                        'internal_id': internal_id,
                        'file_name': file_name,
                        'line_index': 1,
                        'line': 'message2'
                    }
                )
            }
        ]

        actual_messages = self.import_demographics.build_sqs_messages_from_file_lines(
            mock_demographics_file, file_name, internal_id)

        self.assertEqual(expected_messages, actual_messages)


class TestImportDemographicsValidateHeader(TestCase):
    mock_env_vars = {
        'COMPLETED_BUCKET': 'completed_bucket',
        'FAILED_BUCKET': 'failed_bucket',
        'SQS_QUEUE_URL': 'sqs_queue_url',
        'DATA_BUCKET': 'data_bucket'
    }

    @patch('os.environ', mock_env_vars)
    @patch('boto3.client', MagicMock())
    @patch('boto3.resource', MagicMock())
    @patch('common.log.get_internal_id', Mock(return_value='internal_id'))
    def setUp(self):
        import import_demographics.import_demographics as _import_demographics
        self.import_demographics = _import_demographics
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = 'import_demographics_test'
        self.file_contents = 'file contents'.encode('utf-8')

    def test_validate_header_does_not_raise_exception_for_valid_header(self):
        header = '0|CSS|Update|3|4|5|6'.encode('utf-8')
        try:
            self.import_demographics.validate_demographics_file_header(header)
        except Exception:
            self.fail('Validate demographics file header raised exception unexpectedly')

    def test_validate_header_raises_exception_for_incorrect_number_of_keys(self):
        header = 'key'.encode('utf-8')
        with self.assertRaises(Exception) as context:
            self.import_demographics.validate_demographics_file_header(header)
        self.assertEqual(context.exception.args[0], 'Unexpected number of keys in header')

    def test_validate_header_raises_exception_for_incorrect_cohort(self):
        header = '0|blah|Update|3|4|5|6'.encode('utf-8')
        with self.assertRaises(Exception) as context:
            self.import_demographics.validate_demographics_file_header(header)
        self.assertEqual(context.exception.args[0], 'Unrecognised format of header')

    def test_validate_header_raises_exception_for_incorrect_file_type(self):
        header = '0|CSS|blah|3|4|5|6'.encode('utf-8')
        with self.assertRaises(Exception) as context:
            self.import_demographics.validate_demographics_file_header(header)
        self.assertEqual(context.exception.args[0], 'Unrecognised format of header')


class TestImportDemographicsSendMessagesInBatches(TestCase):
    mock_env_vars = {
        'COMPLETED_BUCKET': 'completed_bucket',
        'FAILED_BUCKET': 'failed_bucket',
        'SQS_QUEUE_URL': 'sqs_queue_url',
        'DATA_BUCKET': 'data_bucket'
    }

    @patch('os.environ', mock_env_vars)
    @patch('boto3.client', MagicMock())
    @patch('boto3.resource', MagicMock())
    @patch('common.log.get_internal_id', Mock(return_value='internal_id'))
    def setUp(self):
        import import_demographics.import_demographics as _import_demographics
        self.import_demographics = _import_demographics

        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()

        self.context = Mock()
        self.context.function_name = 'import_demographics_test'
        self.file_contents = 'file contents'.encode('utf-8')

    def tearDown(self):
        self.log_patcher.stop()

    @patch('import_demographics.import_demographics.log')
    @patch('common.utils.sqs_utils._SQS_CLIENT')
    @patch('import_demographics.import_demographics.get_unique_records')
    @patch('import_demographics.import_demographics.build_sqs_messages_from_file_lines')
    def test_send_messages_in_batches_sends_1_batch_for_10_messages(self,
                                                                    build_messages_mock,
                                                                    unique_record_mock,
                                                                    sqs_utils_mock,
                                                                    logger_mock):

        messages = [{'Id': f'{i}', 'message': f'message_{i}', 'internal_id': 'internal_id'} for i in range(10)]

        sqs_utils_mock.send_message_batch.return_value = {'Successful': messages}

        mock_demographics_file = Mock()
        mock_demographics_file.iter_lines.return_value = iter([
            'header',
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        unique_records = ['message1'.encode('utf-8'), 'message2'.encode('utf-8')]
        unique_record_mock.return_value = unique_records
        build_messages_mock.return_value = messages

        self.import_demographics.send_messages_in_batches(mock_demographics_file, 'CSS_Update_20200527_1_3.dat')

        sqs_utils_mock.send_message_batch.assert_called_once_with(QueueUrl='sqs_queue_url', Entries=messages)

        logger_mock.assert_has_calls([
            call({'log_reference': LogReference.IMPORTDEMOG4}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '0'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '1'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '2'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '3'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '4'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '5'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '6'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '7'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '8'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '9'}),
            call({'log_reference': LogReference.IMPORTDEMOG7})
        ])

    @patch('import_demographics.import_demographics.log')
    @patch('common.utils.sqs_utils._SQS_CLIENT')
    @patch('import_demographics.import_demographics.get_unique_records')
    @patch('import_demographics.import_demographics.chunk_list')
    def test_send_messages_in_batches_sends_2_batches_for_12_messages(self,
                                                                      chunk_list_mock,
                                                                      unique_record_mock,
                                                                      sqs_utils_mock,
                                                                      logger_mock):
        messages = [{'Id': f'{i}', 'MessageBody': {
                                        'internal_id': 'internal_id',
                                        'file_name': 'CSS_Update_20200527_1_3.dat',
                                        'line_index': f'{i}',
                                        'line': f'message_{i}'
                                        }
                     } for i in range(0, 10)]
        messages2 = [{'Id': '0', 'MessageBody': {
                                        'internal_id': 'internal_id',
                                        'file_name': 'CSS_Update_20200527_1_3.dat',
                                        'line_index': 0,
                                        'line': {'message': 'message_10'}
                                        }
                      },
                     {'Id': '1', 'MessageBody': {
                                'internal_id': 'internal_id',
                                'file_name': 'CSS_Update_20200527_1_3.dat',
                                'line_index': 1,
                                'line': {'message': 'message_11'}
                                }
                      }]

        expected_messages1 = [{'Id': f'{i}', 'MessageBody': json.dumps({
                                        'internal_id': 'internal_id',
                                        'file_name': 'CSS_Update_20200527_1_3.dat',
                                        'line_index': i,
                                        'line': {'message': f'message_{i}'}
                                        })
                               } for i in range(0, 10)]

        expected_messages2 = [{'Id': '0', 'MessageBody': json.dumps({
                                        'internal_id': 'internal_id',
                                        'file_name': 'CSS_Update_20200527_1_3.dat',
                                        'line_index': 0,
                                        'line': {'message': 'message_10'}
                                        })
                               },
                              {'Id': '1', 'MessageBody': json.dumps({
                                        'internal_id': 'internal_id',
                                        'file_name': 'CSS_Update_20200527_1_3.dat',
                                        'line_index': 1,
                                        'line': {'message': 'message_11'}
                                        })
                               }]

        batch1_messages = [{'message': f'message_{i}'} for i in range(0, 10)]
        batch2_messages = [{'message': f'message_{i}'} for i in range(10, 12)]
        sqs_utils_mock.send_message_batch.side_effect = [{'Successful': messages}, {'Successful': messages2}]

        mock_demographics_file = Mock()
        mock_demographics_file.iter_lines.return_value = iter([
            'header',
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        unique_records = ['message1'.encode('utf-8'), 'message2'.encode('utf-8')]
        unique_record_mock.return_value = unique_records
        chunk_list_mock.return_value = [batch1_messages, batch2_messages]

        self.import_demographics.send_messages_in_batches(mock_demographics_file, 'CSS_Update_20200527_1_3.dat')

        sqs_utils_mock.send_message_batch.assert_has_calls([
            call(QueueUrl='sqs_queue_url', Entries=expected_messages1),
            call(QueueUrl='sqs_queue_url', Entries=expected_messages2)
        ])
        logger_mock.assert_has_calls([
            call({'log_reference': LogReference.IMPORTDEMOG4}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '0'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '1'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '2'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '3'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '4'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '5'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '6'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '7'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '8'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '9'}),
            call({'log_reference': LogReference.IMPORTDEMOG4}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '0'}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '1'}),
            call({'log_reference': LogReference.IMPORTDEMOG7})
        ])

    @patch('import_demographics.import_demographics.log')
    @patch('common.utils.sqs_utils._SQS_CLIENT')
    @patch('import_demographics.import_demographics.build_sqs_messages_from_file_lines')
    @patch('import_demographics.import_demographics.get_unique_records')
    def test_send_messages_in_batches_raises_exception_when_send_fails_messages(self,
                                                                                unique_record_mock,
                                                                                build_messages_mock,
                                                                                sqs_utils_mock,
                                                                                logger_mock):

        mock_demographics_file = Mock()
        mock_demographics_file.iter_lines.return_value = iter([
            'header'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        unique_records = ['message1'.encode('utf-8'), 'message2'.encode('utf-8')]
        unique_record_mock.return_value = unique_records

        messages = [{'Id': '1', 'message': 'message_1'}, {'Id': '2', 'message': 'message_2'}]
        build_messages_mock.return_value = messages

        sqs_utils_mock.send_message_batch.return_value = {
            'Successful': [{'Id': '1', 'message': 'message_1'}],
            'Failed': [{'Id': '2', 'message': 'message_2'}]
        }

        with self.assertRaises(Exception) as context:
            self.import_demographics.send_messages_in_batches(mock_demographics_file, 'CSS_Update_20200527_1_3.dat')

        self.assertEqual(context.exception.args[0], 'Unable to send batch to SQS')
        sqs_utils_mock.send_message_batch.assert_called_once_with(QueueUrl='sqs_queue_url', Entries=messages)
        logger_mock.assert_has_calls([
            call({'log_reference': LogReference.IMPORTDEMOG4}),
            call({'log_reference': LogReference.IMPORTDEMOG5, 'index': '1'}),
            call({'log_reference': LogReference.IMPORTDEMOG6, 'index': '2'})
        ])


class TestImportDemographics(TestCase):
    mock_env_vars = {
        'COMPLETED_BUCKET': 'completed_bucket',
        'FAILED_BUCKET': 'failed_bucket',
        'SQS_QUEUE_URL': 'sqs_queue_url',
        'DATA_BUCKET': 'data_bucket'
    }

    @patch('os.environ', mock_env_vars)
    @patch('boto3.client', MagicMock())
    @patch('boto3.resource', MagicMock())
    @patch('common.log.get_internal_id', Mock(return_value='internal_id'))
    def setUp(self):
        import import_demographics.import_demographics as _import_demographics
        self.import_demographics = _import_demographics
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = 'import_demographics_test'
        self.file_contents = 'file contents'.encode('utf-8')

    @patch('import_demographics.import_demographics.move_object_to_bucket_and_transition_to_infrequent_access_class')
    @patch('import_demographics.import_demographics.send_messages_in_batches')
    @patch('import_demographics.import_demographics.get_file_content')
    @patch('import_demographics.import_demographics.log')
    @patch('import_demographics.import_demographics.get_unique_records')
    @patch('import_demographics.import_demographics.chunk_list')
    def test_lines_put_on_sqs_and_file_moved_to_completed_bucket_for_successful_import(
            self, chunk_list_mock, unique_record_mock, log_mock, get_file_mock,
            send_messages_mock, move_file_mock):
        get_file_mock.return_value = ['file', {'internal_id': '1'}]

        message = {
            'filename': 'CSS_Update_20200527_1_3.dat'
        }
        self.import_demographics.lambda_handler(sqs_mock_event(message), self.context)

        get_file_mock.assert_called_once_with('CSS_Update_20200527_1_3.dat')
        send_messages_mock.assert_called_once_with('file', 'CSS_Update_20200527_1_3.dat')
        move_file_mock.assert_called_once_with(
            'data_bucket', 'completed_bucket',
            'CSS_Update_20200527_1_3.dat',
            'pds_updates/CSS_Update_20200527_1_3.dat')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.IMPORTDEMOG1}),
            call({'log_reference': LogReference.IMPORTDEMOG2}),
            call({'log_reference': LogReference.IMPORTDEMOG3}),
            call({
                'log_reference': LogReference.IMPORTDEMOG9,
                'from_bucket': 'data_bucket',
                'to_bucket': 'completed_bucket'}
            ),
            call({'log_reference': LogReference.IMPORTDEMOG10})
        ])

    @patch('import_demographics.import_demographics.move_object_to_bucket_and_transition_to_infrequent_access_class')
    @patch('import_demographics.import_demographics.send_messages_in_batches')
    @patch('import_demographics.import_demographics.build_sqs_messages_from_file_lines')
    @patch('import_demographics.import_demographics.get_file_content')
    @patch('import_demographics.import_demographics.log')
    @patch('import_demographics.import_demographics.get_unique_records')
    def test_file_moved_to_failed_bucket_if_could_not_be_fetched_from_s3(
            self, unique_record_mock, log_mock, get_file_mock, build_messages_mock, send_messages_mock, move_file_mock):
        mock_exception_message = 'mock failed file fetch exception message'
        get_file_mock.side_effect = Exception(mock_exception_message)

        mock_demographics_file = Mock()
        mock_demographics_file.iter_lines.return_value = iter([
            'header',
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        unique_records = ['message1'.encode('utf-8'), 'message2'.encode('utf-8')]
        unique_record_mock.return_value = unique_records

        messages = [{'Id': '1', 'message': 'message_1'}, {'Id': '2', 'message': 'message_2'}]
        build_messages_mock.return_value = messages

        message = {
            'filename': 'CSS_Update_20200527_1_3.dat'
        }
        self.import_demographics.lambda_handler(sqs_mock_event(message), self.context)

        get_file_mock.assert_called_once_with('CSS_Update_20200527_1_3.dat')
        build_messages_mock.assert_not_called()
        send_messages_mock.assert_not_called()
        move_file_mock.assert_called_once_with('data_bucket', 'failed_bucket', 'CSS_Update_20200527_1_3.dat')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.IMPORTDEMOG1}),
            call({'log_reference': LogReference.IMPORTDEMOG2}),
            call({
                'log_reference': LogReference.IMPORTDEMOG8,
                'error': mock_exception_message,
                'bucket': 'failed_bucket'
            })
        ])

    @patch('import_demographics.import_demographics.move_object_to_bucket_and_transition_to_infrequent_access_class')
    @patch('import_demographics.import_demographics.send_messages_in_batches')
    @patch('import_demographics.import_demographics.build_sqs_messages_from_file_lines')
    @patch('import_demographics.import_demographics.get_file_content')
    @patch('import_demographics.import_demographics.log')
    @patch('import_demographics.import_demographics.get_unique_records')
    def test_file_moved_to_failed_bucket_if_messages_failed_to_send(
            self, unique_record_mock, log_mock, get_file_mock, build_messages_mock, send_messages_mock, move_file_mock):

        get_file_mock.return_value = ['file', {'internal_id': '1'}]

        mock_demographics_file = Mock()
        mock_demographics_file.iter_lines.return_value = iter([
            'header',
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        unique_records = ['message1'.encode('utf-8'), 'message2'.encode('utf-8')]
        unique_record_mock.return_value = unique_records

        messages = [{'Id': '1', 'message': 'message_1'}, {'Id': '2', 'message': 'message_2'}]
        build_messages_mock.return_value = messages

        mock_exception_message = 'mock failed sending messages exception message'
        send_messages_mock.side_effect = Exception(mock_exception_message)

        message = {
            'filename': 'CSS_Update_20200527_1_3.dat'
        }
        self.import_demographics.lambda_handler(sqs_mock_event(message), self.context)

        get_file_mock.assert_called_once_with('CSS_Update_20200527_1_3.dat')

        send_messages_mock.assert_called_once_with('file', 'CSS_Update_20200527_1_3.dat')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.IMPORTDEMOG1}),
            call({'log_reference': LogReference.IMPORTDEMOG2}),
            call({'log_reference': LogReference.IMPORTDEMOG3}),
            call({
                'log_reference': LogReference.IMPORTDEMOG8,
                'error': mock_exception_message,
                'bucket': 'failed_bucket'
            })
        ])

    @patch('import_demographics.import_demographics.get_file_content')
    @patch('import_demographics.import_demographics.move_object_to_bucket_and_transition_to_infrequent_access_class')
    @patch('import_demographics.import_demographics.log')
    def test_404_from_s3_does_not_attempt_to_move_file(
        self,
        mock_log,
        mock_move_object,
        mock_get_file_content
    ):
        response = {
            'Error': {
                'Code': 'NoSuchKey'
            }
        }

        mock_get_file_content.side_effect = ClientError(response, 'move_file')

        expected_log_calls = [
            call({'log_reference': LogReference.IMPORTDEMOG1}),
            call({'log_reference': LogReference.IMPORTDEMOG2}),
            call(
                {
                    'log_reference': LogReference.IMPORTDEMOG8,
                    'error': 'An error occurred (NoSuchKey) when calling the move_file operation: Unknown',
                    'bucket': 'failed_bucket'
                }
            )
        ]

        message = {
            'filename': 'CSS_Update_20200527_1_3.dat'
        }

        self.import_demographics.lambda_handler(sqs_mock_event(message), self.context)

        mock_get_file_content.assert_called_with('CSS_Update_20200527_1_3.dat')
        mock_move_object.assert_not_called()
        mock_log.assert_has_calls(expected_log_calls)
