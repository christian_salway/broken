This lambda is to sit behind the screening_api API Gateway and serve responses for the front end

## Request
``` GET /participants ```

#### Required Query Params
Query parameters are passed in the lambda events dictionary 

``` nhs_number=alphanumeric ```


## Response
* Scenario: Results found

    Status code: 200 OK 

    Body: 
    ```
     {
          "participants": [
                {
                  "nhs_number": "3947297429",
                  "title": "DR",
                  "first_name": "TEST"
                  "middle_names": "WITH MIDDLE NAMES",
                  "last_name": "USER"
                }
          ]
      }
    ```

* Scenario: No results found

    Status code: 200 OK 

    Body: 
     ```
     {
          "participants": [
    
          ]
      }
     ```