from typing import Any, Dict


guaranteed_keys = [
    'nhs_number', 'participant_id', 'title', 'first_name', 'middle_names', 'last_name'
]
other_keys = ['gender', 'date_of_birth', 'address']


def convert_dynamodb_entity_to_participant_response(participant: Dict[str, Any]) -> Dict[str, Any]:
    converted_participant = {}

    for key in guaranteed_keys:
        converted_participant[key] = participant.get(key)

    for key in filter(lambda key: participant.get(key), other_keys):
        converted_participant[key] = participant.get(key)

    return converted_participant
