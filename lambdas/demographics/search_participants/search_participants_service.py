from typing import Any, Dict, List
from common.utils.participant_utils import (
    query_participants_by_nhs_number, query_participants_by_demographics, set_search_indexes)
from common.log import log
from search_participants.log_references import LogReference
from search_participants.participant_entity_to_participant_response_converter import (
    convert_dynamodb_entity_to_participant_response)
from common.utils.pds_fhir_utils import (get_pds_fhir_access_token, search_pds_fhir_by_demographics,
                                         search_pds_fhir_by_nhs_number)
from common.utils.postcode_utils import is_valid_postcode
from common.utils.transform_and_store_utils import remove_empty_values
from search_participants.search_strategies import (
    construct_dob_initial_full_postcode_search,
    construct_dob_initial_last_name_search,
    construct_dob_initial_first_name_search,
    construct_dob_initial_partial_postcode_search,
)
from search_participants.result_similarities import check_similarity


def search_participants_by_nhs_number(nhs_number: str) -> Dict[str, Any]:
    participants = query_participants_by_nhs_number(nhs_number)

    log({'log_reference': LogReference.SEARCHP0002})

    return convert_participants(participants)


def search_pds_by_nhs_number(nhs_number: str, session: Dict) -> Dict:
    log({'log_reference': LogReference.SEARCHP0013})
    session['pds_access_token'] = get_pds_fhir_access_token(session)
    return {'data': search_pds_fhir_by_nhs_number(nhs_number, session)}


def search_pds_by_demographics(demographics: Dict[str, Any], session: Dict, max_results=None) -> Dict:
    log({'log_reference': LogReference.SEARCHP0015})
    session['pds_access_token'] = get_pds_fhir_access_token(session)
    return {'data': search_pds_fhir_by_demographics(demographics, session, max_results)}


def search_participants_by_demographics(demographics: Dict[str, Any]) -> Dict[str, Any]:
    (
        index_name,
        condition_expression,
        filter_expression
     ) = get_search_index_and_condition_expression_to_query_via_demographic(demographics)

    participants = query_participants_by_demographics(
        index_name, condition_expression, filter_expression)
    log({'log_reference': LogReference.SEARCHP0002})

    [set_search_indexes(participant) for participant in participants]

    participants.sort(reverse=True, key=lambda x: check_similarity(demographics, x))

    return convert_participants(participants)


def convert_participants(participants: List[Dict[str, Any]]) -> Dict[str, Any]:
    if not participants:
        return {'data': [], "links": {"self": ""}}

    converted_participants = [convert_dynamodb_entity_to_participant_response(participant)
                              for participant in participants]

    return {'data': converted_participants, "links": {"self": ""}}


def get_search_index_and_condition_expression_to_query_via_demographic(demographics: Dict[str, Any]) -> Dict[str, Any]:

    set_search_indexes(demographics)
    demographics = remove_empty_values(demographics)

    if is_valid_postcode(demographics.get('sanitised_postcode')) and demographics.get('sanitised_last_name'):
        log({'log_reference': LogReference.SEARCHP0007})
        return construct_dob_initial_full_postcode_search(demographics)

    if demographics.get('sanitised_postcode') and demographics.get('sanitised_last_name'):
        log({'log_reference': LogReference.SEARCHP0008})
        return construct_dob_initial_partial_postcode_search(demographics)

    if demographics.get('sanitised_first_name') and demographics.get('sanitised_last_name'):
        log({'log_reference': LogReference.SEARCHP0009})
        return construct_dob_initial_first_name_search(demographics)

    if demographics.get('sanitised_last_name'):
        log({'log_reference': LogReference.SEARCHP0010})
        return construct_dob_initial_last_name_search(demographics)

    raise Exception('No valid search strategy found for query')
