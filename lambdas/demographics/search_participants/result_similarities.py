from search_participants.similarity_weights import DEMOGRAPHIC_SEARCH_SIMILARITY_WEIGHTS
from difflib import SequenceMatcher


def check_similarity(search_data: {}, participant: {}):

    total_weight = sum(DEMOGRAPHIC_SEARCH_SIMILARITY_WEIGHTS.values())

    weighted_similarity = sum([
        weight * compare_data(search_data.get(key, ''), participant.get(key, ''))
        for key, weight in DEMOGRAPHIC_SEARCH_SIMILARITY_WEIGHTS.items()
    ])
    return weighted_similarity / total_weight


def compare_data(search_criterion, database_entry):
    if search_criterion and database_entry:
        return SequenceMatcher(None, search_criterion, database_entry).ratio()
    else:
        return 0
