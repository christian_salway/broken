import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    SEARCHP0001 = (logging.INFO, 'Received request to get participants')
    SEARCHP0002 = (logging.INFO, 'Successfully queried dynamodb for participants')
    SEARCHP0003 = (logging.ERROR, 'Required post request parameters not supplied')
    SEARCHP0004 = (logging.ERROR, 'Error connecting to dynamodb')
    SEARCHP0005 = (logging.INFO, 'Running search via demographics function')
    SEARCHP0006 = (logging.INFO, 'Running search via nhs_number function')
    SEARCHP0007 = (logging.INFO, 'Searching for participants using full postcode, initial, and date of birth')
    SEARCHP0008 = (logging.INFO, 'Searching for participants using partial postcode, initial, and date of birth')
    SEARCHP0009 = (logging.INFO, 'Searching for participants using date of birth, initial, and first name')
    SEARCHP0010 = (logging.INFO, 'Searching for participants using date of birth and last name')
    SEARCHP0011 = (logging.ERROR, 'More than upper limit of results found')
    SEARCHP0012 = (logging.INFO, 'Adding additional filter to participant search')
    SEARCHP0013 = (logging.INFO, 'Searching PDS by NHS number')
    SEARCHP0014 = (logging.WARNING, 'Failed to search PDS as a live token could not be obtained')
    SEARCHP0015 = (logging.INFO, 'Searching PDS by Demographics')
