DEMOGRAPHIC_SEARCH_SIMILARITY_WEIGHTS = {
    'sanitised_first_name': 1,
    'sanitised_last_name': 1,
    'sanitised_postcode': 1,
    'date_of_birth': 1,
}
