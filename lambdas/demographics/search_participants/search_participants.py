import json
import os
from distutils.util import strtobool
from typing import Dict

from common.log import log
from search_participants.log_references import LogReference
from common.utils import json_return_message, json_return_object
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils.audit_utils import AuditActions, audit
from common.utils.session_utils import get_session_from_lambda_event
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.pds_fhir_utils import PdsTokenException
from search_participants.search_participants_service import (
    search_participants_by_nhs_number, search_participants_by_demographics,
    search_pds_by_nhs_number, search_pds_by_demographics)

MAX_SEARCH_RESULTS = int(os.environ.get('MAX_SEARCH_RESULTS'))
PDS_SIGNING_KEY = os.environ.get('PDS_SIGNING_KEY')
ENABLE_PDS_SEARCH = strtobool(os.environ.get('ENABLE_PDS_SEARCH', 'FALSE'))


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    event_data = get_api_request_info_from_event(event)[0]
    session = event_data['session']
    access_groups = session['access_groups']
    is_csas = access_groups['csas']

    log({'log_reference': LogReference.SEARCHP0001})

    try:
        request_body = json.loads(event['body'])
    except Exception:
        return build_bad_request_response('Missing or malformed request body.')

    search_type = request_body.get('search_type')
    if not search_type:
        return build_bad_request_response('Search type is missing from request body.')

    search_functions = {
        'nhs_number',
        'demographics'
    }

    if search_type not in search_functions:
        return build_bad_request_response(f'Unknown search type {search_type} supplied')

    search_parameters = request_body.get('data')
    if not search_parameters:
        return build_bad_request_response('Information to search by missing from request body.')

    log({'log_reference': LogReference.SEARCHP0002})
    try:
        if search_type == 'nhs_number':
            search_response = search_via_nhs_number(search_parameters, session, is_csas)
        elif search_type == 'demographics':
            search_response = search_via_demographics(search_parameters, session, is_csas)
    except PdsTokenException:
        log({'log_reference': LogReference.SEARCHP0014})
        return json_return_message(403, 'No valid PDS FHIR token found')

    if 'error' in search_response:
        return build_bad_request_response(search_response['error'])

    if len(search_response['data']) > MAX_SEARCH_RESULTS:
        log({'log_reference': LogReference.SEARCHP0011, 'MAX_SEARCH_RESULTS': MAX_SEARCH_RESULTS})
        return build_bad_request_response(
            "We couldn't find an exact match. Please refine your search.")

    participant_ids = [participant['participant_id'] for participant in search_response['data']]
    nhs_numbers = [participant['nhs_number'] for participant in search_response['data']]
    if participant_ids:
        session = get_session_from_lambda_event(event)
        audit(session=session,
              action=AuditActions.SEARCH_PARTICIPANTS,
              participant_ids=participant_ids,
              nhs_numbers=nhs_numbers)

    return json_return_object(200, search_response)


def search_via_demographics(request_data: Dict, session, is_csas):
    log({'log_reference': LogReference.SEARCHP0005})

    if not request_data.get('date_of_birth'):
        return {'error': 'Date of birth is required when searching by demographics.'}

    if not request_data.get('last_name'):
        return {'error': 'Last name is required when searching by demographics.'}

    demographic_data_request = convert_request_to_demographic_search(request_data)

    csms_participants = search_participants_by_demographics(demographic_data_request)
    found_participant_count = len(csms_participants['data'])

    if ENABLE_PDS_SEARCH and is_csas and found_participant_count < MAX_SEARCH_RESULTS:
        pds_participants = search_pds_by_demographics(
            demographic_data_request, session)

        if pds_participants:
            merge_and_remove_duplicate_participants(csms_participants['data'], pds_participants['data'])

    return csms_participants


def search_via_nhs_number(request_data, session, is_csas):
    log({'log_reference': LogReference.SEARCHP0006})

    nhs_number = request_data.get('nhs_number')
    if not nhs_number:
        return {
            'error': 'NHS number is missing from request.'
        }

    csms_participants = search_participants_by_nhs_number(nhs_number)

    if ENABLE_PDS_SEARCH and (not csms_participants or not csms_participants.get('data')) and is_csas:
        return search_pds_by_nhs_number(nhs_number, session)

    return csms_participants


def build_bad_request_response(error_message):
    log({'log_reference': LogReference.SEARCHP0003, 'Error': error_message})
    return json_return_message(400, error_message)


def convert_request_to_demographic_search(request: Dict) -> Dict:
    return {
        'last_name': request.get('last_name', ''),
        'date_of_birth': request['date_of_birth'],
        'first_name': request.get('first_name', ''),
        'address': {
            'postcode': request.get('postcode', ''),
        }
    }


def merge_and_remove_duplicate_participants(csms_participants, pds_participants):
    existing_nhs_numbers = [participant['nhs_number'] for participant in csms_participants]
    for pds_participant in pds_participants:
        if pds_participant['nhs_number'] not in existing_nhs_numbers:
            csms_participants.append(pds_participant)

        if len(csms_participants) >= MAX_SEARCH_RESULTS:
            return
