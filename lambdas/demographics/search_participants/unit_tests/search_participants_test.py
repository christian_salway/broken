import json
import os
from unittest.case import TestCase
from mock import patch, Mock
from common.utils.audit_utils import AuditActions
from common.utils.pds_fhir_utils import PdsTokenException
from common.test_assertions.api_assertions import assertApiResponse

with patch('boto3.resource') as resource_mock, patch(
    'os.environ',
    {
        'MAX_SEARCH_RESULTS': '10',
        'PDS_SIGNING_KEY': 'some_key',
        'ENABLE_PDS_SEARCH': 'TRUE'
    }
) as max_search_results_mock:
    from search_participants.search_participants import lambda_handler
    from search_participants.search_participants import merge_and_remove_duplicate_participants


@patch('search_participants.search_participants.audit', Mock())
@patch('search_participants.search_participants.get_session_from_lambda_event', Mock())
@patch('search_participants.search_participants.log', Mock())
class TestSearchParticipants(TestCase):

    def setUp(self):
        super(TestSearchParticipants, self).setUp()
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__
        self.session = [{'session': {'access_groups': {'csas': True}}}]
        self.sesson_not_csas = [{'session': {'access_groups': {'csas': False}}}]

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    def test_bad_request_returned_when_no_json_payload_supplied(self, service_mock, get_event_info_mock):

        lambda_proxy_payload = {
            'body': ''
        }

        get_event_info_mock.return_value = self.session

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 400, {'message': 'Missing or malformed request body.'}, actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_not_called()

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    def test_bad_request_returned_when_no_search_type_supplied(self, service_mock, get_event_info_mock):

        stringified_request = json.dumps({
            'data': {}
        })

        lambda_proxy_payload = {
            'body': stringified_request
        }

        get_event_info_mock.return_value = self.session

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 400, {'message': 'Search type is missing from request body.'}, actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_not_called()

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    def test_bad_request_returned_when_unknown_search_type_supplied(self, service_mock, get_event_info_mock):

        search_type = 'unknown_search_type'
        stringified_request = json.dumps({'search_type': search_type, 'data': {}})
        lambda_proxy_payload = {
            'body': stringified_request
        }

        get_event_info_mock.return_value = self.session

        expected_error = f'Unknown search type {search_type} supplied'

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 400, {'message': expected_error}, actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_not_called()

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    def test_bad_request_returned_when_no_dob_supplied_for_demographics_search(self, service_mock, get_event_info_mock):

        search_type = 'demographics'
        stringified_request = json.dumps({
            'search_type': search_type,
            'data': {
                'first_name': 'J',
                'last_name': 'B',
                'date_of_birth': '',
                'address': {'postcode': 'DE22 2DD'}
            }
        })

        lambda_proxy_payload = {
            'body': stringified_request
        }

        get_event_info_mock.return_value = self.session

        expected_error = 'Date of birth is required when searching by demographics.'

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 400, {'message': expected_error}, actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_not_called()

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    def test_bad_request_returned_when_no_last_name_supplied_for_demographics_search(self,
                                                                                     service_mock,
                                                                                     get_event_info_mock):
        search_type = 'demographics'
        stringified_request = json.dumps({
            'search_type': search_type,
            'data': {
                'first_name': 'J',
                'last_name': '',
                'date_of_birth': '1994-10-10',
                'address': {'postcode': 'DE22 2DD'}
            }
        })

        lambda_proxy_payload = {
            'body': stringified_request
        }

        get_event_info_mock.return_value = self.session

        expected_error = 'Last name is required when searching by demographics.'

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 400, {'message': expected_error}, actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_not_called()

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    def test_bad_request_returned_when_nhs_number_missing_from_data_object(self, service_mock, get_event_info_mock):
        stringified_request = json.dumps({
            'search_type': 'nhs_number',
            'data': {
                'nhs_number': ''
            }
        })
        lambda_proxy_payload = {
            'body': stringified_request
        }

        get_event_info_mock.return_value = self.session

        expected_error = 'NHS number is missing from request.'

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 400, {'message': expected_error}, actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_not_called()

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_demographics')
    @patch('search_participants.search_participants.search_pds_by_demographics')
    def test_bad_when_more_than_upper_limit_of_results_returned(self, search_pds_mock, service_mock,
                                                                get_event_info_mock):

        search_pds_mock.return_value = []
        stringified_request = json.dumps({
            'search_type': 'demographics',
            'data': {
                'first_name': 'J',
                'last_name': 'Brown',
                'date_of_birth': '01/01/1990',
                'address': {'postcode': 'DE22 2DD'}
            }
        })
        lambda_proxy_payload = {
            'body': stringified_request,
            'headers': None
        }

        get_event_info_mock.return_value = self.session

        service_mock.return_value = {'data': [{} for x in range(0, 11)]}

        expected_error = "We couldn't find an exact match. Please refine your search."

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        get_event_info_mock.assert_called_once()
        search_pds_mock.assert_not_called()

        assertApiResponse(self, 400, {'message': expected_error}, actual_response)

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    @patch('search_participants.search_participants.search_pds_by_nhs_number')
    def test_ok_and_return_empty_participant_list_when_nhs_number_returns_no_results(
        self,
        search_pds_mock,
        service_mock,
        get_event_info_mock
    ):
        stringified_request = json.dumps({
            'search_type': 'nhs_number',
            'data': {
                'nhs_number': 'does_not_exist'
            }
        })
        lambda_proxy_payload = {
            'body': stringified_request,
            'headers': None
        }

        get_event_info_mock.return_value = self.session

        service_mock.return_value = {"data": []}
        search_pds_mock.return_value = {"data": []}

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 200, {'data': []}, actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_called_with('does_not_exist')
        search_pds_mock.assert_called_with(
            'does_not_exist',
            {
                'access_groups': {
                    'csas': True
                }
            }
        )

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    @patch('search_participants.search_participants.search_pds_by_nhs_number')
    def test_skip_pds_search_if_not_csas(
        self,
        search_pds_mock,
        service_mock,
        get_event_info_mock
    ):
        stringified_request = json.dumps({
            'search_type': 'nhs_number',
            'data': {
                'nhs_number': 'does_not_exist'
            }
        })
        lambda_proxy_payload = {
            'body': stringified_request,
            'headers': None
        }

        get_event_info_mock.return_value = self.sesson_not_csas

        service_mock.return_value = {"data": []}

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 200, {'data': []}, actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_called_with('does_not_exist')
        search_pds_mock.assert_not_called()

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    @patch('search_participants.search_participants.search_pds_by_nhs_number')
    def test_fails_through_to_pds_search(
        self,
        search_pds_mock,
        service_mock,
        get_event_info_mock
    ):
        stringified_request = json.dumps({
            'search_type': 'nhs_number',
            'data': {
                'nhs_number': '1234567890'
            }
        })
        lambda_proxy_payload = {
            'body': stringified_request,
            'headers': None
        }

        found_patient = {
            'data': [
                {
                    'participant_id': 'PARTICIPANT1',
                    'is_pds': True,
                    'nhs_number': '1234567890'
                }
            ]
        }

        get_event_info_mock.return_value = self.session

        service_mock.return_value = {'data': []}
        search_pds_mock.return_value = found_patient

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 200, found_patient, actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_called_with('1234567890')
        search_pds_mock.assert_called_with(
            '1234567890',
            {
                'access_groups': {
                    'csas': True
                }
            }
        )

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    @patch('search_participants.search_participants.search_pds_by_nhs_number')
    def test_failure_to_refresh_pds_fhir_token_raises_403(
        self,
        search_pds_mock,
        service_mock,
        get_event_info_mock
    ):
        stringified_request = json.dumps({
            'search_type': 'nhs_number',
            'data': {
                'nhs_number': '1234567890'
            }
        })
        lambda_proxy_payload = {
            'body': stringified_request,
            'headers': None
        }

        get_event_info_mock.return_value = self.session

        service_mock.return_value = {'data': []}
        search_pds_mock.side_effect = PdsTokenException()

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 403, {'message': 'No valid PDS FHIR token found'}, actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_called_with('1234567890')
        search_pds_mock.assert_called_with(
            '1234567890',
            {
                'access_groups': {
                    'csas': True
                }
            }
        )

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_nhs_number')
    def test_ok_and_return_participants_list_when_request_to_view_participants_by_nhs_number(self,
                                                                                             service_mock,
                                                                                             get_event_info_mock):
        get_event_info_mock.return_value = self.session
        nhs_number = '12345678'
        stringified_request = json.dumps({
            'search_type': 'nhs_number',
            'data': {
                'nhs_number': nhs_number
            }
        })

        lambda_proxy_payload = {
            'body': stringified_request,
            'headers': None,
            'session': self.session
        }

        service_mock.return_value = {
            'data': [{'participant_id': '000', 'nhs_number': 'nhs_number', 'name': 'test name'}]}

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        assertApiResponse(self, 200,
                          {'data': [{'participant_id': '000', 'nhs_number': 'nhs_number', 'name': 'test name'}]},
                          actual_response)

        get_event_info_mock.assert_called_once()

        service_mock.assert_called_with(nhs_number)

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.search_participants_by_demographics')
    @patch('search_participants.search_participants.search_pds_by_demographics')
    def test_ok_and_return_participants_list_when_request_to_view_participants_by_demographics(self,
                                                                                               search_pds_demo_mock,
                                                                                               service_mock,
                                                                                               get_event_info_mock):

        get_event_info_mock.return_value = self.session
        current_path = os.path.dirname(os.path.realpath(__file__))
        file_name = 'demographic_searching.json'
        full_file_path = os.path.join(current_path, 'fixtures', file_name)
        requests = json.loads(open(full_file_path).read())

        for data in requests:
            request = data['mock-request']
            response = data['mock-response']
            converted_input = data['mock-called-with']

            lambda_proxy_payload = {
                'body': json.dumps(request),
                'headers': None
            }

            service_mock.return_value = response

            actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

            assertApiResponse(self, 200, response, actual_response)

            service_mock.assert_called_with(converted_input)
            search_pds_demo_mock.assert_called_with(converted_input, {'access_groups': {'csas': True}})

    def test_merge_and_remove_duplicate_participants(self):
        csms_participants = [{'nhs_number': '125'}]
        pds_participants = [
            {'nhs_number': '120'}, {'nhs_number': '121'}, {'nhs_number': '123'}, {'nhs_number': '124'},
            {'nhs_number': '125'}, {'nhs_number': '126'}, {'nhs_number': '127'}, {'nhs_number': '128'},
            {'nhs_number': '129'}, {'nhs_number': '130'}, {'nhs_number': '131'}, {'nhs_number': '132'},
        ]

        expected_csms_participants = [
            {'nhs_number': '125'}, {'nhs_number': '120'}, {'nhs_number': '121'}, {'nhs_number': '123'},
            {'nhs_number': '124'}, {'nhs_number': '126'}, {'nhs_number': '127'}, {'nhs_number': '128'},
            {'nhs_number': '129'}, {'nhs_number': '130'}
        ]
        merge_and_remove_duplicate_participants(csms_participants, pds_participants)

        self.assertEqual(expected_csms_participants, csms_participants)


@patch('search_participants.search_participants.log', Mock())
class TestAudit(TestCase):

    def setUp(self):
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__
        self.session = [{'session': {'access_groups': {'csas': True}}}]

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.get_session_from_lambda_event', Mock())
    @patch('search_participants.search_participants.audit')
    @patch('search_participants.search_participants.search_via_nhs_number')
    def test_lambda_handler_calls_audit_with_all_participants_from_search_result(self,
                                                                                 search_mock,
                                                                                 audit_mock,
                                                                                 get_event_info_mock):
        lambda_proxy_payload = {
            'body': json.dumps({"search_type": "nhs_number", "data": {'nhs_number': ''}}),
            'headers': None,
            'session': self.session
        }

        search_mock.return_value = {
            "data": [
                {
                    "participant_id": "001",
                    "nhs_number": "12345",
                    "name": "John Brown"
                },
                {
                    "participant_id": "002",
                    "nhs_number": "12346",
                    "name": "J Brown"
                }
            ]
        }

        get_event_info_mock.return_value = self.session

        self.unwrapped_handler(lambda_proxy_payload, {})
        _, kwargs = audit_mock.call_args

        get_event_info_mock.assert_called_once()

        self.assertEqual(kwargs['participant_ids'], ['001', '002'])

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.get_session_from_lambda_event', Mock())
    @patch('search_participants.search_participants.audit')
    @patch('search_participants.search_participants.search_via_nhs_number')
    def test_lambda_calls_audit_with_the_correct_action(self, search_mock, audit_mock, get_event_info_mock):
        lambda_proxy_payload = {
            'body': json.dumps({"search_type": "nhs_number", "data": {'nhs_number': ''}}),
            'headers': None
        }

        search_mock.return_value = {
            "data": [
                {
                    "participant_id": "001",
                    "nhs_number": "12345",
                    "name": "John Brown"
                },
                {
                    "participant_id": "002",
                    "nhs_number": "12346",
                    "name": "J Brown"
                }
            ]
        }

        get_event_info_mock.return_value = self.session

        self.unwrapped_handler(lambda_proxy_payload, {})
        _, kwargs = audit_mock.call_args

        get_event_info_mock.assert_called_once()

        self.assertEqual(kwargs['action'], AuditActions.SEARCH_PARTICIPANTS)

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.get_session_from_lambda_event')
    @patch('search_participants.search_participants.audit')
    @patch('search_participants.search_participants.search_via_nhs_number')
    def test_lambda_handler_calls_passes_session_correctly_to_audit(self,
                                                                    search_mock,
                                                                    audit_mock,
                                                                    get_session_mock,
                                                                    get_event_info_mock):
        session = {'session_token': 'test_session_token', 'user_data': 'some_data'}
        get_session_mock.return_value = session

        lambda_proxy_payload = {
            'body': json.dumps({"search_type": "nhs_number", "data": {'nhs_number': ''}}),
            'headers': None,
            'requestContext': {'authorizer': {'session': '{"session_key": "session_value"}'}}
        }

        search_mock.return_value = {
            "data": [
                {
                    "participant_id": "001",
                    "nhs_number": "12345",
                    "name": "John Brown"
                },
                {
                    "participant_id": "002",
                    "nhs_number": "12346",
                    "name": "J Brown"
                }
            ]
        }

        get_event_info_mock.return_value = self.session

        self.unwrapped_handler(lambda_proxy_payload, {})
        get_event_info_mock.assert_called_once()
        get_session_mock.assert_called_with(lambda_proxy_payload)
        _, kwargs = audit_mock.call_args

        get_event_info_mock.assert_called_once()

        self.assertEqual(kwargs['session'], session)

    @patch('search_participants.search_participants.get_api_request_info_from_event')
    @patch('search_participants.search_participants.get_session_from_lambda_event')
    @patch('search_participants.search_participants.audit')
    @patch('search_participants.search_participants.search_via_nhs_number')
    def test_lambda_handler_does_not_call_audit_when_no_participants(self,
                                                                     search_mock,
                                                                     audit_mock,
                                                                     get_session_mock,
                                                                     get_event_info_mock):
        lambda_proxy_payload = {
            'body': json.dumps({"search_type": "nhs_number", "data": {'nhs_number': ''}}),
            'headers': None
        }

        search_mock.return_value = {
            "data": []
        }

        get_event_info_mock.return_value = self.session

        self.unwrapped_handler(lambda_proxy_payload, {})

        get_event_info_mock.assert_called_once()
        get_session_mock.assert_not_called()
        audit_mock.assert_not_called()
