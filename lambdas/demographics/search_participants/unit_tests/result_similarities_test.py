from unittest.case import TestCase
from search_participants.result_similarities import check_similarity


def create_record(participant):
    return {
        'sanitised_first_name': participant[0],
        'sanitised_last': participant[1],
        'date_of_birth': participant[2],
        'sanitised_postcode': participant[3]
    }


class TestSearchParticipantsService(TestCase):

    def test_check_similarity(self):
        # Given
        query = {
            'sanitised_first_name': 'ANNA',
            'sanitised_last_name': 'JONES',
            'date_of_birth': '1996-01-01',
            'sanitised_postcode': 'M139DB'
        }
        participant_records = map(create_record, [
            ['A', 'B', '1996-01-01', 'C'],
            ['AN', 'BU', '1996-01-01', 'M'],
            ['AN', 'BU', '1996-01-01', ''],
            ['ANNABELLE', 'BUCHANNON', '1996-01-01', 'AA11AA'],
            ['ANNA', 'JONES', '1996-01-01', 'M1'],
            ['ANNA', 'JONES', '1996-01-01', 'M139DB']
        ])
        expected_results = [0.35, 0.4880952, 0.41666666, 0.4455128, 0.625, 0.75]

        for key, record in enumerate(participant_records):
            # When
            similarity = check_similarity(query, record)

            # Then
            self.assertAlmostEqual(similarity, expected_results[key])
