from unittest.case import TestCase
from mock import patch, call
from boto3.dynamodb.conditions import Key, Attr

from search_participants.log_references import LogReference
from search_participants.search_participants_service import search_pds_by_demographics, search_pds_by_nhs_number
with patch('boto3.resource') as resource_mock:
    from common.utils.participant_utils import set_search_indexes
    from search_participants.search_participants_service import (
        search_participants_by_nhs_number, search_participants_by_demographics,
        get_search_index_and_condition_expression_to_query_via_demographic)


class TestSearchParticipantsService(TestCase):

    CLOSE_MATCH = {
        'nhs_number': '12345678',
        'title': 'Mr',
        'first_name': 'John',
        'middle_names': 'E',
        'last_name': 'Brown',
        'postcode': 'DE11111'
    }

    DISTANT_MATCH = {
        'nhs_number': '12345678',
        'title': 'Mr',
        'first_name': 'John',
        'middle_names': 'E',
        'last_name': 'Brown',
    }

    VERY_DISTANT_MATCH = {
        'nhs_number': '12345678',
        'title': 'Sir',
        'first_name': 'Johnathon',
        'middle_names': 'Ethelbert',
        'last_name': 'Brownington',
    }

    @patch('search_participants.search_participants_service.log')
    @patch('search_participants.search_participants_service.convert_dynamodb_entity_to_participant_response')
    @patch('search_participants.search_participants_service.query_participants_by_nhs_number')
    def test_empty_participant_list_returned_when_searching_via_unknown_nhs_number(self,
                                                                                   query_by_nhs_number_mock,
                                                                                   converter_mock,
                                                                                   log_mock):

        nhs_number = '12345678'

        query_by_nhs_number_mock(nhs_number)

        expected_response = {'data': [], 'links': {'self': ''}}

        actual_response = search_participants_by_nhs_number(nhs_number)

        self.assertDictEqual(expected_response, actual_response)

        query_by_nhs_number_mock.assert_called_with(nhs_number)

        converter_mock.assert_not_called()

    @patch('search_participants.search_participants_service.log')
    @patch('search_participants.search_participants_service.convert_dynamodb_entity_to_participant_response')
    @patch('search_participants.search_participants_service.query_participants_by_nhs_number')
    def test_participant_list_returned_when_searching_via_nhs_number_return_all_fields(self,
                                                                                       query_by_nhs_number_mock,
                                                                                       converter_mock,
                                                                                       log_mock):
        nhs_number = '12345678'

        query_by_nhs_number_mock.return_value = {'Items': [{
            'nhs_number': nhs_number,
            'title': 'MR',
            'first_name': 'TEST',
            'middle_names': 'DYNAMODB',
            'last_name': 'ITEM'
        }]}

        converter_mock.return_value = {
            'nhs_number': '12345678',
            'title': 'MR',
            'first_name': 'TEST',
            'middle_names': 'DYNAMODB',
            'last_name': 'ITEM'
        }

        expected_response = {
            "links": {
                "self": ""
            },
            'data': [
                {
                    'nhs_number': '12345678',
                    'title': 'MR',
                    'first_name': 'TEST',
                    'middle_names': 'DYNAMODB',
                    'last_name': 'ITEM'
                }
            ]
        }

        actual_response = search_participants_by_nhs_number(nhs_number)

        self.assertDictEqual(expected_response, actual_response)

        query_by_nhs_number_mock.assert_called_with(nhs_number)

        for value in query_by_nhs_number_mock.return_value:
            converter_mock.assert_called_with(value)

    @patch('search_participants.search_participants_service.log')
    @patch('search_participants.search_participants_service.convert_dynamodb_entity_to_participant_response')
    @patch('search_participants.search_participants_service.query_participants_by_nhs_number')
    def test_participant_list_returned_when_searching_via_nhs_number_return_missing_fields(self,
                                                                                           query_by_nhs_number_mock,
                                                                                           converter_mock,
                                                                                           log_mock):
        nhs_number = '12345678'

        query_by_nhs_number_mock.return_value = {'Items': [{
            'nhs_number': nhs_number,
        }]}

        converter_mock.return_value = {
            'nhs_number': '12345678',
            'title': None,
            'first_name': None,
            'middle_names': None,
            'last_name': None
        }

        expected_response = {
            "links": {
                "self": ""
            },
            'data': [
                {
                    'nhs_number': '12345678',
                    'title': None,
                    'first_name': None,
                    'middle_names': None,
                    'last_name': None
                }
            ]
        }

        actual_response = search_participants_by_nhs_number(nhs_number)

        self.assertDictEqual(expected_response, actual_response)

        query_by_nhs_number_mock.assert_called_with(nhs_number)

        for value in query_by_nhs_number_mock.return_value:
            converter_mock.assert_called_with(value)

    @patch('search_participants.search_participants_service.log')
    @patch('search_participants.search_participants_service.convert_dynamodb_entity_to_participant_response')
    @patch('search_participants.search_participants_service.check_similarity')
    @patch('search_participants.search_participants_service.query_participants_by_demographics')
    @patch('search_participants.search_participants_service.'
           'get_search_index_and_condition_expression_to_query_via_demographic')
    def test_participant_list_returned_sorted_when_searching_via_dob_initial_postcode_index(self,
                                                                                            search_builder_mock,
                                                                                            query_by_demographics_mock,
                                                                                            check_similarity_mock,
                                                                                            converter_mock,
                                                                                            log_mock):
        demographics = {
            'first_name': '',
            'last_name': 'Brown',
            'date_of_birth': '01/01/1990',
            'address': {'postcode': 'DE11111'},
        }
        set_search_indexes(demographics)

        index_name = 'date-of-birth-initial-partial-postcode'
        condition_expression = (
            Key('date_of_birth_and_initial').eq(demographics['date_of_birth'] + 'B') &
            Key('sanitised_postcode').begins_with(demographics['sanitised_postcode']))
        filter_expression = [Attr('one').begins_with('two')]

        search_builder_mock.return_value = (index_name, condition_expression, filter_expression)

        query_by_demographics_mock.return_value = [self.DISTANT_MATCH, self.CLOSE_MATCH]

        check_similarity_mock.side_effect = [0.6, 0.9]

        converter_mock.side_effect = lambda x: x

        expected_response = {
            "links": {
                "self": ""
            },
            'data': [self.CLOSE_MATCH, self.DISTANT_MATCH]
        }

        actual_response = search_participants_by_demographics(demographics)

        self.assertDictEqual(expected_response, actual_response)

        query_by_demographics_mock.assert_called_with(index_name, condition_expression, filter_expression)

        converter_mock.assert_has_calls([
            call(self.CLOSE_MATCH),
            call(self.DISTANT_MATCH)
        ])

    @patch('search_participants.search_participants_service.log')
    @patch('search_participants.search_participants_service.convert_dynamodb_entity_to_participant_response')
    @patch('search_participants.search_participants_service.check_similarity')
    @patch('search_participants.search_participants_service.query_participants_by_demographics')
    @patch('search_participants.search_participants_service.'
           'get_search_index_and_condition_expression_to_query_via_demographic')
    def test_participant_list_returned_sorted_when_searching_via_dob_lastname_index(self,
                                                                                    search_builder_mock,
                                                                                    query_by_demographics_mock,
                                                                                    check_similarity_mock,
                                                                                    converter_mock,
                                                                                    log_mock):
        demographics = {
            'first_name': '',
            'last_name': 'Brown',
            'date_of_birth': '01/01/1990',
            'address': {'postcode': ''}
        }
        set_search_indexes(demographics)

        index_name = 'date-of-birth-initial-last-name'
        condition_expression = (
            Key('date_of_birth').eq(demographics['date_of_birth'] + 'B') &
            Key('last_name').begins_with(demographics['last_name']))
        filter_expression = [Attr('one').begins_with('two')]

        search_builder_mock.return_value = (index_name, condition_expression, filter_expression)

        query_by_demographics_mock.return_value = [
            self.DISTANT_MATCH, self.VERY_DISTANT_MATCH, self.CLOSE_MATCH
        ]

        check_similarity_mock.side_effect = [0.5, 0.3, 0.8]

        converter_mock.side_effect = lambda x: x

        expected_response = {
            "links": {
                "self": ""
            },
            'data': [
                self.CLOSE_MATCH,
                self.DISTANT_MATCH,
                self.VERY_DISTANT_MATCH
            ]
        }

        actual_response = search_participants_by_demographics(demographics)

        self.assertDictEqual(expected_response, actual_response)

        query_by_demographics_mock.assert_called_with(index_name, condition_expression, filter_expression)

        converter_mock.assert_has_calls([
            call(self.CLOSE_MATCH),
            call(self.DISTANT_MATCH),
            call(self.VERY_DISTANT_MATCH)
        ])

    @patch('search_participants.search_participants_service.log')
    @patch('search_participants.search_participants_service.convert_dynamodb_entity_to_participant_response')
    @patch('search_participants.search_participants_service.check_similarity')
    @patch('search_participants.search_participants_service.query_participants_by_demographics')
    @patch('search_participants.search_participants_service.'
           'get_search_index_and_condition_expression_to_query_via_demographic')
    def test_participant_list_returned_sorted_when_searching_via_dob_firstname_index(self,
                                                                                     search_builder_mock,
                                                                                     query_by_demographics_mock,
                                                                                     check_similarity_mock,
                                                                                     converter_mock,
                                                                                     log_mock):
        demographics = {
            'first_name': 'John',
            'last_name': 'B',
            'date_of_birth': '01/01/1990',
            'address': {'postcode': ''}
        }

        index_name = 'date-of-birth-firstname'
        condition_expression = (
            Key('date_of_birth').eq(demographics['date_of_birth']) &
            Key('first_name').begins_with(demographics['first_name']))
        filter_expression = [Attr('one').begins_with('two')]

        search_builder_mock.return_value = (index_name, condition_expression, filter_expression)

        query_by_demographics_mock.return_value = [
            self.DISTANT_MATCH, self.VERY_DISTANT_MATCH
        ]

        check_similarity_mock.side_effect = [0.5, 0.3]

        converter_mock.side_effect = lambda x: x

        expected_response = {
            "links": {
                "self": ""
            },
            'data': [
                self.DISTANT_MATCH,
                self.VERY_DISTANT_MATCH
            ]
        }

        actual_response = search_participants_by_demographics(demographics)

        self.assertDictEqual(expected_response, actual_response)

        query_by_demographics_mock.assert_called_with(index_name, condition_expression, filter_expression)

        converter_mock.assert_has_calls([
            call(self.DISTANT_MATCH),
            call(self.VERY_DISTANT_MATCH)
        ])

    @patch('search_participants.search_participants_service.log')
    @patch('search_participants.search_participants_service.construct_dob_initial_full_postcode_search')
    @patch('search_participants.search_participants_service.is_valid_postcode')
    def test_get_search_index_and_condition_expression_returns_correct_values_given_valid_postcode_and_initial(
            self,
            is_valid_postcode_mock,
            search_constructor_mock,
            log_mock):

        demographics = {
            'first_name': 'myfirstname',
            'last_name': 'L',
            'date_of_birth': '01/01/1990',
            'address': {'postcode': 'VALID'}
        }

        expected_result = ('my index', 'my conditions', 'my filter')
        is_valid_postcode_mock.return_value = True
        search_constructor_mock.return_value = expected_result

        result = get_search_index_and_condition_expression_to_query_via_demographic(demographics)

        self.assertEqual(expected_result, result)

        search_constructor_mock.assert_called_with(demographics)
        expected_log_calls = [call({'log_reference': LogReference.SEARCHP0007})]
        log_mock.assert_has_calls(expected_log_calls)

    @patch('search_participants.search_participants_service.log')
    @patch('search_participants.search_participants_service.construct_dob_initial_partial_postcode_search')
    @patch('search_participants.search_participants_service.is_valid_postcode')
    def test_get_search_index_and_condition_expression_returns_correct_values_given_partial_postcode_and_initial(
            self,
            is_valid_postcode_mock,
            search_constructor_mock,
            log_mock):

        demographics = {
            'first_name': '',
            'last_name': 'L',
            'date_of_birth': '01/01/1990',
            'address': {'postcode': 'VALID'}
        }
        expected_demographics = {
            'last_name': 'L',
            'date_of_birth': '01/01/1990',
            'address': {'postcode': 'VALID'},
            'sanitised_postcode': 'VALID',
            'sanitised_last_name': 'L',
            'date_of_birth_and_initial_and_postcode': '01/01/1990LVALID',
            'date_of_birth_and_initial': '01/01/1990L'
        }

        is_valid_postcode_mock.return_value = False
        search_constructor_mock.return_value = ('my index', 'my conditions', 'my filter')

        index_name, condition_expression, filter_expression = \
            get_search_index_and_condition_expression_to_query_via_demographic(demographics)

        self.assertEqual('my index', index_name)
        self.assertEqual('my conditions', condition_expression)
        self.assertEqual('my filter', filter_expression)

        search_constructor_mock.assert_called_with(expected_demographics)
        expected_log_calls = [call({'log_reference': LogReference.SEARCHP0008})]
        log_mock.assert_has_calls(expected_log_calls)

    @patch('search_participants.search_participants_service.log')
    @patch('search_participants.search_strategies.construct_filter_expression')
    @patch('search_participants.search_participants_service.construct_dob_initial_first_name_search')
    @patch('search_participants.search_participants_service.is_valid_postcode')
    def test_get_search_index_and_condition_expression_returns_correct_values_given_first_name_and_initial(
            self,
            is_valid_postcode_mock,
            search_constructor_mock,
            mock_filter_constructor,
            log_mock):

        demographics = {
            'first_name': 'firsty',
            'last_name': 'L',
            'date_of_birth': '01/01/1990',
        }
        expected_demographics = {
            'first_name': 'firsty',
            'last_name': 'L',
            'date_of_birth': '01/01/1990',
            'sanitised_first_name': 'FIRSTY',
            'sanitised_last_name': 'L',
            'date_of_birth_and_initial': '01/01/1990L'
        }

        is_valid_postcode_mock.return_value = False
        search_constructor_mock.return_value = ('my index', 'my conditions', 'my filter')

        index_name, condition_expression, filter_expression = \
            get_search_index_and_condition_expression_to_query_via_demographic(demographics)

        self.assertEqual('my index', index_name)
        self.assertEqual('my conditions', condition_expression)
        self.assertEqual('my filter', filter_expression)

        search_constructor_mock.assert_called_with(expected_demographics)
        expected_log_calls = [call({'log_reference': LogReference.SEARCHP0009})]
        log_mock.assert_has_calls(expected_log_calls)

    @patch('search_participants.search_participants_service.log')
    @patch('search_participants.search_participants_service.construct_dob_initial_last_name_search')
    @patch('search_participants.search_participants_service.is_valid_postcode')
    def test_get_search_index_and_condition_expression_returns_correct_values_given_last_name(
            self,
            is_valid_postcode_mock,
            search_constructor_mock,
            log_mock):

        demographics = {
            'last_name': 'verylong',
            'date_of_birth': '01/01/1990',
            'address': {},
        }
        expected_demographics = {
            'last_name': 'verylong',
            'date_of_birth': '01/01/1990',
            'sanitised_last_name': 'VERYLONG',
            'date_of_birth_and_initial': '01/01/1990V'
        }

        is_valid_postcode_mock.return_value = False
        search_constructor_mock.return_value = ('my index', 'my conditions', 'my filter')

        index_name, condition_expression, filter_expression = \
            get_search_index_and_condition_expression_to_query_via_demographic(demographics)

        self.assertEqual('my index', index_name)
        self.assertEqual('my conditions', condition_expression)
        self.assertEqual('my filter', filter_expression)

        search_constructor_mock.assert_called_with(expected_demographics)
        expected_log_calls = [call({'log_reference': LogReference.SEARCHP0010})]
        log_mock.assert_has_calls(expected_log_calls)

    @patch('search_participants.search_participants_service.search_pds_fhir_by_nhs_number')
    @patch('search_participants.search_participants_service.get_pds_fhir_access_token')
    @patch('search_participants.search_participants_service.log')
    def test_search_pds_by_nhs_number(
        self,
        log_mock,
        get_pds_fhir_access_token_mock,
        search_pds_fhir_by_nhs_number_mock
    ):
        get_pds_fhir_access_token_mock.return_value = 'TOKEN123'
        search_pds_fhir_by_nhs_number_mock.return_value = ['PATIENT1']
        expected_result = {'data': ['PATIENT1']}

        actual_result = search_pds_by_nhs_number('1234567890', {})

        self.assertDictEqual(actual_result, expected_result)

    @patch('search_participants.search_participants_service.search_pds_fhir_by_demographics')
    @patch('search_participants.search_participants_service.get_pds_fhir_access_token')
    @patch('search_participants.search_participants_service.log')
    def test_search_pds_by_nhs_demographics(
        self,
        log_mock,
        get_pds_fhir_access_token_mock,
        search_pds_fhir_by_demographics_mock
    ):
        get_pds_fhir_access_token_mock.return_value = 'TOKEN123'
        search_pds_fhir_by_demographics_mock.return_value = ['PATIENT1']
        expected_result = {'data': ['PATIENT1']}

        actual_result = search_pds_by_demographics(
            {'first_name': 'J', 'last_name': 'Brown', 'date_of_birth': '01/01/1990', 'postcode': 'DE22 2DD'}, {}, 2)

        search_pds_fhir_by_demographics_mock.assert_called_with(
            {'first_name': 'J', 'last_name': 'Brown', 'date_of_birth': '01/01/1990', 'postcode': 'DE22 2DD'},
            {'pds_access_token': 'TOKEN123'},
            2
        )
        self.assertDictEqual(actual_result, expected_result)
