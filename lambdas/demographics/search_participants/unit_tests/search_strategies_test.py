from unittest.case import TestCase

from boto3.dynamodb.conditions import Attr, Key
from mock import patch, call

from search_participants.log_references import LogReference
from search_participants.search_strategies import (
    construct_filter_expression,
    construct_dob_initial_full_postcode_search,
    construct_dob_initial_last_name_search,
    construct_dob_initial_first_name_search,
    construct_dob_initial_partial_postcode_search)


@patch('search_participants.search_strategies.log')
class TestSearchParticipants(TestCase):

    @patch('search_participants.search_strategies.construct_filter_expression')
    def test_construct_dob_initial_full_postcode_search_returns_correct_search_parameters(
            self, mock_filter_constructor, mock_logger):
        demographics = {
            'date_of_birth': 'birthday',
            'sanitised_first_name': 'NAMEY',
            'sanitised_last_name': 'NAMERSON',
            'sanitised_postcode': 'CODE'}
        expected_condition_expression = Key('date_of_birth_and_initial_and_postcode').eq('birthdayNCODE')\
            & Key('sanitised_first_name').begins_with('NAMEY')
        mock_filter_constructor.return_value = ['a list of filters']

        index_name, condition_expression, filter_expression = construct_dob_initial_full_postcode_search(demographics)

        self.assertEqual('date-of-birth-initial-full-postcode', index_name)
        self.assertEqual(expected_condition_expression, condition_expression)
        self.assertEqual(['a list of filters'], filter_expression)

    @patch('search_participants.search_strategies.construct_filter_expression')
    def test_construct_dob_initial_last_name_search(self, mock_filter_constructor, mock_logger):
        demographics = {
            'date_of_birth': 'birthday',
            'sanitised_first_name': 'NAMEY',
            'sanitised_last_name': 'NAMERSON',
            'sanitised_postcode': 'CODE'}
        expected_condition_expression = Key('date_of_birth_and_initial').eq('birthday' + 'N') & \
            Key('sanitised_last_name').begins_with('NAMERSON')
        mock_filter_constructor.return_value = ['a list of filters']

        index_name, condition_expression, filter_expression = construct_dob_initial_last_name_search(demographics)

        self.assertEqual('date-of-birth-initial-last-name', index_name)
        self.assertEqual(expected_condition_expression, condition_expression)
        self.assertEqual(['a list of filters'], filter_expression)

    @patch('search_participants.search_strategies.construct_filter_expression')
    def test_construct_dob_initial_first_name_search(self, mock_filter_constructor, mock_logger):
        demographics = {
            'date_of_birth': 'birthday',
            'sanitised_first_name': 'NAMEY',
            'sanitised_last_name': 'NAMERSON',
            'sanitised_postcode': 'CODE'}
        expected_condition_expression = Key('date_of_birth_and_initial').eq('birthday' + 'N') & \
            Key('sanitised_first_name').begins_with('NAMEY')
        mock_filter_constructor.return_value = ['a list of filters']

        index_name, condition_expression, filter_expression = construct_dob_initial_first_name_search(demographics)

        self.assertEqual('date-of-birth-initial-first-name', index_name)
        self.assertEqual(expected_condition_expression, condition_expression)
        self.assertEqual(['a list of filters'], filter_expression)

    @patch('search_participants.search_strategies.construct_filter_expression')
    def test_construct_dob_initial_partial_postcode_search(self, mock_filter_constructor, mock_logger):
        demographics = {
            'date_of_birth': 'birthday',
            'sanitised_first_name': 'NAMEY',
            'sanitised_last_name': 'NAMERSON',
            'sanitised_postcode': 'CODE'}
        expected_condition_expression = Key('date_of_birth_and_initial').eq('birthday' + 'N') & \
            Key('sanitised_postcode').begins_with('CODE')
        mock_filter_constructor.return_value = ['a list of filters']

        index_name, condition_expression, filter_expression = \
            construct_dob_initial_partial_postcode_search(demographics)

        self.assertEqual('date-of-birth-initial-partial-postcode', index_name)
        self.assertEqual(expected_condition_expression, condition_expression)
        self.assertEqual(['a list of filters'], filter_expression)

    def test_filter_expression_is_empty_list_when_no_filter_attributes_supplied(self, mock_logger):
        demographics = {'name': 'bob'}
        filter_attributes = []

        filter = construct_filter_expression(demographics, filter_attributes)

        self.assertListEqual([], filter)
        mock_logger.assert_not_called()

    def test_filter_expression_does_not_contain_attributes_not_present_in_demographics(self, mock_logger):
        demographics = {'name': 'bob'}
        filter_attributes = ['city', 'car']

        filter = construct_filter_expression(demographics, filter_attributes)

        self.assertListEqual([], filter)
        mock_logger.assert_not_called()

    def test_filter_expression_does_not_contain_values_which_are_empty_in_demographics(self, mock_logger):
        demographics = {'name': ''}
        filter_attributes = ['name']

        filter = construct_filter_expression(demographics, filter_attributes)

        self.assertListEqual([], filter)
        mock_logger.assert_not_called()

    def test_filter_expression_contains_all_filter_attributes_present_in_demographics(self, mock_logger):
        demographics = {'name': 'bob', 'car': 'ferrari'}
        filter_attributes = ['name', 'car']

        expected_filter = [Attr('name').begins_with('bob'), Attr('car').begins_with('ferrari')]
        actual_filter = construct_filter_expression(demographics, filter_attributes)

        self.assertListEqual(expected_filter, actual_filter)

        expected_log_calls = [
            call({'log_reference': LogReference.SEARCHP0012, 'filter_attribute': 'name'}),
            call({'log_reference': LogReference.SEARCHP0012, 'filter_attribute': 'car'}),
        ]

        mock_logger.assert_has_calls(expected_log_calls)
