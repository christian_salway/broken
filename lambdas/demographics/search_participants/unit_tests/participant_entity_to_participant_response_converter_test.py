from unittest.case import TestCase
from search_participants.participant_entity_to_participant_response_converter \
    import convert_dynamodb_entity_to_participant_response


class TestSearchParticipantsParticipantResponseConverter(TestCase):

    def test_participant_response_correctly_built_given_participant_entity_with_all_fields(self):

        participant_entity = {
            'nhs_number': '12345678',
            'sort_key': 'PARTICIPANT',
            'participant_id': '37f4524d-e59f-41d3-9071-97eec87955c8',
            'title': 'MR',
            'first_name': 'TEST',
            'middle_names': 'DYNAMODB',
            'last_name': 'ITEM',
            'date_of_birth': '2018-01-04',
            'gender': 'Male',
            'address': {
                'address_line_1': '9 Eastwood Place',
                'address_line_2': 'Headingley',
                'address_line_4': 'Leeds',
                'postcode': 'LS6 2BE'
            }
        }

        expected_response = {
            'nhs_number': '12345678',
            'participant_id': '37f4524d-e59f-41d3-9071-97eec87955c8',
            'title': 'MR',
            'first_name': 'TEST',
            'middle_names': 'DYNAMODB',
            'last_name': 'ITEM',
            'date_of_birth': '2018-01-04',
            'gender': 'Male',
            'address': {
                'address_line_1': '9 Eastwood Place',
                'address_line_2': 'Headingley',
                'address_line_4': 'Leeds',
                'postcode': 'LS6 2BE'
            }
        }

        actual_response = convert_dynamodb_entity_to_participant_response(participant_entity)

        self.assertDictEqual(expected_response, actual_response)

    def test_participant_response_correctly_built_given_participant_entity_with_only_guaranteed_fields(self):

        participant_entity = {
            'nhs_number': 'a random number',
            'participant_id': '37f4524d-e59f-41d3-9071-97eec87955c8',
        }

        expected_response = {
            'nhs_number': 'a random number',
            'participant_id': '37f4524d-e59f-41d3-9071-97eec87955c8',
            'title': None,
            'first_name': None,
            'middle_names': None,
            'last_name': None
        }

        actual_response = convert_dynamodb_entity_to_participant_response(participant_entity)

        self.assertDictEqual(expected_response, actual_response)
