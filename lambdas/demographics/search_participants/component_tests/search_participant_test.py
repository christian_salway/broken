from datetime import datetime, timezone
import json
from unittest.case import TestCase
from boto3.dynamodb.conditions import Attr, Key
from mock import Mock, patch
from common.models.participant import ParticipantSortKey

from common.utils.data_segregation.nhais_ciphers import Cohorts


example_date = datetime(2020, 10, 18, 13, 48, 8, 123456, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

mock_env_vars = {
    'MAX_SEARCH_RESULTS': '10',
    'AUDIT_QUEUE_URL': 'audit-queue',
    'DYNAMODB_PARTICIPANTS': 'participants-table',
    'ENABLE_PDS_SEARCH': 'TRUE'
}

with patch('datetime.datetime', datetime_mock), patch('os.environ', mock_env_vars):
    from search_participants import search_participants

participants_table_mock = Mock()
sqs_client_mock = Mock()

SESSION = {
    "session_id": "12345678",
    "user_data": {'nhsid_useruid': 'NHS12345', 'first_name': 'firsty', 'last_name': 'lasty'},
    'selected_role': {
        'organisation_code': 'NHSE',
        'organisation_name': 'YORKSHIRE AND THE HUMBER',
        'role_id': 'R8010',
        'role_name': 'Clerical Role'
    },
    'access_groups': {
        'pnl': True,
        'sample_taker': False,
        'lab': False,
        'colposcopy': False,
        'csas': False,
        'view_participant': True
    }
}

PARTICIPANTS = {
    "Items": [
        {
            "nhs_number": "12345678",
            "participant_id": "001",
            "sort_key": ParticipantSortKey.PARTICIPANT,
            "title": "dummy_title",
            "first_name": "correct",
            "middle_names": "participant",
            "last_name": "last",
            "nhais_cipher": "LDS",
            'date_of_birth': '1994-01-02',
        },
        {
            "nhs_number": "12345678",
            "participant_id": "002",
            "sort_key": ParticipantSortKey.PARTICIPANT,
            "title": "dummy_title",
            "first_name": "incorrect",
            "middle_names": "participant",
            "last_name": "last",
            "nhais_cipher": "DMS",
            'date_of_birth': '1994-01-02',
        }
    ]
}

PROJECTION_EXPRESSION = ", ".join([
    "nhs_number",
    "participant_id",
    "sort_key",
    "title",
    "first_name",
    "middle_names",
    "last_name",
    "patient_name",
    "gender",
    "address",
    "date_of_birth",
    "is_fp69",
    "is_ceased",
    "event_text",
    "registered_gp_practice_code",
    "active",
    "nhais_cipher",
    "is_paused",
])

EXPECTED_AUDIT_BODY = {
    "action": "SEARCH_PARTICIPANTS",
    "timestamp": "2020-10-18T13:48:08.123456+00:00",
    "internal_id": "1",
    "nhsid_useruid": "NHS12345",
    "session_id": "12345678",
    "first_name": "firsty",
    "last_name": "lasty",
    "role_id": "R8010",
    "user_organisation_code": "NHSE",
    "participant_ids": ["001"],
    "nhs_numbers": ["12345678"]
}

EXPECTED_BODY = {
    "data": [
                {
                    "nhs_number": "12345678",
                    "participant_id": "001",
                    "title": "dummy_title",
                    "first_name": "correct",
                    "middle_names": "participant",
                    "last_name": "last",
                    "date_of_birth": "1994-01-02"
                }
    ],
    "links": {"self": ""}
}

EXPECTED_RESPONSE = {
    'statusCode': 200,
    'headers': {
        'Content-Type': 'application/json',
        'X-Content-Type-Options': 'nosniff',
        'Strict-Transport-Security': 'max-age=1576800'
    },
    'body': json.dumps(EXPECTED_BODY),
    'isBase64Encoded': False
}

dynamodb_resource_mock = Mock()


class TestSearchParticipants(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()

        participants_table_mock.query.return_value = PARTICIPANTS
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = ''

        self.maxDiff = None
        dynamodb_resource_mock.batch_get_item.return_value = {
            'Responses': {
                'participants-table': []
            }
        }

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_resource',
           Mock(return_value=dynamodb_resource_mock))
    @patch('os.environ', mock_env_vars)
    @patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
           Mock(return_value=participants_table_mock))
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    def search_participants_test_nhs_number(self):

        stringified_request = json.dumps({
            'search_type': 'nhs_number',
            'data': {'nhs_number': '12345678'},
        })

        lambda_proxy_payload = {
            'requestContext': {'authorizer': {
                'session': json.dumps(SESSION), 'principalId': 'blah'
            }},
            'body': stringified_request,
            'headers': {'request_id': '1'}
        }

        actual_response = search_participants.lambda_handler(lambda_proxy_payload, self.context)

        participants_table_mock.query.assert_called_once_with(
            IndexName='sanitised-nhs-number-sort-key',
            KeyConditionExpression=Key('sanitised_nhs_number').eq("12345678")
            & Key('sort_key').eq(ParticipantSortKey.PARTICIPANT.value),
            ProjectionExpression=PROJECTION_EXPRESSION
        )

        self.assertEqual(actual_response, EXPECTED_RESPONSE)
        sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='audit-queue', MessageBody=json.dumps(EXPECTED_AUDIT_BODY))

    @patch('os.environ', mock_env_vars)
    @patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_resource',
           Mock(return_value=dynamodb_resource_mock))
    @patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
           Mock(return_value=participants_table_mock))
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    def search_participants_test_demographics(self):
        stringified_request = json.dumps({
            'search_type': 'demographics',
            'data': {
                'first_name': '',
                'last_name': 'part',
                'date_of_birth': '1994-01-02',
                'postcode': 'DE22 2DD'
            },
        })

        lambda_proxy_payload = {
            'requestContext': {'authorizer': {
                'session': json.dumps(SESSION), 'principalId': 'blah'
            }},
            'body': stringified_request,
            'headers': {'request_id': '1'}
        }

        actual_response = search_participants.lambda_handler(lambda_proxy_payload, self.context)

        participants_table_mock.query.assert_called_once_with(
            IndexName='date-of-birth-initial-full-postcode',
            KeyConditionExpression=Key('date_of_birth_and_initial_and_postcode').eq('1994-01-02' + 'P' + 'DE222DD'),
            FilterExpression=Attr('sanitised_last_name').begins_with('PART'),
            ProjectionExpression=PROJECTION_EXPRESSION
        )

        self.assertEqual(actual_response, EXPECTED_RESPONSE)
        sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='audit-queue', MessageBody=json.dumps(EXPECTED_AUDIT_BODY))
