from boto3.dynamodb.conditions import Key, Attr

from common.log import log
from common.utils.participant_utils import get_first_letter
from search_participants.log_references import LogReference

# --- Available indexes ---
# nhs-number-sort-key
# date-of-birth-initial-first-name
# date-of-birth-initial-full-postcode
# date-of-birth-initial-partial-postcode
# date-of-birth-initial-last-name
# next-test-due-date
# live-record-status
# episode-test-due-date


def construct_dob_initial_full_postcode_search(demographics: {}):
    date_of_birth = demographics.get('date_of_birth')
    initial = get_first_letter(demographics.get('sanitised_last_name'))
    sanitised_first_name = demographics.get('sanitised_first_name')
    sanitised_postcode = demographics.get('sanitised_postcode')

    condition_expression = Key(
        'date_of_birth_and_initial_and_postcode'
        ).eq(date_of_birth + initial + sanitised_postcode)

    if sanitised_first_name:
        condition_expression = condition_expression & Key('sanitised_first_name').begins_with(sanitised_first_name)

    filter = construct_filter_expression(demographics, ['sanitised_last_name'])

    return (
        'date-of-birth-initial-full-postcode',
        condition_expression,
        filter
    )


def construct_dob_initial_last_name_search(demographics: {}):
    date_of_birth = demographics.get('date_of_birth')
    initial = get_first_letter(demographics.get('sanitised_last_name'))
    sanitised_last_name = demographics.get('sanitised_last_name')
    filter = construct_filter_expression(demographics, ['sanitised_first_name'])

    return (
        'date-of-birth-initial-last-name',
        Key('date_of_birth_and_initial').eq(date_of_birth + initial) &
        Key('sanitised_last_name').begins_with(sanitised_last_name),
        filter
    )


def construct_dob_initial_first_name_search(demographics: {}):
    date_of_birth = demographics.get('date_of_birth')
    initial = get_first_letter(demographics.get('sanitised_last_name'))
    sanitised_first_name = demographics.get('sanitised_first_name')
    filter = construct_filter_expression(demographics, ['sanitised_postcode'])

    return (
        'date-of-birth-initial-first-name',
        Key('date_of_birth_and_initial').eq(date_of_birth + initial) &
        Key('sanitised_first_name').begins_with(sanitised_first_name),
        filter
    )


def construct_dob_initial_partial_postcode_search(demographics: {}):
    date_of_birth = demographics.get('date_of_birth', '')
    initial = get_first_letter(demographics.get('sanitised_last_name', ''))
    sanitised_postcode = demographics.get('sanitised_postcode', '')
    filter = construct_filter_expression(demographics, ['sanitised_first_name'])

    return (
        'date-of-birth-initial-partial-postcode',
        Key('date_of_birth_and_initial').eq(date_of_birth + initial) &
        Key('sanitised_postcode').begins_with(sanitised_postcode),
        filter
    )


def construct_filter_expression(demographics, filter_attributes: []):
    filter_expression = []

    for filter_attribute in filter_attributes:
        search_value = demographics.get(filter_attribute)
        if search_value:
            log({'log_reference': LogReference.SEARCHP0012, 'filter_attribute': filter_attribute})
            filter_expression.append(Attr(filter_attribute).begins_with(search_value))

    return filter_expression
