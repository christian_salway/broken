This lambda reads files uploaded to the demographics-data s3 bucket, first header in the files are expected to be in format 'CSS_Update_ccyymmdd_xx_zz' (see https://nhsd-confluence.digital.nhs.uk/display/CSP/Handling+multiple+files).

## Generate letter config
 * Then loop through all the files in bucket to get headers which will stored in a dict with file name as the key and first header as value
 * Get the first file from dict to find how many files should be there in the days patch and build expected file list.
 * This list will be matched with the list of expected files
 * If any files are missing the lambda will raise an exception
 * If all the expected files in the patch are there an sqs message will be send to demographics-files-to-process SQS queue a json of filenames


