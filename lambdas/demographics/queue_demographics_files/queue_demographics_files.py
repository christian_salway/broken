import os
import json
from common.log import log
from queue_demographics_files.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.s3_utils import (
    get_object_from_bucket,
    read_bucket,
    move_object_to_bucket_and_transition_to_infrequent_access_class)
from common.utils.sqs_utils import send_new_message_batch

SQS_DEMOGRAPHICS_FILES_TO_PROCESS_URL = os.environ.get('SQS_QUEUE_URL')
S3_DEMOGRAPHICS_DATA_BUCKET = os.environ.get('DATA_BUCKET')
FAILED_BUCKET = os.environ.get('FAILED_BUCKET')
FILE_EXTENSION_DELIMETER = '.'
FILENAME_PART_DELIMETER = '_'
FIELD_DELIMITER = '|'


@lambda_entry_point
def lambda_handler(event, context):
    contents = {}
    try:
        log({'log_reference': LogReference.QUEUEDEMOG1})
        files_in_bucket = read_bucket(S3_DEMOGRAPHICS_DATA_BUCKET)

        contents = files_in_bucket.get('Contents', [])
        if not contents:
            log({'log_reference': LogReference.QUEUEDEMOG2})
            return

        log({'log_reference': LogReference.QUEUEDEMOG3, 'file_count': len(contents)})

        ordered_filename_response = check_full_patch_arrived(contents)
        ordered_filenames = ordered_filename_response['filenames']
        ordered_internal_ids = ordered_filename_response['internal_ids']

        log({
            'log_reference': LogReference.QUEUEDEMOG4,
            'filenames_json': ordered_filenames,
            'internal_ids': ordered_internal_ids
        })
        if ordered_filenames and ordered_internal_ids:
            send_message_to_queue_demographics_files_queue(ordered_filenames, ordered_internal_ids)

    except Exception as e:
        log({'log_reference': LogReference.QUEUEDEMOG5, 'error': str(e)})
        move_all_files_to_failed_bucket(contents)


def check_full_patch_arrived(contents):
    ordered_files = []
    full_ordered = []
    file_patches = {}
    files_data = build_files_header(contents)
    files_header = {key: files_data[key]['header'] for key in files_data.keys()}
    file_internal_ids = {key: files_data[key]['internal_id'] for key in files_data.keys()}
    file_patches['internal_ids'] = file_internal_ids
    if files_header:

        for _, file_header in files_header.items():
            ordered_files = []
            file_name_sections = file_header.split(FILE_EXTENSION_DELIMETER)
            file_name_parts = file_name_sections[0].split(FILENAME_PART_DELIMETER)

            number_of_files_in_batch = int(file_name_parts[4])
            expected_headers = ['CSS_Update_'
                                f'{file_name_parts[2]}_{i+1}_{number_of_files_in_batch}'
                                for i in range(0, number_of_files_in_batch)]
            for expected_header in expected_headers:
                for file_name, header in files_header.items():
                    if expected_header == header:
                        ordered_files.append(file_name)
            if len(ordered_files) == number_of_files_in_batch:
                for file in ordered_files:
                    full_ordered.append(file)
            else:
                error = 'Files missing from the batch {0} expected : {1}, Actual : {2}'.format(
                    file_name_parts[2], number_of_files_in_batch,
                    len(ordered_files))
                log({'log_reference': LogReference.QUEUEDEMOG7, 'error': error})
                raise Exception(error)
        file_patches['filenames'] = list(dict.fromkeys(full_ordered))
        return file_patches
    else:
        log({'log_reference': LogReference.QUEUEDEMOG9})
        return None


def get_file_content(file_name):
    object_data = get_object_from_bucket(S3_DEMOGRAPHICS_DATA_BUCKET, file_name)
    return [object_data.get('Body'), object_data.get('Metadata')]


def build_files_header(files_in_bucket):
    files_header = {}
    invalid_files = []
    for file in files_in_bucket:
        file_information = get_file_information_from_header(file['Key'])
        file_name_header = file_information['header']
        corresponding_internal_id = file_information['internal_id']
        if is_file_name_from_header_valid(file_name_header):
            log({'log_reference': LogReference.QUEUEDEMOG11, 'object_internal_id': corresponding_internal_id})
            files_header[file['Key']] = {
                'header': file_name_header,
                'internal_id': corresponding_internal_id
            }
        else:
            log({'log_reference': LogReference.QUEUEDEMOG12, 'object_internal_id': corresponding_internal_id})
            invalid_files.append(file['Key'])

    if invalid_files:
        error = f'''There is an invalid file {', '.join(invalid_files)} in todays patch'''
        log({'log_reference': LogReference.QUEUEDEMOG6, 'error': error})
        raise Exception(error)

    return files_header


def get_file_information_from_header(file_name):
    demographics_file, demographics_file_meta_data = get_file_content(file_name)
    internal_id = demographics_file_meta_data['internal_id']
    demographics_file_stream = demographics_file.iter_lines()
    header = next(demographics_file_stream)
    if not header:
        error = f'File {file_name} is empty'
        log({'log_reference': LogReference.QUEUEDEMOG8, 'object_internal_id': internal_id, 'error': error})
        raise Exception(error)

    header_keys = header.decode('utf-8').split(FIELD_DELIMITER)
    return {
        'header': header_keys[0],
        'internal_id': internal_id
    }


def is_file_name_from_header_valid(filename):
    return len(filename.split(FILENAME_PART_DELIMETER)) == 5


def send_message_to_queue_demographics_files_queue(filenames, ordered_internal_ids):
    batch_size = 10
    index = 0
    chunks = [filenames[x:x+batch_size] for x in range(0, len(filenames), batch_size)]
    for chunk in chunks:
        entries = []
        for filename in chunk:
            internal_id = ordered_internal_ids[filename]
            entry = {
                'Id': str(index),
                'MessageBody': json.dumps({
                    'internal_id': internal_id,
                    'filename': filename
                })
            }
            entries.append(entry)
            index += 1
        send_new_message_batch(SQS_DEMOGRAPHICS_FILES_TO_PROCESS_URL, entries)


def move_all_files_to_failed_bucket(contents):
    for file in contents:
        move_object_to_bucket_and_transition_to_infrequent_access_class(
            S3_DEMOGRAPHICS_DATA_BUCKET,
            FAILED_BUCKET,
            file['Key'])
        log({'log_reference': LogReference.QUEUEDEMOG10, 'filename': file['Key']})
