import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    QUEUEDEMOG1 = (logging.INFO, 'Reading S3 Bucket to look for files')
    QUEUEDEMOG2 = (logging.INFO, 'No files found in S3 Bucket')
    QUEUEDEMOG3 = (logging.INFO, 'Found files in bucket')
    QUEUEDEMOG4 = (logging.INFO, 'Sending message to SQS')
    QUEUEDEMOG5 = (logging.ERROR, 'Error processing files')
    QUEUEDEMOG6 = (logging.ERROR, 'Invalid file found in todays patch')
    QUEUEDEMOG7 = (logging.ERROR, 'Files missing from batch')
    QUEUEDEMOG8 = (logging.ERROR, 'File is empty')
    QUEUEDEMOG9 = (logging.INFO, 'Found no files with valid headers')
    QUEUEDEMOG10 = (logging.INFO, 'File moved to failed')
    QUEUEDEMOG11 = (logging.INFO, 'File has a valid header. Marked for processing')
    QUEUEDEMOG12 = (logging.ERROR, 'File has an invalid header')
