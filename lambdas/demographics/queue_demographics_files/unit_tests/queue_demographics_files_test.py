from unittest import TestCase
from mock import patch, call, Mock
from queue_demographics_files.log_references import LogReference
from common.test_mocks.mock_events import cloudwatch_mock_event


@patch('queue_demographics_files.queue_demographics_files.send_new_message_batch')
@patch('queue_demographics_files.queue_demographics_files.read_bucket')
@patch('queue_demographics_files.queue_demographics_files.log')
class TestQueueDemographicsFiles(TestCase):
    def setUp(self):
        import queue_demographics_files.queue_demographics_files as _queue_demographics
        self.queue_demographics = _queue_demographics
        self.log_patcher = patch('common.log.logger')
        self.unwrapped_function = self.queue_demographics.lambda_handler.__wrapped__
        self.queue_demographics.S3_DEMOGRAPHICS_DATA_BUCKET = 'bucket'
        self.queue_demographics.SQS_DEMOGRAPHICS_FILES_TO_PROCESS_URL = 'SQS_QUEUE_URL'
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = 'queue_demographics_files_test'

    def tearDown(self):
        self.log_patcher.stop()

    def test_reads_files_from_s3_and_logs_if_empty(
        self, mock_logger, read_bucket_mock, sqs_mock
    ):
        # given
        file_list = {}
        read_bucket_mock.return_value = file_list

        expected_log_calls = [
            call({'log_reference': LogReference.QUEUEDEMOG1}),
            call({'log_reference': LogReference.QUEUEDEMOG2})
        ]

        # when
        self.unwrapped_function(cloudwatch_mock_event(), self.context)
        # then
        read_bucket_mock.assert_called()
        sqs_mock.assert_not_called()
        mock_logger.assert_called()
        mock_logger.assert_has_calls(expected_log_calls)

    @patch('queue_demographics_files.queue_demographics_files.move_all_files_to_failed_bucket')
    def test_should_call_move_files_when_exception_happen(
        self, move_files_mock, mock_logger, read_bucket_mock, sqs_mock
    ):
        # given
        expected_exception = 'Missing body from queue message'

        read_bucket_mock.side_effect = Exception(expected_exception)

        # when
        self.unwrapped_function(cloudwatch_mock_event(), self.context)
        # then
        move_files_mock.assert_called()

    @patch('queue_demographics_files.queue_demographics_files.get_file_content')
    def test_should_raise_an_exception_when_files_are_missing(
        self, get_file_content_mock, mock_logger, read_bucket_mock, sqs_mock
    ):
        # given
        expected_exception = 'Files missing from the batch 20200528 expected : 3, Actual : 2'
        contents = [{"Key": 'CSS_Update_20200528_1_3.dat', 'LastModified': '2020/1/1'},
                    {"Key": 'CSS_Update_20200528_2_3.dat', 'LastModified': '2020/1/1'},
                    {"Key": 'CSS_Update_20200529_1_2.dat', 'LastModified': '2020/1/1'},
                    {"Key": 'CSS_Update_20200529_2_2.dat', 'LastModified': '2020/1/1'}
                    ]

        mock_demographics_file = Mock()
        mock_demographics_file.iter_lines.return_value = iter([
            'CSS_Update_20200528_1_3|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        mock_demographics_file2 = Mock()
        mock_demographics_file2.iter_lines.return_value = iter([
            'CSS_Update_20200528_2_3|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        mock_demographics_file3 = Mock()
        mock_demographics_file3.iter_lines.return_value = iter([
            'CSS_Update_20200529_1_2|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        mock_demographics_file4 = Mock()
        mock_demographics_file4.iter_lines.return_value = iter([
            'CSS_Update_20200529_2_2|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])

        get_file_content_mock.side_effect = [
            [mock_demographics_file, {'internal_id': 1}],
            [mock_demographics_file2, {'internal_id': 2}],
            [mock_demographics_file3, {'internal_id': 3}],
            [mock_demographics_file4, {'internal_id': 4}]
        ]

        # when
        with self.assertRaises(Exception) as context:
            self.queue_demographics.check_full_patch_arrived(contents)
        # then
        self.assertEqual(expected_exception, context.exception.args[0])

    @patch('queue_demographics_files.queue_demographics_files.get_file_content')
    def test_reads_files_from_s3_and_sends_sqs_message_with_file_names(
        self, get_file_content_mock, mock_logger, read_bucket_mock, sqs_mock
    ):
        # given
        file1 = 'CSS_Update_20200528_1_2.dat'
        file2 = 'CSS_Update_20200528_2_2.dat'
        file3 = 'CSS_Update_20200529_1_2.dat'
        file4 = 'CSS_Update_20200529_2_2.dat'
        file_list = {"Contents": [{"Key": file1, 'LastModified': '2020/1/1'},
                                  {"Key": file2, 'LastModified': '2020/1/1'},
                                  {"Key": file3, 'LastModified': '2020/1/1'},
                                  {"Key": file4, 'LastModified': '2020/1/1'}]}
        read_bucket_mock.return_value = file_list

        mock_demographics_file = Mock()
        mock_demographics_file.iter_lines.return_value = iter([
            'CSS_Update_20200528_1_2|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        mock_demographics_file2 = Mock()
        mock_demographics_file2.iter_lines.return_value = iter([
            'CSS_Update_20200528_2_2|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        mock_demographics_file3 = Mock()
        mock_demographics_file3.iter_lines.return_value = iter([
            'CSS_Update_20200529_1_2|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        mock_demographics_file4 = Mock()
        mock_demographics_file4.iter_lines.return_value = iter([
            'CSS_Update_20200529_2_2|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        get_file_content_mock.side_effect = [
            [mock_demographics_file, {'internal_id': 1}],
            [mock_demographics_file2, {'internal_id': 2}],
            [mock_demographics_file3, {'internal_id': 3}],
            [mock_demographics_file4, {'internal_id': 4}]
        ]

        expected_s3_calls = [
            call(file1),
            call(file2),
            call(file3),
            call(file4)
        ]
        expected_log_calls = [
            call({'log_reference': LogReference.QUEUEDEMOG1}),
            call({
                'log_reference': LogReference.QUEUEDEMOG3,
                'file_count': 4
            }),
            call({
                'log_reference': LogReference.QUEUEDEMOG11,
                'object_internal_id': 1
            }),
            call({
                'log_reference': LogReference.QUEUEDEMOG11,
                'object_internal_id': 2
            }),
            call({
                'log_reference': LogReference.QUEUEDEMOG11,
                'object_internal_id': 3
            }),
            call({
                'log_reference': LogReference.QUEUEDEMOG11,
                'object_internal_id': 4
            }),
            call({
                'log_reference':
                LogReference.QUEUEDEMOG4,
                'filenames_json': [
                    'CSS_Update_20200528_1_2.dat',
                    'CSS_Update_20200528_2_2.dat',
                    'CSS_Update_20200529_1_2.dat',
                    'CSS_Update_20200529_2_2.dat'
                ],
                'internal_ids': {
                    'CSS_Update_20200528_1_2.dat': 1,
                    'CSS_Update_20200528_2_2.dat': 2,
                    'CSS_Update_20200529_1_2.dat': 3,
                    'CSS_Update_20200529_2_2.dat': 4
                }
            })
        ]

        # when
        self.unwrapped_function(cloudwatch_mock_event(), self.context)
        # then
        read_bucket_mock.assert_called()
        sqs_mock.assert_called()
        get_file_content_mock.assert_has_calls(expected_s3_calls)
        mock_logger.assert_called()
        mock_logger.assert_has_calls(expected_log_calls)

    @patch('queue_demographics_files.queue_demographics_files.get_file_content')
    def test_build_files_header_only_adds_valid_headers_to_list(
        self, get_file_content_mock, mock_logger, read_bucket_mock, sqs_mock
    ):
        # given
        contents = [{"Key": 'CSS_Update_20200528_1_2.dat', 'LastModified': '2020/1/1'},
                    {"Key": 'CSS_Update_20200528_2_2.dat', 'LastModified': '2020/1/1'}]

        mock_demographics_file = Mock()
        mock_demographics_file.iter_lines.return_value = iter([
            'CSS_Update_20200528_|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        mock_demographics_file2 = Mock()
        mock_demographics_file2.iter_lines.return_value = iter([
            'CSS_Update_20200528_2_2|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])

        get_file_content_mock.side_effect = [
            [mock_demographics_file, {'internal_id': 1}],
            [mock_demographics_file2, {'internal_id': 2}]
        ]
        expected_exception = 'There is an invalid file CSS_Update_20200528_1_2.dat in todays patch'
        # when

        with self.assertRaises(Exception) as context:
            self.queue_demographics.build_files_header(contents)
        # then
        self.assertEqual(expected_exception, context.exception.args[0])

    @patch('queue_demographics_files.queue_demographics_files.get_file_content')
    @patch('queue_demographics_files.queue_demographics_files.move_all_files_to_failed_bucket')
    def test_sqs_message_should_not_be_sent_if_check_full_patch_arrived_returns_null(
        self, move_file_mock, get_file_content_mock, mock_logger, read_bucket_mock, sqs_mock
    ):
        # given
        file1 = 'CSS_Update_20200528_1_2.dat'
        file2 = 'CSS_Update_20200528_2_2.dat'
        file_list = {"Contents": [{"Key": file1, 'LastModified': '2020/1/1'},
                                  {"Key": file2, 'LastModified': '2020/1/1'}]}
        read_bucket_mock.return_value = file_list
        contents = [{"Key": file1, 'LastModified': '2020/1/1'}, {"Key": file2, 'LastModified': '2020/1/1'}]

        mock_demographics_file = Mock()
        mock_demographics_file.iter_lines.return_value = iter([
            'INVALID_1|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])
        mock_demographics_file2 = Mock()
        mock_demographics_file2.iter_lines.return_value = iter([
            'INVALID_2|CSS|Update|20200527013023|1|1|72'.encode('utf-8'),
            'message1'.encode('utf-8'),
            'message2'.encode('utf-8')
        ])

        get_file_content_mock.side_effect = [
            [mock_demographics_file, {'internal_id': 1}],
            [mock_demographics_file2, {'internal_id': 2}]
        ]

        expected_s3_calls = [
            call(file1),
            call(file2)
        ]
        expected_move_file_call = [
            call(contents)
        ]

        # when
        self.unwrapped_function(cloudwatch_mock_event(), self.context)
        # then
        read_bucket_mock.assert_called()
        sqs_mock.assert_not_called()
        move_file_mock.assert_called()
        move_file_mock.assert_has_calls(expected_move_file_call)
        get_file_content_mock.assert_has_calls(expected_s3_calls)
