from unittest import TestCase
from datetime import datetime, timezone
from mock import patch, call, Mock
from create_add_block_request.log_references import LogReference

module = 'create_add_block_request.create_add_block_request'


@patch(f'{module}.log')
class TestCreateAddBlockRequest(TestCase):
    mock_env_vars = {
        'ADD_BLOCK_REQUEST_FROM_MAILBOX_ID': 'from_mailbox_id',
        'ADD_BLOCK_REQUEST_TO_MAILBOX_ID': 'to_mailbox_id',
        'MESH_FILES_TO_SEND_QUEUE_URL': 'mesh_file_to_send_queue_url',
        'MESH_OUTBOUND_MESSAGES_BUCKET': 'test_environment-mesh-outbound-messages',
    }

    @patch('os.environ', mock_env_vars)
    def setUp(self):
        import create_add_block_request.create_add_block_request as _create_add_block_request
        self.create_add_block_request = _create_add_block_request

    @patch(f'{module}.get_participant_id_list_from_event')
    @patch(f'{module}.get_participant_information')
    @patch(f'{module}.create_file_in_s3')
    @patch(f'{module}.send_message_to_mesh_outbound_sqs')
    def test_file_created_and_message_sent_to_sqs(self, send_to_sqs_mock, create_file_mock,
                                                  get_participant_info_mock, get_participant_ids_mock, log_mock):
        event = {'Records': [
            {'body': '{"participant_ids": ["fa54bf01-48db-4df2-9945-346d9870aa93", '
             '"37f4524d-e59f-41d3-9071-97eec87955c8", "0bcca266-dae5-4081-bc53-ea4419ecb27b", '
             '"58a18f99-9b19-4e2e-aab9-4069ac043217"]}'}
        ]}
        get_participant_ids_mock.return_value = [
            'fa54bf01-48db-4df2-9945-346d9870aa93',
            '34f011a3-ba2e-4559-9d44-1366ce1e8e2d',
            '37f4524d-e59f-41d3-9071-97eec87955c8',
            '0bcca266-dae5-4081-bc53-ea4419ecb27b',
            '6a9efee0-1797-4372-9ab5-b334834169f5',
            '58a18f99-9b19-4e2e-aab9-4069ac043217'
        ]
        get_participant_info_mock.side_effect = [
            {'sanitised_nhs_number': '9565512593', 'first_name': 'Jim', 'last_name': 'Tim',
                'date_of_birth': '1990-01-01', 'add_block_request_type': 'ADA'},
            None,
            {'sanitised_nhs_number': '9999999999', 'first_name': 'Jam', 'last_name': 'Tam',
                'date_of_birth': '1990-02-02', 'add_block_request_type': 'ADB'},
            {'sanitised_nhs_number': '1234567890', 'first_name': 'Jom', 'last_name': 'Tom',
                'date_of_birth': '1990-02-03', 'add_block_request_type': 'RMA'},
            None,
            {'sanitised_nhs_number': '9999999998', 'first_name': 'Jan', 'last_name': 'Tan',
                'date_of_birth': '1990-02-04', 'add_block_request_type': 'RMB'}
        ]
        create_file_mock.return_value = 'add_block_request_file'

        self.create_add_block_request.lambda_handler.__wrapped__(event, {})

        get_participant_ids_mock.assert_called_with(
            {'Records': [
                {'body': '{"participant_ids": ["fa54bf01-48db-4df2-9945-346d9870aa93", '
                 '"37f4524d-e59f-41d3-9071-97eec87955c8", "0bcca266-dae5-4081-bc53-ea4419ecb27b", '
                 '"58a18f99-9b19-4e2e-aab9-4069ac043217"]}'}]})
        get_participant_info_calls = [
            call('fa54bf01-48db-4df2-9945-346d9870aa93'),
            call('34f011a3-ba2e-4559-9d44-1366ce1e8e2d'),
            call('37f4524d-e59f-41d3-9071-97eec87955c8'),
            call('0bcca266-dae5-4081-bc53-ea4419ecb27b'),
            call('6a9efee0-1797-4372-9ab5-b334834169f5'),
            call('58a18f99-9b19-4e2e-aab9-4069ac043217')
        ]
        get_participant_info_mock.assert_has_calls(get_participant_info_calls)
        expected_detail_list = [
            '9565512593|Jim|Tim|19900101|ADA',
            '9999999999|Jam|Tam|19900202|ADB',
            '1234567890|Jom|Tom|19900203|RMA',
            '9999999998|Jan|Tan|19900204|RMB'
        ]
        create_file_mock.assert_called_with(expected_detail_list)
        send_to_sqs_mock.assert_called_with('add_block_request_file')
        log_mock.assert_not_called()

    @patch(f'{module}.get_participant_id_list_from_event')
    @patch(f'{module}.get_participant_information')
    @patch(f'{module}.create_file_in_s3')
    @patch(f'{module}.send_message_to_mesh_outbound_sqs')
    def test_file_not_created_when_no_participant_information(self, send_to_sqs_mock, create_file_mock,
                                                              get_participant_info_mock,
                                                              get_participant_ids_mock, log_mock):

        event = {'Records': [
            {'body': '{"participant_ids": ["fa54bf01-48db-4df2-9945-346d9870aa93"]}'}
        ]}
        get_participant_ids_mock.return_value = ['fa54bf01-48db-4df2-9945-346d9870aa93']
        get_participant_info_mock.side_effect = [None]
        create_file_mock.return_value = 'add_block_request_file'
        with self.assertRaises(Exception) as context:
            self.create_add_block_request.lambda_handler.__wrapped__(event, {})

        self.assertEqual(context.exception.args[0], 'Could not find valid information for any participant')
        get_participant_ids_mock.assert_called_with(
            {'Records': [
                {'body': '{"participant_ids": ["fa54bf01-48db-4df2-9945-346d9870aa93"]}'}]})
        get_participant_info_mock.assert_called_with('fa54bf01-48db-4df2-9945-346d9870aa93')
        create_file_mock.assert_not_called()
        send_to_sqs_mock.assert_not_called()
        log_mock.assert_called_with

    def test_get_participant_ids_from_event(self, log_mock):
        event = {'Records': [
            {'body': '{"participant_ids": ["fa54bf01-48db-4df2-9945-346d9870aa93", '
             '"37f4524d-e59f-41d3-9071-97eec87955c8", "0bcca266-dae5-4081-bc53-ea4419ecb27b", '
             '"58a18f99-9b19-4e2e-aab9-4069ac043217"]}'}]}
        participant_ids = self.create_add_block_request.get_participant_id_list_from_event(event)
        self.assertEqual(participant_ids, [
            "fa54bf01-48db-4df2-9945-346d9870aa93",
            "37f4524d-e59f-41d3-9071-97eec87955c8",
            "0bcca266-dae5-4081-bc53-ea4419ecb27b",
            "58a18f99-9b19-4e2e-aab9-4069ac043217"
        ])

    def test_get_participant_ids_from_event_throws_exception_when_participant_ids_not_present(self, log_mock):
        event = {'Records': [{'body': '{"another_field": "One"}'}]}
        with self.assertRaises(Exception) as context:
            self.create_add_block_request.get_participant_id_list_from_event(event)

        self.assertEqual(context.exception.args[0], 'Participant_ids could not be retrieved from sqs queue')

    @patch(f'{module}.get_participant_by_participant_id')
    def test_get_participant_information(self, get_participant_mock, log_mock):
        participant_id = 'fa54bf01-48db-4df2-9945-346d9870aa93'
        get_participant_mock.return_value = {'sanitised_nhs_number': '9565512593', 'add_block_request_type': 'ADA'}
        participant = self.create_add_block_request.get_participant_information(participant_id)

        self.assertEqual(participant, {'sanitised_nhs_number': '9565512593', 'add_block_request_type': 'ADA'})

    @patch(f'{module}.get_participant_by_participant_id')
    def test_get_participant_information_when_no_participant_found(self, get_participant_mock, log_mock):
        participant_id = 'fa54bf01-48db-4df2-9945-346d9870aa93'
        get_participant_mock.return_value = {}
        participant = self.create_add_block_request.get_participant_information(participant_id)

        self.assertEqual(participant, None)
        log_mock.assert_called_with({'log_reference': LogReference.ADDBLOCKREQ0003})

    @patch(f'{module}.get_participant_by_participant_id')
    def test_get_participant_information_when_no_add_block_request_type(self, get_participant_mock, log_mock):
        participant_id = 'fa54bf01-48db-4df2-9945-346d9870aa93'
        get_participant_mock.return_value = {'Item': {'sanitised_nhs_number': '9565512593'}}
        participant = self.create_add_block_request.get_participant_information(participant_id)

        self.assertEqual(participant, None)
        log_mock.assert_called_with({'log_reference': LogReference.ADDBLOCKREQ0004})

    @patch(f'{module}.get_participant_by_participant_id')
    def test_get_participant_information_when_incorrect_add_block_request_type(self, get_participant_mock, log_mock):
        participant_id = 'fa54bf01-48db-4df2-9945-346d9870aa93'
        get_participant_mock.return_value = {
            'Item': {'sanitised_nhs_number': '9565512593', 'add_block_request_type': 'PPP'}}
        participant = self.create_add_block_request.get_participant_information(participant_id)

        self.assertEqual(participant, None)
        log_mock.assert_called_with({'log_reference': LogReference.ADDBLOCKREQ0004})

    @patch(f'{module}.get_participant_by_participant_id')
    def test_get_participant_information_when_no_nhs_number(self, get_participant_mock, log_mock):
        participant_id = 'fa54bf01-48db-4df2-9945-346d9870aa93'
        get_participant_mock.return_value = {'add_block_request_type': 'ADA'}
        participant = self.create_add_block_request.get_participant_information(participant_id)

        self.assertEqual(participant, None)
        log_mock.assert_called_with({'log_reference': LogReference.ADDBLOCKREQ0005})

    @patch(f'{module}.put_object')
    @patch(f'{module}.datetime')
    def test_create_file_in_s3_creates_file_with_correct_format(self, datetime_mock, put_object_mock, log_mock):

        datetime_mock.now = Mock(return_value=datetime(2020, 7, 3, 6, 22, 23, 233, tzinfo=timezone.utc))

        detail_list = [
            '9565512593|Jim|Tim|19900101|ADA',
            '9999999999|Jam|Tam|19900202|ADB',
            '1234567890|Jom|Tom|19900203|RMA',
            '9999999998|Jan|Tan|19900204|RMB'
        ]

        file_name = self.create_add_block_request.create_file_in_s3(detail_list)

        expected_file_body = (
            "CSS_AddBlockReq_20200703062223|CSS|AddBlockRequest|20200703062223|4\n"
            "9565512593|Jim|Tim|19900101|ADA\n"
            "9999999999|Jam|Tam|19900202|ADB\n"
            "1234567890|Jom|Tom|19900203|RMA\n"
            "9999999998|Jan|Tan|19900204|RMB\n"
        )

        expected_file_name = 'AddBlockRequest_20200703062223.txt'
        put_object_mock.assert_called_with('test_environment-mesh-outbound-messages',
                                           expected_file_body, expected_file_name)
        self.assertEqual(file_name, expected_file_name)
        log_mock.assert_called_with({'log_reference': LogReference.ADDBLOCKREQ0007})

    @patch(f'{module}.send_new_message')
    def test_sending_message_to_sqs_queue(self, send_to_sqs_mock, log_mock):
        file_name = 'AddBlockRequest_20200703062223.txt'

        self.create_add_block_request.send_message_to_mesh_outbound_sqs(file_name)

        expected_message_to_queue = {
            'from_mailbox': 'from_mailbox_id',
            'to_mailbox': 'to_mailbox_id',
            'workflow_id': '[TBC]',
            'bucket_name': 'mesh-outbound-messages',
            'object_key': 'AddBlockRequest_20200703062223.txt'
        }

        send_to_sqs_mock.assert_called_with('mesh_file_to_send_queue_url', expected_message_to_queue)
        log_mock.assert_called_with({'log_reference': LogReference.ADDBLOCKREQ0008,
                                     'file_name': 'AddBlockRequest_20200703062223.txt'})
