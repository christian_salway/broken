import json
from os import environ
from datetime import datetime, timezone


from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_utils import get_participant_by_participant_id
from create_add_block_request.log_references import LogReference
from common.utils.s3_utils import put_object
from common.utils.sqs_utils import send_new_message, read_message_from_queue

ADD_BLOCK_REQUEST_TYPES = ['ADA', 'ADB', 'RMA', 'RMB']
ADD_BLOCK_REQUEST_FROM_MAILBOX_ID = environ.get('ADD_BLOCK_REQUEST_FROM_MAILBOX_ID')
ADD_BLOCK_REQUEST_TO_MAILBOX_ID = environ.get('ADD_BLOCK_REQUEST_TO_MAILBOX_ID')
MESH_FILES_TO_SEND_QUEUE_URL = environ.get('MESH_FILES_TO_SEND_QUEUE_URL')
MESH_OUTBOUND_MESSAGES_BUCKET = environ.get('MESH_OUTBOUND_MESSAGES_BUCKET')


@lambda_entry_point
def lambda_handler(event, context):
    participant_id_list = get_participant_id_list_from_event(event)
    detail_list = []
    for participant_id in participant_id_list:
        participant = get_participant_information(participant_id)
        if not participant:
            continue

        nhs_number = participant['sanitised_nhs_number']
        given_name = participant.get('first_name')
        family_name = participant.get('last_name')
        date_of_birth = participant.get('date_of_birth').replace('-', '')
        request_type = participant['add_block_request_type']

        participant_line = f'{nhs_number}|{given_name}|{family_name}|{date_of_birth}|{request_type}'
        detail_list.append(participant_line)

    if not detail_list:
        log({'log_reference': LogReference.ADDBLOCKREQ0006})
        raise Exception(LogReference.ADDBLOCKREQ0006.message)

    file_name = create_file_in_s3(detail_list)
    send_message_to_mesh_outbound_sqs(file_name)


def get_participant_id_list_from_event(event):
    event_json = read_message_from_queue(event)
    sqs_message = json.loads(event_json)
    participant_ids = sqs_message.get('participant_ids', [])
    if not participant_ids:
        log({'log_reference': LogReference.ADDBLOCKREQ0002})
        raise Exception(LogReference.ADDBLOCKREQ0002.message)

    log({'log_reference': LogReference.ADDBLOCKREQ0001})
    return participant_ids


def get_participant_information(participant_id):
    participant = get_participant_by_participant_id(participant_id)

    if not participant:
        log({'log_reference': LogReference.ADDBLOCKREQ0003})
        return None

    if participant.get('add_block_request_type') not in ADD_BLOCK_REQUEST_TYPES:
        log({'log_reference': LogReference.ADDBLOCKREQ0004})
        return None

    if not participant.get("sanitised_nhs_number"):
        log({'log_reference': LogReference.ADDBLOCKREQ0005})
        return None

    return participant


def create_file_in_s3(detail_list):

    timestamp = datetime.now(timezone.utc).strftime('%Y%m%d%H%M%S')
    add_block_request_header = f'CSS_AddBlockReq_{timestamp}|CSS|AddBlockRequest|{timestamp}|{len(detail_list)}'
    file_body = add_block_request_header + '\n'

    for person in detail_list:
        file_body += person + '\n'

    file_name = f'AddBlockRequest_{timestamp}.txt'
    put_object(MESH_OUTBOUND_MESSAGES_BUCKET, file_body, file_name)
    log({'log_reference': LogReference.ADDBLOCKREQ0007})

    return file_name


def send_message_to_mesh_outbound_sqs(file_name):
    message = {
        'from_mailbox': ADD_BLOCK_REQUEST_FROM_MAILBOX_ID,
        'to_mailbox': ADD_BLOCK_REQUEST_TO_MAILBOX_ID,
        'workflow_id': '[TBC]',
        'bucket_name': 'mesh-outbound-messages',
        'object_key': file_name
    }

    send_new_message(MESH_FILES_TO_SEND_QUEUE_URL, message)
    log({'log_reference': LogReference.ADDBLOCKREQ0008, 'file_name': file_name})
