import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    ADDBLOCKREQ0001 = (logging.INFO, 'Participant_ids retrieved from sqs queue')
    ADDBLOCKREQ0002 = (logging.ERROR, 'Participant_ids could not be retrieved from sqs queue')
    ADDBLOCKREQ0003 = (logging.WARNING, 'Participant not found')
    ADDBLOCKREQ0004 = (logging.WARNING, 'Add block request type for a participant was incorrect')
    ADDBLOCKREQ0005 = (logging.WARNING, 'NHS number for a participant was not found')
    ADDBLOCKREQ0006 = (logging.ERROR, 'Could not find valid information for any participant')
    ADDBLOCKREQ0007 = (logging.INFO, 'Add block request file created in mesh-outbound-messages S3 bucket')
    ADDBLOCKREQ0008 = (logging.INFO, 'Message sent to mesh-files-to-send SQS queue')
