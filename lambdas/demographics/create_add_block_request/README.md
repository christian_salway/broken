This Lambda creates an addblock request file taking in a list of participant ids to then send to NHS Spine via mesh
It first reads an array of Participant ids from the `add-block-request-participant-ids` SQS Queue.
It then get the neccessary details from the participants table and then creates a file in the `mesh-outbound-messages` bucket
It then sends a message to the `mesh files to send` SQS Queue with the filename and mailbox details to be picked up by the `send_mesh_message` lambda