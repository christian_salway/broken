THIS LAMBDA IS A PROOF OF CONCEPT
It will need to be improved for actual use in the system

This lambda is the callback for authentication with the PDS FHIR API. It recieves an auth code, and uses that to exchange for a token. It then stores the tokens and expiry times against the user session and redirects to the pds-participant summary page
Request
`/auth/pds-callback?state={}&code={code}`

Response
```
302 redirect to webapp/#/pds-participant/{nhs_number}/summary
```