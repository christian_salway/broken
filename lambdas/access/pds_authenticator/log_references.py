import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    PDSCALLBACK0001 = (logging.INFO, 'Received request to authenticate with pds.')
    PDSCALLBACK0002 = (logging.ERROR, 'State missing from request.')
    PDSCALLBACK0003 = (logging.ERROR, 'State received does not match state sent.')
    PDSCALLBACK0004 = (logging.INFO, 'Getting PDS client secret.')
    PDSCALLBACK0005 = (logging.INFO, 'Requesting PDS auth token.')
    PDSCALLBACK0006 = (logging.ERROR, 'Unable to retrieve PDS auth token.')
    PDSCALLBACK0007 = (logging.INFO, 'Updating user session with PDS auth token.')
    PDSCALLBACK0008 = (logging.INFO, 'Redirecting to PDS participant search page.')
