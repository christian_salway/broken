import unittest

from mock import patch, Mock

module = 'pds_authenticator.pds_authenticator'


@patch(f'{module}.log', Mock())
@patch('os.environ', {
    'PDS_FHIR_AUTH_URL': 'the_pds_auth_url',
    'PDSFHIR_CLIENT_SECRET_ID': 'the_client_secret_id',
    'CALLBACK_URL': 'pds_screening_callback_url',
    'DNS_NAME': 'environment.example.org'
})
class TestGetPdsParticipant(unittest.TestCase):

    @patch('boto3.resource')
    def setUp(self, *args):
        # Setup lambda module
        from pds_authenticator.pds_authenticator import lambda_handler
        global AuditActions
        from common.utils.audit_utils import AuditActions
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    @patch(f'{module}.audit')
    @patch(f'{module}.get_session_from_lambda_event')
    def test_pds_authenticator_returns_bad_request_when_state_missing_from_request(self, get_session_mock, audit_mock):
        get_session_mock.return_value = {'session_token': 'the_session_token'}
        event = {'queryStringParameters': {'notState': 'text'}}
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "State missing from request"}',
            'isBase64Encoded': False
        }
        actual_response = self.unwrapped_handler(event, {})
        self.assertDictEqual(expected_response, actual_response)
        audit_mock.assert_not_called()

    @patch(f'{module}.audit')
    @patch(f'{module}.get_session_from_lambda_event')
    def test_pds_authenticator_returns_bad_request_when_state_does_not_match(self, get_session_mock, audit_mock):
        get_session_mock.return_value = {'session_token': 'the_session_token',
                                         'hashed_session_token': 'a_hashed_token'}
        event = {'queryStringParameters': {'state': 'eyJoYXNoZWRfc2Vzc2lvbl90b2tlbiI6ICJ0aGVfaGFzaGVkX3Rva2VuIn0='}}
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "State received does not match state sent"}',
            'isBase64Encoded': False
        }
        actual_response = self.unwrapped_handler(event, {})
        self.assertDictEqual(expected_response, actual_response)
        audit_mock.assert_not_called()

    @patch(f'{module}.time')
    @patch(f'{module}.audit')
    @patch(f'{module}.update_session')
    @patch(f'{module}.get_pds_fhir_initial_access_tokens')
    @patch(f'{module}.get_session_from_lambda_event')
    def test_pds_authenticator_returns_redirect_to_participant_summary_page(
            self,
            get_session_mock,
            get_token_mock,
            update_session_mock,
            audit_mock,
            time_mock
    ):
        session = {
            'session_token': 'the_session_token',
            'hashed_session_token': 'the_hashed_token',
            'pds_search_params': {
                'search_type': 'nhs_number',
                'nhs_number': '1234567890'
            }
        }
        get_session_mock.return_value = session
        state = 'eyJoYXNoZWRfc2Vzc2lvbl90b2tlbiI6ICJ0aGVfaGFzaGVkX3Rva2VuIiwgIm5oc19udW1iZXIiOiAiOTk5OTk5OTk5OSJ9'
        event = {'queryStringParameters': {'state': state}}
        time_mock.return_value = 1601392160
        get_token_mock.return_value = {
            'access_token': 'the_access_token',
            'expires_in': 500,
            'refresh_token': 'the_refresh_token',
            'refresh_token_expires_in': 1000
        }
        expected_response = {
            'statusCode': 302,
            'headers': {
                'Location': 'https://environment.example.org/#/results?search_type=nhs_number&nhs_number=1234567890'
            }
        }
        actual_response = self.unwrapped_handler(event, {})
        self.assertDictEqual(expected_response, actual_response)
        update_session_mock.assert_called_with('the_session_token',
                                               {'pds_access_token': 'the_access_token',
                                                'pds_access_token_expiry': 1601392660,
                                                'pds_refresh_token': 'the_refresh_token',
                                                'pds_reauthenticate_timestamp': 1601393160},
                                               delete_attributes=['hashed_session_token'])
        audit_mock.assert_called_with(session=session, action=AuditActions.PDS_LOGIN)
