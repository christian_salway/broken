from datetime import datetime, timezone
from time import time
from unittest import TestCase
from unittest.mock import patch, Mock


class MockRequestsResponse:
    json_obj: str = None
    status_code: int = 200
    error: Exception = None

    def __init__(
        self,
        json_obj,
        status_code=200,
        error=None
    ):
        self.json_obj = json_obj
        self.status_code = status_code
        self.error = error

    def json(self):
        return self.json_obj

    def raise_for_status(self):
        if self.error:
            raise self.error


@patch('os.environ', {
    'AUDIT_QUEUE_URL': 'the_audit_queue_url',
    'PDS_FHIR_AUTH_URL': 'the_pds_auth_url',
    'PDS_FHIR_CLIENT_SECRET_ID': 'the_client_secret_id',
    'PDS_FHIR_CALLBACK_URL': 'pds_screening_callback_url',
    'DNS_NAME': 'environment.example.org',
    'SESSION_TABLE_NAME': 'sessions-table'
})
class TestPdsAuthenticatorParticipant(TestCase):
    time_mock = Mock(wraps=time)
    time_mock.return_value = 1601392160

    example_date = datetime(2020, 9, 29, 14, 26, 1, tzinfo=timezone.utc)
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.now.return_value = example_date

    post_response_json = {
        'access_token': 'the_access_token',
        'expires_in': 500,
        'refresh_token': 'the_refresh_token',
        'refresh_token_expires_in': 1000
    }

    @classmethod
    @patch('boto3.session.Session')
    @patch('boto3.client')
    @patch('boto3.resource')
    @patch('datetime.datetime', datetime_mock)
    @patch('time.time', time_mock)
    def setUpClass(cls, boto3_resource, boto3_client, boto3_session, *args):
        cls.log_patcher = patch('pds_authenticator.pds_authenticator.log')

        # Setup database mock
        session_table_mock = Mock()
        session_table_mock.update_item.return_value = {'Attributes': {'a_key': 'a_value'}}
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = session_table_mock
        boto3_resource.return_value = boto3_table_mock
        cls.session_table_mock = session_table_mock

        # Setup secrets manager mock
        session_client_mock = Mock()
        session_client_mock.get_secret_value.return_value = {
            'SecretString': '{"client_secret": "the_secret", "client_id": "the_client"}'
        }
        boto3_session_mock = Mock()
        boto3_session_mock.client.return_value = session_client_mock
        boto3_session.return_value = boto3_session_mock
        cls.boto3_session_mock = boto3_session_mock
        cls.session_client_mock = session_client_mock

        # Setup SQS mock
        sqs_client_mock = Mock()
        boto3_client.return_value = sqs_client_mock
        cls.sqs_client_mock = sqs_client_mock

        # Setup lambda module
        import pds_authenticator.pds_authenticator as _pds_authenticator
        global pds_authenticator_module
        pds_authenticator_module = _pds_authenticator

    def setUp(self):
        self.session_table_mock.reset_mock()
        self.boto3_session_mock.reset_mock()
        self.session_client_mock.reset_mock()
        self.sqs_client_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('requests.post')
    def test_pds_authenticator(self, post_request_mock):
        # Setup requests mocks
        post_request_mock.return_value = MockRequestsResponse(
            self.post_response_json
        )

        # Setup the event and the function invocation
        event = _build_event()
        context = Mock()
        context.function_name = ''

        expected_response = {
            'statusCode': 302,
            'headers': {
                'Location': 'https://environment.example.org/#/results?search_type=nhs_number&nhs_number=1234567890'
            }
        }

        actual_response = pds_authenticator_module.lambda_handler(event, context)

        # Assert client secret was correctly looked up from secrets manager
        self.boto3_session_mock.client.assert_called_with(service_name='secretsmanager', region_name='eu-west-2', endpoint_url='https://secretsmanager.eu-west-2.amazonaws.com') 
        self.session_client_mock.get_secret_value.assert_called_with(SecretId='the_client_secret_id')

        # Assert auth code requested from PDS token endpoint
        post_request_mock.assert_called_with('the_pds_auth_url/token',
                                             headers={'Content-Type': 'application/x-www-form-urlencoded'},
                                             data={
                                                 'client_secret': 'the_secret',
                                                 'client_id': 'the_client',
                                                 'grant_type': 'authorization_code',
                                                 'redirect_uri': 'pds_screening_callback_url',
                                                 'code': 'the_code'
                                             })

        # Assert user session is updated
        self.session_table_mock.update_item.assert_called_with(
            Key={'session_token': 'the_session_token'},
            UpdateExpression='SET #pds_access_token = :pds_access_token, #pds_access_token_expiry = :pds_access_token_'
                             'expiry, #pds_refresh_token = :pds_refresh_token, #pds_reauthenticate_timestamp = '
                             ':pds_reauthenticate_timestamp, #expires = :expires REMOVE #hashed_session_token',
            ExpressionAttributeValues={':pds_access_token': 'the_access_token', ':pds_access_token_expiry': 1601392660,
                                       ':pds_refresh_token': 'the_refresh_token',
                                       ':pds_reauthenticate_timestamp': 1601393160, ':expires': 1601392760},
            ExpressionAttributeNames={'#pds_access_token': 'pds_access_token',
                                      '#pds_access_token_expiry': 'pds_access_token_expiry',
                                      '#pds_refresh_token': 'pds_refresh_token',
                                      '#pds_reauthenticate_timestamp': 'pds_reauthenticate_timestamp',
                                      '#expires': 'expires', '#hashed_session_token': 'hashed_session_token'},
            ReturnValues='ALL_NEW'
        )

        # Assert successful PDS authentication audited
        self.sqs_client_mock.send_message.assert_called_with(
             QueueUrl='the_audit_queue_url',
             MessageBody='{"action": "PDS_LOGIN", "timestamp": "2020-09-29T14:26:01+00:00", "internal_id": "requestID", "nhsid_useruid": "user_uid", "session_id": "the_session_id", "first_name": null, "last_name": null}'  
        )

        # Assert lambda response
        self.assertDictEqual(expected_response, actual_response)


def _build_event():
    return {
        'queryStringParameters': {
            'state': 'eyJoYXNoZWRfc2Vzc2lvbl90b2tlbiI6ICJ0aGVfaGFzaGVkX3Rva2VuIiwgIm5oc19udW1iZXIiOiAiOTk5OTk5OTk5OSJ9',
            'code': 'the_code'
        },
        'headers': {
            'request_id': 'requestID',
        },
        'requestContext': {
            'authorizer': {
                'principalId': 'blah',
                'session': "{\"user_data\": {\"nhsid_useruid\": \"user_uid\"}, \"session_id\": \"the_session_id\", "
                           "\"session_token\": \"the_session_token\", \"hashed_session_token\": \"the_hashed_token\", "
                           "\"pds_search_params\": {\"search_type\": \"nhs_number\", \"nhs_number\": \"1234567890\"}}"
            }
        }
    }
