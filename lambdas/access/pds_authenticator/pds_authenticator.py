import os
import json
import base64
from time import time

from common.log import log
from common.utils.audit_utils import audit, AuditActions
from common.utils import json_return_message
from common.utils.session_utils import update_session, get_session_from_lambda_event
from pds_authenticator.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils.pds_fhir_utils import get_pds_fhir_initial_access_tokens, perform_pds_fhir_redirect_to_search

PDS_FHIR_AUTH_URL = os.environ.get('PDS_FHIR_AUTH_URL')
PDS_FHIR_CLIENT_SECRET_ID = os.environ.get('PDS_FHIR_CLIENT_SECRET_ID')


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    log({'log_reference': LogReference.PDSCALLBACK0001})
    session = get_session_from_lambda_event(event)
    session_token = session['session_token']

    query_string_params = event.get('queryStringParameters') if event.get('queryStringParameters') else {}
    state = query_string_params.get('state')
    if not state:
        log({'log_reference': LogReference.PDSCALLBACK0002})
        return json_return_message(400, 'State missing from request')

    decoded_state = json.loads(base64.urlsafe_b64decode(state).decode())

    if decoded_state['hashed_session_token'] != session['hashed_session_token']:
        log({'log_reference': LogReference.PDSCALLBACK0003})
        return json_return_message(400, 'State received does not match state sent')

    auth_code = query_string_params.get('code')
    response = get_pds_fhir_initial_access_tokens(auth_code)

    log({'log_reference': LogReference.PDSCALLBACK0007})
    now = int(time())
    update_session(
        session_token,
        {
            'pds_access_token': response['access_token'],
            'pds_access_token_expiry': now + int(response['expires_in']),
            'pds_refresh_token': response['refresh_token'],
            'pds_reauthenticate_timestamp': now + int(response['refresh_token_expires_in'])
        },
        delete_attributes=['hashed_session_token']
    )

    log({'log_reference': LogReference.PDSCALLBACK0008})

    audit(session=session, action=AuditActions.PDS_LOGIN)

    search_params = session.get('pds_search_params', {})
    return perform_pds_fhir_redirect_to_search(search_params, session_token)
