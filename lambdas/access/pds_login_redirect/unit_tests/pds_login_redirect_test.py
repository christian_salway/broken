from ddt import ddt, data, unpack
from unittest import TestCase
from mock import MagicMock, patch, call
from pds_login_redirect.log_references import LogReference
from pds_login_redirect.pds_login_redirect import (
    lambda_handler,
    hash_token_and_update_session,
    build_authenticate_redirect,
    store_pds_search_params
)

MODULE_NAME = 'pds_login_redirect.pds_login_redirect'


@ddt
@patch(f'{MODULE_NAME}.log')
@patch(f'{MODULE_NAME}.create_set_cookie_header')
@patch(f'{MODULE_NAME}.build_authenticate_redirect')
@patch(f'{MODULE_NAME}.json')
@patch(f'{MODULE_NAME}.get_secret')
@patch(f'{MODULE_NAME}.hash_token_and_update_session')
@patch(f'{MODULE_NAME}.time')
@patch(f'{MODULE_NAME}.get_session_from_lambda_event')
@patch(f'{MODULE_NAME}.update_session')
@patch('os.environ', {
    'PDS_FHIR_AUTH_URL': 'https://AUTH_URL',
    'PDS_FHIR_CLIENT_SECRET_ID': 'SECRET_ID',
    'ENVIRONMENT_NAME': 'ENVIRONMENT',
    'DNS_NAME': 'environment.example.org',
    'PDS_FHIR_CALLBACK_URL': 'https://callback.example.org/callback'
})
class TestPDSLoginRedirect(TestCase):

    def setUp(self):
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    @patch(f'{MODULE_NAME}.format_query_string')
    @patch(f'{MODULE_NAME}.encode_state')
    def test_build_authenticate_redirect_correctly_builds_redirect(
        self,
        encode_mock,
        format_mock,
        update_session_mock,
        get_session_mock,
        time_mock,
        hash_token_mock,
        get_secrets_mock,
        json_secrets_mock,
        build_redirect_mock,
        set_cookie_mock,
        log_mock
    ):
        encode_mock.return_value = 'encoded state'
        format_mock.return_value = 'formatted'
        client_id = 'test client id'
        hashed_session_token = 'hashed token'
        expected_state = {
            'environment': 'ENVIRONMENT',
            'service': 'pds_fhir',
            'hashed_session_token': hashed_session_token
        }
        expected_params = {
            'redirect_uri': 'https%3A%2F%2Fcallback.example.org%2Fcallback',
            'response_type': 'code',
            'client_id': client_id,
            'state': 'encoded state'
        }
        expected_response = 'https://AUTH_URL/authorize?formatted'

        actual_response = build_authenticate_redirect(client_id, hashed_session_token)
        self.assertEqual(expected_response, actual_response)
        encode_mock.assert_called_once_with(expected_state)
        format_mock.assert_called_once_with(expected_params)

    @patch(f'{MODULE_NAME}.get_salted_and_hashed_value')
    def test_hash_token_and_update_session_correctly_updates_session_and_returns_hashed_token(
        self,
        hash_token_mock,
        update_session_mock,
        get_session_mock,
        time_mock,
        other_hash_token_mock,
        get_secrets_mock,
        json_secrets_mock,
        build_redirect_mock,
        set_cookie_mock,
        log_mock
    ):
        hashed_session_token = 'hashed'
        session_token = 'token'
        hash_token_mock.return_value = hashed_session_token

        actual_hashed_token = hash_token_and_update_session(session_token)
        self.assertEqual(hashed_session_token, actual_hashed_token)
        hash_token_mock.assert_called_once_with(session_token)
        update_session_mock.assert_called_once_with(session_token, {'hashed_session_token': hashed_session_token})
        log_mock.assert_called_once_with({'log_reference': LogReference.PDSAUTH0004})
        self.assertEqual(1, log_mock.call_count)

    @patch(f'{MODULE_NAME}.json_return_message')
    def test_pds_login_redirect_returns_400_when_parameters_missing(
        self,
        return_mock,
        update_session_mock,
        get_session_mock,
        time_mock,
        hash_token_mock,
        get_secrets_mock,
        json_secrets_mock,
        build_redirect_mock,
        set_cookie_mock,
        log_mock
    ):
        get_session_mock.return_value = {'session_token': 'token'}
        return_mock.return_value = 'response'
        actual_response = self.unwrapped_handler({}, {})
        self.assertEqual('response', actual_response)
        get_session_mock.assert_called_once_with({})
        return_mock.assert_called_once_with(400, 'Required parameter(s) not supplied')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.PDSAUTH0001}),
            call({'log_reference': LogReference.PDSAUTH0002})
        ])
        self.assertEqual(2, log_mock.call_count)

    def test_pds_login_redirect_redirects_to_authorize_if_session_has_no_access_token(
        self,
        update_session_mock,
        get_session_mock,
        time_mock,
        hash_token_mock,
        get_secrets_mock,
        json_secrets_mock,
        build_redirect_mock,
        set_cookie_mock,
        log_mock
    ):
        test_event = {
            'queryStringParameters': {
                'nhs_number': 'test',
                'search_type': 'nhs_number'
            }
        }
        hashed_session_token = 'hashed'
        get_session_mock.return_value = {'session_token': 'token'}
        hash_token_mock.return_value = hashed_session_token
        get_secrets_mock.return_value = 'secrets'
        json_secrets_mock.loads.return_value = {'client_id': 'blah'}
        build_redirect_mock.return_value = 'redirect'
        set_cookie_mock.return_value = 'cookie'
        expected_response = {
            'statusCode': 302,
            'headers': {
                'Location': 'redirect',
                'Set-Cookie': 'cookie'
            }
        }
        expected_update_session_call = (
            'token',
            {
                'pds_search_params': {
                    'search_type': 'nhs_number',
                    'nhs_number': 'test'
                }
            }
        )

        actual_response = self.unwrapped_handler(test_event, {})
        self.assertEqual(expected_response, actual_response)
        get_session_mock.assert_called_once_with(test_event)
        get_secrets_mock.assert_called_once_with('SECRET_ID', secret_name_is_key=False)
        json_secrets_mock.loads.assert_called_once_with('secrets')
        hash_token_mock.assert_called_once_with('token')
        build_redirect_mock.assert_called_once_with('blah', 'hashed')
        set_cookie_mock.assert_called_once_with('token', use_strict=False)
        update_session_mock.assert_called_once_with(*expected_update_session_call)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.PDSAUTH0001}),
            call({'log_reference': LogReference.PDSAUTH0003}),
            call({'log_reference': LogReference.PDSAUTH0005}),
            call({'log_reference': LogReference.PDSAUTH0006})
        ])
        self.assertEqual(4, log_mock.call_count)

    def test_pds_login_redirect_redirects_to_authorize_if_access_token_expired(
        self,
        update_session_mock,
        get_session_mock,
        time_mock,
        hash_token_mock,
        get_secrets_mock,
        json_secrets_mock,
        build_redirect_mock,
        set_cookie_mock,
        log_mock
    ):
        test_event = {
            'queryStringParameters': {
                'nhs_number': 'test',
                'search_type': 'nhs_number'
            }
        }
        hashed_session_token = 'hashed'
        get_session_mock.return_value = {
            'session_token': 'token',
            'pds_access_token': 'access_token',
            'pds_reauthenticate_timestamp': 1111
        }
        time_mock.return_value = 2222
        hash_token_mock.return_value = hashed_session_token
        get_secrets_mock.return_value = 'secrets'
        json_secrets_mock.loads.return_value = {'client_id': 'blah'}
        build_redirect_mock.return_value = 'redirect'
        set_cookie_mock.return_value = 'cookie'
        expected_response = {
            'statusCode': 302,
            'headers': {
                'Location': 'redirect',
                'Set-Cookie': 'cookie'
            }
        }
        expected_update_session_call = (
            'token',
            {
                'pds_search_params': {
                    'search_type': 'nhs_number',
                    'nhs_number': 'test'
                }
            }
        )

        actual_response = self.unwrapped_handler(test_event, {})
        self.assertEqual(expected_response, actual_response)
        get_session_mock.assert_called_once_with(test_event)
        get_secrets_mock.assert_called_once_with('SECRET_ID', secret_name_is_key=False)
        json_secrets_mock.loads.assert_called_once_with('secrets')
        hash_token_mock.assert_called_once_with('token')
        build_redirect_mock.assert_called_once_with('blah', 'hashed')
        set_cookie_mock.assert_called_once_with('token', use_strict=False)
        update_session_mock.assert_called_once_with(*expected_update_session_call)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.PDSAUTH0001}),
            call({'log_reference': LogReference.PDSAUTH0003}),
            call({'log_reference': LogReference.PDSAUTH0005}),
            call({'log_reference': LogReference.PDSAUTH0006})
        ])
        self.assertEqual(4, log_mock.call_count)

    @patch('common.utils.pds_fhir_utils.create_set_cookie_header', MagicMock(return_value='cookie'))
    def test_pds_login_redirect_redirects_to_webapp_if_user_authenticated(
        self,
        update_session_mock,
        get_session_mock,
        time_mock,
        hash_token_mock,
        get_secrets_mock,
        json_secrets_mock,
        build_redirect_mock,
        set_cookie_mock,
        log_mock
    ):
        test_event = {
            'queryStringParameters': {
                'search_type': 'nhs_number',
                'nhs_number': 'test'
            }
        }
        get_session_mock.return_value = {
            'session_token': 'token',
            'pds_access_token': 'access_token',
            'pds_reauthenticate_timestamp': 3333
        }
        time_mock.return_value = 2222
        expected_response = {
            'statusCode': 302,
            'headers': {
                'Location': 'https://environment.example.org/#/results?search_type=nhs_number&nhs_number=test',
                'Set-Cookie': 'cookie'
            }
        }
        expected_update_session_call = (
            'token',
            {
                'pds_search_params': {
                    'search_type': 'nhs_number',
                    'nhs_number': 'test'
                }
            }
        )

        actual_response = self.unwrapped_handler(test_event, {})
        self.assertEqual(expected_response, actual_response)
        get_session_mock.assert_called_once_with(test_event)
        update_session_mock.assert_called_once_with(*expected_update_session_call)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.PDSAUTH0001}),
            call({'log_reference': LogReference.PDSAUTH0007})
        ])
        self.assertEqual(2, log_mock.call_count)

    @unpack
    @data(
        (
            {
                'search_type': 'nhs_number',
                'nhs_number': 'TEST'
            },
            {
                'search_type': 'nhs_number',
                'nhs_number': 'TEST'
            },
            {
                'pds_search_params': {
                    'search_type': 'nhs_number',
                    'nhs_number': 'TEST'
                }
            }
        ),
        (
            {
                'search_type': 'nhs_number',
                'nhs_number': 'TEST',
                'ignore_me': 'please'
            },
            {
                'search_type': 'nhs_number',
                'nhs_number': 'TEST'
            },
            {
                'pds_search_params': {
                    'search_type': 'nhs_number',
                    'nhs_number': 'TEST'
                }
            }
        ),
        (
            {
                'nhs_number': 'TEST'
            },
            {},
            None
        )
    )
    def test_store_pds_search_params(
        self,
        query_string_parameters,
        expected_response,
        expected_update_session_call,
        update_session_mock,
        get_session_mock,
        time_mock,
        hash_token_mock,
        get_secrets_mock,
        json_secrets_mock,
        build_redirect_mock,
        set_cookie_mock,
        log_mock
    ):
        actual_response = store_pds_search_params(query_string_parameters, 'TOKEN123')
        self.assertDictEqual(actual_response, expected_response)
        if (expected_update_session_call):
            update_session_mock.assert_called_with('TOKEN123', expected_update_session_call)
