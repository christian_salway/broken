THIS LAMBDA IS A PROOF OF CONCEPT
It will need to be improved for actual use in the system

This lambda authenticates with the PDS FHIR API if the user is not already authenticated, otherwise it redirects to the webapp. An nhs nnumber is required as a query parameter

Request
`/auth/pds-auth?{nhs_number}`

Response
```
if the user is already authenticated
302 redirect to webapp/#/pds-participant/{nhs_number}/summary
if the user is not authenticated
302 redirect to pds_url/oauth2/authorize?redirect_uri={}&response_type={}&client_id={}&state={}
```