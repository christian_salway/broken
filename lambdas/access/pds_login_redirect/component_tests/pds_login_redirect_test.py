from time import time
from unittest import TestCase
from mock import patch, call, Mock
from common.log_references import CommonLogReference
from pds_login_redirect.log_references import LogReference
from common.utils.dynamodb_access.table_names import TableNames

with patch('boto3.resource') as resource_mock:
    from pds_login_redirect.pds_login_redirect import lambda_handler

time_mock = Mock(wraps=time)
time_mock.return_value = 1601392160

log_patcher = Mock()
lambda_wrapper_log_patcher = Mock()
sessions_utils_log_patcher = Mock()


@patch('time.time', time_mock)
@patch('os.environ', {
    'PDS_FHIR_AUTH_URL': 'https://AUTH_URL',
    'PDS_FHIR_CLIENT_SECRET_ID': 'SECRET_ID',
    'ENVIRONMENT_NAME': 'ENVIRONMENT',
    'DNS_NAME': 'environment.example.org',
    'PDS_FHIR_CALLBACK_URL': 'https://callback.example.org/callback',
    'SESSION_TABLE_NAME': 'session_table_name'
})
@patch('pds_login_redirect.pds_login_redirect.log', log_patcher)
@patch('common.utils.lambda_wrappers.log', lambda_wrapper_log_patcher)
@patch('common.utils.session_utils.log', sessions_utils_log_patcher)
class TestPDSLoginRedirect(TestCase):
    def setUp(self):
        log_patcher.reset_mock()
        lambda_wrapper_log_patcher.reset_mock()
        sessions_utils_log_patcher.reset_mock()
        self.test_context = Mock()
        self.test_context.function_name = 'pds_login_redirect'

    def test_pds_login_redirect_returns_400_when_no_nhs_number_supplied(self):
        # Arrange
        log_patcher.getLogger.return_value = log_patcher
        test_session = '{"session_token": "token"}'
        test_event = {
            'requestContext': {'authorizer': {
                'session': test_session,
                'principalId': 'session_id',
                }},
            'headers': {'request_id': 'request_id'}
        }
        expected_response = {
            'body': '{"message": "Required parameter(s) not supplied"}',
            'headers': {
                'Content-Type': 'application/json',
                'Strict-Transport-Security': 'max-age=1576800',
                'X-Content-Type-Options': 'nosniff'
            },
            'isBase64Encoded': False,
            'statusCode': 400
        }

        # Act
        actual_response = lambda_handler(test_event, self.test_context)

        # Assert
        self.assertEqual(expected_response, actual_response)
        self.assertEqual(2, log_patcher.call_count)
        self.assertEqual(3, lambda_wrapper_log_patcher.call_count)
        sessions_utils_log_patcher.assert_not_called()
        log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PDSAUTH0001}),
            call({'log_reference': LogReference.PDSAUTH0002})
        ])

        lambda_wrapper_log_patcher.assert_has_calls([
            call(CommonLogReference.LAMBDA0000),
            call({'log_reference': CommonLogReference.LAMBDA0007, 'assigned_workgroups': []}),
            call(CommonLogReference.LAMBDA0001)
        ])

    @patch('common.utils.secrets_manager_utils.Session')
    @patch('common.utils.session_utils.time', Mock(return_value=1601392160))
    @patch('common.utils.session_utils.dynamodb_update_item')
    @patch('common.utils.session_utils.secrets')
    def test_pds_login_redirects_to_authorize_when_user_not_authenticated(
            self, salt_mock, session_update_mock, secrets_session_mock):
        # Arrange
        log_patcher.getLogger.return_value = log_patcher
        test_session = '{"session_token": "token"}'
        test_event = {
            'requestContext': {'authorizer': {
                'session': test_session,
                'principalId': 'session_id',
                }},
            'headers': {'request_id': 'request_id'},
            'queryStringParameters': {
                'nhs_number': 'test',
                'search_type': 'nhs_number'
            }
        }
        salt_mock.token_bytes.return_value = b'salt'
        expected_hashed_token = '9e58c2ec06bd89c14d683dd17ce447827cf3e4d919944d31206e7aaf8896d35c013da098d7e74d101babf2a3e6162b3cda0f433045badefcd26acd01c25a783d' 
        session_update_mock.return_value = {'Attributes': {}}
        get_secret_mock = Mock(return_value={'SecretString': '{"client_id": "id"}'})
        secrets_session_mock.return_value.client.return_value.get_secret_value = get_secret_mock
        expected_encoded_state = 'eyJlbnZpcm9ubWVudCI6ICJFTlZJUk9OTUVOVCIsICJzZXJ2aWNlIjogInBkc19maGlyIiwgImhhc2hlZF9zZXNzaW9uX3Rva2VuIjogIjllNThjMmVjMDZiZDg5YzE0ZDY4M2RkMTdjZTQ0NzgyN2NmM2U0ZDkxOTk0NGQzMTIwNmU3YWFmODg5NmQzNWMwMTNkYTA5OGQ3ZTc0ZDEwMWJhYmYyYTNlNjE2MmIzY2RhMGY0MzMwNDViYWRlZmNkMjZhY2QwMWMyNWE3ODNkIn0=' 
        expected_redirect_url = f'https://AUTH_URL/authorize?redirect_uri=https%3A%2F%2Fcallback.example.org%2Fcallback&response_type=code&client_id=id&state={expected_encoded_state}' 
        expected_response = {
            'statusCode': 302,
            'headers': {
                'Location': expected_redirect_url,
                'Set-Cookie': 'sp_session_cookie=token; expires=Tue, 29 Sep 2020 23:09:20 GMT; Path=/; Secure'
            }
        }

        # Act
        actual_response = lambda_handler(test_event, self.test_context)

        # Assert
        self.assertEqual(expected_response, actual_response)
        session_update_mock.assert_has_calls(
            [
                call(
                    TableNames.SESSIONS,
                    {
                        'Key': {'session_token': 'token'},
                        'UpdateExpression': 'SET #pds_search_params = :pds_search_params, #expires = :expires',
                        'ExpressionAttributeValues': {
                            ':pds_search_params': {
                                'search_type': 'nhs_number',
                                'nhs_number': 'test'
                            },
                            ':expires': 1601392760
                        },
                        'ExpressionAttributeNames': {
                            '#pds_search_params': 'pds_search_params',
                            '#expires': 'expires'
                        },
                        'ReturnValues': 'ALL_NEW'
                    }
                ),
                call(
                    TableNames.SESSIONS,
                    {
                        'Key': {'session_token': 'token'},
                        'UpdateExpression': 'SET #hashed_session_token = :hashed_session_token, #expires = :expires',
                        'ExpressionAttributeValues': {
                            ':hashed_session_token': '9e58c2ec06bd89c14d683dd17ce447827cf3e4d919944d31206e7aaf8896d35c013da098d7e74d101babf2a3e6162b3cda0f433045badefcd26acd01c25a783d', 
                            ':expires': 1601392760
                        },
                        'ExpressionAttributeNames': {
                            '#hashed_session_token': 'hashed_session_token',
                            '#expires': 'expires'
                        },
                        'ReturnValues': 'ALL_NEW'
                    }
                )
            ]
        )
        get_secret_mock.assert_called_once_with(SecretId='SECRET_ID')
        self.assertEqual(5, log_patcher.call_count)
        self.assertEqual(3, lambda_wrapper_log_patcher.call_count)
        self.assertEqual(4, sessions_utils_log_patcher.call_count)
        log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PDSAUTH0001}),
            call({'log_reference': LogReference.PDSAUTH0003}),
            call({'log_reference': LogReference.PDSAUTH0004}),
            call({'log_reference': LogReference.PDSAUTH0005}),
            call({'log_reference': LogReference.PDSAUTH0006}),
        ])
        lambda_wrapper_log_patcher.assert_has_calls([
            call(CommonLogReference.LAMBDA0000),
            call({'log_reference': CommonLogReference.LAMBDA0007, 'assigned_workgroups': []}),
            call(CommonLogReference.LAMBDA0001)
        ])
        sessions_utils_log_patcher.assert_has_calls([
            call({'log_reference': CommonLogReference.SESSION0004}),
            call({'log_reference': CommonLogReference.SESSION0005}),
        ])

    @patch('pds_login_redirect.pds_login_redirect.time', Mock(return_value=1601392160))
    @patch('common.utils.session_utils.time', Mock(return_value=1601392160))
    @patch('pds_login_redirect.pds_login_redirect.update_session')
    def test_pds_login_redirects_to_webapp_when_user_authenticated(
        self,
        update_session_mock,
    ):
        log_patcher.getLogger.return_value = log_patcher
        test_session = '{"session_token": "token", "pds_access_token": "access_token", "pds_reauthenticate_timestamp": 1601392161 }' 
        test_event = {
            'requestContext': {'authorizer': {
                'session': test_session,
                'principalId': 'session_id',
                }},
            'headers': {'request_id': 'request_id'},
            'queryStringParameters': {
                'nhs_number': 'test',
                'search_type': 'nhs_number'
            }
        }
        expected_response = {
            'statusCode': 302,
            'headers': {
                'Location': 'https://environment.example.org/#/results?search_type=nhs_number&nhs_number=test',
                'Set-Cookie': 'sp_session_cookie=token; expires=Tue, 29 Sep 2020 23:09:20 GMT; Path=/; Secure'
            }
        }

        # Act
        actual_response = lambda_handler(test_event, self.test_context)

        # Assert
        self.assertEqual(expected_response, actual_response)
        self.assertEqual(2, log_patcher.call_count)
        self.assertEqual(3, lambda_wrapper_log_patcher.call_count)
        sessions_utils_log_patcher.assert_not_called()
        log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PDSAUTH0001}),
            call({'log_reference': LogReference.PDSAUTH0007}),
        ])
        lambda_wrapper_log_patcher.assert_has_calls([
            call(CommonLogReference.LAMBDA0000),
            call({'log_reference': CommonLogReference.LAMBDA0007, 'assigned_workgroups': []}),
            call(CommonLogReference.LAMBDA0001)
        ])
        update_session_mock.assert_called_with(
            'token',
            {
                'pds_search_params': {
                    'search_type': 'nhs_number',
                    'nhs_number': 'test'
                }
            }
        )


def get_log_call_object(log_reference, log_message, log_level=20):
    call_string = '{"log_reference": "'+log_reference+'", "message": "'+log_message+'"}'
    return call(log_level, call_string, exc_info=False)
