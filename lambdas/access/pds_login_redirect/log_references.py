import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    PDSAUTH0001 = (logging.INFO, 'Received request to authenticate with pds.')
    PDSAUTH0002 = (logging.ERROR, 'Required search parameter(s) missing from request.')
    PDSAUTH0003 = (logging.INFO, 'User is not authenticated with PDS.')
    PDSAUTH0004 = (logging.INFO, 'Hashing session token and updating session.')
    PDSAUTH0005 = (logging.INFO, 'Getting PDS client secret.')
    PDSAUTH0006 = (logging.INFO, 'Redirecting to PDS authorize endpoint.')
    PDSAUTH0007 = (logging.INFO, 'User is authenticated with PDS. Redirecting to PDS participant search page.')
    PDSAUTH0008 = (logging.INFO, 'Issuing dummy token for sandbox environment')
