import os
import json
from time import time
from typing import Dict
from urllib.parse import quote_plus

from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_message
from common.utils.session_utils import update_session, \
     get_salted_and_hashed_value, encode_state, get_session_from_lambda_event
from common.utils.secrets_manager_utils import get_secret
from common.utils.api_utils import format_query_string
from common.utils.cookie_management_utils import create_set_cookie_header
from common.utils.pds_fhir_utils import perform_pds_fhir_redirect_to_search
from pds_login_redirect.log_references import LogReference


PDS_SEARCH_PARAMS = {
    'search_type': True,
    'nhs_number': False,
    'first_name': False,
    'last_name': False,
    'date_of_birth': False,
    'postcode': False
}


def get_pds_fhir_auth_url():
    return os.environ.get('PDS_FHIR_AUTH_URL')


def get_pds_fhir_client_secret_id():
    return os.environ.get('PDS_FHIR_CLIENT_SECRET_ID')


def get_pds_fhir_callback_url():
    return os.environ.get('PDS_FHIR_CALLBACK_URL')


def get_environment():
    return os.environ.get('ENVIRONMENT_NAME')


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    log({'log_reference': LogReference.PDSAUTH0001})
    session = get_session_from_lambda_event(event)
    session_token = session['session_token']

    query_string_parameters = event.get('queryStringParameters', {})
    search_params = store_pds_search_params(query_string_parameters, session_token)
    if not search_params:
        log({'log_reference': LogReference.PDSAUTH0002})
        return json_return_message(400, 'Required parameter(s) not supplied')

    now = time()
    if not session.get('pds_access_token') or session['pds_reauthenticate_timestamp'] < now:
        log({'log_reference': LogReference.PDSAUTH0003})
        hashed_session_token = hash_token_and_update_session(session_token)
        log({'log_reference': LogReference.PDSAUTH0005})
        client_secrets = json.loads(get_secret(get_pds_fhir_client_secret_id(), secret_name_is_key=False))
        log({'log_reference': LogReference.PDSAUTH0006})
        redirect_url = build_authenticate_redirect(
            client_secrets['client_id'],
            hashed_session_token
        )
        return {
            'statusCode': 302,
            'headers': {
                'Location': redirect_url,
                'Set-Cookie': create_set_cookie_header(session_token, use_strict=False)
            }
        }

    log({'log_reference': LogReference.PDSAUTH0007})
    return perform_pds_fhir_redirect_to_search(search_params, session_token, True)


def store_pds_search_params(query_string_parameters: Dict, session_token: str) -> Dict:
    if not query_string_parameters:
        return {}

    params_to_store = {}

    for param, required in PDS_SEARCH_PARAMS.items():
        value = query_string_parameters.get(param)
        if value:
            params_to_store[param] = value
        elif required:
            return {}

    update_session(session_token, {'pds_search_params': params_to_store})
    return params_to_store


def hash_token_and_update_session(session_token):
    log({'log_reference': LogReference.PDSAUTH0004})
    hashed_session_token = get_salted_and_hashed_value(session_token)
    update_session(session_token, {'hashed_session_token': hashed_session_token})
    return hashed_session_token


def build_authenticate_redirect(client_id, hashed_session_token):
    state = {
        'environment': get_environment(),
        'service': 'pds_fhir',
        'hashed_session_token': hashed_session_token
    }

    params = {
        'redirect_uri': quote_plus(get_pds_fhir_callback_url()),
        'response_type': 'code',
        'client_id': client_id,
        'state': encode_state(state)
    }

    return f"{get_pds_fhir_auth_url()}/authorize?{format_query_string(params)}"
