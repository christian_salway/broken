from unittest import TestCase
from mock import patch, Mock


class TestGetAccessGroups(TestCase):

    @classmethod
    def setUpClass(cls, *args):
        cls.log_patcher = patch('get_access_groups.get_access_groups.log')

        # Setup lambda module
        import get_access_groups.get_access_groups as _get_access_groups
        global get_access_groups_module
        get_access_groups_module = _get_access_groups

    def setUp(self):
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    def test_get_access_groups(self):
        event = self._build_event()
        context = Mock()
        context.function_name = ''

        actual_response = get_access_groups_module.lambda_handler(event, context)

        # Assert access groups extracted from context
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": {"pnl": true, "csas": false}}',
            'isBase64Encoded': False
        }

        self.assertDictEqual(expected_response, actual_response)

    def _build_event(self):
        return {
            'httpMethod': 'GET',
            'resource': '/api/users/access-groups',
            'headers': {
                'request_id': 'requestID',
            },
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': "{\"session_id\": \"6ag345-264gd5-sg82\", \"access_groups\": {\"pnl\": true, \"csas\": false}}"  
                }
            }
        }
