import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    GETACCGRP0001 = (logging.INFO, 'Session or access groups missing.')
    GETACCGRP0002 = (logging.INFO, 'Returning access groups.')
