from unittest import TestCase
from unittest.mock import patch, Mock

from get_access_groups.get_access_groups import lambda_handler
from common.test_assertions.api_assertions import assertApiResponse


@patch('get_access_groups.get_access_groups.log', Mock())
class TestGetAccessGroups(TestCase):

    def setUp(self):
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    @patch('get_access_groups.get_access_groups.get_session_from_lambda_event')
    def test_get_access_groups_returns_access_groups_from_session(self, get_session_mock):
        session = {'access_groups': {'a_key': 'a_value'}}
        get_session_mock.return_value = session
        expected_response_body = {'data': {'a_key': 'a_value'}}

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 200, expected_response_body, actual_response)

    @patch('get_access_groups.get_access_groups.get_session_from_lambda_event')
    def test_get_access_groups_returns_403_give_no_access_groups_in_session(self, get_session_mock):
        session = {'a_key': 'a_value'}
        get_session_mock.return_value = session
        expected_response_body = {'message': 'Session missing, expired or not logged in.'}

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 403, expected_response_body, actual_response)

    @patch('get_access_groups.get_access_groups.get_session_from_lambda_event')
    def test_get_access_groups_returns_403_give_no_session(self, get_session_mock):
        get_session_mock.return_value = None
        expected_response_body = {'message': 'Session missing, expired or not logged in.'}

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 403, expected_response_body, actual_response)
