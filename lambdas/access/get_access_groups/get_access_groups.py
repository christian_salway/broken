from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_data, json_return_message
from common.log import log
from common.utils.session_utils import get_session_from_lambda_event
from get_access_groups.log_references import LogReference


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):

    session = get_session_from_lambda_event(event)

    if not session or not session.get('access_groups'):
        log({'log_reference': LogReference.GETACCGRP0001})
        return json_return_message(403, 'Session missing, expired or not logged in.')

    log({'log_reference': LogReference.GETACCGRP0002})
    return json_return_data(200, session['access_groups'])
