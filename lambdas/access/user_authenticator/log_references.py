import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    UAUTH0001 = (logging.INFO, 'Received redirect from NHS Identity.')
    UAUTH0002 = (logging.ERROR, 'Session missing.')
    UAUTH0003 = (logging.ERROR, 'Invalid query parameters in request.')
    UAUTH0004 = (logging.INFO, 'Query parameters valid.')
    UAUTH0005 = (logging.INFO, 'No nonce found.')
    UAUTH0006 = (logging.ERROR, 'Could not fetch access token or id token.')
    UAUTH0007 = (logging.ERROR, 'Could not verify id token')
    UAUTH0008 = (logging.ERROR, 'Nonce from request does not match known nonce.')
    UAUTH0009 = (logging.INFO, 'Request nonce is correct.')
    UAUTH0010 = (logging.INFO, 'User details could not be fetched from NHS Identity.')
    UAUTH0011 = (logging.ERROR, 'Unexpected generic error during execution.')
    UAUTH0012 = (logging.INFO, 'Execution is complete, returning redirect to role selection.')
    UAUTH0013 = (logging.INFO, 'Returning redirect to logout.')
    UAUTH0014 = (logging.ERROR, 'User does not exist in allowed list')
    UAUTH0015 = (logging.INFO, 'User exists in allowed list')
    UAUTH0016 = (logging.INFO, 'User does not have any roles - logging them out of system.')
    UAUTH0017 = (logging.ERROR, 'Subject claim in user details is not identical to subject claim in ID token')
    UAUTH0018 = (logging.INFO, 'Subject claim in user details is identical to subject claim in ID token')
    UAUTH0019 = (logging.ERROR, 'Session value in state could not be validated')
    UAUTH0020 = (logging.INFO, 'Session value in state successfully validated')
    UAUTH0021 = (logging.INFO, 'User does not meet the required assurance level.')
    UAUTH0022 = (logging.INFO, 'Fetching config from nhsid')
    UAUTH0023 = (logging.INFO, 'User does not have any roles with the cervical screening workgroup '
                               '- logging them out of system.')

    VETOK0003 = (logging.INFO, 'Verified id token.')
    VETOK0004 = (logging.ERROR, 'Unable to verify id token.')

    ACTOK0001 = (logging.INFO, 'Fetching access token.')
    ACTOK0002 = (logging.INFO, 'Fetching client secret.')
    ACTOK0003 = (logging.INFO, 'Successfully fetched client secret.')
    ACTOK0004 = (logging.INFO, 'Requesting access token from NHS Identity.')
    ACTOK0005 = (logging.ERROR, 'Access token request failed.')
    ACTOK0006 = (logging.INFO, 'Access token request completed successfully.')
    ACTOK0007 = (logging.INFO, 'Creating new user.')
    ACTOK0008 = (logging.INFO, 'User validated.')

    UDATA0001 = (logging.INFO, 'Requesting user data from NHS Identity.')
    UDATA0002 = (logging.ERROR, 'Error requesting user data from NHS Identity.')
    UDATA0003 = (logging.INFO, 'Successfully retrieved user data.')
