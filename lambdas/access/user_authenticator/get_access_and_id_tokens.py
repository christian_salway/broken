import json
import boto3
from common.log import log
from user_authenticator.log_references import LogReference
import requests


def get_client_secret(client_secret_id):
    log({'log_reference': LogReference.ACTOK0001})
    client = boto3.session.Session().client(
        service_name='secretsmanager',
        region_name='eu-west-2'
    )
    log({'log_reference': LogReference.ACTOK0002})
    secret_response = client.get_secret_value(SecretId=client_secret_id)
    log({'log_reference': LogReference.ACTOK0003})
    return json.loads(secret_response['SecretString'])['secret']


def get_token_data(response):

    json_data = response.json()

    if response.status_code == 200:
        access_token = json_data.get('access_token')
        id_token = json_data.get('id_token')
        log({'log_reference': LogReference.ACTOK0006})
        return access_token, id_token

    log({
        'log_reference': LogReference.ACTOK0005,
        'response_status_code': response.status_code,
        'error': json_data.get('error_description') or 'No error description provided',
    })

    return None, None


def get_access_and_id_tokens(config, auth_code, nhsid_token_uri):

    client_secret_id = config['NHSID_CLIENT_SECRET_ID']
    screening_client_id = config['SCREENING_CLIENT_ID']
    redirect_uri = config['REDIRECT_URI']
    timeout = config['NHSID_CONNECTION_TIMEOUT']

    client_secret = get_client_secret(client_secret_id)

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }
    parameters = {
        'grant_type': 'authorization_code',
        'code': auth_code,
        'redirect_uri': redirect_uri,
        'client_id': screening_client_id,
        'client_secret': client_secret
    }

    log({'log_reference': LogReference.ACTOK0004})
    response = requests.post(nhsid_token_uri, headers=headers, data=parameters, timeout=timeout)
    return get_token_data(response)
