from common.utils.user_utils import get_user_by_nhsid_useruid, create_user
from user_authenticator.log_references import LogReference
from common.log import log


def validate_user_details(user_details, claims, allowlist_enabled):
    if claims['sub'] != user_details.get('sub'):
        log({
            'log_reference': LogReference.UAUTH0017,
            'user_details_sub': user_details.get('sub'),
            'token_sub': claims['sub']})
        return False

    user = get_user_by_nhsid_useruid(user_details['nhsid_useruid'])
    if not user:
        log({'log_reference': LogReference.ACTOK0007, 'nhsid_useruid': user_details['nhsid_useruid']})
        user = create_user(user_details)

    if not allowlist_enabled:
        log({'log_reference': LogReference.ACTOK0008,
             'nhsid_useruid': user_details['nhsid_useruid'],
             'allowlist': 'disabled'})
        return True

    if not user.get('is_allowed', False):
        log({'log_reference': LogReference.UAUTH0014, 'nhsid_useruid': user_details['nhsid_useruid']})
        return False

    log({'log_reference': LogReference.ACTOK0008,
         'nhsid_useruid': user_details['nhsid_useruid'],
         'allowlist': 'enabled'})

    return True
