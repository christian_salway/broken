from unittest import TestCase
from unittest.mock import patch
from user_authenticator.log_references import LogReference
from user_authenticator.user_authenticator import validate_state

module = 'user_authenticator.user_authenticator'


class TestValidateState(TestCase):

    def setUp(self):
        self.log_patcher = patch(f'{module}.log').start()
        self.get_session_patcher = patch(f'{module}.get_session').start()
        self.session_token = 'hashed_token'
        self.state = 'eyJlbnZpcm9ubWVudCI6ICJlbnZpcm9ubWVudCIsICJoYXNoZWRfc2Vzc2lvbl90b2tlbiI6ICJoYXNoZWRfdG9rZW4ifQ=='
        self.hashed_session_token_key = 'hashed_session_token'

    def tearDown(self):
        self.log_patcher.stop()
        self.get_session_patcher.stop()

    def test_returns_false_if_no_state(self):
        # Arrange
        self.get_session_patcher.return_value = {}
        # Act
        result = validate_state(None, self.session_token, self.hashed_session_token_key)
        # Assert
        self.get_session_patcher.assert_not_called()
        self.log_patcher.assert_called_with({'log_reference': LogReference.UAUTH0019})
        self.assertFalse(result)

    def test_calls_get_session_with_session_token(self):
        # Arrange
        self.get_session_patcher.return_value = {}
        # Act
        validate_state(self.state, self.session_token, self.hashed_session_token_key)
        # Assert
        self.get_session_patcher.assert_called_with(self.session_token)

    def test_returns_false_if_hashed_tokens_do_not_match(self):
        # Arrange
        self.get_session_patcher.return_value = {self.hashed_session_token_key: 'waffles'}
        # Act
        result = validate_state(self.state, self.session_token, self.hashed_session_token_key)
        # Assert
        self.log_patcher.assert_called_with({
                                            'log_reference': LogReference.UAUTH0019,
                                            'expected': 'waffles',
                                            'actual': 'hashed_token'})
        self.assertFalse(result)

    def test_returns_true_if_tokens_match(self):
        # Arrange
        self.get_session_patcher.return_value = {self.hashed_session_token_key: self.session_token}
        # Act
        result = validate_state(self.state, self.session_token, self.hashed_session_token_key)
        # Assert
        self.get_session_patcher.assert_called_with(self.session_token)
        self.log_patcher.assert_called_with({'log_reference': LogReference.UAUTH0020})
        self.assertTrue(result)
