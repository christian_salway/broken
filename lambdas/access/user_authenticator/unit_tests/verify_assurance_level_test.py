from unittest import TestCase
from unittest.mock import patch
from user_authenticator.log_references import LogReference
from user_authenticator.verify_id_token import verify_assurance_level

module = 'user_authenticator.verify_id_token'


class TestValidateUserDetails(TestCase):

    def setUp(self):
        self.log_patcher = patch(f'{module}.log').start()
        self.assurance_level = 'AAL3_ALL'
        self.minimum_assurance_level = 3

    def tearDown(_):
        patch.stopall()

    def test_returns_false_if_user_assurance_level_is_below_the_minimum_required_level(self):
        # Arrange
        # Act
        result = verify_assurance_level('AAL1_USERPASS', self.minimum_assurance_level)
        # Assert
        self.assertFalse(result)
        self.log_patcher.assert_called_with({'log_reference': LogReference.UAUTH0021, 'expected': 3, 'actual': 1})

    def test_returns_false_if_user_assurance_level_is_missing(self):
        # Arrange
        # Act
        result = verify_assurance_level('', self.minimum_assurance_level)
        # Assert
        self.assertFalse(result)
        self.log_patcher.assert_called_with({'log_reference': LogReference.UAUTH0021, 'expected': 3, 'actual': None})

    def test_returns_true_if_user_assurance_level_is_at_the_minimum(self):
        # Arrange
        # Act
        result = verify_assurance_level(self.assurance_level, self.minimum_assurance_level)
        # Assert
        self.assertTrue(result)
