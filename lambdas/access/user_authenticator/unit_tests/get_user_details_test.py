from unittest import TestCase
from unittest.mock import MagicMock, patch
with patch('boto3.resource') as resource_mock:
    from user_authenticator.get_user_details import get_user_details


@patch('user_authenticator.get_user_details.log', MagicMock())
class TestGetUserDetails(TestCase):

    @patch('requests.get')
    @patch('user_authenticator.get_user_details.get_filtered_roles_list_from_user_data')
    def test_get_user_details_returns_user_details_if_successful(self, get_roles_mock, get_request):
        # Arrange
        expected_user = {
            'nhsid_useruid': 'id',
            'first_name': 'given name',
            'last_name': 'family name',
            'user_roles': ['roles'],
            'sub': 'sub',
            'assurance_level': '1',
        }
        response_object = MagicMock()
        response_object.status_code = 200
        response_object.json.return_value = {
            'nhsid_useruid': expected_user['nhsid_useruid'],
            'given_name': expected_user['first_name'],
            'family_name': expected_user['last_name'],
            'nhsid_user_orgs': 'user_orgs',
            'nhsid_nrbac_roles': 'nrbac_roles',
            'sub': 'sub',
            'idassurancelevel': '1'
        }
        get_request.return_value = response_object
        get_roles_mock.return_value = ['roles']

        # Act
        actual_user = get_user_details('111111', 'NHSID_USERINFO_ENDPOINT', 2)

        # Assert
        self.assertEqual(expected_user, actual_user)
        get_roles_mock.assert_called_with('user_orgs', 'nrbac_roles')

    @patch('requests.get')
    def test_get_user_details_returns_None_if_request_unsuccessful(self, get_request):
        # Arrange
        response_object = MagicMock()
        response_object.status_code = 500
        get_request.return_value = response_object
        # Act
        result = get_user_details('11111', 'NHSID_USERINFO_ENDPOINT', 2)
        # Assert
        self.assertIsNone(result)

    @patch('requests.get')
    def test_get_user_details_returns_None_if_request_timesout(self, mock_requests):
        # Arrange
        mock_requests.side_effect = ConnectionError()

        # Act
        with self.assertRaises(ConnectionError):
            response = get_user_details('123456', 'NHSID_USERINFO_ENDPOINT', 2)
            self.assertEqual(None, response)
