import os
import json
from unittest import TestCase
from jwt.api_jwt import encode
from mock import patch, call
from user_authenticator.verify_id_token import verify_id_token
from user_authenticator.log_references import LogReference


module = 'user_authenticator.verify_id_token'


class VerifyIdTokenTests(TestCase):

    def setUp(self):
        self.log_patcher = patch(f'{module}.log').start()

        self.claims = {
            "sub": "240000090895",
            "auditTrackingId": "36f0bc8f-1a54-4384-82e9-579b65bfbb18-4884",
            "iss": "https://am.nhsdev-4.dev.nhsd-esa.net:443/openam/\
                oauth2/realms/root/realms/NHSIdentity/realms/Healthcare",
            "aud": "screening.platform",
            "nonce": "nonce",
            "acr": "AAL1_USERPASS",
            "exp": 9999999999,
            "iat": 1565944491
        }

        self.nonce = 'nonce'
        self.minimum_assurance_level = '1'
        self.discovery_url = 'discovery_url'
        self.audience = 'screening.platform'

        with open(os.path.join(os.path.dirname(__file__), 'test_data/private_key.pem')) as file:
            self.private_key = file.read()

        with open(os.path.join(os.path.dirname(__file__), 'test_data/public_jwks.json')) as file:
            self.public_jwks = json.loads(file.read())

        self.id_token = """eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ijg5ZmYxZmYxLWVjOGItNDJhMC04Y
        zViLWIxZWYwMjMyOGIwNyJ9.eyJpYXQiOjE1ODM4NzcsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUiLCJhdXRoX3RpbWUi
        OjE1ODM4NDcsInZvdCI6IlA5LkNwLkNkIiwidnRtIjoiVE9LRU5fSVNTVUVSL3RydXN0bWFyay9TVUVSIiwibm9uY2Ui
        OiJub25jZSIsInN1YiI6IjEyMzEyMzEyMzEiLCJhY3IiOiJBQUwzX0FOWSIsImV4cCI6MTYxOTg3NywiYXVkIjoiY2xp
        ZW50X2lkIiwiaXNzIjoiVE9LRU5fSVNTVUVSIiwianRpIjoiNzFlMDlhZjAtZDUwMy00ZjE3LWE1NzItZTMxODFhOTJl
        YTQxIn0.TZbo2xuRDprjDEQTvO1G19xHhdwOg-zd4gLMlXxRauLwrnQKKIkO9uCysVwl7wvnIaFi5JR25VkGTb7yi8a8
        j9B9l2kQ3K6PulJtqtGstI5HOCizNlO4WV2AgkYJPQUh_LkP5lqyzaB4gXcCQX2yjg8R31NfY-mEEJU6GkWl27baCug-
        XXKv4Ior8eVa1tWAk13eH45nvp4AZRDhFePPn9njBLpaglNGjQ95IuGitkHdYOZWUPstj1BUP18LXnSWdOmAMAVBTS47
        tzubZVucFsTG9vr4dKVK-SqB6ypFJU01mRb65wDWSSSMlxV-uruo7Q_jclDQt_j8CaO4xJtMVQ"""

        with open(os.path.join(os.path.dirname(__file__), 'test_data/discovery.json')) as file:
            self.discovery_json = json.loads(file.read())

    def tearDown(_):
        patch.stopall()

    def act(self):
        return verify_id_token(self.id_token, self.nonce, self.audience, self.public_jwks.get('keys')[2])

    def test_verify_id_token_returns_None_if_token_cannot_be_verified(self):
        # Arrange
        # Act
        claims = self.act()
        # Assert
        self.assertIsNone(claims)

    def test_verify_id_token_returns_claims_dictionary_if_token_verified(self):
        # Arrange
        self.id_token = encode(self.claims, self.private_key, algorithm='RS256')
        # Act
        actual_claims = self.act()
        # Assert
        self.assertDictEqual(actual_claims, self.claims)

    def test_returns_false_if_nonce_does_not_match(self):
        # Arrange
        self.id_token = encode(self.claims, self.private_key, algorithm='RS256')
        self.nonce = 'wrong_nonce'
        # Act
        actual_claims = self.act()
        # Assert
        self.assertFalse(actual_claims)
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.VETOK0003}),
            call({'log_reference': LogReference.UAUTH0008, 'session_nonce': 'wrong_nonce', 'token_nonce': 'nonce'})])

    def test_returns_none_if_no_algorithm(self):
        # Arrange
        self.id_token = encode({}, headers={'kid': '123'}, key=None, algorithm=None)
        # Act
        actual_claims = self.act()
        # Assert
        self.assertEqual(None, actual_claims)

    def test_returns_none_if_algorithm_not_supported(self):
        # Arrange
        self.id_token = encode({}, headers={'kid': '123'}, key=self.private_key, algorithm="RS256")
        # Act
        actual_claims = self.act()
        # Assert
        self.assertEqual(None, actual_claims)

    def test_returns_sid_if_present_in_token(self):
        # Arrange
        self.claims['sid'] = 'sid'
        self.id_token = encode(self.claims, self.private_key, algorithm='RS256')
        # Act
        actual_claims = self.act()
        # Assert
        self.assertEqual('sid', actual_claims['sid'])

    def test_returns_sub_if_present_in_token(self):
        # Arrange
        self.claims['sub'] = 'sub'
        self.id_token = encode(self.claims, self.private_key, algorithm='RS256')
        # Act
        actual_claims = self.act()
        # Assert
        self.assertEqual('sub', actual_claims['sub'])
