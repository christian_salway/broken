import json
import os
from unittest import TestCase
from unittest.mock import patch

with patch('boto3.resource') as mock_resource:
    from user_authenticator.user_authenticator import get_config_from_environment


class TestGetConfigFromEnvironment(TestCase):

    def setUp(self):
        with open(os.path.join(os.path.dirname(__file__), 'test_data/environment.json')) as file:
            self.env = json.loads(file.read())

    def test_fetches_config_from_environment(self):
        # Arrange
        expected = {
            'LOGGED_IN_REDIRECT_URL': 'LOGGED_IN_REDIRECT_URL',
            'SCREENING_CLIENT_ID': 'SCREENING_CLIENT_ID',
            'NHSID_AUD_ID': 'NHSID_AUD_ID',
            'NHSID_DISCOVERY_URL': 'NHSID_DISCOVERY_URL',
            'NHSID_CLIENT_SECRET_ID': 'NHSID_CLIENT_SECRET_ID',
            'REDIRECT_URI': 'REDIRECT_URI',
            'NHSID_CONNECTION_TIMEOUT': 1,
            'LOGOUT_URL': 'LOGOUT_URL',
            'ALLOW_ONLY_KNOWN_USERS': True,
            'NHSID_MIN_ASSURANCE_LEVEL': 1,
            'AUTHENTICATE_WORKGROUPS': False
        }
        # Act
        result = get_config_from_environment(self.env)

        # Assert
        self.assertEqual(expected, result)

    def test_throws_error_for_missing_required_config(self):
        # Arrange
        required = {
            'LOGGED_IN_REDIRECT_URL',
            'SCREENING_CLIENT_ID',
            'LOGOUT_URL',
            'NHSID_AUD_ID',
            'NHSID_DISCOVERY_URL',
        }

        for item in required:
            del self.env[item]
            with self.assertRaises(KeyError):
                # Act & Assert
                get_config_from_environment(self.env)

    def test_returns_defaults_for_optional_config(self):
        # Arrange
        has_default = {
            'ALLOW_ONLY_KNOWN_USERS',
            'NHSID_MIN_ASSURANCE_LEVEL',
            'AUTHENTICATE_WORKGROUPS'
        }
        expected = {
            'ALLOW_ONLY_KNOWN_USERS': True,
            'NHSID_MIN_ASSURANCE_LEVEL': 3,
            'AUTHENTICATE_WORKGROUPS': True
        }

        for item in has_default:
            del self.env[item]
            # Act
            config = get_config_from_environment(self.env)
            # Assert
            self.assertEqual(expected[item], config[item])
