from unittest import TestCase
from unittest.mock import patch
from user_authenticator.log_references import LogReference
from user_authenticator.user_authenticator import validate_session


class TestValidateSession(TestCase):

    def setUp(self):
        self.log_patcher = patch('user_authenticator.user_authenticator.log').start()

    def tearDown(self):
        self.log_patcher.stop()

    def test_returns_false_if_no_user_details(self):
        # Arrange
        session = None
        # Act
        result = validate_session(session)
        # Assert
        self.assertFalse(result)
        self.log_patcher.assert_called_with({'log_reference': LogReference.UAUTH0002})

    def test_returns_false_if_nonce_is_missing(self):
        # Arrange
        session = {'potatoes': 5}
        # Act
        result = validate_session(session)
        # Assert
        self.assertFalse(result)
        self.log_patcher.assert_called_with({'log_reference': LogReference.UAUTH0005})

    def test_returns_true_if_valid_session(self):
        # Arrange
        session = {'nonce': '12345'}
        # Act
        result = validate_session(session)
        # Assert
        self.assertTrue(result)
        self.log_patcher.assert_called_with({'log_reference': LogReference.UAUTH0020})
