import base64
import json
import os
from common.test_mocks.test_case import PatchTestCase
from unittest.mock import patch, call
from common.utils.audit_utils import AuditActions
from user_authenticator.log_references import LogReference
with patch('boto3.resource') as mock_resource, patch('os.getenv') as environ_mock:
    from user_authenticator.user_authenticator import lambda_handler

# Fixtures
nhsid_useruid = 'id'
nhsid_subject_id = 'nhsid_subject_id'
nhsid_session_id = 'nhsid_session_id'
redirect = 'redirect_url'
nhs_id = 'nhs_id'
client_id = 'client_id'
logout_url = 'logout_url'
session_id = 'session_id'
module = 'user_authenticator.user_authenticator'
updated_session = 'updated_session'
hashed_session_token_key = 'hashed_session_token'
valid_dynamodb_session = {hashed_session_token_key: hashed_session_token_key}
state = base64.urlsafe_b64encode(json.dumps(valid_dynamodb_session).encode('utf-8')).decode()
user_data = {'user_roles': ['blah'], 'sub': 'sub', 'nhsid_useruid': nhsid_useruid,
             'assurance_level': '1', 'activity_codes': ['x']}
claims = {'acr': 'acr', 'sub': nhsid_subject_id, 'sid': nhsid_session_id}
session_token = 'session_token'
redirect_to_logout = 'redirect_to_logout'
public_key = 'public_key'
access_token = 'access_token'
id_token = 'id_token'
auth_code = 'auth_code'
nonce = 'nonce'


class TestUserAuthenticator(PatchTestCase):

    def setUp(self):
        self.config = {
            "LOGGED_IN_REDIRECT_URL": "LOGGED_IN_REDIRECT_URL",
            "SCREENING_CLIENT_ID": "SCREENING_CLIENT_ID",
            "NHSID_AUD_ID": "NHSID_AUD_ID",
            "NHSID_DISCOVERY_URL": "NHSID_DISCOVERY_URL",
            "NHSID_zCLIENT_SECRET_ID": "NHSID_CLIENT_SECRET_ID",
            "REDIRECT_URI": "REDIRECT_URI",
            "NHSID_CONNECTION_TIMEOUT": 1,
            "LOGOUT_URL": "LOGOUT_URL",
            "ALLOW_ONLY_KNOWN_USERS": True,
            "NHSID_MIN_ASSURANCE_LEVEL": 1,
            "AUTHENTICATE_WORKGROUPS": False
        }

        with open(os.path.join(os.path.dirname(__file__), 'test_data/discovery.json')) as file:
            self.nhsid_config = json.loads(file.read())

        self.create_patchers(module, [
          'log', 'audit', 'invalidate_any_duplicate_sessions',
          ['get_config_from_environment', self.config],
          ['discover_nhsid_configuration', self.nhsid_config],
          ['validate_session', True],
          ['return_invalid_parameters', []],
          ['redirect_to_logout', redirect_to_logout],
          ['get_access_and_id_tokens', (access_token, id_token)],
          ['validate_state', True],
          ['verify_id_token', claims],
          ['verify_assurance_level', True],
          ['validate_user_details', True],
          ['get_user_details', user_data],
          ['update_session', updated_session],
          ['get_public_key', public_key],
        ])

        # Arguments
        self.event = {
            'queryStringParameters': {
                'iss': 'nhs_id',
                'client_id': 'client_id',
                'code': auth_code,
                'sub': 'sub',
                'state': state
            },
            'headers': {'Cookie': ''}
        }
        self.context = {}
        self.session = {
            'session_token': session_token,
            'session_id': 'session_id',
            'nonce': nonce
        }

        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__.__wrapped__

    def act(self):
        return self.unwrapped_handler(self.event, self.context, self.session)

    def assert_logout_redirect(self, result):
        self.redirect_to_logout_patcher.assert_called_with('LOGOUT_URL')
        self.assertEqual(redirect_to_logout, result)

    def tearDown(_):
        patch.stopall()

    def test_happy_path(self):
        self.act()

        self.validate_session_patcher.assert_called_with(self.session)
        self.discover_nhsid_configuration_patcher.assert_called_with('NHSID_DISCOVERY_URL')
        self.return_invalid_parameters_patcher.assert_called_with(
            self.event,
            self.nhsid_config['issuer'],
            self.config['SCREENING_CLIENT_ID'])
        self.get_access_and_id_tokens_patcher.assert_called_with(
            self.config,
            auth_code,
            self.nhsid_config['token_endpoint']
        )
        self.validate_state_patcher.assert_called_with(state, session_token, hashed_session_token_key)
        self.get_public_key_patcher.assert_called_with(
            self.nhsid_config,
            self.config,
            id_token)
        self.verify_id_token_patcher.assert_called_with(id_token, nonce, 'NHSID_AUD_ID', public_key)
        self.verify_assurance_level_patcher.assert_called_with(claims['acr'], 1)
        self.get_user_details_patcher.assert_called_with(
            access_token,
            self.nhsid_config['userinfo_endpoint'],
            self.config['NHSID_CONNECTION_TIMEOUT'])
        self.validate_user_details_patcher.assert_called_with(
            user_data,
            claims,
            self.config['ALLOW_ONLY_KNOWN_USERS'])
        self.update_session_patcher.assert_called_with(
            session_token,
            {
                'user_data': user_data,
                'nhsid_useruid': nhsid_useruid,
                'nhsid_session_id': nhsid_session_id,
                'nhsid_subject_id': nhsid_subject_id
            },
            delete_attributes=[hashed_session_token_key])
        self.invalidate_any_duplicate_sessions_patcher.assert_called_with(session_token, nhsid_useruid)

    def test_raises_exception_if_environment_is_misconfigured(self):
        # Arrange
        self.get_config_from_environment_patcher.side_effect = Exception
        # Act
        with self.assertRaises(Exception):
            self.act()
        # Assert
        self.get_config_from_environment_patcher.assert_called_with(os.environ)

    def test_calls_discover_nhsid_configuration_with_discovery_url(self):
        # Arrange
        # Act
        self.act()
        # Assert
        self.discover_nhsid_configuration_patcher.assert_has_calls([call('NHSID_DISCOVERY_URL')])

    def test_returns_false_if_no_user_details(self):
        # Arrange
        self.get_user_details_patcher.return_value = None
        # Act
        result = self.act()
        # Assert
        self.assertEqual('redirect_to_logout', result)
        self.log_patcher.assert_called_with({'log_reference': LogReference.UAUTH0010})

    def test_redirects_to_logout_if_validate_session_returns_false(self):
        # Arrange
        self.validate_session_patcher.return_value = False
        # Act
        result = self.act()
        # Assert
        self.validate_session_patcher.assert_called_with(self.session)
        self.log_patcher.assert_has_calls([
          call({'log_reference': LogReference.UAUTH0001})])
        self.assert_logout_redirect(result)

    def test_redirects_to_logout_if_invalid_parameters(self):
        # Arrange
        self.return_invalid_parameters_patcher.return_value = ['invalid_parameters']
        # Act
        result = self.act()
        # Assert
        self.return_invalid_parameters_patcher.assert_called_with(
            self.event,
            self.nhsid_config['issuer'],
            self.config['SCREENING_CLIENT_ID'])
        self.log_patcher.assert_has_calls([
          call({'log_reference': LogReference.UAUTH0001}),
          call({'log_reference': LogReference.UAUTH0003, 'error': "['invalid_parameters']"})])
        self.assert_logout_redirect(result)

    def test_redirects_to_logout_if_invalid_id_token(self):
        # Arrange
        self.get_access_and_id_tokens_patcher.return_value = access_token, None
        # Act
        result = self.act()
        # Assert
        self.get_access_and_id_tokens_patcher.assert_called_with(
            self.config,
            auth_code,
            self.nhsid_config['token_endpoint'])
        self.log_patcher.assert_has_calls([
          call({'log_reference': LogReference.UAUTH0001}),
          call({'log_reference': LogReference.UAUTH0006})])
        self.assert_logout_redirect(result)

    def test_redirects_to_logout_if_invalid_access_token(self):
        # Arrange
        self.get_access_and_id_tokens_patcher.return_value = None, 'id_token'
        # Act
        result = self.act()
        # Assert
        self.get_access_and_id_tokens_patcher.assert_called_with(
            self.config,
            auth_code,
            self.nhsid_config['token_endpoint'])
        self.log_patcher.assert_has_calls([
          call({'log_reference': LogReference.UAUTH0001}),
          call({'log_reference': LogReference.UAUTH0006})])
        self.assert_logout_redirect(result)

    def test_redirects_to_logout_if_invalid_state(self):
        # Arrange
        self.validate_state_patcher.return_value = False
        # Act
        result = self.act()
        # Assert
        self.validate_state_patcher.assert_called_with(state, session_token, hashed_session_token_key)
        self.log_patcher.assert_has_calls([call({'log_reference': LogReference.UAUTH0001})])
        self.assert_logout_redirect(result)

    def test_redirects_to_logout_if_no_public_key(self):
        # Arrange
        self.get_public_key_patcher.return_value = None
        # Act
        result = self.act()
        # Assert
        self.get_public_key_patcher.assert_called_once_with(
            self.nhsid_config,
            self.config,
            id_token)
        self.assert_logout_redirect(result)

    def test_redirects_to_logout_if_verify_id_token_returns_none(self):
        # Arrange
        self.verify_id_token_patcher.return_value = None
        # Act
        result = self.act()
        # Assert
        self.verify_id_token_patcher.assert_called_with(id_token, nonce, 'NHSID_AUD_ID', public_key)
        self.log_patcher.assert_has_calls([
          call({'log_reference': LogReference.UAUTH0001}),
          call({'log_reference': LogReference.UAUTH0007})])
        self.assert_logout_redirect(result)

    def test_redirects_to_logout_if_verify_assurance_level_returns_false(self):
        # Arrange
        self.verify_assurance_level_patcher.return_value = False
        # Act
        result = self.act()
        # Assert
        self.verify_assurance_level_patcher.assert_called_with(claims['acr'], 1)
        self.log_patcher.assert_called_with({'log_reference': LogReference.UAUTH0001})
        self.redirect_to_logout_patcher.assert_called_with('LOGOUT_URL', 'assuranceLevel')
        self.assertEqual(redirect_to_logout, result)

    def test_redirects_to_logout_if_invalid_user_details(self):
        # Arrange
        self.validate_user_details_patcher.return_value = None
        # Act
        result = self.act()
        # Assert
        self.validate_user_details_patcher.assert_called_with(
            user_data,
            claims,
            self.config['ALLOW_ONLY_KNOWN_USERS'])
        self.log_patcher.assert_has_calls([
          call({'log_reference': LogReference.UAUTH0001})])
        self.assert_logout_redirect(result)

    def test_redirects_to_logout_if_user_has_no_roles(self):
        # Arrange
        self.get_user_details_patcher.return_value = {'user_roles': [], 'assurance_level': '1'}
        # Act
        result = self.act()
        # Assert
        self.get_user_details_patcher.assert_called_with(
            access_token,
            self.nhsid_config['userinfo_endpoint'],
            self.config['NHSID_CONNECTION_TIMEOUT']
        )
        self.audit_patcher.assert_called_with(session=self.session, action=AuditActions.INSUFFICIENT_PERMISSIONS,
                                              additional_information={'reason': 'noRoles'})
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.UAUTH0001}),
            call({'log_reference': LogReference.UAUTH0016})])
        self.redirect_to_logout_patcher.assert_called_with('LOGOUT_URL', 'noRoles')
        self.assertEqual(redirect_to_logout, result)

    def test_redirects_to_logout_if_authenticating_workgroup_and_user_does_not_have_screening_workgroup(self):
        # Arrange
        self.config['AUTHENTICATE_WORKGROUPS'] = True
        self.get_user_details_patcher.return_value = {'user_roles': [{'workgroups': []}, {'workgroups': ['invalid']}],
                                                      'assurance_level': '1'}
        # Act
        result = self.act()
        # Assert
        self.get_user_details_patcher.assert_called_with(
            access_token,
            self.nhsid_config['userinfo_endpoint'],
            self.config['NHSID_CONNECTION_TIMEOUT']
        )
        self.audit_patcher.assert_called_with(session=self.session, action=AuditActions.INSUFFICIENT_PERMISSIONS,
                                              additional_information={'reason': 'noRolesWithWorkgroup'})
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.UAUTH0001}),
            call({'log_reference': LogReference.UAUTH0023})])
        self.redirect_to_logout_patcher.assert_called_with('LOGOUT_URL', 'noRolesWithWorkgroup')
        self.assertEqual(redirect_to_logout, result)

    @patch('time.time')
    def test_redirects_to_logged_in_after_updating_session(self, mock_time):
        # Arrange
        expected_user_data = {
            'user_data': user_data,
            'nhsid_useruid': 'id',
            'nhsid_subject_id': 'nhsid_subject_id',
            'nhsid_session_id': 'nhsid_session_id'}
        expected_delete_attributes = ['hashed_session_token']
        mock_time.return_value = 1580256000  # 'Wed, 29 Jan 2020 00:00:00 GMT'
        expected_cookie = " ".join([
            f'sp_session_cookie={session_token};',
            'expires=Wed, 29 Jan 2020 08:00:00 GMT;',
            'Path=/;',
            'SameSite=Strict; Secure'
        ])
        # Act
        result = self.act()
        # Assert
        self.log_patcher.assert_has_calls([
          call({'log_reference': LogReference.UAUTH0001}),
          call({'log_reference': LogReference.UAUTH0012})])
        self.update_session_patcher.assert_called_with(session_token,
                                                       expected_user_data,
                                                       delete_attributes=expected_delete_attributes)
        self.audit_patcher.assert_called_with(session=updated_session, action=AuditActions.LOGIN)
        self.assertEqual(302, result['statusCode'])
        self.assertEqual('LOGGED_IN_REDIRECT_URL?session_id=session_id', result['headers']['Location'])
        self.assertEqual(expected_cookie, result['headers']['Set-Cookie'])
