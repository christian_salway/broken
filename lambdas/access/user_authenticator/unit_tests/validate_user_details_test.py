from unittest import TestCase
from unittest.mock import patch, call
from user_authenticator.log_references import LogReference
from user_authenticator.validate_user_details import validate_user_details

module = 'user_authenticator.validate_user_details'


class TestValidateUserDetails(TestCase):

    def setUp(self):
        self.log_patcher = patch(f'{module}.log').start()
        self.mock_create_user = patch(f'{module}.create_user').start()
        self.mock_get_user_by_nhsid_useruid = patch(f'{module}.get_user_by_nhsid_useruid').start()
        self.user_details = {
            'sub': 'sub',
            'nhsid_useruid': 'nhsid_useruid',
        }
        self.claims = {'sub': 'sub'}
        self.allowlist_enabled = True

    def tearDown(self):
        patch.stopall()

    def test_returns_false_if_sub_does_not_match(self):
        # Arrange
        claims = {'sub': 'something else'}
        # Act
        result = validate_user_details(self.user_details, claims, self.allowlist_enabled)
        # Assert
        self.assertFalse(result)
        self.log_patcher.assert_called_with({
                'log_reference': LogReference.UAUTH0017,
                'user_details_sub': 'sub',
                'token_sub': 'something else'})

    def test_fetches_user_by_user_id(self):
        # Arrange
        self.mock_get_user_by_nhsid_useruid.return_value = {'Item': {'is_allowed': False}}
        # Act
        result = validate_user_details(self.user_details, self.claims, self.allowlist_enabled)
        # Assert
        self.assertEqual(1, self.mock_get_user_by_nhsid_useruid.call_count)
        self.assertFalse(result)
        self.log_patcher.assert_called_with({'log_reference': LogReference.UAUTH0014, 'nhsid_useruid': 'nhsid_useruid'})

    def test_creates_user_if_needed(self):
        # Arrange
        self.mock_create_user.return_value = {}
        self.mock_get_user_by_nhsid_useruid.return_value = None
        # Act
        result = validate_user_details(self.user_details, self.claims, self.allowlist_enabled)
        # Assert
        self.assertEqual(1, self.mock_create_user.call_count)
        self.assertEqual({'sub': 'sub', 'nhsid_useruid': 'nhsid_useruid'}, self.mock_create_user.call_args[0][0])
        self.assertFalse(result)
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.ACTOK0007, 'nhsid_useruid': 'nhsid_useruid'}),
            call({'log_reference': LogReference.UAUTH0014, 'nhsid_useruid': 'nhsid_useruid'})])

    def test_returns_true_if_allowlist_is_disabled(self):
        # Arrange
        self.mock_get_user_by_nhsid_useruid.return_value = {'Item': {'is_allowed': False}}
        self.allowlist_enabled = False
        # Act
        result = validate_user_details(self.user_details, self.claims, self.allowlist_enabled)
        # Assert
        self.assertEqual(0, self.mock_create_user.call_count)
        self.assertTrue(result)
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.ACTOK0008, 'nhsid_useruid': 'nhsid_useruid', 'allowlist': 'disabled'})])

    def allows_the_user_if_they_are_on_the_allowlist(self):
        # Arrange
        self.mock_get_user_by_nhsid_useruid.return_value = {'Item': {'is_allowed': True}}
        # Act
        result = validate_user_details(self.user_details, self.claims, self.allowlist_enabled)
        # Assert
        self.assertEqual(0, self.mock_create_user.call_count)
        self.assertTrue(result)
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.ACTOK0008, 'nhsid_useruid': 'nhsid_useruid', 'allowlist': 'enabled'})])

    def rejects_the_user_if_they_are_not_on_the_allowlist(self):
        # Arrange
        self.mock_get_user_by_nhsid_useruid.return_value = {'Item': {'is_allowed': False}}
        # Act
        result = validate_user_details(self.user_details, self.claims, self.allowlist_enabled)
        # Assert
        self.assertEqual(0, self.mock_create_user.call_count)
        self.assertFalse(result)
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.UAUTH0014, 'nhsid_useruid': 'nhsid_useruid'})])
