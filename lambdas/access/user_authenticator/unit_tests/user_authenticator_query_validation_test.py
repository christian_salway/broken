from unittest import TestCase
from unittest.mock import patch, MagicMock
with patch('boto3.resource') as mock_resource, patch('common.log.log') as mock_log:
    from user_authenticator.user_authenticator import return_invalid_parameters

domain = 'fakedomain'
nhs_id = 'nhs_id'
client_id = 'client_id'


@patch('user_authenticator.user_authenticator.log', MagicMock())
@patch('os.environ', {'NHSID_ID': nhs_id, 'SCREENING_CLIENT_ID': client_id})
class TestReturnInvalidQueryParameters(TestCase):

    def setUp(self):
        self.event = {
            'queryStringParameters': {
                'iss': nhs_id,
                'client_id': client_id,
                'code': 'code',
                'sub': 'sub'
            }
        }

    def test_return_invalid_query_parameters_returns_empty_list_when_parameters_valid(self):
        # Act
        invalid_parameters = return_invalid_parameters(self.event, nhs_id, client_id)
        # Assert
        self.assertEqual(0, len(invalid_parameters))

    def test_return_invalid_query_parameters_returns_error_string_when_no_parameters_given(self):
        # Arrange
        self.event = {}
        # Act
        invalid_parameters = return_invalid_parameters(self.event, nhs_id, client_id)
        # Assert
        self.assertEqual('No parameters given', invalid_parameters)

    def test_return_invalid_query_parameters_returns_iss_when_iss_incorrect(self):
        # Arrange
        self.event['queryStringParameters']['iss'] = 'incorrect'
        # Act
        invalid_parameters = return_invalid_parameters(self.event, nhs_id, client_id)
        # Assert
        self.assertIn('iss', invalid_parameters)

    def test_return_invalid_query_parameters_returns_client_id_when_client_id_incorrect(self):
        # Arrange
        self.event['queryStringParameters']['client_id'] = 'incorrect'

        # Act & Assert
        invalid_parameters = return_invalid_parameters(self.event, nhs_id, client_id)
        assert 'client_id' in invalid_parameters

    def test_return_invalid_query_parameters_returns_code_when_code_has_no_length(self):
        # Arrange
        self.event['queryStringParameters']['code'] = ''
        # Act
        invalid_parameters = return_invalid_parameters(self.event, nhs_id, client_id)
        # Assert
        assert 'code' in invalid_parameters

    def test_return_invalid_query_parameters_returns_all_parameters_when_all_incorrect(self):
        # Arrange
        self.event['queryStringParameters'] = {
            'iss': 'incorrect',
            'client_id': 'incorrect',
            'code': ''
        }
        # Act
        invalid_parameters = return_invalid_parameters(self.event, nhs_id, client_id)
        # Assert
        assert 'code' in invalid_parameters and 'iss' in invalid_parameters and 'client_id' in invalid_parameters
