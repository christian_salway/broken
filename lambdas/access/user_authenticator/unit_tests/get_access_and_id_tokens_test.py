import json
from unittest import TestCase
from mock import patch, MagicMock
from user_authenticator.get_access_and_id_tokens import get_client_secret, get_token_data, get_access_and_id_tokens


@patch('user_authenticator.get_access_and_id_tokens.log', MagicMock())
class TestGetAccessToken(TestCase):

    def setUp(self):
        self.client_id = 'client_id'
        self.redirect_uri = 'redirect_uri'
        self.client_secret_id = 'secret_id'
        self.client_secret = 'secret'
        self.auth_code = 'auth_code'
        self.config = {
            'NHSID_DISCOVERY_URL': 'NHSID_DISCOVERY_URL',
            'NHSID_CLIENT_SECRET_ID': 'NHSID_CLIENT_SECRET_ID',
            'SCREENING_CLIENT_ID': 'SCREENING_CLIENT_ID',
            'REDIRECT_URI': 'REDIRECT_URI',
            'NHSID_CONNECTION_TIMEOUT': 10,
        }

    @patch('boto3.session.Session')
    def test_get_client_secret_returns_secret(self, session_mock):
        # Arrange
        json_blob = json.dumps({'secret': 'my_client_secret_shhh!'})
        mock_response = {
            'SecretString': json_blob
        }
        client_object = MagicMock()

        client_object.get_secret_value.return_value = mock_response
        session_object = MagicMock()
        session_object.client.return_value = client_object

        session_mock.return_value = session_object
        expected_secret = 'my_client_secret_shhh!'

        # Act
        actual_secret = get_client_secret(self.client_secret_id)

        # Assert
        self.assertEqual(expected_secret, actual_secret)

    @patch('user_authenticator.get_access_and_id_tokens.get_client_secret')
    @patch('user_authenticator.get_access_and_id_tokens.requests')
    def test_get_access_and_id_tokens_makes_request_with_correct_parameters(
            self, requests_mock, get_client_secret_mock):
        # Arrange
        screening_id = 'SCREENING_CLIENT_ID'
        redirect_uri = 'REDIRECT_URI'
        client_secret = 'secret'
        timeout = '10'
        get_client_secret_mock.return_value = client_secret

        # Act
        get_access_and_id_tokens(self.config, self.auth_code, 'NHSID_TOKEN_ENDPOINT')

        # Assert
        token, other_args = requests_mock.post.call_args
        expected_args = {
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            },
            'data': {
                'grant_type': 'authorization_code',
                'code': 'auth_code',
                'redirect_uri': redirect_uri,
                'client_id': screening_id,
                'client_secret': client_secret
            },
            'timeout': int(timeout)
        }

        self.assertTupleEqual(token, ('NHSID_TOKEN_ENDPOINT',))
        self.assertDictEqual(expected_args, other_args)

    def test_get_token_data_returns_token_data(self):
        # Arrange
        access_token = 'access token'
        id_token = 'id token'

        fake_response = MagicMock()
        fake_response.status_code = 200
        fake_response.json.return_value = {
            'access_token': access_token,
            'id_token': id_token
        }
        # Act
        token_data = get_token_data(fake_response)
        # Assert
        expected_data = access_token, id_token,
        self.assertTupleEqual(token_data, expected_data)

    def test_get_token_data_returns_None_tuple_if_response_code_is_400(self):
        # Arrange
        fake_response = MagicMock()
        fake_response.status_code = 400
        # Act
        token_data = get_token_data(fake_response)
        # Assert
        expected_data = None, None
        self.assertTupleEqual(token_data, expected_data)
