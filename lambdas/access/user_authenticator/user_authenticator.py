import os
import base64
import json
import traceback
import requests
from distutils import util
from common.log import log
from common.utils.data_segregation.nhais_ciphers import Cohorts
from user_authenticator.log_references import LogReference
from common.utils.audit_utils import AuditActions, audit
from common.utils.auth.get_public_key import get_public_key
from common.utils.lambda_wrappers import (
    lambda_entry_point, auth_exception_wrapper, auth_lambda
)

from user_authenticator.get_access_and_id_tokens import get_access_and_id_tokens
from user_authenticator.get_user_details import get_user_details
from user_authenticator.verify_id_token import verify_id_token, verify_assurance_level
from user_authenticator.validate_user_details import validate_user_details

from common.utils.session_utils import (
    update_session, get_session, invalidate_any_duplicate_sessions
)
from common.utils.cookie_management_utils import create_set_cookie_header


def get_config_from_environment(environment):
    return {
        'LOGGED_IN_REDIRECT_URL': environment['LOGGED_IN_REDIRECT_URL'],
        'SCREENING_CLIENT_ID': environment['SCREENING_CLIENT_ID'],
        'NHSID_CLIENT_SECRET_ID': environment['NHSID_CLIENT_SECRET_ID'],
        'LOGOUT_URL': environment['LOGOUT_URL'],
        'NHSID_AUD_ID': environment['NHSID_AUD_ID'],
        'NHSID_DISCOVERY_URL': environment['NHSID_DISCOVERY_URL'],
        'REDIRECT_URI': environment['REDIRECT_URI'],
        'NHSID_CONNECTION_TIMEOUT': int(environment.get('NHSID_CONNECTION_TIMEOUT', 2)),
        'ALLOW_ONLY_KNOWN_USERS': util.strtobool(environment.get('ALLOW_ONLY_KNOWN_USERS', 'true')),
        'NHSID_MIN_ASSURANCE_LEVEL': int(environment.get('NHSID_MIN_ASSURANCE_LEVEL', 3)),
        'AUTHENTICATE_WORKGROUPS': util.strtobool(environment.get('AUTHENTICATE_WORKGROUPS', 'true')),
    }


def redirect_to_logout(logout_url, reason='unauthorised'):
    log({'log_reference': LogReference.UAUTH0013, 'reason': reason})
    return {
        'statusCode': 302,
        'headers': {
            'Location': f'{logout_url}?reason={reason}'
        }
    }


def return_invalid_parameters(event, nhsid_id, screening_client_id):
    query_parameters = event.get('queryStringParameters')
    if query_parameters is None:
        return 'No parameters given'

    invalid_parameters = {}
    if query_parameters.get('iss') != nhsid_id:
        invalid_parameters['iss'] = {
            'expected': nhsid_id,
            'actual': query_parameters.get('iss')
        }
    if query_parameters.get('client_id') != screening_client_id:
        invalid_parameters['client_id'] = {
            'expected': screening_client_id,
            'actual': query_parameters.get('client_id')
        }
    if not query_parameters.get('code'):
        invalid_parameters['code'] = {
            'actual': query_parameters.get('code')
        }

    return invalid_parameters


def validate_session(session):
    if not session:
        log({'log_reference': LogReference.UAUTH0002})
        return False

    if 'nonce' not in session:
        log({'log_reference': LogReference.UAUTH0005})
        return False

    log({'log_reference': LogReference.UAUTH0020})
    return True


def validate_state(state, session_token, hashed_session_token_key):
    if not state:
        log({'log_reference': LogReference.UAUTH0019})
        return False

    dynamo_session = get_session(session_token)
    dynamo_token = dynamo_session.get(hashed_session_token_key) if dynamo_session else None

    decoded_state = json.loads(base64.urlsafe_b64decode(state).decode())
    state_token = decoded_state.get(hashed_session_token_key)
    if state_token is None or state_token != dynamo_token:
        log({'log_reference': LogReference.UAUTH0019, 'expected': dynamo_token, 'actual': state_token})
        return False

    log({'log_reference': LogReference.UAUTH0020})
    return True


def discover_nhsid_configuration(discovery_url):
    return requests.get(discovery_url).json()


@auth_exception_wrapper
@lambda_entry_point
@auth_lambda
def lambda_handler(event, context, session):

    log({'log_reference': LogReference.UAUTH0001})
    config = get_config_from_environment(os.environ)
    logout_url = config['LOGOUT_URL']

    try:
        if not validate_session(session):
            log({'log_reference': LogReference.UAUTH0002})
            return redirect_to_logout(logout_url)

        hashed_session_token_key = 'hashed_session_token'
        session_token = session['session_token']

        nhsid_config = discover_nhsid_configuration(config['NHSID_DISCOVERY_URL'])

        invalid_parameters = return_invalid_parameters(event, nhsid_config['issuer'], config['SCREENING_CLIENT_ID'])
        if len(invalid_parameters):
            log({'log_reference': LogReference.UAUTH0003, 'error': str(invalid_parameters)})
            return redirect_to_logout(logout_url)

        auth_code = event['queryStringParameters']['code']
        access_token, id_token = get_access_and_id_tokens(
            config,
            auth_code,
            nhsid_config['token_endpoint'])

        if not access_token or not id_token:
            log({'log_reference': LogReference.UAUTH0006})
            return redirect_to_logout(logout_url)

        state = event.get('queryStringParameters').get('state')
        if not validate_state(state, session_token, hashed_session_token_key):
            return redirect_to_logout(logout_url)

        jwk = get_public_key(
            nhsid_config,
            config,
            id_token)

        if not jwk:
            log({'log_reference': LogReference.UAUTH0007})
            return redirect_to_logout(logout_url)

        claims = verify_id_token(
            id_token,
            session.get('nonce'),
            config['NHSID_AUD_ID'],
            jwk)

        if not claims:
            log({'log_reference': LogReference.UAUTH0007})
            return redirect_to_logout(logout_url)

        if not verify_assurance_level(claims.get('acr'), config['NHSID_MIN_ASSURANCE_LEVEL']):
            return redirect_to_logout(logout_url, 'assuranceLevel')

        user_details = get_user_details(
            access_token,
            nhsid_config['userinfo_endpoint'],
            config['NHSID_CONNECTION_TIMEOUT']
        )
        if not user_details:
            log({'log_reference': LogReference.UAUTH0010})
            return redirect_to_logout(logout_url)
        if not validate_user_details(user_details, claims, config['ALLOW_ONLY_KNOWN_USERS']):
            return redirect_to_logout(logout_url)

        assurance_level = user_details.get('assurance_level')
        if assurance_level is None or \
           not assurance_level.isdigit() or \
           int(assurance_level) < config['NHSID_MIN_ASSURANCE_LEVEL']:
            return redirect_to_logout(logout_url, 'assuranceLevel')

        if 'user_roles' not in user_details or len(user_details['user_roles']) == 0:
            log({'log_reference': LogReference.UAUTH0016})
            session['user_data'] = user_details
            audit(session=session, action=AuditActions.INSUFFICIENT_PERMISSIONS,
                  additional_information={'reason': 'noRoles'})
            return redirect_to_logout(logout_url, 'noRoles')

        if config['AUTHENTICATE_WORKGROUPS']:
            workgroups = set(Cohorts.list())
            roles_with_screening_workgroup = [
                role for role in user_details['user_roles']
                if len(workgroups.intersection(role['workgroups'])) > 0
            ]
            user_details['user_roles'] = roles_with_screening_workgroup
            if len(roles_with_screening_workgroup) == 0:
                log({'log_reference': LogReference.UAUTH0023})
                session['user_data'] = user_details
                audit(session=session, action=AuditActions.INSUFFICIENT_PERMISSIONS,
                      additional_information={'reason': 'noRolesWithWorkgroup'})
                return redirect_to_logout(logout_url, 'noRolesWithWorkgroup')

        nhsid_useruid = user_details['nhsid_useruid']
        session_data = {
            'user_data': user_details,
            'nhsid_useruid': nhsid_useruid
        }
        nhsid_session_id = claims.get('sid')
        if nhsid_session_id:
            session_data['nhsid_session_id'] = nhsid_session_id

        nhsid_subject_id = claims.get('sub')
        if nhsid_subject_id:
            session_data['nhsid_subject_id'] = nhsid_subject_id

        updated_session = update_session(session_token,
                                         session_data,
                                         delete_attributes=[hashed_session_token_key])

        invalidate_any_duplicate_sessions(session_token, nhsid_useruid)

    except Exception as e:
        log({'log_reference': LogReference.UAUTH0011, 'error': str(e), 'traceback': traceback.format_exc()})
        return redirect_to_logout(logout_url)

    log({'log_reference': LogReference.UAUTH0012})
    audit(session=updated_session, action=AuditActions.LOGIN)

    return {
        'statusCode': 302,
        'headers': {
            'Location': f'{config["LOGGED_IN_REDIRECT_URL"]}?session_id={session["session_id"]}',
            'Set-Cookie': create_set_cookie_header(session_token)
        }
    }
