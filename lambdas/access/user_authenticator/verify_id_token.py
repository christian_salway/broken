import re
import json
from jwt.api_jwt import decode
from jwt.algorithms import RSAAlgorithm
from common.log import log
from user_authenticator.log_references import LogReference


def verify_id_token(id_token, nonce, audience, jwk):
    try:
        key = RSAAlgorithm.from_jwk(json.dumps(jwk))
        claims: dict = decode(id_token, algorithms=jwk['alg'], key=key, audience=audience)
        log({'log_reference': LogReference.VETOK0003})

        if verify_claims(claims, nonce):
            return claims
        return False

    except (Exception) as e:
        log({'log_reference': LogReference.VETOK0004, 'error': str(e)})
        return None


def verify_claims(claims, nonce):
    if nonce != claims['nonce']:
        log({'log_reference': LogReference.UAUTH0008, 'session_nonce': nonce, 'token_nonce': claims['nonce']})
        return False
    log({'log_reference': LogReference.UAUTH0009})
    return True


def verify_assurance_level(assurance_level, minimum_assurance_level):
    assurance_level_pattern = r'^aal(\d)_'
    user_level_match = re.match(assurance_level_pattern, assurance_level, re.IGNORECASE)
    user_level = int(user_level_match.groups()[0]) if user_level_match else None
    if not user_level or user_level < int(minimum_assurance_level):
        log({
            'log_reference': LogReference.UAUTH0021,
            'expected': minimum_assurance_level,
            'actual': user_level})
        return False

    return True
