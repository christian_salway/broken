from typing import Dict, Optional
import requests
from common.log import log
from user_authenticator.log_references import LogReference
from common.utils.roles_utils import (
    get_filtered_roles_list_from_user_data
)

NHSID_USERINFO_ENDPOINT = None


def get_user_details(access_token, userinfo_endpoint_uri, timeout) -> Optional[Dict]:
    headers = {
        'Authorization': f'Bearer {access_token}'
    }

    log({'log_reference': LogReference.UDATA0001})
    response = requests.get(userinfo_endpoint_uri, headers=headers, timeout=timeout)
    json_data = response.json()

    if response.status_code != 200:
        log({
            'log_reference': LogReference.UDATA0002,
            'error': json_data.get('error_description') or 'No error description provided',
        })
        return None

    user_orgs = json_data['nhsid_user_orgs']
    nrbac_roles = json_data['nhsid_nrbac_roles']
    roles = get_filtered_roles_list_from_user_data(user_orgs, nrbac_roles)
    user = {
        'nhsid_useruid': json_data['nhsid_useruid'],
        'first_name': json_data['given_name'],
        'last_name': json_data['family_name'],
        'user_roles': roles,
        'sub': json_data['sub'],
        'assurance_level': json_data['idassurancelevel'],
    }
    log({'log_reference': LogReference.UDATA0003})
    return user
