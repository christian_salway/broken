import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    GETUSER1 = (logging.INFO, 'Received request to get user')
    GETUSER2 = (logging.INFO, 'Session missing, expired or not logged in.')
    GETUSER3 = (logging.INFO, 'Found roles for user')
    GETUSER4 = (logging.ERROR, 'Unable to find user in users table')
    GETUSER5 = (logging.INFO, 'Returning user data')
