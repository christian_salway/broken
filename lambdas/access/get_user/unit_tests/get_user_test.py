import json
from mock import patch, call
from unittest.case import TestCase

with patch('boto3.resource') as mock_resource, patch('common.log.log') as mock_log:
    from get_user.get_user import format_response_body, lambda_handler, LogReference


def get_event(session_token):
    cookie = f'sp_session_cookie={session_token}'
    return {'headers': {'Cookie': cookie}}


def get_session(session_token):
    user_data = {
        'nhsid_useruid': '9876543210',
        'first_name': 'Bob',
        'last_name': 'Smith',
        'user_roles': [{'role_id': '1', 'role_name': 'test role'}]
    }
    return {
        'session_token': session_token,
        'user_data': user_data
    }


class TestGetUser(TestCase):

    def setUp(self):
        super(TestGetUser, self).setUp()
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    def test_first_and_last_name_and_roles_correctly_populated_in_response(self):
        user_data = {'first_name': 'Joe', 'last_name': 'bloggs'}

        roles = [{'role_id': 1, 'role_name': 'role_1'}, {'role_id': 2, 'role_name': 'role_2'}]

        expected_response = {
            'roles': [
                {'role_id': 1, 'role_name': 'role_1'},
                {'role_id': 2, 'role_name': 'role_2'}
            ],
            'first_name': 'Joe', 'last_name': 'bloggs'}

        actual_response = format_response_body(
            user_data['first_name'], user_data['last_name'], roles)

        self.assertDictEqual(expected_response, actual_response)

    @patch('get_user.get_user.get_user_by_nhsid_useruid')
    @patch('get_user.get_user.log')
    def test_returns_OK_and_user_response_given_user_with_active_session(
            self, mock_logger, mock_get_user_by_nhsid_useruid):

        # Arrange
        session_token = '123456'
        event = get_event(session_token)
        user = {
            'legitimate_relations_accepted': '2020-02-02',
            'nhsid_useruid': '9876543210',
        }
        mock_get_user_by_nhsid_useruid.return_value = user
        session = get_session(session_token)

        expected_body = {
            "data": {
                "roles": [
                    {"role_id": "1", "role_name": "test role"}
                ],
                "first_name": "Bob",
                "last_name": "Smith",
                'nhsid_useruid': '9876543210',
                "legitimate_relations_accepted": '2020-02-02',
            }
        }

        expected_log_calls = [
            call({'log_reference': LogReference.GETUSER1}),
            call({'log_reference': LogReference.GETUSER3, 'number_of_roles': 1}),
            call({'log_reference': LogReference.GETUSER5, 'nhsid_useruid': '9876543210'}),
        ]

        # Act
        actual_response = self.unwrapped_handler(event, {}, session)

        # Assert
        self.assertEqual(200, actual_response.get('statusCode'))
        self.assertDictEqual(expected_body, json.loads(actual_response.get('body')))
        mock_logger.assert_has_calls(expected_log_calls)

    @patch('get_user.get_user.log')
    def test_returns_403_and_no_roles_when_session_not_found(self, mock_logger):

        expected_response = {
            'statusCode': 403,
            'headers': {
                'Content-Type': 'application/json',
                'X-Content-Type-Options': 'nosniff',
                'Strict-Transport-Security': 'max-age=1576800'
            },
            'body': '{"message": "Session missing, expired or not logged in."}',
            'isBase64Encoded': False
        }
        expected_log_calls = [
            call({'log_reference': LogReference.GETUSER1}),
            call({'log_reference': LogReference.GETUSER2}),
        ]

        actual_response = self.unwrapped_handler({}, {}, {})
        self.assertDictEqual(expected_response, actual_response)
        mock_logger.assert_has_calls(expected_log_calls)

    @patch('get_user.get_user.get_user_by_nhsid_useruid')
    @patch('get_user.get_user.log')
    def test_returns_500_if_user_not_in_users_table(self, mock_logger, mock_get_user_by_nhsid_useruid):
        # Arrange
        session_token = '123456'
        event = get_event(session_token)
        session = get_session(session_token)
        mock_get_user_by_nhsid_useruid.return_value = {}
        expected_log_calls = [
            call({'log_reference': LogReference.GETUSER1}),
            call({'log_reference': LogReference.GETUSER3, 'number_of_roles': 1}),
            call({'log_reference': LogReference.GETUSER4, 'nhsid_useruid': '9876543210'}),
        ]

        # Act
        actual_response = self.unwrapped_handler(event, {}, session)

        # Assert
        self.assertEqual(500, actual_response.get('statusCode'))
        mock_logger.assert_has_calls(expected_log_calls)
