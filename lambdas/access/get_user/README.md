This lambda uses the user's cookie to find their dynamodb session. 
It returns a list of roles for the user and the user's first and last names, based on the session user_data obtained from NHS Identity by the user_authenticator lambda, and the user data stored in the dynamodb users table for the user's nhsid_useruid.
The roles are ordered alphabetically by role_name. 

## Response
* Scenario: Results found

    Status code: 200 OK 

    Body: 
    ```
     {
          "first_name": "Test",
          "last_name": "User",
          "legitimate_relations_accepted": "2020-07-02",
          "roles": [
                {
                    "role_id": "39472974",
                    "role_name": "CSAS Team Member",
                    "organisation_name": "GREATER MANCHESTER",
                    "organisation_code": "X09"
                }
          ]
      }
    ```