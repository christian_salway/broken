from common.log import log
from get_user.log_references import LogReference
from common.utils import json_return_message, json_return_data
from common.utils.lambda_wrappers import lambda_entry_point, auth_lambda
from common.utils.user_utils import get_user_by_nhsid_useruid


@lambda_entry_point
@auth_lambda
def lambda_handler(event, context, session):

    log({'log_reference': LogReference.GETUSER1})

    if not session or not session.get('user_data'):
        log({'log_reference': LogReference.GETUSER2})
        return json_return_message(403, 'Session missing, expired or not logged in.')

    session_user_data = session['user_data']
    roles = session_user_data['user_roles']
    log({'log_reference': LogReference.GETUSER3, 'number_of_roles': len(roles)})

    nhsid_useruid = session_user_data.get('nhsid_useruid')
    user = get_user_by_nhsid_useruid(nhsid_useruid)
    if not user:
        log({'log_reference': LogReference.GETUSER4, 'nhsid_useruid': nhsid_useruid})
        return json_return_message(500, 'Unable to find user in users table')

    response_body = format_response_body(
        session_user_data['first_name'], session_user_data['last_name'], roles)

    add_user_data(response_body, user)
    log({'log_reference': LogReference.GETUSER5, 'nhsid_useruid': nhsid_useruid})

    return json_return_data(200, response_body)


def format_response_body(first_name, last_name, roles):
    return {
            'roles': roles,
            'first_name': first_name,
            'last_name': last_name
    }


def add_user_data(response_body, user):
    response_body['nhsid_useruid'] = user.get('nhsid_useruid')
    legitimate_relations_accepted = user.get('legitimate_relations_accepted')
    if legitimate_relations_accepted:
        response_body['legitimate_relations_accepted'] = legitimate_relations_accepted
