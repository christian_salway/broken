import base64
import json


def build_redirect(
        client_id: str,
        login_uri: str,
        redirect_url: str,
        environment: str,
        token: str,
        hashed_session_token: str,
        scopes):
    # The list of available scopes is defined at
    # https://nhsd-confluence.digital.nhs.uk/display/CSP/Linking+to+NHS+Identity?preview=/97771430/97773257/NHS-Identity%2B-%2BInterface%2BSpecification%2B-%2BFederation%2Bv1.0.docx

    scope = '+'.join(scopes)

    # We can put things in state that we need to get back after the user has
    # logged in, like environment name so that they can be redirected back to
    # the correct environment
    state = {'environment': environment, 'hashed_session_token': hashed_session_token}

    params = {
        'redirect_uri': redirect_url,
        'scope': scope,
        'response_type': 'code',
        'client_id': client_id,
        'state': encode_state(state),
        'nonce': token,
        'prompt': 'login',
    }

    return f"{login_uri}?{format_query_string(params)}"


def encode_state(state: dict) -> str:
    state_string = json.dumps(state)
    encoded_bytes = state_string.encode('utf-8')
    return base64.urlsafe_b64encode(encoded_bytes).decode()


def format_query_string(params):
    return "&".join([f"{k}={v}" for k, v in params.items()])
