import json
import base64
from unittest import TestCase
import urllib.parse as urlparse
from urllib.parse import parse_qs
from mock import patch
with patch('boto3.resource') as mock_resource, patch('common.log.log') as mock_log:
    from login_redirect.login_redirect import build_redirect


class TestRedirectBuilder(TestCase):

    def setUp(self):
        self.authorize_url = "https://am.nhsspit-2.ptl.nhsd-esa.net/openam/oauth2/oidc/authorize"
        self.redirect_url = "https://www.screeningdev.nhs.uk/oauth/redirect"
        self.client_id = '12345678.apps.national'
        self.token = 'token'
        self.environment_name = "sp-444"
        self.hashed_session_token = 'hashed_token'
        self.scopes = [
          'openid',
          'nhsperson',
          'associatedorgs',
          'orgaccess',
          'nationalrbacaccess',
          'odscodes'
        ]

    def build_expected_url(self):
        scopes = '+'.join(self.scopes)
        self.qs = '&'.join([
          f'redirect_uri={self.redirect_url}',
          f'scope={scopes}',
          'response_type=code',
          f'client_id={self.client_id}',
          'state=eyJlbnZpcm9ubWVudCI6ICJzcC00NDQiLCAiaGFzaGVkX3Nlc3Npb25fdG9rZW4iOiAiaGFzaGVkX3Rva2VuIn0=',
          'nonce=token',
          'prompt=login'
        ])
        return f"{self.authorize_url}?{self.qs}"

    def test_builds_redirect_url(self):
        expected_url = self.build_expected_url()
        url = build_redirect(
          self.client_id,
          self.authorize_url,
          self.redirect_url,
          self.environment_name,
          self.token,
          self.hashed_session_token,
          self.scopes)
        self.assertEqual(expected_url, url)

    def test_puts_environment_name_in_state(self):
        self.environment_name = 'my-environment'
        expected_state = 'state=eyJlbnZpcm9ubWVudCI6ICJteS1lbnZpcm9ubWVudCIs'\
                         'ICJoYXNoZWRfc2Vzc2lvbl90b2tlbiI6ICJoYXNoZWRfdG9rZW4ifQ=='
        url = build_redirect(
          self.client_id,
          self.authorize_url,
          self.redirect_url,
          self.environment_name,
          self.token,
          self.hashed_session_token,
          self.scopes)
        self.assertIn(expected_state, url)

    def test_base64_encodes_state(self):
        url = build_redirect(
          self.client_id,
          self.authorize_url,
          self.redirect_url,
          self.environment_name,
          self.token,
          self.hashed_session_token,
          self.scopes)

        parsed_url = urlparse.urlparse(url)
        state = parse_qs(parsed_url.query)['state'][0]
        self.assertEqual(
          state, 'eyJlbnZpcm9ubWVudCI6ICJzcC00NDQiLCAiaGFzaGVkX3Nlc3Npb25fdG9rZW4iOiAiaGFzaGVkX3Rva2VuIn0=')
        decoded = base64.urlsafe_b64decode(state)
        self.assertEqual(b'{"environment": "sp-444", "hashed_session_token": "hashed_token"}', decoded)
        parsed_dict = json.loads(decoded)
        self.assertEqual(
          {'environment': 'sp-444', 'hashed_session_token': 'hashed_token'}, parsed_dict)

    def adds_the_list_of_scopes_separated_by_a_plus_sign(self):
        self.scopts = ["somescope1", "otherscope2", "360noscope"]
        url = build_redirect(
          self.client_id,
          self.authorize_url,
          self.redirect_url,
          self.environment_name,
          self.token,
          self.scopes)
        self.assertIn("somescope1+otherscope2+360noscope", url)
