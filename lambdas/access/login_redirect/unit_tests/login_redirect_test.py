from unittest import TestCase
from datetime import datetime, timezone
from mock import patch
with patch('boto3.resource') as mock_resource, patch('common.log.log') as mock_log:
    from login_redirect.login_redirect import build_response


class TestLoginRedirect(TestCase):

    def setUp(self):
        self.session_token = 'session_token'
        self.token = 'token'
        self.expires_in = datetime(2020, 2, 5, 9, 45, 23, tzinfo=timezone.utc)
        self.redirect_url = 'redirect_url'
        self.session_table_name = 'session_table_name'
        self.session_cookie_name = 'session_cookie_name'
        self.session_expiry_refresh_minutes = 10

    @patch('login_redirect.login_redirect.create_set_cookie_header')
    def test_returns_302_redirect_with_a_session_cookie(self, create_set_cookie_header_mock):
        create_set_cookie_header_mock.return_value = \
            f'{self.session_cookie_name}={self.session_token}; expires=Mon, 27 Jan 2020 14:30:54 GMT'

        expected_response = {
            "statusCode": 302,
            "headers": {
                "Location": "redirect_url",
                "Set-Cookie": "session_cookie_name=session_token; expires=Mon, 27 Jan 2020 14:30:54 GMT"
            }
        }

        response = build_response(
            self.session_token,
            self.redirect_url)

        self.assertEqual(expected_response, response)
        create_set_cookie_header_mock.assert_called_with('session_token', use_strict=False)
