from unittest import TestCase
from mock import patch, MagicMock
with patch('boto3.resource') as mock_resource, patch('common.log.log') as mock_log:
    from login_redirect.login_redirect import lambda_handler


@patch('login_redirect.login_redirect.log', MagicMock())
class TestLoginRedirect(TestCase):

    @patch.dict('os.environ', {
        'CLIENT_ID': 'CLIENT_ID',
        'LOGIN_URL': 'LOGIN_URL',
        'LOGGED_IN_URL': 'LOGGED_IN_URL',
        'ENVIRONMENT_NAME': 'ENVIRONMENT_NAME',
        'CALLBACK_URL': 'CALLBACK_URL',
        'SESSION_TABLE_NAME': 'SESSION_TABLE_NAME',
        'SESSION_COOKIE_NAME': 'SESSION_COOKIE_NAME',
        'SESSION_EXPIRY_REFRESH_MINUTES': '10',
        'NHSID_DISCOVERY_URL': 'NHSID_DISCOVERY_URL',
    })
    @patch(
      'login_redirect.login_redirect.discover_nhsid_configuration',
      MagicMock(return_value={'authorization_endpoint': 'AUTH_ENDPOINT'}))
    @patch('login_redirect.login_redirect.create_session')
    @patch('login_redirect.login_redirect.create_set_cookie_header')
    def test_lambda_handler_calls_dynamodb(self, create_set_cookie_header_mock,
                                           create_session_mock):
        create_set_cookie_header_mock.return_value = 'SESSION_COOKIE_NAME=1234; expires=Mon, 27 Jan 2020 14:30:54 GMT'

        create_session_mock.return_value = 'session_token', 'session_id', 'nonce', 'hashed_token'

        event = {'resource': '/auth/login', 'path': '/auth/login', 'httpMethod': 'GET', 'headers': None, 'multiValueHeaders': None, 'queryStringParameters': None, 'multiValueQueryStringParameters': None, 'pathParameters': None, 'stageVariables': None, 'requestContext': {'resourceId': '5wqzsa', 'resourcePath': '/auth/login', 'httpMethod': 'GET', 'extendedRequestId': 'GcxvGEQmLPEFmOQ=', 'requestTime': '17/Jan/2020:14:49:55 +0000', 'path': '/auth/login', 'accountId': '092130162833', 'protocol': 'HTTP/1.1', 'stage': 'test-invoke-stage', 'domainPrefix': 'testPrefix', 'requestTimeEpoch': 1579272595754, 'requestId': 'f6c18325-52f0-405b-ba9c-cdd8950be567', 'identity': {'cognitoIdentityPoolId': None, 'cognitoIdentityId': None, 'apiKey': 'test-invoke-api-key', 'principalOrgId': None, 'cognitoAuthenticationType': None, 'userArn': 'arn:aws:sts::092130162833:assumed-role/NHSDDeveloperRole/luke.pearson2', 'apiKeyId': 'test-invoke-api-key-id', 'userAgent': 'aws-internal/3 aws-sdk-java/1.11.690 Linux/4.9.184-0.1.ac.235.83.329.metal1.x86_64 OpenJDK_64-Bit_Server_VM/25.232-b09 java/1.8.0_232 vendor/Oracle_Corporation', 'accountId': '092130162833', 'caller': 'AROARK43CXCISRIWURINN:luke.pearson2', 'sourceIp': 'test-invoke-source-ip', 'accessKey': 'ASIARK43CXCISVFAZZ67', 'cognitoAuthenticationProvider': None, 'user': 'AROARK43CXCISRIWURINN:luke.pearson2'}, 'domainName': 'testPrefix.testDomainName', 'apiId': '6t3xgnhmsg'}, 'body': None, 'isBase64Encoded': False}  

        expected_response = {
          'headers': {
            'Location': 'AUTH_ENDPOINT?redirect_uri=CALLBACK_URL&scope=openid+nhsperson+associatedorgs+nationalrbacaccess&response_type=code&client_id=CLIENT_ID&state=eyJlbnZpcm9ubWVudCI6ICJFTlZJUk9OTUVOVF9OQU1FIiwgImhhc2hlZF9zZXNzaW9uX3Rva2VuIjogImhhc2hlZF90b2tlbiJ9&nonce=nonce&prompt=login',  
            'Set-Cookie': 'SESSION_COOKIE_NAME=1234; expires=Mon, 27 Jan 2020 14:30:54 GMT'
          },
          'statusCode': 302
        }

        unwrapped_lambda = lambda_handler.__wrapped__.__wrapped__.__wrapped__
        response = unwrapped_lambda(event, {}, {})

        self.maxDiff = None
        self.assertDictEqual(expected_response, response)
