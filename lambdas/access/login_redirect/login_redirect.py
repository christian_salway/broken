import os
import requests
from common.utils.cookie_management_utils import create_set_cookie_header
from common.log import log, update_logger_metadata_fields
from common.utils.lambda_wrappers import lambda_entry_point, auth_exception_wrapper, auth_lambda
from login_redirect.log_references import LogReference
from login_redirect.redirect_builder import build_redirect
from common.utils.session_utils import create_session, valid_session_with_selected_role

SCOPES = [
    'openid',  # to get access token
    'nhsperson',  # to get user id and name
    'associatedorgs',  # to get organisations user belongs to
    'nationalrbacaccess',  # to get RBAC roles
]
NHSID_AUTH_ENDPOINT = None


@auth_exception_wrapper
@lambda_entry_point
@auth_lambda
def lambda_handler(event, context, session):
    global NHSID_AUTH_ENDPOINT
    log(LogReference.LOGIN_REDIRECT001)
    client_id = os.environ['CLIENT_ID']
    nhsid_config = discover_nhsid_configuration(os.environ['NHSID_DISCOVERY_URL'])
    NHSID_AUTH_ENDPOINT = NHSID_AUTH_ENDPOINT or nhsid_config['authorization_endpoint']
    logged_in_page_url = os.environ['LOGGED_IN_URL']
    callback_url = os.environ['CALLBACK_URL']
    environment_name = os.environ['ENVIRONMENT_NAME']
    log(LogReference.LOGIN_REDIRECT002)

    if has_existing_valid_session(event, session):
        return redirect_to_landing_page(logged_in_page_url)

    log(LogReference.LOGIN_REDIRECT003)

    session_token, session_id, token, hashed_session_token = create_session()
    update_logger_metadata_fields({'session_id': session_id})

    log(LogReference.LOGIN_REDIRECT004)

    log(LogReference.LOGIN_REDIRECT005)
    redirect_url = build_redirect(
        client_id, NHSID_AUTH_ENDPOINT, callback_url, environment_name, token, hashed_session_token, SCOPES)
    log(LogReference.LOGIN_REDIRECT006)

    log(LogReference.LOGIN_REDIRECT007)
    response = build_response(session_token, redirect_url)
    log(LogReference.LOGIN_REDIRECT008)

    return response


def has_existing_valid_session(event, session):

    log({'log_reference': LogReference.LOGIN_REDIRECT009})
    has_session = valid_session_with_selected_role(session)

    log({
        'log_reference': LogReference.LOGIN_REDIRECT010,
        'has_session': has_session,
        'session_token': session.get('session_token')
    })
    return has_session


def redirect_to_landing_page(logged_in_page_url):
    return {
        'statusCode': 302,
        'headers': {
            'Location': logged_in_page_url,
        }
    }


def build_response(session_token, redirect_url):

    return {
        'statusCode': 302,
        'headers': {
            'Location': redirect_url,
            'Set-Cookie': create_set_cookie_header(session_token, use_strict=False)
        }
    }


def discover_nhsid_configuration(discovery_url):
    return requests.get(discovery_url).json()
