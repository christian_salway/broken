from unittest import TestCase
from unittest.mock import patch
from ddt import ddt, data

from user_authoriser.log_references import LogReference
from user_authoriser.user_authoriser import lambda_handler
from copy import deepcopy

nhs_id = 'nhs_id'
client_id = 'client_id'
module = 'user_authoriser.user_authoriser'


@ddt
class TestLambdaHandler(TestCase):

    STAGE_URL = 'arn:aws:execute-api:eu-west-2:092130162833:*/ESTestInvoke-stage/'

    LAMBDA_EVENT = {
        'methodArn': 'arn:aws:execute-api:eu-west-2:092130162833:gpr4f82n3m/ESTestInvoke-stage/GET/',
        'queryStringParameters': {
            'iss': nhs_id,
            'client_id': client_id,
            'code': 'code'
        },
        'headers': {'Cookie': 'sp_session_cookie=123456789'}
    }

    AUTHORISED_USER_SESSION = {
        'expires': 1581951222,
        'session_id': '100001',
        'selected_role': {
            'role_name': 'Breast Screening'
        },
        'user_data': {
            'first_name': 'Tom',
            'last_name': 'Smith',
            'nhsid_useruid': '555249831105'
        }
    }

    def setUp(self):
        self.log_patcher = patch(f'{module}.log').start()
        self.get_session_patcher = patch(f'{module}.get_session').start()
        self.valid_session_with_selected_role_patcher = patch(f'{module}.valid_session_with_selected_role').start()
        self.update_session_patcher = patch(f'{module}.update_session').start()

    def test_lambda_raises_unauthorised_exception_when_no_headers(self):
        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__({}, {})

        self.assertEqual('Unauthorized', context.exception.args[0])

        actual_log_1 = self.log_patcher.call_args_list[0][0][0]
        actual_log_2 = self.log_patcher.call_args_list[1][0][0]

        self.assertDictEqual({'log_reference': LogReference.AUTH0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.AUTH0002}, actual_log_2)

    @patch('user_authoriser.user_authoriser.get_session_from_cookie')
    def test_lambda_raises_unauthorised_exception_when_no_session_cookie_found(self, mock_get_session_from_cookie):
        mock_get_session_from_cookie.return_value = None

        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(self.LAMBDA_EVENT, {})

        self.assertEqual('Unauthorized', context.exception.args[0])

        actual_log_1 = self.log_patcher.call_args_list[0][0][0]
        actual_log_2 = self.log_patcher.call_args_list[1][0][0]

        self.assertDictEqual({'log_reference': LogReference.AUTH0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.AUTH0002}, actual_log_2)

    def test_lambda_raised_unauthorised_exception_when_cookie_not_set(self):
        event_with_unset_cookie = {'headers': {'Cookie': ''}}

        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(event_with_unset_cookie, {})

        self.assertEqual('Unauthorized', context.exception.args[0])

        actual_log_1 = self.log_patcher.call_args_list[0][0][0]
        actual_log_2 = self.log_patcher.call_args_list[1][0][0]

        self.assertDictEqual({'log_reference': LogReference.AUTH0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.AUTH0002}, actual_log_2)

    def test_lambda_raises_unauthorised_exception_when_session_in_dynamodb_not_found(self):
        self.get_session_patcher.return_value = None
        self.valid_session_with_selected_role_patcher.return_value = False

        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(self.LAMBDA_EVENT, {})

        self.assertEqual('Unauthorized', context.exception.args[0])

        actual_log_1 = self.log_patcher.call_args_list[0][0][0]
        actual_log_2 = self.log_patcher.call_args_list[1][0][0]

        self.assertDictEqual({'log_reference': LogReference.AUTH0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.AUTH0004}, actual_log_2)

    def test_exception_is_passed_to_user_authorizer_lambda_when_update_session_fails(self):
        self.get_session_patcher.return_value = self.AUTHORISED_USER_SESSION
        self.valid_session_with_selected_role_patcher.return_value = True
        self.update_session_patcher.side_effect = Exception('update failed!')

        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(self.LAMBDA_EVENT, {})

        self.assertEqual('update failed!', context.exception.args[0])

        actual_log_1 = self.log_patcher.call_args_list[0][0][0]

        self.assertDictEqual({'log_reference': LogReference.AUTH0001}, actual_log_1)

    def test_lambda_returns_allow_and_deny_policy_given_user_with_valid_role_code_for_csas_user(self):
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'R8010'
        session['selected_role']['organisation_code'] = 'DXX'

        expected_allowed_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'POST/api/participants/*/hmr101',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/list-print-files',
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    def test_lambda_returns_allow_and_deny_policy_given_user_with_valid_role_code_for_csas_dms_user(self):
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'R8010'
        session['selected_role']['organisation_code'] = 'DXX'
        session['selected_role']['workgroups'] = ['cervicalscreening', '13q_cervicalscreening']

        expected_allowed_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/list-print-files',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'POST/api/participants/*/hmr101'
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    def test_lambda_returns_allow_and_deny_policy_given_user_with_valid_role_code_for_csas_iom_user(self):
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'R8010'
        session['selected_role']['organisation_code'] = 'DXX'
        session['selected_role']['workgroups'] = ['cervicalscreening', 'iom_cervicalscreening']

        expected_allowed_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/list-print-files',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'POST/api/participants/*/hmr101'
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    def test_lambda_returns_allow_and_deny_policy_given_user_with_valid_role_code_for_admin_user(self):
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'R8008'
        session['selected_role']['activity_codes'] = []

        expected_allowed_endpoints = [
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'POST/api/participants/*/hmr101',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations',
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/list-print-files',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    def test_lambda_returns_allow_and_deny_policy_given_user_with_valid_role_code_for_biomedical_scientist_user(self):
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'R8005'

        expected_allowed_endpoints = [
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'POST/api/participants/*/hmr101',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations',
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/list-print-files',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    def test_lambda_returns_allow_and_deny_policy_given_user_with_valid_role_code_for_clerical_user(self):
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'R8010'

        expected_allowed_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'POST/api/participants/*/hmr101',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed'
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/list-print-files',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    def test_lambda_returns_allow_and_deny_policy_given_user_with_valid_role_code_for_nurse_user(self):
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'R8001'

        expected_allowed_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'POST/api/participants/*/hmr101',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed'
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/list-print-files',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    def test_lambda_returns_allow_and_deny_policy_given_user_with_valid_role_code_for_clinical_practitioner_user(self):
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'R8000'

        expected_allowed_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'POST/api/participants/*/hmr101',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed'
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/list-print-files',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    def test_lambda_returns_allow_and_deny_policy_given_user_with_invalid_role_code(self):
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'NONSENSE_ROLE'

        expected_allowed_endpoints = [
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations'
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'POST/api/participants/*/hmr101',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/list-print-files',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    def test_lambda_returns_allow_for_deep_ping(self):
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'deep_ping'

        expected_allowed_endpoints = [
            f'{self.STAGE_URL}GET/api/non-responder-list',
            f'{self.STAGE_URL}PUT/api/participants/*/accept',
            f'{self.STAGE_URL}GET/api/participants/*/audit',
            f'{self.STAGE_URL}PUT/api/participants/*/noaction',
            f'{self.STAGE_URL}PUT/api/participants/*/cease',
            f'{self.STAGE_URL}PUT/api/participants/*/defer',
            f'{self.STAGE_URL}POST/api/participants',
            f'{self.STAGE_URL}POST/api/participants/*/hmr101',
            f'{self.STAGE_URL}GET/api/participants/*/results',
            f'{self.STAGE_URL}POST/api/participants/*/results',
            f'{self.STAGE_URL}POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            f'{self.STAGE_URL}PUT/api/participants/*/results/*',
            f'{self.STAGE_URL}DELETE/api/participants/*/results/*',
            f'{self.STAGE_URL}GET/api/participants/*/reinstate',
            f'{self.STAGE_URL}PUT/api/participants/*/reinstate',
            f'{self.STAGE_URL}PUT/api/participants/*/event',
            f'{self.STAGE_URL}GET/api/participants/*',
            f'{self.STAGE_URL}GET/api/participants/*/notifications/*',
            f'{self.STAGE_URL}GET/api/prior-notifications',
            f'{self.STAGE_URL}POST/api/roles',
            f'{self.STAGE_URL}GET/api/users/access-groups',
            f'{self.STAGE_URL}POST/api/users/audit',
            f'{self.STAGE_URL}GET/api/users',
            f'{self.STAGE_URL}POST/api/users/accept-legitimate-relations',
            f'{self.STAGE_URL}POST/api/organisation-supplemental',
            f'{self.STAGE_URL}GET/api/organisation-supplemental/*',
            f'{self.STAGE_URL}DELETE/api/organisation-supplemental/*/*',
            f'{self.STAGE_URL}GET/api/csas/results/*/*',
            f'{self.STAGE_URL}POST/api/csas/results/*/*',
            f'{self.STAGE_URL}POST/api/csas/results/*/*/lock',
            f'{self.STAGE_URL}POST/api/csas/results/*/*/unlock',
            f'{self.STAGE_URL}POST/api/csas/results/pause-participant/*',
            f'{self.STAGE_URL}POST/api/csas/results/unpause-participant/*',
            f'{self.STAGE_URL}GET/api/csas/results/*',
            f'{self.STAGE_URL}GET/api/gp-notifications',
            f'{self.STAGE_URL}PUT/api/gp-notifications/*/*/reviewed',
            f'{self.STAGE_URL}GET/api/download-print-file',
            f'{self.STAGE_URL}GET/api/confirmation-report-list',
            f'{self.STAGE_URL}GET/api/download-confirmation-report',
            f'{self.STAGE_URL}GET/api/list-print-files',
            f'{self.STAGE_URL}GET/api/gp-supplemental',
            f'{self.STAGE_URL}POST/api/manual-add-participant',
            f'{self.STAGE_URL}POST/api/participants/*/notifications/*/resend',
            f'{self.STAGE_URL}GET/api/reports/letters-sent/*',
            f'{self.STAGE_URL}GET/auth/pds-auth',
            f'{self.STAGE_URL}GET/auth/pds-callback',
            f'{self.STAGE_URL}GET/api/participants/pds-participant/*',
            f'{self.STAGE_URL}GET/api/sender/*/*',
            f'{self.STAGE_URL}POST/api/sender/*/*'
        ]

        expected_deny_endpoints = []

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    @data({}, {'is_held': True})
    @patch('os.environ', {'HOLDING_PAGE': 'TRUE'})
    @patch('user_authoriser.role_based_access.get_user_by_nhsid_useruid')
    def test_lambda_returns_allow_and_deny_policy_given_holding_page_active(self, user_data, get_user_mock):
        get_user_mock.return_value = user_data
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'R8000'

        expected_allowed_endpoints = [
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations'
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'POST/api/participants/*/hmr101',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/list-print-files',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    @patch('os.environ', {'HOLDING_PAGE': 'TRUE'})
    @patch('user_authoriser.role_based_access.get_user_by_nhsid_useruid')
    def test_lambda_returns_allow_and_deny_policy_given_holding_page_active_and_is_held_false(
            self, get_user_mock):
        get_user_mock.return_value = {'is_held': False}
        session = deepcopy(self.AUTHORISED_USER_SESSION)
        session['selected_role']['role_id'] = 'R8000'

        expected_allowed_endpoints = [
            self.STAGE_URL + 'GET/api/non-responder-list',
            self.STAGE_URL + 'PUT/api/participants/*/accept',
            self.STAGE_URL + 'PUT/api/participants/*/noaction',
            self.STAGE_URL + 'PUT/api/participants/*/cease',
            self.STAGE_URL + 'PUT/api/participants/*/defer',
            self.STAGE_URL + 'POST/api/participants',
            self.STAGE_URL + 'POST/api/participants/*/hmr101',
            self.STAGE_URL + 'GET/api/participants/*/results',
            self.STAGE_URL + 'GET/api/participants/*',
            self.STAGE_URL + 'GET/api/prior-notifications',
            self.STAGE_URL + 'POST/api/roles',
            self.STAGE_URL + 'GET/api/users/access-groups',
            self.STAGE_URL + 'GET/api/users',
            self.STAGE_URL + 'POST/api/users/accept-legitimate-relations',
            self.STAGE_URL + 'POST/api/organisation-supplemental',
            self.STAGE_URL + 'GET/api/organisation-supplemental/*',
            self.STAGE_URL + 'DELETE/api/organisation-supplemental/*/*',
            self.STAGE_URL + 'GET/api/gp-notifications',
            self.STAGE_URL + 'PUT/api/gp-notifications/*/*/reviewed'
        ]

        expected_deny_endpoints = [
            self.STAGE_URL + 'GET/api/participants/*/audit',
            self.STAGE_URL + 'POST/api/participants/*/results',
            self.STAGE_URL + 'POST/api/participants/*/results/next-test-due-date',
            self.STAGE_URL + 'GET/api/participants/*/results/next-test-due-date-after-delete/*',
            self.STAGE_URL + 'PUT/api/participants/*/results/*',
            self.STAGE_URL + 'DELETE/api/participants/*/results/*',
            self.STAGE_URL + 'GET/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/reinstate',
            self.STAGE_URL + 'PUT/api/participants/*/event',
            self.STAGE_URL + 'GET/api/participants/*/notifications/*',
            self.STAGE_URL + 'POST/api/users/audit',
            self.STAGE_URL + 'GET/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*',
            self.STAGE_URL + 'POST/api/csas/results/*/*/lock',
            self.STAGE_URL + 'POST/api/csas/results/*/*/unlock',
            self.STAGE_URL + 'POST/api/csas/results/pause-participant/*',
            self.STAGE_URL + 'POST/api/csas/results/unpause-participant/*',
            self.STAGE_URL + 'GET/api/csas/results/*',
            self.STAGE_URL + 'GET/api/download-print-file',
            self.STAGE_URL + 'GET/api/confirmation-report-list',
            self.STAGE_URL + 'GET/api/download-confirmation-report',
            self.STAGE_URL + 'GET/api/list-print-files',
            self.STAGE_URL + 'GET/api/gp-supplemental',
            self.STAGE_URL + 'POST/api/manual-add-participant',
            self.STAGE_URL + 'POST/api/participants/*/notifications/*/resend',
            self.STAGE_URL + 'GET/api/reports/letters-sent/*',
            self.STAGE_URL + 'GET/auth/pds-auth',
            self.STAGE_URL + 'GET/auth/pds-callback',
            self.STAGE_URL + 'GET/api/participants/pds-participant/*',
            self.STAGE_URL + 'GET/api/sender/*/*',
            self.STAGE_URL + 'POST/api/sender/*/*'
        ]

        self.assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            expected_allowed_endpoints, expected_deny_endpoints, session
        )

    def assert_policy_for_allow_and_deny_equal_and_mocks_called_correctly(
            self, expected_allowed_endpoints, expected_deny_endpoints, session):

        expected_statement = [
            {
                'Action': 'execute-api:Invoke',
                'Effect': 'Allow',
                'Resource': expected_allowed_endpoints
            },
        ]

        if expected_deny_endpoints:
            expected_statement.append({
                'Action': 'execute-api:Invoke',
                'Effect': 'Deny',
                'Resource': expected_deny_endpoints
            })

        self.get_session_patcher.return_value = session
        self.valid_session_with_selected_role_patcher.return_value = True
        self.update_session_patcher.return_value = {'Attributes': {'a_key': 'a_value'}}

        response = lambda_handler.__wrapped__(self.LAMBDA_EVENT, {})

        statement = response['policyDocument']['Statement']

        self.maxDiff = None

        self.assertEqual(expected_statement, statement)

        actual_log_1 = self.log_patcher.call_args_list[0][0][0]
        actual_log_2 = self.log_patcher.call_args_list[1][0][0]

        self.assertDictEqual({'log_reference': LogReference.AUTH0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.AUTH0006}, actual_log_2)
