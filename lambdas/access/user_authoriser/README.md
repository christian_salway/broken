# User Authoriser

This lambda will act as an Api Gateway Authoriser see https://docs.aws.amazon.com/apigateway/latest/developerguide/apigateway-use-lambda-authorizer.html

The lambda checks that the user has supplied a token (sp_session_cookie), and based on  the selected role code(s) of the associated user session, returns an IAM policy which specifies allow or deny access to each of the api gateway methods, for specific http verbs (GET,POST,PUT).

In the event of an Allow policy the api gateway proceeds to execute the target lambda.

In the event of a Deny Policy a 403 error is returned to the client browser.

In the event that there is no user session associated with the session token (cookie), or if the session is about to reach the maximum session time to reauthenticate i.e. 8 hours, a 401 Unauthorized response is returned to the client.
