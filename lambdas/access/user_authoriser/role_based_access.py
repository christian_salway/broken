import os
from distutils import util
from common.utils.data_segregation.nhais_ciphers import Cohorts
from common.utils.user_utils import get_user_by_nhsid_useruid


def get_rules_for_user(user):
    def role(role_id):
        user_role = user.get('selected_role', {})
        return user_role.get('role_id') == role_id

    def org(org):
        user_role = user.get('selected_role', {})
        return user_role.get('organisation_code') == org

    def user_id(user_id):
        user_data = user.get('user_data', {})
        return user_data.get('nhsiduseruid') == user_id

    def workgroup(workgroup: Cohorts):
        user_role = user.get('selected_role', {})
        return workgroup.value in user_role.get('workgroups', [])

    def no_activity_codes():
        user_role = user.get('selected_role', {})
        activity_codes = user_role.get('activity_codes', [])
        return len(activity_codes) == 0

    def is_held():
        user_id = user.get('nhsid_useruid')
        user_record = get_user_by_nhsid_useruid(user_id)
        return user_record.get('is_held', True)

    # Groups - https://nhsd-confluence.digital.nhs.uk/x/3wkJCQ section 5102
    deep_ping = role('deep_ping')

    clinical_practitioner = role('R8000')

    nurse = role('R8001')

    clerical = role('R8010') and not org('DXX')

    biomedical_scientist = role('R8005')

    admin = (role('R8008') and no_activity_codes())

    csas = role('R8010') and org('DXX')

    csas_dms = csas and workgroup(Cohorts.DMS)

    csas_iom = csas and workgroup(Cohorts.IOM)

    view_participant = clinical_practitioner or nurse or clerical or biomedical_scientist or admin or csas

    anyone = True

    holding_page = util.strtobool(os.environ.get('HOLDING_PAGE', 'FALSE')) and is_held()

    public_routes = ['/api/users/access-groups', '/api/roles', '/api/users', '/api/users/accept-legitimate-relations']

    role_based_access = {
        'groups': {
            'clinical_practitioner': clinical_practitioner,
            'nurse': nurse,
            'clerical': clerical,
            'biomedical_scientist': biomedical_scientist,
            'admin': admin,
            'csas': csas,
            'deep_ping': deep_ping,
            'csas_iom': csas_iom,
            'csas_dms': csas_dms,
            'holding_page': holding_page,
        },
        'routes': [
            {
                'route': '/api/non-responder-list',
                'method': 'GET',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/participants/*/accept',
                'method': 'PUT',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/participants/*/audit',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/*/noaction',
                'method': 'PUT',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/participants/*/cease',
                'method': 'PUT',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/participants/*/defer',
                'method': 'PUT',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/participants',
                'method': 'POST',
                'has_access': deep_ping or view_participant
            },
            {
                'route': '/api/participants/*/hmr101',
                'method': 'POST',
                'has_access': deep_ping or (
                    clinical_practitioner or nurse or clerical or biomedical_scientist or admin
                )
            },
            {
                'route': '/api/participants/*/results',
                'method': 'GET',
                'has_access': deep_ping or view_participant
            },
            {
                'route': '/api/participants/*/results',
                'method': 'POST',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/*/results/next-test-due-date',
                'method': 'POST',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/*/results/next-test-due-date-after-delete/*',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/*/results/*',
                'method': 'PUT',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/*/results/*',
                'method': 'DELETE',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/*/reinstate',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/*/reinstate',
                'method': 'PUT',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/*/event',
                'method': 'PUT',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/*',
                'method': 'GET',
                'has_access': deep_ping or view_participant
            },
            {
                'route': '/api/participants/*/notifications/*',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/prior-notifications',
                'method': 'GET',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/roles',
                'method': 'POST',
                'has_access': deep_ping or anyone
            },
            {
                'route': '/api/users/access-groups',
                'method': 'GET',
                'has_access': deep_ping or anyone
            },
            {
                'route': '/api/users/audit',
                'method': 'POST',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/users',
                'method': 'GET',
                'has_access': deep_ping or anyone
            },
            {
                'route': '/api/users/accept-legitimate-relations',
                'method': 'POST',
                'has_access': deep_ping or anyone
            },
            {
                'route': '/api/organisation-supplemental',
                'method': 'POST',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/organisation-supplemental/*',
                'method': 'GET',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/organisation-supplemental/*/*',
                'method': 'DELETE',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/csas/results/*/*',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/csas/results/*/*',
                'method': 'POST',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/csas/results/*/*/lock',
                'method': 'POST',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/csas/results/*/*/unlock',
                'method': 'POST',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/csas/results/pause-participant/*',
                'method': 'POST',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/csas/results/unpause-participant/*',
                'method': 'POST',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/csas/results/*',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/gp-notifications',
                'method': 'GET',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/gp-notifications/*/*/reviewed',
                'method': 'PUT',
                'has_access': deep_ping or (clinical_practitioner or nurse or clerical or csas)
            },
            {
                'route': '/api/download-print-file',
                'method': 'GET',
                'has_access': deep_ping or (csas_iom or csas_dms)
            },
            {
                'route': '/api/confirmation-report-list',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/download-confirmation-report',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/list-print-files',
                'method': 'GET',
                'has_access': deep_ping or (csas_iom or csas_dms)
            },
            {
                'route': '/api/gp-supplemental',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/manual-add-participant',
                'method': 'POST',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/*/notifications/*/resend',
                'method': 'POST',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/reports/letters-sent/*',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/auth/pds-auth',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/auth/pds-callback',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/participants/pds-participant/*',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/sender/*/*',
                'method': 'GET',
                'has_access': deep_ping or csas
            },
            {
                'route': '/api/sender/*/*',
                'method': 'POST',
                'has_access': deep_ping or csas
            }
        ],
    }

    if holding_page:
        for route in role_based_access['routes']:
            if route.get('route') not in public_routes:
                route['has_access'] = False

    return role_based_access
