from common.utils.session_utils import get_session, valid_session_with_selected_role, update_session
from common.utils.cookie_management_utils import get_session_from_cookie
from common.log import log
from user_authoriser.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from user_authoriser.policy_builder import AuthPolicy
from user_authoriser.role_based_access import get_rules_for_user


@lambda_entry_point
def lambda_handler(event, context):
    """
    If there is no session token found, the user has no selected role or the session cannot be updated then this lambda
    will raise Exception('Unauthorized') in order for API Gateway to return 401 Unauthorized and message 'Unauthorized'.

    If the session_token is valid, a policy must be generated which will allow or deny access to the client.

    If access is denied this lambda returns a deny policy and the client will receive a 403 Access Denied response.

    If access is allowed this lambda returns an allow policy and API Gateway will proceed with the backend integration
    configured on the method.

    Clients are granted or denied access on a per-route basis depending on the users selected role. Roles groups are
    defined within this lambda. A generated policy may contain both allow and deny statements so that a client may have
    restricted access to a subset of endpoints/routes.
    """

    log({'log_reference': LogReference.AUTH0001})

    session_token = event.get('headers') and get_session_from_cookie(event['headers'])

    if not session_token:
        log({'log_reference': LogReference.AUTH0002})
        raise Exception('Unauthorized')

    session = get_session(session_token)

    if not session:
        log({'log_reference': LogReference.AUTH0004})
        raise Exception('Unauthorized')

    if not valid_session_with_selected_role(session):
        log({'log_reference': LogReference.AUTH0005})
        raise Exception('Unauthorized')

    update_session(session_token)

    rules = get_rules_for_user(session)
    session['access_groups'] = rules['groups']
    principal_id = session['session_id']
    policy = AuthPolicy(event, principal_id)

    for route in rules['routes']:
        if route['has_access']:
            policy.allow_method(route["method"], route['route'])
        else:
            policy.deny_method(route["method"], route['route'])

    policy.set_session_attributes_in_context(session)

    auth_response = policy.build()
    log({'log_reference': LogReference.AUTH0006})
    return auth_response
