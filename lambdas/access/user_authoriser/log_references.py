import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    AUTH0001 = (logging.INFO, 'Received authorisation request.')
    AUTH0002 = (logging.ERROR, 'Could not determine session token from cookie')
    AUTH0004 = (logging.ERROR, 'Invalid session')
    AUTH0005 = (logging.ERROR, 'User has no selected role code')
    AUTH0006 = (logging.INFO, 'Authorisation request processed')
