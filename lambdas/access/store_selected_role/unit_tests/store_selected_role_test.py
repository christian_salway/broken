import json
from unittest import TestCase
from unittest.mock import patch, Mock

from common.test_assertions.api_assertions import assertApiResponse
from store_selected_role.log_references import LogReference
from store_selected_role.store_selected_role import lambda_handler, get_role_from_session_roles
from common.utils.audit_utils import AuditActions


session_token = '654321'
user_data = {
    'first_name': 'Bob',
    'last_name': 'Smith',
    'user_roles': [{'role_id': '456dafp789', 'role_name': 'test role'}]
}


@patch('store_selected_role.store_selected_role.audit', Mock())
@patch('store_selected_role.store_selected_role.update_session', Mock())
class TestStoreSelectedRole(TestCase):

    def setUp(self):
        super(TestStoreSelectedRole, self).setUp()
        self.session = {
            'session_token': '654321',
            'user_data': user_data
        }
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    @patch('store_selected_role.store_selected_role.log')
    @patch('store_selected_role.store_selected_role.update_session')
    @patch('store_selected_role.store_selected_role.get_role_from_session_roles')
    def test_selected_role_returns_OK(self, get_role_mock, update_session_mock, log_mock):
        # Arrange
        cookie = f'sp_session_cookie={session_token}'

        event = {'headers': {'cookie': cookie}, 'body': json.dumps({'role_id': '456dafp789'})}

        get_role_mock.return_value = {'role_id': '456dafp789', 'role_name': 'test role'}

        update_session_mock.return_value = {'Attributes': {'some_key': 'some_value'}}

        # Act
        actual_response = self.unwrapped_handler(event, {}, self.session)

        # Assert
        assertApiResponse(self, 200, {}, actual_response)

        actual_log_1 = log_mock.call_args_list[0][0][0]
        actual_log_2 = log_mock.call_args_list[1][0][0]
        actual_log_3 = log_mock.call_args_list[2][0][0]
        actual_log_4 = log_mock.call_args_list[3][0][0]
        actual_log_5 = log_mock.call_args_list[4][0][0]

        self.assertDictEqual({'log_reference': LogReference.STOREROLE0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0004, 'role_id=': '456dafp789'},  actual_log_2)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0006, 'number_of_roles': 1},  actual_log_3)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0007},  actual_log_4)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0008},  actual_log_5)

    @patch('store_selected_role.store_selected_role.log')
    @patch('store_selected_role.store_selected_role.store_role_in_sessions_table')
    @patch('store_selected_role.store_selected_role.get_role_from_session_roles')
    def test_selected_role_raises_exception_if_error_updating_session(
            self, get_role_mock, store_role_mock, log_mock):
        # Arrange
        cookie = f'sp_session_cookie={session_token}'

        event = {'headers': {'cookie': cookie}, 'body': json.dumps({'role_id': '456dafp789'})}

        get_role_mock.return_value = {'role_id': '456dafp789', 'role_name': 'test role'}

        store_role_mock.side_effect = Exception('update failed!')

        # Act
        with self.assertRaises(Exception) as context:
            self.unwrapped_handler(event, {}, self.session)

        self.assertEqual('update failed!', context.exception.args[0])

        # Assert
        actual_log_1 = log_mock.call_args_list[0][0][0]
        actual_log_2 = log_mock.call_args_list[1][0][0]
        actual_log_3 = log_mock.call_args_list[2][0][0]
        actual_log_4 = log_mock.call_args_list[3][0][0]
        actual_log_5 = log_mock.call_args_list[4][0][0]

        self.assertDictEqual({'log_reference': LogReference.STOREROLE0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0004, 'role_id=': '456dafp789'},  actual_log_2)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0006, 'number_of_roles': 1},  actual_log_3)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0007},  actual_log_4)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0009},  actual_log_5)

    @patch('store_selected_role.store_selected_role.log')
    def test_selected_role_returns_403_when_session_not_found(self, log_mock):
        # Arrange
        cookie = f'sp_session_cookie={session_token}'
        event = {'headers': {'cookie': cookie}, 'body': json.dumps({'role_id': '454dafp789'})}

        # Act
        actual_response = self.unwrapped_handler(event, {}, {})

        # Assert
        assertApiResponse(self, 403, {'message': 'Session missing, expired or not logged in.'}, actual_response)

        actual_log_1 = log_mock.call_args_list[0][0][0]
        actual_log_2 = log_mock.call_args_list[1][0][0]

        self.assertDictEqual({'log_reference': LogReference.STOREROLE0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0002},  actual_log_2)

    @patch('store_selected_role.store_selected_role.log')
    def test_selected_role_returns_400_when_no_role_id_in_request_body(self, log_mock):
        # Arrange
        cookie = f'sp_session_cookie={session_token}'
        event = {'headers': {'cookie': cookie}, 'body': json.dumps({})}

        # Act
        actual_response = self.unwrapped_handler(event, {}, self.session)

        # Assert
        assertApiResponse(self, 400, {'message': 'Role id missing from request.'}, actual_response)

        actual_log_1 = log_mock.call_args_list[0][0][0]
        actual_log_2 = log_mock.call_args_list[1][0][0]

        self.assertDictEqual({'log_reference': LogReference.STOREROLE0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0003},  actual_log_2)

    @patch('store_selected_role.store_selected_role.log')
    def test_selected_role_returns_400_when_no_roles_found_in_user_data(self, log_mock):
        # Arrange
        cookie = f'sp_session_cookie={session_token}'
        event = {'headers': {'cookie': cookie}, 'body': json.dumps({'role_id': '456dafp789'})}

        session = self.session = {
            'session_token': '654321',
            'user_data': {
                'first_name': 'bob',
                'last_name': 'fred',
                'user_roles': []
            }
        }
        # Act
        actual_response = self.unwrapped_handler(event, {}, session)

        # Assert
        assertApiResponse(self, 400, {'message': 'Invalid role requested.'}, actual_response)

        actual_log_1 = log_mock.call_args_list[0][0][0]
        actual_log_2 = log_mock.call_args_list[1][0][0]
        actual_log_3 = log_mock.call_args_list[2][0][0]

        self.assertDictEqual({'log_reference': LogReference.STOREROLE0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0004, 'role_id=': '456dafp789'},  actual_log_2)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0005},  actual_log_3)

    @patch('store_selected_role.store_selected_role.log')
    def test_selected_role_returns_400_when_requested_role_not_found_in_user_data(self, log_mock):
        # Arrange
        cookie = f'sp_session_cookie={session_token}'
        event = {'headers': {'cookie': cookie}, 'body': json.dumps({'role_id': '123456'})}

        # Act
        actual_response = self.unwrapped_handler(event, {}, self.session)

        # Assert
        assertApiResponse(self, 400, {'message': 'Invalid role requested.'}, actual_response)

        actual_log_1 = log_mock.call_args_list[0][0][0]
        actual_log_2 = log_mock.call_args_list[1][0][0]
        actual_log_3 = log_mock.call_args_list[2][0][0]
        actual_log_4 = log_mock.call_args_list[3][0][0]

        self.assertDictEqual({'log_reference': LogReference.STOREROLE0001}, actual_log_1)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0004, 'role_id=': '123456'},  actual_log_2)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0006, 'number_of_roles': 1},  actual_log_3)
        self.assertDictEqual({'log_reference': LogReference.STOREROLE0010},  actual_log_4)

    def test_get_role_from_session_roles_returns_selected_role(self):
        # Arrange
        role_id = "703375335317"
        roles = [
            {
                'role_id': '703375335307',
                'role_name': 'Breast Screening',
                'organisation_code': 'X09',
                'organisation_name': 'NHS CONNECTING FOR HEALTH (CFHS)'
            },
            {
                'role_id': '703375335568',
                'role_name': 'System Administrator',
                'organisation_code': 'X09',
                'organisation_name': 'NHS CONNECTING FOR HEALTH (CFHS)'
            },
            {
                'role_id': '703375335317',
                'role_name': 'Systems Support Access Role',
                'organisation_code': 'X09',
                'organisation_name': 'NHS CONNECTING FOR HEALTH (CFHS)'
            }]
        expected_response = {
            'role_id': '703375335317',
            'role_name': 'Systems Support Access Role',
            'organisation_code': 'X09',
            'organisation_name': 'NHS CONNECTING FOR HEALTH (CFHS)'
        }
        # Act
        actual_response = get_role_from_session_roles(role_id, roles)

        # Assert
        assert actual_response == expected_response


@patch('store_selected_role.store_selected_role.log', Mock())
class TestAudit(TestCase):

    def setUp(self):
        self.session = {
            'session_token': '654321',
            'user_data': user_data
        }
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    @patch('store_selected_role.store_selected_role.update_session')
    @patch('store_selected_role.store_selected_role.get_role_from_session_roles')
    @patch('store_selected_role.store_selected_role.audit')
    def test_lambda_calls_audit_with_session_from_the_arguments(self, audit_mock, get_role_mock, update_session_mock):
        # Arrange

        event = {'headers': None, 'body': json.dumps({'role_id': '456dafp789'})}

        get_role_mock.return_value = {'role_id': '456dafp789', 'role_name': 'test role'}

        update_session_mock.return_value = self.session
        self.unwrapped_handler(event, {}, self.session)
        _, kwargs = audit_mock.call_args

        self.assertEqual(kwargs['session'], self.session)

    @patch('store_selected_role.store_selected_role.update_session')
    @patch('store_selected_role.store_selected_role.get_role_from_session_roles')
    @patch('store_selected_role.store_selected_role.audit')
    def test_lambda_calls_audit_with_the_correct_action(self, audit_mock, get_role_mock, update_session_mock):
        # Arrange

        event = {'headers': None, 'body': json.dumps({'role_id': '456dafp789'})}

        get_role_mock.return_value = {'role_id': '456dafp789', 'role_name': 'test role'}

        update_session_mock.return_value = self.session
        self.unwrapped_handler(event, {}, self.session)
        _, kwargs = audit_mock.call_args

        self.assertEqual(kwargs['action'], AuditActions.SELECT_ROLE)

    @patch('store_selected_role.store_selected_role.audit')
    def test_lambda_does_not_call_audit_if_fails(self, audit_mock):

        self.unwrapped_handler({}, {}, {})

        self.assertFalse(audit_mock.called)
