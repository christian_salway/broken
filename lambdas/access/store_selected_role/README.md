This lambda accepts a user role ID and uses the user's cookie find their dynamoDB session and 
check the user has the requested role in their user data. 
If the session exists and role exists, the role information from user_roles corresponding with the role_id
and org_name is stored against the session in DynamoDB

## Response
* Scenario: Role stored successfully

    Status code: 200 OK 
