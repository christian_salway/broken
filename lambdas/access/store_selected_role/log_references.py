import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    STOREROLE0001 = (logging.INFO, 'Request received to store selected role.')
    STOREROLE0002 = (logging.ERROR, 'Session missing, expired or not logged in.')
    STOREROLE0003 = (logging.ERROR, 'Request has no body or role_id.')
    STOREROLE0004 = (logging.INFO, 'Role ID requested.')
    STOREROLE0005 = (logging.ERROR, 'No roles found for user.')
    STOREROLE0006 = (logging.INFO, 'Found roles for user.')
    STOREROLE0007 = (logging.INFO, 'Selected role found in stored session roles.')
    STOREROLE0008 = (logging.INFO, 'Successfully stored selected role in sessions table.')
    STOREROLE0009 = (logging.ERROR, 'Failed to store selected role in sessions table.')
    STOREROLE0010 = (logging.ERROR, 'Selected role not found in stored session roles.')
