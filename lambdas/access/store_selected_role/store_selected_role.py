import json

from common.utils.audit_utils import AuditActions, audit
from common.utils.lambda_wrappers import lambda_entry_point, auth_lambda
from common.utils import json_return_object, json_return_message
from common.log import log
from common.utils.session_utils import update_session
from store_selected_role.log_references import LogReference


@lambda_entry_point
@auth_lambda
def lambda_handler(event, context, session):
    log({'log_reference': LogReference.STOREROLE0001})

    if not session or not session.get('user_data'):
        log({'log_reference': LogReference.STOREROLE0002})
        return json_return_message(403, 'Session missing, expired or not logged in.')

    session_token = session['session_token']
    body = json.loads(event.get('body'))
    role_id = body.get('role_id')

    if role_id is None:
        log({'log_reference': LogReference.STOREROLE0003})
        return json_return_message(400, 'Role id missing from request.')

    log({'log_reference': LogReference.STOREROLE0004, 'role_id=': role_id})

    user_data = session['user_data']
    roles = user_data['user_roles']

    if len(roles) == 0:
        log({'log_reference': LogReference.STOREROLE0005})
        return json_return_message(400, 'Invalid role requested.')

    log({'log_reference': LogReference.STOREROLE0006, 'number_of_roles': len(roles)})
    role = get_role_from_session_roles(role_id, roles)

    if not role:
        log({'log_reference': LogReference.STOREROLE0010})
        return json_return_message(400, 'Invalid role requested.')

    log({'log_reference': LogReference.STOREROLE0007})

    try:
        updated_session = store_role_in_sessions_table(session_token, role)

        audit(session=updated_session, action=AuditActions.SELECT_ROLE)

        return json_return_object(200)
    except Exception as e:
        log({'log_reference': LogReference.STOREROLE0009})
        raise e


def get_role_from_session_roles(role_id, roles):
    for role in roles:
        if role_id in role['role_id']:
            return role


def store_role_in_sessions_table(session_token, role):
    updated_session = update_session(session_token, {'selected_role': role})

    log({'log_reference': LogReference.STOREROLE0008})

    return updated_session
