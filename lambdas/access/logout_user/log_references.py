import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    LOGOUT0001 = (logging.INFO, 'Received request to logout user')
    LOGOUT0002 = (logging.INFO, 'Successfully destroyed session')
    LOGOUT0003 = (logging.ERROR, 'Unable to audit logout - user id not known for session')
    LOGOUT0004 = (logging.INFO, 'Redirecting to logged-out page')
