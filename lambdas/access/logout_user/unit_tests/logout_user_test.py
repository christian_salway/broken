from unittest import TestCase
from mock import patch, MagicMock
from common.utils.audit_utils import AuditActions
from logout_user.log_references import LogReference

mock_env_vars = {
    'ENVIRONMENT_DOMAIN': 'test-environment',
    'SESSION_TABLE_NAME': 'table-name',
}

with patch('common.log.log') as mock_log, patch('os.environ', mock_env_vars):
    from logout_user.logout_user import lambda_handler


@patch('logout_user.logout_user.audit', MagicMock())
@patch('common.log.logger', MagicMock())
@patch('os.environ', mock_env_vars)
class TestLogoutUser(TestCase):

    def setUp(self):
        super(TestLogoutUser, self).setUp()
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__.__wrapped__

    @patch('logout_user.logout_user.delete_session')
    def test_delete_session_invoked_if_session_cookie_present(self, delete_mock):
        event = {'headers': {'Cookie': 'sp_session_cookie="cookie"'}}
        self.unwrapped_handler(event, {}, {'session_token': '123456'})
        delete_mock.assert_called_with('123456')

    @patch('logout_user.logout_user.delete_session')
    def test_delete_session_invoked_if_session_cookie_present_and_expired(self, delete_mock):
        event = {'headers': {'Cookie': 'sp_session_cookie="cookie"; expires=Sat, 18 Sep 2010 00:00:00 GMT'}}
        self.unwrapped_handler(event, {}, {'session_token': '123456'})
        delete_mock.assert_called_with('123456')

    @patch('logout_user.logout_user.delete_cookie_header')
    @patch('logout_user.logout_user.delete_session')
    def test_delete_session_not_invoked_if_session_cookie_not_present(self, delete_session_mock, delete_cookie_mock):
        delete_cookie_header_value = 'sp_session_cookie=""; expires=Sat, 18 Sep 2010 00:00:00 GMT'
        delete_cookie_mock.return_value = delete_cookie_header_value

        event = {'headers': {'Cookie': 'wrong_session_cookie_name="123456"; expires=Sat, 18 Sep 2010 00:00:00 GMT'}}

        response = self.unwrapped_handler(event, {}, {})

        delete_session_mock.assert_not_called()

        self.assertEqual(302, response['statusCode'])
        self.assertEqual('https://test-environment/#/logged-out', response['headers']['Location'])
        self.assertEqual('application/json', response['headers']['Content-Type'])
        self.assertEqual('sp_session_cookie=""; expires=Sat, 18 Sep 2010 00:00:00 GMT',
                         response['headers']['Set-Cookie'])

    @patch('logout_user.logout_user.delete_cookie_header')
    def test_lambda_returns_expired_cookie_if_session_cookie_present(self, mock_delete_cookie_header):
        delete_cookie_header_value = 'sp_session_cookie=""; expires=Sat, 18 Sep 2010 00:00:00 GMT'
        mock_delete_cookie_header.return_value = delete_cookie_header_value
        event = {'headers': {'Cookie': 'sp_session_cookie="123456"'}}

        response = self.unwrapped_handler(event, {}, {})

        self.assertEqual(302, response['statusCode'])
        self.assertEqual('https://test-environment/#/logged-out', response['headers']['Location'])
        self.assertEqual('application/json', response['headers']['Content-Type'])
        self.assertEqual('sp_session_cookie=""; expires=Sat, 18 Sep 2010 00:00:00 GMT',
                         response['headers']['Set-Cookie'])

    @patch('logout_user.logout_user.delete_cookie_header')
    @patch('logout_user.logout_user.delete_session')
    def test_lambda_adds_no_roles_to_location_when_called_with_no_roles(
            self, delete_session_mock, mock_delete_cookie_header):
        mock_delete_cookie_header.return_value = 'deleted cookie'
        event = {
            'headers': {'Cookie': 'sp_session_cookie="123456"'},
            'queryStringParameters': {'reason': 'noRoles'}
        }

        response = self.unwrapped_handler(event, {}, {'session_token': '123456'})

        self.assertEqual(302, response['statusCode'])
        self.assertEqual('https://test-environment/#/logged-out?reason=noRoles', response['headers']['Location'])
        delete_session_mock.assert_called_once_with('123456')

    @patch('logout_user.logout_user.delete_cookie_header')
    def test_lambda_does_not_error_when_query_string_parameters_are_none(self, mock_delete_cookie_header):
        delete_cookie_header_value = 'sp_session_cookie=""; expires=Sat, 18 Sep 2010 00:00:00 GMT'
        mock_delete_cookie_header.return_value = delete_cookie_header_value

        event = {'headers': {'Cookie': 'sp_session_cookie="123456"'}, 'queryStringParameters': None}

        return_headers = self.unwrapped_handler(event, {}, {})['headers']

        self.assertListEqual(list(return_headers), ['Content-Type', 'Location', 'Set-Cookie'])
        self.assertEqual(return_headers['Set-Cookie'], delete_cookie_header_value)

    @patch('logout_user.logout_user.delete_cookie_header')
    @patch('logout_user.logout_user.delete_session')
    def test_lambda_redirects_to_logout_error_page_if_error_query_parameter_supplied(
            self, delete_session_mock, mock_delete_cookie_header):
        delete_cookie_header_value = 'sp_session_cookie=""; expires=Sat, 18 Sep 2010 00:00:00 GMT'
        mock_delete_cookie_header.return_value = delete_cookie_header_value

        event = {
            'headers': {
                'Cookie': 'sp_session_cookie="cookie"'
            },
            'queryStringParameters': {
                'reason': 'unauthorised'
            }
        }

        response = self.unwrapped_handler(event, {}, {'session_token': '123456'})

        self.assertEqual(302, response['statusCode'])
        self.assertEqual('https://test-environment/#/logged-out?reason=unauthorised', response['headers']['Location'])
        self.assertEqual('application/json', response['headers']['Content-Type'])
        self.assertEqual('sp_session_cookie=""; expires=Sat, 18 Sep 2010 00:00:00 GMT',
                         response['headers']['Set-Cookie'])

        delete_session_mock.assert_called_once_with('123456')

    @patch('logout_user.logout_user.delete_cookie_header')
    @patch('logout_user.logout_user.delete_session')
    def test_lambda_redirects_to_logout_error_page_with_session_id_if_error_and_session_id_present(
            self, delete_session_mock, mock_delete_cookie_header):
        delete_cookie_header_value = 'sp_session_cookie=""; expires=Sat, 18 Sep 2010 00:00:00 GMT'
        mock_delete_cookie_header.return_value = delete_cookie_header_value

        event = {
            'headers': {
                'Cookie': 'sp_session_cookie="cookie"'
            },
            'queryStringParameters': {
                'reason': 'unauthorised'
            }
        }

        response = self.unwrapped_handler(event, {}, {'session_token': '123456', 'session_id': 'my_session'})

        self.assertEqual(302, response['statusCode'])
        self.assertEqual('https://test-environment/#/logged-out?reason=unauthorised&session_id=my_session',
                         response['headers']['Location'])
        self.assertEqual('application/json', response['headers']['Content-Type'])
        self.assertEqual('sp_session_cookie=""; expires=Sat, 18 Sep 2010 00:00:00 GMT',
                         response['headers']['Set-Cookie'])

        delete_session_mock.assert_called_once_with('123456')


class TestAudit(TestCase):

    def setUp(self):
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__.__wrapped__
        self.session_data = {
            'session_token': 'test_session_token',
            'user_data': {'nhsid_useruid': 'userid'}
        }

    @patch('logout_user.logout_user.audit')
    @patch('logout_user.logout_user.delete_session')
    @patch('logout_user.logout_user.log', MagicMock())
    def test_lambda_calls_audit_with_session_from_the_arguments(self, delete_session_mock, audit_mock):
        self.unwrapped_handler({'headers': None}, {}, self.session_data)
        _, kwargs = audit_mock.call_args

        self.assertEqual(kwargs['session'], self.session_data)
        delete_session_mock.assert_called_once_with('test_session_token')

    @patch('logout_user.logout_user.audit')
    @patch('logout_user.logout_user.delete_session')
    @patch('logout_user.logout_user.log', MagicMock())
    def test_lambda_calls_audit_with_the_correct_action(self, delete_session_mock, audit_mock):
        self.unwrapped_handler({'headers': None}, {}, self.session_data)
        _, kwargs = audit_mock.call_args

        self.assertEqual(kwargs['action'], AuditActions.LOGOUT)
        delete_session_mock.assert_called_once_with('test_session_token')

    @patch('logout_user.logout_user.audit')
    @patch('logout_user.logout_user.log')
    @patch('logout_user.logout_user.delete_session')
    def test_lambda_does_not_call_audit_if_user_has_no_useruid(self, delete_session_mock, mock_logger, audit_mock):
        session_data = {'session_token': 'test_session_token'}
        self.unwrapped_handler({'headers': None}, {}, session_data)

        self.assertFalse(audit_mock.called)
        actual_log = mock_logger.call_args_list[-2][0][0]
        self.assertDictEqual({'log_reference': LogReference.LOGOUT0003}, actual_log)
        delete_session_mock.assert_called_once_with('test_session_token')

    @patch('logout_user.logout_user.audit')
    @patch('logout_user.logout_user.log', MagicMock())
    def test_lambda_does_not_call_audit_if_lambda_fails(self, audit_mock):
        with self.assertRaises(Exception):
            self.unwrapped_handler(None, {}, {1: 1})

        self.assertFalse(audit_mock.called)
