from common.log import log
from common.utils.audit_utils import AuditActions, audit
from common.utils.lambda_wrappers import lambda_entry_point, auth_exception_wrapper, auth_lambda
from common.utils.cookie_management_utils import delete_cookie_header
from common.utils.session_utils import delete_session
from logout_user.log_references import LogReference
import os
import json

ENVIRONMENT_DOMAIN = os.getenv('ENVIRONMENT_DOMAIN')


@auth_exception_wrapper
@lambda_entry_point
@auth_lambda
def lambda_handler(event, context, session):
    log({'log_reference': LogReference.LOGOUT0001})

    if not session:
        return redirect_to_logged_out_page_and_delete_cookie_response()

    # get session_token
    session_token = session.get('session_token')

    # destroy session
    delete_session(session_token)
    log({'log_reference': LogReference.LOGOUT0002})

    if session.get('user_data', {}).get('nhsid_useruid'):
        audit(session=session, action=AuditActions.LOGOUT)
    else:
        log({'log_reference': LogReference.LOGOUT0003})

    query_string_parameters = event.get('queryStringParameters')
    logout_reason = query_string_parameters.get('reason') if query_string_parameters else None

    log({'log_reference': LogReference.LOGOUT0004, 'reason': logout_reason})
    session_id = session.get('session_id', None)
    return redirect_to_logged_out_page_and_delete_cookie_response(logout_reason, session_id)


def redirect_to_logged_out_page_and_delete_cookie_response(reason=None, session_id=None):
    location = f'https://{ENVIRONMENT_DOMAIN}/#/logged-out'
    if reason:
        location += f'?reason={reason}'
        if session_id:
            location += f'&session_id={session_id}'

    return {
        'statusCode': 302,
        'headers': {
            'Content-Type': 'application/json',
            'Location': location,
            'Set-Cookie': delete_cookie_header()
        },
        'body': json.dumps({'message': 'You have been logged out.'}),
        'isBase64Encoded': False
    }
