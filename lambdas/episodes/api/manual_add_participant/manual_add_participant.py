import json
import os
import boto3
import base64
from time import time
from datetime import datetime
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.log import log
from common.utils import json_return_message, json_return_data
from common.utils.sqs_utils import get_sqs_client
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.pds_record_utils import PDS_RECORD_KEYS_FOR_SIGNATURE, get_pds_record_fields_for_signature
from .log_references import LogReference
from .manual_add_participant_service import (
    participant_already_exists,
    add_participant
)

TIMESTAMP_EXPIRY = 1800

KMS_MASTER_KEY_IDENT = os.environ.get('KMS_MASTER_KEY_IDENT')


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):

    log({'log_reference': LogReference.MANUALADDPART0001})

    event_data = get_api_request_info_from_event(event)[0]
    session = event_data['session']

    try:
        request_body = json.loads(event['body'])
    except Exception:
        log({'log_reference': LogReference.MANUALADDPARTER0001})
        return json_return_message(400, 'Missing or malformed request body')

    pds_data = request_body.get('pds_data')
    if not pds_data:
        log({'log_reference': LogReference.MANUALADDPARTER0002})
        return json_return_message(400, 'PDS data missing')

    timestamp = pds_data.get('timestamp')
    if timestamp:
        pds_data.update({'pds_timestamp': timestamp})
    if not _is_pds_data_complete(pds_data):
        log({'log_reference': LogReference.MANUALADDPARTER0003})
        return json_return_message(400, 'PDS data incomplete')

    # workaround - UI corrupts the original pds date of birth by converting it to an iso-format time
    pds_data['date_of_birth'] = datetime.fromisoformat(pds_data['date_of_birth'].replace('Z', '')).strftime('%Y-%m-%d')

    signature = request_body.get('signature')
    if not signature:
        log({'log_reference': LogReference.MANUALADDPARTER0004})
        return json_return_message(401, 'Signature missing')
    if not _is_signature_complete(signature):
        log({'log_reference': LogReference.MANUALADDPARTER0005, 'signature': signature})
        return json_return_message(401, 'Signature incomplete')
    if not _is_signature_valid(pds_data, signature):
        log({'log_reference': LogReference.MANUALADDPARTER0006, 'ciphertext': signature['ciphertext']})
        return json_return_message(401, 'Signature invalid')

    timestamp = pds_data.get('pds_timestamp')
    if not _is_timestamp_valid(timestamp):
        log({'log_reference': LogReference.MANUALADDPARTER0007})
        return json_return_message(401, 'Timestamp expired')

    nhs_number = pds_data.get('nhs_number')
    if participant_already_exists(session, nhs_number):
        log({'log_reference': LogReference.MANUALADDPARTER0008})
        return json_return_message(400, 'Participant with this NHS number already exists')

    try:
        participant_id = add_participant(session, pds_data, request_body.get('crm_case_details', {}))
        _send_to_block_request_participant_ids_queue(participant_id)
    except Exception as e:
        log({'log_reference': LogReference.MANUALADDPARTER0009, 'error': str(e)})
        return json_return_message(500, 'Cannot complete request')

    return json_return_data(200, {'participant_id': participant_id})


def _is_pds_data_complete(pds_record):
    return all(key in pds_record for key in PDS_RECORD_KEYS_FOR_SIGNATURE)


def _is_signature_complete(signature):
    return 'ciphertext' in signature


def _is_signature_valid(pds_data, signature):
    rx_side_data = get_pds_record_fields_for_signature(pds_data)
    plaintext_rx_side = bytes(str(rx_side_data), 'utf-8')
    client = boto3.client('kms')
    base64_bytes = signature['ciphertext'].encode('utf-8')
    decoded = client.decrypt(
        CiphertextBlob=base64.b64decode(base64_bytes),
        EncryptionContext={'nhs_number': pds_data['nhs_number']}
    )
    plaintext_tx_side = decoded['Plaintext']
    if plaintext_rx_side != plaintext_tx_side:
        log({'log_reference': LogReference.MANUALADDPARTER0010,
             'tx side': plaintext_tx_side.decode("utf-8"),
             'rx side': plaintext_rx_side.decode("utf-8")})
    return plaintext_rx_side == plaintext_tx_side


def _is_timestamp_valid(timestamp):
    now = int(time())
    try:
        valid = now <= int(timestamp) + TIMESTAMP_EXPIRY
        return valid
    except ValueError:
        return False


def _send_to_block_request_participant_ids_queue(participant_id):
    participant_ids = {'participant_ids': [participant_id]}
    get_sqs_client().send_message(QueueUrl=os.environ.get('BLOCK_REQUEST_PARTICIPANT_IDS_QUEUE_URL'),
                                  MessageBody=json.dumps(participant_ids))
