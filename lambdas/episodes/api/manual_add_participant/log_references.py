import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    MANUALADDPART0001 = (logging.INFO, 'Received request to manually add participant.')
    MANUALADDPART0002 = (logging.INFO, 'Checking for exising participant.')
    MANUALADDPART0003 = (logging.INFO, 'Adding participant.')
    MANUALADDPART0004 = (logging.INFO, 'Add participant complete.')

    MANUALADDPARTER0001 = (logging.ERROR, 'Request body missing.')
    MANUALADDPARTER0002 = (logging.ERROR, 'PDS data missing.')
    MANUALADDPARTER0003 = (logging.ERROR, 'PDS data incomplete.')
    MANUALADDPARTER0004 = (logging.ERROR, 'Signature missing.')
    MANUALADDPARTER0005 = (logging.ERROR, 'Signature incomplete.')
    MANUALADDPARTER0006 = (logging.ERROR, 'Signature invalid.')
    MANUALADDPARTER0007 = (logging.ERROR, 'Timestamp expired.')
    MANUALADDPARTER0008 = (logging.ERROR, 'Participant with this NHS number already exists.')
    MANUALADDPARTER0009 = (logging.ERROR, 'Error processing request.')
    MANUALADDPARTER0010 = (logging.ERROR, 'Plaintext mismatch.')
