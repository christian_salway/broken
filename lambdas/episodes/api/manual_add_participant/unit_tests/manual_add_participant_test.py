import json
from unittest.case import TestCase
from mock import patch, call, Mock
from ddt import ddt, data, unpack

from manual_add_participant.log_references import LogReference


module = 'manual_add_participant.manual_add_participant'

mock_env_vars = {
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'BLOCK_REQUEST_PARTICIPANT_IDS_QUEUE_URL': 'BLOCK_REQUEST_PARTICIPANT_IDS_QUEUE_URL',
    'KMS_MASTER_KEY_IDENT': 'master_key_ident'
}


class TestManualAddParticipant(TestCase):
    @classmethod
    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    def setUpClass(cls):
        global manual_add_participant_module
        import manual_add_participant.manual_add_participant as manual_add_participant_module

    def setUp(self):
        super(TestManualAddParticipant, self).setUp()
        self.unwrapped_handler = manual_add_participant_module.lambda_handler.__wrapped__.__wrapped__

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_manual_add_participant_returns_400_if_request_body_missing(self,
                                                                        get_api_request_mock,
                                                                        log_mock):
        # Given
        api_request = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {}
            },
            '',
            '')
        get_api_request_mock.return_value = api_request
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Missing or malformed request body"}',
            'isBase64Encoded': False
        }
        event = {}

        # When
        response = self.unwrapped_handler(event, 'context')

        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with(event)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0001}),
            call({'log_reference': LogReference.MANUALADDPARTER0001})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_manual_add_participant_returns_400_if_pds_data_missing(self,
                                                                    get_api_request_mock,
                                                                    log_mock):
        # Given
        api_request = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {}
            },
            '',
            '')
        get_api_request_mock.return_value = api_request
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "PDS data missing"}',
            'isBase64Encoded': False
        }
        event = {'body': json.dumps({})}

        # When
        response = self.unwrapped_handler(event, 'context')

        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with(event)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0001}),
            call({'log_reference': LogReference.MANUALADDPARTER0002})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}._is_pds_data_complete')
    def test_manual_add_participant_returns_400_if_pds_data_incomplete(self,
                                                                       pds_data_complete_mock,
                                                                       get_api_request_mock,
                                                                       log_mock):
        # Given
        api_request = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {}
            },
            '',
            '')
        get_api_request_mock.return_value = api_request
        pds_data_complete_mock.return_value = False
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "PDS data incomplete"}',
            'isBase64Encoded': False
        }
        event = {'body': json.dumps({'pds_data': {'nhs_number': '1'}})}

        # When
        response = self.unwrapped_handler(event, 'context')

        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with(event)
        pds_data_complete_mock.assert_called_once_with({'nhs_number': '1'})
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0001}),
            call({'log_reference': LogReference.MANUALADDPARTER0003})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}._is_pds_data_complete')
    def test_manual_add_participant_returns_401_if_signature_missing(self,
                                                                     pds_data_complete_mock,
                                                                     get_api_request_mock,
                                                                     log_mock):
        # Given
        api_request = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {}
            },
            '',
            '')
        get_api_request_mock.return_value = api_request
        pds_data_complete_mock.return_value = True
        expected_response = {
            'statusCode': 401,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Signature missing"}',
            'isBase64Encoded': False
        }
        pds_data = {
            'nhs_number': '1',
            'pds_timestamp': '1',
            'date_of_birth': '1990-01-01T00:00:00.000Z'
        }
        event = {'body': json.dumps({'pds_data': pds_data})}

        # When
        response = self.unwrapped_handler(event, 'context')

        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with(event)
        pds_data_complete_mock.assert_called_once_with(
            {'nhs_number': '1', 'pds_timestamp': '1', 'date_of_birth': '1990-01-01'})
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0001}),
            call({'log_reference': LogReference.MANUALADDPARTER0004})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}._is_pds_data_complete')
    @patch(f'{module}._is_signature_complete')
    def test_manual_add_participant_returns_401_if_signature_incomplete(self,
                                                                        pds_data_complete_mock,
                                                                        has_signature_fields_mock,
                                                                        get_api_request_mock,
                                                                        log_mock):
        # Given
        api_request = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {}
            },
            '',
            '')
        get_api_request_mock.return_value = api_request
        has_signature_fields_mock.return_value = True
        pds_data_complete_mock.return_value = False
        expected_response = {
            'statusCode': 401,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Signature incomplete"}',
            'isBase64Encoded': False
        }
        pds_data = {
            'nhs_number': '1',
            'pds_timestamp': '1',
            'date_of_birth': '1990-01-01T00:00:00.000Z'
        }
        event = {'body': json.dumps({'pds_data': pds_data, 'signature': {'ciphertext': '1'}})}

        # When
        response = self.unwrapped_handler(event, 'context')

        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with(event)
        has_signature_fields_mock.assert_called_once_with(
            {'nhs_number': '1', 'pds_timestamp': '1', 'date_of_birth': '1990-01-01'})
        pds_data_complete_mock.assert_called_once_with({'ciphertext': '1'})
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0001}),
            call({'log_reference': LogReference.MANUALADDPARTER0005, 'signature': {'ciphertext': '1'}})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}._is_pds_data_complete')
    @patch(f'{module}._is_signature_complete')
    @patch(f'{module}._is_signature_valid')
    def test_manual_add_participant_returns_401_if_signature_invalid(self,
                                                                     signature_valid_mock,
                                                                     signature_complete_mock,
                                                                     pds_data_complete_mock,
                                                                     get_api_request_mock,
                                                                     log_mock):
        # Given
        api_request = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {}
            },
            '',
            '')
        get_api_request_mock.return_value = api_request
        pds_data_complete_mock.return_value = True
        signature_complete_mock.return_value = True
        signature_valid_mock.return_value = False
        expected_response = {
            'statusCode': 401,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Signature invalid"}',
            'isBase64Encoded': False
        }
        pds_data = {
            'nhs_number': '1',
            'pds_timestamp': '1',
            'date_of_birth': '1990-01-01T00:00:00.000Z'
        }
        event = {'body': json.dumps({'pds_data': pds_data, 'signature': {'ciphertext': '1'}})}

        # When
        response = self.unwrapped_handler(event, 'context')

        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with(event)
        pds_data_complete_mock.assert_called_once_with(
            {'nhs_number': '1', 'pds_timestamp': '1', 'date_of_birth': '1990-01-01'})
        signature_complete_mock.assert_called_once_with({'ciphertext': '1'})
        signature_valid_mock.assert_called_once_with(
            {'nhs_number': '1', 'pds_timestamp': '1', 'date_of_birth': '1990-01-01'}, {'ciphertext': '1'})
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0001}),
            call({'log_reference': LogReference.MANUALADDPARTER0006, 'ciphertext': '1'})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}._is_pds_data_complete')
    @patch(f'{module}._is_signature_complete')
    @patch(f'{module}._is_signature_valid')
    @patch(f'{module}._is_timestamp_valid')
    def test_manual_add_participant_returns_401_if_timestamp_expired(self,
                                                                     timestamp_valid_mock,
                                                                     signature_valid_mock,
                                                                     signature_complete_mock,
                                                                     pds_data_complete_mock,
                                                                     get_api_request_mock,
                                                                     log_mock):
        # Given
        api_request = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {}
            },
            '',
            '')
        get_api_request_mock.return_value = api_request
        pds_data_complete_mock.return_value = True
        signature_complete_mock.return_value = True
        signature_valid_mock.return_value = True
        timestamp_valid_mock.return_value = False
        expected_response = {
            'statusCode': 401,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Timestamp expired"}',
            'isBase64Encoded': False
        }
        pds_data = {
            'nhs_number': '1',
            'pds_timestamp': '1',
            'date_of_birth': '1990-01-01T00:00:00.000Z'
        }
        event = {'body': json.dumps({
            'pds_data': pds_data,
            'signature': {'ciphertext': '1'}
        })}

        # When
        response = self.unwrapped_handler(event, 'context')

        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with(event)
        pds_data_complete_mock.assert_called_once_with(
            {'nhs_number': '1', 'pds_timestamp': '1', 'date_of_birth': '1990-01-01'})
        signature_complete_mock.assert_called_once_with({'ciphertext': '1'})
        signature_valid_mock.assert_called_once_with(
            {'nhs_number': '1', 'pds_timestamp': '1', 'date_of_birth': '1990-01-01'}, {'ciphertext': '1'})
        timestamp_valid_mock.assert_called_once_with('1')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0001}),
            call({'log_reference': LogReference.MANUALADDPARTER0007})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}._is_pds_data_complete')
    @patch(f'{module}._is_signature_complete')
    @patch(f'{module}._is_signature_valid')
    @patch(f'{module}._is_timestamp_valid')
    @patch(f'{module}.participant_already_exists')
    def test_manual_add_participant_returns_400_if_NHS_number_already_exists(self,
                                                                             participant_exists_mock,
                                                                             timestamp_valid_mock,
                                                                             signature_valid_mock,
                                                                             signature_complete_mock,
                                                                             pds_data_complete_mock,
                                                                             get_api_request_mock,
                                                                             log_mock):
        # Given
        api_request = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {'user': 'session_user'}
            },
            '',
            '')
        get_api_request_mock.return_value = api_request
        pds_data_complete_mock.return_value = True
        signature_complete_mock.return_value = True
        signature_valid_mock.return_value = True
        timestamp_valid_mock.return_value = True
        participant_exists_mock.return_value = True
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Participant with this NHS number already exists"}',
            'isBase64Encoded': False
        }
        pds_data = {
            'nhs_number': '1',
            'pds_timestamp': '1',
            'date_of_birth': '1990-01-01T00:00:00.000Z'
        }
        event = {'body': json.dumps({
            'pds_data': pds_data,
            'signature': {'ciphertext': '1'}
        })}

        # When
        response = self.unwrapped_handler(event, 'context')

        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with(event)
        pds_data_complete_mock.assert_called_once_with(
            {'pds_timestamp': '1', 'nhs_number': '1', 'date_of_birth': '1990-01-01'})
        signature_complete_mock.assert_called_once_with({'ciphertext': '1'})
        signature_valid_mock.assert_called_once_with(
            {'pds_timestamp': '1', 'nhs_number': '1', 'date_of_birth': '1990-01-01'}, {'ciphertext': '1'})
        timestamp_valid_mock.assert_called_once_with('1')
        participant_exists_mock.assert_called_once_with({'user': 'session_user'}, '1')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0001}),
            call({'log_reference': LogReference.MANUALADDPARTER0008})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}._is_pds_data_complete')
    @patch(f'{module}._is_signature_complete')
    @patch(f'{module}._is_signature_valid')
    @patch(f'{module}._is_timestamp_valid')
    @patch(f'{module}.participant_already_exists')
    @patch(f'{module}.add_participant')
    @patch(f'{module}._send_to_block_request_participant_ids_queue')
    def test_manual_add_participant_returns_500_on_service_function_exception(self,
                                                                              send_to_queue_mock,
                                                                              add_participant_mock,
                                                                              participant_exists_mock,
                                                                              timestamp_valid_mock,
                                                                              signature_valid_mock,
                                                                              signature_complete_mock,
                                                                              pds_data_complete_mock,
                                                                              get_api_request_mock,
                                                                              log_mock):
        # Given
        api_request = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {'user': 'session_user'}
            },
            '',
            '')
        get_api_request_mock.return_value = api_request
        pds_data_complete_mock.return_value = True
        signature_complete_mock.return_value = True
        signature_valid_mock.return_value = True
        timestamp_valid_mock.return_value = True
        participant_exists_mock.return_value = False
        add_participant_mock.side_effect = Exception('Test')
        expected_response = {
            'statusCode': 500,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Cannot complete request"}',
            'isBase64Encoded': False
        }
        pds_data = {
            'nhs_number': '1',
            'pds_timestamp': '1',
            'date_of_birth': '1990-01-01T00:00:00.000Z'
        }
        event = {'body': json.dumps({
            'pds_data': pds_data,
            'signature': {'ciphertext': '1'},
            'crm_case_details': {'crm_number': '1', 'comments': 'a'}
        })}

        # When
        response = self.unwrapped_handler(event, 'context')

        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with(event)
        pds_data_complete_mock.assert_called_once_with(
            {'pds_timestamp': '1', 'nhs_number': '1', 'date_of_birth': '1990-01-01'})
        signature_complete_mock.assert_called_once_with({'ciphertext': '1'})
        signature_valid_mock.assert_called_once_with(
            {'pds_timestamp': '1', 'nhs_number': '1', 'date_of_birth': '1990-01-01'}, {'ciphertext': '1'})
        timestamp_valid_mock.assert_called_once_with('1')
        participant_exists_mock.assert_called_once_with({'user': 'session_user'}, '1')
        add_participant_mock.assert_called_once_with(
            {'user': 'session_user'}, {'pds_timestamp': '1', 'nhs_number': '1', 'date_of_birth': '1990-01-01'},
            {'crm_number': '1', 'comments': 'a'})
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0001}),
            call({'log_reference': LogReference.MANUALADDPARTER0009, 'error': 'Test'})
        ])

        send_to_queue_mock.assert_not_called()

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}._is_pds_data_complete')
    @patch(f'{module}._is_signature_complete')
    @patch(f'{module}._is_signature_valid')
    @patch(f'{module}._is_timestamp_valid')
    @patch(f'{module}.participant_already_exists')
    @patch(f'{module}.add_participant')
    @patch(f'{module}._send_to_block_request_participant_ids_queue')
    def test_manual_add_participant_calls_through_to_service(self,
                                                             send_to_queue_mock,
                                                             add_participant_mock,
                                                             participant_exists_mock,
                                                             timestamp_valid_mock,
                                                             signature_valid_mock,
                                                             signature_complete_mock,
                                                             pds_data_complete_mock,
                                                             get_api_request_mock,
                                                             log_mock):
        # Given
        api_request = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {'user': 'session_user'}
            },
            '',
            '')
        get_api_request_mock.return_value = api_request
        pds_data_complete_mock.return_value = True
        signature_complete_mock.return_value = True
        signature_valid_mock.return_value = True
        timestamp_valid_mock.return_value = True
        participant_exists_mock.return_value = False
        add_participant_mock.return_value = 'participant_id_1'
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": {"participant_id": "participant_id_1"}}',
            'isBase64Encoded': False
        }
        pds_data = {
            'nhs_number': '1',
            'pds_timestamp': '1',
            'date_of_birth': '1990-01-01T00:00:00.000Z'
        }
        event = {'body': json.dumps({
            'pds_data': pds_data,
            'signature': {'ciphertext': '1'},
            'crm_case_details': {'crm_number': '1', 'comments': 'a'}
        })}

        # When
        response = self.unwrapped_handler(event, 'context')

        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with(event)
        pds_data_complete_mock.assert_called_once_with(
            {'pds_timestamp': '1', 'nhs_number': '1', 'date_of_birth': '1990-01-01'})
        signature_complete_mock.assert_called_once_with({'ciphertext': '1'})
        signature_valid_mock.assert_called_once_with(
            {'pds_timestamp': '1', 'nhs_number': '1', 'date_of_birth': '1990-01-01'}, {'ciphertext': '1'})
        timestamp_valid_mock.assert_called_once_with('1')
        participant_exists_mock.assert_called_once_with({'user': 'session_user'}, '1')
        add_participant_mock.assert_called_once_with(
            {'user': 'session_user'}, {'pds_timestamp': '1', 'nhs_number': '1', 'date_of_birth': '1990-01-01'},
            {'crm_number': '1', 'comments': 'a'})
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0001})
        ])
        send_to_queue_mock.assert_called_once_with('participant_id_1')


@ddt
class TestManualAddParticipantFunctions(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.env_patcher = patch('os.environ', mock_env_vars)
        global manual_add_participant_module
        import manual_add_participant.manual_add_participant as manual_add_participant_module

    def setUp(self):
        super(TestManualAddParticipantFunctions, self).setUp()
        self.env_patcher.start()

    def tearDown(self):
        self.env_patcher.stop()

    @unpack
    @data(
        (
            {},
            False
        ),
        (
            {
                'NHS_number': '1'
            },
            False
        ),
        (
            {
                'nhs_number': '1',
                'first_name': 'firsty',
                'last_name': 'lasty',
                'title': 'Sir',
                'date_of_birth': '2001-01-01T00:00:00.000Z',
                'address': {},
                'pds_timestamp': '1'
            },
            True
        )
    )
    def test_is_pds_data_complete(self,
                                  pds_data,
                                  expected_response):
        # Given
        # When
        actual_response = manual_add_participant_module._is_pds_data_complete(pds_data)
        # Then
        self.assertEqual(expected_response, actual_response)

    @unpack
    @data(
        (
            {},
            False
        ),
        (
            {
                'ciphertext': '123456'
            },
            True
        )
    )
    def test_is_signature_complete(self,
                                   signature,
                                   expected_response):
        # Given
        # When
        actual_response = manual_add_participant_module._is_signature_complete(signature)
        # Then
        self.assertEqual(expected_response, actual_response)

    @patch(f'{module}.log')
    @patch('boto3.client')
    def test_is_signature_valid(self,
                                client_mock,
                                log_mock):
        # Given
        pds_data = {
            'nhs_number': '1',
            'first_name': 'firsty',
            'last_name': 'lasty',
            'title': 'Sir',
            'date_of_birth': '1990-01-01',
            'address': {
                'address_line_1': '1 some road',
                'postcode': 'post code'
            },
            'pds_timestamp': '1'
        }
        plaintext_tx_side = bytes(str(pds_data), 'utf-8')
        signature = {
            'ciphertext': 'cyphertext_string'
        }
        mock_client = Mock()
        mock_client.decrypt.return_value = {
            'Plaintext': plaintext_tx_side
        }
        client_mock.return_value = mock_client

        # When
        valid = manual_add_participant_module._is_signature_valid(pds_data, signature)
        # Then
        self.assertEqual(valid, True)

    @patch(f'{module}.get_sqs_client')
    def test_send_to_block_request_participant_ids_queue(self,
                                                         get_sqs_client_mock):
        # Given
        participant_id = 'participant_id_1'
        mock_queue = Mock()
        mock_queue.send_message = Mock()
        get_sqs_client_mock.return_value = mock_queue

        # When
        manual_add_participant_module._send_to_block_request_participant_ids_queue(participant_id)

        # Then
        get_sqs_client_mock.assert_called()
        mock_queue.send_message.assert_called_once_with(QueueUrl='BLOCK_REQUEST_PARTICIPANT_IDS_QUEUE_URL',
                                                        MessageBody='{"participant_ids": ["participant_id_1"]}')
