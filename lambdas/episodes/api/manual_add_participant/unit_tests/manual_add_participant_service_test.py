from unittest.case import TestCase
from mock import patch, call
from ddt import ddt, data, unpack
from common.utils.audit_utils import AuditActions
from manual_add_participant.manual_add_participant import LogReference
from datetime import date

module = 'manual_add_participant.manual_add_participant_service'


@ddt
class TestManualAddParticipantService(TestCase):
    @classmethod
    def setUpClass(cls):
        global manual_add_participant_service_module
        import manual_add_participant.manual_add_participant_service as manual_add_participant_service_module  

    def setUp(self):
        super(TestManualAddParticipantService, self).setUp()

    @unpack
    @data(
        (
            [{'participant_id': 'id1', 'nhs_number': '12345'}],
            ['id1'],
            ['12345']
        ),
        (
            [{'participant_id': 'id1', 'nhs_number': '12345'},
             {'participant_id': 'id2', 'nhs_number': '12345'}],
            ['id1', 'id2'],
            ['12345', '12345']
        )

    )
    @patch(f'{module}.log')
    @patch(f'{module}.query_participants_by_nhs_number')
    @patch(f'{module}.sanitise_nhs_number')
    @patch(f'{module}.audit')
    def test_participant_already_exists_returns_true_if_they_exist(self,
                                                                   existing_participants,
                                                                   participant_ids,
                                                                   nhs_numbers,
                                                                   audit_mock,
                                                                   sanitise_mock,
                                                                   query_participants_by_nhs_number_mock,
                                                                   log_mock):
        # Given
        nhs_number = '12345'
        sanitise_mock.return_value = nhs_number
        query_participants_by_nhs_number_mock.return_value = existing_participants
        session = 'session'

        # When
        actual_result = manual_add_participant_service_module.participant_already_exists(session, nhs_number)

        # Then
        self.assertEqual(True, actual_result)
        sanitise_mock.assert_called_once_with(nhs_number)
        query_participants_by_nhs_number_mock.assert_called_once_with(nhs_number)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0002, 'nhs_number': nhs_number})
        ])
        audit_mock.assert_called_once_with(session='session',
                                           action=AuditActions.SEARCH_PARTICIPANTS,
                                           participant_ids=participant_ids,
                                           nhs_numbers=nhs_numbers)

    @unpack
    @data(
        (
            [],
        ),
        (
            {},
        )
    )
    @patch(f'{module}.log')
    @patch(f'{module}.query_participants_by_nhs_number')
    @patch(f'{module}.sanitise_nhs_number')
    @patch(f'{module}.audit')
    def test_participant_already_exists_returns_false_if_none_exist(self,
                                                                    existing_participants,
                                                                    audit_mock,
                                                                    sanitise_mock,
                                                                    query_participants_by_nhs_number_mock,
                                                                    log_mock):
        # Given
        nhs_number = '12345'
        sanitise_mock.return_value = nhs_number
        query_participants_by_nhs_number_mock.return_value = existing_participants
        session = 'session'

        # When
        actual_result = manual_add_participant_service_module.participant_already_exists(session, nhs_number)

        # Then
        self.assertEqual(False, actual_result)
        sanitise_mock.assert_called_once_with(nhs_number)
        query_participants_by_nhs_number_mock.assert_called_once_with(nhs_number)
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0002, 'nhs_number': nhs_number})
        ])

    @patch(f'{module}.sanitise_nhs_number')
    @patch(f'{module}.generate_participant_id')
    @patch(f'{module}.calculate_next_test_due_date_for_new_participant')
    def test_create_new_participant_record(self,
                                           calculate_ntdd_mock,
                                           generate_mock,
                                           sanitise_mock):
        # Given
        nhs_number = '1234567890'
        PDS_data = {
            'nhs_number': nhs_number,
            'first_name': 'firsty',
            'last_name': 'lasty',
            'title': 'Sir',
            'date_of_birth': '2021-01-01',
            'address': {
                'address_line_1': '1 some road',
                'address_line_2': 'some village',
                'address_line_3': 'some area',
                'address_line_4': 'some_county',
                'address_line_5': '',
                'postcode': 'A12 3BC'
            },
        }
        sanitise_mock.return_value = nhs_number
        generate_mock.return_value = '[UUID]'
        expected_record = {
            'participant_id': '[UUID]',
            'sort_key': 'PARTICIPANT',
            'nhs_number': nhs_number,
            'sanitised_nhs_number': nhs_number,
            'first_name': 'firsty',
            'last_name': 'lasty',
            'title': 'Sir',
            'date_of_birth': '2021-01-01',
            'address': {
                'address_line_1': '1 some road',
                'address_line_2': 'some village',
                'address_line_3': 'some area',
                'address_line_4': 'some_county',
                'address_line_5': '',
                'postcode': 'A12 3BC'
            },
            'status': 'ROUTINE',
            'next_test_due_date': '2023-01-01',
            'add_block_request_type': 'ADA'
        }
        expected_birth_date = date(2021, 1, 1)
        expected_next_test_due_date = date(2023, 1, 1)
        calculate_ntdd_mock.return_value = expected_next_test_due_date

        # When
        actual_record = manual_add_participant_service_module._create_new_participant_record(nhs_number, PDS_data)

        # Then
        self.assertEqual(expected_record, actual_record)
        calculate_ntdd_mock.assert_called_once_with(expected_birth_date)
        generate_mock.assert_called()
        sanitise_mock.assert_called_once_with('1234567890')

    @patch(f'{module}.log')
    @patch(f'{module}.sanitise_nhs_number')
    @patch(f'{module}._create_new_participant_record')
    @patch(f'{module}.create_replace_record_in_participant_table')
    @patch(f'{module}.audit')
    def test_add_participant(self,
                             audit_mock,
                             create_replace_record_mock,
                             create_new_record_mock,
                             sanitise_mock,
                             log_mock):
        # Given
        nhs_number = '1234567890'
        mock_participant_id = '[UUID]'
        mock_participant_record = {
            'participant_id': mock_participant_id,
            'sort_key': 'PARTICIPANT',
            'nhs_number': nhs_number,
            'sanitised_nhs_number': nhs_number,
            'first_name': 'firsty',
            'last_name': 'lasty',
            'title': 'Sir',
            'date_of_birth': '2021-01-01',
            'address': {
                'address_line_1': '1 some road',
                'address_line_2': 'some village',
                'address_line_3': 'some area',
                'address_line_4': 'some_county',
                'address_line_5': '',
                'postcode': 'A12 3BC'
            },
            'status': 'ROUTINE',
            'next_test_due_date': '2023-01-01'
        }
        sanitise_mock.return_value = nhs_number
        create_new_record_mock.return_value = mock_participant_record
        PDS_data = {'nhs_number': nhs_number}
        session = 'session'
        crm_case_details = {'crm_number': '1', 'comments': 'a'}

        # When
        participant_id = manual_add_participant_service_module.add_participant(session, PDS_data, crm_case_details)

        # Then
        self.assertEqual(participant_id, mock_participant_id)
        sanitise_mock.assert_called_once_with(nhs_number)
        create_new_record_mock.assert_called_once_with(nhs_number, PDS_data)
        create_replace_record_mock.assert_called_once_with(mock_participant_record)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANUALADDPART0003, 'nhs_number': nhs_number}),
            call({'log_reference': LogReference.MANUALADDPART0004}),
        ])
        audit_mock.assert_called_once_with(session='session',
                                           action=AuditActions.MANUAL_ADD_PARTICIPANT,
                                           participant_ids=[mock_participant_id],
                                           nhs_numbers=[nhs_number],
                                           additional_information={'crm_number': '1', 'comments': 'a'})

    @unpack
    @data(
        (
            {},
            {}
        ),
        (
            {'not': 'relevant'},
            {}
        ),
        (
            {'crm_number': '1', 'comments': 'a'},
            {'crm_number': '1', 'comments': 'a'}
        )
    )
    def test_get_additional_information(self,
                                        crm_case_details,
                                        expected_response):
        # Given
        # When
        actual_response = manual_add_participant_service_module._get_additional_information(crm_case_details)
        # Then
        self.assertEqual(expected_response, actual_response)
