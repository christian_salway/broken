from datetime import datetime
from common.utils.participant_utils import (
    query_participants_by_nhs_number,
    generate_participant_id,
    create_replace_record_in_participant_table
)
from common.utils.nhs_number_utils import sanitise_nhs_number
from common.log import log
from .log_references import LogReference
from common.utils.audit_utils import (
    audit,
    AuditActions
)
from common.models.participant import ParticipantStatus
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator import (
    calculate_next_test_due_date_for_new_participant
)


def participant_already_exists(session, nhs_number):
    sanitised_nhs_number = sanitise_nhs_number(nhs_number)
    log({'log_reference': LogReference.MANUALADDPART0002, 'nhs_number': sanitised_nhs_number})
    nhs_number_matches = query_participants_by_nhs_number(sanitised_nhs_number)
    if not nhs_number_matches:
        return False
    participant_ids = [participant['participant_id'] for participant in nhs_number_matches]
    nhs_numbers = [participant['nhs_number'] for participant in nhs_number_matches]
    if participant_ids:
        audit(session=session,
              action=AuditActions.SEARCH_PARTICIPANTS,
              participant_ids=participant_ids,
              nhs_numbers=nhs_numbers)
    return True


def add_participant(session, pds_data, crm_case_detais):
    sanitised_nhs_number = sanitise_nhs_number(pds_data.get('nhs_number'))
    log({'log_reference': LogReference.MANUALADDPART0003, 'nhs_number': sanitised_nhs_number})
    participant_record = _create_new_participant_record(sanitised_nhs_number, pds_data)
    create_replace_record_in_participant_table(participant_record)
    participant_id = participant_record['participant_id']
    additional_information = _get_additional_information(crm_case_detais)
    audit(session=session,
          action=AuditActions.MANUAL_ADD_PARTICIPANT,
          participant_ids=[participant_id],
          nhs_numbers=[sanitised_nhs_number],
          additional_information=additional_information)
    log({'log_reference': LogReference.MANUALADDPART0004})
    return participant_id


def _create_new_participant_record(nhs_number, pds_data):
    date_of_birth = datetime.strptime(pds_data['date_of_birth'], '%Y-%m-%d').date()
    participant_record = {
        'participant_id': generate_participant_id(),
        'sort_key': 'PARTICIPANT',
        'nhs_number': nhs_number,
        'sanitised_nhs_number': sanitise_nhs_number(nhs_number),
        'first_name': pds_data.get('first_name'),
        'last_name': pds_data.get('last_name'),
        'title': pds_data.get('title'),
        'date_of_birth': date_of_birth.isoformat(),
        'address': pds_data.get('address'),
        'status': ParticipantStatus.ROUTINE.value,
        'next_test_due_date': calculate_next_test_due_date_for_new_participant(date_of_birth).isoformat(),
        'add_block_request_type': 'ADA'
    }
    return participant_record


def _get_additional_information(crm_case_details):
    return {key: crm_case_details[key] for key in ['crm_number', 'comments'] if key in crm_case_details}
