import json
from datetime import datetime, timezone
from unittest import TestCase
from unittest.mock import patch, Mock, call

from common.utils.data_segregation.nhais_ciphers import Cohorts
from get_pnl_by_gp_practice_code.component_tests.get_pnl_by_gp_practice_code_data import \
    participant_1_records, participant_2_records, participant_3_records, organisation_data, \
    expected_participant_1_object, expected_participant_2_object, expected_participant_3_object, expected_log_calls

environ_mock = {
    'DYNAMODB_ORGANISATION_SUPPLEMENTAL': 'organisation_supplemental_table',
    'DYNAMODB_ORGANISATIONS': 'organisations_table',
    'DYNAMODB_PARTICIPANTS': 'participants_table',
    'AUDIT_QUEUE_URL': 'AUDIT QUEUE',
}
sqs_client_mock = Mock()
participants_table_mock = Mock()
organisation_supplemental_table_mock = Mock()

example_date = datetime(2020, 1, 23, 13, 48, 8, 123456, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

mock_filtered_participants = {'PARTICIPANT1': 'MAN', 'PARTICIPANT2': 'MAN', 'PARTICIPANT3': 'MAN'}

with patch('datetime.datetime', datetime_mock):
    from get_pnl_by_gp_practice_code import get_pnl_by_gp_practice_code


@patch('common.log.get_internal_id', Mock(return_value='1'))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
       Mock(return_value=organisation_supplemental_table_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('common.utils.organisation_supplementary_utils.uuid4', Mock(return_value='test-uuid-12345'))
@patch('os.environ', environ_mock)
@patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids',
       Mock(return_value=mock_filtered_participants))
class TestGetPNL(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        sqs_client_mock.reset_mock()
        organisation_supplemental_table_mock.reset_mock()
        participants_table_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.audit_utils.datetime')
    @patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('common.utils.participant_utils.Key')
    @patch('common.utils.participant_episode_utils.Key')
    @patch('common.log.logging')
    @patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    def test_get_pnl_by_gp_practise_code_returns_correctly_sorted_list(
            self, logging_mock, episode_condition_key_mock,
            participant_condition_key_mock, audit_datetime_mock):
        self.maxDiff = None
        log_mock = Mock()

        logging_mock.getLogger.return_value = log_mock
        test_event = {'headers': {}, 'requestContext': {'authorizer': {
            'session': json.dumps(
                {
                    'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                    'user_data': {'nhsid_useruid': 'USER_UID', 'first_name': 'firsty', 'last_name': 'lasty'},
                    'session_id': 'TEST SESSION'
                }),
            'principalId': 'PRINCIPAL ID'
        }}}

        test_context = Mock()
        test_context.function_name = 'get_pnl_by_gp_practise_code'

        episode_condition_key_mock.return_value.eq.return_value = True
        episode_condition_key_mock.return_value.begins_with.return_value = True
        participant_condition_key_mock.return_value.eq.return_value = 'condition'
        participants_table_mock.query.side_effect = [
            {'Items': [
                {
                    'participant_id': 'PARTICIPANT1',
                    'test_due_date': '2020-03-13',
                    'proposed_test_due_date': '2021-04-01',
                    'sort_key': 'PARTICIPANT',
                    'nhais_cipher': 'MAN'
                },
                {
                    'participant_id': 'PARTICIPANT2',
                    'test_due_date': '2020-03-13',
                    'proposed_test_due_date': '2020-01-02',
                    'sort_key': 'PARTICIPANT',
                    'nhais_cipher': 'MAN'
                },
                {
                    'participant_id': 'PARTICIPANT3',
                    'test_due_date': '2020-03-13',
                    'proposed_test_due_date': '2020-11-19',
                    'sort_key': 'PARTICIPANT',
                    'nhais_cipher': 'MAN'
                }
            ]},
            {'Items': participant_1_records},
            {'Items': participant_2_records},
            {'Items': participant_3_records}
        ]
        organisation_supplemental_table_mock.query.return_value = {'Items': [organisation_data]}

        audit_datetime_mock.now.return_value.isoformat.return_value = '2020-01-01'

        response = get_pnl_by_gp_practice_code.lambda_handler(test_event, test_context)

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': json.dumps({'data': [
                expected_participant_1_object,
                expected_participant_3_object,
                expected_participant_2_object
            ],
                'links': {'self': ''}
            }),
            'isBase64Encoded': False
        }

        self.assertEqual(expected_response, response)

        episode_condition_key_mock.assert_has_calls([
            call('live_record_status'),
            call().eq('PNL'),
            call('registered_gp_practice_code_and_test_due_date'),
            call().begins_with('DXX')
        ])
        participant_condition_key_mock.assert_has_calls([
            call('participant_id'),
            call().eq('PARTICIPANT1'),
            call('participant_id'),
            call().eq('PARTICIPANT2'),
            call('participant_id'),
            call().eq('PARTICIPANT3')
        ])

        self.assertEqual(participants_table_mock.query.call_count, 4)

        participants_table_mock.query.assert_has_calls(
            [
               call(IndexName='live-record-status',
                    KeyConditionExpression=True)
            ] +
            [
               call(
                   ExpressionAttributeNames={'#result': 'result',
                                             '#action': 'action',
                                             '#status': 'status'},
                   KeyConditionExpression='condition',
                   ProjectionExpression='nhs_number, sort_key, participant_id, title, first_name, middle_names, last_name, gender, address, date_of_birth, date_of_death, is_ceased, is_invalid_patient, next_test_due_date, registered_gp_practice_code, infection_result, infection_code, #result, #action, action_code, test_date, recall_months, result_code, sender_code, source_code, sender_source_type, slide_number, sending_lab, #status, suppression_reason, invited_date, reason, date_from, is_fp69, event_text, health_authority, crm, active, reason_for_removal_code, reason_for_removal_effective_from_date, hpv_primary, self_sample, practice_code, nhais_cipher, is_paused, is_paused_received_time', 
                   ScanIndexForward=False
               )
            ] * 3
            )
        expected_audit_record = {
            'action': 'GET_PNL_BY_GP_PRACTICE_CODE',
            'timestamp': '2020-01-01',
            'internal_id': '1',
            'nhsid_useruid': 'USER_UID',
            'session_id': 'TEST SESSION',
            'first_name': 'firsty',
            'last_name': 'lasty',
            'role_id': 'R8010',
            'user_organisation_code': 'DXX',
            'participant_ids': ["PARTICIPANT1", "PARTICIPANT3", "PARTICIPANT2"],
            'nhs_numbers': ['9999999999', '9999999997', '9999999998'],
            'gp_practice_code': 'DXX'
        }
        organisation_supplemental_table_mock.put_item.assert_called_once()
        organisation_supplemental_table_mock.put_item.assert_called_with(Item={
            'organisation_code': 'DXX',
            'type_value': 'PRINT_PNL#2020-01-23T13:48:08.123456+00:00#firsty.lasty',
            'organisation_supplemental_id': 'test-uuid-12345'
        })

        sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='AUDIT QUEUE', MessageBody=json.dumps(expected_audit_record))
        log_mock.log.assert_has_calls(expected_log_calls, any_order=True)

    @patch('common.utils.audit_utils.datetime')
    @patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('common.utils.participant_utils.Key')
    @patch('common.utils.participant_episode_utils.Key')
    @patch('common.log.logging')
    @patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.IOM]))
    def test_get_pnl_by_gp_practise_code_returns_no_results_from_different_workgroup(
            self, logging_mock, episode_condition_key_mock,
            participant_condition_key_mock, audit_datetime_mock):
        log_mock = Mock()

        logging_mock.getLogger.return_value = log_mock
        test_event = {'headers': {}, 'requestContext': {'authorizer': {
            'session': json.dumps(
                {
                    'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                    'user_data': {'nhsid_useruid': 'USER_UID', 'first_name': 'firsty', 'last_name': 'lasty'},
                    'session_id': 'TEST SESSION'
                }),
            'principalId': 'PRINCIPAL ID'
        }}}

        test_context = Mock()
        test_context.function_name = 'get_pnl_by_gp_practise_code'

        episode_condition_key_mock.return_value.eq.return_value = True
        episode_condition_key_mock.return_value.begins_with.return_value = True
        participant_condition_key_mock.return_value.eq.return_value = 'condition'
        participants_table_mock.query.side_effect = [
            {'Items': []}
        ]
        organisation_supplemental_table_mock.query.return_value = {'Items': [organisation_data]}

        audit_datetime_mock.now.return_value.isoformat.return_value = '2020-01-01'

        response = get_pnl_by_gp_practice_code.lambda_handler(test_event, test_context)

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': json.dumps({'data': [],
                                'links': {'self': ''}
                                }),
            'isBase64Encoded': False
        }

        self.assertEqual(expected_response, response)

        episode_condition_key_mock.assert_has_calls([
            call('live_record_status'),
            call().eq('PNL'),
            call('registered_gp_practice_code_and_test_due_date'),
            call().begins_with('DXX')
        ])
        participant_condition_key_mock.assert_not_called()

        self.assertEqual(participants_table_mock.query.call_count, 1)

        participants_table_mock.query.assert_has_calls([
            call(IndexName='live-record-status',
                 KeyConditionExpression=True)
        ]
        )
        organisation_supplemental_table_mock.put_item.assert_called_once()
        organisation_supplemental_table_mock.put_item.assert_called_with(Item={
            'organisation_code': 'DXX',
            'type_value': 'PRINT_PNL#2020-01-23T13:48:08.123456+00:00#firsty.lasty',
            'organisation_supplemental_id': 'test-uuid-12345'
        })

        sqs_client_mock.send_message.assert_not_called()

        log_mock.log.assert_has_calls(
            [call(20, '{"log_reference": "PNLGP0005", "message": "No episodes found for gp id"}', exc_info=False)],
            any_order=True)
