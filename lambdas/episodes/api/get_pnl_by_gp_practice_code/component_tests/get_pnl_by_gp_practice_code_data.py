from unittest.mock import call

from common.models.participant import ParticipantStatus

participant_1_records = [
    {
        'sort_key': 'PARTICIPANT',
        'participant_id': 'PARTICIPANT1',
        'title': 'LADY',
        'first_name': 'DUCK',
        'middle_names': 'VON',
        'last_name': 'QUACK',
        'nhs_number': '9999999999',
        'nhais_cipher': 'MAN',
        'date_of_birth': '1994-02-24',
        'status': ParticipantStatus.SUSPENDED
    },
    {
        'sort_key': 'RESULT#1',
        'participant_id': 'PARTICIPANT1',
        'test_date': '2019-03-01'
    },
    {
        'sort_key': 'RESULT#2',
        'participant_id': 'PARTICIPANT1',
        'test_date': '2020-04-19',
        'action': 'ACTION',
        'action_code': 'ACTION CODE',
        'result': 'RESULT',
        'result_code': 'RESULT CODE',
        'infection_result': 'INFECTION RESULT',
        'infection_code': 'INFECTION CODE'
    },
    {
        'sort_key': 'RESULT#3',
        'participant_id': 'PARTICIPANT1',
        'test_date': '2018-11-12'
    }
]

expected_participant_1_object = {
    "nhs_number": "9999999999",
    "date_of_birth": "1994-02-24",
    "gender": None,
    "title": "LADY",
    "first_name": "DUCK",
    "middle_names": "VON",
    "last_name": "QUACK",
    "next_test_due_date": None,
    "date_of_death": None,
    "is_invalid_patient": None,
    "status": "SUSPENDED",
    "is_fp69": None,
    "event_text": None,
    "test_results": [
        {
            "sort_key": "RESULT#1",
            "title": None,
            "first_name": None,
            "last_name": None,
            "date_of_birth": None,
            "gender": None,
            "health_authority": None,
            "sending_lab": None,
            "source_code": None,
            "sender_code": None,
            "sender_source_type": None,
            "slide_number": None,
            "test_date": "2019-03-01",
            "infection_result": None,
            "infection_code": None,
            "result": None,
            "result_code": None,
            "action": None,
            "action_code": None,
            "hpv_primary": None,
            "self_sample": None,
            "recall_months": None,
            "crm": [],
            "notification_status": None,
            "nhais_cipher": None
        },
        {
            "sort_key": "RESULT#2",
            "title": None,
            "first_name": None,
            "last_name": None,
            "date_of_birth": None,
            "gender": None,
            "health_authority": None,
            "sending_lab": None,
            "source_code": None,
            "sender_code": None,
            "sender_source_type": None,
            "slide_number": None,
            "test_date": "2020-04-19",
            "infection_result": "INFECTION RESULT",
            "infection_code": "INFECTION CODE",
            "result": "RESULT",
            "result_code": "RESULT CODE",
            "action": "ACTION",
            "action_code": "ACTION CODE",
            "hpv_primary": None,
            "self_sample": None,
            "recall_months": None,
            "crm": [],
            "notification_status": None,
            "nhais_cipher": None
        },
        {
            "sort_key": "RESULT#3",
            "title": None,
            "first_name": None,
            "last_name": None,
            "date_of_birth": None,
            "gender": None,
            "health_authority": None,
            "sending_lab": None,
            "source_code": None,
            "sender_code": None,
            "sender_source_type": None,
            "slide_number": None,
            "test_date": "2018-11-12",
            "infection_result": None,
            "infection_code": None,
            "result": None,
            "result_code": None,
            "action": None,
            "action_code": None,
            "hpv_primary": None,
            "self_sample": None,
            "recall_months": None,
            "crm": [],
            "notification_status": None,
            "nhais_cipher": None
        }
    ],
    "registered_gp_practice": {
        "organisation_code": "A85609",
        "active": True,
        "address": {},
        "name": "TEST PRACTICE",
        "type": "UNKNOWN",
        "phone_number": "",
        "last_updated": "2020-01-23T13:48:08.123456+00:00",
        "gp_practice_status": False,
        "operational_periods": []
    },
    "address": None,
    "participant_id": "PARTICIPANT1",
    "active": True,
    "reason_for_removal_code": None,
    "reason_for_removal_effective_from_date": None,
    "is_ceased": None,
    "serial_change_number": None,
    "superseded_nhs_number": None,
    "address_effective_from_date": None,
    "invited_date": None,
    "is_paused": None,
    "is_paused_received_time": None
}

participant_2_records = [
    {
        'sort_key': 'PARTICIPANT',
        'participant_id': 'PARTICIPANT2',
        'title': 'MR',
        'first_name': 'CHARLES',
        'middle_names': 'E',
        'last_name': 'WOOF',
        'nhs_number': '9999999998',
        'date_of_birth': '1998-01-22',
        'status': ParticipantStatus.ROUTINE
    },
    {
        'sort_key': 'RESULT#1',
        'participant_id': 'PARTICIPANT2',
        'test_date': '2019-01-20',
        'action': 'ACTION',
        'action_code': 'ACTION CODE',
        'result': 'RESULT',
        'result_code': 'RESULT CODE',
        'infection_result': 'INFECTION RESULT',
        'infection_code': 'INFECTION CODE'
    }
]

expected_participant_2_object = {
    "nhs_number": "9999999998",
    "date_of_birth": "1998-01-22",
    "gender": None,
    "title": "MR",
    "first_name": "CHARLES",
    "middle_names": "E",
    "last_name": "WOOF",
    "next_test_due_date": None,
    "date_of_death": None,
    "is_invalid_patient": None,
    "status": "ROUTINE",
    "is_fp69": None,
    "event_text": None,
    "test_results": [
        {
            "sort_key": "RESULT#1",
            "title": None,
            "first_name": None,
            "last_name": None,
            "date_of_birth": None,
            "gender": None,
            "health_authority": None,
            "sending_lab": None,
            "source_code": None,
            "sender_code": None,
            "sender_source_type": None,
            "slide_number": None,
            "test_date": "2019-01-20",
            "infection_result": "INFECTION RESULT",
            "infection_code": "INFECTION CODE",
            "result": "RESULT",
            "result_code": "RESULT CODE",
            "action": "ACTION",
            "action_code": "ACTION CODE",
            "hpv_primary": None,
            "self_sample": None,
            "recall_months": None,
            "crm": [],
            "notification_status": None,
            "nhais_cipher": None
        }
    ],
    "registered_gp_practice": {
        "organisation_code": "A85609",
        "active": True,
        "address": {},
        "name": "TEST PRACTICE",
        "type": "UNKNOWN",
        "phone_number": "",
        "last_updated": "2020-01-23T13:48:08.123456+00:00",
        "gp_practice_status": False,
        "operational_periods": []
    },
    "address": None,
    "participant_id": "PARTICIPANT2",
    "active": True,
    "reason_for_removal_code": None,
    "reason_for_removal_effective_from_date": None,
    "is_ceased": None,
    "serial_change_number": None,
    "superseded_nhs_number": None,
    "address_effective_from_date": None,
    "invited_date": None,
    "is_paused": None,
    "is_paused_received_time": None
}

participant_3_records = [
    {
        'sort_key': 'PARTICIPANT',
        'participant_id': 'PARTICIPANT3',
        'title': 'MS',
        'first_name': 'PUPPY',
        'middle_names': 'T',
        'last_name': 'DOGG',
        'nhs_number': '9999999997',
        'nhais_cipher': 'MAN',
        'date_of_birth': '1989-03-11',
        'status': ParticipantStatus.INADEQUATE
    }
]
expected_participant_3_object = {
    "nhs_number": "9999999997",
    "date_of_birth": "1989-03-11",
    "gender": None,
    "title": "MS",
    "first_name": "PUPPY",
    "middle_names": "T",
    "last_name": "DOGG",
    "next_test_due_date": None,
    "date_of_death": None,
    "is_invalid_patient": None,
    "status": "INADEQUATE",
    "is_fp69": None,
    "event_text": None,
    "test_results": [],
    "registered_gp_practice": {
        "organisation_code": "A85609",
        "active": True,
        "address": {},
        "name": "TEST PRACTICE",
        "type": "UNKNOWN",
        "phone_number": "",
        "last_updated": "2020-01-23T13:48:08.123456+00:00",
        "gp_practice_status": False,
        "operational_periods": []
    },
    "address": None,
    "participant_id": "PARTICIPANT3",
    "active": True,
    "reason_for_removal_code": None,
    "reason_for_removal_effective_from_date": None,
    "is_ceased": None,
    "serial_change_number": None,
    "superseded_nhs_number": None,
    "address_effective_from_date": None,
    "invited_date": None,
    "is_paused": None,
    "is_paused_received_time": None
}

organisation_data = {
    'data':
        {'active': True,
         'address': {'address_line_1': '108 RAWLING ROAD',
                     'address_line_2': 'BENSHAM',
                     'address_line_3': 'GATESHEAD',
                     'address_line_4': 'TYNE AND WEAR',
                     'address_line_5': 'ENGLAND',
                     'postcode': 'NE8 4QR'},
         'gp_practice_status': True,
         'last_updated': '2021-02-05T08:15:48.123456',
         'name': 'TEST PRACTICE',
         'operational_periods': [{'end': '2021-06-30', 'start': '1974-04-01'}],
         'organisation_code': 'A85609',
         'phone_number': '0191 4203255',
         'type': 'UNKNOWN'
         }}


def get_log_call_object(log_reference, log_message):
    call_string = '{"log_reference": "' + log_reference + '", "message": "' + log_message + '"}'
    return call(20, call_string, exc_info=False)


expected_log_calls = [
    get_log_call_object('LAMBDA0000', 'Lambda started'),
    call(20, '{"log_reference": "LAMBDA0007", "assigned_workgroups": [], "message": "Setting user workgroups from session"}', exc_info=False), 
    get_log_call_object('PNLGP0001', 'Received request to get saved episode record'),
    call(20, '{"log_reference": "PNLGP0006", "gp id": "DXX", "message": "Using gp id"}', exc_info=False),
    get_log_call_object('PNLGP0007', 'Adding last viewed by record.'),
    get_log_call_object('PNLGP0002', 'Successfully obtained episode records'),
    get_log_call_object('PNLGP0003', 'Successfully obtained participants related to episode records'),
    call(20, '{"log_reference": "ODS0002", "organisation_code": "DXX", "message": "Returning cached organisation data."}', exc_info=False), 
    get_log_call_object('PNLGP0004', 'Successfully mapped participants and results to api response objects'),
    get_log_call_object('AUDIT0001', 'Successfully sent an audit record to the audit queue'),
    get_log_call_object('LAMBDA0001', 'Lambda exited with status success')
]
