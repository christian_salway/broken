This lambda will retrieve all participants and their results for the organisation code in the requestee session

## Response
* Scenario: List retrieved successfully

    Status code: 200 OK 

* Scenario: No organisation code or no episode records associated with a given code

    Status code: 400