import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    PNLGP0001 = (logging.INFO, 'Received request to get saved episode record')
    PNLGP0002 = (logging.INFO, 'Successfully obtained episode records')
    PNLGP0003 = (logging.INFO, 'Successfully obtained participants related to episode records')
    PNLGP0004 = (logging.INFO, 'Successfully mapped participants and results to api response objects')
    PNLGP0005 = (logging.INFO, 'No episodes found for gp id')
    PNLGP0006 = (logging.INFO, 'Using gp id')
    PNLGP0007 = (logging.INFO, 'Adding last viewed by record.')

    PNLGPEX0001 = (logging.ERROR, 'Unable to find organisation code in event or session data')
    PNLGPEX0002 = (logging.ERROR, 'Found an episode without a corresponding participant')
    PNLGPEX0003 = (logging.ERROR, 'Failed to add last viewed by record')
