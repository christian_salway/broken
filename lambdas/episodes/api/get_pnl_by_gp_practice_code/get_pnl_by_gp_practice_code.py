from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_object, json_return_message
from common.utils.audit_utils import audit, AuditActions
from get_pnl_by_gp_practice_code.log_references import LogReference
from common.log import log
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.participant_episode_utils import \
    query_for_participant_by_gp_id_and_episode_live_record_status, EpisodeStatus
from common.utils.participant_utils import (get_participant_and_test_results,
                                            convert_dynamodb_entity_to_participant_response)
from common.utils.organisation_directory_utils \
    import get_organisation_information_by_organisation_code
from common.models.participant import ParticipantStatus
from common.utils.organisation_supplementary_utils import add_last_viewed_by_record

PARTICIPANTS_SORT_KEY = 'PARTICIPANT'
TEST_SORT_KEY = 'RESULT'
UNKNOWN_STATUS_ORDER = 5
NO_EPISODE_RECORD_JSON = {
    'data': list(),
    'links': {
        'self': '',
    },
}
STATUS_ORDER = {
    ParticipantStatus.SUSPENDED: 0,
    ParticipantStatus.REPEAT_ADVISED: 1,
    ParticipantStatus.INADEQUATE: 2,
    ParticipantStatus.ROUTINE: 3,
    ParticipantStatus.CALLED: 4,
    None: UNKNOWN_STATUS_ORDER
}


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):

    log({'log_reference': LogReference.PNLGP0001})

    event_data = get_api_request_info_from_event(event)[0]
    session = event_data['session']
    session_user = session.get('user_data')
    first_name = session_user.get('first_name')
    last_name = session_user.get('last_name')

    role_id = session.get('selected_role', {}).get('role_id')
    org = session.get('selected_role', {}).get('organisation_code')
    isCsas = role_id == 'R8010' and org == 'DXX'

    gp_id = event_data.get('query_parameters', {}).get('gp_practice_code')
    if not gp_id:
        gp_id = session.get('selected_role', {}).get('organisation_code') if session else None

    if gp_id:
        log({'log_reference': LogReference.PNLGP0006, 'gp id': gp_id})
    else:
        log({'log_reference': LogReference.PNLGPEX0001})
        return json_return_message(400, 'Organisation code not supplied')

    if first_name is None or last_name is None:
        return json_return_message(400, 'Cannot find first_name or last_name')

    if isCsas:
        try:
            log({'log_reference': LogReference.PNLGP0007})
            print_type = 'PRINT_PNL'
            add_last_viewed_by_record(first_name, last_name, gp_id, print_type)
        except Exception as e:
            log({'log_reference': LogReference.PNLGPEX0003, 'error': str(e)})
            return json_return_message(500, 'Failed to add last viewed by record')

    episode_records = query_for_participant_by_gp_id_and_episode_live_record_status(gp_id, EpisodeStatus.PNL.value)
    if not episode_records:
        log({'log_reference': LogReference.PNLGP0005})
        return json_return_object(200, NO_EPISODE_RECORD_JSON)

    log({'log_reference': LogReference.PNLGP0002})

    try:
        pnl_participants = get_participants_from_episodes(episode_records, gp_id)
    except Exception as e:
        return json_return_message(500, str(e))

    participant_list = {
        'data': pnl_participants,
        'links': {
            'self': '',
        },
    }

    participant_ids = [participant['participant_id'] for participant in pnl_participants]
    nhs_numbers = [participant['nhs_number'] for participant in pnl_participants]
    audit(
        session=session,
        action=AuditActions.GET_PNL_BY_GP_PRACTICE_CODE,
        participant_ids=participant_ids,
        nhs_numbers=nhs_numbers,
        gp_practice_code=gp_id
    )

    return json_return_object(200, participant_list)


def get_participants_from_episodes(episode_list, gp_id):

    participant_and_results_lists = map(
        lambda episode: get_participant_and_test_results(episode['participant_id'], segregate=True),
        episode_list
    )

    log({'log_reference': LogReference.PNLGP0003})

    registered_gp_practice_details = get_organisation_information_by_organisation_code(gp_id)

    participant_objects = [
        map_participant_and_results_to_participant_response(records, registered_gp_practice_details)
        for records in participant_and_results_lists
    ]

    log({'log_reference': LogReference.PNLGP0004})

    participant_objects.sort(
        key=lambda participant: (
            participant.get('next_test_due_date'),
            STATUS_ORDER.get(participant.get('status').upper() if participant.get('status') else '',
                             UNKNOWN_STATUS_ORDER)
        )
    )

    return participant_objects


def map_participant_and_results_to_participant_response(participant_and_results, registered_gp_practice_details):
    test_results = [record for record in participant_and_results if record['sort_key'].startswith(TEST_SORT_KEY)]

    participant = next(
        (record for record in participant_and_results if record['sort_key'] == PARTICIPANTS_SORT_KEY), None
    )

    if not participant:
        log({'log_reference': LogReference.PNLGPEX0002})
        raise Exception(LogReference.PNLGPEX0002.message)

    return convert_dynamodb_entity_to_participant_response(
        participant, test_results, registered_gp_practice_details
    )
