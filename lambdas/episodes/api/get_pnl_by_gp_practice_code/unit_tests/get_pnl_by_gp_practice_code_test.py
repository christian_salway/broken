from unittest.case import TestCase
from mock import patch, call, Mock
import json
from get_pnl_by_gp_practice_code.log_references import LogReference
from common.utils.audit_utils import AuditActions
from common.models.participant import EpisodeStatus
with patch('boto3.resource') as resource_mock:
    from get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code import \
        lambda_handler, map_participant_and_results_to_participant_response, \
        get_participants_from_episodes


@patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.get_api_request_info_from_event')
@patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.log')
class TestGetPNLByGPPracticeCode(TestCase):

    def setUp(self):
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    def test_lambda_uses_gp_id_if_supplied_in_event(self,
                                                    query_by_gp_id_mock,
                                                    log_mock,
                                                    get_api_request_mock):
        gp_practice_code_in_query_data = '1234'
        gp_practice_code_in_session_data = '5678'
        lambda_event = {'pathParameters': {}}
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'gp_practice_code': gp_practice_code_in_query_data},
                'body': '',
                'session': {'selected_role': {'organisation_code': gp_practice_code_in_session_data},
                            'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')

        query_by_gp_id_mock.return_value = []

        self.unwrapped_handler(lambda_event, {})

        query_by_gp_id_mock.assert_called_with(gp_practice_code_in_query_data, EpisodeStatus.PNL.value)

        expected_log_calls = [
            call({'log_reference': LogReference.PNLGP0001}),
            call({'log_reference': LogReference.PNLGP0006, 'gp id': gp_practice_code_in_query_data}),
            call({'log_reference': LogReference.PNLGP0005})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    def test_lambda_uses_gp_id_from_session_if_not_supplied_in_event(self,
                                                                     query_by_gp_id_mock,
                                                                     log_mock,
                                                                     get_api_request_mock):
        gp_practice_code_in_session_data = '5678'
        lambda_event = {'pathParameters': {}}
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {'selected_role': {'organisation_code': gp_practice_code_in_session_data},
                            'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')

        query_by_gp_id_mock.return_value = []

        self.unwrapped_handler(lambda_event, {})

        query_by_gp_id_mock.assert_called_with(gp_practice_code_in_session_data, EpisodeStatus.PNL.value)

        expected_log_calls = [
            call({'log_reference': LogReference.PNLGP0001}),
            call({'log_reference': LogReference.PNLGP0006, 'gp id': gp_practice_code_in_session_data}),
            call({'log_reference': LogReference.PNLGP0005})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    def test_lambda_returns_bad_request_given_no_organisation_code(self,
                                                                   log_mock,
                                                                   get_api_request_mock):
        lambda_event = {'pathParameters': {}}
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {'session_id': 'the_session',
                            'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')

        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Organisation code not supplied"}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(expected_response, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.PNLGP0001}),
            call({'log_reference': LogReference.PNLGPEX0001})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    def test_lambda_returns_empty_list_when_no_episodes_through_gp_id(self,
                                                                      query_by_gp_id_mock,
                                                                      log_mock,
                                                                      get_api_request_mock):

        lambda_event = {}
        gp_practice_code_in_query_data = '1234'
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'gp_practice_code': gp_practice_code_in_query_data},
                'body': '',
                'session': {'session_id': 'the_session',
                            'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': json.dumps({
                'data': [],
                'links': {
                    'self': '',
                },
            }),
            'isBase64Encoded': False
        }
        query_by_gp_id_mock.return_value = []

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(expected_response, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.PNLGP0001}),
            call({'log_reference': LogReference.PNLGP0006, 'gp id': gp_practice_code_in_query_data}),
            call({'log_reference': LogReference.PNLGP0005})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.audit')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.get_participants_from_episodes')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    def test_lambda_returns_sorted_list_on_correct_gp_id(self,
                                                         query_by_gp_id_mock,
                                                         get_participants_from_episodes_mock,
                                                         add_record_mock,
                                                         audit_mock,
                                                         log_mock,
                                                         get_api_request_mock):
        test_session = {'selected_role': {'organisation_code': '1234'},
                        'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
        lambda_event = {}
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': test_session
            },
            '',
            '')

        test_participants = [{'participant_id': 'test1', 'nhs_number': 'number1'},
                             {'participant_id': 'test2', 'nhs_number': 'number2'}]
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': json.dumps({
                'data': test_participants,
                'links': {
                    'self': '',
                },
            }),
            'isBase64Encoded': False
        }

        get_participants_from_episodes_mock.return_value = test_participants

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(expected_response, actual_response)
        add_record_mock.assert_not_called()
        audit_mock.assert_called_once_with(
            session=test_session,
            action=AuditActions.GET_PNL_BY_GP_PRACTICE_CODE,
            participant_ids=['test1', 'test2'],
            nhs_numbers=['number1', 'number2'],
            gp_practice_code='1234'
        )

    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.get_participants_from_episodes')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    def test_lambda_returns_error_if_no_participant_error_caught(self,
                                                                 query_by_gp_id_mock,
                                                                 get_participants_from_episodes_mock,
                                                                 log_mock,
                                                                 get_api_request_mock):
        lambda_event = {}
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'gp_practice_code': '1234'},
                'body': '',
                'session': {'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')

        expected_response = {
            'statusCode': 500,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Found an episode without a corresponding participant"}',
            'isBase64Encoded': False
        }

        get_participants_from_episodes_mock.side_effect = Exception(LogReference.PNLGPEX0002.message)

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(expected_response, actual_response)

    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.audit')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.get_participants_from_episodes')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    def test_lambda_returns_error_if_no_first_name_last_name(self,
                                                             query_by_gp_id_mock,
                                                             get_participants_from_episodes_mock,
                                                             add_record_mock,
                                                             audit_mock,
                                                             log_mock,
                                                             get_api_request_mock):
        lambda_event = {}
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'gp_practice_code': '1234'},
                'body': '',
                'session': {'user_data': {}}
            },
            '',
            '')

        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Cannot find first_name or last_name"}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler(lambda_event, {})

        add_record_mock.assert_not_called()
        audit_mock.assert_not_called()

        self.assertDictEqual(expected_response, actual_response)

    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.audit')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.get_participants_from_episodes')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    def test_lambda_returns_calls_add_last_viewed_by_if_user_is_csas(self,
                                                                     query_by_gp_id_mock,
                                                                     get_participants_from_episodes_mock,
                                                                     add_record_mock,
                                                                     audit_mock,
                                                                     log_mock,
                                                                     get_api_request_mock):

        test_participants = [{'participant_id': 'test1', 'nhs_number': 'number1'},
                             {'participant_id': 'test2', 'nhs_number': 'number2'}]

        test_session = {'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                        'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
        lambda_event = {}
        add_record_mock.return_value = []
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'gp_practice_code': '1234'},
                'body': '',
                'session': test_session
            },
            '',
            '')

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": [{"participant_id": "test1", "nhs_number": "number1"}, '
            '{"participant_id": "test2", "nhs_number": "number2"}], "links": '
            '{"self": ""}}',
            'isBase64Encoded': False
        }

        get_participants_from_episodes_mock.return_value = test_participants

        actual_response = self.unwrapped_handler(lambda_event, {})

        audit_mock.assert_called_once_with(
            session=test_session,
            action=AuditActions.GET_PNL_BY_GP_PRACTICE_CODE,
            participant_ids=['test1', 'test2'],
            nhs_numbers=['number1', 'number2'],
            gp_practice_code='1234'
        )

        add_record_mock.assert_called_once_with('Bob', 'Smith', '1234', 'PRINT_PNL')

        self.assertDictEqual(expected_response, actual_response)

    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.audit')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.get_participants_from_episodes')
    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    def test_lambda_returns_calls_does_not_add_last_viewed_by_if_user_is_not_csas(self,
                                                                                  query_by_gp_id_mock,
                                                                                  get_participants_from_episodes_mock,
                                                                                  add_record_mock,
                                                                                  audit_mock,
                                                                                  log_mock,
                                                                                  get_api_request_mock):

        test_participants = [{'participant_id': 'test1', 'nhs_number': 'number1'},
                             {'participant_id': 'test2', 'nhs_number': 'number2'}]

        test_session = {'selected_role': {'organisation_code': 'DXX', 'role_id': 'NOTCSAS'},
                        'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
        lambda_event = {}

        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'gp_practice_code': '1234'},
                'body': '',
                'session': test_session
            },
            '',
            '')

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": [{"participant_id": "test1", "nhs_number": "number1"}, '
            '{"participant_id": "test2", "nhs_number": "number2"}], "links": '
            '{"self": ""}}',
            'isBase64Encoded': False
        }

        get_participants_from_episodes_mock.return_value = test_participants

        actual_response = self.unwrapped_handler(lambda_event, {})

        audit_mock.assert_called_once_with(
            session=test_session,
            action=AuditActions.GET_PNL_BY_GP_PRACTICE_CODE,
            participant_ids=['test1', 'test2'],
            nhs_numbers=['number1', 'number2'],
            gp_practice_code='1234'
        )

        add_record_mock.assert_not_called()

        self.assertDictEqual(expected_response, actual_response)


@patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.get_organisation_information_by_organisation_code',
       Mock())
@patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.log')
@patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.map_participant_and_results_to_participant_response')
@patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.get_participant_and_test_results')
class TestGetParticipantsFromEpisodes(TestCase):

    def setUp(self):
        self.episode_list = [{'participant_id': 'test'}]*3
        self.get_participants_response = {'Items': 'test'}

    def test_get_participants_can_sort_different_ntdd(self,
                                                      get_participants_mock,
                                                      map_participants_mock,
                                                      log_mock):
        test_participants = [
            {'participant_id': 'test1', 'next_test_due_date': '2025-10-09'},
            {'participant_id': 'test1', 'next_test_due_date': '2026-11-11'},
            {'participant_id': 'test2', 'next_test_due_date': '2023-10-09'}
        ]
        map_participants_mock.side_effect = test_participants
        get_participants_mock.return_value = self.get_participants_response

        expected_participants = [test_participants[2], test_participants[0], test_participants[1]]
        actual_participants = get_participants_from_episodes(self.episode_list, '')

        self.assertEqual(expected_participants, actual_participants)

        expected_log_calls = [
            call({'log_reference': LogReference.PNLGP0003}),
            call({'log_reference': LogReference.PNLGP0004})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    def test_get_participants_can_sort_different_status(self, get_participants_mock, map_participants_mock, log_mock):
        test_participants = [
            {'participant_id': 'test1', 'status': 'REPEAT_ADVISED'},
            {'participant_id': 'test2', 'status': ''},
            {'participant_id': 'test3', 'status': EpisodeStatus.SUSPENDED.value}
        ]
        map_participants_mock.side_effect = test_participants
        get_participants_mock.return_value = self.get_participants_response

        expected_participants = [test_participants[2], test_participants[0], test_participants[1]]
        actual_participants = get_participants_from_episodes(self.episode_list, '')

        self.assertEqual(expected_participants, actual_participants)

        expected_log_calls = [
            call({'log_reference': LogReference.PNLGP0003}),
            call({'log_reference': LogReference.PNLGP0004})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    def test_get_participants_can_sort_by_ntdd_and_status(self, get_participants_mock, map_participants_mock, log_mock):
        test_participants = [
            {'participant_id': 'test1', 'next_test_due_date': '2025-10-09', 'status': 'REPEAT_ADVISED'},
            {'participant_id': 'test2', 'next_test_due_date': '2023-01-01', 'status': ''},
            {'participant_id': 'test3', 'next_test_due_date': '2025-10-09', 'status': EpisodeStatus.SUSPENDED.value}
        ]
        map_participants_mock.side_effect = test_participants
        get_participants_mock.return_value = self.get_participants_response

        expected_participants = [test_participants[1], test_participants[2], test_participants[0]]
        actual_participants = get_participants_from_episodes(self.episode_list, '')

        self.assertEqual(expected_participants, actual_participants)

        expected_log_calls = [
            call({'log_reference': LogReference.PNLGP0003}),
            call({'log_reference': LogReference.PNLGP0004})
        ]

        log_mock.assert_has_calls(expected_log_calls)


@patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.log')
class TestMapParticipantAndResultsToParticipantResponse(TestCase):

    @patch('get_pnl_by_gp_practice_code.get_pnl_by_gp_practice_code.convert_dynamodb_entity_to_participant_response')
    def test_can_parse_participant_and_results(self, convert_dynamodb_entity_to_participant_response_mock, log_mock):
        registered_gp_details = []
        participant_record = {
            "sort_key": 'PARTICIPANT'
        }
        result_record = {
            "sort_key": 'RESULT#123'
        }
        participant_and_result_object = [participant_record, result_record, result_record]

        map_participant_and_results_to_participant_response(participant_and_result_object, registered_gp_details)

        convert_dynamodb_entity_to_participant_response_mock.assert_called_with(
            participant_record, [result_record, result_record], registered_gp_details
        )

    def test_can_throw_custom_error_if_no_participant(self, log_mock):
        registered_gp_details = []
        participant_record = {
            "sort_key": 'EXCEPTION'
        }
        result_record = {
            "sort_key": 'RESULT#123'
        }

        participant_and_result_object = [participant_record, result_record, result_record]

        self.assertRaises(
            Exception,
            map_participant_and_results_to_participant_response,
            participant_and_result_object,
            registered_gp_details
        )
