This lambda is to sit behind the screening_api API Gateway and serve responses for the front end.
It has a GET request that reads GP-NOTIFICATION records from the participant table for a specified GP Practice,
and a PUT request that updates the status for a given (by participant_id and sort_key) GP-NOTIFICATION RECORD from NEW_NOTIFICATION [?] to REVIEWED_NOTIFICATION

## Request
```  PUT /gp-notifications/{participant_id}/{sort_key}/reviewed ```

#### Required Params
``` participant_id=string ```
``` sort_key=string ```

## Response
* Scenario: Record updated successfully

    Status code: 200 OK 

    Body: 
    ```
    {
      data: {
        'message': 'gp-notification record updated'
      }
    }
    ```

* Scenario: Record not found

    Status code: 404 NOT FOUND 

    Body: 
    ```
    {
      data: {
        'message': 'gp notification record not found'
      }
    }
    ```

* Scenario: Record's status is not NEW_NOTIFICATION

    Status code: 409 CONFLICT

    Body:
    ```
    {
      data: {
        'message': 'gp notification record does not have status NEW_NOTIFICATION'
      }
    }
    ```
     
## Request
```  GET /gp-notifications?gp_practice_code=XXX ```

#### Required Params
``` gp_practice_code=string ```

## Response
* Scenario: Results found

    Status code: 200 OK 

    Body: 
    ```
    {
      data: [
                {
                  "participant_id": "UUID",
                  "sort_key": "GP-NOTIFICATION#ABC1234#2020-01-23T13:48:08.000",
                  "created": "2020-01-23T13:48:08.000",
                  "expires": "2020-02-23T13:48:08.000",
                  "registered_gp_practice_code": "ABC1234",
                  "user_id": "XXXXXX",
                  "status": "NEW_NOTIFICATION",
                  "live_record_status: "NEW_NOTIFICATION"
                }
          ]
    }
    ```

* Scenario: No results found

    Status code: 200 OK 

    Body: 
     ``` {
       data: []
      } ```
