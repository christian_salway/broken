from common.log import log
from manage_gp_notifications.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils import json_return_data, json_return_message
from common.utils.api_utils import get_api_request_info_from_event
from manage_gp_notifications.manage_gp_notifications_service import get_gp_notifications, review_gp_notification
from manage_gp_notifications.exceptions import RequestResourceException, \
    MissingPracticeCodeException, SortKeyFormattingException, MissingNotificationException, BadStatusException


@lambda_entry_point
def lambda_handler(event, context):
    BASE_URL = '/api/gp-notifications'
    REQUEST_FUNCTIONS = {
        ('GET', BASE_URL): get_gp_notifications,
        ('PUT', BASE_URL + '/{participant_id}/{sort_key}/reviewed'): review_gp_notification,
    }
    event_data, http_method, resource = get_api_request_info_from_event(event)
    log({'log_reference': LogReference.GPNOTS0001})

    try:
        response = REQUEST_FUNCTIONS.get((http_method, resource), bad_request)(event_data)
    except RequestResourceException as e:
        log({'log_reference': LogReference.GPNOTS0003, 'error': e.message})
        return json_return_message(400, e.message)
    except MissingPracticeCodeException as e:
        log({'log_reference': LogReference.GPNOTS0003, 'error': e.message})
        return json_return_message(400, e.message)
    except SortKeyFormattingException as e:
        log({'log_reference': LogReference.GPNOTS0003, 'error': e.message})
        return json_return_message(400, 'sort_key has incorrect format')
    except MissingNotificationException as e:
        log({'log_reference': LogReference.GPNOTS0003, 'error': e.message})
        return json_return_message(404, 'gp notification record not found')
    except BadStatusException as e:
        log({'log_reference': LogReference.GPNOTS0003, 'error': e.message})
        return json_return_message(409, 'gp notification record does not have status NEW_NOTIFICATION')
    except Exception as e:
        log({'log_reference': LogReference.GPNOTS0003, 'error': str(e)})
        return json_return_message(500, 'Cannot complete request')

    log({'log_reference': LogReference.GPNOTS0002})
    return json_return_data(200, response)


def bad_request(event_data):
    log({'log_reference': LogReference.GPNOTS0004})
    raise RequestResourceException()
