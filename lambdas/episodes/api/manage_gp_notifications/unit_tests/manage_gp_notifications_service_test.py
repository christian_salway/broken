from unittest import TestCase
from mock import patch, call
from boto3.dynamodb.conditions import Key
from common.utils.audit_utils import AuditActions
from common.models.participant import EpisodeStatus
from manage_gp_notifications.log_references import LogReference
from manage_gp_notifications.exceptions import \
    MissingPracticeCodeException, SortKeyFormattingException, MissingNotificationException, BadStatusException
from common.utils.dynamodb_access.table_names import TableNames

MOCK_ENV_VARS = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE',
}

with patch('os.environ', MOCK_ENV_VARS):
    import manage_gp_notifications.manage_gp_notifications_service as service_module


module_path = 'manage_gp_notifications.manage_gp_notifications_service'
EXAMPLE_UUID = '1234-asdf-efecxc-2343-7'
PROJECTION_EXPRESSION = ', '.join([
    'organisation_code',
    'type_value',
    'organisation_supplemental_id'
])


@patch(f'{module_path}.log')
class TestManageGPNotificationEmails(TestCase):
    # Review records
    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.segregated_dynamodb_update_item')
    @patch(f'{module_path}.segregated_dynamodb_get_item')
    @patch(f'{module_path}.build_update_query')
    @patch(f'{module_path}.audit')
    @patch(f'{module_path}.get_nhs_number_by_participant_id')
    def test_review_calls_through(self, get_nhs_number_by_participant_id_mock,
                                  audit_mock, build_query_mock, get_item_mock, update_item_mock,
                                  add_record_mock, log_mock):
        # Given
        participant_id = 'abcde-12345'
        sort_key = 'GP-NOTIFICATION#X09#2020-01-01T09:21:30'
        get_item_mock.return_value = {
            'participant_id': participant_id,
            'sort_key': sort_key,
            'live_record_status': 'NEW_NOTIFICATION',
            'nhs_number': 'nhs_number'
        }
        build_query_mock.return_value = ('a', 'b', 'c')
        get_nhs_number_by_participant_id_mock.return_value = 'nhs_number'

        # When
        event_data = {
            'path_parameters': {
                'participant_id': participant_id,
                'sort_key': sort_key
            },
            'session': {
                'user_data': {
                    'nhsid_useruid': 'wah'
                }
            }
        }
        service_module.review_gp_notification(event_data)

        # Then
        key = {
            'participant_id': participant_id,
            'sort_key': sort_key
        }
        get_item_mock.assert_called_with(TableNames.PARTICIPANTS, key)
        condition_expression = Key('sort_key').eq(sort_key) & Key('participant_id').eq(participant_id)
        update_item_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            Key=key,
            UpdateExpression='a',
            ExpressionAttributeValues='c',
            ConditionExpression=condition_expression,
            ExpressionAttributeNames='b',
            ReturnValues='NONE'
        ))
        get_nhs_number_by_participant_id_mock.assert_called_with(participant_id, segregate=True)
        add_record_mock.assert_not_called()
        audit_mock.assert_called_with(
            AuditActions.REVIEW_GP_NOTIFICATION,
            session={'user_data': {'nhsid_useruid': 'wah'}},
            participant_ids=[participant_id],
            nhs_numbers=['nhs_number']
        )

        expected_log_calls = [call({'log_reference': LogReference.REVGPNOTS0001}),
                              call({'log_reference': LogReference.REVGPNOTS0002}),
                              call({'log_reference': LogReference.REVGPNOTS0004})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.build_update_query')
    @patch(f'{module_path}.audit')
    @patch(f'{module_path}.get_nhs_number_by_participant_id')
    def test_review_bad_sort_key(self, get_nhs_number_by_participant_id_mock,
                                 audit_mock, build_query_mock, add_record_mock, log_mock):
        participant_id = '1234'
        sort_key = 'spujb'
        event_data = {
            'path_parameters': {
                'participant_id': participant_id,
                'sort_key': sort_key
            },
        }

        with self.assertRaises(SortKeyFormattingException):
            service_module.review_gp_notification(event_data)

        audit_mock.assert_not_called()
        add_record_mock.assert_not_called()
        build_query_mock.assert_not_called()
        get_nhs_number_by_participant_id_mock.assert_not_called()
        log_mock.assert_called_with(
            {'log_reference': LogReference.REVGPNOTS0001}
        )

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.segregated_dynamodb_update_item')
    @patch(f'{module_path}.segregated_dynamodb_get_item')
    @patch(f'{module_path}.build_update_query')
    @patch(f'{module_path}.audit')
    @patch(f'{module_path}.get_nhs_number_by_participant_id')
    def test_review_fails_appropriately_for_no_notification(
            self, get_nhs_number_by_participant_id_mock, audit_mock, build_query_mock,
            get_item_mock, update_item_mock, add_record_mock, log_mock):
        # Given
        participant_id = 'abcde-12345'
        sort_key = 'GP-NOTIFICATION#X09#2020-01-01T09:21:30+00:00'
        get_item_mock.return_value = {}
        build_query_mock.return_value = ('a', 'b', 'c')

        # When
        event_data = {
            'path_parameters': {
                'participant_id': participant_id,
                'sort_key': sort_key
            },
            'session': {
                'user_data': {
                    'nhsid_useruid': 'wah'
                }
            }
        }
        with self.assertRaises(MissingNotificationException):
            service_module.review_gp_notification(event_data)

        # Then
        key = {
            'participant_id': participant_id,
            'sort_key': sort_key
        }
        get_item_mock.assert_called_with(TableNames.PARTICIPANTS, key)
        update_item_mock.assert_not_called()
        audit_mock.assert_not_called()
        add_record_mock.assert_not_called()
        get_nhs_number_by_participant_id_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.REVGPNOTS0001}),
                              call({'log_reference': LogReference.REVGPNOTS0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.segregated_dynamodb_update_item')
    @patch(f'{module_path}.segregated_dynamodb_get_item')
    @patch(f'{module_path}.build_update_query')
    @patch(f'{module_path}.audit')
    @patch(f'{module_path}.get_nhs_number_by_participant_id')
    def test_review_fails_appropriately_for_bad_status(
            self, get_nhs_number_by_participant_id_mock, audit_mock, build_query_mock,
            get_item_mock, update_item_mock, add_record_mock, log_mock):
        # Given
        participant_id = 'abcde-12345'
        sort_key = 'GP-NOTIFICATION#X09#2020-01-01T09:21:30+00:00'
        get_item_mock.return_value = {
            'participant_id': participant_id,
            'sort_key': sort_key,
            'live_record_status': 'REVIEWED_NOTIFICATION'
        }
        build_query_mock.return_value = ('a', 'b', 'c')

        # When
        event_data = {
            'path_parameters': {
                'participant_id': participant_id,
                'sort_key': sort_key
            },
            'session': {
                'user_data': {
                    'nhsid_useruid': 'wah'
                }
            }
        }
        with self.assertRaises(BadStatusException):
            service_module.review_gp_notification(event_data)

        # Then
        key = {
            'participant_id': participant_id,
            'sort_key': sort_key
        }
        get_item_mock.assert_called_with(TableNames.PARTICIPANTS, key)
        update_item_mock.assert_not_called()
        audit_mock.assert_not_called()
        add_record_mock.assert_not_called()
        get_nhs_number_by_participant_id_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.REVGPNOTS0001}),
                              call({'log_reference': LogReference.REVGPNOTS0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.segregated_dynamodb_update_item')
    @patch(f'{module_path}.segregated_dynamodb_get_item')
    @patch(f'{module_path}.audit')
    @patch(f'{module_path}.get_nhs_number_by_participant_id')
    def test_review_fails_appropriately_for_get_item_error(
            self, get_nhs_number_by_participant_id_mock, audit_mock, get_item_mock,
            update_item_mock, add_record_mock, log_mock):
        # Given
        participant_id = 'abcde-12345'
        sort_key = 'GP-NOTIFICATION#X09#2020-01-01T09:21:30'
        get_item_mock.side_effect = Exception('test')

        # When
        event_data = {
            'path_parameters': {
                'participant_id': participant_id,
                'sort_key': sort_key
            },
            'session': {
                'user_data': {
                    'nhsid_useruid': 'wah'
                }
            }
        }
        with self.assertRaises(Exception):
            service_module.review_gp_notification(event_data)

        # Then
        get_item_mock.assert_called_once_with(TableNames.PARTICIPANTS, event_data['path_parameters'])
        update_item_mock.assert_not_called()
        audit_mock.assert_not_called()
        add_record_mock.assert_not_called()
        get_nhs_number_by_participant_id_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.REVGPNOTS0001}),
                              call({'log_reference': LogReference.REVGPNOTS0003})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.audit')
    @patch(f'{module_path}.get_nhs_number_by_participant_id')
    def test_get_fails_appropriately_for_no_gp_practice(self, get_nhs_number_by_participant_id_mock,
                                                        audit_mock, add_record_mock, log_mock):
        # When
        event_data = {
            'session': {'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
        }

        # Then
        with self.assertRaises(MissingPracticeCodeException):
            service_module.get_gp_notifications(event_data)

        audit_mock.assert_not_called()
        add_record_mock.assert_not_called()
        get_nhs_number_by_participant_id_mock.assert_not_called()
        log_mock.assert_not_called()

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.query_for_participant_by_gp_id_and_gp_notification_live_record_status')
    @patch(f'{module_path}.get_participant_and_test_results')
    @patch(f'{module_path}.audit')
    def test_read_calls_through_single_notification_single_participant_with_test(
        self, audit_mock, participant_results_mock,
        query_notifications_mock, add_record_mock, log_mock
    ):

        self.maxDiff = None
        # Given
        participant_id = 'qwerty1'
        nhs_number = 'nhs_number'
        notification_record = {
            "sort_key": "GP-NOTIFICATION#2019-04-23T00:00:00.000+00:00",
            "participant_id": participant_id,
            "user_id": "1234567890",
            "live_record_status": "NEW_NOTIFICATION",
            "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
            "created": "2019-04-23T00:00:00.000+00:00",
            "expires": "2020-10-02T00:00:00.000+00:00"
        }
        query_notifications_mock.return_value = [notification_record]
        participant_record = {
            "sort_key": "PARTICIPANT",
            "participant_id": "DIFFERENT_TEST_DUE_DATES_#1",
            "nhs_number": nhs_number
        }
        test_result = {
            'participant_id': participant_id,
            'sort_key': 'RESULT#2017-01-01T00:00:00.000+00:00',
            'test_date': '2017-01-01',
            'action': 'go',
            'action_code': 'A',
            'result': 'negative',
            'result_code': '2',
            'infection_result': 'negative',
            'infection_code': '7'
        }
        ceased_record = {
            "participant_id": participant_id,
            "sort_key": "CEASE#2020-12-15",
            "reason": "NO_CERVIX"
        }
        participant_results_mock.return_value = [participant_record, test_result, ceased_record]
        # When
        event_data = {
            'query_parameters': {
                'gp_practice_code': 'A60'
            },
            'session': {'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
        }
        actual_response = service_module.get_gp_notifications(event_data)

        # Then
        expected_response = [{
            'participant_id': participant_id,
            'title': participant_record.get('title'),
            'first_name': participant_record.get('first_name'),
            'middle_names': participant_record.get('middle_names'),
            'last_name': participant_record.get('last_name'),
            'nhs_number': participant_record.get('nhs_number'),
            'date_of_birth': participant_record.get('date_of_birth'),
            'status': participant_record.get('status'),
            'next_test_due_date': participant_record.get('next_test_due_date'),
            'gp_notification': notification_record,
            'ceased_reason': ceased_record.get('reason'),
            'last_test': {
                'test_date': '2017-01-01',
                'action': 'go',
                'action_code': 'A',
                'result': 'negative',
                'result_code': '2',
                'infection_result': 'negative',
                'infection_code': '7'
            }
        }]
        self.assertEqual(expected_response, actual_response)
        audit_mock.assert_called_with(
            session={'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}},
            action=AuditActions.GET_GP_NOTIFICATIONS_BY_GP_PRACTICE_CODE,
            participant_ids=[participant_id],
            nhs_numbers=[nhs_number],
            gp_practice_code='A60'
        )

        add_record_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.GETGPNOTS0001, 'gp_practice_code': 'A60'}),
                              call({'log_reference': LogReference.GETGPNOTS0002, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0003, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0004, 'number_of_participants': 1})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.query_for_participant_by_gp_id_and_gp_notification_live_record_status')
    @patch(f'{module_path}.get_participant_and_test_results')
    @patch(f'{module_path}.audit')
    def test_read_calls_through_multiple_notifications_multiple_participants(
        self, audit_mock, participant_results_mock,
        query_notifications_mock, add_record_mock, log_mock
    ):
        self.maxDiff = None
        # Given
        participant_id_1 = 'qwerty1'
        participant_id_2 = 'asdf2'
        nhs_number_1 = 'nhs_number_1'
        nhs_number_2 = 'nhs_number_2'
        notification_record_1 = {
            "sort_key": "GP-NOTIFICATION#2019-04-23T00:00:00.000+00:00",
            "participant_id": participant_id_1,
            "user_id": "1234567890",
            "live_record_status": "NEW_NOTIFICATION",
            "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
            "created": "2019-04-23T00:00:00.000+00:00",
            "expires": "2020-10-02T00:00:00.000+00:00"
        }
        notification_record_2 = {
            "sort_key": "GP-NOTIFICATION#2018-03-23T00:00:00.000+00:00",
            "participant_id": participant_id_2,
            "user_id": "1234567890",
            "live_record_status": "NEW_NOTIFICATION",
            "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
            "created": "2019-04-23T00:00:00.000+00:00",
            "expires": "2020-10-02T00:00:00.000+00:00"
        }
        notification_record_3 = {
            "sort_key": "GP-NOTIFICATION#2019-04-22T00:00:00.000+00:00",
            "participant_id": participant_id_1,
            "user_id": "1234567890",
            "live_record_status": "NEW_NOTIFICATION",
            "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
            "created": "2019-04-22T00:00:00.000+00:00",
            "expires": "2020-10-03T00:00:00.000+00:00"
        }
        query_notifications_mock.return_value = [notification_record_1, notification_record_2, notification_record_3]
        participant_record_1 = {
            "sort_key": "PARTICIPANT",
            "participant_id": participant_id_1,
            "nhs_number": nhs_number_1,
            'title': 'Mx',
            'first_name': 'a',
            'middle_names': 'b',
            'last_name': 'c',
            'date_of_birth': '1992-03-03',
            'next_test_due_date': '2021-07-07',
            'status': EpisodeStatus.SUSPENDED.value
        }
        participant_record_2 = {
            "sort_key": "PARTICIPANT",
            "participant_id": participant_id_2,
            "nhs_number": nhs_number_2,
            'title': 'Ms',
            'first_name': 'a',
            'middle_names': 'b',
            'last_name': 'c',
            'date_of_birth': '1984-03-03',
            'next_test_due_date': '2022-07-07',
            'status': 'ROUTINE'
        }
        participant_results_mock.side_effect = [
            [participant_record_1],
            [participant_record_2],
            [participant_record_1],
        ]
        add_record_mock.return_value = []

        # When
        event_data = {
            'query_parameters': {
                'gp_practice_code': 'A60'
            },
            'session': {'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
        }
        actual_response = service_module.get_gp_notifications(event_data)

        # Then
        expected_response = [{
            'participant_id': participant_id_1,
            'title': 'Mx',
            'first_name': 'a',
            'middle_names': 'b',
            'last_name': 'c',
            'nhs_number': nhs_number_1,
            'date_of_birth': '1992-03-03',
            'next_test_due_date': '2021-07-07',
            'status': EpisodeStatus.SUSPENDED.value,
            'gp_notification': notification_record_1
        }, {
            'participant_id': participant_id_2,
            'title': 'Ms',
            'first_name': 'a',
            'middle_names': 'b',
            'last_name': 'c',
            'nhs_number': nhs_number_2,
            'date_of_birth': '1984-03-03',
            'next_test_due_date': '2022-07-07',
            'status': 'ROUTINE',
            'gp_notification': notification_record_2
        }, {
            'participant_id': participant_id_1,
            'title': 'Mx',
            'first_name': 'a',
            'middle_names': 'b',
            'last_name': 'c',
            'nhs_number': nhs_number_1,
            'date_of_birth': '1992-03-03',
            'next_test_due_date': '2021-07-07',
            'status': EpisodeStatus.SUSPENDED.value,
            'gp_notification': notification_record_3
        }]
        self.assertListEqual(expected_response, actual_response)
        audit_mock.assert_called_with(
            session={'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}},
            action=AuditActions.GET_GP_NOTIFICATIONS_BY_GP_PRACTICE_CODE,
            participant_ids=[participant_id_1, participant_id_2, participant_id_1],
            nhs_numbers=[nhs_number_1, nhs_number_2, nhs_number_1],
            gp_practice_code='A60'
        )
        add_record_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.GETGPNOTS0001, 'gp_practice_code': 'A60'}),
                              call({'log_reference': LogReference.GETGPNOTS0002, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0003, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0002, 'participant_id': 'asdf2'}),
                              call({'log_reference': LogReference.GETGPNOTS0003, 'participant_id': 'asdf2'}),
                              call({'log_reference': LogReference.GETGPNOTS0002, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0003, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0004, 'number_of_participants': 3})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.query_for_participant_by_gp_id_and_gp_notification_live_record_status')
    @patch(f'{module_path}.get_participant_and_test_results')
    @patch(f'{module_path}.audit')
    @patch(f'{module_path}.get_nhs_number_by_participant_id')
    def test_throws_error_if_no_participant_found(
        self, get_nhs_number_by_participant_id_mock, audit_mock, participant_results_mock,
        query_notifications_mock, add_record_mock, log_mock
    ):
        # Given
        participant_id = 'qwerty1'
        notification_record = {
            "sort_key": "GP-NOTIFICATION#2019-04-23",
            "participant_id": participant_id,
            "user_id": "1234567890",
            "live_record_status": "NEW_NOTIFICATION",
            "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
            "created": "2019-04-23",
            "expires": "2020-10-02T00:00:00.000"
        }
        query_notifications_mock.return_value = [notification_record]
        participant_results_mock.return_value = []
        add_record_mock.return_value = []

        # When
        event_data = {
            'query_parameters': {
                'gp_practice_code': 'A60'
            },
            'session': {'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
        }
        with self.assertRaises(Exception) as context:
            service_module.get_gp_notifications(event_data)

        # Then
        self.assertEqual(
            'No participant found matching participant_id in notification record.',
            context.exception.args[0]
        )
        audit_mock.assert_not_called()
        add_record_mock.assert_not_called()
        get_nhs_number_by_participant_id_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.GETGPNOTS0001, 'gp_practice_code': 'A60'}),
                              call({'log_reference': LogReference.GETGPNOTS0002, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0005, 'participant_id': 'qwerty1'}),
                              ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.query_for_participant_by_gp_id_and_gp_notification_live_record_status')
    @patch(f'{module_path}.get_participant_and_test_results')
    @patch(f'{module_path}.audit')
    @patch(f'{module_path}.get_nhs_number_by_participant_id')
    def test_throws_error_if_no_first_name_last_name_found(
        self, get_nhs_number_by_participant_id_mock, audit_mock, participant_results_mock,
        query_notifications_mock, add_record_mock, log_mock
    ):
        # Given
        participant_id = 'qwerty1'
        notification_record = {
            "sort_key": "GP-NOTIFICATION#2019-04-23",
            "participant_id": participant_id,
            "user_id": "1234567890",
            "live_record_status": "NEW_NOTIFICATION",
            "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
            "created": "2019-04-23",
            "expires": "2020-10-02T00:00:00.000"
        }
        query_notifications_mock.return_value = [notification_record]
        participant_record = {
            "sort_key": "PARTICIPANT",
            "participant_id": "DIFFERENT_TEST_DUE_DATES_#1",
            "nhs_number": '12345'
        }
        test_result = {
            'participant_id': participant_id,
            'sort_key': 'RESULT#2017-01-01T00:00:00.000+00:00',
            'test_date': '2017-01-01',
            'action': 'go',
            'action_code': 'A',
            'result': 'negative',
            'result_code': '2',
            'infection_result': 'negative',
            'infection_code': '7'
        }
        ceased_record = {
            "participant_id": participant_id,
            "sort_key": "CEASE#2020-12-15",
            "reason": "NO_CERVIX"
        }
        participant_results_mock.return_value = [participant_record, test_result, ceased_record]

        # When
        event_data = {
            'query_parameters': {
                'gp_practice_code': 'A60'
            },
            'session': {'user_data': {}}
        }
        with self.assertRaises(Exception) as context:
            service_module.get_gp_notifications(event_data)

        # Then
        self.assertEqual(
            'Cannot find first_name or last_name',
            context.exception.args[0]
        )
        audit_mock.assert_not_called()
        add_record_mock.assert_not_called()
        get_nhs_number_by_participant_id_mock.assert_not_called()

        log_mock.assert_not_called()

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.query_for_participant_by_gp_id_and_gp_notification_live_record_status')
    @patch(f'{module_path}.get_participant_and_test_results')
    @patch(f'{module_path}.audit')
    def test_get_calls_through_with_add_last_viewed_record_if_user_is_csas(
            self, audit_mock, participant_results_mock,
            query_notifications_mock, add_record_mock, log_mock):

        self.maxDiff = None
        # Given
        participant_id = 'qwerty1'
        nhs_number = 'nhs_number'
        notification_record = {
            "sort_key": "GP-NOTIFICATION#2019-04-23T00:00:00.000+00:00",
            "participant_id": participant_id,
            "user_id": "1234567890",
            "live_record_status": "NEW_NOTIFICATION",
            "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
            "created": "2019-04-23T00:00:00.000+00:00",
            "expires": "2020-10-02T00:00:00.000+00:00"
        }
        query_notifications_mock.return_value = [notification_record]
        participant_record = {
            "sort_key": "PARTICIPANT",
            "participant_id": "DIFFERENT_TEST_DUE_DATES_#1",
            "nhs_number": nhs_number
        }
        test_result = {
            'participant_id': participant_id,
            'sort_key': 'RESULT#2017-01-01T00:00:00.000+00:00',
            'test_date': '2017-01-01',
            'action': 'go',
            'action_code': 'A',
            'result': 'negative',
            'result_code': '2',
            'infection_result': 'negative',
            'infection_code': '7'
        }
        ceased_record = {
            "participant_id": participant_id,
            "sort_key": "CEASE#2020-12-15",
            "reason": "NO_CERVIX"
        }
        participant_results_mock.return_value = [participant_record, test_result, ceased_record]
        add_record_mock.return_value = []
        # When
        event_data = {
            'query_parameters': {
                'gp_practice_code': 'A60'
            },
            'session': {'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                        'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
        }
        actual_response = service_module.get_gp_notifications(event_data)

        # Then
        expected_response = [{
            'participant_id': participant_id,
            'title': participant_record.get('title'),
            'first_name': participant_record.get('first_name'),
            'middle_names': participant_record.get('middle_names'),
            'last_name': participant_record.get('last_name'),
            'nhs_number': participant_record.get('nhs_number'),
            'date_of_birth': participant_record.get('date_of_birth'),
            'status': participant_record.get('status'),
            'next_test_due_date': participant_record.get('next_test_due_date'),
            'gp_notification': notification_record,
            'ceased_reason': ceased_record.get('reason'),
            'last_test': {
                'test_date': '2017-01-01',
                'action': 'go',
                'action_code': 'A',
                'result': 'negative',
                'result_code': '2',
                'infection_result': 'negative',
                'infection_code': '7'
            }
        }]
        self.assertEqual(expected_response, actual_response)
        audit_mock.assert_called_with(
            session={'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                     'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}},
            action=AuditActions.GET_GP_NOTIFICATIONS_BY_GP_PRACTICE_CODE,
            participant_ids=[participant_id],
            nhs_numbers=[nhs_number],
            gp_practice_code='A60'
        )

        add_record_mock.assert_called_once_with('Bob', 'Smith', 'A60', 'PRINT_REVIEW')

        expected_log_calls = [call({'log_reference': LogReference.GETGPNOTS0006}),
                              call({'log_reference': LogReference.GETGPNOTS0001, 'gp_practice_code': 'A60'}),
                              call({'log_reference': LogReference.GETGPNOTS0002, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0003, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0004, 'number_of_participants': 1})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module_path}.add_last_viewed_by_record')
    @patch(f'{module_path}.query_for_participant_by_gp_id_and_gp_notification_live_record_status')
    @patch(f'{module_path}.get_participant_and_test_results')
    @patch(f'{module_path}.audit')
    def test_get_call_returns_does_not_add_last_viewed_record_if_user_is_not_csas(
            self, audit_mock, participant_results_mock, query_notifications_mock, add_record_mock, log_mock):

        self.maxDiff = None
        # Given
        participant_id = 'qwerty1'
        nhs_number = 'nhs_number'
        notification_record = {
            "sort_key": "GP-NOTIFICATION#2019-04-23T00:00:00.000+00:00",
            "participant_id": participant_id,
            "user_id": "1234567890",
            "live_record_status": "NEW_NOTIFICATION",
            "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
            "created": "2019-04-23T00:00:00.000+00:00",
            "expires": "2020-10-02T00:00:00.000+00:00"
        }
        query_notifications_mock.return_value = [notification_record]
        participant_record = {
            "sort_key": "PARTICIPANT",
            "participant_id": "DIFFERENT_TEST_DUE_DATES_#1",
            "nhs_number": nhs_number
        }
        test_result = {
            'participant_id': participant_id,
            'sort_key': 'RESULT#2017-01-01T00:00:00.000+00:00',
            'test_date': '2017-01-01',
            'action': 'go',
            'action_code': 'A',
            'result': 'negative',
            'result_code': '2',
            'infection_result': 'negative',
            'infection_code': '7'
        }
        ceased_record = {
            "participant_id": participant_id,
            "sort_key": "CEASE#2020-12-15",
            "reason": "NO_CERVIX"
        }
        participant_results_mock.return_value = [participant_record, test_result, ceased_record]
        # When
        event_data = {
            'query_parameters': {
                'gp_practice_code': 'A60'
            },
            'session': {'selected_role': {'organisation_code': 'DXX', 'role_id': 'NOTCSAS'},
                        'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
        }
        service_module.get_gp_notifications(event_data)

        audit_mock.assert_called_with(
            session={'selected_role': {'organisation_code': 'DXX', 'role_id': 'NOTCSAS'},
                     'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}},
            action=AuditActions.GET_GP_NOTIFICATIONS_BY_GP_PRACTICE_CODE,
            participant_ids=[participant_id],
            nhs_numbers=[nhs_number],
            gp_practice_code='A60'
        )

        add_record_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.GETGPNOTS0001, 'gp_practice_code': 'A60'}),
                              call({'log_reference': LogReference.GETGPNOTS0002, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0003, 'participant_id': 'qwerty1'}),
                              call({'log_reference': LogReference.GETGPNOTS0004, 'number_of_participants': 1})]

        log_mock.assert_has_calls(expected_log_calls)
