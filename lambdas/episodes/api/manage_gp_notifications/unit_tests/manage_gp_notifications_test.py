from unittest import TestCase
from mock import patch, Mock, call
from manage_gp_notifications.log_references import LogReference
from common.test_assertions.api_assertions import assertApiResponse
from manage_gp_notifications.exceptions import MissingPracticeCodeException, \
    SortKeyFormattingException, MissingNotificationException, BadStatusException


@patch('manage_gp_notifications.manage_gp_notifications.log')
@patch('manage_gp_notifications.manage_gp_notifications.get_api_request_info_from_event')
class TestManageGPNotifications(TestCase):
    bad_request_response = 'Requested type not recognised - must be either notification_PNL or notification_NRL'

    @classmethod
    @patch('boto3.resource', Mock())
    def setUpClass(cls):
        global manage_notifications_module
        import manage_gp_notifications.manage_gp_notifications as manage_notifications_module

    def setUp(self):
        super(TestManageGPNotifications, self).setUp()
        self.unwrapped_handler = manage_notifications_module.lambda_handler.__wrapped__

    # Review record

    @patch('manage_gp_notifications.manage_gp_notifications.review_gp_notification')
    def test_lambda_returns_200_for_successful_review_request(self, review_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'participant_id': '1234',
                    'sort_key': 'GP-NOTIFICATION#X09#2020-01-01'
                },
                'headers': None,
                'body': '{}'
            },
            'PUT',
            '/api/gp-notifications/{participant_id}/{sort_key}/reviewed'
        )
        review_mock.return_value = {}

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 200, {'data': {}}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPNOTS0001}),
                              call({'log_reference': LogReference.GPNOTS0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_notifications.manage_gp_notifications.review_gp_notification')
    def test_lambda_returns_500_for_general_unsuccessful_review_request(self, review_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'participant_id': '1234',
                    'sort_key': 'GP-NOTIFICATION#X09#2020-01-01'
                },
                'headers': None,
                'body': '{}'
            },
            'PUT',
            '/api/gp-notifications/{participant_id}/{sort_key}/reviewed'
        )
        review_mock.side_effect = Mock(side_effect=Exception('Test'))

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 500, {'message': 'Cannot complete request'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPNOTS0001}),
                              call({'log_reference': LogReference.GPNOTS0003, 'error': 'Test'})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_notifications.manage_gp_notifications.review_gp_notification')
    def test_lambda_returns_404_for_notification_not_found(self, review_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'participant_id': '1234',
                    'sort_key': 'GP-NOTIFICATION#X09#2020-01-01'
                },
                'headers': None,
                'body': '{}'
            },
            'PUT',
            '/api/gp-notifications/{participant_id}/{sort_key}/reviewed'
        )
        review_mock.side_effect = Mock(side_effect=MissingNotificationException())

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 404,
                          {'message': "gp notification record not found"},
                          actual_response)

        expected_calls = [call({'log_reference': LogReference.GPNOTS0001}),
                          call({'log_reference': LogReference.GPNOTS0003,
                                'error': "Notification not found"})]

        log_mock.assert_has_calls(expected_calls)

    @patch('manage_gp_notifications.manage_gp_notifications.review_gp_notification')
    def test_lambda_returns_409_for_notification_has_wrong_status(self, review_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'participant_id': '1234',
                    'sort_key': 'GP-NOTIFICATION#X09#2020-01-01'
                },
                'headers': None,
                'body': '{}'
            },
            'PUT',
            '/api/gp-notifications/{participant_id}/{sort_key}/reviewed'
        )
        review_mock.side_effect = Mock(side_effect=BadStatusException())

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 409,
                          {'message': "gp notification record does not have status NEW_NOTIFICATION"},
                          actual_response)

        expected_calls = [call({'log_reference': LogReference.GPNOTS0001}),
                          call({'log_reference': LogReference.GPNOTS0003,
                                'error': "Notification does not have status NEW_NOTIFICATION"})]

        log_mock.assert_has_calls(expected_calls)

    @patch('manage_gp_notifications.manage_gp_notifications.review_gp_notification')
    def test_lambda_returns_400_for_malformed_sort_key(self, review_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'participant_id': '1234',
                    'sort_key': 'GP-NOTIFICATION#X09#2020-01-01'
                },
                'headers': None,
                'body': '{}'
            },
            'PUT',
            '/api/gp-notifications/{participant_id}/{sort_key}/reviewed'
        )
        review_mock.side_effect = Mock(side_effect=SortKeyFormattingException())

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 400,
                          {'message': "sort_key has incorrect format"},
                          actual_response)

        expected_calls = [call({'log_reference': LogReference.GPNOTS0001}),
                          call({'log_reference': LogReference.GPNOTS0003,
                                'error': "sort_key must begin with GP-NOTIFICATION#"})]

        log_mock.assert_has_calls(expected_calls)

    # Read existing records

    @patch('manage_gp_notifications.manage_gp_notifications.get_gp_notifications')
    def test_lambda_returns_data_and_200_for_successful_get_request(self, read_mock, event_data_mock, log_mock):
        read_response = [{'hi': 'ba'}, {'a': 'b'}]
        event_data_mock.return_value = (
            {
                'queryParameters': {
                    'gp-notification-code': 'X09',
                },
                'headers': None,
                'body': '{}'
            },
            'GET',
            '/api/gp-notifications'
        )
        read_mock.return_value = read_response

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 200, {'data': read_response}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPNOTS0001}),
                              call({'log_reference': LogReference.GPNOTS0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_notifications.manage_gp_notifications.get_gp_notifications')
    def test_lambda_returns_400_for_missing_gp_practice_code(self, read_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'queryParameters': {
                    'gp-notification-code': 'X09',
                },
                'headers': None,
                'body': '{}'
            },
            'GET',
            '/api/gp-notifications'
        )
        read_mock.side_effect = Mock(side_effect=MissingPracticeCodeException())

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 400, {'message': 'gp_practice_code is a required query parameter'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPNOTS0001}),
                              call({'log_reference': LogReference.GPNOTS0003,
                                    'error': 'gp_practice_code is a required query parameter'})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_notifications.manage_gp_notifications.get_gp_notifications')
    def test_lambda_returns_500_for_failed_get_request(self, read_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'queryParameters': {
                    'gp-notification-code': 'X09',
                },
                'headers': None,
                'body': '{}'
            },
            'GET',
            '/api/gp-notifications'
        )
        read_mock.side_effect = Mock(side_effect=Exception('bad'))

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 500, {'message': 'Cannot complete request'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPNOTS0001}),
                              call({'log_reference': LogReference.GPNOTS0003, 'error': 'bad'})]

        log_mock.assert_has_calls(expected_log_calls)

    # Bad request

    def test_lambda_returns_500_for_bad_request(self, event_data_mock, log_mock):
        organisation_code = '12345'
        organisation_supplemental_id = '54321'
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'organisation_code': organisation_code,
                    'organisation_supplemental_id': organisation_supplemental_id
                },
                'headers': None,
                'body': '{}'
            },
            'GET',
            '/api/organisation-supplemental/{organisation_code}/{organisation_supplemental_id}'
        )

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 400, {'message': 'unrecognised method/resource combination'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPNOTS0001}),
                              call({'log_reference': LogReference.GPNOTS0004})]

        log_mock.assert_has_calls(expected_log_calls)
