import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    GPNOTS0001 = (logging.INFO, 'Received request')
    GPNOTS0002 = (logging.INFO, 'Request successful')
    GPNOTS0003 = (logging.ERROR, 'Request failure')
    GPNOTS0004 = (logging.ERROR, 'Unrecognised request')

    REVGPNOTS0001 = (logging.INFO, 'Reviewing notification record')
    REVGPNOTS0002 = (logging.INFO, 'Retrieved existing record')
    REVGPNOTS0003 = (logging.ERROR, 'Failed to retrieve existing record')
    REVGPNOTS0004 = (logging.INFO, 'Record updated to reviewed')
    REVGPNOTS0005 = (logging.ERROR, 'Failed to update record')

    GETGPNOTS0001 = (logging.INFO, 'Querying notifications by practice code')
    GETGPNOTS0002 = (logging.INFO, 'Querying for other records')
    GETGPNOTS0003 = (logging.INFO, 'Participant return object built')
    GETGPNOTS0004 = (logging.INFO, 'Queries successful')
    GETGPNOTS0005 = (logging.ERROR, 'Participant record not found')
    GETGPNOTS0006 = (logging.INFO, 'Adding last viewed by record.')
    GETGPNOTS0007 = (logging.ERROR, 'Failed to add last viewed by record')
