class RequestResourceException(Exception):
    message = 'unrecognised method/resource combination'

    def __init__(self):
        super().__init__(self.message)


class MissingPracticeCodeException(Exception):
    message = 'gp_practice_code is a required query parameter'

    def __init__(self):
        super().__init__(self.message)


class SortKeyFormattingException(Exception):
    message = 'sort_key must begin with GP-NOTIFICATION#'

    def __init__(self):
        super().__init__(self.message)


class MissingNotificationException(Exception):
    message = 'Notification not found'

    def __init__(self):
        super().__init__(self.message)


class BadStatusException(Exception):
    message = 'Notification does not have status NEW_NOTIFICATION'

    def __init__(self):
        super().__init__(self.message)
