import json
from datetime import datetime, timezone
from unittest import TestCase
from unittest.mock import patch, Mock, call

from common.models.participant import EpisodeStatus
from common.utils.data_segregation.nhais_ciphers import Cohorts

environ_mock = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE',
    'AUDIT_QUEUE_URL': 'AUDIT QUEUE'
}

sqs_client_mock = Mock()
participants_table_mock = Mock()

example_date = datetime(2020, 1, 23, 13, 48, 8, 123456, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

notification_record = {
    "sort_key": "GP-NOTIFICATION#2019-04-23T00:00:00.000+00:00",
    "participant_id": "PARTICIPANT1",
    "user_id": "1234567890",
    "live_record_status": "NEW_NOTIFICATION",
    "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
    "created": "2019-04-23T00:00:00.000+00:00",
    "expires": "2020-10-02T00:00:00.000+00:00"
}

participant_record = {
    'participant_id': 'PARTICIPANT1',
    'test_due_date': '2020-03-13',
    'proposed_test_due_date': '2021-04-01',
    'sort_key': 'PARTICIPANT',
    'nhais_cipher': 'MAN',
    'nhs_number': 'nhs_number_1',
    'date_of_birth': '1992-03-03',
    'next_test_due_date': '2021-07-07',
    'title': 'Ms',
    'first_name': 'a',
    'middle_names': 'b',
    'last_name': 'c',
    'status': EpisodeStatus.SUSPENDED.value,
}

mock_filtered_participants = {'PARTICIPANT1': 'MAN', 'PARTICIPANT2': 'MAN'}

with patch('datetime.datetime', datetime_mock):
    import manage_gp_notifications.manage_gp_notifications as notifications_module


@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.organisation_supplementary_utils.uuid4', Mock(return_value='test-uuid-12345'))
@patch('os.environ', environ_mock)
@patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids',
       Mock(return_value=mock_filtered_participants))
@patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
class TestReviewGpNotification(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        sqs_client_mock.reset_mock()
        participants_table_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.audit_utils.datetime')
    @patch('manage_gp_notifications.manage_gp_notifications_service.Key')
    @patch('common.log.logging')
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    def test_review_gp_notification_successful(self, logging_mock, condition_key_mock, audit_datetime_mock):
        log_mock = Mock()
        logging_mock.getLogger.return_value = log_mock
        audit_datetime_mock.now.return_value.isoformat.return_value = '2020-04-23'

        test_event = self._build_event()
        test_context = Mock()

        condition_key_mock.return_value.eq.return_value = True
        participants_table_mock.get_item.side_effect = [{'Item': notification_record}, {'Item': participant_record}]

        response = notifications_module.lambda_handler(test_event, test_context)

        expected_response = {'body': '{"data": {"message": "record reviewed"}}',
                             'headers': {'Content-Type': 'application/json',
                                         'Strict-Transport-Security': 'max-age=1576800',
                                         'X-Content-Type-Options': 'nosniff'},
                             'isBase64Encoded': False,
                             'statusCode': 200}

        self.assertEqual(expected_response, response)

        self.assertEqual(participants_table_mock.get_item.call_count, 2)

        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': 'PARTICIPANT1', 'sort_key': 'GP-NOTIFICATION#2019-04-23T00:00:00.000+00:00'}),
            call(Key={'participant_id': 'PARTICIPANT1', 'sort_key': 'PARTICIPANT'})
        ])

        self.assertEqual(participants_table_mock.update_item.call_count, 1)

        participants_table_mock.update_item.assert_has_calls(
            [call(Key={'participant_id': 'PARTICIPANT1', 'sort_key': 'GP-NOTIFICATION#2019-04-23T00:00:00.000+00:00'},
                  UpdateExpression='SET #status = :status, #reviewed_date = :reviewed_date, #user_id = :user_id REMOVE #live_record_status', 
                  ExpressionAttributeValues={':status': 'REVIEWED_NOTIFICATION', ':reviewed_date': '2020-01-23',
                                             ':user_id': 'USER_UID'}, ConditionExpression=True,
                  ExpressionAttributeNames={'#status': 'status', '#reviewed_date': 'reviewed_date',
                                            '#user_id': 'user_id', '#live_record_status': 'live_record_status'},
                  ReturnValues='NONE')]

        )

        expected_audit_record = {
            "action": "REVIEW_GP_NOTIFICATION",
            "timestamp": "2020-04-23",
            "internal_id": "1",
            "nhsid_useruid": "USER_UID",
            "session_id": "TEST SESSION",
            "first_name": "firsty",
            "last_name": "lasty",
            "role_id": "R8010",
            "user_organisation_code": "DXX",
            "participant_ids": ["PARTICIPANT1"],
            "nhs_numbers": ["nhs_number_1"]
        }

        sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='AUDIT QUEUE', MessageBody=json.dumps(expected_audit_record))

        expected_log_calls = [
            call(30, '{"log_reference": "LAMBDA0004", "message": "Request does not contain an internal id, creating new one"}', exc_info=False), 
            call(20, '{"log_reference": "LAMBDA0000", "message": "Lambda started"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0007", "assigned_workgroups": [], "message": "Setting user workgroups from session"}', exc_info=False), 
            call(20, '{"log_reference": "GPNOTS0001", "message": "Received request"}', exc_info=False),
            call(20, '{"log_reference": "REVGPNOTS0001", "message": "Reviewing notification record"}', exc_info=False),
            call(20, '{"log_reference": "REVGPNOTS0002", "message": "Retrieved existing record"}', exc_info=False),
            call(20, '{"log_reference": "REVGPNOTS0004", "message": "Record updated to reviewed"}', exc_info=False),
            call(20, '{"log_reference": "AUDIT0001", "message": "Successfully sent an audit record to the audit queue"}', exc_info=False), 
            call(20, '{"log_reference": "GPNOTS0002", "message": "Request successful"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0001", "message": "Lambda exited with status success"}', exc_info=False)
        ]

        log_mock.log.assert_has_calls(expected_log_calls, any_order=True)

    @patch('common.utils.audit_utils.datetime')
    @patch('manage_gp_notifications.manage_gp_notifications_service.Key')
    @patch('common.log.logging')
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.IOM]))
    def test_cannot_review_gp_notification_for_different_workgroup(self,
                                                                   logging_mock,
                                                                   condition_key_mock,
                                                                   audit_datetime_mock):
        log_mock = Mock()
        logging_mock.getLogger.return_value = log_mock
        audit_datetime_mock.now.return_value.isoformat.return_value = '2020-04-23'

        test_event = self._build_event()
        test_context = Mock()

        condition_key_mock.return_value.eq.return_value = True
        participants_table_mock.get_item.side_effect = [{'Item': notification_record}]

        response = notifications_module.lambda_handler(test_event, test_context)

        expected_response = {'body': '{"message": "gp notification record not found"}',
                             'headers': {'Content-Type': 'application/json',
                                         'Strict-Transport-Security': 'max-age=1576800',
                                         'X-Content-Type-Options': 'nosniff'},
                             'isBase64Encoded': False,
                             'statusCode': 404}

        self.assertEqual(expected_response, response)

        self.assertEqual(participants_table_mock.get_item.call_count, 1)

        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': 'PARTICIPANT1', 'sort_key': 'GP-NOTIFICATION#2019-04-23T00:00:00.000+00:00'})
        ])

        participants_table_mock.update_item.assert_not_called()

        sqs_client_mock.send_message.assert_not_called()

        expected_log_calls = [
            call(30, '{"log_reference": "LAMBDA0004", "message": "Request does not contain an internal id, creating new one"}', exc_info=False), 
            call(20, '{"log_reference": "LAMBDA0000", "message": "Lambda started"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0007", "assigned_workgroups": [], "message": "Setting user workgroups from session"}', exc_info=False), 
            call(20, '{"log_reference": "GPNOTS0001", "message": "Received request"}', exc_info=False),
            call(20, '{"log_reference": "REVGPNOTS0001", "message": "Reviewing notification record"}', exc_info=False),
            call(30, '{"log_reference": "DATASEG0002", "removed_items_count": 1, "message": "Participant table items filtered by data segregation"}', exc_info=False), 
            call(20, '{"log_reference": "REVGPNOTS0002", "message": "Retrieved existing record"}', exc_info=False),
            call(40, '{"log_reference": "GPNOTS0003", "error": "Notification not found", "message": "Request failure"}', exc_info=False), 
            call(20, '{"log_reference": "LAMBDA0001", "message": "Lambda exited with status success"}', exc_info=False)
        ]

        log_mock.log.assert_has_calls(expected_log_calls, any_order=True)

    def _build_event(self):
        return {
            'httpMethod': 'PUT',
            'resource': '/api/gp-notifications/{participant_id}/{sort_key}/reviewed',
            'headers': {},
            'requestContext': {'authorizer': {
                'session': json.dumps(
                    {
                        'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                        'user_data': {'nhsid_useruid': 'USER_UID', 'first_name': 'firsty', 'last_name': 'lasty'},
                        'session_id': 'TEST SESSION'
                    }),
                'principalId': 'PRINCIPAL ID'
            }},
            'queryStringParameters': {
                'gp_practice_code': 'X09'
            },
            'pathParameters': {
                'sort_key': 'GP-NOTIFICATION#2019-04-23T00:00:00.000+00:00',
                'participant_id': 'PARTICIPANT1',
            },
        }
