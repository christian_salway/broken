import json
from datetime import datetime, timezone
from unittest import TestCase
from unittest.mock import patch, Mock, call

from common.models.participant import EpisodeStatus
from common.utils.data_segregation.nhais_ciphers import Cohorts

environ_mock = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE',
    'AUDIT_QUEUE_URL': 'AUDIT QUEUE'
}

organisation_supplemental_table_mock = Mock()
sqs_client_mock = Mock()
participants_table_mock = Mock()

notification_record_1 = {
    "sort_key": "GP-NOTIFICATION#2019-04-23T00:00:00.000+00:00",
    "participant_id": "PARTICIPANT1",
    "user_id": "1234567890",
    "live_record_status": "NEW_NOTIFICATION",
    "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
    "created": "2019-04-23T00:00:00.000+00:00",
    "expires": "2020-10-02T00:00:00.000+00:00"
}

notification_record_2 = {
    "sort_key": "GP-NOTIFICATION#2018-03-23T00:00:00.000+00:00",
    "participant_id": "PARTICIPANT2",
    "user_id": "1234567890",
    "live_record_status": "NEW_NOTIFICATION",
    "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
    "created": "2019-04-23T00:00:00.000+00:00",
    "expires": "2020-10-02T00:00:00.000+00:00"
}

notification_record_3 = {
    "sort_key": "GP-NOTIFICATION#2019-04-22T00:00:00.000+00:00",
    "participant_id": "PARTICIPANT1",
    "user_id": "1234567890",
    "live_record_status": "NEW_NOTIFICATION",
    "registered_gp_practice_code_and_test_due_date": "X09#2019-04-23",
    "created": "2019-04-22T00:00:00.000+00:00",
    "expires": "2020-10-03T00:00:00.000+00:00"
}

participant1_record = [
    {
        'participant_id': 'PARTICIPANT1',
        'test_due_date': '2020-03-13',
        'proposed_test_due_date': '2021-04-01',
        'sort_key': 'PARTICIPANT',
        'nhais_cipher': 'MAN',
        'nhs_number': 'nhs_number_1',
        'date_of_birth': '1992-03-03',
        'next_test_due_date': '2021-07-07',
        'title': 'Ms',
        'first_name': 'a',
        'middle_names': 'b',
        'last_name': 'c',
        'status': EpisodeStatus.SUSPENDED.value,
    }
]

participant2_record = [
    {
        'participant_id': 'PARTICIPANT2',
        'test_due_date': '2020-03-13',
        'proposed_test_due_date': '2020-01-02',
        'sort_key': 'PARTICIPANT',
        'nhais_cipher': 'MAN',
        'nhs_number': 'nhs_number_2',
        'date_of_birth': '1984-03-03',
        'next_test_due_date': '2022-07-07',
        'title': 'Ms',
        'first_name': 'a',
        'middle_names': 'b',
        'last_name': 'c',
        'status': 'ROUTINE',
    }
]

mock_filtered_participants = {'PARTICIPANT1': 'MAN', 'PARTICIPANT2': 'MAN'}

example_date = datetime(2020, 1, 23, 13, 48, 8, 123456, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

with patch('datetime.datetime', datetime_mock):
    import manage_gp_notifications.manage_gp_notifications as notifications_module


@patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
       Mock(return_value=organisation_supplemental_table_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.organisation_supplementary_utils.uuid4', Mock(return_value='test-uuid-12345'))
@patch('os.environ', environ_mock)
@patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids',
       Mock(return_value=mock_filtered_participants))
@patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
class TestGetGpNotifications(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        sqs_client_mock.reset_mock()
        organisation_supplemental_table_mock.reset_mock()
        participants_table_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.audit_utils.datetime')
    @patch('common.utils.participant_gp_notification_utils.Key')
    @patch('common.utils.participant_utils.Key')
    @patch('common.log.logging')
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    def test_get_gp_notifications_successful(self, logging_mock, participant_key_mock, notification_key_mock,
                                             audit_datetime_mock):
        log_mock = Mock()
        logging_mock.getLogger.return_value = log_mock
        audit_datetime_mock.now.return_value.isoformat.return_value = '2020-04-23'

        test_event = self._build_event()
        test_context = Mock()

        participant_key_mock.return_value.eq.return_value = True
        notification_key_mock.return_value.eq.return_value = True
        notification_key_mock.return_value.begins_with.return_value = True
        participants_table_mock.query.side_effect = [
            {'Items': [notification_record_1, notification_record_2, notification_record_3]},
            {'Items': participant1_record},
            {'Items': participant2_record},
            {'Items': participant1_record}
        ]

        expected_participant_record_1 = {
            "participant_id": "PARTICIPANT1",
            "title": "Ms",
            "first_name": "a",
            "middle_names": "b",
            "last_name": "c",
            "nhs_number": "nhs_number_1",
            "date_of_birth": "1992-03-03",
            "status": "SUSPENDED",
            "next_test_due_date": "2021-07-07",
            "gp_notification": notification_record_1
        }
        expected_participant_record_2 = {
            "participant_id": "PARTICIPANT2",
            "title": "Ms",
            "first_name": "a",
            "middle_names": "b",
            "last_name": "c",
            "nhs_number": "nhs_number_2",
            "date_of_birth": "1984-03-03",
            "status": "ROUTINE",
            "next_test_due_date": "2022-07-07",
            "gp_notification": notification_record_2
        }
        expected_participant_record_3 = {
            "participant_id": "PARTICIPANT1",
            "title": "Ms",
            "first_name": "a",
            "middle_names": "b",
            "last_name": "c",
            "nhs_number": "nhs_number_1",
            "date_of_birth": "1992-03-03",
            "status": "SUSPENDED",
            "next_test_due_date": "2021-07-07",
            "gp_notification": notification_record_3
        }

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': json.dumps({'data': [
                expected_participant_record_1,
                expected_participant_record_2,
                expected_participant_record_3
            ]
            }),
            'isBase64Encoded': False
        }

        response = notifications_module.lambda_handler(test_event, test_context)

        self.assertEqual(expected_response, response)

        participant_key_mock.assert_has_calls([call('participant_id'),
                                               call().eq('PARTICIPANT1'),
                                               call('participant_id'),
                                               call().eq('PARTICIPANT2'),
                                               call('participant_id'),
                                               call().eq('PARTICIPANT1')])

        notification_key_mock.assert_has_calls([call('live_record_status'),
                                                call().eq('NEW_NOTIFICATION'),
                                                call('registered_gp_practice_code_and_test_due_date'),
                                                call().begins_with('X09')])

        self.assertEqual(participants_table_mock.query.call_count, 4)

        participants_table_mock.query.assert_has_calls(
            [call(IndexName='live-record-status', KeyConditionExpression=True)] +
            [call(KeyConditionExpression=True,
                  ProjectionExpression='nhs_number, sort_key, participant_id, title, first_name, middle_names, last_name, gender, address, date_of_birth, date_of_death, is_ceased, is_invalid_patient, next_test_due_date, registered_gp_practice_code, infection_result, infection_code, #result, #action, action_code, test_date, recall_months, result_code, sender_code, source_code, sender_source_type, slide_number, sending_lab, #status, suppression_reason, invited_date, reason, date_from, is_fp69, event_text, health_authority, crm, active, reason_for_removal_code, reason_for_removal_effective_from_date, hpv_primary, self_sample, practice_code, nhais_cipher, is_paused, is_paused_received_time', 
                  ExpressionAttributeNames={'#result': 'result', '#action': 'action', '#status': 'status'},
                  ScanIndexForward=False)] * 3
        )

        self.assertEqual(organisation_supplemental_table_mock.put_item.call_count, 1)

        organisation_supplemental_table_mock.put_item.assert_has_calls(
            [call(Item={'organisation_code': 'X09',
                        'type_value': 'PRINT_REVIEW#2020-01-23T13:48:08.123456+00:00#firsty.lasty',
                        'organisation_supplemental_id': 'test-uuid-12345'})]
        )

        expected_audit_record = {
            "action": "GET_GP_NOTIFICATIONS_BY_GP_PRACTICE_CODE",
            "timestamp": "2020-04-23",
            "internal_id": "1",
            "nhsid_useruid": "USER_UID",
            "session_id": "TEST SESSION",
            "first_name": "firsty",
            "last_name": "lasty",
            "role_id": "R8010",
            "user_organisation_code": "DXX",
            "participant_ids": ["PARTICIPANT1", "PARTICIPANT2", "PARTICIPANT1"],
            "nhs_numbers": ["nhs_number_1", "nhs_number_2", "nhs_number_1"],
            "gp_practice_code": "X09"
        }

        sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='AUDIT QUEUE', MessageBody=json.dumps(expected_audit_record))

        expected_log_calls = [
            call(30, '{"log_reference": "LAMBDA0004", "message": "Request does not contain an internal id, creating new one"}', exc_info=False), 
            call(20, '{"log_reference": "LAMBDA0000", "message": "Lambda started"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0007", "assigned_workgroups": [], "message": "Setting user workgroups from session"}', exc_info=False), 
            call(20, '{"log_reference": "GPNOTS0001", "message": "Received request"}', exc_info=False),
            call(20, '{"log_reference": "GETGPNOTS0006", "message": "Adding last viewed by record."}', exc_info=False),
            call(20, '{"log_reference": "GETGPNOTS0001", "gp_practice_code": "X09", "message": "Querying notifications by practice code"}', exc_info=False), 
            call(20, '{"log_reference": "GETGPNOTS0002", "participant_id": "PARTICIPANT1", "message": "Querying for other records"}', exc_info=False), 
            call(20, '{"log_reference": "GETGPNOTS0003", "participant_id": "PARTICIPANT1", "message": "Participant return object built"}', exc_info=False), 
            call(20, '{"log_reference": "GETGPNOTS0002", "participant_id": "PARTICIPANT2", "message": "Querying for other records"}', exc_info=False), 
            call(20, '{"log_reference": "GETGPNOTS0003", "participant_id": "PARTICIPANT2", "message": "Participant return object built"}', exc_info=False), 
            call(20, '{"log_reference": "GETGPNOTS0002", "participant_id": "PARTICIPANT1", "message": "Querying for other records"}', exc_info=False), 
            call(20, '{"log_reference": "GETGPNOTS0003", "participant_id": "PARTICIPANT1", "message": "Participant return object built"}', exc_info=False), 
            call(20, '{"log_reference": "AUDIT0001", "message": "Successfully sent an audit record to the audit queue"}', exc_info=False), 
            call(20, '{"log_reference": "GETGPNOTS0004", "number_of_participants": 3, "message": "Queries successful"}', exc_info=False), 
            call(20, '{"log_reference": "GPNOTS0002", "message": "Request successful"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0001", "message": "Lambda exited with status success"}', exc_info=False)
        ]

        log_mock.log.assert_has_calls(expected_log_calls, any_order=True)

    @patch('common.utils.audit_utils.datetime')
    @patch('common.utils.participant_gp_notification_utils.Key')
    @patch('common.utils.participant_utils.Key')
    @patch('common.log.logging')
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.IOM]))
    def test_cannot_get_gp_notifications_for_different_workgroup(self, logging_mock, participant_key_mock,
                                                                 notification_key_mock, audit_datetime_mock):
        log_mock = Mock()
        logging_mock.getLogger.return_value = log_mock
        audit_datetime_mock.now.return_value.isoformat.return_value = '2020-04-23'

        test_event = self._build_event()
        test_context = Mock()

        participant_key_mock.return_value.eq.return_value = True
        notification_key_mock.return_value.eq.return_value = True
        notification_key_mock.return_value.begins_with.return_value = True
        participants_table_mock.query.side_effect = [
            {'Items': [notification_record_1, notification_record_2, notification_record_3]},
            {'Items': participant1_record},
            {'Items': participant2_record},
            {'Items': participant1_record}
        ]

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": []}',
            'isBase64Encoded': False
        }

        response = notifications_module.lambda_handler(test_event, test_context)

        self.assertEqual(expected_response, response)

        participant_key_mock.assert_not_called()

        notification_key_mock.assert_has_calls([call('live_record_status'),
                                                call().eq('NEW_NOTIFICATION'),
                                                call('registered_gp_practice_code_and_test_due_date'),
                                                call().begins_with('X09')])

        self.assertEqual(participants_table_mock.query.call_count, 1)

        participants_table_mock.query.assert_has_calls(
            [call(IndexName='live-record-status', KeyConditionExpression=True)]
        )

        self.assertEqual(organisation_supplemental_table_mock.put_item.call_count, 1)

        organisation_supplemental_table_mock.put_item.assert_has_calls(
            [call(Item={'organisation_code': 'X09',
                        'type_value': 'PRINT_REVIEW#2020-01-23T13:48:08.123456+00:00#firsty.lasty',
                        'organisation_supplemental_id': 'test-uuid-12345'})]
        )

        sqs_client_mock.send_message.assert_not_called()

        expected_log_calls = [
            call(30, '{"log_reference": "LAMBDA0004", "message": "Request does not contain an internal id, creating new one"}', exc_info=False), 
            call(20, '{"log_reference": "LAMBDA0000", "message": "Lambda started"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0007", "assigned_workgroups": [], "message": "Setting user workgroups from session"}', exc_info=False), 
            call(20, '{"log_reference": "GPNOTS0001", "message": "Received request"}', exc_info=False),
            call(20, '{"log_reference": "GETGPNOTS0006", "message": "Adding last viewed by record."}', exc_info=False),
            call(20, '{"log_reference": "GETGPNOTS0001", "gp_practice_code": "X09", "message": "Querying notifications by practice code"}', exc_info=False), 
            call(30, '{"log_reference": "DATASEG0002", "removed_items_count": 3, "message": "Participant table items filtered by data segregation"}', exc_info=False), 
            call(20, '{"log_reference": "GETGPNOTS0004", "number_of_participants": 0, "message": "Queries successful"}', exc_info=False), 
            call(20, '{"log_reference": "GPNOTS0002", "message": "Request successful"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0001", "message": "Lambda exited with status success"}', exc_info=False)
        ]

        log_mock.log.assert_has_calls(expected_log_calls, any_order=True)

    def _build_event(self):
        return {
            'httpMethod': 'GET',
            'resource': '/api/gp-notifications',
            'headers': {},
            'requestContext': {'authorizer': {
                'session': json.dumps(
                    {
                        'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                        'user_data': {'nhsid_useruid': 'USER_UID', 'first_name': 'firsty', 'last_name': 'lasty'},
                        'session_id': 'TEST SESSION'
                    }),
                'principalId': 'PRINCIPAL ID'
            }},
            'queryStringParameters': {
                'gp_practice_code': 'X09'
            }
        }
