from datetime import datetime, timezone
from urllib.parse import unquote

from boto3.dynamodb.conditions import Key
from manage_gp_notifications.exceptions import \
    MissingPracticeCodeException, SortKeyFormattingException, MissingNotificationException, BadStatusException
from manage_gp_notifications.log_references import LogReference

from common.log import log
from common.models.participant import GPNotificationStatus, ParticipantSortKey, ParticipantStatus
from common.utils.audit_utils import audit, AuditActions
from common.utils.dynamodb_access.segregated_operations import segregated_dynamodb_get_item, \
    segregated_dynamodb_update_item
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.dynamodb_helper_utils import build_update_query
from common.utils.organisation_supplementary_utils import add_last_viewed_by_record
from common.utils.participant_gp_notification_utils import \
    query_for_participant_by_gp_id_and_gp_notification_live_record_status
from common.utils.participant_utils import (
    get_participant_and_test_results,
    get_nhs_number_by_participant_id
)

PARTICIPANTS_SORT_KEY = 'PARTICIPANT'
TEST_SORT_KEY = 'RESULT'
CEASED_SORT_KEY = 'CEASE'

PROJECTION_EXPRESSION = ', '.join([
    'organisation_code',
    'type_value',
    'organisation_supplemental_id'
])


def review_gp_notification(event_data):
    log({'log_reference': LogReference.REVGPNOTS0001})

    participant_id = event_data['path_parameters']['participant_id']
    sort_key = unquote(event_data['path_parameters']['sort_key'])

    if not sort_key.startswith(ParticipantSortKey.GP_NOTIFICATION):
        raise SortKeyFormattingException()

    try:
        notification = segregated_dynamodb_get_item(TableNames.PARTICIPANTS, {
            'participant_id': participant_id,
            'sort_key': sort_key
        })
    except Exception as e:
        log({'log_reference': LogReference.REVGPNOTS0003})
        raise e

    log({'log_reference': LogReference.REVGPNOTS0002})

    if not notification:
        raise MissingNotificationException()

    if notification.get('live_record_status') != 'NEW_NOTIFICATION':
        raise BadStatusException()

    user_id = event_data['session']['user_data']['nhsid_useruid']
    try:
        update_notification_to_reviewed(participant_id, user_id, sort_key)
    except Exception as e:
        log({'log_reference': LogReference.REVGPNOTS0005})
        raise e

    log({'log_reference': LogReference.REVGPNOTS0004})
    audit(
        AuditActions.REVIEW_GP_NOTIFICATION,
        session=event_data.get('session'),
        participant_ids=[participant_id],
        nhs_numbers=[get_nhs_number_by_participant_id(participant_id, segregate=True)]
    )
    return {'message': 'record reviewed'}


def update_notification_to_reviewed(participant_id, user_id, sort_key):
    now_date = datetime.now(timezone.utc).date().isoformat()
    condition_expression = Key('sort_key').eq(sort_key) & Key('participant_id').eq(participant_id)
    key = {
        'participant_id': participant_id,
        'sort_key': sort_key
    }
    attributes_to_update = {
        'status': 'REVIEWED_NOTIFICATION',
        'reviewed_date': now_date,
        'user_id': user_id
    }
    attributes_to_delete = ['live_record_status']

    expression, names, values = build_update_query(attributes_to_update, attributes_to_delete)

    return segregated_dynamodb_update_item(TableNames.PARTICIPANTS, dict(
        Key=key,
        UpdateExpression=expression,
        ExpressionAttributeValues=values,
        ConditionExpression=condition_expression,
        ExpressionAttributeNames=names,
        ReturnValues='NONE'
    ))


def get_gp_notifications(event_data):
    session = event_data['session']
    session_user = session.get('user_data')
    first_name = session_user.get('first_name')
    last_name = session_user.get('last_name')

    role_id = session.get('selected_role', {}).get('role_id')
    org = session.get('selected_role', {}).get('organisation_code')
    isCsas = role_id == 'R8010' and org == 'DXX'

    if first_name is None or last_name is None:
        raise Exception('Cannot find first_name or last_name')

    ods_code = event_data.get('query_parameters', {}).get('gp_practice_code')
    if not ods_code:
        raise MissingPracticeCodeException()

    if isCsas:
        try:
            log({'log_reference': LogReference.GETGPNOTS0006})
            print_type = 'PRINT_REVIEW'
            add_last_viewed_by_record(first_name, last_name, ods_code, print_type)
        except Exception as e:
            log({'log_reference': LogReference.GETGPNOTS0007, 'error': str(e)})
            raise e

    log({'log_reference': LogReference.GETGPNOTS0001, 'gp_practice_code': ods_code})
    notification_records = query_for_participant_by_gp_id_and_gp_notification_live_record_status(
        ods_code,
        GPNotificationStatus.NEW_NOTIFICATION.value
    )

    notification_participants = []
    participant_ids = []
    nhs_numbers = []
    for notification_record in notification_records:
        participant_id = notification_record['participant_id']

        participant_ids.append(participant_id)

        log({'log_reference': LogReference.GETGPNOTS0002, 'participant_id': participant_id})
        participant_and_test_records = get_participant_and_test_results(participant_id, segregate=True)

        participant_record = get_participant_record_from_records(participant_id, participant_and_test_records)
        nhs_numbers.append(participant_record['nhs_number'])

        test_result_record = get_test_record_from_records(participant_and_test_records)

        cease_record = get_cease_record_from_records(participant_and_test_records)

        participant = build_participant_object(
            participant_id, notification_record, participant_record, test_result_record, cease_record)
        log({'log_reference': LogReference.GETGPNOTS0003, 'participant_id': participant_id})
        notification_participants.append(participant)

    number_of_participants = len(notification_participants)
    if number_of_participants > 0:
        audit(
            session=event_data.get('session'),
            action=AuditActions.GET_GP_NOTIFICATIONS_BY_GP_PRACTICE_CODE,
            participant_ids=participant_ids,
            nhs_numbers=nhs_numbers,
            gp_practice_code=ods_code
        )

    notification_participants.sort(
        key=lambda participant: (
            sort_by_date(participant),
            sort_by_status(participant))
    )

    log({'log_reference': LogReference.GETGPNOTS0004, 'number_of_participants': number_of_participants})
    return notification_participants


def get_participant_record_from_records(participant_id, records):
    for record in records:
        if record['sort_key'] == PARTICIPANTS_SORT_KEY:
            return record
    log({'log_reference': LogReference.GETGPNOTS0005, 'participant_id': participant_id})
    raise Exception('No participant found matching participant_id in notification record.')


def get_test_record_from_records(records):
    test_result_records = [
        record for record in records if record['sort_key'].startswith(TEST_SORT_KEY)]
    test_result_records.sort(
        reverse=True,
        key=lambda result: result.get('test_date', '')
    )
    return test_result_records[0] if len(test_result_records) > 0 else None


def get_cease_record_from_records(records):
    cease_records = [
        record for record in records if record['sort_key'].startswith(CEASED_SORT_KEY)]

    cease_records.sort(
        reverse=True,
        key=lambda record: record.get('sort_key').split('#')[1]
    )
    return cease_records[0] if len(cease_records) > 0 else None


def sort_by_date(participant):
    expires_string = participant['gp_notification']['expires']
    return datetime.fromisoformat(expires_string).date()


def sort_by_status(participant):
    INVALID_STATUS_STRING = '_UNKNOWN'
    STATUS_ORDER = {
        ParticipantStatus.SUSPENDED: 0,
        ParticipantStatus.REPEAT_ADVISED: 1,
        ParticipantStatus.INADEQUATE: 2,
        ParticipantStatus.ROUTINE: 3,
        ParticipantStatus.CALLED: 4,
        INVALID_STATUS_STRING: 5
    }
    status = participant.get('status')
    if not status or status.upper() not in STATUS_ORDER.keys():
        status = INVALID_STATUS_STRING
    return STATUS_ORDER[status.upper()]


def build_participant_object(
        participant_id, notification_record, participant_record, test_result=None, cease_record=None):
    participant_object = {
        'participant_id': participant_id,
        'title': participant_record.get('title'),
        'first_name': participant_record.get('first_name'),
        'middle_names': participant_record.get('middle_names'),
        'last_name': participant_record.get('last_name'),
        'nhs_number': participant_record.get('nhs_number'),
        'date_of_birth': participant_record.get('date_of_birth'),
        'status': participant_record.get('status'),
        'next_test_due_date': participant_record.get('next_test_due_date'),
        'gp_notification': notification_record
    }
    if test_result:
        participant_object['last_test'] = {
            'test_date': test_result.get('test_date'),
            'action': test_result.get('action'),
            'action_code': test_result.get('action_code'),
            'result': test_result.get('result'),
            'result_code': test_result.get('result_code'),
            'infection_result': test_result.get('infection_result'),
            'infection_code': test_result.get('infection_code')
        }
    if cease_record:
        participant_object['ceased_reason'] = cease_record.get('reason')
    return participant_object
