This lambda returns a list of participants who are non-responders for a GP practice

* Successful response in the 'data' field of the response body:

{
    participant_id,
    title,
    first_name,
    middle_names,
    last_name,
    nhs_number,
    date_of_birth,
    status,
    test_due_date,
    review_by,
    proposed_test_due_date
}

if the participant has previous test results, the most recent will be returned within the object above as follows

last_test: {
    test_date,
    action,
    action_code,
    result,
    result_code,
    infection_result,
    infection_code
}