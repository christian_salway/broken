from mock import call
from common.models.participant import ParticipantStatus

participant_1_records = [
    {
        'sort_key': 'PARTICIPANT',
        'participant_id': 'PARTICIPANT1',
        'title': 'LADY',
        'first_name': 'DUCK',
        'middle_names': 'VON',
        'last_name': 'QUACK',
        'nhs_number': '9999999999',
        'date_of_birth': '1994-02-24',
        'status': ParticipantStatus.SUSPENDED
    },
    {
        'sort_key': 'RESULT#1',
        'participant_id': 'PARTICIPANT1',
        'test_date': '2019-03-01'
    },
    {
        'sort_key': 'RESULT#2',
        'participant_id': 'PARTICIPANT1',
        'test_date': '2020-04-19',
        'action': 'ACTION',
        'action_code': 'ACTION CODE',
        'result': 'RESULT',
        'result_code': 'RESULT CODE',
        'infection_result': 'INFECTION RESULT',
        'infection_code': 'INFECTION CODE'
    },
    {
        'sort_key': 'RESULT#3',
        'participant_id': 'PARTICIPANT1',
        'test_date': '2018-11-12'
    }
]
expected_participant_1_object = {
    'participant_id': 'PARTICIPANT1',
    'title': 'LADY',
    'first_name': 'DUCK',
    'middle_names': 'VON',
    'last_name': 'QUACK',
    'nhs_number': '9999999999',
    'date_of_birth': '1994-02-24',
    'status': ParticipantStatus.SUSPENDED,
    'next_test_due_date': '2020-03-13',
    'review_by': '2020-11-06',
    'proposed_next_test_due_date': '2021-04-01',
    'last_test': {
        'test_date': '2020-04-19',
        'action': 'ACTION',
        'action_code': 'ACTION CODE',
        'result': 'RESULT',
        'result_code': 'RESULT CODE',
        'infection_result': 'INFECTION RESULT',
        'infection_code': 'INFECTION CODE'
    }
}

participant_2_records = [
    {
        'sort_key': 'PARTICIPANT',
        'participant_id': 'PARTICIPANT2',
        'title': 'MR',
        'first_name': 'CHARLES',
        'middle_names': 'E',
        'last_name': 'WOOF',
        'nhs_number': '9999999998',
        'date_of_birth': '1998-01-22',
        'status': ParticipantStatus.ROUTINE
    },
    {
        'sort_key': 'RESULT#1',
        'participant_id': 'PARTICIPANT2',
        'test_date': '2019-01-20',
        'action': 'ACTION',
        'action_code': 'ACTION CODE',
        'result': 'RESULT',
        'result_code': 'RESULT CODE',
        'infection_result': 'INFECTION RESULT',
        'infection_code': 'INFECTION CODE'
    }
]
expected_participant_2_object = {
    'participant_id': 'PARTICIPANT2',
    'title': 'MR',
    'first_name': 'CHARLES',
    'middle_names': 'E',
    'last_name': 'WOOF',
    'nhs_number': '9999999998',
    'date_of_birth': '1998-01-22',
    'status': ParticipantStatus.ROUTINE,
    'next_test_due_date': '2020-03-13',
    'review_by': '2020-11-06',
    'proposed_next_test_due_date': '2020-01-02',
    'last_test': {
        'test_date': '2019-01-20',
        'action': 'ACTION',
        'action_code': 'ACTION CODE',
        'result': 'RESULT',
        'result_code': 'RESULT CODE',
        'infection_result': 'INFECTION RESULT',
        'infection_code': 'INFECTION CODE'
    }
}

participant_3_records = [
    {
        'sort_key': 'PARTICIPANT',
        'participant_id': 'PARTICIPANT3',
        'title': 'MS',
        'first_name': 'PUPPY',
        'middle_names': 'T',
        'last_name': 'DOGG',
        'nhs_number': '9999999997',
        'date_of_birth': '1989-03-11',
        'status': ParticipantStatus.INADEQUATE
    }
]
expected_participant_3_object = {
    'participant_id': 'PARTICIPANT3',
    'title': 'MS',
    'first_name': 'PUPPY',
    'middle_names': 'T',
    'last_name': 'DOGG',
    'nhs_number': '9999999997',
    'date_of_birth': '1989-03-11',
    'status': ParticipantStatus.INADEQUATE,
    'next_test_due_date': '2020-03-13',
    'review_by': '2020-11-06',
    'proposed_next_test_due_date': '2020-11-19'
}

participant_4_records = [
    {
        'sort_key': 'PARTICIPANT',
        'participant_id': 'PARTICIPANT4',
        'title': 'LORD',
        'first_name': 'CAT',
        'middle_names': 'MEOW',
        'last_name': 'BARK',
        'nhs_number': '9999999996',
        'date_of_birth': '1978-12-24',
        'status': ParticipantStatus.ROUTINE
    }
]
expected_participant_4_object = {
    'participant_id': 'PARTICIPANT4',
    'title': 'LORD',
    'first_name': 'CAT',
    'middle_names': 'MEOW',
    'last_name': 'BARK',
    'nhs_number': '9999999996',
    'date_of_birth': '1978-12-24',
    'status': ParticipantStatus.ROUTINE,
    'next_test_due_date': '2019-10-22',
    'review_by': '2020-06-16',
    'proposed_next_test_due_date': '2019-10-11'
}

participant_5_records = [
    {
        'sort_key': 'PARTICIPANT',
        'participant_id': 'PARTICIPANT5',
        'title': 'MRS',
        'first_name': 'BERTIE',
        'middle_names': 'THE',
        'last_name': 'BIRD',
        'nhs_number': '9999999995',
        'date_of_birth': '1993-05-02',
        'status': ParticipantStatus.SUSPENDED
    },
    {
        'sort_key': 'RESULT#1',
        'participant_id': 'PARTICIPANT5',
        'test_date': '2017-12-12',
        'action': 'ACTION',
        'action_code': 'ACTION CODE',
        'result': 'RESULT',
        'result_code': 'RESULT CODE',
        'infection_result': 'INFECTION RESULT',
        'infection_code': 'INFECTION CODE'
    }
]
expected_participant_5_object = {
    'participant_id': 'PARTICIPANT5',
    'title': 'MRS',
    'first_name': 'BERTIE',
    'middle_names': 'THE',
    'last_name': 'BIRD',
    'nhs_number': '9999999995',
    'date_of_birth': '1993-05-02',
    'status': ParticipantStatus.SUSPENDED,
    'next_test_due_date': '2020-10-11',
    'review_by': '2021-06-06',
    'proposed_next_test_due_date': '2020-08-21',
    'last_test': {
        'test_date': '2017-12-12',
        'action': 'ACTION',
        'action_code': 'ACTION CODE',
        'result': 'RESULT',
        'result_code': 'RESULT CODE',
        'infection_result': 'INFECTION RESULT',
        'infection_code': 'INFECTION CODE'
    }
}


def get_log_call_object(log_reference, log_message):
    call_string = '{"log_reference": "'+log_reference+'", "message": "'+log_message+'"}'
    return call(20, call_string, exc_info=False)


expected_log_calls = [
    get_log_call_object('LAMBDA0000', 'Lambda started'),
    get_log_call_object('GETNRL0001', 'Received request to fetch participants on NRL.'),
    call(20, '{"log_reference": "GETNRL0002", "gp id": "DXX", "message": "Fetching episode records in GP practice with NRL status."}', exc_info=False),  
    get_log_call_object('GETNRL0003', 'Fetching participant and test result records for participant.'),
    get_log_call_object('GETNRL0004', 'Building participant object.'),
    get_log_call_object('GETNRL0005', 'Previous test result for participant exists. Building test result part of participant object.'),  
    get_log_call_object('GETNRL0003', 'Fetching participant and test result records for participant.'),
    get_log_call_object('GETNRL0004', 'Building participant object.'),
    get_log_call_object('GETNRL0005', 'Previous test result for participant exists. Building test result part of participant object.'),  
    get_log_call_object('GETNRL0003', 'Fetching participant and test result records for participant.'),
    get_log_call_object('GETNRL0004', 'Building participant object.'),
    get_log_call_object('GETNRL0003', 'Fetching participant and test result records for participant.'),
    get_log_call_object('GETNRL0004', 'Building participant object.'),
    get_log_call_object('GETNRL0003', 'Fetching participant and test result records for participant.'),
    get_log_call_object('GETNRL0004', 'Building participant object.'),
    get_log_call_object('GETNRL0005', 'Previous test result for participant exists. Building test result part of participant object.'),  
    get_log_call_object('GETNRL0006', 'Sorting participants.'),
    get_log_call_object('AUDIT0001', 'Successfully sent an audit record to the audit queue'),
    call(20, '{"log_reference": "GETNRL0007", "number_of_participants": 5, "message": "Returning list of sorted NRL participants."}', exc_info=False),  
    get_log_call_object('GETNRL0008', 'Adding last viewed by record.'),
    get_log_call_object('LAMBDA0001', 'Lambda exited with status success')
]

expected_log_calls_for_different_workgroup = [
    get_log_call_object('LAMBDA0000', 'Lambda started'),
    get_log_call_object('GETNRL0001', 'Received request to fetch participants on NRL.'),
    call(20, '{"log_reference": "GETNRL0002", "gp id": "DXX", "message": "Fetching episode records in GP practice with NRL status."}', exc_info=False),  
    call(30, '{"log_reference": "DATASEG0002", "removed_items_count": 5, "message": "Participant table items filtered by data segregation"}', exc_info=False), 
    get_log_call_object('GETNRL0006', 'Sorting participants.'),
    call(20, '{"log_reference": "GETNRL0007", "number_of_participants": 0, "message": "Returning list of sorted NRL participants."}', exc_info=False),  
    get_log_call_object('GETNRL0008', 'Adding last viewed by record.'),
    get_log_call_object('LAMBDA0001', 'Lambda exited with status success')
]
