from datetime import datetime, timedelta
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_message, json_return_data
from get_nrl_by_gp_practice_code.log_references import LogReference
from common.log import log
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.participant_episode_utils import \
    query_for_participant_by_gp_id_and_episode_live_record_status, EpisodeStatus
from common.utils.participant_utils import get_participant_and_test_results
from common.utils.audit_utils import audit, AuditActions
from common.models.participant import ParticipantStatus
from common.utils.organisation_supplementary_utils import add_last_viewed_by_record


PARTICIPANTS_SORT_KEY = 'PARTICIPANT'
TEST_SORT_KEY = 'RESULT'

INVALID_STATUS_STRING = '_UNKNOWN'
STATUS_ORDER = {
    ParticipantStatus.SUSPENDED: 0,
    ParticipantStatus.REPEAT_ADVISED: 1,
    ParticipantStatus.INADEQUATE: 2,
    ParticipantStatus.ROUTINE: 3,
    ParticipantStatus.CALLED: 4,
    INVALID_STATUS_STRING: 5
}


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):

    log({'log_reference': LogReference.GETNRL0001})

    event_data = get_api_request_info_from_event(event)[0]
    session = event_data['session']
    session_user = session.get('user_data')
    first_name = session_user.get('first_name')
    last_name = session_user.get('last_name')

    role_id = session.get('selected_role', {}).get('role_id')
    org = session.get('selected_role', {}).get('organisation_code')
    isCsas = role_id == 'R8010' and org == 'DXX'

    gp_id = event_data.get('query_parameters', {}).get('gp_practice_code')
    if not gp_id:
        gp_id = session.get('selected_role', {}).get('organisation_code') if session else None

    if not gp_id:
        log({'log_reference': LogReference.GETNRLER0001})
        return json_return_message(400, 'Organisation code not supplied')

    if first_name is None or last_name is None:
        return json_return_message(400, 'Cannot find first_name or last_name')

    log({'log_reference': LogReference.GETNRL0002, 'gp id': gp_id})
    episode_records = query_for_participant_by_gp_id_and_episode_live_record_status(gp_id, EpisodeStatus.NRL.value)

    nrl_participants = []
    participant_ids = []
    nhs_numbers = []
    for episode_record in episode_records:
        participant_id = episode_record['participant_id']
        participant_ids.append(participant_id)

        log({'log_reference': LogReference.GETNRL0003})
        participant_and_test_records = get_participant_and_test_results(participant_id, segregate=True)

        participant_record = get_participant_record_from_records(participant_id, participant_and_test_records)
        test_result_record = get_test_record_from_records(participant_and_test_records)

        nhs_numbers.append(participant_record['nhs_number'])

        participant = build_participant_object(
            participant_id, episode_record, participant_record, test_result_record)
        nrl_participants.append(participant)

    log({'log_reference': LogReference.GETNRL0006})
    nrl_participants.sort(
        key=lambda participant: (
            participant['review_by'],
            sort_by_status(participant))
    )

    number_of_participants = len(nrl_participants)
    if number_of_participants > 0:
        audit(
            session=session,
            action=AuditActions.GET_NRL_BY_GP_PRACTICE_CODE,
            participant_ids=participant_ids,
            nhs_numbers=nhs_numbers,
            gp_practice_code=gp_id
        )

    log({'log_reference': LogReference.GETNRL0007, 'number_of_participants': number_of_participants})

    if isCsas:
        try:
            log({'log_reference': LogReference.GETNRL0008})
            print_type = 'PRINT_NRL'
            add_last_viewed_by_record(first_name, last_name, gp_id, print_type)
        except Exception as e:
            log({'log_reference': LogReference.GETNRLER0004, 'error': str(e)})
            return json_return_message(500, 'Failed to add last viewed by record')

    return json_return_data(200, nrl_participants)


def get_participant_record_from_records(participant_id, records):
    for record in records:
        if record['sort_key'] == PARTICIPANTS_SORT_KEY:
            return record
    log({'log_reference': LogReference.GETNRLER0002})
    raise Exception('No participant found matching participant_id in episode record.')


def get_test_record_from_records(records):
    test_result_records = [
        record for record in records if record['sort_key'].startswith(TEST_SORT_KEY)]
    test_result_records.sort(
        reverse=True,
        key=lambda result: result.get('test_date', '')
    )
    return test_result_records[0] if len(test_result_records) > 0 else None


def sort_by_status(participant):
    status = participant.get('status')
    if not status or status.upper() not in STATUS_ORDER.keys():
        status = INVALID_STATUS_STRING
    return STATUS_ORDER[status.upper()]


def calculate_review_by(test_due_date):
    test_date = datetime.strptime(test_due_date, '%Y-%m-%d').date()
    review_by = test_date + timedelta(days=238)
    return review_by.isoformat()


def build_participant_object(participant_id, episode_record, participant_record, test_result=None):
    log({'log_reference': LogReference.GETNRL0004})
    test_due_date = episode_record.get('test_due_date')
    if not test_due_date:
        log({'log_reference': LogReference.GETNRLER0003})
        raise Exception('Episode does not have a test_due_date.')
    participant_object = {
        'participant_id': participant_id,
        'title': participant_record.get('title'),
        'first_name': participant_record.get('first_name'),
        'middle_names': participant_record.get('middle_names'),
        'last_name': participant_record.get('last_name'),
        'nhs_number': participant_record.get('nhs_number'),
        'date_of_birth': participant_record.get('date_of_birth'),
        'status': participant_record.get('status'),
        'next_test_due_date': test_due_date,
        'review_by': calculate_review_by(test_due_date),
        'proposed_next_test_due_date': episode_record.get('proposed_next_test_due_date')
    }
    if test_result:
        log({'log_reference': LogReference.GETNRL0005})
        participant_object['last_test'] = {
            'test_date': test_result.get('test_date'),
            'action': test_result.get('action'),
            'action_code': test_result.get('action_code'),
            'result': test_result.get('result'),
            'result_code': test_result.get('result_code'),
            'infection_result': test_result.get('infection_result'),
            'infection_code': test_result.get('infection_code')
        }
    return participant_object
