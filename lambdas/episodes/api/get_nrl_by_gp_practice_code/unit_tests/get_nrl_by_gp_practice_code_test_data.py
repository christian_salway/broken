from common.models.participant import EpisodeStatus

EXAMPLE_SUSPENDED_PARTICIPANT_RECORD = {
    'title': 'LADY',
    'first_name': 'DUCK',
    'middle_names': 'VON',
    'last_name': 'QUACK',
    'nhs_number': '9999999999',
    'date_of_birth': '1994-02-24',
    'status': EpisodeStatus.SUSPENDED.value
}

EXAMPLE_EPISODE_RECORD = {
    'test_due_date': '2020-03-13',
    'proposed_test_due_date': '2021-04-01',
    'proposed_next_test_due_date': '2021-04-01'
}

EXAMPLE_TEST_RESULT_RECORD = {
    'test_date': '2019-11-03',
    'action': 'ACTION',
    'action_code': 'ACTION CODE',
    'result': 'RESULT',
    'result_code': 'RESULT CODE',
    'infection_result': 'INFECTION RESULT',
    'infection_code': 'INFECTION CODE'
}

EXPECTED_PARTICIPANT_OBJECT_NO_TEST_DATA = {
        'participant_id': 'PARTICIPANT ID',
        'title': 'LADY',
        'first_name': 'DUCK',
        'middle_names': 'VON',
        'last_name': 'QUACK',
        'nhs_number': '9999999999',
        'date_of_birth': '1994-02-24',
        'status': EpisodeStatus.SUSPENDED.value,
        'next_test_due_date': '2020-03-13',
        'review_by': '2020-11-06',
        'proposed_next_test_due_date': '2021-04-01'
    }

EXPECTED_PARTICIPANT_OBJECT_WITH_TEST_DATA = EXPECTED_PARTICIPANT_OBJECT_NO_TEST_DATA.copy()
EXPECTED_PARTICIPANT_OBJECT_WITH_TEST_DATA.update({'last_test': EXAMPLE_TEST_RESULT_RECORD})
