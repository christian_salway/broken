import json
from unittest import TestCase
from mock import patch, call
from .get_nrl_by_gp_practice_code_test_data import EXAMPLE_SUSPENDED_PARTICIPANT_RECORD, EXAMPLE_EPISODE_RECORD, \
    EXAMPLE_TEST_RESULT_RECORD, EXPECTED_PARTICIPANT_OBJECT_NO_TEST_DATA, EXPECTED_PARTICIPANT_OBJECT_WITH_TEST_DATA
from get_nrl_by_gp_practice_code.log_references import LogReference
from common.models.participant import EpisodeStatus


class TestNRLFunctions(TestCase):
    @classmethod
    @patch('boto3.resource')
    @patch('boto3.client')
    def setUpClass(cls, client_mock, resource_mock):
        global get_nrl_module
        import get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code as get_nrl_module

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.log')
    def test_get_participant_record_from_records_raises_exception_if_no_participants(self, log_mock):
        records = [{'sort_key': 'NOT PARTICIPANT'}]
        with self.assertRaises(Exception) as context:
            get_nrl_module.get_participant_record_from_records('ID1', records)
        log_mock.assert_called_once_with({'log_reference': LogReference.GETNRLER0002})
        self.assertEqual(context.exception.args[0], 'No participant found matching participant_id in episode record.')

    def test_get_participant_record_from_records_returns_participant_record_from_list(self):
        records = [{'sort_key': 'PARTICIPANT'}]
        actual = get_nrl_module.get_participant_record_from_records('ID1', records)
        self.assertEqual(actual, {'sort_key': 'PARTICIPANT'})

    def test_get_participant_record_from_records_returns_first_participant_record_if_multiple_exist(self):
        records = [
            {'sort_key': 'PARTICIPANT', 'name': 'one', 'other_key': 'value'},
            {'sort_key': 'PARTICIPANT', 'name': 'two', 'other_key': 'value'}]
        actual = get_nrl_module.get_participant_record_from_records('ID1', records)
        self.assertEqual(actual, {'sort_key': 'PARTICIPANT', 'name': 'one', 'other_key': 'value'})

    def test_get_test_record_from_records_returns_None_if_no_results_exist(self):
        records = [{'sort_key': 'NOT RESULT'}]
        actual = get_nrl_module.get_test_record_from_records(records)
        self.assertEqual(actual, None)

    def test_get_test_record_from_records_returns_test_record_if_one_exists(self):
        records = [{'sort_key': 'RESULT#1'}]
        actual = get_nrl_module.get_test_record_from_records(records)
        self.assertEqual(actual, {'sort_key': 'RESULT#1'})

    def test_get_test_record_from_records_returns_most_recent_test_record_if_multiple_exist(self):
        records = [
            {'sort_key': 'RESULT#1', 'test_date': '2020-01-01', 'other_key': 'value'},
            {'sort_key': 'RESULT#2', 'test_date': '2019-11-12', 'other_key': 'value'},
            {'sort_key': 'RESULT#3', 'test_date': '2020-05-24', 'other_key': 'value'}
        ]
        actual = get_nrl_module.get_test_record_from_records(records)
        self.assertEqual(actual, {'sort_key': 'RESULT#3', 'test_date': '2020-05-24', 'other_key': 'value'})

    def test_calculate_review_by_correctly_calculates_review_by(self):
        expected = '2020-11-04'
        actual = get_nrl_module.calculate_review_by('2020-03-11')
        self.assertEqual(expected, actual)

    def test_sort_by_status_returns_correct_number_for_lowercase_valid_status(self):
        expected = get_nrl_module.STATUS_ORDER[EpisodeStatus.SUSPENDED.value]
        actual = get_nrl_module.sort_by_status({'status': 'suspended'})

        self.assertEqual(expected, actual)

    def test_sort_by_status_returns_invalid_status_code_for_invalid_status(self):
        expected = get_nrl_module.STATUS_ORDER[get_nrl_module.INVALID_STATUS_STRING]
        actual = get_nrl_module.sort_by_status({'status': 'this is not a real status'})

        self.assertEqual(expected, actual)

    def test_sort_by_status_returns_invalid_status_when_participant_has_no_status(self):
        expected = get_nrl_module.STATUS_ORDER[get_nrl_module.INVALID_STATUS_STRING]
        actual = get_nrl_module.sort_by_status({'key': 'value'})

        self.assertEqual(expected, actual)

    def test_sort_by_status_returns_invalid_status_when_participant_has_null_status(self):
        expected = get_nrl_module.STATUS_ORDER[get_nrl_module.INVALID_STATUS_STRING]
        actual = get_nrl_module.sort_by_status({'status': None})

        self.assertEqual(expected, actual)

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.log')
    def test_build_participant_raises_exception_when_no_test_due_date_present(self, log_mock):
        with self.assertRaises(Exception) as context:
            get_nrl_module.build_participant_object(
                'PARTICIPANT ID', {}, EXAMPLE_SUSPENDED_PARTICIPANT_RECORD)
        self.assertEqual(context.exception.args[0], 'Episode does not have a test_due_date.')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRLER0003})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.log')
    def test_build_participant_object_correctly_formats_participant_with_no_test_result(self, log_mock):
        actual = get_nrl_module.build_participant_object(
            'PARTICIPANT ID', EXAMPLE_EPISODE_RECORD, EXAMPLE_SUSPENDED_PARTICIPANT_RECORD)
        self.assertEqual(EXPECTED_PARTICIPANT_OBJECT_NO_TEST_DATA, actual)
        log_mock.assert_has_calls([call({'log_reference': LogReference.GETNRL0004})])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.log')
    def test_build_participant_object_correctly_formats_participant_with_test_result(self, log_mock):
        actual = get_nrl_module.build_participant_object(
            'PARTICIPANT ID', EXAMPLE_EPISODE_RECORD, EXAMPLE_SUSPENDED_PARTICIPANT_RECORD, EXAMPLE_TEST_RESULT_RECORD)
        self.assertEqual(EXPECTED_PARTICIPANT_OBJECT_WITH_TEST_DATA, actual)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0005})
        ])


@patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.log')
class TestGetNRL(TestCase):
    @classmethod
    @patch('boto3.resource')
    @patch('boto3.client')
    def setUpClass(cls, client_mock, resource_mock):
        global get_nrl_module, audit_utils_module, episode_utils_module
        import get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code as get_nrl_module
        import common.utils.audit_utils as audit_utils_module
        import common.utils.participant_episode_utils as episode_utils_module

    def setUp(self):
        self.session = {
            'selected_role': {'organisation_code': 'ORG1'},
            'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}
        }
        self.api_request_data = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': self.session
            },
            '',
            '')

        self.unwrapped_handler = get_nrl_module.lambda_handler.__wrapped__.__wrapped__

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_uses_gp_practice_code_from_query_by_preferemce(self,
                                                                    get_api_request_mock,
                                                                    get_episode_records_mock,
                                                                    add_record_mock,
                                                                    audit_mock,
                                                                    log_mock):
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'gp_practice_code': '1234'},
                'body': '',
                'session': {'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                            'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')

        add_record_mock.return_value = []

        get_episode_records_mock.return_value = []
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": []}',
            'isBase64Encoded': False
        }

        response = self.unwrapped_handler('event', 'context')
        self.assertEqual(expected_response, response)

        get_api_request_mock.assert_called_once_with('event')
        add_record_mock.assert_called_once_with('Bob', 'Smith', '1234', 'PRINT_NRL')
        get_episode_records_mock.assert_called_once_with('1234', EpisodeStatus.NRL.value)
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRL0002, 'gp id': '1234'}),
            call({'log_reference': LogReference.GETNRL0006}),
            call({'log_reference': LogReference.GETNRL0007, 'number_of_participants': 0})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_uses_gp_practice_code_from_session_as_fallback(self,
                                                                    get_api_request_mock,
                                                                    get_episode_records_mock,
                                                                    add_record_mock,
                                                                    audit_mock,
                                                                    log_mock):
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {
                    'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                    'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}
                }
            },
            '',
            '')

        add_record_mock.return_value = []

        get_episode_records_mock.return_value = []
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": []}',
            'isBase64Encoded': False
        }

        response = self.unwrapped_handler('event', 'context')
        self.assertEqual(expected_response, response)

        get_api_request_mock.assert_called_once_with('event')
        add_record_mock.assert_called_once_with('Bob', 'Smith', 'DXX', 'PRINT_NRL')
        get_episode_records_mock.assert_called_once_with('DXX', EpisodeStatus.NRL.value)
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRL0002, 'gp id': 'DXX'}),
            call({'log_reference': LogReference.GETNRL0006}),
            call({'log_reference': LogReference.GETNRL0007, 'number_of_participants': 0})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_returns_bad_request_if_user_has_no_ods_code(self,
                                                                 get_api_request_mock,
                                                                 add_record_mock,
                                                                 log_mock):
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Organisation code not supplied"}',
            'isBase64Encoded': False
        }
        response = self.unwrapped_handler('event', 'context')
        self.assertEqual(expected_response, response)
        add_record_mock.assert_not_called()
        get_api_request_mock.assert_called_once_with('event')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRLER0001})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_returns_400_when_first_name_last_name_is_missing(self,
                                                                      get_api_request_mock,
                                                                      get_episode_records_mock,
                                                                      audit_mock,
                                                                      add_record_mock,
                                                                      log_mock):
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {
                    'selected_role': {'organisation_code': '5678'},
                    'user_data': {}
                }
            },
            '',
            '')

        get_episode_records_mock.return_value = []
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Cannot find first_name or last_name"}',
            'isBase64Encoded': False
        }

        response = self.unwrapped_handler('event', 'context')
        self.assertEqual(expected_response, response)

        get_api_request_mock.assert_called_once_with('event')

        audit_mock.assert_not_called()
        add_record_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_returns_500_when_add_last_viewed_by_raises_exception(self,
                                                                  get_api_request_mock,
                                                                  get_episode_records_mock,
                                                                  add_record_mock,
                                                                  audit_mock,
                                                                  log_mock):

        add_record_mock.side_effect = Exception('Something went wrong.')

        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {
                    'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                    'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')
        get_episode_records_mock.return_value = []

        expected_response = {
            'statusCode': 500,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Failed to add last viewed by record"}',
            'isBase64Encoded': False
        }

        response = self.unwrapped_handler('event', 'context')
        self.assertEqual(expected_response, response)

        get_api_request_mock.assert_called_once_with('event')
        get_episode_records_mock.assert_called_once_with('DXX', EpisodeStatus.NRL.value)
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRL0002, 'gp id': 'DXX'}),
            call({'log_reference': LogReference.GETNRL0006}),
            call({'log_reference': LogReference.GETNRL0007, 'number_of_participants': 0})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_returns_empty_list_when_no_episodes_with_nrl_status_in_ods_code(self,
                                                                                     get_api_request_mock,
                                                                                     get_episode_records_mock,
                                                                                     add_record_mock,
                                                                                     audit_mock,
                                                                                     log_mock):
        get_api_request_mock.return_value = self.api_request_data
        get_episode_records_mock.return_value = []
        add_record_mock.return_value = []
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": []}',
            'isBase64Encoded': False
        }

        response = self.unwrapped_handler('event', 'context')

        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with('event')
        add_record_mock.assert_not_called()
        get_episode_records_mock.assert_called_once_with('ORG1', EpisodeStatus.NRL.value)
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRL0002, 'gp id': 'ORG1'}),
            call({'log_reference': LogReference.GETNRL0006}),
            call({'log_reference': LogReference.GETNRL0007, 'number_of_participants': 0})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_participant_and_test_results')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_raises_exception_if_no_participant_found_for_episode(self,
                                                                          get_api_request_mock,
                                                                          get_episode_records_mock,
                                                                          get_participant_records_mock,
                                                                          add_record_mock,
                                                                          log_mock):
        get_api_request_mock.return_value = self.api_request_data
        get_episode_records_mock.return_value = [{'participant_id': 'PARTICIPANT1'}]
        get_participant_records_mock.return_value = []

        with self.assertRaises(Exception) as context:
            self.unwrapped_handler('event', 'context')

        self.assertEqual(context.exception.args[0], 'No participant found matching participant_id in episode record.')
        add_record_mock.assert_not_called()
        get_api_request_mock.assert_called_once_with('event')
        get_episode_records_mock.assert_called_once_with('ORG1', EpisodeStatus.NRL.value)
        get_participant_records_mock.assert_called_once_with('PARTICIPANT1', segregate=True)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRL0002, 'gp id': 'ORG1'}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRLER0002})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_participant_and_test_results')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_returns_null_for_fields_if_they_do_not_exist(self,
                                                                  get_api_request_mock,
                                                                  get_episode_records_mock,
                                                                  get_participant_records_mock,
                                                                  add_record_mock,
                                                                  audit_mock,
                                                                  log_mock):
        get_api_request_mock.return_value = self.api_request_data
        get_episode_records_mock.return_value = [{'participant_id': 'PARTICIPANT1', 'test_due_date': '2020-03-11'}]
        get_participant_records_mock.return_value = [{'sort_key': 'PARTICIPANT', 'nhs_number': '1'}]

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": [{"participant_id": "PARTICIPANT1", "title": null, "first_name": null, "middle_names": null, "last_name": null, "nhs_number": "1", "date_of_birth": null, "status": null, "next_test_due_date": "2020-03-11", "review_by": "2020-11-04", "proposed_next_test_due_date": null}]}',  
            'isBase64Encoded': False
        }

        response = self.unwrapped_handler('event', 'context')
        self.assertEqual(expected_response, response)

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_participant_and_test_results')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_returns_most_recent_test_result_if_participant_has_multiple_past_results(self,
                                                                                              get_api_request_mock,
                                                                                              get_episode_records_mock,
                                                                                              get_participant_records_mock,   
                                                                                              add_record_mock,
                                                                                              audit_mock,
                                                                                              log_mock):
        get_api_request_mock.return_value = self.api_request_data
        get_episode_records_mock.return_value = [{
            'participant_id': 'PARTICIPANT1',
            'test_due_date': '2020-03-11'
        }]
        get_participant_records_mock.return_value = [
            {'sort_key': 'PARTICIPANT', 'nhs_number': '99999999999'},
            {'sort_key': 'RESULT', 'test_date': '2019-11-04'},
            {'sort_key': 'RESULT', 'test_date': '2019-01-12'},
            {'sort_key': 'RESULT', 'test_date': '2020-12-15'},
            {'sort_key': 'RESULT', 'test_date': '2018-04-22'}
        ]

        add_record_mock.return_value = []

        response = self.unwrapped_handler('event', 'context')
        response_body = json.loads(response['body'])

        self.assertEqual(response_body['data'][0]['last_test']['test_date'], '2020-12-15')
        get_api_request_mock.assert_called_once_with('event')
        add_record_mock.assert_not_called()
        get_episode_records_mock.assert_called_once_with('ORG1', EpisodeStatus.NRL.value)
        get_participant_records_mock.assert_called_once_with('PARTICIPANT1', segregate=True)
        audit_mock.assert_called_once_with(
            session=self.session,
            action=audit_utils_module.AuditActions.GET_NRL_BY_GP_PRACTICE_CODE,
            participant_ids=['PARTICIPANT1'],
            nhs_numbers=['99999999999'],
            gp_practice_code='ORG1'
        )
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRL0002, 'gp id': 'ORG1'}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0005}),
            call({'log_reference': LogReference.GETNRL0006}),
            call({'log_reference': LogReference.GETNRL0007, 'number_of_participants': 1})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_participant_and_test_results')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_sorts_participants_by_review_by(self,
                                                     get_api_request_mock,
                                                     get_episode_records_mock,
                                                     get_participant_records_mock,
                                                     add_record_mock,
                                                     audit_mock,
                                                     log_mock):
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'gp_practice_code': 'ORG1'},
                'body': '',
                'session': {'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                            'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')
        get_episode_records_mock.return_value = [
            {'participant_id': 'PARTICIPANT1', 'test_due_date': '2020-03-11'},
            {'participant_id': 'PARTICIPANT2', 'test_due_date': '2019-11-23'},
            {'participant_id': 'PARTICIPANT3', 'test_due_date': '2020-07-30'}
        ]
        get_participant_records_mock.return_value = [{'sort_key': 'PARTICIPANT', 'nhs_number': '99999999999'}]

        add_record_mock.return_value = []
        response = self.unwrapped_handler('event', 'context')
        response_body = json.loads(response['body'])['data']

        self.assertEqual(len(response_body), 3)
        self.assertEqual(response_body[0]['participant_id'], 'PARTICIPANT2')
        self.assertEqual(response_body[1]['participant_id'], 'PARTICIPANT1')
        self.assertEqual(response_body[2]['participant_id'], 'PARTICIPANT3')
        get_api_request_mock.assert_called_once_with('event')
        add_record_mock.assert_called_once_with('Bob', 'Smith', 'ORG1', 'PRINT_NRL')
        get_episode_records_mock.assert_called_once_with('ORG1', EpisodeStatus.NRL.value)
        self.assertEqual(get_participant_records_mock.call_count, 3)
        get_participant_records_mock.assert_has_calls([
            call('PARTICIPANT1', segregate=True),
            call('PARTICIPANT2', segregate=True),
            call('PARTICIPANT3', segregate=True)
        ])
        audit_mock.assert_called_once_with(
            session={'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                     'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}},
            action=audit_utils_module.AuditActions.GET_NRL_BY_GP_PRACTICE_CODE,
            participant_ids=['PARTICIPANT1', 'PARTICIPANT2', 'PARTICIPANT3'],
            nhs_numbers=['99999999999', '99999999999', '99999999999'],
            gp_practice_code='ORG1'
        )
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRL0002, 'gp id': 'ORG1'}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0006}),
            call({'log_reference': LogReference.GETNRL0007, 'number_of_participants': 3})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_participant_and_test_results')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_secondary_sorts_participants_by_status_when_review_by_matches(self,
                                                                                   get_api_request_mock,
                                                                                   get_episode_records_mock,
                                                                                   get_participant_records_mock,
                                                                                   add_record_mock,
                                                                                   audit_mock,
                                                                                   log_mock):
        get_api_request_mock.return_value = self.api_request_data
        get_episode_records_mock.return_value = [
            {'participant_id': 'PARTICIPANT1', 'test_due_date': '2020-03-11'},
            {'participant_id': 'PARTICIPANT2', 'test_due_date': '2019-11-23'},
            {'participant_id': 'PARTICIPANT3', 'test_due_date': '2020-03-12'},
            {'participant_id': 'PARTICIPANT4', 'test_due_date': '2020-03-11'},
            {'participant_id': 'PARTICIPANT5', 'test_due_date': '2020-03-11'}
        ]
        get_participant_records_mock.side_effect = [
            [{'sort_key': 'PARTICIPANT', 'nhs_number': '99999999991', 'status': 'ROUTINE'}],
            [{'sort_key': 'PARTICIPANT', 'nhs_number': '99999999992', 'status': EpisodeStatus.SUSPENDED.value}],  
            [{'sort_key': 'PARTICIPANT', 'nhs_number': '99999999993', 'status': EpisodeStatus.SUSPENDED.value}],  
            [{'sort_key': 'PARTICIPANT', 'nhs_number': '99999999994', 'status': 'CALLED'}],
            [{'sort_key': 'PARTICIPANT', 'nhs_number': '99999999995'}]
        ]

        response = self.unwrapped_handler('event', 'context')
        response_body = json.loads(response['body'])['data']

        self.assertEqual(len(response_body), 5)
        self.assertEqual(response_body[0]['participant_id'], 'PARTICIPANT2')
        self.assertEqual(response_body[1]['participant_id'], 'PARTICIPANT1')
        self.assertEqual(response_body[2]['participant_id'], 'PARTICIPANT4')
        self.assertEqual(response_body[3]['participant_id'], 'PARTICIPANT5')
        self.assertEqual(response_body[4]['participant_id'], 'PARTICIPANT3')
        get_api_request_mock.assert_called_once_with('event')
        get_episode_records_mock.assert_called_once_with('ORG1', EpisodeStatus.NRL.value)
        self.assertEqual(get_participant_records_mock.call_count, 5)
        get_participant_records_mock.assert_has_calls([
            call('PARTICIPANT1', segregate=True),
            call('PARTICIPANT2', segregate=True),
            call('PARTICIPANT3', segregate=True),
            call('PARTICIPANT4', segregate=True),
            call('PARTICIPANT5', segregate=True)
        ])
        audit_mock.assert_called_once_with(
            session=self.session, action=audit_utils_module.AuditActions.GET_NRL_BY_GP_PRACTICE_CODE,
            participant_ids=['PARTICIPANT1', 'PARTICIPANT2', 'PARTICIPANT3', 'PARTICIPANT4', 'PARTICIPANT5'],
            nhs_numbers=['99999999991', '99999999992', '99999999993', '99999999994', '99999999995'],
            gp_practice_code='ORG1'
        )
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRL0002, 'gp id': 'ORG1'}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0006}),
            call({'log_reference': LogReference.GETNRL0007, 'number_of_participants': 5})
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_participant_and_test_results')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_adds_last_viewed_record_if_role_is_csas(self,
                                                             get_api_request_mock,
                                                             get_episode_records_mock,
                                                             get_participant_records_mock,
                                                             add_record_mock,
                                                             audit_mock,
                                                             log_mock):
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'gp_practice_code': 'ORG1'},
                'body': '',
                'session': {'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                            'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')
        get_episode_records_mock.return_value = [
            {'participant_id': 'PARTICIPANT1', 'test_due_date': '2020-03-11'},
            {'participant_id': 'PARTICIPANT2', 'test_due_date': '2019-11-23'},
            {'participant_id': 'PARTICIPANT3', 'test_due_date': '2020-07-30'}
        ]
        get_participant_records_mock.return_value = [{'sort_key': 'PARTICIPANT', 'nhs_number': '99999999999'}]

        add_record_mock.return_value = []
        self.unwrapped_handler('event', 'context')

        add_record_mock.assert_called_once_with('Bob', 'Smith', 'ORG1', 'PRINT_NRL')

        audit_mock.assert_called_once_with(
            session={'selected_role': {'organisation_code': 'DXX', 'role_id': 'R8010'},
                     'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}},
            action=audit_utils_module.AuditActions.GET_NRL_BY_GP_PRACTICE_CODE,
            participant_ids=['PARTICIPANT1', 'PARTICIPANT2', 'PARTICIPANT3'],
            nhs_numbers=['99999999999', '99999999999', '99999999999'],
            gp_practice_code='ORG1'
        )
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRL0002, 'gp id': 'ORG1'}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0006}),
            call({'log_reference': LogReference.GETNRL0007, 'number_of_participants': 3}),
            call({'log_reference': LogReference.GETNRL0008}),
        ])

    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.audit')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.add_last_viewed_by_record')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_participant_and_test_results')
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.query_for_participant_by_gp_id_and_episode_live_record_status')  
    @patch('get_nrl_by_gp_practice_code.get_nrl_by_gp_practice_code.get_api_request_info_from_event')
    def test_get_nrl_does_not_adds_last_viewed_record_if_role_is_not_csas(self,
                                                                          get_api_request_mock,
                                                                          get_episode_records_mock,
                                                                          get_participant_records_mock,
                                                                          add_record_mock,
                                                                          audit_mock,
                                                                          log_mock):
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'gp_practice_code': 'ORG1'},
                'body': '',
                'session': {'selected_role': {'organisation_code': 'DXX', 'role_id': 'NOTCSAS'},
                            'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}}
            },
            '',
            '')
        get_episode_records_mock.return_value = [
            {'participant_id': 'PARTICIPANT1', 'test_due_date': '2020-03-11'},
            {'participant_id': 'PARTICIPANT2', 'test_due_date': '2019-11-23'},
            {'participant_id': 'PARTICIPANT3', 'test_due_date': '2020-07-30'}
        ]
        get_participant_records_mock.return_value = [{'sort_key': 'PARTICIPANT', 'nhs_number': '99999999999'}]

        self.unwrapped_handler('event', 'context')

        add_record_mock.assert_not_called()
        audit_mock.assert_called_once_with(
            session={'selected_role': {'organisation_code': 'DXX', 'role_id': 'NOTCSAS'},
                     'user_data': {'first_name': 'Bob', 'last_name': 'Smith'}},
            action=audit_utils_module.AuditActions.GET_NRL_BY_GP_PRACTICE_CODE,
            participant_ids=['PARTICIPANT1', 'PARTICIPANT2', 'PARTICIPANT3'],
            nhs_numbers=['99999999999', '99999999999', '99999999999'],
            gp_practice_code='ORG1'
        )
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETNRL0001}),
            call({'log_reference': LogReference.GETNRL0002, 'gp id': 'ORG1'}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0003}),
            call({'log_reference': LogReference.GETNRL0004}),
            call({'log_reference': LogReference.GETNRL0006}),
            call({'log_reference': LogReference.GETNRL0007, 'number_of_participants': 3})
        ])
