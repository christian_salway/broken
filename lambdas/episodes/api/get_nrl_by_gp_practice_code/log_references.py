import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    GETNRL0001 = (logging.INFO, 'Received request to fetch participants on NRL.')
    GETNRL0002 = (logging.INFO, 'Fetching episode records in GP practice with NRL status.')
    GETNRL0003 = (logging.INFO, 'Fetching participant and test result records for participant.')
    GETNRL0004 = (logging.INFO, 'Building participant object.')
    GETNRL0005 = (
        logging.INFO,
        'Previous test result for participant exists. Building test result part of participant object.'
    )
    GETNRL0006 = (logging.INFO, 'Sorting participants.')
    GETNRL0007 = (logging.INFO, 'Returning list of sorted NRL participants.')
    GETNRL0008 = (logging.INFO, 'Adding last viewed by record.')

    GETNRLER0001 = (logging.ERROR, 'Logged in user does not have ODS code.')
    GETNRLER0002 = (logging.ERROR, 'No participant exists with given participant id.')
    GETNRLER0003 = (logging.ERROR, 'Episode record does not have a test_due_date.')
    GETNRLER0004 = (logging.ERROR, 'Failed to add last viewed by record')
