import json
from ddt import ddt, data, unpack
from datetime import date
from unittest import TestCase
from mock import patch, call
from reinstate_participant.log_references import LogReference
from common.services.reinstate.reinstate_response import ReinstateResponse


@ddt
class TestGETReinstateParticipant(TestCase):

    module = 'reinstate_participant.reinstate_participant'

    @patch('boto3.resource')
    def setUp(self, *args):
        # Setup lambda module
        from reinstate_participant.reinstate_participant import lambda_handler
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    def assertResponse(self, status_code, body, actual_response):
        expected_response = {
            'statusCode': status_code,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': json.dumps(body),
            'isBase64Encoded': False
        }
        self.assertDictEqual(expected_response, actual_response)

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_reinstate_participant_returns_bad_request_given_unsupported_http_method(self, request_info_mock, log_mock):
        request_info_mock.return_value = {}, 'POST', ''
        expected_body = {"message": "Requested method not supported."}

        actual_response = self.unwrapped_handler({}, {})

        self.assertResponse(400, expected_body, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'POST'}),
            call({'log_reference': LogReference.REINSTATE0002}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_reinstate_participant_returns_404_given_participant_not_found(
            self, request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'GET', ''
        get_participant_mock.return_value = []
        expected_body = {"message": "Participant not found."}

        actual_response = self.unwrapped_handler({}, {})

        self.assertResponse(404, expected_body, actual_response)

        get_participant_mock.assert_called_with('123', segregate=True)

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'GET'}),
            call({'log_reference': LogReference.REINSTATE0003}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.can_participant_be_reinstated')
    def test_reinstate_participant_returns_400_given_participant_cannot_be_reinstated(
            self, can_be_reinstated_mock, request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'GET', ''
        participant = {'participant_id': '123', 'sort_key': 'PARTICIPANT', 'active': False}
        get_participant_mock.return_value = [participant]
        can_be_reinstated_mock.return_value = False
        expected_body = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Participant cannot be reinstated."}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler({}, {})

        self.assertDictEqual(expected_body, actual_response)

        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'GET'}),
            call({'log_reference': LogReference.REINSTATE0004}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.can_participant_be_reinstated')
    def test_reinstate_participant_returns_400_given_no_cease_record(
            self, can_be_reinstated_mock, request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'GET', ''
        participant = {'participant_id': '123', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        get_participant_mock.return_value = [participant]
        can_be_reinstated_mock.return_value = True
        expected_body = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Participant missing cease record."}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler({}, {})

        self.assertDictEqual(expected_body, actual_response)

        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'GET'}),
            call({'log_reference': LogReference.REINSTATE0005}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.can_participant_be_reinstated')
    @patch(f'{module}.is_valid_cease_reason')
    def test_reinstate_participant_returns_400_given_invalid_cease_reason(
            self, valid_cease_reason_mock, can_be_reinstated_mock,
            request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'GET', ''
        participant = {'participant_id': '123', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        test_result_1 = {'id': '1', 'sort_key': 'RESULT#2020-05-31'}
        test_result_2 = {'id': '1', 'sort_key': 'RESULT#2017-05-31'}
        cease_record = {'sort_key': 'CEASE#2020-06-01', 'reason': 'because'}
        get_participant_mock.return_value = [cease_record, participant, test_result_1, test_result_2]
        can_be_reinstated_mock.return_value = True
        valid_cease_reason_mock.return_value = False
        expected_body = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Cease reason is not valid."}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler({}, {})

        self.assertDictEqual(expected_body, actual_response)

        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)
        valid_cease_reason_mock.assert_called_with('because')

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'GET'}),
            call({'log_reference': LogReference.REINSTATE0006}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}._build_get_reinstate_parameters_response')
    @patch(f'{module}.calculate_for_manual_reinstate')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.can_participant_be_reinstated')
    @patch(f'{module}.is_valid_cease_reason')
    def test_reinstate_participant_calls_calculator(
            self, valid_cease_reason_mock, can_be_reinstated_mock,
            request_info_mock, get_participant_mock, calculator_mock, build_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'GET', ''
        participant = {'participant_id': '123', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        test_result_1 = {'id': '1', 'sort_key': 'RESULT#2020-05-31'}
        test_result_2 = {'id': '1', 'sort_key': 'RESULT#2017-05-31'}
        cease_record = {'sort_key': 'CEASE#2020-06-01', 'reason': 'because'}
        get_participant_mock.return_value = [cease_record, participant, test_result_1, test_result_2]
        can_be_reinstated_mock.return_value = True
        valid_cease_reason_mock.return_value = True
        reinstate_response = ReinstateResponse(status=None, next_test_due_date=None, explanation=['None'])
        calculator_mock.return_value = reinstate_response

        self.unwrapped_handler({}, {})

        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)
        valid_cease_reason_mock.assert_called_with('because')
        calculator_mock.assert_called_with(participant, [test_result_1, test_result_2], 'because')
        build_mock.assert_called_with(reinstate_response)

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'GET'}),
            call({'log_reference': LogReference.REINSTATE0007}),
            call({'log_reference': LogReference.REINSTATE0008, 'calculator_response': {'result': None, 'status': None, 'next_test_due_date': None, 'explanation': ['None'], 'error': False}}),   
            call({'log_reference': LogReference.REINSTATE0009})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @unpack
    @data(
        (None, None, ["exp1"], True,  '{"explanation": ["exp1"]}'),
        (None, None, ["exp2"], False, '{"explanation": ["exp2"]}'),
        ('xx', None, ["exp3"], True,  '{"status": "xx", "explanation": ["exp3"]}')
    )
    @patch(f'{module}.log')
    @patch(f'{module}.calculate_for_manual_reinstate')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.can_participant_be_reinstated')
    @patch(f'{module}.is_valid_cease_reason')
    def test_reinstate_participant_calls_calculator_returns_400_if_calculator_problem(
            self,
            ntdd_status, ntdd_date, ntdd_explanation, ntdd_error, expected_message,
            valid_cease_reason_mock, can_be_reinstated_mock,
            request_info_mock, get_participant_mock, calculator_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'GET', ''
        participant = {'participant_id': '123', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        test_result_1 = {'id': '1', 'sort_key': 'RESULT#2020-05-31'}
        test_result_2 = {'id': '1', 'sort_key': 'RESULT#2017-05-31'}
        cease_record = {'sort_key': 'CEASE#2020-06-01', 'reason': 'because'}
        get_participant_mock.return_value = [cease_record, participant, test_result_1, test_result_2]
        can_be_reinstated_mock.return_value = True
        valid_cease_reason_mock.return_value = True
        reinstate_response = ReinstateResponse(status=ntdd_status,
                                               next_test_due_date=ntdd_date,
                                               explanation=ntdd_explanation)
        reinstate_response.error = ntdd_error
        calculator_mock.return_value = reinstate_response
        expected_body = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': f'{{"data": {expected_message}}}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler({}, {})

        self.assertDictEqual(expected_body, actual_response)

        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)
        valid_cease_reason_mock.assert_called_with('because')
        calculator_mock.assert_called_with(participant, [test_result_1, test_result_2], 'because')

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'GET'}),
            call({'log_reference': LogReference.REINSTATE0007}),
            call({'log_reference': LogReference.REINSTATE0008, 'calculator_response': {'result': None, 'status': ntdd_status, 'next_test_due_date': ntdd_date, 'explanation': ntdd_explanation, 'error': ntdd_error}}),   
            call({'log_reference': LogReference.REINSTATE0009}),
            call({'log_reference': LogReference.REINSTATE0010})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.calculate_for_manual_reinstate')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.can_participant_be_reinstated')
    @patch(f'{module}.is_valid_cease_reason')
    def test_reinstate_participant_calls_calculator_returns_200_if_calculator_succeeds(
            self, valid_cease_reason_mock, can_be_reinstated_mock,
            request_info_mock, get_participant_mock, calculator_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'GET', ''
        participant = {'participant_id': '123', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        test_result_1 = {'id': '1', 'sort_key': 'RESULT#2020-05-31'}
        test_result_2 = {'id': '1', 'sort_key': 'RESULT#2017-05-31'}
        cease_record = {'sort_key': 'CEASE#2020-06-01', 'reason': 'because'}
        get_participant_mock.return_value = [cease_record, participant, test_result_1, test_result_2]
        can_be_reinstated_mock.return_value = True
        valid_cease_reason_mock.return_value = True
        reinstate_response = ReinstateResponse(status='normal',
                                               next_test_due_date=date(2019, 12, 4),
                                               explanation=['None'],
                                               error=False)
        calculator_mock.return_value = reinstate_response
        expected_body = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": {"status": "normal", "next_test_due_date": "2019-12-04", "explanation": ["None"]}}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler({}, {})

        self.assertDictEqual(expected_body, actual_response)

        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)
        valid_cease_reason_mock.assert_called_with('because')
        calculator_mock.assert_called_with(participant, [test_result_1, test_result_2], 'because')

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'GET'}),
            call({'log_reference': LogReference.REINSTATE0007}),
            call({'log_reference': LogReference.REINSTATE0008, 'calculator_response': {'result': None, 'status': 'normal', 'next_test_due_date': '2019-12-04', 'explanation': ['None'], 'error': False}}),  
            call({'log_reference': LogReference.REINSTATE0009}),
            call({'log_reference': LogReference.REINSTATE0011})
        ]

        log_mock.assert_has_calls(expected_log_calls)
