import json
from datetime import date, datetime, timezone
from unittest import TestCase
from mock import patch, call, Mock
from ddt import ddt, data, unpack
from common.services.reinstate.reinstate_response import ReinstateResponse
from reinstate_participant.log_references import LogReference


@ddt
class TestPUTReinstateParticipant(TestCase):

    module = 'reinstate_participant.reinstate_participant'

    FIXED_NOW_TIME = datetime(2020, 6, 23, 13, 48, 8, tzinfo=timezone.utc)

    @patch('boto3.resource')
    def setUp(self, *args):
        # Setup lambda module
        global reinstate_participant_module, AuditActions
        import reinstate_participant.reinstate_participant as reinstate_participant_module
        from common.utils.audit_utils import AuditActions
        self.unwrapped_handler = reinstate_participant_module.lambda_handler.__wrapped__.__wrapped__

    def assertResponse(self, status_code, body, actual_response):
        expected_response = {
            'statusCode': status_code,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': json.dumps(body),
            'isBase64Encoded': False
        }
        self.assertDictEqual(expected_response, actual_response)

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_reinstate_participant_returns_bad_request_given_unsupported_http_method(self, request_info_mock, log_mock):
        request_info_mock.return_value = {}, 'POST', ''

        actual_response = self.unwrapped_handler({}, {})

        self.assertResponse(400, {'message': 'Requested method not supported.'}, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'POST'}),
            call({'log_reference': LogReference.REINSTATE0002}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_reinstate_participant_returns_404_given_participant_not_found(
            self, request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'PUT', ''
        get_participant_mock.return_value = []

        actual_response = self.unwrapped_handler({}, {})

        self.assertResponse(404, {'message': 'Participant not found.'}, actual_response)

        get_participant_mock.assert_called_with('123', segregate=True)

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'PUT'}),
            call({'log_reference': LogReference.REINSTATE0003}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.can_participant_be_reinstated')
    def test_reinstate_participant_returns_400_given_participant_cannot_be_reinstated(
            self, can_be_reinstated_mock, request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'PUT', ''
        participant = {'participant_id': '123', 'sort_key': 'PARTICIPANT', 'active': False}
        get_participant_mock.return_value = [participant]
        can_be_reinstated_mock.return_value = False
        expected_body = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Participant cannot be reinstated."}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler({}, {})

        self.assertDictEqual(expected_body, actual_response)

        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'PUT'}),
            call({'log_reference': LogReference.REINSTATE0004}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.can_participant_be_reinstated')
    def test_reinstate_participant_returns_400_given_no_cease_record(
            self, can_be_reinstated_mock, request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'PUT', ''
        participant = {'participant_id': '123', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        get_participant_mock.return_value = [participant]
        can_be_reinstated_mock.return_value = True
        expected_body = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Participant missing cease record."}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler({}, {})

        self.assertDictEqual(expected_body, actual_response)

        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'PUT'}),
            call({'log_reference': LogReference.REINSTATE0005}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.can_participant_be_reinstated')
    @patch(f'{module}.is_valid_cease_reason')
    def test_reinstate_participant_returns_400_given_invalid_cease_reason(
            self, valid_cease_reason_mock, can_be_reinstated_mock,
            request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'PUT', ''
        participant = {'participant_id': '123', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        test_result_1 = {'id': '1', 'sort_key': 'RESULT#2020-05-31'}
        test_result_2 = {'id': '1', 'sort_key': 'RESULT#2017-05-31'}
        cease_record = {'sort_key': 'CEASE#2020-06-01', 'reason': 'because'}
        get_participant_mock.return_value = [cease_record, participant, test_result_1, test_result_2]
        can_be_reinstated_mock.return_value = True
        valid_cease_reason_mock.return_value = False
        expected_body = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Cease reason is not valid."}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler({}, {})

        self.assertDictEqual(expected_body, actual_response)

        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)
        valid_cease_reason_mock.assert_called_with('because')

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'PUT'}),
            call({'log_reference': LogReference.REINSTATE0006}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_session_from_lambda_event')
    @patch(f'{module}.calculate_for_manual_reinstate')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.reinstate_participant')
    @patch(f'{module}.can_participant_be_reinstated')
    @patch(f'{module}.is_valid_cease_reason')
    def test_reinstate_participant_returns_400_when_no_calculator_response(
            self, valid_cease_reason_mock, can_be_reinstated_mock,
            reinstate_mock, request_info_mock, get_participant_mock, calculator_mock, get_session_mock,
            log_mock):
        request_info_mock.return_value = {'path_parameters': {'participant_id': '123'}}, 'PUT', ''
        participant = {'participant_id': '123', 'nhs_number': '1', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        test_results = [{'id': '1', 'sort_key': 'RESULT#2020-05-31'}]
        cease_record = {'sort_key': 'CEASE#2020-06-01', 'reason': 'because'}
        get_participant_mock.return_value = [cease_record, participant] + test_results
        can_be_reinstated_mock.return_value = True
        valid_cease_reason_mock.return_value = True
        calculator_mock.return_value = ReinstateResponse(status=None, next_test_due_date=None, explanation=['None'])

        actual_response = self.unwrapped_handler({}, {})

        self.assertResponse(400, {'message': 'Participant cannot be reinstated.'}, actual_response)
        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)
        valid_cease_reason_mock.assert_called_with('because')
        calculator_mock.assert_called_with(participant, test_results, 'because')

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'PUT'}),
            call({'log_reference': LogReference.REINSTATE0007}),
            call({'log_reference': LogReference.REINSTATE0008, 'calculator_response': {'result': None, 'status': None, 'next_test_due_date': None, 'explanation': ['None'], 'error': False}}),  
            call({'log_reference': LogReference.REINSTATE0012}),
            call({'log_reference': LogReference.REINSTATE0010})
        ]
        log_mock.assert_has_calls(expected_log_calls)

        get_session_mock.assert_called()
        reinstate_mock.assert_not_called()

    @patch(f'{module}.log')
    @patch(f'{module}.get_session_from_lambda_event')
    @patch(f'{module}.calculate_for_manual_reinstate')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.reinstate_participant')
    @patch(f'{module}.can_participant_be_reinstated')
    @patch(f'{module}.is_valid_cease_reason')
    def test_reinstate_participant_returns_400_on_missing_request_params(
            self, valid_cease_reason_mock, can_be_reinstated_mock,
            reinstate_mock, request_info_mock, get_participant_mock, calculator_mock, get_session_mock,
            log_mock):
        request_info_mock.return_value = {
            'path_parameters': {'participant_id': '123'},
            'body': {}
        }, 'PUT', ''
        participant = {'participant_id': '123', 'nhs_number': '1', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        test_results = [{'id': '1', 'sort_key': 'RESULT#2020-05-31'}]
        cease_record = {'sort_key': 'CEASE#2020-06-01', 'reason': 'because'}
        get_participant_mock.return_value = [cease_record, participant] + test_results
        can_be_reinstated_mock.return_value = True
        valid_cease_reason_mock.return_value = True
        calculator_mock.return_value = ReinstateResponse(status='normal',
                                                         next_test_due_date=date(2019, 12, 4),
                                                         explanation=['None'])

        actual_response = self.unwrapped_handler({}, {})

        self.assertResponse(400, {'message': 'Missing mandatory fields from request body.'}, actual_response)
        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)
        valid_cease_reason_mock.assert_called_with('because')
        calculator_mock.assert_called_with(participant, test_results, 'because')

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'PUT'}),
            call({'log_reference': LogReference.REINSTATE0007}),
            call({'log_reference': LogReference.REINSTATE0008, 'calculator_response': {'result': None, 'status': 'normal', 'next_test_due_date': '2019-12-04', 'explanation': ['None'], 'error': False}}),  
            call({'log_reference': LogReference.REINSTATE0012}),
            call({'log_reference': LogReference.REINSTATE0013})
        ]
        log_mock.assert_has_calls(expected_log_calls)

        get_session_mock.assert_called()
        reinstate_mock.assert_not_called()

    @unpack
    @data(
        ('NOT_DEFAULT', 'normal',     '2019-12-04'),
        ('DEFAULT',     'not_normal', '2020-12-04')
    )
    @patch(f'{module}.audit', Mock())
    @patch(f'{module}.log')
    @patch(f'{module}.get_session_from_lambda_event')
    @patch(f'{module}.calculate_for_manual_reinstate')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.reinstate_participant')
    @patch(f'{module}.can_participant_be_reinstated')
    @patch(f'{module}.is_valid_cease_reason')
    def test_reinstate_participant_returns_400_on_mismatched_request_params(
            self,
            reinstate_type, status, next_test_due_date,
            valid_cease_reason_mock, can_be_reinstated_mock,
            reinstate_mock, request_info_mock, get_participant_mock, calculator_mock, get_session_mock,
            log_mock):
        request_info_mock.return_value = {
            'path_parameters': {'participant_id': '123'},
            'body': {
                'reinstate_type': f'{reinstate_type}',
                'status': f'{status}',
                'next_test_due_date': f'{next_test_due_date}'
            }
        }, 'PUT', ''
        participant = {'participant_id': '123', 'nhs_number': '1', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        test_results = [{'id': '1', 'sort_key': 'RESULT#2020-05-31'}]
        cease_record = {'sort_key': 'CEASE#2020-06-01', 'reason': 'because'}
        get_participant_mock.return_value = [cease_record, participant] + test_results
        can_be_reinstated_mock.return_value = True
        valid_cease_reason_mock.return_value = True
        calculator_mock.return_value = ReinstateResponse(status=status,
                                                         next_test_due_date=date(2019, 12, 4),
                                                         explanation=['None'])

        actual_response = self.unwrapped_handler({}, {})

        self.assertResponse(
            400, {'message': 'Supplied reinstate values do not match calculated reinstate values.'}, actual_response)
        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)
        valid_cease_reason_mock.assert_called_with('because')
        calculator_mock.assert_called_with(participant, test_results, 'because')

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'PUT'}),
            call({'log_reference': LogReference.REINSTATE0007}),
            call({'log_reference': LogReference.REINSTATE0008, 'calculator_response': {'result': None, 'status': status, 'next_test_due_date': '2019-12-04', 'explanation': ['None'], 'error': False}}),   
            call({'log_reference': LogReference.REINSTATE0012}),
            call({'log_reference': LogReference.REINSTATE0014})
        ]
        log_mock.assert_has_calls(expected_log_calls)

        get_session_mock.assert_called()
        reinstate_mock.assert_not_called()

    @patch(f'{module}.audit')
    @patch(f'{module}.log')
    @patch(f'{module}.get_session_from_lambda_event')
    @patch(f'{module}.calculate_for_manual_reinstate')
    @patch(f'{module}.get_participant_and_test_results')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.reinstate_participant')
    @patch(f'{module}.can_participant_be_reinstated')
    @patch(f'{module}.is_valid_cease_reason')
    def test_reinstate_participant_returns_calls_through_to_service(
            self, valid_cease_reason_mock, can_be_reinstated_mock,
            reinstate_mock, request_info_mock, get_participant_mock, calculator_mock, get_session_mock,
            log_mock, audit_mock):
        request_info_mock.return_value = {
            'path_parameters': {'participant_id': '123'},
            'body': {
                'reinstate_type': 'DEFAULT',
                'status': 'normal',
                'next_test_due_date': '2019-12-04',
                'crm_number': '12345',
                'comments': 'a comment'
            }
        }, 'PUT', ''
        participant = {'participant_id': '123', 'nhs_number': '1', 'sort_key': 'PARTICIPANT', 'is_ceased': True}
        test_results = [{'id': '1', 'sort_key': 'RESULT#2020-05-31'}]
        cease_record = {'sort_key': 'CEASE#2020-06-01', 'reason': 'because'}
        get_participant_mock.return_value = [cease_record, participant] + test_results
        can_be_reinstated_mock.return_value = True
        valid_cease_reason_mock.return_value = True
        calculator_mock.return_value = ReinstateResponse(status='normal',
                                                         next_test_due_date=date(2019, 12, 4),
                                                         explanation=['None'])
        get_session_mock.return_value = {'user_data': {'nhsid_useruid': '321'}}
        self.unwrapped_handler({}, {})

        get_participant_mock.assert_called_with('123', segregate=True)
        can_be_reinstated_mock.assert_called_with(participant)
        valid_cease_reason_mock.assert_called_with('because')
        calculator_mock.assert_called_with(participant, test_results, 'because')

        expected_log_calls = [
            call({'log_reference': LogReference.REINSTATE0001, 'http_method': 'PUT'}),
            call({'log_reference': LogReference.REINSTATE0007}),
            call({'log_reference': LogReference.REINSTATE0008, 'calculator_response': {'result': None, 'status': 'normal', 'next_test_due_date': '2019-12-04', 'explanation': ['None'], 'error': False}}),  
            call({'log_reference': LogReference.REINSTATE0012}),
            call({'log_reference': LogReference.REINSTATE0015})
        ]
        log_mock.assert_has_calls(expected_log_calls)

        get_session_mock.assert_called()
        reinstate_mock.assert_called_once_with('123', '1', 'normal', '2019-12-04', '321', 'a comment', '12345')
        audit_mock.assert_called_once_with(action=AuditActions.REINSTATE_PARTICIPANT,
                                           session={'user_data': {'nhsid_useruid': '321'}},
                                           participant_ids=['123'],
                                           nhs_numbers=['1'],
                                           additional_information={'updating_next_test_due_date_to': '2019-12-04'})
