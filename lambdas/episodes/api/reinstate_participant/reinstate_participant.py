from common.log import log
from common.utils.audit_utils import audit, AuditActions
from common.utils.next_test_due_date_calculators.reinstate_calculator import calculate_for_manual_reinstate
from common.utils.participant_utils import get_participant_and_test_results
from common.utils.session_utils import get_session_from_lambda_event
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_message, json_return_data, json_return_object
from common.utils.api_utils import get_api_request_info_from_event
from common.models.participant import ParticipantSortKey
from common.services.reinstate.reinstate_service import reinstate_participant
from common.services.reinstate.reinstate_eligibility import can_participant_be_reinstated, is_valid_cease_reason
from common.services.reinstate.reinstate import remove_no_recall_change_result_combinations, filter_by_sort_key
from reinstate_participant.log_references import LogReference

SUPPORTED_METHODS = ['GET', 'PUT']
DEFAULT_REINSTATE = 'DEFAULT'
REJECTED_REINSTATE = 'REJECTED'

REINSTATE_FIELDS_TO_AUDIT = ['crm_number', 'comments']


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):

    event_data, http_method, _ = get_api_request_info_from_event(event)
    log({'log_reference': LogReference.REINSTATE0001, 'http_method': http_method})

    if http_method not in SUPPORTED_METHODS:
        log({'log_reference': LogReference.REINSTATE0002})
        return json_return_message(400, 'Requested method not supported.')

    participant_id = event_data['path_parameters'].get('participant_id')
    participant_and_test_results = get_participant_and_test_results(participant_id, segregate=True)

    if not participant_and_test_results:
        log({'log_reference': LogReference.REINSTATE0003})
        return json_return_message(404, 'Participant not found.')

    participant = filter_by_sort_key(participant_and_test_results, ParticipantSortKey.PARTICIPANT)[0]
    if not can_participant_be_reinstated(participant):
        log({'log_reference': LogReference.REINSTATE0004})
        return json_return_message(400, 'Participant cannot be reinstated.')

    # test results and cease records ordered newest first by participant_utils.get_participant_and_test_results
    test_results = filter_by_sort_key(participant_and_test_results, ParticipantSortKey.RESULT)
    cease_record = filter_by_sort_key(participant_and_test_results, ParticipantSortKey.CEASE)

    test_results = remove_no_recall_change_result_combinations(test_results)
    if not cease_record:
        log({'log_reference': LogReference.REINSTATE0005})
        return json_return_message(400, 'Participant missing cease record.')

    cease_reason = cease_record[0]['reason']
    if not is_valid_cease_reason(cease_reason):
        log({'log_reference': LogReference.REINSTATE0006})
        return json_return_message(400, 'Cease reason is not valid.')

    log({'log_reference': LogReference.REINSTATE0007})
    calculator_response = calculate_for_manual_reinstate(participant, test_results, cease_reason)
    log({'log_reference': LogReference.REINSTATE0008, 'calculator_response': calculator_response.to_data()})

    if http_method == 'GET':
        log({'log_reference': LogReference.REINSTATE0009})
        return _build_get_reinstate_parameters_response(calculator_response)

    if http_method == 'PUT':
        log({'log_reference': LogReference.REINSTATE0012})
        session = get_session_from_lambda_event(event)
        nhs_number = participant['nhs_number']
        return _reinstate_participant(participant_id,
                                      nhs_number,
                                      calculator_response,
                                      event_data,
                                      session)


def _build_get_reinstate_parameters_response(calculator_response):
    if calculator_response.error is True or calculator_response.next_test_due_date is None:
        log({'log_reference': LogReference.REINSTATE0010})
        return json_return_data(400, calculator_response.to_json())
    log({'log_reference': LogReference.REINSTATE0011})
    return json_return_data(200, calculator_response.to_json())


def _reinstate_participant(participant_id, nhs_number, calculator_response, event_data, session):
    if calculator_response.error is True or calculator_response.next_test_due_date is None:
        log({'log_reference': LogReference.REINSTATE0010})
        return json_return_message(400, 'Participant cannot be reinstated.')

    request_body = event_data['body']
    reinstate_type = request_body.get('reinstate_type')
    status = request_body.get('status')
    next_test_due_date = request_body.get('next_test_due_date')
    crm_number = request_body.get('crm_number')
    comments = request_body.get('comments')
    user_id = session['user_data']['nhsid_useruid']

    if not reinstate_type or not status or not next_test_due_date:
        log({'log_reference': LogReference.REINSTATE0013})
        return json_return_message(400, 'Missing mandatory fields from request body.')

    if reinstate_type != DEFAULT_REINSTATE \
            or status != calculator_response.to_data().get('status') \
            or next_test_due_date != calculator_response.to_data().get('next_test_due_date'):
        log({'log_reference': LogReference.REINSTATE0014})
        return json_return_message(400, 'Supplied reinstate values do not match calculated reinstate values.')

    log({'log_reference': LogReference.REINSTATE0015})

    reinstate_record = reinstate_participant(participant_id,
                                             nhs_number,
                                             status,
                                             next_test_due_date,
                                             user_id,
                                             comments,
                                             crm_number)

    additional_information = {k: v for k, v in reinstate_record.items() if k in REINSTATE_FIELDS_TO_AUDIT}
    additional_information.update({'updating_next_test_due_date_to': next_test_due_date})
    audit(
        action=AuditActions.REINSTATE_PARTICIPANT,
        session=session,
        participant_ids=[participant_id],
        nhs_numbers=[nhs_number],
        additional_information=additional_information
    )

    return json_return_object(200)
