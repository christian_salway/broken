import json
from unittest import TestCase
from mock import patch, Mock
from boto3.dynamodb.conditions import Key
from datetime import datetime, timezone

from common.models.participant import ParticipantStatus
from common.utils.data_segregation.nhais_ciphers import Cohorts

dynamodb_resource_mock = Mock()
participants_table_mock = Mock()
sqs_client_mock = Mock()
dynamodb_client_mock = Mock()

example_date = datetime(2020, 10, 18, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

mock_env_vars = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE',
    'AUDIT_QUEUE_URL': 'the_audit_queue_url'
}

with patch('datetime.datetime', datetime_mock):
    from reinstate_participant import reinstate_participant


@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_client',
       Mock(return_value=dynamodb_client_mock))
@patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_resource',
       Mock(return_value=dynamodb_resource_mock))
@patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
       Mock(return_value=[Cohorts.ENGLISH]))
@patch('common.utils.audit_utils._SQS_CLIENT', sqs_client_mock)
@patch('common.utils.next_test_due_date_calculators.next_test_due_date_calculator.datetime', datetime_mock)  
@patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
@patch('os.environ', mock_env_vars)
class TestReinstateParticipant(TestCase):

    mock_participant_record = {
        'participant_id': '123456',
        'sort_key': 'PARTICIPANT',
        'date_of_birth': '1993-03-21',
        'is_ceased': True,
        'nhs_number': 'the_nhs_number'
    }

    mock_participant_record_disallowed_cipher = {
        'participant_id': '135789',
        'sort_key': 'PARTICIPANT',
        'date_of_birth': '1993-03-21',
        'is_ceased': True,
        'nhs_number': 'the_nhs_number',
        'nhais_cipher': 'IM'
    }

    mock_test_results = [
        {
            'participant_id': '123456',
            'sort_key': 'RESULT#2020-05-09',
            'test_date': '2020-05-09',
            'result_code': 'X',
            'infection_code': 'U',
            'action_code': 'H',
            'sender_code': 'SLFSMP',
            'self_sample': True,
        },
        {
            'participant_id': '123456',
            'sort_key': 'RESULT#2017-11-28',
            'test_date': '2017-11-28',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'A'
        }
    ]

    mock_cease_record = {
        'participant_id': '123456',
        'sort_key': 'CEASE#2020-06-13',
        'reason': 'PATIENT_INFORMED_CHOICE'
    }

    mock_items = [mock_cease_record, mock_participant_record] + mock_test_results

    participant_records_projection_expression = 'nhs_number, sort_key, participant_id, title, first_name, ' \
                                                'middle_names, last_name, gender, address, date_of_birth, ' \
                                                'date_of_death, is_ceased, is_invalid_patient, next_test_due_date, ' \
                                                'registered_gp_practice_code, infection_result, ' \
                                                'infection_code, #result, #action, action_code, test_date, ' \
                                                'recall_months, result_code, sender_code, ' \
                                                'source_code, sender_source_type, slide_number, '\
                                                'sending_lab, #status, suppression_reason, '\
                                                'invited_date, reason, date_from, is_fp69, event_text, ' \
                                                'health_authority, crm, active, reason_for_removal_code, ' \
                                                'reason_for_removal_effective_from_date, ' \
                                                'hpv_primary, self_sample, practice_code, nhais_cipher, ' \
                                                'is_paused, is_paused_received_time'

    @classmethod
    def setUpClass(cls, *args):
        cls.log_patcher = patch('reinstate_participant.reinstate_participant.log')
        # Setup database mock
        participants_table_mock.query.return_value = {'Items': cls.mock_items}

    def setUp(self):
        dynamodb_resource_mock.reset_mock()
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        self.log_patcher.start()

        dynamodb_resource_mock.batch_get_item.return_value = {
            'Responses': {
                'PARTICIPANTS_TABLE': [self.mock_participant_record]
            }
        }

    def tearDown(self):
        self.log_patcher.stop()

    def test_GET_reinstate_participant_for_ceased_participant_that_can_be_reinstated(self):
        event = self._build_event("123456", "23456789", "987654321", 'GET', ["cervicalscreening"])
        context = Mock()
        context.function_name = ''

        actual_response = reinstate_participant.lambda_handler(event, context)

        # Assert participant, test results and cease record looked up from DynamoDB
        participants_table_mock.query.assert_called_once()
        _, kwargs = participants_table_mock.query.call_args

        expected_key_condition = Key('participant_id').eq('123456')
        expected_projection_expression = self.participant_records_projection_expression
        expected_expression_attribute_names = {'#result': 'result', '#action': 'action', '#status': 'status'}
        expected_scan_index_forward = False
        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])
        self.assertEqual(expected_projection_expression, kwargs['ProjectionExpression'])
        self.assertEqual(expected_expression_attribute_names, kwargs['ExpressionAttributeNames'])
        self.assertEqual(expected_scan_index_forward, kwargs['ScanIndexForward'])

        # Assert reinstate calculation has worked
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': json.dumps({
                'data': {
                    'status': ParticipantStatus.ROUTINE,
                    'next_test_due_date': '2020-12-28',
                    'explanation': ['ROUTINE_STATUS', 'UNDER_50']
                }
            }),
            'isBase64Encoded': False
        }
        self.assertDictEqual(expected_response, actual_response)

    def test_PUT_reinstate_participant_for_ceased_participant_that_can_be_reinstated(self):
        request_body = '{"reinstate_type": "DEFAULT", "status": "ROUTINE", "next_test_due_date": "2020-12-28", ' \
                       '"crm_number": "CAS-ABCDE-12345", "comments": "a comment"}'
        event = self._build_event("123456", "23456789", "987654321", 'PUT', ["cervicalscreening"], request_body)
        context = Mock()
        context.function_name = ''

        actual_response = reinstate_participant.lambda_handler(event, context)

        # Assert participant, test results and cease record looked up from DynamoDB
        participants_table_mock.query.assert_called_once()
        _, kwargs = participants_table_mock.query.call_args

        expected_key_condition = Key('participant_id').eq('123456')
        expected_projection_expression = self.participant_records_projection_expression
        expected_expression_attribute_names = {'#result': 'result', '#action': 'action', '#status': 'status'}
        expected_scan_index_forward = False

        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])
        self.assertEqual(expected_projection_expression, kwargs['ProjectionExpression'])
        self.assertEqual(expected_expression_attribute_names, kwargs['ExpressionAttributeNames'])
        self.assertEqual(expected_scan_index_forward, kwargs['ScanIndexForward'])

        # Assert participant updated and reinstate record created
        dynamodb_client_mock.transact_write_items.assert_called_once()
        dynamodb_client_mock.transact_write_items.assert_called_with(
            TransactItems=[{
                'Put': {
                    'TableName': 'PARTICIPANTS_TABLE',
                    'Item': {
                        'participant_id': {'S': '123456'},
                        'sort_key': {'S': 'REINSTATE#2020-10-18T13:48:08+00:00'},
                        'date_from': {'S': '2020-10-18T13:48:08+00:00'},
                        'user_id': {'S': '23456789'},
                        'comments': {'S': 'a comment'},
                        'crm_number': {'S': 'CAS-ABCDE-12345'}
                    }
                }
            },
                {
                'Update': {
                    'TableName': 'PARTICIPANTS_TABLE',
                    'Key': {
                        'participant_id': {'S': '123456'},
                        'sort_key': {'S': 'PARTICIPANT'}
                    },
                    'UpdateExpression': 'SET #is_ceased = :is_ceased, #next_test_due_date = :next_test_due_date, #status = :status',   
                    'ExpressionAttributeValues': {
                        ':is_ceased': {'BOOL': False},
                        ':next_test_due_date': {'S': '2020-12-28'},
                        ':status': {'S': 'ROUTINE'}
                    },
                    'ExpressionAttributeNames': {
                        '#is_ceased': 'is_ceased',
                        '#next_test_due_date': 'next_test_due_date',
                        '#status': 'status'
                    }
                }
            }])

        # Assert reinstate is audited
        sqs_client_mock.send_message.assert_called_with(
            QueueUrl='the_audit_queue_url',
            MessageBody='{"action": "REINSTATE_PARTICIPANT", "timestamp": "2020-10-18T13:48:08+00:00", '
                        '"internal_id": "1", "nhsid_useruid": "23456789", "session_id": "987654321", '
                        '"first_name": "firsty", "last_name": "lasty", "role_id": "test_role", '
                        '"user_organisation_code": "test_organisation_code", '
                        '"participant_ids": ["123456"], "nhs_numbers": ["the_nhs_number"], '
                        '"additional_information": {"comments": "a comment", "crm_number": "CAS-ABCDE-12345", '
                        '"updating_next_test_due_date_to": "2020-12-28"}}'
        )

        # Assert lambda response
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(expected_response, actual_response)

    def test_PUT_reinstate_participant_for_ceased_participant_with_disallowed_cipher(self):
        request_body = '{"reinstate_type": "DEFAULT", "status": "ROUTINE", "next_test_due_date": "2020-12-28", ' \
                       '"crm_number": "CAS-ABCDE-12345", "comments": "a comment"}'
        event = self._build_event("135789", "23456789", "987654321", 'PUT', ["cervicalscreening"], request_body)
        context = Mock()
        context.function_name = ''

        participants_table_mock.query.return_value = {'Items': [self.mock_participant_record_disallowed_cipher]}

        actual_response = reinstate_participant.lambda_handler(event, context)

        # Assert participant, test results and cease record looked up from DynamoDB
        expected_key_condition = Key('participant_id').eq('135789')
        expected_projection_expression = self.participant_records_projection_expression
        expected_expression_attribute_names = {'#result': 'result', '#action': 'action', '#status': 'status'}
        expected_scan_index_forward = False

        participants_table_mock.query.assert_called_once_with(
            KeyConditionExpression=expected_key_condition,
            ProjectionExpression=expected_projection_expression,
            ExpressionAttributeNames=expected_expression_attribute_names,
            ScanIndexForward=expected_scan_index_forward
        )
        _, kwargs = participants_table_mock.query.call_args

        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])
        self.assertEqual(expected_projection_expression, kwargs['ProjectionExpression'])
        self.assertEqual(expected_expression_attribute_names, kwargs['ExpressionAttributeNames'])
        self.assertEqual(expected_scan_index_forward, kwargs['ScanIndexForward'])

        # Assert participant update is not called
        participants_table_mock.update_item.assert_not_called()

        # Assert reinstate record not created
        participants_table_mock.put_item.assert_not_called()

        # Assert audit is not called
        sqs_client_mock.send_message.assert_not_called()

        # Assert empty lambda response
        self.assertDictEqual({
            'statusCode': 404,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Participant not found."}',
            'isBase64Encoded': False
        }, actual_response)

    def _build_event(self, participant_id, user_uid, session_id, http_method, workgroups, body=None):
        if not body:
            body = "{}"
        return {
            'httpMethod': http_method,
            'resource': '/api/participants/123456/reinstate',
            'pathParameters': {
                'participant_id': participant_id
            },
            'headers': {
                'request_id': 'requestID',
            },
            'body': body,
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': json.dumps({
                        "session_id": session_id,
                        "user_data": {
                            "nhsid_useruid": user_uid,
                            "first_name": "firsty",
                            "last_name": "lasty"
                        },
                        "selected_role": {
                            "workgroups": workgroups,
                            "role_id": "test_role",
                            "organisation_code": "test_organisation_code"
                        }
                    })
                }
            }
        }
