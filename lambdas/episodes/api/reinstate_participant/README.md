This lambda determines if a participant can be reinstated.

A participant must be ceased in order to be reinstated. They must also meet other criteria - see [reinstate decision tree](https://nhsd-confluence.digital.nhs.uk/display/CSP/Reinstate+decision+tree)

The lambda accepts a GET and a PUT request.

Both endpoints make use of the next_test_due_date_calculator - see [SP-2221](https://nhsd-jira.digital.nhs.uk/browse/SP-2221) for all input and output combinations. 

The GET request is used by the front end to tell CSAS on the reinstate screen if a participant can be reinstated, and if they can what the system will set their status and next test due date to. 
The GET request does NOT reinstate the participant.
If a participant cannot be reinstated it returns this response:
```{"status": null, "explanation": ["NO_PREVIOUS_TEST"]}```
If the participant can be reinstated it returns data in this format with reinstate_type set to DEFAULT:
```{"status": "ROUTINE", "next_test_due_date": "2020-05-01", "explanation": ["CEASED_DUE_TO_AGE", "UNDER_60"]}```
