import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    REINSTATE0001 = (logging.INFO, 'Received request to reinstate participant')
    REINSTATE0002 = (logging.ERROR, 'Requested method not supported.')
    REINSTATE0003 = (logging.WARNING, 'Participant not found.')
    REINSTATE0004 = (logging.WARNING, 'Participant cannot be reinstated.')
    REINSTATE0005 = (logging.WARNING, 'Participant cease record missing.')
    REINSTATE0006 = (logging.WARNING, 'Participant cease reason not valid.')
    REINSTATE0007 = (logging.INFO, 'Calculating status and next test due date.')
    REINSTATE0008 = (logging.INFO, 'Status and next test due date calculated.')
    REINSTATE0009 = (logging.INFO, 'Building response for GET request.')
    REINSTATE0010 = (logging.INFO, 'Calculator determined participant cannot be reinstated.')
    REINSTATE0011 = (logging.INFO, 'Calculator determined status and next test due date. Reinstate type is DEFAULT.')
    REINSTATE0012 = (logging.INFO, 'Request is PUT. Attempting to reinstate participant.')
    REINSTATE0013 = (logging.INFO, 'Mandatory fields missing from request body.')
    REINSTATE0014 = (logging.WARNING, 'Supplied reinstate values do not match calculated reinstate values.')
    REINSTATE0015 = (logging.INFO, 'Participant can be reinstated. Updating participant.')
    REINSTATE0017 = (logging.WARNING, 'Participant is inactive and cannot be reinstated.')
