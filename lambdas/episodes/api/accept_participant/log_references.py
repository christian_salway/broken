import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    ACPTPAR0001 = (logging.INFO, 'Receiving API call for accept PNL participant')
    ACPTPAR0002 = (logging.INFO, 'Checking if participant has live episode')
    ACPTPAR0003 = (logging.INFO, 'Updating episode status to accepted')
    ACPTPAR0004 = (logging.INFO, 'Successfully set episode status to ACCEPTED')

    ACPTPAREX0001 = (logging.ERROR, 'Unable to find live episode')
    ACPTPAREX0002 = (logging.ERROR, 'Unable to find live PNL episode')
    ACPTPAREX0003 = (logging.ERROR, 'Participant has multiple live PNL episodes')
