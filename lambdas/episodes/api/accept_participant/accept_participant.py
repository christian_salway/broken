import os
from datetime import datetime, timezone
from boto3.dynamodb.conditions import Key, Attr
from common.utils.participant_episode_utils import (
    query_for_live_episode,
    update_episode_with_condition_expression
)
from common.utils.session_utils import get_session_from_lambda_event
from common.utils.audit_utils import audit, AuditActions
from common.utils import json_return_message, json_return_object
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from accept_participant.log_references import LogReference
from common.log import log
from common.utils.participant_utils import get_nhs_number_by_participant_id
from common.models.participant import EpisodeStatus

AUDIT_QUEUE_URL = os.environ.get('AUDIT_QUEUE_URL')

EPISODE_SORT_KEY = 'EPISODE#'


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    log({'log_reference': LogReference.ACPTPAR0001})

    participant_id = event['pathParameters']['participant_id']

    log({'log_reference': LogReference.ACPTPAR0002})
    live_episodes = query_for_live_episode(participant_id, segregate=True)

    if not live_episodes:
        log({'log_reference': LogReference.ACPTPAREX0001})
        return json_return_message(404, 'Unable to find episode')

    pnl_episode_count, sort_key = check_participant_has_single_episode_in_pnl_state(live_episodes)

    if pnl_episode_count > 1:
        log({'log_reference': LogReference.ACPTPAREX0003})
        return json_return_message(500, 'Participant has multiple live episodes')

    if pnl_episode_count == 0:
        log({'log_reference': LogReference.ACPTPAREX0002})
        return json_return_message(409, 'Unable to find live PNL episode')

    session = get_session_from_lambda_event(event)

    user_id = session['user_data']['nhsid_useruid']
    update_episode_to_accepted(participant_id, user_id, sort_key)
    raise_audit_for_event(session, participant_id)

    log({'log_reference': LogReference.ACPTPAR0004})
    return json_return_object(200)


def check_participant_has_single_episode_in_pnl_state(live_episodes):
    pnl_episode_count = 0
    first_found_sort_key = ""

    for episode in live_episodes:
        if episode['live_record_status'] == EpisodeStatus.PNL.value:
            first_found_sort_key = episode['sort_key']
            pnl_episode_count = pnl_episode_count + 1

    return pnl_episode_count, first_found_sort_key


def update_episode_to_accepted(participant_id, user_id, sort_key):
    log({'log_reference': LogReference.ACPTPAR0003})

    now_date = datetime.now(timezone.utc).date().isoformat()
    condition_expression = Key('sort_key').begins_with('EPISODE') & Attr('live_record_status').eq(EpisodeStatus.PNL.value) 

    key = {
        'participant_id': participant_id,
        'sort_key': sort_key
    }

    attributes_to_update = {
        'status': EpisodeStatus.ACCEPTED.value,
        'live_record_status': EpisodeStatus.ACCEPTED.value,
        'accepted_date': now_date,
        'user_id': user_id
    }
    update_episode_with_condition_expression(key, attributes_to_update, condition_expression)


def raise_audit_for_event(session, participant_id):
    nhs_number = get_nhs_number_by_participant_id(participant_id, segregate=True)
    audit(session=session,
          action=AuditActions.ACCEPT_EPISODE,
          participant_ids=[participant_id],
          nhs_numbers=[nhs_number])
