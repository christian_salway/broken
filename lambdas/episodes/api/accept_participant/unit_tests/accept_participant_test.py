from unittest import TestCase
from mock import patch, MagicMock, Mock
from common.models.participant import EpisodeStatus


@patch('common.log.logger', MagicMock())
class TestAcceptParticipant(TestCase):

    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    def setUp(self):
        import accept_participant.accept_participant as _accept_participant
        import common.utils.audit_utils as _audit_utils
        self.accept_participant_module = _accept_participant
        self.audit_utils = _audit_utils
        self.log_patcher = patch('common.log.log')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = ''
        self.event = {
            'pathParameters': {'participant_id': '12345678'},
            'requestContext': {'authorizer': {'session': '{"session_id": "12345678"}', 'principalId': 'blah'}},
            'headers': {'request_id': 'blah'}
        }

    def tearDown(self):
        self.log_patcher.stop()

    @patch('accept_participant.accept_participant.audit')
    @patch('accept_participant.accept_participant.get_session_from_lambda_event')
    @patch('accept_participant.accept_participant.query_for_live_episode')
    @patch('accept_participant.accept_participant.update_episode_with_condition_expression')
    @patch('accept_participant.accept_participant.get_nhs_number_by_participant_id')
    def test_lambda_returns_409_if_episode_in_incorrect_state(self,
                                                              get_nhs_number_by_participant_id_mock,
                                                              update_mock, query_mock, session_mock, audit_mock):
        query_mock.return_value = [{'live_record_status': 'bogus'}, {'live_record_status': 'notNice'}]

        response = self.accept_participant_module.lambda_handler(self.event, self.context)

        query_mock.assert_called_with('12345678', segregate=True)
        session_mock.assert_not_called()
        update_mock.assert_not_called()
        audit_mock.assert_not_called()
        get_nhs_number_by_participant_id_mock.assert_not_called()

        self.assertEqual(409, response['statusCode'])

    @patch('accept_participant.accept_participant.audit')
    @patch('accept_participant.accept_participant.get_session_from_lambda_event')
    @patch('accept_participant.accept_participant.query_for_live_episode')
    @patch('accept_participant.accept_participant.update_episode_with_condition_expression')
    @patch('accept_participant.accept_participant.get_nhs_number_by_participant_id')
    def test_lambda_returns_500_if_multiple_live_pnl_episodes(self,
                                                              get_nhs_number_by_participant_id_mock,
                                                              update_mock, query_mock, session_mock, audit_mock):
        query_mock.return_value = [
            {'live_record_status': EpisodeStatus.PNL.value, 'sort_key': '11111111'},
            {'live_record_status': EpisodeStatus.PNL.value, 'sort_key': '22222222'}
        ]

        response = self.accept_participant_module.lambda_handler(self.event, self.context)

        query_mock.assert_called_with('12345678', segregate=True)
        session_mock.assert_not_called()
        update_mock.assert_not_called()
        audit_mock.assert_not_called()
        get_nhs_number_by_participant_id_mock.assert_not_called()

        self.assertEqual(500, response['statusCode'])

    @patch('accept_participant.accept_participant.audit')
    @patch('accept_participant.accept_participant.get_session_from_lambda_event')
    @patch('accept_participant.accept_participant.query_for_live_episode')
    @patch('accept_participant.accept_participant.update_episode_with_condition_expression')
    @patch('accept_participant.accept_participant.get_nhs_number_by_participant_id')
    def test_lambda_returns_404_if_episode_in_incorrect_state(self,
                                                              get_nhs_number_by_participant_id_mock,
                                                              update_mock, query_mock, session_mock, audit_mock):
        query_mock.return_value = []

        response = self.accept_participant_module.lambda_handler(self.event, self.context)

        query_mock.assert_called_with('12345678', segregate=True)
        session_mock.assert_not_called()
        update_mock.assert_not_called()
        audit_mock.assert_not_called()
        get_nhs_number_by_participant_id_mock.assert_not_called()

        self.assertEqual(404, response['statusCode'])

    @patch('accept_participant.accept_participant.audit')
    @patch('accept_participant.accept_participant.get_session_from_lambda_event')
    @patch('accept_participant.accept_participant.query_for_live_episode')
    @patch('accept_participant.accept_participant.update_episode_with_condition_expression')
    @patch('accept_participant.accept_participant.get_nhs_number_by_participant_id')
    def test_lambda_returns_200_if_operation_successful(self,
                                                        get_nhs_number_by_participant_id_mock,
                                                        update_mock, query_mock, session_mock, audit_mock):
        query_mock.return_value = [
            {'live_record_status': EpisodeStatus.PNL.value, 'sort_key': '11111111'},
            {'live_record_status': 'ignore', 'sort_key': '22222222'}
        ]
        session_mock.return_value = {'user_data': {'nhsid_useruid': '23456789'}}
        get_nhs_number_by_participant_id_mock.return_value = 'the_nhs_number'

        response = self.accept_participant_module.lambda_handler(self.event, self.context)

        query_mock.assert_called_with('12345678', segregate=True)
        session_mock.assert_called_with(self.event)

        update_mock.assert_called()
        get_nhs_number_by_participant_id_mock.assert_called_with('12345678', segregate=True)
        audit_mock.assert_called_with(
            session={'user_data': {'nhsid_useruid': '23456789'}},
            action=self.audit_utils.AuditActions.ACCEPT_EPISODE,
            participant_ids=['12345678'],
            nhs_numbers=['the_nhs_number'])

        self.assertEqual(200, response['statusCode'])
