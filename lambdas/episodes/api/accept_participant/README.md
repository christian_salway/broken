Lambda, which updates episode records which are in 'PNL' state to 'ACCEPTED' state. This is triggered by an API event,
which is available on the following path:

```
    /api/participants/{participant_id}/ACCEPTED
```

[JIRA](https://nhsd-jira.digital.nhs.uk/browse/SP-1244)
[Confluence](https://nhsd-confluence.digital.nhs.uk/pages/viewpage.action?spaceKey=CSP&title=10.+Accept+process)
