from unittest import TestCase
import json
from mock import patch, Mock

path = 'cease_participant.cease_participant'


@patch('common.utils.audit_utils.get_sqs_client', Mock())
@patch('common.utils.sqs_utils.get_sqs_client', Mock())
class TestCeaseParticipant(TestCase):

    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    def setUp(self):
        import cease_participant.cease_participant as _cease_participant
        import common.utils.audit_utils as _audit_utils
        import common.utils.cease_utils as _cease_utils
        self.cease_participant_module = _cease_participant
        self.audit_utils = _audit_utils
        self.cease_utils = _cease_utils
        self.log_patcher = patch('common.log.logger').start()
        self.context = Mock()
        self.context.function_name = ''
        self.mock_user_id = {'is_ceased': False, 'participant_id': '123456'}

        self.mock_session = {
            'session_id': '12345678',
            'user_data': {
                'nhsid_useruid': self.mock_user_id
            },
            'selected_role': {
                'organisation_code': 'DXX',
                'organisation_name': 'YORKSHIRE AND THE HUMBER',
                'role_id': 'R8010',
                'role_name': 'CSAS Team Member'
            },
            'access_groups': {
                'pnl': False,
                'sample_taker': False,
                'lab': False,
                'colposcopy': False,
                'csas': True,
                'view_participant': False
            }
        }

        self.mock_event_body = {'reason': 'NO_CERVIX', 'crm_number': 'CAS-12345-ABCDE'}

        self.mock_event = {
            'requestContext': {'authorizer': {
                'session': json.dumps(self.mock_session), 'principalId': 'blah'
            }},
            'headers': {'request_id': 'blah'},
            'pathParameters': {'participant_id': '123456'},
            'body': json.dumps(self.mock_event_body)
        }

    def tearDown(self):
        self.log_patcher.stop()

    @patch(f'{path}.cease_participant_and_send_notification_messages')
    @patch(f'{path}.get_participant_by_participant_id')
    def test_lambda_returns_200_when_no_live_episodes(self, get_participant_mock, cease_mock):
        participant = {'is_ceased': False, 'participant_id': '123456', 'nhs_number': '1', 'next_test_due_date': 'ntdd'}
        get_participant_mock.return_value = participant
        response = self.cease_participant_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(response['statusCode'], 200)
        cease_mock.assert_called_with(participant, self.mock_user_id, self.mock_event_body, segregate=True)

    @patch(f'{path}.cease_participant_and_send_notification_messages')
    @patch(f'{path}.get_participant_by_participant_id')
    def test_lambda_returns_200_when_episode_status_is_anything(self, get_participant_mock, cease_mock):
        participant = {'is_ceased': False, 'participant_id': '123456', 'nhs_number': '1', 'next_test_due_date': 'ntdd'}
        get_participant_mock.return_value = participant
        response = self.cease_participant_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(response['statusCode'], 200)
        cease_mock.assert_called_with(participant, self.mock_user_id, self.mock_event_body, segregate=True)

    @patch(f'{path}.cease_participant_and_send_notification_messages')
    @patch(f'{path}.get_participant_by_participant_id')
    def test_lambda_returns_409_when_participant_already_ceased(self, get_participant_mock, cease_mock):
        get_participant_mock.return_value = {
            'is_ceased': True, 'participant_id': '123456', 'nhs_number': '1'
        }
        response = self.cease_participant_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(409, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Participant already ceased.'}), response['body'])
        cease_mock.assert_not_called()

    @patch(f'{path}.get_participant_by_participant_id')
    def test_lambda_returns_404_when_participant_not_found(self, get_participant_mock):
        get_participant_mock.return_value = {}
        response = self.cease_participant_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(404, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Participant not found.'}), response['body'])

    @patch('cease_participant.cease_participant.get_participant_by_participant_id')
    def test_lambda_returns_400_when_participant_is_not_active(self, get_participant_mock):
        get_participant_mock.return_value = {'active': False}
        response = self.cease_participant_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(response['statusCode'], 400)
        self.assertEqual(response['body'], '{"message": "Participant is inactive and cannot be ceased."}')

    def test_lambda_returns_400_when_cease_reason_not_valid_for_pnl_user(self):
        event_body = {'reason': 'PATIENT_INFORMED_CHOICE'}

        session = {
            'session_id': '01788592-74e3-4a26-b25c-106e2c52b37978',
            'user_data': {
                'nhsid_useruid': 'NHS12345'
            },
            'selected_role': {
                'organisation_code': 'X09',
                'organisation_name': 'NHS CONNECTING FOR HEALTH (CFHS)',
                'role_id': 'R8010',
                'role_name': 'Nurse Access Role'
            },
            'access_groups': {
                'pnl': True,
                'sample_taker': False,
                'lab': False,
                'colposcopy': False,
                'csas': False,
                'view_participant': False
            }
        }

        event = {
            'requestContext': {'authorizer': {
                'session': json.dumps(session), 'principalId': 'blah'
            }},
            'headers': {'request_id': 'blah'},
            'pathParameters': {'participant_id': '123456'},
            'body': json.dumps(event_body)
        }

        response = self.cease_participant_module.lambda_handler(event, self.context)

        self.assertEqual(400, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Cease reason is invalid for user role'}), response['body'])

    def test_lambda_returns_400_when_cease_reason_not_valid(self):
        event_body = {'reason': 'Invalid reason', 'crm_number': 'CAS-12345-ABCDE'}

        event = {
            'requestContext': {'authorizer': {
                'session': json.dumps(self.mock_session), 'principalId': 'blah'
            }},
            'headers': {'request_id': 'blah'},
            'pathParameters': {'participant_id': '123456'},
            'body': json.dumps(event_body)
        }

        response = self.cease_participant_module.lambda_handler(event, self.context)

        self.assertEqual(400, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Cease reason is invalid for user role'}), response['body'])
