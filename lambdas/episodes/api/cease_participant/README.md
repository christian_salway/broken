This lambda removes a participant from the cervical screening process, either via CSAS or PNL.
The cease process is described on Confluence here: https://nhsd-confluence.digital.nhs.uk/display/CSP/2+Cease+process

If the participant has a live episode, the participant is first verified to be in an appropriate state for ceasing, before their episode is updated to remove the live record status. If their episode is in an incorrect state, a 409 is returned to the front-end. If they do not have a live episode, the lambda carries on as normal.

The relevant participant record is updated to remove the NTDD and set the participant to ceased.

A cease record is then created.

Finally, the cease action is audited.

The happy-path response is just a 200 status code with an empty body.