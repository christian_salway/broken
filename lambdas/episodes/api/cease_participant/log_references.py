import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    CEASEPAR0001 = (logging.INFO, 'Received API call for cease participant')
    CEASEPAR0002 = (logging.INFO, 'Participant is ceased')

    CEASEPAREX0002 = (logging.ERROR, 'Participant has already been ceased')
    CEASEPAREX0003 = (logging.ERROR, 'Participant not found')
    CEASEPAREX0004 = (logging.ERROR, 'Cease reason is invalid for user role')
    CEASEPAREX0005 = (logging.ERROR, 'CRM case number was not submitted')
    CEASEPAREX0006 = (logging.ERROR, 'Participant is inactive and cannot be ceased')
