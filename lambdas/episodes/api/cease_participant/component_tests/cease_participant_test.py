import json
from datetime import datetime, timezone
from unittest import TestCase

from boto3.dynamodb.conditions import Key, Attr
from mock import patch, Mock, call

from common.models.participant import EpisodeStatus
from common.models.participant import ParticipantStatus
from common.utils.data_segregation.nhais_ciphers import Cohorts

mock_happy_episode = [
    {
        'sort_key': 'EPISODE#2019-12-02',
        'participant_id': '123456',
        'live_record_status': EpisodeStatus.PNL.value
    }
]
mock_participant_record = {
    'participant_id': '123456',
    'sort_key': 'PARTICIPANT',
    'nhs_number': '9999999999',
    'is_ceased': False,
    'registered_gp_practice_code': 'A60',
    'next_test_due_date': '2020-12-25',
    'nhais_cipher': 'MAN'
}
mock_env_vars = {
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
    'GP_NOTIFICATION_QUEUE_URL': 'GP_NOTIFICATION_QUEUE_URL',
    'NOTIFICATION_QUEUE_URL': 'NOTIFICATION_QUEUE_URL'
}
participants_table_mock = Mock()
sqs_client_mock = Mock()
example_date = datetime(2020, 10, 18, 13, 48, 8, 123456, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

mock_filtered_participants = {mock_participant_record['participant_id']: mock_participant_record['nhais_cipher']}

with patch('datetime.datetime', datetime_mock):
    with patch('common.log.get_internal_id', Mock(return_value='1')):
        with patch('os.environ', mock_env_vars):
            from cease_participant import cease_participant


@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids',
       Mock(return_value=mock_filtered_participants))
@patch('os.environ', mock_env_vars)
class TestCeaseParticipant(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        participants_table_mock.query.return_value = {'Items': mock_happy_episode}
        participants_table_mock.get_item.return_value = {'Item': mock_participant_record}
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    def test_cease_participant_with_valid_participant_id(self):
        # Setup the event and the function invocation
        session = self.get_session()
        event = self.get_event(session)
        context = Mock()
        context.function_name = ''

        cease_participant.lambda_handler(event, context)

        # Asserts that participant has been looked up by participant id
        participants_table_mock.get_item.assert_called_with(
            Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        )

        # Asserts that the episode has been checked for the PNL state
        participants_table_mock.query.assert_called_with(
            ExpressionAttributeNames={'#live_record_status': 'live_record_status',
                                      '#HMR101_form_data': 'HMR101_form_data', '#participant_id': 'participant_id',
                                      '#sort_key': 'sort_key', '#status': 'status', '#user_id': 'user_id',
                                      '#test_date': 'test_date', '#test_due_date': 'test_due_date'},
            FilterExpression=Attr('live_record_status').exists(),
            KeyConditionExpression=Key('participant_id').eq('123456') & Key('sort_key').begins_with('EPISODE#'),
            ProjectionExpression='#live_record_status, #HMR101_form_data, #participant_id, #sort_key, #status, #user_id, #test_date, #test_due_date'  
        )

        # Asserts episode and participant have been updated
        expected_update_calls = [
            call(
                ExpressionAttributeNames={'#status': 'status', '#ceased_date': 'ceased_date', '#user_id': 'user_id', '#live_record_status': 'live_record_status'},  
                ExpressionAttributeValues={':status': EpisodeStatus.CEASED.value, ':ceased_date': '2020-10-18', ':user_id': 'NHS12345'}, 
                Key={'participant_id': '123456', 'sort_key': 'EPISODE#2019-12-02'},
                ReturnValues='NONE',
                UpdateExpression='SET #status = :status, #ceased_date = :ceased_date, #user_id = :user_id REMOVE #live_record_status'),  
            call(
                ExpressionAttributeNames={
                    '#is_ceased': 'is_ceased',
                    '#status': 'status',
                    '#next_test_due_date': 'next_test_due_date'
                },
                ExpressionAttributeValues={':is_ceased': True, ':status': ParticipantStatus.CEASED},
                Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'},
                ReturnValues='NONE',
                UpdateExpression='SET #is_ceased = :is_ceased, #status = :status REMOVE #next_test_due_date')]
        participants_table_mock.update_item.assert_has_calls(expected_update_calls)

        # Asserts the cease record has been uploaded
        expected_record = {
            'participant_id': '123456',
            'sort_key': 'CEASE#2020-10-18',
            'reason': 'NO_CERVIX',
            'user_id': 'NHS12345',
            'date_from': '2020-10-18T13:48:08.123456+00:00',
            'comments': 'a comment',
            'crm_number': 'CAS-12345-ABCDE',
        }
        participants_table_mock.put_item.assert_called_with(Item=expected_record)

        # Asserts the gp notification message, the letter notification message, and the audit messages have been sent
        expected_sqs_calls = [call(MessageBody='{\n    "internal_id": "1",\n    "notification_origin": "CEASED",\n    "participant_id": "123456",\n    "registered_gp_practice_code": "A60"\n}',  
                                   QueueUrl='GP_NOTIFICATION_QUEUE_URL'),
                              call(MessageBody='{\n    "participant_id": "123456",\n    "template": "CXNC",\n    "type": "Cease letter"\n}',  
                                   QueueUrl='NOTIFICATION_QUEUE_URL'),
                              call(MessageBody='{"action": "CEASE_EPISODE", "timestamp": "2020-10-18T13:48:08.123456+00:00", "internal_id": "1", "nhsid_useruid": "NHS12345", "session_id": "12345678", "first_name": null, "last_name": null, "role_id": "R8010", "user_organisation_code": "DXX", "participant_ids": ["123456"], "nhs_numbers": ["9999999999"], "additional_information": {"reason": "NO_CERVIX", "comments": "a comment", "crm_number": "CAS-12345-ABCDE", "updating_next_test_due_date_from": "2020-12-25"}}',  
                                   QueueUrl='AUDIT_QUEUE_URL')]
        sqs_client_mock.send_message.assert_has_calls(expected_sqs_calls)

    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.IOM]))
    def test_cannot_cease_participant_from_different_workgroup(self):
        # Setup the event and the function invocation
        session = self.get_session()
        event = self.get_event(session)
        context = Mock()
        context.function_name = ''

        cease_participant.lambda_handler(event, context)

        # Asserts that participant has been looked up by participant id
        participants_table_mock.get_item.assert_called_with(
            Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        )

        participants_table_mock.query.assert_not_called()

        participants_table_mock.update_item.assert_not_called()

        participants_table_mock.put_item.assert_not_called()

        sqs_client_mock.send_message.assert_not_called()

    def get_session(self):
        return {
            "session_id": "12345678",
            "user_data": {'nhsid_useruid': 'NHS12345'},
            'selected_role': {
                'organisation_code': 'DXX',
                'organisation_name': 'YORKSHIRE AND THE HUMBER',
                'role_id': 'R8010',
                'role_name': 'CSAS Team Member'
            },
            'access_groups': {
                'pnl': False,
                'sample_taker': False,
                'lab': False,
                'colposcopy': False,
                'csas': True,
                'view_participant': False
            }
        }

    def get_event(self, session):
        return {
            'requestContext': {'authorizer': {
                'session': json.dumps(session), 'principalId': 'blah'
            }},
            'headers': {'request_id': 'blah'},
            'pathParameters': {'participant_id': '123456'},
            'body': json.dumps({'reason': 'NO_CERVIX', 'crm_number': 'CAS-12345-ABCDE', 'comments': 'a comment'})
        }
