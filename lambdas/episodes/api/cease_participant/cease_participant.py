import json
from common.utils.audit_utils import audit, AuditActions
from common.utils.participant_utils import get_participant_by_participant_id
from common.utils import json_return_message, json_return_object
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils.session_utils import get_session_from_lambda_event
from cease_participant.log_references import LogReference
from common.log import log
from common.models.participant import CeaseReason
from common.utils.cease_utils import cease_participant_and_send_notification_messages

VALID_PNL_CEASE_REASONS = [
    CeaseReason.NO_CERVIX,
    CeaseReason.RADIOTHERAPY,
    CeaseReason.DUE_TO_AGE
]

VALID_CEASE_REASONS = VALID_PNL_CEASE_REASONS + [
    CeaseReason.PATIENT_INFORMED_CHOICE,
    CeaseReason.MENTAL_CAPACITY_ACT
]

CEASE_FIELDS_TO_AUDIT = ['crm_number', 'comments', 'reason']


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    participant_id = event['pathParameters']['participant_id']
    log({'log_reference': LogReference.CEASEPAR0001})

    session = get_session_from_lambda_event(event)

    access_groups = session['access_groups']
    event_body = json.loads(event['body'])
    cease_reason = event_body['reason']
    user_id = session['user_data']['nhsid_useruid']

    if access_groups['csas']:
        is_valid_cease_reason = cease_reason in VALID_CEASE_REASONS
    else:
        is_valid_cease_reason = cease_reason in VALID_PNL_CEASE_REASONS

    if not is_valid_cease_reason:
        log({'log_reference': LogReference.CEASEPAREX0004,
             'cease_reason': cease_reason, 'role': session['selected_role']})
        return json_return_message(400, 'Cease reason is invalid for user role')

    participant = get_participant_by_participant_id(participant_id, segregate=True)

    if not participant:
        log({'log_reference': LogReference.CEASEPAREX0003})
        return json_return_message(404, 'Participant not found.')

    if not participant.get('active', True):
        log({'log_reference': LogReference.CEASEPAREX0006})
        return json_return_message(400, 'Participant is inactive and cannot be ceased.')

    if participant.get('is_ceased'):
        log({'log_reference': LogReference.CEASEPAREX0002})
        return json_return_message(409, 'Participant already ceased.')

    cease_record = cease_participant_and_send_notification_messages(participant, user_id, event_body, segregate=True)

    log({'log_reference': LogReference.CEASEPAR0002})
    additional_information = {k: v for k, v in cease_record.items() if k in CEASE_FIELDS_TO_AUDIT}
    additional_information.update({'updating_next_test_due_date_from': participant['next_test_due_date']})
    audit(
        session=session,
        action=AuditActions.CEASE_EPISODE,
        participant_ids=[participant_id],
        nhs_numbers=[participant['nhs_number']],
        additional_information=additional_information
    )

    return json_return_object(200, {})
