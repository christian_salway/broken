import json
from datetime import datetime, timedelta, timezone
from dateutil.relativedelta import relativedelta
from common.utils.participant_episode_utils import query_for_live_episode, EpisodeStatus
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_message, json_return_object
from common.utils.participant_utils import (
    update_participant_record, create_replace_record_in_participant_table, get_participant_by_participant_id
)
from common.utils.audit_utils import audit, AuditActions
from common.utils.session_utils import get_session_from_lambda_event
from defer_participant.log_references import LogReference
from common.log import log
from common.models.participant import DeferReason


DEFER_REASONS_PNL = [
    DeferReason.CURRENT_PREGNANCY,
    DeferReason.RECENT_TEST,
    DeferReason.UNDER_COLPOSCOPY,
    DeferReason.UNDER_TREATMENT_RELEVANT_TO_SCREENING,
    DeferReason.DISCHARGE_FROM_COLPOSCOPY
]

DEFER_REASONS_CSAS = DEFER_REASONS_PNL + [
    DeferReason.PATIENT_INFORMED_CHOICE,
    DeferReason.ADMINISTRATIVE
]

DEFER_FIELDS_TO_AUDIT = ['crm_number', 'comments', 'reason']


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    log({'log_reference': LogReference.DEFERPART0001})

    participant_id = event['pathParameters']['participant_id']

    try:
        request_body = json.loads(event['body'])
    except Exception:
        log({'log_reference': LogReference.DEFERPARTEX0001})
        return json_return_message(400, 'Missing request body')

    next_test_due_date = request_body.get('next_test_due_date')
    defer_reason = request_body.get('defer_reason')
    participant = get_participant_by_participant_id(participant_id, segregate=True)
    if not participant:
        return json_return_message(404, 'Participant not found.')

    crm_number = request_body.get('crm_number')
    comments = request_body.get('comments')

    date_of_birth = participant.get('date_of_birth')
    is_ceased = participant.get('is_ceased', False)

    if not participant.get('active', True):
        log({'log_reference': LogReference.DEFERPARTEX0006})
        return json_return_message(400, 'Participant is inactive and cannot be deferred.')

    if is_ceased:
        log({'log_reference': LogReference.DEFERPARTEX0004})
        return json_return_message(400, LogReference.DEFERPARTEX0004.message)

    if not next_test_due_date or not defer_reason:
        log({'log_reference': LogReference.DEFERPARTEX0001})
        return json_return_message(400, 'Missing mandatory fields from request body')

    session = get_session_from_lambda_event(event)

    if not is_valid_defer_reason(defer_reason, session['access_groups']):
        log({'log_reference': LogReference.DEFERPARTEX0003})
        return json_return_message(400, 'Invalid defer reason')

    if not is_valid_next_test_due_date(next_test_due_date, defer_reason, date_of_birth):
        log({'log_reference': LogReference.DEFERPARTEX0002})
        return json_return_message(400, 'Invalid next test due date')

    log({'log_reference': LogReference.DEFERPART0002})
    live_episodes = query_for_live_episode(participant_id, segregate=True)

    nhsid_useruid = session['user_data']['nhsid_useruid']

    episode = None
    for episode in live_episodes:
        log({'log_reference': LogReference.DEFERPART0003})
        defer_key = {
            'participant_id': episode['participant_id'],
            'sort_key': episode['sort_key']
        }
        defer_episode_record_today(defer_key, nhsid_useruid)
        log({'log_reference': LogReference.DEFERPART0004, 'participant_id': participant_id,
             'previous_episode_status': episode['live_record_status']})

    log({'log_reference': LogReference.DEFERPART0005})
    participant_key = {
        'participant_id': participant_id,
        'sort_key': 'PARTICIPANT',
    }
    update_participant_record(participant_key, update_attributes={'next_test_due_date': next_test_due_date},
                              segregate=True)
    log({'log_reference': LogReference.DEFERPART0006})

    log({'log_reference': LogReference.DEFERPART0007})
    previous_next_test_due_date = participant['next_test_due_date']
    defer_record = create_defer_record(
        participant_id,
        defer_reason,
        previous_next_test_due_date,
        next_test_due_date,
        nhsid_useruid,
        crm_number,
        comments
    )
    log({'log_reference': LogReference.DEFERPART0008})

    log({'log_reference': LogReference.DEFERPART0009})
    additional_information = {k: v for k, v in defer_record.items() if k in DEFER_FIELDS_TO_AUDIT}
    additional_information.update({
        'updating_next_test_due_date_from': previous_next_test_due_date,
        'updating_next_test_due_date_to': next_test_due_date,
    })
    audit(
        session=session,
        action=AuditActions.DEFER_PARTICIPANT,
        participant_ids=[participant_id],
        nhs_numbers=[participant['nhs_number']],
        additional_information=additional_information
    )
    log({'log_reference': LogReference.DEFERPART0010})

    return json_return_object(200)


def is_valid_next_test_due_date(next_test_due_date, defer_reason, date_of_birth):
    try:
        today = get_today()
        dob = datetime.strptime(date_of_birth, '%Y-%m-%d').date()
        age = today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day))
        converted_date = datetime.strptime(next_test_due_date, '%Y-%m-%d').date()
        max_defer_months = get_max_defer_months(next_test_due_date, defer_reason, age)
        max_ntdd = today + relativedelta(months=max_defer_months)
        in_ten_weeks = today + timedelta(weeks=10)
        return converted_date <= max_ntdd and converted_date > in_ten_weeks
    except ValueError:
        return False


def get_today():
    return datetime.now(timezone.utc).date()


def get_max_defer_months(next_test_due_date, defer_reason, participant_age):
    if defer_reason in [
        DeferReason.CURRENT_PREGNANCY,
        DeferReason.UNDER_COLPOSCOPY,
        DeferReason.UNDER_TREATMENT_RELEVANT_TO_SCREENING
    ]:
        return 12
    if defer_reason == DeferReason.PATIENT_INFORMED_CHOICE:
        return 18
    if defer_reason == DeferReason.RECENT_TEST:
        return 3
    if defer_reason == DeferReason.DISCHARGE_FROM_COLPOSCOPY:
        return 36
    if defer_reason == DeferReason.ADMINISTRATIVE:
        if participant_age < 50:
            return 36
        return 60
    return None


def is_valid_defer_reason(defer_reason, access_groups):
    if access_groups['csas']:
        return defer_reason in DEFER_REASONS_CSAS

    return defer_reason in DEFER_REASONS_PNL


def defer_episode_record_today(key, nhsid_useruid):
    attributes_to_update = {
        'status': EpisodeStatus.DEFERRED.value,
        'deferred_date': datetime.now(timezone.utc).date().isoformat(),
        'user_id': nhsid_useruid
    }
    attributes_to_remove = ['live_record_status']
    update_participant_record(key, update_attributes=attributes_to_update, delete_attributes=attributes_to_remove,
                              segregate=True)


def create_defer_record(participant_id, defer_reason, previous_test_due_date, next_test_due_date, user_id,
                        crm_number, comments):
    today_date = datetime.now(timezone.utc)
    defer_record = {
        'participant_id': participant_id,
        'sort_key': f'DEFER#{today_date.date().isoformat()}',
        'date_from': today_date.isoformat(),
        'reason': defer_reason,
        'previous_test_due_date': previous_test_due_date,
        'new_test_due_date': next_test_due_date,
        'user_id': user_id,
    }
    if crm_number:
        defer_record['crm_number'] = crm_number
    if comments:
        defer_record['comments'] = comments

    create_replace_record_in_participant_table(defer_record, segregate=True)
    return defer_record
