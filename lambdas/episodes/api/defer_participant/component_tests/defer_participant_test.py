import json
from datetime import datetime, timezone
from unittest import TestCase
from boto3.dynamodb.conditions import Key, Attr
from unittest.mock import patch, Mock
from common.models.participant import EpisodeStatus
from common.utils.data_segregation.nhais_ciphers import Cohorts

participants_table_mock = Mock()
sqs_client_mock = Mock()
internal_id_mock = 'blah'


@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'participants-table', 'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL'})
class TestDeferParticipant(TestCase):

    FIXED_NOW_TIME = datetime(2020, 11, 11, 11, 11, 11, tzinfo=timezone.utc)

    mock_participant_id = '01788592-74e3-4a26-b25c-106e2c52b379'
    mock_sort_key = 'EPISODE#2020-04-29'
    mock_live_episodes = [{
        'participant_id':               mock_participant_id,
        'sort_key':                     mock_sort_key,
        'created':                      '2020-01-23T13:48:08.000+00:00',
        'test_due_date':                '2020-04-29',
        'registered_gp_practice_code':  'ABC1234',
        'user_id':                      'System_action',
        'status':                       EpisodeStatus.PNL.value,
        'live_record_status':           EpisodeStatus.PNL.value,
        'pnl_date':                     '2020-01-23'
    }]

    mock_participant_record = {
        'participant_id': mock_participant_id,
        'nhs_number': '9999999999',
        'sort_key': 'PARTICIPANT',
        'date_of_birth': '1980-04-01',
        'next_test_due_date': '2019-12-25',
        'nhais_cipher': 'MAN'
    }

    mock_filtered_participants = {mock_participant_id: 'MAN'}

    @classmethod
    @patch('common.log.get_internal_id', Mock(return_value=internal_id_mock))
    def setUpClass(cls, *_):
        cls.log_patcher = patch('common.log.log')

        # Setup database mocks
        participants_table_mock.get_item.return_value = {'Item': cls.mock_participant_record}
        participants_table_mock.query.return_value = {'Items': cls.mock_live_episodes}

        # Setup lambda module
        import defer_participant.defer_participant as _defer_participant
        global defer_participant_module
        defer_participant_module = _defer_participant

        import common.utils.audit_utils as _audit
        global audit_module
        audit_module = _audit
        cls.defer_date_time_patcher = patch.object(defer_participant_module, 'datetime', Mock(wraps=datetime))
        cls.audit_date_time_patcher = patch.object(audit_module, 'datetime', Mock(wraps=datetime))

    def setUp(self):
        self.maxDiff = None
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        self.log_patcher.start()
        self.defer_date_time_patcher.start()
        self.audit_date_time_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()
        self.defer_date_time_patcher.stop()
        self.audit_date_time_patcher.stop()

    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    @patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids',
           Mock(return_value=mock_filtered_participants))
    def test_defer_participant_with_valid_participant_id(self):
        # Setup the event and the function invocation
        defer_participant_module.datetime.now.return_value = self.FIXED_NOW_TIME
        audit_module.datetime.now.return_value = self.FIXED_NOW_TIME

        session = self.get_session()
        event = self.get_event(session)
        context = Mock()
        context.function_name = ''
        response = defer_participant_module.lambda_handler(event, context)

        # Get dynamodb call references
        get_item_calls = participants_table_mock.get_item.call_args_list
        query_calls = participants_table_mock.query.call_args_list
        update_item_calls = participants_table_mock.update_item.call_args_list
        put_item_calls = participants_table_mock.put_item.call_args_list

        # Assert that the get_participant_by_participant_id contains the correct fields
        get_participant_by_participant_id_call = get_item_calls[0]
        _, kwargs = get_participant_by_participant_id_call
        self.assertDictEqual(
            kwargs, {'Key': {'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379', 'sort_key': 'PARTICIPANT'}})

        # Assert that the query_for_live_episodes contains the correct fields
        query_for_live_episode_call = query_calls[0]
        keys = [
            'live_record_status', 'HMR101_form_data', 'participant_id', 'sort_key', 'status', 'user_id', 'test_date', 'test_due_date']  
        _, query_for_live_episode_call_kwargs = query_for_live_episode_call
        self.assertDictEqual(query_for_live_episode_call_kwargs, {
            'KeyConditionExpression':
                Key('participant_id').eq(self.mock_participant_id) &
                Key('sort_key').begins_with('EPISODE#'),
            'FilterExpression': Attr('live_record_status').exists(),
            'ProjectionExpression': ', '.join([f'#{key}' for key in keys]),
            'ExpressionAttributeNames': {f'#{key}': key for key in keys}
        })

        # Assert that the defer episode record contains the correct fields
        defer_episode_record_calls = update_item_calls[0]
        deferred_time = self.FIXED_NOW_TIME.date().isoformat()
        _, defer_episode_record_calls_kwargs = defer_episode_record_calls
        self.assertDictEqual(defer_episode_record_calls_kwargs, {
            'Key': {'participant_id': self.mock_participant_id, 'sort_key': self.mock_sort_key},
            'UpdateExpression': 'SET #status = :status, #deferred_date = :deferred_date, #user_id = :user_id REMOVE #live_record_status',  
            'ExpressionAttributeNames': {
                '#deferred_date': 'deferred_date',
                '#live_record_status': 'live_record_status',
                '#status': 'status',
                '#user_id': 'user_id'
            },
            'ExpressionAttributeValues': {
                ':deferred_date': deferred_time,
                ':status': EpisodeStatus.DEFERRED.value,
                ':user_id': 'NHS12345'
            },
            'ReturnValues': 'NONE'
        })

        # Assert that the update participant record contains the correct fields
        update_participant_record_calls = update_item_calls[1]
        _, update_participant_record_calls_kwargs = update_participant_record_calls
        self.assertDictEqual(update_participant_record_calls_kwargs, {
            'Key': {'participant_id': self.mock_participant_id, 'sort_key': 'PARTICIPANT'},
            'UpdateExpression': 'SET #next_test_due_date = :next_test_due_date',
            'ExpressionAttributeValues': {':next_test_due_date': '2021-02-04'},
            'ExpressionAttributeNames': {'#next_test_due_date': 'next_test_due_date'},
            'ReturnValues': 'NONE'
        })

        # Assert the create defer record contains the correct fields
        create_defer_record_calls = put_item_calls[0]
        _, create_defer_record_calls_kwargs = create_defer_record_calls
        self.assertDictEqual(create_defer_record_calls_kwargs, {
            'Item': {
                'crm_number': 'CAS-1234-ABCDE',
                'date_from': '2020-11-11T11:11:11+00:00',
                'new_test_due_date': '2021-02-04',
                'participant_id': self.mock_participant_id,
                'previous_test_due_date': '2019-12-25',
                'reason': 'CURRENT_PREGNANCY',
                'sort_key': 'DEFER#2020-11-11',
                'user_id': 'NHS12345'
            }
        })

        # Asserts the audit record has been sent to the audit queue once
        sqs_client_mock.send_message.assert_called_once()
        _, kwargs = sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertDictEqual({
            'action': 'DEFER_PARTICIPANT',
            'additional_information': {
                'crm_number': 'CAS-1234-ABCDE',
                'reason': 'CURRENT_PREGNANCY',
                'updating_next_test_due_date_from': '2019-12-25',
                'updating_next_test_due_date_to': '2021-02-04'
            },
            'first_name': 'firsty',
            'last_name': 'lasty',
            'internal_id': internal_id_mock,
            'nhsid_useruid': 'NHS12345',
            'session_id': '12345678',
            'participant_ids': [self.mock_participant_id],
            'nhs_numbers': ['9999999999'],
            'role_id': 'R8010',
            'user_organisation_code': 'DXX',
            'timestamp': self.FIXED_NOW_TIME.isoformat()
        }, actual_audit_record_sent)
        self.assertEqual('AUDIT_QUEUE_URL', kwargs.get('QueueUrl'))

        # Final assert to make sure the lambda response status code is 200
        self.assertEqual(response['statusCode'], 200)

    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.IOM]))
    @patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids',
           Mock(return_value=mock_filtered_participants))
    def test_cannot_defer_participant_from_different_workgroup(self):
        # Setup the event and the function invocation
        defer_participant_module.datetime.now.return_value = self.FIXED_NOW_TIME
        audit_module.datetime.now.return_value = self.FIXED_NOW_TIME

        session = self.get_session()
        event = self.get_event(session)
        context = Mock()
        context.function_name = ''
        response = defer_participant_module.lambda_handler(event, context)

        # Get dynamodb call references
        get_item_calls = participants_table_mock.get_item.call_args_list

        # Assert that the get_participant_by_participant_id contains the correct fields
        get_participant_by_participant_id_call = get_item_calls[0]
        _, kwargs = get_participant_by_participant_id_call
        self.assertDictEqual(
            kwargs, {'Key': {'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379', 'sort_key': 'PARTICIPANT'}})

        participants_table_mock.query.assert_not_called()
        participants_table_mock.update_item.assert_not_called()
        participants_table_mock.put_item.assert_not_called()

        sqs_client_mock.send_message.assert_not_called()

        self.assertEqual(response['statusCode'], 404)
        self.assertEqual(response['body'], '{"message": "Participant not found."}')

    def get_session(self):
        return {
            "session_id": "12345678",
            "user_data": {'nhsid_useruid': 'NHS12345', 'first_name': 'firsty', 'last_name': 'lasty'},
            'selected_role': {
                'organisation_code': 'DXX',
                'organisation_name': 'YORKSHIRE AND THE HUMBER',
                'role_id': 'R8010',
                'role_name': 'CSAS Team Member'
            },
            'access_groups': {
                'pnl': False,
                'sample_taker': False,
                'lab': False,
                'colposcopy': False,
                'csas': True,
                'view_participant': False
            }
        }

    def get_event(self, session):
        return {
            'requestContext': {'authorizer': {
                'session': json.dumps(session), 'principalId': 'fake_id'
            }},
            'headers': {'request_id': internal_id_mock},
            'pathParameters': {'participant_id': self.mock_participant_id},
            'body': json.dumps({
                'defer_reason': 'CURRENT_PREGNANCY',
                'next_test_due_date': '2021-02-04',
                'crm_number': 'CAS-1234-ABCDE',
            })
        }
