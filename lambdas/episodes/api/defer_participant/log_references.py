import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    DEFERPART0001 = (logging.INFO, 'Received API request to defer PNL participant')
    DEFERPART0002 = (logging.INFO, 'Checking if participant has live episode')
    DEFERPART0003 = (logging.INFO, 'Updating episode status to deferred')
    DEFERPART0004 = (logging.INFO, 'Successfully set episode status to DEFERRED')
    DEFERPART0005 = (logging.INFO, 'Updating participant next test due date')
    DEFERPART0006 = (logging.INFO, 'Successfully updated participant next test due date')
    DEFERPART0007 = (logging.INFO, 'Creating DEFER record for participant')
    DEFERPART0008 = (logging.INFO, 'Successfully created DEFER record for participant')
    DEFERPART0009 = (logging.INFO, 'Creating audit record')
    DEFERPART0010 = (logging.INFO, 'Successfully created audit record')

    DEFERPARTEX0001 = (logging.ERROR, 'Missing required field in request body')
    DEFERPARTEX0002 = (logging.ERROR, 'Invalid next test due date supplied')
    DEFERPARTEX0003 = (logging.ERROR, 'Defer reason is invalid for user role')
    DEFERPARTEX0004 = (logging.ERROR, 'You cannot defer a ceased participant')
    DEFERPARTEX0006 = (logging.ERROR, 'You cannot defer an inactive participant')
