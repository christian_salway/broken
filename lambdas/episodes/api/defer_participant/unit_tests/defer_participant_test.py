import json
from botocore.exceptions import ClientError
from datetime import datetime, timezone
from mock import patch, Mock, call
from ddt import ddt, data

from common.utils import audit_utils
from defer_participant.log_references import LogReference
from common.test_mocks.test_case import PatchTestCase
from common.models.participant import EpisodeStatus

from defer_participant.defer_participant import lambda_handler

module = 'defer_participant.defer_participant'
FIXED_NOW_TIME = datetime(2020, 11, 11, 11, 11, 11, 11, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = FIXED_NOW_TIME


@ddt
@patch(f'{module}.datetime', datetime_mock)
class TestDeferParticipant(PatchTestCase):
    def setUp(self):
        self.create_patchers(module, [
            'log', 'create_replace_record_in_participant_table', 'audit',
            'update_participant_record', 'defer_episode_record_today',
            ['query_for_live_episode', [self.get_valid_episode_record()]],
            ['get_participant_by_participant_id', self.get_participant()],
            ['get_today', FIXED_NOW_TIME.date()],
        ])

        self.mock_session = {
            'session_id': '01788592-74e3-4a26-b25c-106e2c52b37978',
            'user_data': {
                'nhsid_useruid': 'NHS12345'
            },
            'selected_role': {
                'organisation_code': 'DXX',
                'organisation_name': 'YORKSHIRE AND THE HUMBER',
                'role_id': 'R8010',
                'role_name': 'CSAS Team Member'
            },
            'access_groups': {
                'pnl': False,
                'sample_taker': False,
                'lab': False,
                'colposcopy': False,
                'csas': True,
                'view_participant': False
            }
        }
        self.context = {}
        self.event = {
            'requestContext': {'authorizer': {
                'session': json.dumps(self.mock_session), 'principalId': 'some-id'
            }},
            'headers': {'request_id': 'blah'},
            'pathParameters': {'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379'}
        }
        self.setBody({
            'defer_reason': 'CURRENT_PREGNANCY',
            'next_test_due_date': '2021-06-06',
            'crm_number': 'CAS-12345-ABCDE',
            'comments': 'comment comment comment'
        })

        self.expected_create_replace_record = {
            'participant_id': self.get_participant()['participant_id'],
            'sort_key': 'DEFER#2020-11-11',
            'date_from': '2020-11-11T11:11:11.000011+00:00',
            'reason': 'CURRENT_PREGNANCY',
            'comments': 'comment comment comment',
            'crm_number': 'CAS-12345-ABCDE',
            'previous_test_due_date': '2019-12-25',
            'new_test_due_date': '2021-06-06',
            'user_id': 'NHS12345',
        }

        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    def setBody(self, body):
        self.event.update({'body': json.dumps(body)})

    def act(self):
        return self.unwrapped_handler(self.event, self.context)

    def tearDown(self):
        patch.stopall()

    def get_valid_episode_record(self):
        return {
            'participant_id':               '01788592-74e3-4a26-b25c-106e2c52b379',
            'sort_key':                     'EPISODE#2020-04-29',
            'created':                      '2020-01-23T13:48:08.000+00:00',
            'test_due_date':                '2020-04-29',
            'registered_gp_practice_code':  'ABC1234',
            'user_id':                      'System_action',
            'status':                       EpisodeStatus.PNL.value,
            'live_record_status':           EpisodeStatus.PNL.value,
            'pnl_date':                     '2020-01-23'
        }

    def get_participant(self):
        return {
            'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379',
            'nhs_number': '9999999999',
            'date_of_birth': '1970-06-04',
            'next_test_due_date': '2019-12-25',
            'nhais_cipher': 'MAN'
        }

    def assertBody(self, response, body):
        self.assertDictEqual(body, json.loads(response['body']))

    def test_lambda_happy_path(self):
        # Arrange & Act
        response = self.act()
        # Assert
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.DEFERPART0001}),
            call({'log_reference': LogReference.DEFERPART0002}),
            call({'log_reference': LogReference.DEFERPART0003}),
            call({'log_reference': LogReference.DEFERPART0004,
                  'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379', 'previous_episode_status': EpisodeStatus.PNL.value}), 
            call({'log_reference': LogReference.DEFERPART0005}),
            call({'log_reference': LogReference.DEFERPART0006}),
            call({'log_reference': LogReference.DEFERPART0007}),
            call({'log_reference': LogReference.DEFERPART0008}),
            call({'log_reference': LogReference.DEFERPART0009}),
            call({'log_reference': LogReference.DEFERPART0010})])
        self.query_for_live_episode_patcher.assert_has_calls(
            [call('01788592-74e3-4a26-b25c-106e2c52b379', segregate=True)])
        self.defer_episode_record_today_patcher.assert_has_calls([
            call({'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379', 'sort_key': 'EPISODE#2020-04-29'},
                 'NHS12345')])
        self.update_participant_record_patcher.assert_has_calls([
            call({'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379',
                  'sort_key': 'PARTICIPANT'},
                 update_attributes={'next_test_due_date': '2021-06-06'}, segregate=True)])
        self.audit_patcher.assert_called_with(
            session=self.mock_session,
            action=audit_utils.AuditActions.DEFER_PARTICIPANT,
            participant_ids=['01788592-74e3-4a26-b25c-106e2c52b379'],
            nhs_numbers=['9999999999'],
            additional_information={
                'reason': 'CURRENT_PREGNANCY',
                'crm_number': 'CAS-12345-ABCDE',
                'comments': 'comment comment comment',
                'updating_next_test_due_date_from': '2019-12-25',
                'updating_next_test_due_date_to': '2021-06-06'
            }
        )

        self.create_replace_record_in_participant_table_patcher.assert_called_once_with(
            self.expected_create_replace_record, segregate=True)
        self.assertEqual(200, response['statusCode'])

    def test_lambda_happy_path_with_multiple_live_episodes(self):
        # Arrange & Act
        participant_id = self.get_participant()['participant_id']
        self.query_for_live_episode_patcher.return_value = [
            {'participant_id': participant_id, 'sort_key': 'EPISODE#1', 'live_record_status': EpisodeStatus.WALKIN.value}, 
            {'participant_id': participant_id, 'sort_key': 'EPISODE#2', 'live_record_status': EpisodeStatus.ACCEPTED.value} 
        ]
        response = self.act()
        # Assert
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.DEFERPART0001}),
            call({'log_reference': LogReference.DEFERPART0002}),
            call({'log_reference': LogReference.DEFERPART0003}),
            call({'log_reference': LogReference.DEFERPART0004,
                  'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379', 'previous_episode_status': EpisodeStatus.WALKIN.value}), 
            call({'log_reference': LogReference.DEFERPART0003}),
            call({'log_reference': LogReference.DEFERPART0004,
                  'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379', 'previous_episode_status': EpisodeStatus.ACCEPTED.value}), 
            call({'log_reference': LogReference.DEFERPART0005}),
            call({'log_reference': LogReference.DEFERPART0006}),
            call({'log_reference': LogReference.DEFERPART0007}),
            call({'log_reference': LogReference.DEFERPART0008}),
            call({'log_reference': LogReference.DEFERPART0009}),
            call({'log_reference': LogReference.DEFERPART0010})])
        self.query_for_live_episode_patcher.assert_has_calls(
            [call('01788592-74e3-4a26-b25c-106e2c52b379', segregate=True)])
        self.defer_episode_record_today_patcher.assert_has_calls([
            call({'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379', 'sort_key': 'EPISODE#1'},
                 'NHS12345'),
            call({'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379', 'sort_key': 'EPISODE#2'},
                 'NHS12345')
        ])
        self.update_participant_record_patcher.assert_has_calls([
            call({'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379',
                  'sort_key': 'PARTICIPANT'},
                 update_attributes={'next_test_due_date': '2021-06-06'}, segregate=True)])
        self.audit_patcher.assert_called_with(
            session=self.mock_session,
            action=audit_utils.AuditActions.DEFER_PARTICIPANT,
            participant_ids=['01788592-74e3-4a26-b25c-106e2c52b379'],
            nhs_numbers=['9999999999'],
            additional_information={
                'reason': 'CURRENT_PREGNANCY',
                'crm_number': 'CAS-12345-ABCDE',
                'comments': 'comment comment comment',
                'updating_next_test_due_date_from': '2019-12-25',
                'updating_next_test_due_date_to': '2021-06-06'
            }
        )
        self.assertEqual(200, response['statusCode'])
        self.create_replace_record_in_participant_table_patcher.assert_called_once_with(
            self.expected_create_replace_record, segregate=True)

    def test_lambda_returns_400_given_request_body_missing_from_event(self):
        # Arrange
        self.event.update({'body': ''})
        # Act
        response = self.act()
        # Assert
        self.assertEqual(400, response['statusCode'])
        self.assertBody(response, {'message': 'Missing request body'})

    def test_lambda_returns_400_given_next_test_due_date_missing_from_request_body(self):
        # Arrange
        self.setBody({'defer_reason': 'Some reason'})
        # Act
        response = self.act()
        # Assert
        self.assertEqual(400, response['statusCode'])
        self.assertBody(response, {'message': 'Missing mandatory fields from request body'})

    def test_lambda_returns_400_given_defer_reason_missing_from_request_body(self):
        # Arrange
        self.setBody({'next_test_due_date': 'Some data'})
        # Act
        response = self.act()
        # Assert
        self.assertEqual(400, response['statusCode'])
        self.assertBody(response, {'message': 'Missing mandatory fields from request body'})

    @data('   ', '\t', '\n', '\t\n')
    def test_lambda_returns_400_given_defer_reason_is_invalid(self, defer_reason):
        # Arrange
        self.setBody({'defer_reason': defer_reason, 'next_test_due_date': '2021-06-06'})
        # Act
        response = self.act()
        # Assert
        self.assertEqual(400, response['statusCode'])
        self.assertBody(response, {'message': 'Invalid defer reason'})

    def test_lambda_returns_400_given_defer_reason_not_valid_for_pnl_user(self):
        self.setBody({
            'defer_reason': 'PATIENT_INFORMED_CHOICE',  # reason valid for CSAS but not for PNL users
            'next_test_due_date': '2020-02-10',
            'crm_number': 'CAS-12345-ABCDE',
            'comments': 'comment comment comment',
        })
        session = {
            'session_id': '01788592-74e3-4a26-b25c-106e2c52b37978',
            'user_data': {
                'nhsid_useruid': 'NHS12345'
            },
            'selected_role': {
                'organisation_code': 'X09',
                'organisation_name': 'NHS CONNECTING FOR HEALTH (CFHS)',
                'role_id': 'R8010',
                'role_name': 'Nurse Access Role'
            },
            'access_groups': {
                'pnl': True,
                'sample_taker': False,
                'lab': False,
                'colposcopy': False,
                'csas': False,
                'view_participant': False
            }
        }
        self.event['requestContext']['authorizer']['session'] = json.dumps(session)
        response = self.act()
        self.assertEqual(response['statusCode'], 400)
        self.assertEqual(response['body'], json.dumps({'message': 'Invalid defer reason'}))

    def test_lambda_returns_400_given_defer_reason_not_valid_for_csas_user(self):
        self.setBody({
            'defer_reason': 'Invalid reason',
            'next_test_due_date': '2020-02-10'
        })
        response = self.act()
        self.assertEqual(response['statusCode'], 400)
        self.assertEqual(response['body'], json.dumps({'message': 'Invalid defer reason'}))

    @data('invalid', '1999-12-01', '2020-01-01')
    def test_lambda_returns_400_given_next_test_due_date_is_invalid(self, next_test_due_date):
        # Arrange
        self.setBody({'defer_reason': 'CURRENT_PREGNANCY', 'next_test_due_date': next_test_due_date})
        # Act
        response = self.act()
        # Assert
        self.assertEqual(400, response['statusCode'])
        self.assertBody(response, {'message': 'Invalid next test due date'})

    def test_lambda_returns_200_given_no_live_episodes(self):
        # Arrange
        self.query_for_live_episode_patcher.return_value = []
        # Act
        response = self.act()
        # Assert
        self.assertEqual(200, response['statusCode'])
        self.assertBody(response, {})

    def test_lambda_returns_400_when_participant_is_ceased(self):
        self.get_participant_by_participant_id_patcher.return_value = dict(
            self.get_participant(), **{'is_ceased': True}
        )
        response = self.act()
        self.assertEqual(400, response['statusCode'])
        self.assertBody(response, {
            'message': 'You cannot defer a ceased participant'
        })

    def test_lambda_returns_400_when_participant_is_not_active(self):
        self.get_participant_by_participant_id_patcher.return_value = dict(
            self.get_participant(), **{'active': False}
        )
        response = self.act()
        self.assertEqual(400, response['statusCode'])
        self.assertBody(response, {
            'message': 'Participant is inactive and cannot be deferred.'
        })

    def test_lambda_raises_client_error_exception_given_query_for_live_episode_encounters_a_problem(self):
        self.query_for_live_episode_patcher.side_effect = ClientError(error_response={'Error': {
            'Message': 'Query for live episodes encountered an error'}}, operation_name='query')
        with self.assertRaises(ClientError) as context:
            self.act()

        self.assertEqual(
            'An error occurred (Unknown) when calling the query operation: ' +
            'Query for live episodes encountered an error',
            context.exception.args[0])

    def test_lambda_raises_client_error_exception_given_defer_episode_record_today_encounters_a_problem(self):
        self.defer_episode_record_today_patcher.side_effect = ClientError(error_response={'Error': {
            'Message': 'Defer episode record encountered an error'}}, operation_name='update')
        with self.assertRaises(ClientError) as context:
            self.act()

        self.assertEqual(
            'An error occurred (Unknown) when calling the update operation: ' +
            'Defer episode record encountered an error',
            context.exception.args[0])

    def test_lambda_raises_client_error_exception_given_update_participant_record_encounters_a_problem(self):
        self.defer_episode_record_today_patcher.side_effect = Mock()
        self.update_participant_record_patcher.side_effect = Mock()
        self.create_replace_record_in_participant_table_patcher.side_effect = ClientError(error_response={'Error': {
            'Message': 'Create defer record encountered an error'}}, operation_name='put')
        with self.assertRaises(ClientError) as context:
            self.act()

        self.assertEqual(
            'An error occurred (Unknown) when calling the put operation: ' +
            'Create defer record encountered an error',
            context.exception.args[0])

    def test_saves_crm_case_number_when_supplied(self):
        response = self.act()
        self.assertEqual(200, response['statusCode'])

        self.create_replace_record_in_participant_table_patcher.assert_called_once_with({
            **self.expected_create_replace_record,
            'participant_id': '01788592-74e3-4a26-b25c-106e2c52b379'
            }, segregate=True)
