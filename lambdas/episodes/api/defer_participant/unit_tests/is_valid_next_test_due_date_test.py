from unittest import TestCase
from unittest.mock import patch, MagicMock
from dateutil.relativedelta import relativedelta
from datetime import datetime, timezone
from ddt import ddt, data, unpack


module = 'defer_participant.defer_participant'


@ddt
class TestIsValidNextTestDueDate(TestCase):

    @patch('common.log.log', MagicMock())
    @patch('boto3.client', MagicMock())
    @patch('boto3.resource', MagicMock())
    def setUp(self):
        self.now = datetime(2020, 11, 11, 11, 11, 11, tzinfo=timezone.utc)
        self.get_today_patcher = patch(f'{module}.get_today').start()
        from defer_participant.defer_participant import is_valid_next_test_due_date
        self.is_valid_next_test_due_date = is_valid_next_test_due_date

        self.get_today_patcher.return_value = self.now.date()

    def tearDown(self):
        patch.stopall()

    @unpack
    @data(('CURRENT_PREGNANCY', 12),
          ('UNDER_COLPOSCOPY', 12),
          ('PATIENT_INFORMED_CHOICE', 18),
          ('UNDER_TREATMENT_RELEVANT_TO_SCREENING', 12),
          ('DISCHARGE_FROM_COLPOSCOPY', 36),
          ('RECENT_TEST', 3))
    def test_ntdd_upper_bound(self, defer_reason, max_months):
        # Arrange
        valid_ntdd = (self.now + relativedelta(months=max_months)).date().isoformat()
        invalid_ntdd = (self.now + relativedelta(months=max_months + 1)).date().isoformat()
        date_of_birth = '1990-01-01'
        # Act
        valid = self.is_valid_next_test_due_date(valid_ntdd, defer_reason, date_of_birth)
        invalid = self.is_valid_next_test_due_date(invalid_ntdd, defer_reason, date_of_birth)
        # Assert
        self.assertEqual(True, valid)
        self.assertEqual(False, invalid)

    @unpack
    @data(('ADMINISTRATIVE', 36, 49),
          ('ADMINISTRATIVE', 60, 50))
    def test_ntdd_upper_bound_with_age(self, defer_reason, max_months, age):
        # Arrange
        valid_ntdd = (self.now + relativedelta(months=max_months)).date().isoformat()
        invalid_ntdd = (self.now + relativedelta(months=max_months + 1)).date().isoformat()
        date_of_birth = (self.now + relativedelta(years=-age)).date().isoformat()
        # Act
        valid_result = self.is_valid_next_test_due_date(valid_ntdd, defer_reason, date_of_birth)
        invalid_result = self.is_valid_next_test_due_date(invalid_ntdd, defer_reason, date_of_birth)
        # Assert
        self.assertEqual(True, valid_result)
        self.assertEqual(False, invalid_result)

    def test_ntdd_lower_bound(self):
        # Arrange
        valid_ntdd = (self.now + relativedelta(months=10)).date().isoformat()
        invalid_ntdd = (self.now + relativedelta(weeks=9)).date().isoformat()
        date_of_birth = '1990-01-01'
        # Act
        valid_result = self.is_valid_next_test_due_date(valid_ntdd, 'CURRENT_PREGNANCY', date_of_birth)
        invalid_result = self.is_valid_next_test_due_date(invalid_ntdd, 'CURRENT_PREGNANCY', date_of_birth)
        # Assert
        self.assertEqual(True, valid_result)
        self.assertEqual(False, invalid_result)
