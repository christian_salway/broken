This lambda defers a participant within the cervical screening programme. 

To defer a participant, a next test due date (NTDD) for the participant and reason for deferring must be supplied. The NTDD must be in the future.

If the NTDD is not a date or not in the future, a 400 status code is returned.

If the participant doe not have a live episode, a 400 status code is returned. 

If the participant has a live episode, the participant is first verified to be in an appropriate state for deferring. 

If the participant episode is not in a valid state for deferring, a 409 conflict status code is returned. 

If the participant episode is in a valid state for deferring:
- the episode is updated with status DEFERRED, deferred date of today, user id from the session and live_record_status is removed
- the participant is updated with the supplied next test due date
- a defer record is created for the participant
- an audit event is created
- 200 status code with an empty body is returned 

[JIRA](https://nhsd-jira.digital.nhs.uk/browse/SP-1245)
[Confluence](https://nhsd-confluence.digital.nhs.uk/display/CSP/3+Defer+process)
