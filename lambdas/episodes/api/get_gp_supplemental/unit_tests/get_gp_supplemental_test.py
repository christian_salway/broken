from unittest.case import TestCase
from mock import patch, call

from get_gp_supplemental.get_gp_supplemental import LogReference


class TestGetGPSupplemental(TestCase):
    @classmethod
    @patch('boto3.resource')
    @patch('boto3.client')
    def setUpClass(cls, client_mock, resource_mock):
        import get_gp_supplemental.get_gp_supplemental as get_gp_supplemental_module
        global get_gp_supplemental_module

    def setUp(self):
        super(TestGetGPSupplemental, self).setUp()
        self.unwrapped_handler = get_gp_supplemental_module.lambda_handler.__wrapped__.__wrapped__

    @patch('get_gp_supplemental.get_gp_supplemental.log')
    @patch('get_gp_supplemental.get_gp_supplemental.get_api_request_info_from_event')
    def test_get_gp_supplemental_returns_bad_request_if_no_type_value(self,
                                                                      get_api_request_mock,
                                                                      log_mock):
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {}
            },
            '',
            '')
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Supplemental data type value not supplied"}',
            'isBase64Encoded': False
        }
        response = self.unwrapped_handler('event', 'context')
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with('event')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETGPSUPP0001}),
            call({'log_reference': LogReference.GETGPSUPPER0001})
        ])

    @patch('get_gp_supplemental.get_gp_supplemental.log')
    @patch('get_gp_supplemental.get_gp_supplemental.get_api_request_info_from_event')
    def test_get_gp_supplemental_returns_bad_request_if_type_value_unsupported(self,
                                                                               get_api_request_mock,
                                                                               log_mock):
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'type_value': 'XYZ'},
                'body': '',
                'session': {}
            },
            '',
            '')

        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Supplemental data type value not supported"}',
            'isBase64Encoded': False
        }
        response = self.unwrapped_handler('event', 'context')
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with('event')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETGPSUPP0001}),
            call({'log_reference': LogReference.GETGPSUPPER0002, 'type value': 'XYZ'})
        ])

    @patch('get_gp_supplemental.get_gp_supplemental.log')
    @patch('get_gp_supplemental.get_gp_supplemental.get_api_request_info_from_event')
    @patch('get_gp_supplemental.get_gp_supplemental.get_not_registered')
    def test_get_gp_supplemental_calls_through_to_service_for_unregistered_gps(self,
                                                                               get_not_registered_mock,
                                                                               get_api_request_mock,
                                                                               log_mock):
        mock_event_data = (
            {
                'path_parameters': {},
                'query_parameters': {'type_value': 'NOT_REGISTERED'},
                'body': '',
                'session': {}
            },
            '',
            '')
        get_api_request_mock.return_value = mock_event_data
        get_not_registered_mock.return_value = {}

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": {}}',
            'isBase64Encoded': False
        }
        response = self.unwrapped_handler('event', 'context')
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with('event')
        get_not_registered_mock.assert_called_once_with(
            {'path_parameters': {}, 'query_parameters': {'type_value': 'NOT_REGISTERED'}, 'body': '', 'session': {}}
        )
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETGPSUPP0001})
        ])

    @patch('get_gp_supplemental.get_gp_supplemental.log')
    @patch('get_gp_supplemental.get_gp_supplemental.get_api_request_info_from_event')
    @patch('get_gp_supplemental.get_gp_supplemental.get_not_registered')
    def test_get_gp_supplemental_returns_500_on_service_function_excception(self,
                                                                            get_not_registered_mock,
                                                                            get_api_request_mock,
                                                                            log_mock):
        mock_event_data = (
            {
                'path_parameters': {},
                'query_parameters': {'type_value': 'NOT_REGISTERED'},
                'body': '',
                'session': {}
            },
            '',
            '')
        get_api_request_mock.return_value = mock_event_data
        get_not_registered_mock.side_effect = Exception('Test')
        get_not_registered_mock.return_value = {}

        expected_response = {
            'statusCode': 500,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Cannot complete request"}',
            'isBase64Encoded': False
        }
        response = self.unwrapped_handler('event', 'context')
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with('event')
        get_not_registered_mock.assert_called_once_with(
            {'path_parameters': {}, 'query_parameters': {'type_value': 'NOT_REGISTERED'}, 'body': '', 'session': {}}
        )
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETGPSUPP0001}),
            call({'log_reference': LogReference.GETGPSUPPER0003, 'error': 'Test'})
        ])
