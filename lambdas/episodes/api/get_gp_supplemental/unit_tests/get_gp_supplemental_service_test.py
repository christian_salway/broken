from unittest import TestCase
from ddt import data, ddt, unpack
from mock import patch, Mock, call
from boto3.dynamodb.conditions import Key
from common.utils.organisation_supplementary_utils import NOT_REGISTERED_PREFIX
from get_gp_supplemental.get_gp_supplemental import LogReference
from common.utils.dynamodb_access.table_names import TableNames


class TestGetGPSupplementalModule(TestCase):

    @classmethod
    @patch('boto3.resource', Mock())
    def setUpClass(cls):
        global service_module
        import get_gp_supplemental.get_gp_supplemental_service as service_module

    @patch('get_gp_supplemental.get_gp_supplemental_service.get_organisation_information_by_organisation_code')
    @patch('common.utils.organisation_supplementary_utils.dynamodb_query')
    @patch('get_gp_supplemental.get_gp_supplemental_service.log')
    @patch('get_gp_supplemental.get_gp_supplemental_service._get_pnl_viewing_info')
    @patch('get_gp_supplemental.get_gp_supplemental_service._get_nrl_viewing_info')
    @patch('get_gp_supplemental.get_gp_supplemental_service._get_review_viewing_info')
    def test_get_not_registered_returns_empty_list_when_no_results(self,
                                                                   get_review_mock,
                                                                   get_nrl_mock,
                                                                   get_pnl_mock,
                                                                   log_mock,
                                                                   query_mock,
                                                                   get_info_mock):
        # Given
        query_mock.return_value = []

        # When
        event_data = {
            'path_parameters': {},
            'query_parameters': {'type_value': 'NOT_REGISTERED'},
            'body': '',
            'session': {}
        }
        actual_result = service_module.get_not_registered(event_data)

        # Then
        expected_key_expression = Key('type_value').eq(NOT_REGISTERED_PREFIX)
        query_mock.assert_called_with(
            TableNames.ORGANISATION_SUPPLEMENTAL,
            dict(
                IndexName='type_value',
                KeyConditionExpression=expected_key_expression
            )
        )
        expected_result = []
        expected_log_calls = [call({'log_reference': LogReference.GETGPSUPP0002}),
                              call({'log_reference': LogReference.GETGPSUPP0003, 'records_found': '0'})]

        log_mock.assert_has_calls(expected_log_calls)
        self.assertEqual(actual_result, expected_result)
        get_info_mock.assert_not_called()
        get_review_mock.assert_not_called()
        get_pnl_mock.assert_not_called()
        get_nrl_mock.assert_not_called()

    @patch('get_gp_supplemental.get_gp_supplemental_service.get_organisation_information_by_organisation_code')
    @patch('common.utils.organisation_supplementary_utils.dynamodb_query')
    @patch('get_gp_supplemental.get_gp_supplemental_service.log')
    @patch('get_gp_supplemental.get_gp_supplemental_service._get_pnl_viewing_info')
    @patch('get_gp_supplemental.get_gp_supplemental_service._get_nrl_viewing_info')
    @patch('get_gp_supplemental.get_gp_supplemental_service._get_review_viewing_info')
    def test_get_not_registered_returns_basic_results(self,
                                                      get_review_mock,
                                                      get_nrl_mock,
                                                      get_pnl_mock,
                                                      log_mock,
                                                      query_mock,
                                                      get_info_mock):
        # Given
        query_mock.return_value = [
            {
                'organisation_code': 'A1',
                'type_value': 'NOT_REGISTERED',
                'organisation_supplemental_id': '37f4524d-e59f-41d3-9071-97eec87955c8'
            },
            {
                'organisation_code': 'A2',
                'type_value': 'NOT_REGISTERED',
                'organisation_supplemental_id': '37f4524d-e59f-41d3-9071-97eec87955c9'
            }
        ]

        get_info_mock.return_value = {'address': {'address_line_1': 'address1', 'postcode': 'postcode'}}
        get_pnl_mock.return_value = {'by': 'John', 'date': '2020-01-05'}
        get_nrl_mock.return_value = {'by': 'Joan', 'date': '2020-02-05'}
        get_review_mock.return_value = {'by': 'Jane', 'date': '2020-03-05'}

        # When
        event_data = {
            'path_parameters': {},
            'query_parameters': {'type_value': 'NOT_REGISTERED'},
            'body': '',
            'session': {}
        }
        actual_result = service_module.get_not_registered(event_data)

        # Then
        expected_key_expression = Key('type_value').eq(NOT_REGISTERED_PREFIX)
        query_mock.assert_called_with(
            TableNames.ORGANISATION_SUPPLEMENTAL,
            dict(
                IndexName='type_value',
                KeyConditionExpression=expected_key_expression
            )
        )
        expected_result = [
            {
                'organisation_code': 'A1',
                'address': 'address1 postcode',
                'last_views': {
                    'pnl': {'by': 'John', 'date': '2020-01-05'},
                    'nrl': {'by': 'Joan', 'date': '2020-02-05'},
                    'review': {'by': 'Jane', 'date': '2020-03-05'}
                }
            },
            {
                'organisation_code': 'A2',
                'address': 'address1 postcode',
                'last_views': {
                    'pnl': {'by': 'John', 'date': '2020-01-05'},
                    'nrl': {'by': 'Joan', 'date': '2020-02-05'},
                    'review': {'by': 'Jane', 'date': '2020-03-05'}
                }
            }
        ]
        expected_log_calls = [call({'log_reference': LogReference.GETGPSUPP0002}),
                              call({'log_reference': LogReference.GETGPSUPP0003, 'records_found': '2'})]

        log_mock.assert_has_calls(expected_log_calls)
        self.assertEqual(actual_result, expected_result)
        expected_calls = [call('A1'), call('A2')]
        get_info_mock.assert_has_calls(expected_calls)
        get_review_mock.assert_has_calls(expected_calls)
        get_pnl_mock.assert_has_calls(expected_calls)
        get_nrl_mock.assert_has_calls(expected_calls)


@ddt
class TestGetGPSupplementalFunctions(TestCase):

    @classmethod
    @patch('boto3.resource', Mock())
    def setUpClass(cls):
        global service_module
        import get_gp_supplemental.get_gp_supplemental_service as service_module

    @unpack
    @data(
        ({}, ''),
        ({'address_line_1': '123 My Road'}, ''),
        ({'postcode': 'EC12 8AJ'}, ''),
        ({'address_line_1': '123 My Road', 'postcode': 'EC12 8AJ'}, '123 My Road EC12 8AJ')
    )
    def test_condense_address_info(self, data, expected_output):
        actual_output = service_module._condense_address_info(data)
        self.assertEqual(expected_output, actual_output)

    @patch('get_gp_supplemental.get_gp_supplemental_service._get_pnl_viewing_info')
    @patch('get_gp_supplemental.get_gp_supplemental_service._get_nrl_viewing_info')
    @patch('get_gp_supplemental.get_gp_supplemental_service._get_review_viewing_info')
    def test_get_last_view_info(self,
                                get_review_mock,
                                get_nrl_mock,
                                get_pnl_mock):
        get_pnl_mock.return_value = {'by': 'John', 'date': '2020-01-05'}
        get_nrl_mock.return_value = {'by': 'Joan', 'date': '2020-02-05'}
        get_review_mock.return_value = {'by': 'Jane', 'date': '2020-03-05'}
        expected_result = {
            'pnl': {'by': 'John', 'date': '2020-01-05'},
            'nrl': {'by': 'Joan', 'date': '2020-02-05'},
            'review': {'by': 'Jane', 'date': '2020-03-05'}
        }
        actual_result = service_module._get_last_view_info('A1')
        self.assertEqual(expected_result, actual_result)
        get_pnl_mock.assert_has_calls([call('A1')])
        get_nrl_mock.assert_has_calls([call('A1')])
        get_review_mock.assert_has_calls([call('A1')])

    @patch('get_gp_supplemental.get_gp_supplemental_service.get_print_pnl_record')
    def test_get_pnl_viewing_info(self,
                                  get_print_pnl_record_mock):
        get_print_pnl_record_mock.return_value = {
            'organisation_code': 'A1',
            'type_value': 'PRINT_PNL#2021-07-14#Jane.Smith',
            'organisation_supplemental_id': 'new_guid'
        }
        actual_result = service_module._get_pnl_viewing_info('A1')
        get_print_pnl_record_mock.assert_has_calls([call('A1')])
        expected_result = {'by': 'Jane Smith', 'date': '2021-07-14'}
        self.assertEqual(expected_result, actual_result)

    @patch('get_gp_supplemental.get_gp_supplemental_service.get_print_nrl_record')
    def test_get_nrl_viewing_info(self,
                                  get_print_nrl_record_mock):
        get_print_nrl_record_mock.return_value = {
            'organisation_code': 'A2',
            'type_value': 'PRINT_NRL#2021-07-14#John.Smith',
            'organisation_supplemental_id': 'new_guid'
        }
        actual_result = service_module._get_nrl_viewing_info('A2')
        get_print_nrl_record_mock.assert_has_calls([call('A2')])
        expected_result = {'by': 'John Smith', 'date': '2021-07-14'}
        self.assertEqual(expected_result, actual_result)

    @patch('get_gp_supplemental.get_gp_supplemental_service.get_print_review_record')
    def test_get_review_viewing_info(self,
                                     get_print_review_record_mock):
        get_print_review_record_mock.return_value = {
            'organisation_code': 'A3',
            'type_value': 'PRINT_REVIEW#2021-07-14#Joan.Smith',
            'organisation_supplemental_id': 'new_guid'
        }
        actual_result = service_module._get_review_viewing_info('A3')
        get_print_review_record_mock.assert_has_calls([call('A3')])
        expected_result = {'by': 'Joan Smith', 'date': '2021-07-14'}
        self.assertEqual(expected_result, actual_result)

    def test_convert_viewing_info(self):
        raw_data = {'type_value': 'SOME_KEY#SOME_DATE#SOME.NAME'}
        actual_result = service_module._convert_viewing_info(raw_data)
        expected_result = {'by': 'SOME NAME', 'date': 'SOME_DATE'}
        self.assertEqual(expected_result, actual_result)
