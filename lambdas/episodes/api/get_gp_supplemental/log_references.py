import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    GETGPSUPP0001 = (logging.INFO, 'Received request to fetch GP supplemental info.')
    GETGPSUPP0002 = (logging.INFO, 'Querying for GPs not registered.')
    GETGPSUPP0003 = (logging.INFO, 'Querying for GPs not registered complete.')

    GETGPSUPPER0001 = (logging.ERROR, 'No supplemental data type value supplied.')
    GETGPSUPPER0002 = (logging.ERROR, 'Supplemental data type value not supported.')
    GETGPSUPPER0003 = (logging.ERROR, 'Error processing request.')
