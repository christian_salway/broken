from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.log import log
from common.utils import json_return_message, json_return_data
from common.utils.api_utils import get_api_request_info_from_event
from get_gp_supplemental.get_gp_supplemental_service import get_not_registered
from get_gp_supplemental.log_references import LogReference


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):

    log({'log_reference': LogReference.GETGPSUPP0001})

    event_data = get_api_request_info_from_event(event)[0]

    type_value = event_data.get('query_parameters', {}).get('type_value')
    if not type_value:
        log({'log_reference': LogReference.GETGPSUPPER0001})
        return json_return_message(400, 'Supplemental data type value not supplied')

    REQUEST_FUNCTIONS = {
        'NOT_REGISTERED': get_not_registered
    }

    if type_value not in REQUEST_FUNCTIONS:
        log({'log_reference': LogReference.GETGPSUPPER0002, 'type value': type_value})
        return json_return_message(400, 'Supplemental data type value not supported')

    try:
        supplemental = REQUEST_FUNCTIONS.get(type_value)(event_data)
    except Exception as e:
        log({'log_reference': LogReference.GETGPSUPPER0003, 'error': str(e)})
        return json_return_message(500, 'Cannot complete request')

    return json_return_data(200, supplemental)
