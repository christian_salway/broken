from common.log import log
from get_gp_supplemental.log_references import LogReference
from common.utils.organisation_supplementary_utils import (
    query_for_not_registered_records,
    get_print_pnl_record,
    get_print_nrl_record,
    get_print_review_record
)
from common.utils.organisation_directory_utils import (
    get_organisation_information_by_organisation_code
)


def get_not_registered(event_data):

    log({'log_reference': LogReference.GETGPSUPP0002})

    unregistered_gps = query_for_not_registered_records()

    log({'log_reference': LogReference.GETGPSUPP0003, 'records_found': str(len(unregistered_gps))})

    records = []
    for item in unregistered_gps:
        organisation_code = item['organisation_code']
        org_info = get_organisation_information_by_organisation_code(organisation_code)
        record = {
            'organisation_code': organisation_code,
            'address': _condense_address_info(org_info.get('address', {})),
            'last_views': _get_last_view_info(organisation_code)
        }
        records.append(record)

    return records


def _condense_address_info(data):
    address_1 = data.get('address_line_1', '') if data else ''
    postcode = data.get('postcode', '') if data else ''
    return (address_1 + ' ' + postcode) if address_1 and postcode else ''


def _get_last_view_info(organisation_code):
    return {
        'pnl': _get_pnl_viewing_info(organisation_code),
        'nrl': _get_nrl_viewing_info(organisation_code),
        'review': _get_review_viewing_info(organisation_code)
    }


def _get_pnl_viewing_info(organisation_code):
    return _convert_viewing_info(get_print_pnl_record(organisation_code))


def _get_nrl_viewing_info(organisation_code):
    return _convert_viewing_info(get_print_nrl_record(organisation_code))


def _get_review_viewing_info(organisation_code):
    return _convert_viewing_info(get_print_review_record(organisation_code))


def _convert_viewing_info(record):
    result = {'by': '', 'date': ''}
    raw_type_value = record.get('type_value', '') if record else ''
    if raw_type_value:
        spl = raw_type_value.split('#')
        if len(spl) >= 3:
            result['by'] = spl[2].replace('.', ' ')
            result['date'] = spl[1]
    return result
