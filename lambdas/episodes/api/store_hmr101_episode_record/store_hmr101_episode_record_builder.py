from datetime import datetime, timezone
from common.models.participant import EpisodeStatus


HMR101_FIELD_VALUES = [
    'last_menstrual_period',
    'test_date',
    'episode_address',
    'participant_condition',
    'sample_source',
    'reason_for_sample',
    'specimen_type',
    'specimen_details',
    'clinical_data'
]


def build_episode_record(participant_id, episode_record, is_new_episode, form_data, walkin_test_date, session):
    now = datetime.now(timezone.utc).isoformat()

    if is_new_episode:
        episode_record['participant_id'] = participant_id
        episode_record['sort_key'] = f'EPISODE#{walkin_test_date}'
        episode_record['created'] = now
        episode_record['status'] = EpisodeStatus.WALKIN.value
        episode_record['live_record_status'] = EpisodeStatus.WALKIN.value
        episode_record['test_due_date'] = walkin_test_date

    episode_record['user_id'] = session['user_data']['nhsid_useruid']
    episode_record['test_date'] = walkin_test_date

    hmr101_form_data = {
        'session_id': session.get('session_id'),
        'timestamp': now,
        'sample_taker_code': form_data['sample_taker_code']
    }

    for key, value in form_data.items():
        if key in HMR101_FIELD_VALUES:
            hmr101_form_data[key] = value

    if not episode_record.get('HMR101_form_data'):
        episode_record['HMR101_form_data'] = []

    episode_record['HMR101_form_data'].append(hmr101_form_data)

    return episode_record
