import json
from datetime import datetime, timezone

from common.utils.audit_utils import audit, AuditActions
from common.utils import json_return_object, json_return_message
from store_hmr101_episode_record.log_references import LogReference
from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils.participant_episode_utils import (
    query_for_live_episode, create_replace_episode_record
)
from common.utils.participant_utils import get_nhs_number_by_participant_id
from store_hmr101_episode_record.store_hmr101_episode_record_builder import build_episode_record
from common.utils.session_utils import get_session_from_lambda_event
from common.utils.participant_utils import get_participant_by_participant_id


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    log({'log_reference': LogReference.HMREP0001})
    path_parameters = event.get('pathParameters')
    participant_id = path_parameters.get('participant_id') if path_parameters else None
    if not participant_id:
        log({'log_reference': LogReference.HMREPEX0001})
        return json_return_message(400, 'Parameter participant_id missing from path parameters')

    request_body = json.loads(event.get('body')) if event.get('body') else {}
    hmr101_form_data = request_body.get('hmr_101_form_data', {})

    if not hmr101_form_data.get('sample_taker_code'):
        log({'log_reference': LogReference.HMREPEX0002})
        return json_return_message(400, 'sample_taker_code missing from hmr101 form data')

    participant = get_participant_by_participant_id(participant_id, True)
    if not participant:
        log({'log_reference': LogReference.HMREPEX0003})
        return json_return_message(404, 'No participant matching participant_id')

    session = get_session_from_lambda_event(event)

    walkin_test_date = hmr101_form_data.get('test_date', datetime.now(timezone.utc).date().isoformat())

    log({'log_reference': LogReference.HMREP0002})

    episode_records = query_for_live_episode(participant_id, use_projection=False, segregate=True)

    if episode_records:
        is_new_episode = False
        episode_record = episode_records[0]
        log({'log_reference': LogReference.HMREP0003})
    else:
        is_new_episode = True
        episode_record = {}
        log({'log_reference': LogReference.HMREP0004})

    episode_record_to_save = build_episode_record(participant_id, episode_record, is_new_episode,
                                                  hmr101_form_data, walkin_test_date, session)

    create_replace_episode_record(episode_record_to_save)

    audit(session=session,
          action=AuditActions.PRINT_HMR101,
          participant_ids=[participant_id],
          nhs_numbers=[get_nhs_number_by_participant_id(participant_id)])

    return json_return_object(200)
