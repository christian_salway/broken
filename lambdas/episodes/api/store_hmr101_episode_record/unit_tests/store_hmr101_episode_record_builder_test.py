from unittest.case import TestCase
from mock import patch, Mock
from datetime import datetime, timezone
from common.models.participant import EpisodeStatus
import store_hmr101_episode_record.store_hmr101_episode_record_builder as builder


@patch.object(builder, 'datetime', Mock(wraps=datetime))
class TestStoreHMR101EpisodeRecordBuilder(TestCase):

    FIXED_NOW_TIME = datetime(2020, 4, 16, 17, 23, 5, 123456, tzinfo=timezone.utc)

    def test_build_episode_record_creates_new_episode_record_given_is_new_episode_and_no_hmr101_form_data(self):
        builder.datetime.now.return_value = self.FIXED_NOW_TIME
        participant_id = 'the_participant'
        episode_record = {}
        is_new_episode = True
        form_data = {'sample_taker_code': 'code'}
        walkin_test_date = '2019-05-30'
        session = {'session_id': 'the_session', 'user_data': {'nhsid_useruid': 'the_user'}}

        expected_record = {
            'participant_id': 'the_participant',
            'sort_key': 'EPISODE#2019-05-30',
            'created': '2020-04-16T17:23:05.123456+00:00',
            'test_due_date': '2019-05-30',
            'status': EpisodeStatus.WALKIN.value,
            'live_record_status': EpisodeStatus.WALKIN.value,
            'HMR101_form_data': [{
                'session_id': 'the_session',
                'timestamp': '2020-04-16T17:23:05.123456+00:00',
                'sample_taker_code': 'code'
            }],
            'user_id': 'the_user',
            'test_date': '2019-05-30'
        }

        actual_record = builder.build_episode_record(participant_id, episode_record, is_new_episode, form_data,
                                                     walkin_test_date, session)

        self.assertDictEqual(expected_record, actual_record)

    def test_build_episode_record_creates_new_episode_record_given_is_new_episode_and_hmr101_form_data(self):
        builder.datetime.now.return_value = self.FIXED_NOW_TIME
        participant_id = 'the_participant'
        episode_record = {}
        is_new_episode = True
        form_data = {
            'sample_taker_code': 'code',
            'not_a_valid_form_key': 'not_a_valid_form_value',
            'last_menstrual_period': 'last_month',
            'test_date': 'today',
            'episode_address': 'the_address',
            'participant_condition': 'the_condition',
            'sample_source': 'the_source',
            'reason_for_sample': 'the_reason',
            'specimen_type': 'the_type',
            'specimen_details': 'the_details',
            'clinical_data': 'the_data'
        }
        walkin_test_date = '2019-05-30'
        session = {'session_id': 'the_session', 'user_data': {'nhsid_useruid': 'the_user'}}

        expected_record = {
            'participant_id': 'the_participant',
            'sort_key': 'EPISODE#2019-05-30',
            'created': '2020-04-16T17:23:05.123456+00:00',
            'status': EpisodeStatus.WALKIN.value,
            'live_record_status': EpisodeStatus.WALKIN.value,
            'test_due_date': '2019-05-30',
            'HMR101_form_data': [{
                'session_id': 'the_session',
                'timestamp': '2020-04-16T17:23:05.123456+00:00',
                'sample_taker_code': 'code',
                'last_menstrual_period': 'last_month',
                'test_date': 'today',
                'episode_address': 'the_address',
                'participant_condition': 'the_condition',
                'sample_source': 'the_source',
                'reason_for_sample': 'the_reason',
                'specimen_type': 'the_type',
                'specimen_details': 'the_details',
                'clinical_data': 'the_data'
            }],
            'user_id': 'the_user',
            'test_date': '2019-05-30'
        }

        actual_record = builder.build_episode_record(participant_id, episode_record, is_new_episode, form_data,
                                                     walkin_test_date, session)

        self.assertDictEqual(expected_record, actual_record)

    def test_build_episode_record_updates_episode_record_given_is_not_new_episode_and_no_hmr101_form_data(self):
        builder.datetime.now.return_value = self.FIXED_NOW_TIME
        participant_id = 'the_participant'
        episode_record = {'existing_key': 'existing_value',
                          'HMR101_form_data': [{'old_hmr101_key': 'old_hmr101_value'}]}
        is_new_episode = False
        form_data = {'sample_taker_code': 'code'}
        walkin_test_date = '2019-05-30'
        session = {'session_id': 'the_session', 'user_data': {'nhsid_useruid': 'the_user'}}

        expected_record = {
            'existing_key': 'existing_value',
            'HMR101_form_data': [
                {'old_hmr101_key': 'old_hmr101_value'},
                {
                    'session_id': 'the_session',
                    'timestamp': '2020-04-16T17:23:05.123456+00:00',
                    'sample_taker_code': 'code'
                }
            ],
            'user_id': 'the_user',
            'test_date': '2019-05-30'
        }

        actual_record = builder.build_episode_record(participant_id, episode_record, is_new_episode, form_data,
                                                     walkin_test_date, session)

        self.assertDictEqual(expected_record, actual_record)

    def test_build_episode_record_updates_episode_record_given_is_not_new_episode_and_hmr101_form_data(self):
        builder.datetime.now.return_value = self.FIXED_NOW_TIME
        participant_id = 'the_participant'
        episode_record = {'existing_key': 'existing_value',
                          'HMR101_form_data': [{'old_hmr101_key': 'old_hmr101_value'}]}
        is_new_episode = False
        form_data = {
            'not_a_valid_form_key': 'not_a_valid_form_value',
            'sample_taker_code': 'code',
            'last_menstrual_period': 'last_month',
            'test_date': 'today',
            'episode_address': 'the_address',
            'participant_condition': 'the_condition',
            'sample_source': 'the_source',
            'reason_for_sample': 'the_reason',
            'specimen_type': 'the_type',
            'specimen_details': 'the_details',
            'clinical_data': 'the_data'
        }
        walkin_test_date = '2019-05-30'
        session = {'session_id': 'the_session', 'user_data': {'nhsid_useruid': 'the_user'}}

        expected_record = {
            'existing_key': 'existing_value',
            'HMR101_form_data': [
                {
                    'old_hmr101_key': 'old_hmr101_value'
                },
                {
                    'session_id': 'the_session',
                    'timestamp': '2020-04-16T17:23:05.123456+00:00',
                    'sample_taker_code': 'code',
                    'last_menstrual_period': 'last_month',
                    'test_date': 'today',
                    'episode_address': 'the_address',
                    'participant_condition': 'the_condition',
                    'sample_source': 'the_source',
                    'reason_for_sample': 'the_reason',
                    'specimen_type': 'the_type',
                    'specimen_details': 'the_details',
                    'clinical_data': 'the_data'
                }
            ],
            'user_id': 'the_user',
            'test_date': '2019-05-30'
        }

        actual_record = builder.build_episode_record(participant_id, episode_record, is_new_episode, form_data,
                                                     walkin_test_date, session)

        self.assertDictEqual(expected_record, actual_record)

    def test_build_episode_record_updates_episode_record_given_existing_epsiode_has_no_hmr101_form_data_key(self):
        builder.datetime.now.return_value = self.FIXED_NOW_TIME
        participant_id = 'the_participant'
        episode_record = {'existing_key': 'existing_value'}
        is_new_episode = False
        form_data = {'sample_taker_code': 'code'}
        walkin_test_date = '2019-05-30'
        session = {'session_id': 'the_session', 'user_data': {'nhsid_useruid': 'the_user'}}

        expected_record = {
            'existing_key': 'existing_value',
            'HMR101_form_data': [
                {
                    'session_id': 'the_session',
                    'timestamp': '2020-04-16T17:23:05.123456+00:00',
                    'sample_taker_code': 'code'
                }
            ],
            'user_id': 'the_user',
            'test_date': '2019-05-30'
        }

        actual_record = builder.build_episode_record(participant_id, episode_record, is_new_episode, form_data,
                                                     walkin_test_date, session)

        self.assertDictEqual(expected_record, actual_record)
