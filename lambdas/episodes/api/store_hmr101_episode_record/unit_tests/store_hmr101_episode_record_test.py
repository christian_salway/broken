from datetime import datetime, timezone
from unittest.case import TestCase
from mock import patch, Mock, call

from common.utils.audit_utils import AuditActions
from store_hmr101_episode_record.log_references import LogReference
with patch('boto3.resource') as resource_mock:
    import store_hmr101_episode_record.store_hmr101_episode_record as environment
    from store_hmr101_episode_record.store_hmr101_episode_record import lambda_handler


@patch.object(environment, 'datetime', Mock(wraps=datetime))
@patch('store_hmr101_episode_record.store_hmr101_episode_record.get_participant_by_participant_id')
@patch('store_hmr101_episode_record.store_hmr101_episode_record.audit')
@patch('store_hmr101_episode_record.store_hmr101_episode_record.get_nhs_number_by_participant_id')
@patch('store_hmr101_episode_record.store_hmr101_episode_record.create_replace_episode_record')
@patch('store_hmr101_episode_record.store_hmr101_episode_record.build_episode_record')
@patch('store_hmr101_episode_record.store_hmr101_episode_record.query_for_live_episode')
@patch('store_hmr101_episode_record.store_hmr101_episode_record.get_session_from_lambda_event')
@patch('store_hmr101_episode_record.store_hmr101_episode_record.log')
class TestStoreHMR101EpisodeRecord(TestCase):

    FIXED_NOW_TIME = datetime(2020, 4, 16, 17, 23, 5, tzinfo=timezone.utc)

    EXPECTED_OK_RESPONSE = {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json',
            'X-Content-Type-Options': 'nosniff',
            'Strict-Transport-Security': 'max-age=1576800'
        },
        'body': '{}',
        'isBase64Encoded': False,
    }

    def setUp(self):
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__

    def test_lambda_returns_bad_request_given_no_path_parameters(
            self, log_mock, get_session_mock, query_episodes_mock, build_record_mock, create_replace_mock,
            get_nhs_number_mock, audit_mock, get_participant_mock):
        lambda_event = {'pathParameters': None}
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Parameter participant_id missing from path parameters"}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(expected_response, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.HMREP0001}),
            call({'log_reference': LogReference.HMREPEX0001})
        ]

        log_mock.assert_has_calls(expected_log_calls)
        get_session_mock.assert_not_called()
        query_episodes_mock.assert_not_called()
        build_record_mock.assert_not_called()
        create_replace_mock.assert_not_called()
        get_nhs_number_mock.assert_not_called()
        audit_mock.assert_not_called()
        get_participant_mock.assert_not_called()

    def test_lambda_returns_bad_request_given_no_participant_id_in_path_parameters(
            self, log_mock, get_session_mock, query_episodes_mock, build_record_mock, create_replace_mock,
            get_nhs_number_mock, audit_mock, get_participant_mock):
        lambda_event = {'pathParameters': {'a_key': 'a_value'}}
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Parameter participant_id missing from path parameters"}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(expected_response, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.HMREP0001}),
            call({'log_reference': LogReference.HMREPEX0001})
        ]

        log_mock.assert_has_calls(expected_log_calls)
        get_session_mock.assert_not_called()
        query_episodes_mock.assert_not_called()
        build_record_mock.assert_not_called()
        create_replace_mock.assert_not_called()
        get_nhs_number_mock.assert_not_called()
        audit_mock.assert_not_called()
        get_participant_mock.assert_not_called()

    def test_lambda_returns_bad_request_given_no_sample_taker_code_in_form_data(
            self, log_mock, get_session_mock, query_episodes_mock, build_record_mock, create_replace_mock,
            get_nhs_number_mock, audit_mock, get_participant_mock):
        lambda_event = {'pathParameters': {'participant_id': 'the_participant'},
                        'body': '{"hmr_101_form_data": {"entered_key": "entered_value"}}'}
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "sample_taker_code missing from hmr101 form data"}',
            'isBase64Encoded': False
        }

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(expected_response, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.HMREP0001}),
            call({'log_reference': LogReference.HMREPEX0002})
        ]

        log_mock.assert_has_calls(expected_log_calls)
        get_session_mock.assert_not_called()
        query_episodes_mock.assert_not_called()
        build_record_mock.assert_not_called()
        create_replace_mock.assert_not_called()
        get_nhs_number_mock.assert_not_called()
        audit_mock.assert_not_called()
        get_participant_mock.assert_not_called()

    def test_lambda_returns_not_found_when_no_participant_found_in_cohort(
            self, log_mock, get_session_mock, query_episodes_mock, build_record_mock, create_replace_mock,
            get_nhs_number_mock, audit_mock, get_participant_mock):

        environment.datetime.now.return_value = self.FIXED_NOW_TIME
        lambda_event = {'pathParameters': {'participant_id': 'the_participant'},
                        'body': '{"hmr_101_form_data": {"sample_taker_code": "code"}}'}

        expected_response = {
            'statusCode': 404,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "No participant matching participant_id"}',
            'isBase64Encoded': False
        }

        get_participant_mock.return_value = None

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(expected_response, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.HMREP0001}),
            call({'log_reference': LogReference.HMREPEX0003})
        ]

        log_mock.assert_has_calls(expected_log_calls)
        get_session_mock.assert_not_called()
        query_episodes_mock.assert_not_called()
        build_record_mock.assert_not_called()
        create_replace_mock.assert_not_called()
        get_nhs_number_mock.assert_not_called()
        audit_mock.assert_not_called()
        get_participant_mock.assert_called_with('the_participant', True)

    def test_lambda_creates_episode_record_given_no_hmr101_form_data_and_no_existing_episode(
            self, log_mock, get_session_mock, query_episodes_mock, build_record_mock, create_replace_mock,
            get_nhs_number_mock, audit_mock, get_participant_mock):
        environment.datetime.now.return_value = self.FIXED_NOW_TIME
        lambda_event = {'pathParameters': {'participant_id': 'the_participant'},
                        'body': '{"hmr_101_form_data": {"sample_taker_code": "code"}}'}
        session = {'session_id': 'the_session', 'user_data': {'nhsid_useruid': 'the_user'}}
        get_session_mock.return_value = session
        query_episodes_mock.return_value = []
        build_record_mock.return_value = {'built_key': 'built_value'}
        get_nhs_number_mock.return_value = 'the_nhs_number'
        get_participant_mock.return_value = {'participant_id': 'the_participant'}

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(self.EXPECTED_OK_RESPONSE, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.HMREP0001}),
            call({'log_reference': LogReference.HMREP0002}),
            call({'log_reference': LogReference.HMREP0004})
        ]

        log_mock.assert_has_calls(expected_log_calls)
        get_session_mock.assert_called_with(lambda_event)
        query_episodes_mock.assert_called_with('the_participant', use_projection=False, segregate=True)
        build_record_mock.assert_called_with(
            'the_participant', {}, True, {'sample_taker_code': 'code'}, '2020-04-16', session)
        create_replace_mock.assert_called_with({'built_key': 'built_value'})
        get_nhs_number_mock.assert_called_with('the_participant')
        audit_mock.assert_called_with(session=session,
                                      action=AuditActions.PRINT_HMR101,
                                      participant_ids=['the_participant'],
                                      nhs_numbers=['the_nhs_number'])
        get_participant_mock.assert_called_with('the_participant', True)

    def test_lambda_creates_episode_record_given_hmr101_form_data_and_no_existing_episode(
            self, log_mock, get_session_mock, query_episodes_mock, build_record_mock, create_replace_mock,
            get_nhs_number_mock, audit_mock, get_participant_mock):
        environment.datetime.now.return_value = self.FIXED_NOW_TIME
        lambda_event = {'pathParameters': {'participant_id': 'the_participant'},
                        'body': '{"hmr_101_form_data": {"sample_taker_code": "code", "clinical_data": "data"}}'}
        session = {'session_id': 'the_session', 'user_data': {'nhsid_useruid': 'the_user'}}
        get_session_mock.return_value = session
        query_episodes_mock.return_value = []
        build_record_mock.return_value = {'built_key': 'built_value'}
        get_nhs_number_mock.return_value = 'the_nhs_number'
        get_participant_mock.return_value = {'participant_id': 'the_participant'}

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(self.EXPECTED_OK_RESPONSE, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.HMREP0001}),
            call({'log_reference': LogReference.HMREP0002}),
            call({'log_reference': LogReference.HMREP0004})
        ]

        log_mock.assert_has_calls(expected_log_calls)
        get_session_mock.assert_called_with(lambda_event)
        query_episodes_mock.assert_called_with('the_participant', use_projection=False, segregate=True)
        build_record_mock.assert_called_with(
            'the_participant', {}, True, {'sample_taker_code': 'code', 'clinical_data': 'data'},
            '2020-04-16', session)
        create_replace_mock.assert_called_with({'built_key': 'built_value'})
        get_nhs_number_mock.assert_called_with('the_participant')
        audit_mock.assert_called_with(session=session,
                                      action=AuditActions.PRINT_HMR101,
                                      participant_ids=['the_participant'],
                                      nhs_numbers=['the_nhs_number'])
        get_participant_mock.assert_called_with('the_participant', True)

    def test_lambda_replaces_episode_record_given_no_hmr101_form_data_and_existing_episode(
            self, log_mock, get_session_mock, query_episodes_mock, build_record_mock, create_replace_mock,
            get_nhs_number_mock, audit_mock, get_participant_mock):
        environment.datetime.now.return_value = self.FIXED_NOW_TIME
        lambda_event = {'pathParameters': {'participant_id': 'the_participant'},
                        'body': '{"hmr_101_form_data": {"sample_taker_code": "code"}}'}
        session = {'session_id': 'the_session', 'user_data': {'nhsid_useruid': 'the_user'}}
        get_session_mock.return_value = session
        query_episodes_mock.return_value = [{'existing_episode_key': 'existing_episode_value'}]
        build_record_mock.return_value = {'built_key': 'built_value'}
        get_nhs_number_mock.return_value = 'the_nhs_number'
        get_participant_mock.return_value = {'participant_id': 'the_participant'}

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(self.EXPECTED_OK_RESPONSE, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.HMREP0001}),
            call({'log_reference': LogReference.HMREP0002}),
            call({'log_reference': LogReference.HMREP0003})
        ]

        log_mock.assert_has_calls(expected_log_calls)
        get_session_mock.assert_called_with(lambda_event)
        query_episodes_mock.assert_called_with('the_participant', use_projection=False, segregate=True)
        build_record_mock.assert_called_with('the_participant', {'existing_episode_key': 'existing_episode_value'},
                                             False, {'sample_taker_code': 'code'}, '2020-04-16', session)
        create_replace_mock.assert_called_with({'built_key': 'built_value'})
        get_nhs_number_mock.assert_called_with('the_participant')
        audit_mock.assert_called_with(session=session,
                                      action=AuditActions.PRINT_HMR101,
                                      participant_ids=['the_participant'],
                                      nhs_numbers=['the_nhs_number'])
        get_participant_mock.assert_called_with('the_participant', True)

    def test_lambda_replaces_episode_record_given_hmr101_form_data_and_existing_episode(
            self, log_mock, get_session_mock, query_episodes_mock, build_record_mock, create_replace_mock,
            get_nhs_number_mock, audit_mock, get_participant_mock):
        environment.datetime.now.return_value = self.FIXED_NOW_TIME
        lambda_event = {'pathParameters': {'participant_id': 'the_participant'},
                        'body': '{"hmr_101_form_data": {"sample_taker_code": "code", "clinical_data": "data"}}'}
        session = {'session_id': 'the_session', 'user_data': {'nhsid_useruid': 'the_user'}}
        get_session_mock.return_value = session
        query_episodes_mock.return_value = [{'existing_episode_key': 'existing_episode_value'}]
        build_record_mock.return_value = {'built_key': 'built_value'}
        get_nhs_number_mock.return_value = 'the_nhs_number'
        get_participant_mock.return_value = {'participant_id': 'the_participant'}

        actual_response = self.unwrapped_handler(lambda_event, {})

        self.assertDictEqual(self.EXPECTED_OK_RESPONSE, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.HMREP0001}),
            call({'log_reference': LogReference.HMREP0002}),
            call({'log_reference': LogReference.HMREP0003})
        ]

        log_mock.assert_has_calls(expected_log_calls)
        get_session_mock.assert_called_with(lambda_event)
        query_episodes_mock.assert_called_with('the_participant', use_projection=False, segregate=True)
        build_record_mock.assert_called_with(
            'the_participant', {'existing_episode_key': 'existing_episode_value'},
            False, {'sample_taker_code': 'code', 'clinical_data': 'data'}, '2020-04-16', session)
        create_replace_mock.assert_called_with({'built_key': 'built_value'})
        get_nhs_number_mock.assert_called_with('the_participant')
        audit_mock.assert_called_with(session=session,
                                      action=AuditActions.PRINT_HMR101,
                                      participant_ids=['the_participant'],
                                      nhs_numbers=['the_nhs_number'])
        get_participant_mock.assert_called_with('the_participant', True)
