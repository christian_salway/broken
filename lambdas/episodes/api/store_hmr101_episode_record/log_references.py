import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    HMREP0001 = (logging.INFO, 'Received request to get saved episode record')
    HMREP0002 = (logging.INFO, 'Looking for existing participant episode record')
    HMREP0003 = (logging.INFO, 'Updating existing episode record for participant with hmr101 form data')
    HMREP0004 = (logging.INFO, 'No current episode record. Creating new episode record for participant')

    HMREPEX0001 = (logging.ERROR, 'Unable to find participant id in path parameters')
    HMREPEX0002 = (logging.ERROR, 'Unable to find sample taker code in hmr101 form  data')
    HMREPEX0003 = (logging.ERROR, 'Participant not found')
