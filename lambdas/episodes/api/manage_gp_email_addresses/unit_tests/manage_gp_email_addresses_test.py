import json
from unittest import TestCase
from mock import patch, Mock, call
from manage_gp_email_addresses.log_references import LogReference


@patch('manage_gp_email_addresses.manage_gp_email_addresses.log')
@patch('manage_gp_email_addresses.manage_gp_email_addresses.get_api_request_info_from_event')
class TestManageGPNotificationEmails(TestCase):
    bad_request_response = 'Requested type not recognised - must be either email_PNL or email_NRL'

    @classmethod
    @patch('boto3.resource', Mock())
    def setUpClass(cls):
        import manage_gp_email_addresses.manage_gp_email_addresses as manage_emails_module
        global manage_emails_module
        from common.test_assertions.api_assertions import assertApiResponse
        global assertApiResponse

    def setUp(self):
        super(TestManageGPNotificationEmails, self).setUp()
        self.unwrapped_handler = manage_emails_module.lambda_handler.__wrapped__

        self.mock_user_id = {'participant_id': '123456'}

        self.mock_session = {
            'session_id': '12345678',
            'user_data': {
                'nhsid_useruid': self.mock_user_id
            },
            'selected_role': {
                'organisation_code': 'DXX',
                'organisation_name': 'YORKSHIRE AND THE HUMBER',
                'role_id': 'R8010',
                'role_name': 'CSAS Team Member'
            },
            'access_groups': {
                'pnl': True,
            }
        }

# Create new record

    @patch('manage_gp_email_addresses.manage_gp_email_addresses.add_email_address')
    def test_lambda_returns_200_for_successful_create_request(self, create_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'headers': None,
                'body': '{}',
                'requestContext': {'authorizer': {
                    'session': json.dumps(self.mock_session), 'principalId': 'blah'
                }},
            },
            'POST',
            '/api/organisation-supplemental'
        )
        create_mock.return_value = {}

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 200, {'data': {}}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPEMAILS0001}),
                              call({'log_reference': LogReference.GPEMAILS0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses.add_email_address')
    def test_lambda_returns_500_for_unsuccessful_create_request(self, create_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'headers': None,
                'body': '{}'
            },
            'POST',
            '/api/organisation-supplemental'
        )
        create_mock.side_effect = Mock(side_effect=Exception('Test'))

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 500, {'message': 'Cannot complete request'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPEMAILS0001}),
                              call({'log_reference': LogReference.GPEMAILS0003, 'error': 'Test'})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses.add_email_address')
    def test_lambda_returns_400_for_bad_type_create_request(self, create_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'headers': None,
                'body': '{}'
            },
            'POST',
            '/api/organisation-supplemental'
        )
        create_mock.side_effect = Mock(side_effect=KeyError(self.bad_request_response))

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 400,
                          {'message': "\'Requested type not recognised - must be either email_PNL or email_NRL\'"},
                          actual_response)

        expected_calls = [call({'log_reference': LogReference.GPEMAILS0001}),
                          call({'log_reference': LogReference.GPEMAILS0003,
                                'error': "'Requested type not recognised - must be either email_PNL or email_NRL'"})]

        log_mock.assert_has_calls(expected_calls)

# Read existing records

    @patch('manage_gp_email_addresses.manage_gp_email_addresses.get_email_addresses')
    def test_lambda_returns_data_and_200_for_successful_get_request(self, read_mock, event_data_mock, log_mock):
        organisation_code = '12345'
        read_response = [{'hi': 'ba'}]
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'organisation_code': organisation_code,
                },
                'headers': None,
                'body': '{}'
            },
            'GET',
            '/api/organisation-supplemental/{organisation_code}'
        )
        read_mock.return_value = read_response

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 200, {'data': read_response}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPEMAILS0001}),
                              call({'log_reference': LogReference.GPEMAILS0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses.get_email_addresses')
    def test_lambda_returns_500_for_failed_get_request(self, read_mock, event_data_mock, log_mock):
        organisation_code = '12345'
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'organisation_code': organisation_code,
                },
                'headers': None,
                'body': '{}'
            },
            'GET',
            '/api/organisation-supplemental/{organisation_code}'
        )
        read_mock.side_effect = Mock(side_effect=Exception('bad'))

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 500, {'message': 'Cannot complete request'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPEMAILS0001}),
                              call({'log_reference': LogReference.GPEMAILS0003, 'error': 'bad'})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses.get_email_addresses')
    def test_lambda_returns_400_for_bad_get_request(self, read_mock, event_data_mock, log_mock):
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'organisation_code': '12345',
                },
                'headers': None,
                'body': '{}'
            },
            'GET',
            '/api/organisation-supplemental/{organisation_code}'
        )
        read_mock.side_effect = Mock(side_effect=KeyError(self.bad_request_response))

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 400,
                          {'message': "\'Requested type not recognised - must be either email_PNL or email_NRL\'"},
                          actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPEMAILS0001}),
                              call({'log_reference': LogReference.GPEMAILS0003,
                                    'error': "'Requested type not recognised - must be either email_PNL or email_NRL'"})
                              ]

        log_mock.assert_has_calls(expected_log_calls)

# Delete existing records

    @patch('manage_gp_email_addresses.manage_gp_email_addresses.delete_email_address')
    def test_lambda_returns_200_for_successful_delete_request(self, delete_mock, event_data_mock, log_mock):
        organisation_code = '12345'
        organisation_supplemental_id = '54321'
        delete_response = {'message': 'record deleted'}
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'organisation_code': organisation_code,
                    'organisation_supplemental_id': organisation_supplemental_id
                },
                'headers': None,
                'body': '{}'
            },
            'DELETE',
            '/api/organisation-supplemental/{organisation_code}/{organisation_supplemental_id}'
        )
        delete_mock.return_value = delete_response

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 200, {'data': delete_response}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPEMAILS0001}),
                              call({'log_reference': LogReference.GPEMAILS0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses.delete_email_address')
    def test_lambda_returns_500_for_failed_delete_request(self, delete_mock, event_data_mock, log_mock):
        organisation_code = '12345'
        organisation_supplemental_id = '54321'
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'organisation_code': organisation_code,
                    'organisation_supplemental_id': organisation_supplemental_id
                },
                'headers': None,
                'body': '{}'
            },
            'DELETE',
            '/api/organisation-supplemental/{organisation_code}/{organisation_supplemental_id}'
        )
        delete_mock.side_effect = Mock(side_effect=Exception('Test'))

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 500, {'message': 'Cannot complete request'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPEMAILS0001}),
                              call({'log_reference': LogReference.GPEMAILS0003, 'error': 'Test'})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses.delete_email_address')
    def test_lambda_returns_404_for_missing_delete_request(self, delete_mock, event_data_mock, log_mock):
        organisation_code = '12345'
        organisation_supplemental_id = '54321'
        delete_response = None
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'organisation_code': organisation_code,
                    'organisation_supplemental_id': organisation_supplemental_id
                },
                'headers': None,
                'body': '{}'
            },
            'DELETE',
            '/api/organisation-supplemental/{organisation_code}/{organisation_supplemental_id}'
        )
        delete_mock.return_value = delete_response

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 404, {'message': 'record not found'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPEMAILS0001}),
                              call({'log_reference': LogReference.GPEMAILS0002})]

        log_mock.assert_has_calls(expected_log_calls)

# Bad request

    def test_lambda_returns_500_for_bad_request(self, event_data_mock, log_mock):
        organisation_code = '12345'
        organisation_supplemental_id = '54321'
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'organisation_code': organisation_code,
                    'organisation_supplemental_id': organisation_supplemental_id
                },
                'headers': None,
                'body': '{}'
            },
            'GET',
            '/api/organisation-supplemental/{organisation_code}/{organisation_supplemental_id}'
        )

        actual_response = self.unwrapped_handler({}, {})

        assertApiResponse(self, 500, {'message': 'Cannot complete request'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.GPEMAILS0001}),
                              call({'log_reference': LogReference.GPEMAILS0004})]

        log_mock.assert_has_calls(expected_log_calls)
