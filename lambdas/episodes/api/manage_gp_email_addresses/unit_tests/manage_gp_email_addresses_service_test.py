import json
from unittest import TestCase
from mock import patch, Mock, call
from boto3.dynamodb.conditions import Key
from common.utils.audit_utils import AuditActions
from common.utils.dynamodb_access.table_names import TableNames
from manage_gp_email_addresses.log_references import LogReference
import manage_gp_email_addresses.manage_gp_email_addresses_service as service_module

EXAMPLE_UUID = '1234-asdf-efecxc-2343-7'
PROJECTION_EXPRESSION = ', '.join([
    'organisation_code',
    'type_value',
    'organisation_supplemental_id'
])


@patch('manage_gp_email_addresses.manage_gp_email_addresses_service.log')
class TestManageGPNotificationEmails(TestCase):

    def setUp(self):
        self.mock_user_id = {'participant_id': '123456'}

        self.mock_session = {
            'session_id': '12345678',
            'user_data': {
                'nhsid_useruid': self.mock_user_id
            },
            'selected_role': {
                'organisation_code': '12345',
            },
            'access_groups': {
                'pnl': True,
            }
        }

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_put_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_create_calls_through(self, audit_mock, mock_uuid, dynamodb_put_item_mock, log_mock):
        # Given
        mock_uuid.uuid4.return_value = EXAMPLE_UUID

        # When
        event_data = {
            'body': {
                'organisation_code': '12345',
                'type': 'email_PNL',
                'email': '1234@nhs.org.uk'
            },
            'session': self.mock_session
        }
        actual_response = service_module.add_email_address(event_data)

        # Then
        expected_call = {
            'organisation_code': '12345',
            'type_value': 'email_PNL#1234@nhs.org.uk',
            'organisation_supplemental_id': EXAMPLE_UUID
        }
        dynamodb_put_item_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, expected_call)
        expected_response = {'message': 'record created'}
        self.assertEqual(expected_response, actual_response)
        audit_mock.assert_called_with(
            AuditActions.ADD_GP_PNL_EMAIL_ADDRESS,
            session=self.mock_session,
            organisation_code='12345',
            email_address='1234@nhs.org.uk'
        )

        expected_log_calls = [call({'log_reference': LogReference.CGPEMAILS0001}),
                              call({'log_reference': LogReference.CGPEMAILS0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_put_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_create_fails_appropriately_for_dynamodb_error(
            self,
            audit_mock,
            mock_uuid,
            dynamodb_put_item_mock,
            log_mock):
        # Given
        mock_uuid.uuid4.return_value = EXAMPLE_UUID
        dynamodb_put_item_mock.side_effect = Exception('Test')

        # When
        event_data = {
            'body': {
                'organisation_code': '12345',
                'type': 'email_PNL',
                'email': '1234@nhs.org.uk'
            },
            'session': self.mock_session
        }

        # Then
        with self.assertRaises(Exception):
            service_module.add_email_address(event_data)
        audit_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.CGPEMAILS0001}),
                              call({'log_reference': LogReference.CGPEMAILS0003})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_put_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_create_fails_appropriately_for_malformed_type(self, audit_mock, mock_uuid, _, log_mock):
        # Given
        mock_uuid.uuid4.return_value = EXAMPLE_UUID

        # When
        event_data = {
            'body': {
                'organisation_code': '12345',
                'type': 'email_flekdl',
                'email': '1234@nhs.org.uk'
            },
            'session': self.mock_session
        }

        # Then
        with self.assertRaises(KeyError):
            service_module.add_email_address(event_data)
        audit_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.CGPEMAILS0001})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_put_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_create_fails_appropriately_for_bad_body(self, audit_mock, mock_uuid, _, log_mock):
        for body in [
            {'organisation_code': '12345', 'email_address': 'wah@nhs.net'},
            {'email_address': 'blah@nhs.org', 'type': 'email_PNL'},
            {'organisation_code': '12345', 'type': 'email_PNL'}
        ]:
            # Given
            mock_uuid.uuid4.return_value = EXAMPLE_UUID

            # When
            event_data = {
                'body': body,
                'session': self.mock_session
            }

            # Then
            with self.assertRaises(KeyError):
                service_module.add_email_address(event_data)
            audit_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.CGPEMAILS0001})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_put_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_create_fails_appropriately_for_mismatched_organisation_codes(self, audit_mock, mock_uuid, _, log_mock):
        # Given
        mock_uuid.uuid4.return_value = EXAMPLE_UUID

        # When
        event_data = {
            'body': {
                'organisation_code': 'abcde',
                'type': 'email_PNL',
                'email': '1234@nhs.org.uk'
            },
            'session': self.mock_session
        }

        # Then
        response = service_module.add_email_address(event_data)
        audit_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.CGPEMAILS0001}),
                              call({'log_reference': LogReference.CGPEMAILS0003})]

        log_mock.assert_has_calls(expected_log_calls)

        self.assertEqual(400, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Cannot create email with different organisation code to session'}),
                         response['body'])

# Read existing records

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_query')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_get_fails_appropriately_for_malformed_type(self, audit_mock, mock_uuid, _, log_mock):
        # Given
        mock_uuid.uuid4.return_value = EXAMPLE_UUID

        # When
        event_data = {
            'path_parameters': {
                'organisation_code': 'abcde'
            },
            'query_parameters': {
                'type': 'email_PL'
            }
        }

        # Then
        with self.assertRaises(KeyError):
            service_module.get_email_addresses(event_data)
        audit_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.RGPEMAILS0001})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_query')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    def test_read_calls_through_PNL(self, mock_uuid, dynamodb_query_mock, log_mock):
        # Given
        dynamodb_query_mock.return_value = [{
            'organisation_code': '12345',
            'type_value': 'email_PNL#wah@nhs.co.uk',
            'organisation_supplemental_id': '12345'
            },
            {
            'organisation_code': '12345',
            'type_value': 'email_PNL#blah@nhs.co.uk',
            'organisation_supplemental_id': '54321'
            }
        ]

        # When
        event_data = {
            'path_parameters': {
                'organisation_code': '12345'
            },
            'query_parameters': {
                'type': 'email_PNL'
            },
            'session': self.mock_session
        }
        actual_response = service_module.get_email_addresses(event_data)

        # Then
        expected_condition = Key('organisation_code').eq('12345') & Key('type_value').begins_with('email_PNL#')
        dynamodb_query_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, dict(
            KeyConditionExpression=expected_condition,
            ProjectionExpression=PROJECTION_EXPRESSION
        ))
        expected_response = [{
            'organisation_code': '12345',
            'type': 'email_PNL',
            'email': 'wah@nhs.co.uk',
            'organisation_supplemental_id': '12345'
            },
            {
            'organisation_code': '12345',
            'type': 'email_PNL',
            'email': 'blah@nhs.co.uk',
            'organisation_supplemental_id': '54321'
            }
        ]
        self.assertEqual(expected_response, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.RGPEMAILS0001}),
                              call({'log_reference': LogReference.RGPEMAILS0002, 'organisation_code': '12345'}),
                              call({'log_reference': LogReference.RGPEMAILS0003})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_query')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    def test_read_calls_through_both(self, mock_uuid, dynamodb_query_mock, log_mock):
        # Given
        dynamodb_query_mock.return_value = [{
            'organisation_code': '12345',
            'type_value': 'email_PNL#wah@nhs.co.uk',
            'organisation_supplemental_id': '12345'
            },
            {
            'organisation_code': '12345',
            'type_value': 'email_NRL#blah@nhs.co.uk',
            'organisation_supplemental_id': '54321'
            }
        ]

        # When
        event_data = {
            'path_parameters': {
                'organisation_code': '12345'
            },
            'query_parameters': {},
            'session': self.mock_session
        }
        actual_response = service_module.get_email_addresses(event_data)

        # Then
        expected_condition_expression = Key('organisation_code').eq('12345') & Key('type_value').begins_with('email_')
        dynamodb_query_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, dict(
            KeyConditionExpression=expected_condition_expression,
            ProjectionExpression=PROJECTION_EXPRESSION
        ))
        expected_response = [{
            'organisation_code': '12345',
            'type': 'email_PNL',
            'email': 'wah@nhs.co.uk',
            'organisation_supplemental_id': '12345'
            },
            {
            'organisation_code': '12345',
            'type': 'email_NRL',
            'email': 'blah@nhs.co.uk',
            'organisation_supplemental_id': '54321'
            }
        ]
        self.assertEqual(expected_response, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.RGPEMAILS0001}),
                              call({'log_reference': LogReference.RGPEMAILS0002, 'organisation_code': '12345'}),
                              call({'log_reference': LogReference.RGPEMAILS0003})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_query')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    def test_read_fails_throws_exception(self, mock_uuid, dynamodb_query_mock, log_mock):
        # Given
        mock_uuid.uuid4.return_value = EXAMPLE_UUID
        dynamodb_query_mock.side_effect = Exception('Test')

        # When
        event_data = {
            'path_parameters': {
                'organisation_code': '12345'
            },
            'query_parameters': {},
            'session': self.mock_session
        }
        with self.assertRaises(Exception):
            service_module.get_email_addresses(event_data)

        expected_log_calls = [call({'log_reference': LogReference.RGPEMAILS0001}),
                              call({'log_reference': LogReference.RGPEMAILS0002, 'organisation_code': '12345'}),
                              call({'log_reference': LogReference.RGPEMAILS0004})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_put_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_read_fails_appropriately_for_mismatched_organisation_codes(self, audit_mock, mock_uuid, _, log_mock):
        # Given
        mock_uuid.uuid4.return_value = EXAMPLE_UUID

        # When
        event_data = {
            'path_parameters': {
                'organisation_code': 'abcde'
            },
            'query_parameters': {},
            'session': self.mock_session
        }

        # Then
        response = service_module.get_email_addresses(event_data)
        audit_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.RGPEMAILS0001}),
                              call({'log_reference': LogReference.RGPEMAILS0005,
                                    'organisation_code': 'abcde',
                                    'session organisation code': '12345'})]

        log_mock.assert_has_calls(expected_log_calls)

        self.assertEqual(400, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Requested organisation code did not match session organisation code'}),
                         response['body'])

# Delete Existing Records

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_delete_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service._get_email_address_by_id')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_delete_calls_through(self, audit_mock, get_email_address_by_id_mock, dynamodb_delete_item_mock, log_mock):
        # Given
        get_email_address_by_id_mock.return_value = {
            'organisation_code': '12345',
            'type_value': 'email_NRL#b@d.net',
            'organisation_supplemental_id': EXAMPLE_UUID
        }

        # When
        event_data = {
            'path_parameters': {
                'organisation_supplemental_id': '54321'
            },
            'session': self.mock_session
        }
        actual_response = service_module.delete_email_address(event_data)

        # Then
        expected_key = {
            'organisation_code': '12345',
            'type_value': 'email_NRL#b@d.net',
        }
        dynamodb_delete_item_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, expected_key)
        expected_response = {'message': 'record deleted'}
        self.assertEqual(expected_response, actual_response)
        audit_mock.assert_called_with(
            AuditActions.REMOVE_GP_NRL_EMAIL_ADDRESS,
            session=self.mock_session,
            organisation_code='12345',
            email_address='b@d.net'
        )

        expected_log_calls = [call({'log_reference': LogReference.DGPEMAILS0001}),
                              call({'log_reference': LogReference.DGPEMAILS0002, 'id': '54321'}),
                              call({'log_reference': LogReference.DGPEMAILS0006})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_delete_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service._get_email_address_by_id')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_delete_calls_through_for_no_result(
            self,
            audit_mock,
            get_email_address_by_id_mock,
            dynamodb_delete_item_mock,
            log_mock):
        # Given
        get_email_address_by_id_mock.return_value = None

        # When
        event_data = {
            'path_parameters': {
                'organisation_supplemental_id': '12345'
            },
            'session': self.mock_session
        }
        actual_response = service_module.delete_email_address(event_data)

        # Then
        dynamodb_delete_item_mock.assert_not_called()
        expected_response = None
        self.assertEqual(expected_response, actual_response)
        audit_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.DGPEMAILS0001}),
                              call({'log_reference': LogReference.DGPEMAILS0002, 'id': '12345'}),
                              call({'log_reference': LogReference.DGPEMAILS0005})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_delete_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service._get_email_address_by_id')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_delete_fails_appropriately_when_get_fails(
        self, audit_mock, get_email_address_by_id_mock, _, log_mock
    ):
        # Given
        get_email_address_by_id_mock.side_effect = Mock(side_effect=Exception('Test'))

        # When
        event_data = {
            'body': {
                'organisation_code': '12345',
                'type': 'email_PNL',
                'email': '1234@nhs.org.uk'
            },
            'session': self.mock_session
        }

        # Then
        with self.assertRaises(Exception):
            service_module.delete_email_address(event_data)
        audit_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.DGPEMAILS0001})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_delete_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service._get_email_address_by_id')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_delete_fails_appropriately_when_delete_fails(
        self, audit_mock, get_email_address_by_id_mock, dynamodb_delete_item_mock, log_mock
    ):
        # Given
        get_email_address_by_id_mock.return_value = {
            'organisation_code': '12345',
            'type_value': 'email_NRL#b@d.net',
            'organisation_supplemental_id': EXAMPLE_UUID
        }
        dynamodb_delete_item_mock.side_effect = Exception('Test')

        # When
        event_data = {
            'path_parameters': {
                'organisation_supplemental_id': '12345'
            },
            'session': self.mock_session
        }

        # Then
        with self.assertRaises(Exception):
            service_module.delete_email_address(event_data)
        audit_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.DGPEMAILS0001}),
                              call({'log_reference': LogReference.DGPEMAILS0002, 'id': '12345'}),
                              call({'log_reference': LogReference.DGPEMAILS0007, 'error': 'Test'})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_put_item')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service._get_email_address_by_id')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.audit')
    def test_delete_fails_appropriately_for_mismatched_organisation_codes(self, audit_mock,
                                                                          get_email_address_by_id_mock,
                                                                          _,
                                                                          log_mock):
        # Given
        get_email_address_by_id_mock.return_value = {
            'organisation_code': 'abcde',
            'type_value': 'email_NRL#b@d.net',
            'organisation_supplemental_id': EXAMPLE_UUID
        }

        # When
        event_data = {
            'path_parameters': {
                'organisation_supplemental_id': '12345'
            },
            'session': self.mock_session
        }

        # Then
        response = service_module.delete_email_address(event_data)
        audit_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.DGPEMAILS0002, 'id': '12345'}),
                              call({'log_reference': LogReference.DGPEMAILS0007})]

        log_mock.assert_has_calls(expected_log_calls)

        self.assertEqual(400, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Cannot delete email with different organisation code to session'}),
                         response['body'])

# Get record by id

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_query')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    def test_get_by_id_calls_through(self, mock_uuid, dynamodb_query_mock, log_mock):
        # Given
        dynamodb_query_mock.return_value = [{
            'organisation_code': 'abcde',
            'type_value': 'email_PNL#wah@nhs.co.uk',
            'organisation_supplemental_id': '12345'
            }
        ]

        # When
        actual_response = service_module._get_email_address_by_id('12345')

        # Then
        expected_condition_expression = Key('organisation_supplemental_id').eq('12345')

        dynamodb_query_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, dict(
            IndexName='organisation_supplemental_id',
            KeyConditionExpression=expected_condition_expression,
            ProjectionExpression=PROJECTION_EXPRESSION
        ))

        expected_response = {
                'organisation_code': 'abcde',
                'type_value': 'email_PNL#wah@nhs.co.uk',
                'organisation_supplemental_id': '12345'
                }
        self.assertEqual(expected_response, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.DGPEMAILS0003,
                                    'organisation_supplemental_id': '12345'})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_query')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    def test_get_by_id_calls_through_for_no_result(self, mock_uuid, dynamodb_query_mock, log_mock):
        # Given
        dynamodb_query_mock.return_value = []

        # When
        actual_response = service_module._get_email_address_by_id('12345')

        # Then
        expected_condition_expression = Key('organisation_supplemental_id').eq('12345')
        dynamodb_query_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, dict(
            IndexName='organisation_supplemental_id',
            KeyConditionExpression=expected_condition_expression,
            ProjectionExpression=PROJECTION_EXPRESSION
        ))
        expected_response = None
        self.assertEqual(expected_response, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.DGPEMAILS0003,
                                    'organisation_supplemental_id': '12345'})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.dynamodb_query')
    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid')
    def test_get_by_id_fails_throws_exception(self, mock_uuid, dynamodb_query_mock, log_mock):
        # Given
        dynamodb_query_mock.side_effect = Exception('Test')

        # When
        with self.assertRaises(Exception):
            service_module._get_email_address_by_id('12345')

        expected_log_calls = [call({'log_reference': LogReference.DGPEMAILS0004, 'id': '12345', 'error': 'Test'})]

        log_mock.assert_has_calls(expected_log_calls)
