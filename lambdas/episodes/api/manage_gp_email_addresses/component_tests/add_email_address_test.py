import json
from unittest import TestCase
from unittest.mock import patch, Mock, call

import manage_gp_email_addresses.manage_gp_email_addresses as manage_emails_module

environ_mock = {
    'AUDIT_QUEUE_URL': 'AUDIT QUEUE',
}

organisation_supplemental_table_mock = Mock()
sqs_client_mock = Mock()

mock_uuid = 'TEST_UUID'


@patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
       Mock(return_value=organisation_supplemental_table_mock))
@patch('manage_gp_email_addresses.manage_gp_email_addresses_service.uuid.uuid4',
       Mock(return_value=mock_uuid))
@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('os.environ', environ_mock)
class TestAddGpEmailAddress(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        organisation_supplemental_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.audit_utils.datetime')
    @patch('common.log.logging')
    def test_add_email_address_successful(self, logging_mock, audit_datetime_mock):
        log_mock = Mock()
        logging_mock.getLogger.return_value = log_mock
        audit_datetime_mock.now.return_value.isoformat.return_value = '2020-04-23'

        address_to_add = {
            'organisation_code': '12345',
            'type': 'email_PNL',
            'email': '1234@nhs.org.uk'
        }
        test_event = self._build_event('12345', address_to_add)
        test_context = Mock()
        test_context.function_name = ''

        expected_response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json",
                "X-Content-Type-Options": "nosniff",
                "Strict-Transport-Security": "max-age=1576800"
            },
            "body": '{"data": {"message": "record created"}}',
            "isBase64Encoded": False
        }

        response = manage_emails_module.lambda_handler(test_event, test_context)

        self.assertEqual(expected_response, response)

        self.assertEqual(organisation_supplemental_table_mock.put_item.call_count, 1)
        organisation_supplemental_table_mock.put_item.assert_has_calls([
            call(Item={'organisation_code': '12345', 'type_value': 'email_PNL#1234@nhs.org.uk',
                       'organisation_supplemental_id': mock_uuid})
        ])

        self.assertEqual(sqs_client_mock.send_message.call_count, 1)
        sqs_client_mock.send_message.assert_has_calls([
            call(QueueUrl='AUDIT QUEUE',
                 MessageBody='{"action": "ADD_GP_PNL_EMAIL_ADDRESS", '
                             '"timestamp": "2020-04-23", '
                             '"internal_id": "requestID", '
                             '"nhsid_useruid": "u-12345", '
                             '"session_id": "s-12345", '
                             '"first_name": null, '
                             '"last_name": null, '
                             '"role_id": "abcde", '
                             '"user_organisation_code": "12345", '
                             '"organisation_code": "12345", '
                             '"email_address": "1234@nhs.org.uk"}')
        ])

        expected_log_calls = [
            call(20, '{"log_reference": "LAMBDA0000", "message": "Lambda started"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0007", "assigned_workgroups": [], "message": "Setting user workgroups from session"}', exc_info=False), 
            call(20, '{"log_reference": "GPEMAILS0001", "message": "Received request to change GP emails"}', exc_info=False), 
            call(20, '{"log_reference": "CGPEMAILS0001", "message": "creating email record"}', exc_info=False),
            call(20, '{"log_reference": "CGPEMAILS0002", "message": "email record created"}', exc_info=False),
            call(20, '{"log_reference": "AUDIT0001", "message": "Successfully sent an audit record to the audit queue"}', exc_info=False), 
            call(20, '{"log_reference": "GPEMAILS0002", "message": "Request successful"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0001", "message": "Lambda exited with status success"}', exc_info=False)
        ]

        log_mock.log.assert_has_calls(expected_log_calls, any_order=True)

    @patch('common.log.logging')
    def test_cannot_add_email_address_with_different_session_org_code(self, logging_mock):
        log_mock = Mock()
        logging_mock.getLogger.return_value = log_mock

        address_to_add = {
            'organisation_code': '12345',
            'type': 'email_PNL',
            'email': '1234@nhs.org.uk'
        }
        test_event = self._build_event('99999', address_to_add)
        test_context = Mock()
        test_context.function_name = ''

        expected_response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json",
                "X-Content-Type-Options": "nosniff",
                "Strict-Transport-Security": "max-age=1576800"
            },
            "body": '{"data": {"statusCode": 400, "headers": {"Content-Type": '
                    '"application/json", "X-Content-Type-Options": "nosniff", '
                    '"Strict-Transport-Security": "max-age=1576800"}, "body": '
                    '"{\\"message\\": \\"Cannot create email with different organisation '
                    'code to session\\"}", "isBase64Encoded": false}}',
            "isBase64Encoded": False
        }

        response = manage_emails_module.lambda_handler(test_event, test_context)

        self.assertEqual(expected_response, response)

        organisation_supplemental_table_mock.put_item.assert_not_called()

        sqs_client_mock.send_message.assert_not_called()

        expected_log_calls = [
            call(20, '{"log_reference": "LAMBDA0000", "message": "Lambda started"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0007", "assigned_workgroups": [], "message": "Setting user workgroups from session"}', exc_info=False), 
            call(20, '{"log_reference": "GPEMAILS0001", "message": "Received request to change GP emails"}', exc_info=False), 
            call(20, '{"log_reference": "CGPEMAILS0001", "message": "creating email record"}', exc_info=False),
            call(40, '{"log_reference": "CGPEMAILS0003", "message": "failed to create record"}', exc_info=False),
            call(20, '{"log_reference": "GPEMAILS0002", "message": "Request successful"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0001", "message": "Lambda exited with status success"}', exc_info=False)
        ]

        log_mock.log.assert_has_calls(expected_log_calls, any_order=True)

    def _build_event(
            self,
            user_org_code,
            address_to_add):
        return {
            'httpMethod': 'POST',
            'resource': '/api/organisation-supplemental',
            'pathParameters': {
                'participant_id': 'p-12345',
                'organisation_code': '12345'
            },
            'queryStringParameters': {
                'type': 'email_PNL'
            },
            'headers': {
                'request_id': 'requestID',
            },
            'body': json.dumps(address_to_add),
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': json.dumps({
                        'session_id': 's-12345',
                        'user_data': {
                            'nhsid_useruid': 'u-12345'
                        },
                        'selected_role': {
                            'organisation_code': user_org_code,
                            'role_id': 'abcde'
                        },
                        'access_groups': {
                            'pnl': True,
                        }
                    })
                }
            }
        }
