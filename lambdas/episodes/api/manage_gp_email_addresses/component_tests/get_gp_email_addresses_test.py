import json
from unittest import TestCase
from unittest.mock import patch, Mock, call

import manage_gp_email_addresses.manage_gp_email_addresses as manage_emails_module

organisation_supplemental_table_mock = Mock()

environ_mock = {
    'AUDIT_QUEUE_URL': 'AUDIT QUEUE',
}


@patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
       Mock(return_value=organisation_supplemental_table_mock))
class TestGetGpEmailAddresses(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        organisation_supplemental_table_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.Key')
    @patch('common.log.logging')
    def test_get_email_addresses_successful(self, logging_mock, condition_key_mock):
        log_mock = Mock()
        logging_mock.getLogger.return_value = log_mock
        condition_key_mock.return_value.eq.return_value = True
        condition_key_mock.return_value.begins_with.return_value = True

        test_event = self._build_event('12345')
        test_context = Mock()
        test_context.function_name = ''

        email_objects = [
            {
                'organisation_code': '12345',
                'type_value': 'email_PNL#wah@nhs.co.uk',
                'organisation_supplemental_id': '12345'
            }, {
                'organisation_code': '12345',
                'type_value': 'email_PNL#blah@nhs.co.uk',
                'organisation_supplemental_id': '54321'
            }
        ]

        organisation_supplemental_table_mock.query.return_value = {'Items': email_objects}

        expected_emails = [
            {
                'organisation_code': '12345',
                'type': 'email_PNL',
                'email': 'wah@nhs.co.uk',
                'organisation_supplemental_id': '12345'
            },
            {
                'organisation_code': '12345',
                'type': 'email_PNL',
                'email': 'blah@nhs.co.uk',
                'organisation_supplemental_id': '54321'
            }
        ]

        expected_response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json",
                "X-Content-Type-Options": "nosniff",
                "Strict-Transport-Security": "max-age=1576800"
            },
            "body": json.dumps({"data": expected_emails}),
            "isBase64Encoded": False
        }

        response = manage_emails_module.lambda_handler(test_event, test_context)

        self.assertEqual(expected_response, response)

        self.assertEqual(organisation_supplemental_table_mock.query.call_count, 1)
        organisation_supplemental_table_mock.query.assert_has_calls([
            call(KeyConditionExpression=True,
                 ProjectionExpression='organisation_code, type_value, organisation_supplemental_id')
        ])

        expected_log_calls = [
            call(20, '{"log_reference": "LAMBDA0000", "message": "Lambda started"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0007", "assigned_workgroups": [], "message": "Setting user workgroups from session"}', exc_info=False),  
            call(20, '{"log_reference": "GPEMAILS0001", "message": "Received request to change GP emails"}', exc_info=False),  
            call(20, '{"log_reference": "RGPEMAILS0001", "message": "reading email"}', exc_info=False),
            call(20, '{"log_reference": "RGPEMAILS0002", "organisation_code": "12345", "message": "successfully built request"}', exc_info=False),  
            call(20, '{"log_reference": "RGPEMAILS0003", "message": "successfully read email"}', exc_info=False),
            call(20, '{"log_reference": "GPEMAILS0002", "message": "Request successful"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0001", "message": "Lambda exited with status success"}', exc_info=False)
        ]

        log_mock.log.assert_has_calls(expected_log_calls, any_order=True)

    @patch('manage_gp_email_addresses.manage_gp_email_addresses_service.Key')
    @patch('common.log.logging')
    def test_cannot_get_email_addresses_with_different_session_org_code(self, logging_mock, condition_key_mock):
        log_mock = Mock()
        logging_mock.getLogger.return_value = log_mock
        condition_key_mock.return_value.eq.return_value = True
        condition_key_mock.return_value.begins_with.return_value = True
        test_event = self._build_event('99999')
        test_context = Mock()
        test_context.function_name = ''

        email_objects = [
            {
                'organisation_code': '12345',
                'type_value': 'email_PNL#wah@nhs.co.uk',
                'organisation_supplemental_id': '12345'
            }, {
                'organisation_code': '12345',
                'type_value': 'email_PNL#blah@nhs.co.uk',
                'organisation_supplemental_id': '54321'
            }
        ]

        organisation_supplemental_table_mock.query.return_value = {'Items': email_objects}

        expected_response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json",
                "X-Content-Type-Options": "nosniff",
                "Strict-Transport-Security": "max-age=1576800"
            },
            "body": '{"data": {"statusCode": 400, "headers": {"Content-Type": "application/json", "X-Content-Type-Options": "nosniff", "Strict-Transport-Security": "max-age=1576800"}, "body": "{\\"message\\": \\"Requested organisation code did not match session organisation code\\"}", "isBase64Encoded": false}}', 
            "isBase64Encoded": False
        }

        response = manage_emails_module.lambda_handler(test_event, test_context)

        self.assertEqual(expected_response, response)

        organisation_supplemental_table_mock.query.assert_not_called()

        expected_log_calls = [
            call(20, '{"log_reference": "LAMBDA0000", "message": "Lambda started"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0007", "assigned_workgroups": [], "message": "Setting user workgroups from session"}', exc_info=False),  
            call(20, '{"log_reference": "GPEMAILS0001", "message": "Received request to change GP emails"}', exc_info=False),  
            call(20, '{"log_reference": "RGPEMAILS0001", "message": "reading email"}', exc_info=False),
            call(40, '{"log_reference": "RGPEMAILS0005", "organisation_code": "12345", "session organisation code": "99999", "message": "requested organisation code did not match user session organisation code"}', exc_info=False), 
            call(20, '{"log_reference": "GPEMAILS0002", "message": "Request successful"}', exc_info=False),
            call(20, '{"log_reference": "LAMBDA0001", "message": "Lambda exited with status success"}', exc_info=False)
        ]

        log_mock.log.assert_has_calls(expected_log_calls, any_order=True)

    def _build_event(
            self,
            user_org_code):
        return {
            'httpMethod': 'GET',
            'resource': '/api/organisation-supplemental/{organisation_code}',
            'pathParameters': {
                'participant_id': 'p-12345',
                'organisation_code': '12345'
            },
            'queryStringParameters': {
                'type': 'email_PNL'
            },
            'headers': {
                'request_id': 'requestID',
            },
            'body': '{}',
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': json.dumps({
                        'session_id': 's-12345',
                        'user_data': {
                            'nhsid_useruid': 'u-12345'
                        },
                        'selected_role': {
                            'organisation_code': user_org_code,
                            'role_id': 'abcde'
                        },
                        'access_groups': {
                            'pnl': True,
                        }
                    })
                }
            }
        }
