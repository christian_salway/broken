from manage_gp_email_addresses.log_references import LogReference
from manage_gp_email_addresses.manage_gp_email_addresses_service import \
    add_email_address, get_email_addresses, delete_email_address

from common.log import log
from common.utils import json_return_object, json_return_message
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.lambda_wrappers import lambda_entry_point


@lambda_entry_point
def lambda_handler(event, context):
    BASE_URL = '/api/organisation-supplemental'
    REQUEST_FUNCTIONS = {
        ('POST', BASE_URL): add_email_address,
        ('GET', BASE_URL + '/{organisation_code}'): get_email_addresses,
        ('DELETE', BASE_URL + '/{organisation_code}/{organisation_supplemental_id}'): delete_email_address,
    }
    event_data, http_method, resource = get_api_request_info_from_event(event)

    log({'log_reference': LogReference.GPEMAILS0001})

    try:
        response = REQUEST_FUNCTIONS.get((http_method, resource), bad_request)(event_data)
    except KeyError as e:
        log({'log_reference': LogReference.GPEMAILS0003, 'error': str(e)})
        return json_return_message(400, str(e))
    except Exception as e:
        log({'log_reference': LogReference.GPEMAILS0003, 'error': str(e)})
        return json_return_message(500, 'Cannot complete request')

    log({'log_reference': LogReference.GPEMAILS0002})
    if response is None and http_method == 'DELETE':
        return json_return_message(404, 'record not found')
    return json_return_object(200, {'data': response})


def bad_request(event_data):
    log({'log_reference': LogReference.GPEMAILS0004})
    raise Exception('unrecognised method/resource combination')
