import uuid
from boto3.dynamodb.conditions import Key
from common.log import log
from common.utils import json_return_message
from common.utils.dynamodb_access.table_names import TableNames
from manage_gp_email_addresses.log_references import LogReference
from common.utils.audit_utils import audit, AuditActions
from common.utils.organisation_supplementary_utils import EMAIL_PREFIX
from common.utils.dynamodb_access.operations import dynamodb_delete_item, dynamodb_put_item, dynamodb_query

UNRECOGNISED_TYPE = 'Requested type not recognised - must be either email_PNL or email_NRL'

PROJECTION_EXPRESSION = ', '.join([
    'organisation_code',
    'type_value',
    'organisation_supplemental_id'
])

CREATE_ACTIONS = {
    'email_PNL': AuditActions.ADD_GP_PNL_EMAIL_ADDRESS,
    'email_NRL': AuditActions.ADD_GP_NRL_EMAIL_ADDRESS
}

DELETE_ACTIONS = {
    'email_PNL': AuditActions.REMOVE_GP_PNL_EMAIL_ADDRESS,
    'email_NRL': AuditActions.REMOVE_GP_NRL_EMAIL_ADDRESS
}


def add_email_address(event_data):
    log({'log_reference': LogReference.CGPEMAILS0001})
    user_org_code = event_data['session']['selected_role']['organisation_code']
    organisation_code = event_data['body']['organisation_code']
    if organisation_code == user_org_code:
        email_type = event_data['body']['type']
        if email_type not in ['email_PNL', 'email_NRL']:
            raise KeyError(UNRECOGNISED_TYPE)
        email_address = event_data['body']['email']
        email_record = email_type + '#' + email_address
        record_to_add = {
            'organisation_code': organisation_code,
            'type_value': email_record,
            'organisation_supplemental_id': str(uuid.uuid4())
        }
        try:
            dynamodb_put_item(TableNames.ORGANISATION_SUPPLEMENTAL, record_to_add)
            log({'log_reference': LogReference.CGPEMAILS0002})
        except Exception as e:
            log({'log_reference': LogReference.CGPEMAILS0003})
            raise e
    else:
        log({'log_reference': LogReference.CGPEMAILS0003})
        return json_return_message(400, 'Cannot create email with different organisation code to session')

    audit_action = CREATE_ACTIONS.get(email_type)
    audit(
        audit_action,
        session=event_data.get('session'),
        organisation_code=organisation_code,
        email_address=email_address
    )
    return {'message': 'record created'}


def get_email_addresses(event_data):
    log({'log_reference': LogReference.RGPEMAILS0001})
    user_org_code = event_data['session']['selected_role']['organisation_code']
    organisation_code = event_data['path_parameters'].get('organisation_code')

    if organisation_code == user_org_code:
        query_type = event_data['query_parameters'].get('type', '')
        if query_type:
            if query_type not in ['email_PNL', 'email_NRL']:
                raise KeyError(UNRECOGNISED_TYPE)
            prefix = query_type + '#'
            condition_expression = Key('organisation_code').eq(organisation_code) & Key('type_value').begins_with(
                prefix)
        else:
            condition_expression = (
                    Key('organisation_code').eq(organisation_code) &
                    Key('type_value').begins_with(EMAIL_PREFIX)
            )
        log({'log_reference': LogReference.RGPEMAILS0002, 'organisation_code': organisation_code})
        try:
            items = dynamodb_query(TableNames.ORGANISATION_SUPPLEMENTAL, dict(
                KeyConditionExpression=condition_expression,
                ProjectionExpression=PROJECTION_EXPRESSION
            ))

            log({'log_reference': LogReference.RGPEMAILS0003})
        except Exception as e:
            log({'log_reference': LogReference.RGPEMAILS0004})
            raise e
    else:
        log({'log_reference': LogReference.RGPEMAILS0005,
             'organisation_code': organisation_code,
            'session organisation code': user_org_code})
        return json_return_message(400, 'Requested organisation code did not match session organisation code')

    return [convert_to_api_format(item) for item in items]


def delete_email_address(event_data):
    log({'log_reference': LogReference.DGPEMAILS0001})
    user_org_code = event_data['session']['selected_role']['organisation_code']
    organisation_supplemental_id = event_data['path_parameters'].get('organisation_supplemental_id')
    log({'log_reference': LogReference.DGPEMAILS0002, 'id': organisation_supplemental_id})
    existing_data = _get_email_address_by_id(organisation_supplemental_id)
    if not existing_data:
        log({'log_reference': LogReference.DGPEMAILS0005})
        return None
    organisation_code = existing_data.get('organisation_code')
    if organisation_code == user_org_code:
        type_value = existing_data.get('type_value')
        [email_type, email_address] = type_value.split('#', 1)
        try:
            dynamodb_delete_item(TableNames.ORGANISATION_SUPPLEMENTAL, {
                'organisation_code': organisation_code,
                'type_value': type_value
            })
            log({'log_reference': LogReference.DGPEMAILS0006})
            audit_action = DELETE_ACTIONS.get(email_type)
            audit(
                audit_action,
                session=event_data.get('session'),
                organisation_code=organisation_code,
                email_address=email_address
            )
        except Exception as e:
            log({'log_reference': LogReference.DGPEMAILS0007, 'error': str(e)})
            raise e
    else:
        log({'log_reference': LogReference.DGPEMAILS0007})
        return json_return_message(400, 'Cannot delete email with different organisation code to session')
    return {'message': 'record deleted'}


def _get_email_address_by_id(organisation_supplemental_id):
    condition_expression = Key('organisation_supplemental_id').eq(organisation_supplemental_id)
    try:
        items = dynamodb_query(TableNames.ORGANISATION_SUPPLEMENTAL, dict(
            IndexName='organisation_supplemental_id',
            KeyConditionExpression=condition_expression,
            ProjectionExpression=PROJECTION_EXPRESSION
        ))

        log({'log_reference': LogReference.DGPEMAILS0003, 'organisation_supplemental_id': organisation_supplemental_id})
    except Exception as e:
        log({
            'log_reference': LogReference.DGPEMAILS0004,
            'id': organisation_supplemental_id,
            'error': str(e)
        })
        raise e
    if items:
        return items[0]
    return None


def convert_to_api_format(email_record):
    [email_type, email] = email_record['type_value'].split('#', 1)
    return {
        "organisation_code": email_record['organisation_code'],
        "type": email_type,
        "email": email,
        "organisation_supplemental_id": email_record['organisation_supplemental_id']
    }
