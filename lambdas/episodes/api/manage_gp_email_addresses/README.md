This lambda is to sit behind the screening_api API Gateway and serve responses for the front end.
It creates, reads, and deletes records from the organisation_supplemental table for the weekly email notifications for the Prior Notification List and Non Responder List.

## Request
```  POST /organisation-supplemental ```

Body:
```
 { “organisation_code”: “AB12345”, 
   “type”: “email_PNL”, 
   “email”: "a.smith@nhs.net" 
 }
```

## Response
* Scenario: Record created found

    Status code: 200 OK 

    Body: 
    ```
    {
      data: {
        'message': 'record created'
      }
    }
    ```
     
## Request
```  GET /organisation-supplemental/{organisation_code}?type={type} ```

#### Required Params
``` organisation_code=string ```

#### Optional Params
``` type (query parameter)='email_PNL' | 'email_NRL'```
This retrieves either just the PNL email addresses or the NRL email addresses. When omitting it both are returned

## Response
* Scenario: Results found

    Status code: 200 OK 

    Body: 
    ```
    {
      data: [ 
            {“organisation_code”: “AB12345”, 
            “type”: “email_PNL”, 
            “email”: "a.smith@nhs.net",
            "organisation_supplemental_id": "eac5b180-297a-44a5-b7df-53e5dae0aa27"}, 
            {“organisation_code”: “AB12345”, 
            “type”: “email_NRL”, 
            “email”: "a.smith@nhs.net",
            "organisation_supplemental_id": "72ac0b45-153a-4028-abac-c11cfff23eb2"}, 
            {“organisation_code”: “AB12345”, 
            “type”: “email_PNL”, 
            “email”: "a.smith@nhs.net",
            "organisation_supplemental_id": "1220dd01-6b67-4e97-87d6-e90bb839bffc"}
]
    }
    ```

* Scenario: No results found

    Status code: 200 OK 

    Body: 
     ``` [] ```

## Delete Request

 ``` DELETE /organisation-supplemental/{organisation_code}/{organisation_supplemental_id} ```

#### Required Params
```organisation_code=string
organisation_supplemental_id=string```

## Response
* Scenario: Record deleted

    Status code: 200 OK 

    Body: 
    ```
    {
      data: {
        'message': 'record deleted'
      }
    }
    ```

* Scenario: Record not found

    Status code: 404 Not found 

    Body: 
    ```
    {
      data: {
        'message': 'record not found'
      }
    }
    ```