import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    GPEMAILS0001 = (logging.INFO, 'Received request to change GP emails')
    GPEMAILS0002 = (logging.INFO, 'Request successful')
    GPEMAILS0003 = (logging.ERROR, 'Request failure')
    GPEMAILS0004 = (logging.ERROR, 'Requested method/resource not supported')

    CGPEMAILS0001 = (logging.INFO, 'creating email record')
    CGPEMAILS0002 = (logging.INFO, 'email record created')
    CGPEMAILS0003 = (logging.ERROR, 'failed to create record')

    RGPEMAILS0001 = (logging.INFO, 'reading email')
    RGPEMAILS0002 = (logging.INFO, 'successfully built request')
    RGPEMAILS0003 = (logging.INFO, 'successfully read email')
    RGPEMAILS0004 = (logging.ERROR, 'dynamodb query failed')
    RGPEMAILS0005 = (logging.ERROR, 'requested organisation code did not match user session organisation code')

    DGPEMAILS0001 = (logging.INFO, 'deleting email')
    DGPEMAILS0002 = (logging.INFO, 'attempting to retrieve record')
    DGPEMAILS0003 = (logging.INFO, 'successfully queried email address record')
    DGPEMAILS0004 = (logging.ERROR, 'error querying email address record')
    DGPEMAILS0005 = (logging.INFO, 'could not find record to delete')
    DGPEMAILS0006 = (logging.INFO, 'successfully deleted record')
    DGPEMAILS0007 = (logging.ERROR, 'failed to delete record')
