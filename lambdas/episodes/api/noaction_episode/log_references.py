import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    NOACTION0001 = (logging.INFO, 'Receiving API call for noaction NRL participant')
    NOACTION0002 = (logging.INFO, 'Checking if participant has episode')
    NOACTION0003 = (logging.INFO, 'Updating episode status to NOACTION')
    NOACTION0004 = (logging.INFO, 'Successfully set episode status to NOACTION')

    NOACTIONEX0001 = (logging.ERROR, 'Unable to find live episode')
    NOACTIONEX0002 = (logging.ERROR, 'Unable to find live NRL episode')
    NOACTIONEX0003 = (logging.ERROR, 'Participant has multiple live NRL episodes')
