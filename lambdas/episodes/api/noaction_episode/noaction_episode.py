import os
from datetime import datetime, timezone
from boto3.dynamodb.conditions import Key, Attr

from common.utils.participant_episode_utils import (
    query_for_live_episode,
    update_episode_with_condition_expression
)
from common.utils.session_utils import get_session_from_lambda_event
from common.utils.audit_utils import audit, AuditActions
from common.utils import json_return_message, json_return_object
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils.participant_utils import get_nhs_number_by_participant_id
from common.models.participant import EpisodeStatus
from noaction_episode.log_references import LogReference
from common.log import log

AUDIT_QUEUE_URL = os.environ.get('AUDIT_QUEUE_URL')

EPISODE_SORT_KEY = 'EPISODE#'
NRL_STATUS = 'NRL'


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    log({'log_reference': LogReference.NOACTION0001})

    participant_id = event['pathParameters']['participant_id']

    log({'log_reference': LogReference.NOACTION0002})
    live_episodes = query_for_live_episode(participant_id, segregate=True)

    if not live_episodes:
        log({'log_reference': LogReference.NOACTIONEX0001})
        return json_return_message(404, 'Unable to find episode')

    nrl_episode_count, sort_key = check_participant_has_single_episode_in_nrl_state(live_episodes)

    if nrl_episode_count > 1:
        log({'log_reference': LogReference.NOACTIONEX0003})
        return json_return_message(500, 'Participant has multiple live episodes')

    if nrl_episode_count == 0:
        log({'log_reference': LogReference.NOACTIONEX0002})
        return json_return_message(409, 'Unable to find live NRL episode')

    session = get_session_from_lambda_event(event)

    user_id = session['user_data']['nhsid_useruid']
    update_episode_to_noaction(participant_id, user_id, sort_key)
    raise_audit_for_event(session, participant_id)

    log({'log_reference': LogReference.NOACTION0004})
    return json_return_object(200)


def check_participant_has_single_episode_in_nrl_state(live_episodes):
    nrl_episode_count = 0
    first_found_sort_key = ""

    for episode in live_episodes:
        if episode['live_record_status'] == 'NRL':
            first_found_sort_key = episode['sort_key']
            nrl_episode_count = nrl_episode_count + 1

    return nrl_episode_count, first_found_sort_key


def update_episode_to_noaction(participant_id, user_id, sort_key):
    log({'log_reference': LogReference.NOACTION0003})

    now_date = datetime.now(timezone.utc).date().isoformat()
    condition_expression = Key('sort_key').begins_with('EPISODE') & Attr('live_record_status').eq(NRL_STATUS)

    key = {
        'participant_id': participant_id,
        'sort_key': sort_key
    }

    attributes_to_update = {
        'status': EpisodeStatus.NOACTION,
        'live_record_status': EpisodeStatus.NOACTION,
        'noaction_date': now_date,
        'user_id': user_id
    }
    update_episode_with_condition_expression(key, attributes_to_update, condition_expression, segregate=True)


def raise_audit_for_event(session, participant_id):
    audit(session=session,
          action=AuditActions.NOACTION_EPISODE,
          participant_ids=[participant_id],
          nhs_numbers=[get_nhs_number_by_participant_id(participant_id, segregate=True)])
