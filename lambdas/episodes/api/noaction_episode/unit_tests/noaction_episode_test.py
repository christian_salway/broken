from unittest import TestCase
from mock import patch, MagicMock, Mock


@patch('common.log.logger', MagicMock())
class TestNoActionParticipant(TestCase):

    def setUp(self):
        import noaction_episode.noaction_episode as _noaction_episode
        import common.utils.audit_utils as _audit_utils
        self.noaction_episode_module = _noaction_episode
        self.audit_utils = _audit_utils
        self.log_patcher = patch('common.log.log')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = ''
        self.event = {
            'pathParameters': {'participant_id': '12345678'},
            'requestContext': {'authorizer': {'session': '{"session_id": "12345678"}', 'principalId': 'blah'}},
            'headers': {'request_id': 'blah'}
        }

    def tearDown(self):
        self.log_patcher.stop()

    @patch('noaction_episode.noaction_episode.audit')
    @patch('noaction_episode.noaction_episode.get_session_from_lambda_event')
    @patch('noaction_episode.noaction_episode.query_for_live_episode')
    @patch('noaction_episode.noaction_episode.update_episode_with_condition_expression')
    def test_lambda_returns_409_if_episode_in_incorrect_state(self, update_mock, query_mock, session_mock, audit_mock):
        query_mock.return_value = [{'live_record_status': 'notNice'}]

        response = self.noaction_episode_module.lambda_handler(self.event, self.context)

        query_mock.assert_called_with('12345678', segregate=True)
        session_mock.assert_not_called()
        update_mock.assert_not_called()
        audit_mock.assert_not_called()

        self.assertEqual(409, response['statusCode'])

    @patch('noaction_episode.noaction_episode.audit')
    @patch('noaction_episode.noaction_episode.get_session_from_lambda_event')
    @patch('noaction_episode.noaction_episode.query_for_live_episode')
    @patch('noaction_episode.noaction_episode.update_episode_with_condition_expression')
    def test_lambda_returns_404_if_no_episode_found(self, update_mock, query_mock, session_mock, audit_mock):
        query_mock.return_value = []

        response = self.noaction_episode_module.lambda_handler(self.event, self.context)

        query_mock.assert_called_with('12345678', segregate=True)
        session_mock.assert_not_called()
        update_mock.assert_not_called()
        audit_mock.assert_not_called()

        self.assertEqual(404, response['statusCode'])

    @patch('noaction_episode.noaction_episode.audit')
    @patch('noaction_episode.noaction_episode.get_session_from_lambda_event')
    @patch('noaction_episode.noaction_episode.query_for_live_episode')
    @patch('noaction_episode.noaction_episode.update_episode_with_condition_expression')
    def test_lambda_returns_500_if_multiple_live_nrl_episodes(self, update_mock, query_mock, session_mock, audit_mock):
        query_mock.return_value = [
            {'live_record_status': 'NRL', 'sort_key': '11111111'},
            {'live_record_status': 'NRL', 'sort_key': '1122341'}
        ]

        response = self.noaction_episode_module.lambda_handler(self.event, self.context)

        query_mock.assert_called_with('12345678', segregate=True)
        session_mock.assert_not_called()
        update_mock.assert_not_called()
        audit_mock.assert_not_called()

        self.assertEqual(500, response['statusCode'])
        self.assertEqual('{"message": "Participant has multiple live episodes"}', response['body'])

    @patch('noaction_episode.noaction_episode.audit')
    @patch('noaction_episode.noaction_episode.get_session_from_lambda_event')
    @patch('noaction_episode.noaction_episode.query_for_live_episode')
    @patch('noaction_episode.noaction_episode.update_episode_with_condition_expression')
    @patch('noaction_episode.noaction_episode.get_nhs_number_by_participant_id')
    def test_lambda_returns_200_if_operation_successful(self, get_nhs_number_by_participant_id_mock,
                                                        update_mock, query_mock, session_mock, audit_mock):
        query_mock.return_value = [
            {'live_record_status': 'NRL', 'sort_key': '11111111'},
        ]
        session_mock.return_value = {'user_data': {'nhsid_useruid': '23456789'}}
        nhs_number = '9999999999'
        get_nhs_number_by_participant_id_mock.return_value = nhs_number

        response = self.noaction_episode_module.lambda_handler(self.event, self.context)

        query_mock.assert_called_with('12345678', segregate=True)
        session_mock.assert_called_with(self.event)

        update_mock.assert_called()
        audit_mock.assert_called_with(
            session={'user_data': {'nhsid_useruid': '23456789'}},
            action=self.audit_utils.AuditActions.NOACTION_EPISODE,
            participant_ids=['12345678'],
            nhs_numbers=[nhs_number])

        self.assertEqual(200, response['statusCode'])
