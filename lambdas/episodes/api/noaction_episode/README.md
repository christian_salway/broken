Lambda, which updates episode records which are in 'PNL' state to 'NOACTION' state. This is triggered by an API event,
which is available on the following path:

```
    /api/episode/{participant_id}/noaction
```

[JIRA](https://nhsd-jira.digital.nhs.uk/browse/cerss-296)
[Confluence](https://nhsd-confluence.digital.nhs.uk/pages/viewpage.action?spaceKey=CSP&title=Episode+Lifecycle+Management)
