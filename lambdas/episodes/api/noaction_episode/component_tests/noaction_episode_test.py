import json
from unittest import TestCase
from unittest.mock import patch, Mock
from boto3.dynamodb.conditions import Key, Attr
from datetime import datetime, timezone
from common.models.participant import EpisodeStatus
from common.utils.data_segregation.nhais_ciphers import Cohorts


@patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'participants-table', 'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL'})
class TestNoActionEpisode(TestCase):
    mock_participant_id = "12345678"

    mock_live_episodes = [
        {
            'participant_id': mock_participant_id,
            'sort_key': 'EPISODE#12-03-2019',
            'live_record_status': EpisodeStatus.NRL.value
        },
        {
            'participant_id': mock_participant_id,
            'sort_key': 'EPISODE#12-03-2019',
            'live_record_status': EpisodeStatus.WALKIN.value
        },
    ]

    mock_participant_record = {
        'participant_id': mock_participant_id,
        'nhs_number': '9999999999',
        'sort_key': 'PARTICIPANT',
        'date_of_birth': '1980-04-01',
        'next_test_due_date': '2019-12-25',
        'nhais_cipher': 'MAN'
    }

    mock_filtered_participants = {mock_participant_id: 'MAN'}

    @classmethod
    @patch('boto3.client')
    @patch('boto3.resource')
    def setUpClass(cls, boto3_resource, boto3_client, *args):
        cls.log_patcher = patch('common.log.log')

        # Setup database mocks
        participants_table_mock = Mock()
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = participants_table_mock
        boto3_resource.return_value = boto3_table_mock
        cls.participants_table_mock = participants_table_mock

        # Setup SQS mock
        sqs_client_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: sqs_client_mock if args and args[0] == 'sqs' else Mock()
        cls.sqs_client_mock = sqs_client_mock

        # Setup lambda module
        import noaction_episode.noaction_episode as _noaction_episode
        global noaction_episode_module
        noaction_episode_module = _noaction_episode

    def setUp(self):
        self.participants_table_mock.reset_mock()
        self.participants_table_mock.query.return_value = {'Items': self.mock_live_episodes}
        self.participants_table_mock.get_item.return_value = {'Item': self.mock_participant_record}
        self.sqs_client_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids',
           Mock(return_value=mock_filtered_participants))
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    def test_noaction_episode_with_valid_participant_id(self):
        # Setup the event and the function invocation
        event = _build_event(self.mock_participant_id, "23456789", "987654321")
        context = Mock()
        context.function_name = ''

        response = noaction_episode_module.lambda_handler(event, context)

        # Asserts that the headers are in place

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{}',
            'isBase64Encoded': False
        }
        self.assertEqual(response, expected_response)

        # Asserts that the episode has been checked for the NRL state
        self.participants_table_mock.query.assert_called_once()
        _, kwargs = self.participants_table_mock.query.call_args

        expected_key_condition = Key('participant_id').eq(self.mock_participant_id) & Key('sort_key').begins_with('EPISODE#') 
        expected_filter_expression = Attr('live_record_status').exists()
        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])
        self.assertEqual(expected_filter_expression, kwargs['FilterExpression'])

        # Asserts the episode has been updated correctly
        self.participants_table_mock.update_item.assert_called_once()
        _, kwargs = self.participants_table_mock.update_item.call_args

        expected_condition_expression = Key('sort_key').begins_with('EPISODE') & Attr('live_record_status').eq('NRL')
        expected_record = {'participant_id': self.mock_participant_id, 'sort_key': 'EPISODE#12-03-2019'}
        expected_expression_values = {
            ':status': EpisodeStatus.NOACTION.value,
            ':live_record_status': EpisodeStatus.NOACTION.value,
            ':noaction_date': datetime.now(timezone.utc).date().isoformat(),
            ':user_id': '23456789'
        }

        self.assertEqual(expected_condition_expression, kwargs['ConditionExpression'])
        self.assertEqual(expected_expression_values, kwargs['ExpressionAttributeValues'])
        self.assertEqual(expected_record, kwargs['Key'])

        # Asserts the audit record has been sent to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'NOACTION_EPISODE')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), '23456789')
        self.assertEqual(actual_audit_record_sent.get('session_id'), '987654321')
        self.assertEqual(actual_audit_record_sent.get('participant_ids'), [self.mock_participant_id])
        self.assertEqual(actual_audit_record_sent.get('nhs_numbers'), ['9999999999'])
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids',
           Mock(return_value=mock_filtered_participants))
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.IOM]))
    def test_cannot_noaction_episode_with_valid_participant_id_from_different_workgroup(self):
        # Setup the event and the function invocation
        event = _build_event(self.mock_participant_id, "23456789", "987654321")
        context = Mock()
        context.function_name = ''

        response = noaction_episode_module.lambda_handler(event, context)

        # Asserts that the headers are in place

        expected_response = {
            'statusCode': 404,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Unable to find episode"}',
            'isBase64Encoded': False
        }
        self.assertEqual(response, expected_response)

        # Asserts that the episode has been checked for the NRL state
        self.participants_table_mock.query.assert_called_once()
        _, kwargs = self.participants_table_mock.query.call_args

        expected_key_condition = Key('participant_id').eq(self.mock_participant_id) & Key('sort_key').begins_with('EPISODE#') 
        expected_filter_expression = Attr('live_record_status').exists()
        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])
        self.assertEqual(expected_filter_expression, kwargs['FilterExpression'])

        self.participants_table_mock.update_item.assert_not_called()

        self.sqs_client_mock.send_message.assert_not_called()


def _build_event(participant_id, user_uid, session_id):
    return {
        'pathParameters': {
            'participant_id': participant_id
        },
        'headers': {
            'request_id': 'requestID',
        },
        'requestContext': {
            'authorizer': {
                'principalId': 'blah',
                'session': f"{{\"user_data\": {{\"nhsid_useruid\": \"{user_uid}\"}}, \"session_id\": \"{session_id}\"}}"
            }
        }
    }
