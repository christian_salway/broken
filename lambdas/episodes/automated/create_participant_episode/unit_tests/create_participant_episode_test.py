from unittest import TestCase
from mock import patch, MagicMock
from common.models.participant import EpisodeStatus
from create_participant_episode.create_participant_episode import LogReference
from common.utils.audit_utils import AuditActions, AuditUsers
with patch('boto3.resource') as resource_mock:
    from create_participant_episode.create_participant_episode import lambda_handler

module = 'create_participant_episode.create_participant_episode'

VALID_SQS_EVENT = {
    'Records': [
        {
            'body': '''{
                            "participant_id":"1234567890",
                            "next_test_due_date":"2020-03-11",
                            "registered_gp_practice_code":"ABC1234"
                        }'''
        }
    ]
}


class TestCreateParticipantEpisode(TestCase):

    def setUp(self):
        self.datetime_patcher = patch(f'{module}.datetime').start()
        self.create_replace_record_in_participant_table_patcher = patch(
            f'{module}.create_replace_record_in_participant_table').start()
        self.get_participant_by_participant_id_patcher = patch(f'{module}.get_participant_by_participant_id').start()
        self.audit_patcher = patch(f'{module}.audit').start()
        self.log_patcher = patch(f'{module}.log').start()

    def test_update_participant(self):
        utcnow_mock = MagicMock()
        utcnow_mock.isoformat.return_value = '2019-12-05T09:45:23.000000+00:00'
        utcnow_mock.strftime.return_value = '2019-12-05'
        self.datetime_patcher.now.return_value = utcnow_mock

        participant = {
            'participant_id': '1234567890',
            'next_test_due_date': '2020-03-11',
            'registered_gp_practice_code': 'ABC1234',
            'nhs_number': 'nhs_number'
        }

        self.get_participant_by_participant_id_patcher.return_value = participant

        expected_episode = {
            'participant_id': '1234567890',
            'sort_key': 'EPISODE#2020-03-11',
            'registered_gp_practice_code_and_test_due_date': 'ABC1234#2020-03-11',
            'created': '2019-12-05T09:45:23.000000+00:00',
            'pnl_date': '2019-12-05',
            'test_due_date': '2020-03-11',
            'registered_gp_practice_code': 'ABC1234',
            'user_id': 'create_participant_episode',
            'status': EpisodeStatus.PNL.value,
            'live_record_status': EpisodeStatus.PNL.value
        }

        # Act
        lambda_handler.__wrapped__(VALID_SQS_EVENT, {})

        # Assert
        self.create_replace_record_in_participant_table_patcher.assert_called_with(expected_episode)
        self.audit_patcher.assert_called_once_with(action=AuditActions.CREATE_EPISODE,
                                                   participant_ids=['1234567890'],
                                                   nhs_numbers=['nhs_number'],
                                                   user=AuditUsers.SYSTEM)

    def test_update_participant_when_gp_practice_code_missing(self):
        utcnow_mock = MagicMock()
        utcnow_mock.isoformat.return_value = '2019-12-05T09:45:23.000000+00:00'
        utcnow_mock.strftime.return_value = '2019-12-05'
        self.datetime_patcher.now.return_value = utcnow_mock

        participant = {
            'participant_id': '1234567890',
            'next_test_due_date': '2020-03-11',
            'nhs_number': 'nhs_number'
        }

        self.get_participant_by_participant_id_patcher.return_value = participant

        expected_episode = {
            'participant_id': '1234567890',
            'sort_key': 'EPISODE#2020-03-11',
            'registered_gp_practice_code_and_test_due_date': '#2020-03-11',
            'created': '2019-12-05T09:45:23.000000+00:00',
            'pnl_date': '2019-12-05',
            'test_due_date': '2020-03-11',
            'user_id': 'create_participant_episode',
            'status': EpisodeStatus.PNL.value,
            'live_record_status': EpisodeStatus.PNL.value
        }

        # Act
        lambda_handler.__wrapped__(VALID_SQS_EVENT, {})

        # Assert
        self.create_replace_record_in_participant_table_patcher.assert_called_with(expected_episode)
        self.audit_patcher.assert_called_once_with(action=AuditActions.CREATE_EPISODE,
                                                   participant_ids=['1234567890'],
                                                   nhs_numbers=['nhs_number'],
                                                   user=AuditUsers.SYSTEM)

    @patch('create_participant_episode.create_participant_episode.create_replace_record_in_participant_table')
    def test_error_is_logged(self, mock_create_replace_record_in_participant_table):
        with self.assertRaises(Exception):
            lambda_handler.__wrapped__({}, {})

        mock_create_replace_record_in_participant_table.assert_not_called()
        self.log_patcher.assert_called_with({'log_reference': LogReference.EPISODE0001, 'error': "'Records'"})
