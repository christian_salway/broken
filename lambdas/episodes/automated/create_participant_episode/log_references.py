import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    EPISODE0001 = (logging.ERROR, 'Could not create participant episode')
    EPISODE0002 = (logging.INFO, 'SQS message parsed successfully')
    EPISODE0003 = (logging.INFO, 'Retrieved participant from DB')
    EPISODE0004 = (logging.INFO, 'Episode created successfully')
