import json
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from create_participant_episode.log_references import LogReference
from datetime import datetime, timezone
from common.utils.participant_utils import (
    create_replace_record_in_participant_table,
    get_participant_by_participant_id
)
from common.models.participant import EpisodeStatus
from common.utils.audit_utils import AuditActions, AuditUsers, audit


@lambda_entry_point
def lambda_handler(event, context):
    try:
        sqs_message = json.loads(event['Records'][0]['body'])
        log({'log_reference': LogReference.EPISODE0002})

        participant_id = sqs_message['participant_id']
        participant = get_participant_by_participant_id(participant_id)
        log({'log_reference': LogReference.EPISODE0003})

        episode = create_episode(participant)

        create_replace_record_in_participant_table(episode)
        log({'log_reference': LogReference.EPISODE0004})

    except Exception as e:
        log({'log_reference': LogReference.EPISODE0001, 'error': str(e)})
        raise e


def create_episode(participant):
    next_test_due_date = participant['next_test_due_date']
    registered_gp_practice_code = participant.get('registered_gp_practice_code', '')
    participant_id = participant['participant_id']

    sort_key = 'EPISODE#{}'.format(next_test_due_date)
    registered_gp_practice_code_and_test_due_date = '{}#{}'.format(registered_gp_practice_code, next_test_due_date)
    utc_date = datetime.now(timezone.utc)
    create_date = utc_date.isoformat()

    episode = {
        'participant_id': participant_id,
        'sort_key': sort_key,
        'registered_gp_practice_code_and_test_due_date': registered_gp_practice_code_and_test_due_date,
        'created': create_date,
        'pnl_date': utc_date.strftime("%Y-%m-%d"),
        'test_due_date': next_test_due_date,
        'user_id': 'create_participant_episode',
        'status': EpisodeStatus.PNL.value,
        'live_record_status': EpisodeStatus.PNL.value
    }

    audit(
        action=AuditActions.CREATE_EPISODE,
        user=AuditUsers.SYSTEM,
        participant_ids=[participant_id],
        nhs_numbers=[participant.get('nhs_number', '')]
    )

    if(registered_gp_practice_code):
        episode['registered_gp_practice_code'] = registered_gp_practice_code

    return episode
