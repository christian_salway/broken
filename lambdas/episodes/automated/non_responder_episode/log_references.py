import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    NON_RESPONDER0001 = (logging.INFO, 'Received participants to mark as non-responder')
    NON_RESPONDER0002 = (logging.ERROR, 'Missing required field in SQS record')
    NON_RESPONDER0003 = (logging.INFO, 'Marking episode as non-responder')
    NON_RESPONDER0004 = (logging.ERROR, 'Participant not found')
    NON_RESPONDER0005 = (logging.ERROR, 'Unable to calculate next test due date')
    NON_RESPONDER0006 = (logging.INFO, 'Successfully marked as non-responder')
