from unittest import TestCase
from mock import patch, Mock, call
import json
from datetime import datetime, timezone
from common.test_mocks.mock_events import sqs_mock_event


class TestNonResponderEpisode(TestCase):
    mock_env_vars = {
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
        'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS'
    }
    example_date = datetime(2020, 1, 23, 13, 48, 8, tzinfo=timezone.utc)
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.now.return_value = example_date

    @classmethod
    @patch('datetime.datetime', datetime_mock)
    @patch('boto3.client')
    @patch('boto3.resource')
    @patch('os.environ', mock_env_vars)
    @patch('common.log.get_internal_id', Mock(return_value='1'))
    def setUpClass(cls, boto3_resource, boto3_client):
        cls.log_patcher = patch('common.log.log')
        cls.env_patcher = patch('os.environ', cls.mock_env_vars)

        # Setup database mock
        participants_table_mock = Mock()
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = participants_table_mock
        boto3_resource.return_value = boto3_table_mock
        cls.participants_table_mock = participants_table_mock

        # Setup SQS mock
        sqs_client_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: sqs_client_mock if args and args[0] == 'sqs' else Mock()
        cls.sqs_client_mock = sqs_client_mock

        # Setup lambda module
        import non_responder_episode.non_responder_episode as _non_responder_episode
        global non_responder_episode_module
        non_responder_episode_module = _non_responder_episode

    def setUp(self):
        self.participants_table_mock.reset_mock()
        self.sqs_client_mock.reset_mock()
        self.log_patcher.start()
        self.env_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    def test_non_responder_episode(self):
        # Setup the function invocation
        context = Mock()
        context.function_name = ''
        event = {
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-01-01',
            'nhs_number': '9999999999',
            'test_due_date': '2020-01-01'
        }
        participant = {
            'date_of_birth': '2000-01-01',
            'participant_id': '123456',
            'sort_key': 'PARTICIPANT',
            'status': 'ROUTINE'
        }

        self.participants_table_mock.get_item.return_value = {'Item': participant}

        non_responder_episode_module.lambda_handler(sqs_mock_event(event), context)

        # Asserts that episode has been updated
        expected_update_expression = ", ".join([
            'SET #status = :status',
            '#live_record_status = :live_record_status',
            '#non_responder_list_date = :non_responder_list_date',
            '#proposed_next_test_due_date = :proposed_next_test_due_date'])
        self.participants_table_mock.update_item.assert_any_call(
            Key={'participant_id': '123456', 'sort_key': 'EPISODE#2020-01-01'},
            UpdateExpression=expected_update_expression,
            ExpressionAttributeValues={
                ':status': 'NRL', ':live_record_status': 'NRL', ':non_responder_list_date': '2020-01-23',
                ':proposed_next_test_due_date': '2023-01-01'
            },
            ExpressionAttributeNames={
                '#status': 'status', '#live_record_status': 'live_record_status',
                '#non_responder_list_date': 'non_responder_list_date',
                '#proposed_next_test_due_date': 'proposed_next_test_due_date'
            },
            ReturnValues='NONE'
        )

        # Asserts audit has been sent to the audit queue
        audit_call = call(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody=json.dumps({
                "action": "NRL_EPISODE",
                "timestamp": "2020-01-23T13:48:08+00:00",
                "internal_id": "1",
                "nhsid_useruid": "SYSTEM",
                "session_id": "NONE",
                "participant_ids": ["123456"]
            })
        )
        self.sqs_client_mock.send_message.assert_has_calls([audit_call])
