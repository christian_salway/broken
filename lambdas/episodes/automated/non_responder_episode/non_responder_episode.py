import json
from datetime import datetime, timezone
from common.utils import json_return_object
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_episode_utils import (
    update_participant_episode_record
)
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator \
    import calculate_next_test_due_date_for_closed_episode
from common.utils.participant_utils import get_participant_by_participant_id
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.models.participant import EpisodeStatus
from common.log import log
from non_responder_episode.log_references import LogReference


@lambda_entry_point
def lambda_handler(event, context):
    records = event.get('Records', [{}])
    log({'log_reference': LogReference.NON_RESPONDER0001, 'record_count': len(records)})
    for msg in records:
        record = json.loads(msg['body'])
        update_episode_record(record)
    return json_return_object(200, '')


def get_missing_fields(record):
    required_fields = ['participant_id', 'test_due_date', 'sort_key']
    return [field for field in required_fields if field not in record]


def update_episode_record(record):

    missing_fields = get_missing_fields(record)
    participant_id = record.get('participant_id')
    if missing_fields:
        log({'log_reference': LogReference.NON_RESPONDER0002, 'missing_fields': missing_fields})
        return

    log({'log_reference': LogReference.NON_RESPONDER0003, 'participant_id': participant_id})

    participant = get_participant_by_participant_id(participant_id)

    if not participant:
        log({'log_reference': LogReference.NON_RESPONDER0004, 'participant_id': participant_id})
        return

    proposed_test_due_date = calculate_next_test_due_date_for_closed_episode(participant, record)

    if not proposed_test_due_date:
        log({'log_reference': LogReference.NON_RESPONDER0005, 'participant_id': participant_id})
        return

    now = datetime.now(timezone.utc)
    today = now.date().isoformat()

    key = {
        'participant_id': participant_id,
        'sort_key': record['sort_key']
    }
    update_participant_episode_record(key, {
        'status': EpisodeStatus.NRL.value,
        'live_record_status': EpisodeStatus.NRL.value,
        'non_responder_list_date': today,
        'proposed_next_test_due_date': proposed_test_due_date.isoformat()
    })
    audit(AuditActions.NRL_EPISODE, user=AuditUsers.SYSTEM, participant_ids=[participant_id])
    log({'log_reference': LogReference.NON_RESPONDER0006, participant_id: participant_id})
