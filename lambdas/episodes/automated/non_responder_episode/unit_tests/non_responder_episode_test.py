import os
from unittest import TestCase
from datetime import datetime, timezone
from mock import patch, Mock, MagicMock
from common.test_mocks.mock_events import sqs_mock_event

module = 'non_responder_episode.non_responder_episode'


class TestCreateInvite(TestCase):
    mock_env_vars = {
        'LETTER_QUEUE_URL': 'LETTER_QUEUE_URL',
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    }

    mock_participant = {
        'participant_id': 'participant_id',
        'date_of_birth': '2000-01-01',
        'test_due_date': '2020-10-10',
        'status': 'ROUTINE',
    }
    sqs_event = {
        'participant_id': mock_participant['participant_id'],
        'sort_key': 'EPISODE#2020-01-01',
        'test_due_date': mock_participant['test_due_date'],
    }

    calculated_ntdd = datetime(2020, 1, 1, tzinfo=timezone.utc)

    @patch('boto3.client', MagicMock())
    @patch('boto3.resource', MagicMock())
    @patch.dict(os.environ, mock_env_vars)
    def setUp(self):
        self.log_patcher = patch('common.log.logger').start()
        self.env_patcher = patch('os.environ', self.mock_env_vars).start()
        self.audit_patcher = patch(f'{module}.audit').start()
        self.get_participant_by_participant_id_patcher = patch(f'{module}.get_participant_by_participant_id').start()
        self.calculate_next_test_due_date_for_closed_episode_patcher = patch(
            f'{module}.calculate_next_test_due_date_for_closed_episode').start()
        self.update_participant_episode_record_patcher = patch(f'{module}.update_participant_episode_record').start()

        self.datetime_patcher = patch(f'{module}.datetime').start()
        self.datetime_patcher.now.return_value = datetime(2020, 1, 1, tzinfo=timezone.utc)
        self.get_participant_by_participant_id_patcher.return_value = self.mock_participant
        self.calculate_next_test_due_date_for_closed_episode_patcher.return_value = self.calculated_ntdd.date()

        self.context = Mock()
        self.context.function_name = ''

        import non_responder_episode.non_responder_episode as _non_responder_episode
        import common.utils.audit_utils as _audit_utils
        self.non_responder_episode_module = _non_responder_episode
        self.audit_utils = _audit_utils

        self.event = sqs_mock_event(self.sqs_event)

    def tearDown(self):
        patch.stopall()

    def act(self):
        return self.non_responder_episode_module.lambda_handler(self.event, self.context)

    def test_lambda_happy_path(self):
        # Arrange
        expected_dynamo_key = {'participant_id': 'participant_id', 'sort_key': 'EPISODE#2020-01-01'}
        expected_dynamo_payload = {
            'status': 'NRL',
            'live_record_status': 'NRL',
            'non_responder_list_date': '2020-01-01',
            'proposed_next_test_due_date': '2020-01-01'
        }

        # Act
        response = self.act()

        # Assert
        self.get_participant_by_participant_id_patcher.assert_called_with('participant_id')
        self.calculate_next_test_due_date_for_closed_episode_patcher.assert_called_with(
            self.mock_participant,
            self.sqs_event
        )
        self.update_participant_episode_record_patcher.assert_called_with(expected_dynamo_key, expected_dynamo_payload)
        self.audit_patcher.assert_called_with(
            self.audit_utils.AuditActions.NRL_EPISODE,
            user=self.audit_utils.AuditUsers.SYSTEM,
            participant_ids=['participant_id']
        )
        self.assertEqual(response['statusCode'], 200)

    def test_participant_not_found(self):
        # Arrange
        self.get_participant_by_participant_id_patcher.return_value = {}
        # Act
        response = self.act()
        # Assert
        self.get_participant_by_participant_id_patcher.assert_called_with('participant_id')
        self.calculate_next_test_due_date_for_closed_episode_patcher.assert_not_called()
        self.update_participant_episode_record_patcher.assert_not_called()
        self.audit_patcher.assert_not_called()
        self.assertEqual(response['statusCode'], 200)

    def test_ntdd_not_calculated(self):
        # Arrange
        self.calculate_next_test_due_date_for_closed_episode_patcher.return_value = None
        # Act
        response = self.act()
        # Assert
        self.get_participant_by_participant_id_patcher.assert_called_with('participant_id')
        self.calculate_next_test_due_date_for_closed_episode_patcher.assert_called_with(
            self.mock_participant,
            self.sqs_event,
        )
        self.update_participant_episode_record_patcher.assert_not_called()
        self.audit_patcher.assert_not_called()
        self.assertEqual(response['statusCode'], 200)
