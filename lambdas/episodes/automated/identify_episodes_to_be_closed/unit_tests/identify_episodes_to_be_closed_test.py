from unittest import TestCase

from ddt import data, unpack, ddt
from mock import patch, Mock
from datetime import datetime, timezone
from identify_episodes_to_be_closed.log_references import LogReference
from identify_episodes_to_be_closed import identify_episodes_to_be_closed
from common.models.participant import EpisodeStatus

module = 'identify_episodes_to_be_closed.identify_episodes_to_be_closed'
mock_env_vars = {
    'EPISODES_TO_BE_CLOSED_QUEUE_URL': 'the_episodes_to_be_closed_queue',
    'EPISODES_TO_BE_CLOSED_THRESHOLD_IN_DAYS': '52'
}


@ddt
@patch('os.environ', mock_env_vars)
class TestEpisodesToBeClosed(TestCase):

    default_episode = {
        'participant_id': '123456',
        'sort_key': 'EPISODE#2019-10-01',
        'nhs_number': '9999999999',
        'test_due_date': '2019-01-01'
    }

    @patch('common.log.log', Mock())
    def setUp(self):
        self.context = Mock()
        self.context.function_name = ''

        self.batch_send_messages_patcher = patch(f'{module}.batch_send_messages').start()

        self.batch_send_messages_patcher.return_value = []

    @unpack
    @data(
        (0, {'log_reference': LogReference.IDENTTBC0001}),
        (3, {'log_reference': LogReference.IDENTTBC0002, 'episodes_to_be_closed_count': 3})
    )
    @patch(f'{module}.datetime')
    @patch(f'{module}.log')
    @patch(f'{module}.episode_state_change')
    def test_lambda_calls_episode_state_change_function_with_correct_parameters_and_logs_count(
            self, processed_epsiodes_count, expected_log_call, episode_state_change_mock, log_mock, datetime_mock):
        episode_state_change_mock.return_value = processed_epsiodes_count
        datetime_mock.now.return_value = datetime(2020, 12, 25, 12, 21, 35, 123456, tzinfo=timezone.utc)

        identify_episodes_to_be_closed.lambda_handler.__wrapped__({}, {})

        episode_state_change_mock.assert_called_with(
            statuses=[EpisodeStatus.NRL.value, EpisodeStatus.WALKIN.value, EpisodeStatus.NOACTION.value],
            timestamp='2020-11-03',
            state_change_function=identify_episodes_to_be_closed.add_episode_to_closure_queue,
            required_fields=['sort_key', 'participant_id']
        )
        log_mock.assert_called_with(expected_log_call)

    def test_add_episode_to_closure_queue_sends_correct_message_for_each_record(self):
        episode_record_1 = {'participant_id': '123456', 'sort_key': 'EPISODE#2021-01-07', 'live_record_status': EpisodeStatus.NRL.value} 
        episode_record_2 = {'participant_id': '654321', 'sort_key': 'EPISODE#2021-02-25', 'live_record_status': EpisodeStatus.NRL.value} 
        episode_records = [episode_record_1, episode_record_2]

        expected_sqs_batch = [
            {"participant_id": "123456", "sort_key": "EPISODE#2021-01-07"},
            {"participant_id": "654321", "sort_key": "EPISODE#2021-02-25"}
        ]

        actual_count = identify_episodes_to_be_closed.add_episode_to_closure_queue(episode_records)

        self.batch_send_messages_patcher.assert_called_with('the_episodes_to_be_closed_queue', expected_sqs_batch)
        self.assertEqual(2, actual_count)

    @patch(f'{module}.log')
    def test_add_episode_to_closure_queue_raises_exception_if_records_in_batch_send_fail(self, log_mock):
        episode_record_1 = {'participant_id': '123456', 'sort_key': 'EPISODE#2021-01-07', 'live_record_status': 'NRL'}
        episode_record_2 = {'participant_id': '654321', 'sort_key': 'EPISODE#2021-02-25', 'live_record_status': 'NRL'}
        episode_records = [episode_record_1, episode_record_2]
        self.batch_send_messages_patcher.return_value = [episode_record_1, episode_record_2]

        expected_sqs_batch = [
            {"participant_id": "123456", "sort_key": "EPISODE#2021-01-07"},
            {"participant_id": "654321", "sort_key": "EPISODE#2021-02-25"}
        ]

        with self.assertRaises(Exception) as context:
            identify_episodes_to_be_closed.add_episode_to_closure_queue(episode_records)

        self.batch_send_messages_patcher.assert_called_with('the_episodes_to_be_closed_queue', expected_sqs_batch)
        self.assertEqual('Failed to send sqs messages in batch operation', context.exception.args[0])
        log_mock.assert_called_with({'log_reference': LogReference.IDENTTBC0003, 'number_of_failed_messages': 2})
