from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_episode_utils import (
    episode_state_change
)
from common.utils.sqs_utils import batch_send_messages
from common.models.participant import EpisodeStatus
from common.log import log
from identify_episodes_to_be_closed.log_references import LogReference
from datetime import datetime, timedelta, timezone
import os


BATCH_SIZE = 10


def episodes_to_be_closed_queue():
    return os.environ.get('EPISODES_TO_BE_CLOSED_QUEUE_URL')


def episodes_to_be_closed_threshold_in_days():
    return int(os.environ.get('EPISODES_TO_BE_CLOSED_THRESHOLD_IN_DAYS'))


@lambda_entry_point
def lambda_handler(event, context):
    episodes_to_be_closed_date = datetime.now(timezone.utc) - timedelta(episodes_to_be_closed_threshold_in_days())
    episodes_to_be_closed_timestamp = episodes_to_be_closed_date.date().isoformat()

    count_of_epsiodes_to_be_closed = episode_state_change(
        statuses=[EpisodeStatus.NRL.value, EpisodeStatus.WALKIN.value, EpisodeStatus.NOACTION.value],
        timestamp=episodes_to_be_closed_timestamp,
        state_change_function=add_episode_to_closure_queue,
        required_fields=['sort_key', 'participant_id']
    )

    if count_of_epsiodes_to_be_closed == 0:
        log({'log_reference': LogReference.IDENTTBC0001})
    else:
        log({'log_reference': LogReference.IDENTTBC0002, 'episodes_to_be_closed_count': count_of_epsiodes_to_be_closed})


def add_episode_to_closure_queue(records):

    messages = [{
        'sort_key': participant['sort_key'],
        'participant_id': participant['participant_id'],
    } for participant in records]

    failed = batch_send_messages(episodes_to_be_closed_queue(), messages)
    if failed:
        log({
            'log_reference': LogReference.IDENTTBC0003,
            'number_of_failed_messages': len(failed),
        })
        raise Exception('Failed to send sqs messages in batch operation')

    return len(records)
