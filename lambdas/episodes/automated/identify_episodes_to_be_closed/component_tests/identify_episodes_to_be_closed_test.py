from unittest import TestCase

from boto3.dynamodb.conditions import And, Key
from mock import patch, Mock, call
from datetime import datetime, timezone
from identify_episodes_to_be_closed import identify_episodes_to_be_closed
from common.models.participant import EpisodeStatus

example_datetime = datetime(2020, 12, 25, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_datetime


mock_env_vars = {
    'EPISODES_TO_BE_CLOSED_QUEUE_URL': 'EPISODES_TO_BE_CLOSED_QUEUE_URL',
    'EPISODES_TO_BE_CLOSED_THRESHOLD_IN_DAYS': 238,
}


participants_table_mock = Mock()
sqs_client_mock = Mock()


class TestEpisodesToBeClosed(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('identify_episodes_to_be_closed.identify_episodes_to_be_closed.datetime', datetime_mock)
    @patch('os.environ', mock_env_vars)
    @patch('common.utils.sqs_utils.get_internal_id',
           Mock(return_value='the_internal_id'))
    @patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
           Mock(return_value=participants_table_mock))
    def test_identify_episodes_to_be_closed(self):
        # Setup the function invocation
        context = Mock()
        context.function_name = ''

        # This is the result of the episode queries
        participants_table_mock.query = Mock(side_effect=[
            {
                'Items': [
                    {
                        'participant_id': '123456',
                        'sort_key': 'EPISODE#2019-01-01'
                    }
                ],
                'LastEvaluatedKey': '123'
            },
            {
                'Items': [
                    {
                        'participant_id': '234567',
                        'sort_key': 'EPISODE#2019-02-02'
                    }
                ]
            },
            {
                'Items': [
                    {
                        'participant_id': '345678',
                        'sort_key': 'EPISODE#2019-03-01'
                    }
                ]
            },
            {
                'Items': [
                    {
                        'participant_id': '456789',
                        'sort_key': 'EPISODE#2019-04-02',
                    },
                    {
                        'participant_id': '567890',
                        'sort_key': 'EPISODE#2019-04-07',
                    },
                ]
            },
        ])
        sqs_client_mock.send_message_batch = Mock(side_effect=[{'Successful': []} for _ in range(4)])

        identify_episodes_to_be_closed.lambda_handler({}, context)

        # Asserts that NRL, WALKIN and NOACTION episodes have been queried for
        # with calls for additional pages if required
        expected_query_calls = [
            call(
                IndexName='episode-test-due-date',
                KeyConditionExpression=And(Key('live_record_status').eq(EpisodeStatus.NRL.value), (Key('test_due_date').lte('2020-05-01'))),  
                ProjectionExpression='#sort_key, #participant_id',
                ExpressionAttributeNames={'#sort_key': 'sort_key', '#participant_id': 'participant_id'},
            ),
            call(
                IndexName='episode-test-due-date',
                KeyConditionExpression=And(Key('live_record_status').eq(EpisodeStatus.NRL.value), (Key('test_due_date').lte('2020-05-01'))),  
                ExclusiveStartKey='123',
                ProjectionExpression='#sort_key, #participant_id',
                ExpressionAttributeNames={'#sort_key': 'sort_key', '#participant_id': 'participant_id'},
            ),
            call(
                IndexName='episode-test-due-date',
                KeyConditionExpression=And(Key('live_record_status').eq(EpisodeStatus.WALKIN.value), (Key('test_due_date').lte('2020-05-01'))),  
                ProjectionExpression='#sort_key, #participant_id',
                ExpressionAttributeNames={'#sort_key': 'sort_key', '#participant_id': 'participant_id'},
            ),
            call(
                IndexName='episode-test-due-date',
                KeyConditionExpression=And(Key('live_record_status').eq(EpisodeStatus.NOACTION.value), (Key('test_due_date').lte('2020-05-01'))),  
                ProjectionExpression='#sort_key, #participant_id',
                ExpressionAttributeNames={'#sort_key': 'sort_key', '#participant_id': 'participant_id'},
            )
        ]
        participants_table_mock.query.assert_has_calls(expected_query_calls)

        queue_url = 'EPISODES_TO_BE_CLOSED_QUEUE_URL'
        nrl_page1_entries = [{
            'Id': '0',
            'MessageBody':
                '{"sort_key": "EPISODE#2019-01-01", "participant_id": "123456", "internal_id": "the_internal_id"}'
        }]
        nrl_page2_entries = [{
            'Id': '0',
            'MessageBody':
                '{"sort_key": "EPISODE#2019-02-02", "participant_id": "234567", "internal_id": "the_internal_id"}'
        }]
        noaction_entries = [{
            'Id': '0',
            'MessageBody':
                '{"sort_key": "EPISODE#2019-03-01", "participant_id": "345678", "internal_id": "the_internal_id"}'
        }]
        walkin_entries = [{
            'Id': '0',
            'MessageBody':
                '{"sort_key": "EPISODE#2019-04-02", "participant_id": "456789", "internal_id": "the_internal_id"}'
        }, {
            'Id': '1',
            'MessageBody':
                '{"sort_key": "EPISODE#2019-04-07", "participant_id": "567890", "internal_id": "the_internal_id"}'
        }]
        # Asserts that for each item returned by dynamodb, a message is sent to the sqs queue
        expected_sqs_calls = [
            call(QueueUrl=queue_url, Entries=nrl_page1_entries),
            call(QueueUrl=queue_url, Entries=nrl_page2_entries),
            call(QueueUrl=queue_url, Entries=noaction_entries),
            call(QueueUrl=queue_url, Entries=walkin_entries)]
        sqs_client_mock.send_message_batch.assert_has_calls(expected_sqs_calls)
