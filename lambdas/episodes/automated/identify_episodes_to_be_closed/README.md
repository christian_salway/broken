# Identify episodes to be closed

This lambda identifies the episodes that are ready to be closed, and places an entry onto the 
EPISODES_TO_BE_CLOSED_QUEUE_URL queue containing the participant_id and sort_key

## Environment variables
* EPISODES_TO_BE_CLOSED_QUEUE_URL - contains the episodes to be closed queue url
* EPISODES_TO_BE_CLOSED_THRESHOLD_IN_DAYS - holds the number of days past the test-due-date that episodes are picked up
to be closed (Currently this is 238)


