import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    IDENTTBC0001 = (logging.INFO, 'No NRL/WALKIN/NOACTION episodes to be closed found')
    IDENTTBC0002 = (logging.INFO, 'Episodes queued to be closed')
    IDENTTBC0003 = (logging.INFO, 'Failed to send sqs messages in batch operation')
