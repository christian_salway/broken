# Identify eligible participants

This lambda checks the eligibility criteria of people in the participants table and then adds them to the current episode if they are eligible.


## Eligibility criteria
* aged 24.5 - 64 (inclusive)
* next test due-date is in 10 weeks
 
See https://nhsd-confluence.digital.nhs.uk/display/CSP/Identify+Participants+-+ExMap for more info
