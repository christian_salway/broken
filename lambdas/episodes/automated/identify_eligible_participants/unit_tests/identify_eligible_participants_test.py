import datetime
from unittest import TestCase
from mock import patch, Mock


class TestIdentifyEligibleParticipants(TestCase):

    FIXED_NOW_TIME = datetime.datetime(2019, 12, 5, 9, 45, 23, tzinfo=datetime.timezone.utc)

    def setUp(self):
        import identify_eligible_participants.identify_eligible_participants as _identify_eligible_participants
        self.identify_eligible_participants = _identify_eligible_participants
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = 'identify_eligible_participants_test'

    @patch('identify_eligible_participants.identify_eligible_participants.log')
    @patch('identify_eligible_participants.identify_eligible_participants.datetime')
    @patch('identify_eligible_participants.identify_eligible_participants.query_participants_by_next_test_due_date')
    def test_calls_identify_eligible_participants_with_correct_parameters(
                                                                          self,
                                                                          mock_query_participants_by_next_test_due_date,
                                                                          mock_datetime,
                                                                          mock_log):

        # Arrange
        mock_datetime.now.return_value = self.FIXED_NOW_TIME
        next_test_due_date = (self.FIXED_NOW_TIME + datetime.timedelta(weeks=10)).date().isoformat()

        # Act
        self.identify_eligible_participants.lambda_handler({}, self.context)

        # Assert
        self.assertEqual(mock_query_participants_by_next_test_due_date.call_count, 1)
        self.assertEqual(mock_query_participants_by_next_test_due_date.call_args[0][0], next_test_due_date)

    @patch('identify_eligible_participants.identify_eligible_participants.log')
    @patch('identify_eligible_participants.identify_eligible_participants.datetime')
    @patch('identify_eligible_participants.identify_eligible_participants.query_participants_by_next_test_due_date')
    def test_date_in_event_used_in_place_of_today_date_when_present(self,
                                                                    mock_query_participants_by_next_test_due_date,
                                                                    mock_datetime,
                                                                    mock_log):

        # Arrange
        event = {'date': '2020-03-25'}
        mock_datetime.now.return_value = self.FIXED_NOW_TIME
        mock_datetime.strptime.return_value = datetime.datetime(2020, 3, 25, 0, 0)
        event_date = datetime.datetime(2020, 3, 25, 8, 18, 23)
        next_test_due_date = (event_date + datetime.timedelta(weeks=10)).date().isoformat()

        # Act
        self.identify_eligible_participants.lambda_handler(event, self.context)

        # Assert
        self.assertEqual(mock_query_participants_by_next_test_due_date.call_count, 1)
        self.assertEqual(mock_query_participants_by_next_test_due_date.call_args[0][0], next_test_due_date)

    @patch('identify_eligible_participants.identify_eligible_participants.log')
    @patch('identify_eligible_participants.identify_eligible_participants.datetime')
    @patch('common.utils.participant_utils.dynamodb_query')
    @patch('identify_eligible_participants.identify_eligible_participants.send_eligible_participants_to_sqs')
    def test_calls_send_eligible_participants_to_sqs_with_correct_parameters(
                                                                          self,
                                                                          mock_send_eligible_participants_to_sqs,
                                                                          query_mock,
                                                                          mock_datetime,
                                                                          mock_log):
        # Arrange
        participants = [{'participant_id': 'abc123'}]
        query_mock.return_value = {'Items': participants}

        # Act
        self.identify_eligible_participants.lambda_handler({}, self.context)

        # Assert
        self.assertEqual(mock_send_eligible_participants_to_sqs.call_count, 1)
        self.assertEqual(mock_send_eligible_participants_to_sqs.call_args[0][0], participants)

    @patch('identify_eligible_participants.identify_eligible_participants.log')
    @patch('identify_eligible_participants.identify_eligible_participants.datetime')
    @patch('identify_eligible_participants.identify_eligible_participants.boto3')
    def test_sends_participants_to_sqs_in_batches(
                                                  self,
                                                  mock_boto3,
                                                  _,
                                                  __):

        # Arrange
        sqs_results = {'Success': [], 'Failed': []}
        mock_sqs = Mock()
        mock_sqs.send_message_batch.return_value = sqs_results
        participants = [{'participant_id': i} for i in range(101)]
        mock_boto3.client.return_value = mock_sqs

        # Act
        self.identify_eligible_participants.send_eligible_participants_to_sqs(participants)

        # Assert
        self.assertEqual(mock_sqs.send_message_batch.call_count, 11)
