import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    ELIGIBLE1 = (logging.INFO, 'Gathering eligible participants')
    ELIGIBLE2 = (logging.INFO, 'Finding participants with next test due date')
    ELIGIBLE3 = (logging.INFO, 'Filtered eligible participants')
