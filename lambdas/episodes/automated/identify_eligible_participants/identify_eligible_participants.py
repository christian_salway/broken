import boto3
import os
import json
from botocore.client import Config
from datetime import datetime, timedelta, timezone

from common.log import log, get_internal_id
from identify_eligible_participants.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils import chunk_list
from common.utils.participant_utils import query_participants_by_next_test_due_date

NEXT_TEST_DUE_DATE_WEEK_OFFSET = 10
SQS_QUEUE_URL = os.environ.get('SQS_QUEUE_URL')


@lambda_entry_point
def lambda_handler(event, context):

    today = datetime.now(timezone.utc)
    custom_date = event.get('date')
    if custom_date:
        today = datetime.strptime(custom_date, "%Y-%m-%d")

    log({'log_reference': LogReference.ELIGIBLE1, 'today': today.isoformat()})

    next_test_due_date = (today + timedelta(weeks=NEXT_TEST_DUE_DATE_WEEK_OFFSET)).date().isoformat()

    log({'log_reference': LogReference.ELIGIBLE2, 'next_test_due_date': next_test_due_date})

    query_participants_by_next_test_due_date(next_test_due_date, send_eligible_participants_to_sqs)
    return True


def send_eligible_participants_to_sqs(eligible_participants):
    log({'log_reference': LogReference.ELIGIBLE3, 'eligible_participants_chunk_count': len(eligible_participants)})
    client = boto3.client('sqs', config=Config(retries={'max_attempts': 3}))
    internal_id = get_internal_id()
    participant_chunks = chunk_list(eligible_participants)

    for chunk in participant_chunks:

        entries = []
        for item in chunk:
            item['internal_id'] = internal_id
            entries.append({
                'Id': item.get('participant_id'),
                'MessageBody': json.dumps(item)
            })

        responses = client.send_message_batch(
          QueueUrl=SQS_QUEUE_URL,
          Entries=entries
        )

        if len(responses.get('Failed', [])):
            raise Exception('Unable to send batch to SQS')
