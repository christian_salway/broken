import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    IDENTREM0001 = (logging.INFO, 'No remindable episodes found')
    IDENTREM0002 = (logging.INFO, 'Successfully sent reminders')
    IDENTREM0003 = (logging.ERROR, 'Failed to send sqs messages in batch operation')
