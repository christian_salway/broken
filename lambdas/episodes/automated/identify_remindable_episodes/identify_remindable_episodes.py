from common.utils.lambda_wrappers import lambda_entry_point
from common.utils import json_return_object, chunks
from common.utils.sqs_utils import send_new_message_batch
from common.log import log, get_internal_id
from common.utils.participant_episode_utils import episode_state_change
from common.models.participant import EpisodeStatus
from identify_remindable_episodes.log_references import LogReference
from datetime import datetime, timedelta, timezone
import os
import json


REMINDER_QUEUE_URL = os.environ['REMINDER_QUEUE_URL']
REMINDER_DAYS = int(os.environ['REMINDER_THRESHOLD_IN_DAYS'])


@lambda_entry_point
def lambda_handler(event, context):
    reminder_date = datetime.now(timezone.utc) - timedelta(REMINDER_DAYS)
    reminder_timestamp = reminder_date.date().isoformat()

    record_count = episode_state_change([EpisodeStatus.INVITED.value], reminder_timestamp, send_to_remind_queue,
                                        required_fields=['sort_key', 'participant_id', 'registered_gp_practice_code'])

    if record_count == 0:
        log({'log_reference': LogReference.IDENTREM0001})
    else:
        log({'log_reference': LogReference.IDENTREM0002, 'reminder_count': record_count})
    return json_return_object(200)


def send_to_remind_queue(records):

    for batch in chunks(records, 10):
        messages = []
        for index, record in enumerate(batch):
            messages.append({
                'Id': str(index),
                'MessageBody': json.dumps({
                    'participant_id': record['participant_id'],
                    'sort_key': record['sort_key'],
                    'registered_gp_practice_code': record.get('registered_gp_practice_code'),
                    'internal_id': get_internal_id()
                })
            })

        result = send_new_message_batch(REMINDER_QUEUE_URL, messages)
        if result.get('Failed'):
            log({
                'log_reference': LogReference.IDENTREM0003, 'number_of_failed_messages': len(result.get('Failed'))})
            raise Exception('Failed to send sqs messages in batch operation')
    return len(records)
