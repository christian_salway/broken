import json
from importlib import reload
from unittest import TestCase
from mock import patch, Mock, call
from datetime import datetime, timezone
from common.models.participant import EpisodeStatus


@patch('identify_remindable_episodes.identify_remindable_episodes.get_internal_id', Mock(return_value='1'))
class TestCeaseParticipant(TestCase):

    mock_env_vars = {
        'REMINDER_QUEUE_URL': 'QUEUE_URL',
        'REMINDER_THRESHOLD_IN_DAYS': 84
    }

    @patch('boto3.client')
    @patch('boto3.resource')
    def setUp(self, *args):
        self.env_patcher = patch('os.environ', self.mock_env_vars)
        self.env_patcher.start()
        import identify_remindable_episodes.identify_remindable_episodes as _identify_remindable_episodes
        reload(_identify_remindable_episodes)
        self.identify_remindable_episodes_module = _identify_remindable_episodes
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = ''

    def tearDown(self):
        self.env_patcher.stop()
        self.log_patcher.stop()

    @patch('identify_remindable_episodes.identify_remindable_episodes.send_new_message_batch')
    @patch('common.utils.participant_episode_utils.query_by_state_and_test_due_date')
    def test_lambda_happy_path(self, query_mock, message_mock):
        message_mock.return_value = {}
        query_mock.return_value = {'Items': [
            {'participant_id': '123456', 'live_record_status': EpisodeStatus.INVITED.value, 'sort_key': 'EPISODE#2020-10-01', 'registered_gp_practice_code': 'X09'}   
        ]}

        response = self.identify_remindable_episodes_module.lambda_handler({}, self.context)

        message_mock.assert_called_with('QUEUE_URL', [{'Id': '0', 'MessageBody': json.dumps({
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-10-01',
            'registered_gp_practice_code': 'X09',
            'internal_id': '1'
        })}])
        self.assertEqual(response['statusCode'], 200)

    @patch('identify_remindable_episodes.identify_remindable_episodes.send_new_message_batch')
    def test_send_to_remind_queue_does_format_the_sqs_message_correctly(self, send_new_message_batch_mock):

        send_new_message_batch_mock.return_value = {}
        records = [
            {
                'participant_id': 'test_participant_id',
                'sort_key': 'test_sort_key'
            },
            {
                'participant_id': 'test_participant_id',
                'sort_key': 'test_sort_key'
            }
        ]

        expected_messages = [
            {
                'Id': '0',
                'MessageBody': json.dumps({
                    'participant_id': 'test_participant_id',
                    'sort_key': 'test_sort_key',
                    'registered_gp_practice_code': None,
                    'internal_id': '1'})
            },
            {
                'Id': '1',
                'MessageBody': json.dumps({
                    'participant_id': 'test_participant_id',
                    'sort_key': 'test_sort_key',
                    'registered_gp_practice_code': None,
                    'internal_id': '1'})
            }
        ]

        self.identify_remindable_episodes_module.send_to_remind_queue(records)

        send_new_message_batch_mock.assert_called_with('QUEUE_URL', expected_messages)

    @patch('identify_remindable_episodes.identify_remindable_episodes.send_new_message_batch')
    def test_send_to_remind_queue_in_batches_of_10(self, send_new_message_batch_mock):
        episode_records = [{'participant_id': f'{i}', 'sort_key': f'EPISODE#{i}'} for i in range(12)]
        send_new_message_batch_mock.side_effect = [
            {'Successful': [{'Id': '0', 'message': f'{i}'} for i in range(10)]},
            {'Successful': [{'Id': '1', 'message': f'{i}'} for i in range(10, 12)]},
        ]

        expected_sqs_batch_1 = [
            {'Id': f'{i}', 'MessageBody': f'{{"participant_id": "{i}", "sort_key": "EPISODE#{i}", "registered_gp_practice_code": null, "internal_id": "1"}}'}  
            for i in range(0, 10)
        ]
        expected_sqs_batch_2 = [
            {'Id': f'{i - 10}', 'MessageBody': f'{{"participant_id": "{i}", "sort_key": "EPISODE#{i}", "registered_gp_practice_code": null, "internal_id": "1"}}'} 
            for i in range(10, 12)
        ]

        actual_count = self.identify_remindable_episodes_module.send_to_remind_queue(episode_records)

        sqs_calls = send_new_message_batch_mock.call_args_list
        self.assertEqual(2, len(sqs_calls))
        self.assertEqual(sqs_calls[0], call('QUEUE_URL', expected_sqs_batch_1))
        self.assertEqual(sqs_calls[1], call('QUEUE_URL', expected_sqs_batch_2))

        self.assertEqual(12, actual_count)

    @patch('identify_remindable_episodes.identify_remindable_episodes.send_new_message_batch')
    def test_send_to_remind_queue_returns_record_count_number(self, send_new_message_batch_mock):
        send_new_message_batch_mock.return_value = {}
        records = [{'participant_id': 'test_participant_id1', 'sort_key': 'test_sort_key'} for i in range(39)]

        return_value = self.identify_remindable_episodes_module.send_to_remind_queue(records)

        self.assertEqual(return_value, 39)

    @patch('identify_remindable_episodes.identify_remindable_episodes.send_new_message_batch')
    def test_send_to_remind_queue_raises_exception_if_messages_failed_to_send(self, send_new_message_batch_mock):
        send_new_message_batch_mock.return_value = {'Failed': ['test']}
        records = [{'participant_id': 'test_participant_id1', 'sort_key': 'test_sort_key'}]

        with self.assertRaises(Exception):
            self.identify_remindable_episodes_module.send_to_remind_queue(records)

    @patch('identify_remindable_episodes.identify_remindable_episodes.datetime')
    @patch('identify_remindable_episodes.identify_remindable_episodes.episode_state_change')
    def test_lamda_handler_episode_state_change_is_called_with_correct_parameters(self, episode_state_change_mock,
                                                                                  datetime_mock):
        datetime_mock.now.return_value = datetime(2020, 1, 1, tzinfo=timezone.utc)
        episode_state_change_mock.return_value = 1

        self.identify_remindable_episodes_module.lambda_handler({}, self.context)

        episode_state_change_mock.assert_called_with(
            [EpisodeStatus.INVITED.value], '2019-10-09',
            self.identify_remindable_episodes_module.send_to_remind_queue,
            required_fields=['sort_key', 'participant_id', 'registered_gp_practice_code']
        )
