import json
from unittest import TestCase
from mock import patch, Mock
from boto3.dynamodb.conditions import Key, And
from datetime import datetime, timedelta, timezone
from common.models.participant import EpisodeStatus


sqs_client_mock = Mock()
participants_table_mock = Mock()
mock_env_vars = {
    'REMINDER_QUEUE_URL': 'REMINDER_QUEUE_URL',
    'REMINDER_THRESHOLD_IN_DAYS': 84
}

with patch('os.environ', mock_env_vars):
    from identify_remindable_episodes import identify_remindable_episodes


@patch('identify_remindable_episodes.identify_remindable_episodes.get_internal_id', Mock(return_value='1'))
class TestCeaseParticipant(TestCase):

    @patch('common.log.log')
    def setUp(self, *args):
        # Setup database mock
        participants_table_mock.query.return_value = {'Items': [
            {
                'participant_id': '123456',
                'sort_key': 'EPISODE#2020-01-01',
            }
        ]}

    @patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
           Mock(return_value=participants_table_mock))
    def test_identify_remindable_episodes(self):
        participants_table_mock.query.return_value = {'Items': [
            {'participant_id': '123456', 'live_record_status': EpisodeStatus.INVITED.value, 'sort_key': 'EPISODE#2020-10-01', 'registered_gp_practice_code': 'X09'}  
        ]}
        sqs_client_mock.send_message_batch.return_value = {}
        # Setup the function invocation
        context = Mock()
        context.function_name = ''
        reminder_date = datetime.now(timezone.utc) - timedelta(mock_env_vars['REMINDER_THRESHOLD_IN_DAYS'])
        reminder_timestamp = reminder_date.date().isoformat()

        identify_remindable_episodes.lambda_handler({}, context)

        # Asserts that remindable episodes have been queried for
        expected_condition_expression = And(
            Key('live_record_status').eq(EpisodeStatus.INVITED.value), Key('test_due_date').lte(reminder_timestamp)
        )
        participants_table_mock.query.assert_called_with(
            IndexName='episode-test-due-date',
            KeyConditionExpression=expected_condition_expression,
            ProjectionExpression='#sort_key, #participant_id, #registered_gp_practice_code',
            ExpressionAttributeNames={
                '#sort_key': 'sort_key',
                '#participant_id': 'participant_id',
                '#registered_gp_practice_code': 'registered_gp_practice_code'
            }
        )

        # Asserts the record has been sent to the reminder queue
        sqs_client_mock.send_message_batch.assert_called_with(
            QueueUrl='REMINDER_QUEUE_URL',
            Entries=[{'Id': '0', 'MessageBody': json.dumps({
                'participant_id': '123456',
                'sort_key': 'EPISODE#2020-10-01',
                'registered_gp_practice_code': 'X09',
                'internal_id': '1'
            })}])
