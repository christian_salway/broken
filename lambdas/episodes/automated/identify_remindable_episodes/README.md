# Identify remindable participants

This lambda is invoked under a cron cycle through cloudwatch, and determines which participants have their episodes test_due_date within a date range specified in terraform, and starts the process to send a reminder letter to the participant.

This lambda specifically will send a message to the `participants-to-get-a-reminder` queue for each participant identified as needing a reminder, from which the `create-reminder` lambda consumes the message. This message will contain the following:

```
{
  'participant_id': '<current_participant_id>',
  'sort_key': 'PARTICIPANT'
}
```


## Eligibility criteria
* next test due-date is in 6 weeks
 
See https://nhsd-confluence.digital.nhs.uk/display/CSP/5+Selection+into+Reminder+process for more info

