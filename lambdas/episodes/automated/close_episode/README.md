# Close Episodes

This lambda consumes episodes from the EPISODES_TO_BE_CLOSED_QUEUE_URL queue and closes them should they be in the correct state. Adding them to the episode DLQ if not

## Environment variables
* EPISODES_TO_BE_CLOSED_QUEUE_URL - contains the episodes to be closed queue url
