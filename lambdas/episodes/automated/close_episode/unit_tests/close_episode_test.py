from unittest import TestCase
from mock import patch, Mock
from datetime import datetime, timezone
from common.test_mocks.mock_events import sqs_mock_event
from common.utils.audit_utils import AuditActions, AuditUsers
from common.models.participant import CeaseReason, EpisodeStatus

module = 'close_episode.close_episode'


class TestCloseEpisode(TestCase):

    default_episode = {
        'participant_id': '123456',
        'sort_key': 'EPISODE#2019-10-01',
        'nhs_number': '9999999999',
        'test_due_date': '2019-01-01',
        'live_record_status': 'NRL'
    }

    walkin_episode = {**default_episode, 'live_record_status': EpisodeStatus.WALKIN.value}

    default_participant = {
        'participant_id': '123',
        'sort_key': 'PARTICIPANT',
        'next_test_due_date': '2020-12-25'
    }

    def setUp(self):
        self.log_patcher = patch('common.log.logger')
        self.get_internal_id_patcher = patch(f'{module}.get_internal_id').start()
        self.get_current_date_patcher = patch(f'{module}.get_current_date').start()
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = ''
        self.default_date = datetime(2020, 1, 1, tzinfo=timezone.utc).date()
        self.default_date_string = self.default_date.isoformat()

        import close_episode.close_episode as _close_episode
        self.close_episode_module = _close_episode

        self.mock_event = sqs_mock_event(self.default_episode)
        self.get_internal_id_patcher.return_value = '1'
        self.get_current_date_patcher.return_value = self.default_date_string

    def tearDown(self):
        self.log_patcher.stop()

    @patch(f'{module}.cease_participant_and_send_notification_messages')
    @patch(f'{module}.get_episode_record')
    @patch(f'{module}.update_to_nrl_closed_episode')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.update_participant_record')
    @patch(f'{module}.calculate_next_test_due_date_for_closed_episode')
    @patch(f'{module}.participant_eligible_for_auto_cease')
    @patch(f'{module}.audit')
    def test_lambda_happy_path_when_participant_not_eligile_for_autocease_and_episode_is_not_walkin(
            self, audit_mock, participant_eligible_for_auto_cease_mock, propose_next_test_due_date_mock,
            update_participant_mock, get_participant_mock, close_record_mock, get_episode_mock, cease_mock):
        get_episode_mock.return_value = self.default_episode
        get_participant_mock.return_value = self.default_participant
        propose_next_test_due_date_mock.return_value = self.default_date
        participant_eligible_for_auto_cease_mock.return_value = False

        self.close_episode_module.lambda_handler(self.mock_event, self.context)

        participant_eligible_for_auto_cease_mock.assert_called_with(
            self.default_participant,
            self.default_date
        )
        cease_mock.assert_not_called()
        close_record_mock.assert_called_with(
            self.default_episode['participant_id'],
            self.default_episode['sort_key'],
            self.default_date_string
        )
        update_participant_mock.assert_called_with(
            {'participant_id': '123', 'sort_key': 'PARTICIPANT'}, {'next_test_due_date': self.default_date_string}
        )
        audit_mock.assert_called_once_with(action=AuditActions.CLOSE_NON_RESPONDER_EPISODE,
                                           additional_information={'updating_next_test_due_date_from': '2020-12-25',
                                                                   'updating_next_test_due_date_to': '2020-01-01'},
                                           participant_ids=['123456'],
                                           user=AuditUsers.SYSTEM)

    @patch(f'{module}.cease_participant_and_send_notification_messages')
    @patch(f'{module}.get_episode_record')
    @patch(f'{module}.update_to_nrl_closed_episode')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.update_participant_record')
    @patch(f'{module}.calculate_next_test_due_date_for_closed_episode')
    @patch(f'{module}.participant_eligible_for_auto_cease')
    @patch(f'{module}.audit')
    def test_lambda_happy_path_when_participant_eligible_for_autocease(
            self, audit_mock, participant_eligible_for_auto_cease_mock, propose_next_test_due_date_mock,
            update_participant_mock, get_participant_mock, close_record_mock, get_episode_mock, cease_mock):
        get_episode_mock.return_value = self.default_episode
        get_participant_mock.return_value = self.default_participant
        propose_next_test_due_date_mock.return_value = self.default_date
        participant_eligible_for_auto_cease_mock.return_value = True

        self.close_episode_module.lambda_handler(self.mock_event, self.context)

        participant_eligible_for_auto_cease_mock.assert_called_once_with(
            self.default_participant,
            self.default_date
        )
        cease_mock.assert_called_once_with(self.default_participant, AuditUsers.SYSTEM.value,
                                           {'reason': CeaseReason.AUTOCEASE_DUE_TO_AGE})
        close_record_mock.assert_not_called()
        update_participant_mock.assert_not_called()
        audit_mock.assert_called_once_with(
            action=AuditActions.CEASE_EPISODE,
            additional_information={'updating_next_test_due_date_from': '2020-12-25',
                                    'reason': 'AUTOCEASE_DUE_TO_AGE'},
            participant_ids=['123456'],
            user=AuditUsers.SYSTEM)

    @patch(f'{module}.cease_participant_and_send_notification_messages')
    @patch(f'{module}.get_episode_record')
    @patch(f'{module}.update_to_nrl_closed_episode')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.update_participant_record')
    @patch(f'{module}.calculate_next_test_due_date_for_closed_episode')
    @patch(f'{module}.participant_eligible_for_auto_cease')
    @patch(f'{module}.audit')
    def test_ntdd_unchanged_for_walkin_episode(
            self, audit_mock, participant_eligible_for_auto_cease_mock, propose_next_test_due_date_mock,
            update_participant_mock, get_participant_mock, close_record_mock, get_episode_mock, cease_mock):
        get_episode_mock.return_value = self.walkin_episode

        self.close_episode_module.lambda_handler(self.mock_event, self.context)

        close_record_mock.assert_called_once_with(
            self.default_episode['participant_id'],
            self.default_episode['sort_key'],
            self.default_date_string
        )
        get_participant_mock.assert_not_called()
        propose_next_test_due_date_mock.assert_not_called()
        participant_eligible_for_auto_cease_mock.assert_not_called()
        cease_mock.assert_not_called()
        update_participant_mock.assert_not_called()
        audit_mock.assert_called_once_with(action=AuditActions.CLOSE_NON_RESPONDER_EPISODE,
                                           participant_ids=['123456'],
                                           user=AuditUsers.SYSTEM)

    @patch(f'{module}.get_episode_record')
    @patch(f'{module}.update_to_nrl_closed_episode')
    @patch(f'{module}.send_new_message')
    @patch(f'{module}.EPISODE_DLQ', 'TEST')
    def test_lambda_with_no_episodes_to_close(self, message_mock, close_record_mock, get_episode_mock):
        self.default_episode['live_record_status'] = 'INVALID'
        get_episode_mock.return_value = self.default_episode
        get_episode_mock.get.return_value = 'INVALID'
        self.close_episode_module.lambda_handler(self.mock_event, self.context)
        message_mock.assert_called_with('TEST', {
            'participant_id': self.default_episode['participant_id'],
            'sort_key': self.default_episode['sort_key'],
            'internal_id': '1'
        })
