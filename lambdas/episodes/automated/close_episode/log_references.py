import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    CLOSEEP0001 = (logging.INFO, 'Evaluating episode for closure')
    CLOSEEP0002 = (logging.INFO, 'Episode is in correct state, beginning closure')
    CLOSEEP0003 = (logging.INFO, 'Successful closure')
    CLOSEEP0004 = (logging.WARNING, 'Episode status was not NRL, WALKIN, or NOACTION, not closing')
    CLOSEEP0005 = (logging.INFO, 'Adding invalid record to DLQ')
    CLOSEEP0006 = (logging.INFO, 'Record successfully added to DLQ')
