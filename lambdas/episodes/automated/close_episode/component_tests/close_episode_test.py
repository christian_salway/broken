import json
from datetime import datetime, timezone
from unittest import TestCase

from mock import patch, Mock, call

from common.models.participant import EpisodeStatus, ParticipantStatus
from common.test_mocks.mock_events import sqs_mock_event

module = 'close_episode.close_episode'

sqs_message = {
    'participant_id': '123456',
    'sort_key': 'EPISODE#2019-10-01'
}
default_participant = {
    'participant_id': '123456',
    'sort_key': 'PARTICIPANT',
    'date_of_birth': '1890-01-01',
    'status': 'ROUTINE',
    'next_test_due_date': '2020-12-24',
    'nhais_cipher': 'MAN'

}
mock_env_vars = {
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
    'EPISODE_DLQ': 'EPISODE_DLQ',
}
participants_table_mock = Mock()
sqs_client_mock = Mock()
mock_event = sqs_mock_event(sqs_message)

example_date = datetime(2020, 1, 1, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

mock_filtered_participants = {default_participant['participant_id']: default_participant['nhais_cipher']}


@patch(f'{module}.get_current_date', Mock(return_value='2020-01-01'))
@patch('common.utils.cease_utils.datetime', datetime_mock)
@patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
@patch('common.utils.audit_utils.datetime', datetime_mock)
@patch('os.environ', mock_env_vars)
@patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(return_value=participants_table_mock))
class TestEpisodesToBeClosed(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.logger').start()
        self.env_patcher = patch('os.environ', mock_env_vars).start()
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()

        from close_episode.close_episode import lambda_handler
        self.lambda_handler = lambda_handler

    def tearDown(self):
        patch.stopall()

    @patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
           Mock(return_value=participants_table_mock))
    @patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids',
           Mock(return_value=mock_filtered_participants))
    def test_close_episode_with_nrl_status_when_autoceased(self):
        episode = {'participant_id': '123456', 'sort_key': 'EPISODE#2019-10-01', 'nhs_number': '9999999999',
                   'test_due_date': '2019-01-01', 'date_of_birth': '1990-01-01', 'live_record_status': 'NRL'}
        participants_table_mock.get_item.side_effect = [
            {'Item': episode}, {'Item': default_participant}
        ]
        participants_table_mock.query.side_effect = [
            {'Items': [
                {
                    'participant_id': '123456',
                    'sort_key': 'PARTICIPANT',
                    'date_of_birth': '1800-01-01',
                    'status': 'ROUTINE'
                },
                {
                    'participant_id': '123456',
                    'result_code': 'X',
                    'infection_code': '0',
                    'action_code': 'A',
                    'test_date': '2020-01-01',
                    'sort_key': 'RESULT#2020-01-01#'
                }
            ]},
            {'Items': [{
                'live_record_status': 'NRL',
                'sort_key': 'EPISODE'
            }]},
            {'Items': [{
                'action_code': 'A',
                'test_date': '2020-01-01'
            }]}
        ]

        # Setup the function invocation
        context = Mock()
        context.function_name = ''

        self.lambda_handler(mock_event, context)
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'EPISODE#2019-10-01'}),
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'})]
        )
        participants_table_mock.update_item.assert_has_calls([
            call(
                Key={'participant_id': '123456', 'sort_key': 'EPISODE'},
                UpdateExpression='SET #status = :status, #ceased_date = :ceased_date, #user_id = :user_id REMOVE #live_record_status',  
                ExpressionAttributeValues={
                    ':ceased_date': '2020-01-01',
                    ':status': EpisodeStatus.CEASED.value,
                    ':user_id': 'SYSTEM'
                },
                ExpressionAttributeNames={
                    '#status': 'status',
                    '#live_record_status': 'live_record_status',
                    '#ceased_date': 'ceased_date',
                    '#user_id': 'user_id'
                },
                ReturnValues='NONE'
            ),
            call(
                Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'},
                UpdateExpression='SET #is_ceased = :is_ceased, #status = :status REMOVE #next_test_due_date',
                ExpressionAttributeValues={':is_ceased': True, ':status': ParticipantStatus.CEASED},
                ExpressionAttributeNames={
                    '#is_ceased': 'is_ceased', '#status': 'status', '#next_test_due_date': 'next_test_due_date'
                },
                ReturnValues='NONE'
            )
        ])

        audit_message = {
            'action': 'CEASE_EPISODE',
            'timestamp': '2020-01-01T00:00:00+00:00',
            'internal_id': '1',
            'nhsid_useruid': 'SYSTEM',
            'session_id': 'NONE',
            'participant_ids': ['123456'],
            'additional_information': {'reason': 'AUTOCEASE_DUE_TO_AGE',
                                       'updating_next_test_due_date_from': '2020-12-24'}
        }

        # Asserts the audit record has been sent to the audit queue once
        sqs_client_mock.send_message.assert_called_with(
            QueueUrl='AUDIT_QUEUE_URL', MessageBody=json.dumps(audit_message)
        )

    def test_close_episode_with_walkin_status(self):
        episode = {'participant_id': '123456', 'sort_key': 'EPISODE#2019-10-01', 'nhs_number': '9999999999',
                   'test_due_date': '2019-01-01', 'date_of_birth': '1990-01-01', 'live_record_status': EpisodeStatus.WALKIN.value} 
        participants_table_mock.get_item.side_effect = [{'Item': episode}]

        # Setup the function invocation
        context = Mock()
        context.function_name = ''

        self.lambda_handler(mock_event, context)
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'EPISODE#2019-10-01'})
        ])
        participants_table_mock.update_item.assert_has_calls([call(
            Key={'participant_id': '123456', 'sort_key': 'EPISODE#2019-10-01'},
            UpdateExpression='SET #status = :status, #closed_date = :closed_date REMOVE #live_record_status',
            ExpressionAttributeValues={':status': f'{EpisodeStatus.CLOSED}', ':closed_date': '2020-01-01'},
            ExpressionAttributeNames={
                '#status': 'status', '#closed_date': 'closed_date', '#live_record_status': 'live_record_status'
            },
            ReturnValues='NONE')
        ])

    def test_close_episode_with_invalid_status(self):
        episode = {'participant_id': '123456', 'sort_key': 'EPISODE#2019-10-01', 'nhs_number': '9999999999',
                   'test_due_date': '2019-01-01', 'date_of_birth': '1990-01-01', 'live_record_status': EpisodeStatus.PNL.value} 
        participants_table_mock.get_item.side_effect = [
            {'Item': episode}, {'Item': default_participant}
        ]

        # Setup the function invocation
        context = Mock()
        context.function_name = ''

        self.lambda_handler(mock_event, context)
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'EPISODE#2019-10-01'})]
        )
