from common.utils.participant_episode_utils import (
    get_episode_record, update_to_nrl_closed_episode
)
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_utils import get_participant_by_participant_id, \
    update_participant_record
from common.utils.cease_utils import participant_eligible_for_auto_cease, \
    cease_participant_and_send_notification_messages
from common.models.participant import CeaseReason, EpisodeStatus
from common.utils.sqs_utils import send_new_message
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator \
    import calculate_next_test_due_date_for_closed_episode
from common.log import log, get_internal_id
from close_episode.log_references import LogReference
from datetime import datetime, timezone
import os
import json

EPISODE_DLQ = os.environ.get('EPISODE_DLQ')


@lambda_entry_point
def lambda_handler(event, context):
    for record in event['Records']:
        payload = json.loads(record['body'])
        participant_id = payload.get('participant_id')
        sort_key = payload.get('sort_key')
        episode = get_episode_record(participant_id, sort_key)
        episode = episode
        live_record_status = episode.get('live_record_status')
        log({'log_reference': LogReference.CLOSEEP0001, 'participant_id': participant_id})
        if live_record_status in [EpisodeStatus.NRL.value, EpisodeStatus.WALKIN.value, EpisodeStatus.NOACTION.value]:
            close_episode(participant_id, sort_key, episode, live_record_status)
            log({'log_reference': LogReference.CLOSEEP0003, 'participant_id': participant_id})
        else:
            log(
                {
                    'log_reference': LogReference.CLOSEEP0004,
                    'participant_id': participant_id,
                    'live_record_status': live_record_status
                }
            )
            add_episode_to_dlq(EPISODE_DLQ, episode)
            log({'log_reference': LogReference.CLOSEEP0006, 'participant_id': participant_id})


def close_episode(participant_id, sort_key, episode, live_record_status):
    log({'log_reference': LogReference.CLOSEEP0002, 'participant_id': participant_id,
         'live_record_status': live_record_status})

    if live_record_status == EpisodeStatus.WALKIN.value:
        update_to_nrl_closed_episode(participant_id, sort_key, get_current_date())
        audit(action=AuditActions.CLOSE_NON_RESPONDER_EPISODE,
              user=AuditUsers.SYSTEM,
              participant_ids=[episode['participant_id']])
    else:
        participant = get_participant_by_participant_id(participant_id)
        formatted_current_next_test_due_date = participant['next_test_due_date']
        next_test_due_date = calculate_next_test_due_date_for_closed_episode(participant, episode)
        formatted_next_test_due_date = next_test_due_date.isoformat()

        if participant_eligible_for_auto_cease(participant, next_test_due_date):
            reason = CeaseReason.AUTOCEASE_DUE_TO_AGE
            cease_participant_and_send_notification_messages(
                participant, AuditUsers.SYSTEM.value, {'reason': reason}
            )
            audit(action=AuditActions.CEASE_EPISODE,
                  user=AuditUsers.SYSTEM,
                  participant_ids=[episode['participant_id']],
                  additional_information={'reason': reason,
                                          'updating_next_test_due_date_from': formatted_current_next_test_due_date})
        else:
            update_to_nrl_closed_episode(participant_id, sort_key, get_current_date())
            update_participant_record(
                {'participant_id': participant['participant_id'], 'sort_key': participant['sort_key']},
                {'next_test_due_date': formatted_next_test_due_date}
            )
            audit(action=AuditActions.CLOSE_NON_RESPONDER_EPISODE,
                  user=AuditUsers.SYSTEM,
                  participant_ids=[episode['participant_id']],
                  additional_information={'updating_next_test_due_date_from': formatted_current_next_test_due_date,
                                          'updating_next_test_due_date_to': formatted_next_test_due_date})


def get_current_date():
    return datetime.now(timezone.utc).date().isoformat()


def add_episode_to_dlq(queue_url, record):
    log({'log_reference': LogReference.CLOSEEP0005})
    message = {
        'participant_id': record['participant_id'],
        'sort_key': record['sort_key'],
        'internal_id': get_internal_id()
    }
    send_new_message(queue_url, message)
