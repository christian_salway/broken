import json
from datetime import datetime, timedelta, timezone
from unittest import TestCase
from boto3.dynamodb.conditions import Key, And
from mock import patch, Mock, call
from common.models.participant import EpisodeStatus

mock_env_vars = {
    'NON_RESPONDERS_QUEUE': 'NON_RESPONDERS_QUEUE',
    'NON_RESPONDER_THRESHOLD_IN_DAYS': '182'
}

with patch('os.environ', mock_env_vars):
    with patch('common.log.get_internal_id', Mock(return_value='1')):
        from identify_non_responders import identify_non_responders

sqs_client_mock = Mock()
participants_table_mock = Mock()


@patch('common.utils.sqs_utils.get_sqs_client',
       Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
class TestIdentifyInvitableParticipants(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        participants_table_mock.query.return_value = {'Items': [
            {
                'participant_id': '123456',
                'sort_key': 'EPISODE#2020-01-01',
                'test_due_date': '2020-01-01',
                'proposed_next_test_due_date': '2021-01-01'
            }
        ]}
        sqs_client_mock.send_message_batch.return_value = {'Success': 1}
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    def test_identify_non_responders(self):
        # Setup the function invocation
        context = Mock()
        context.function_name = ''
        non_responder_date = datetime.now(timezone.utc) - \
            timedelta(int(mock_env_vars['NON_RESPONDER_THRESHOLD_IN_DAYS']))
        non_responder_timestamp = non_responder_date.date().isoformat()

        identify_non_responders.lambda_handler({}, context)

        reminded_condition_expression = And(
            Key('live_record_status').eq(EpisodeStatus.REMINDED.value), (Key('test_due_date').lte(non_responder_timestamp)) 
        )

        reminded_call = call(
            IndexName='episode-test-due-date',
            KeyConditionExpression=reminded_condition_expression,
            ProjectionExpression='#sort_key, #participant_id, #test_due_date, #proposed_next_test_due_date',
            ExpressionAttributeNames={
                '#sort_key': 'sort_key',
                '#participant_id': 'participant_id',
                '#test_due_date': 'test_due_date',
                '#proposed_next_test_due_date': 'proposed_next_test_due_date'
            }
        )

        expected_message = {
            "sort_key": "EPISODE#2020-01-01",
            "participant_id": "123456",
            "test_due_date": "2020-01-01",
            "proposed_next_test_due_date": "2021-01-01",
            "internal_id": "1"}
        expected_entries = [{
            'Id': '0',
            'MessageBody': json.dumps(expected_message)
        }]

        participants_table_mock.query.assert_has_calls([reminded_call])

        # Asserts the record has been sent to the invite queue
        sqs_client_mock.send_message_batch.assert_has_calls(
            [call(
                QueueUrl='NON_RESPONDERS_QUEUE',
                Entries=expected_entries)])
