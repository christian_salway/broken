import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    IDENTNR0001 = (logging.INFO, 'No NRL episodes found')
    IDENTNR0002 = (logging.INFO, 'Episodes added to non responders queue')
    IDENTNR0003 = (logging.ERROR, 'Failed to send episodes to SQS')
