import os
from datetime import datetime, timedelta, timezone
from common.utils import json_return_object
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_episode_utils import episode_state_change
from common.utils.sqs_utils import batch_send_messages
from common.models.participant import EpisodeStatus
from common.log import log
from identify_non_responders.log_references import LogReference

NON_RESPONDERS_QUEUE = os.environ['NON_RESPONDERS_QUEUE']
NON_RESPONDER_THRESHOLD_IN_DAYS = int(os.environ['NON_RESPONDER_THRESHOLD_IN_DAYS'])


@lambda_entry_point
def lambda_handler(event, context):
    non_responder_date = datetime.now(timezone.utc) - timedelta(NON_RESPONDER_THRESHOLD_IN_DAYS)
    non_responder_timestamp = non_responder_date.date().isoformat()

    non_responders_count = episode_state_change(
        [EpisodeStatus.REMINDED.value],
        non_responder_timestamp,
        send_to_non_responder_queue,
        required_fields=['sort_key', 'participant_id', 'test_due_date', 'proposed_next_test_due_date'])

    if non_responders_count == 0:
        log({'log_reference': LogReference.IDENTNR0001})
        return json_return_object(200, '')

    log({'log_reference': LogReference.IDENTNR0002, 'non_responders_count': non_responders_count})
    return json_return_object(200, '')


def send_to_non_responder_queue(records):
    messages = [_build_message(participant) for participant in records]

    failed = batch_send_messages(NON_RESPONDERS_QUEUE, messages)
    if failed:
        log({'log_reference': LogReference.IDENTNR0003, 'number_of_failed_messages': len(failed)})
        raise Exception('Failed to send sqs messages in batch operation')

    return len(messages)


def _build_message(participant):
    if 'proposed_next_test_due_date' in participant:
        return {'sort_key': participant['sort_key'],
                'participant_id': participant['participant_id'],
                'test_due_date': participant['test_due_date'],
                'proposed_next_test_due_date': participant['proposed_next_test_due_date']}
    else:
        return {'sort_key': participant['sort_key'],
                'participant_id': participant['participant_id'],
                'test_due_date': participant['test_due_date']}
