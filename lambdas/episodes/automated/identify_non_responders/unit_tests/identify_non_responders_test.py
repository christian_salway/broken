import os
from datetime import datetime, timedelta, timezone
from unittest import TestCase
from mock import patch, Mock, call, MagicMock
from common.models.participant import EpisodeStatus

QUERY_RESPONSE = {'Items': [{'participant_id': '123456',
                             'live_record_status': EpisodeStatus.REMINDED.value,
                             'sort_key': 'EPISODE#2020-10-01'}]}
NON_RESPONDER_THRESHOLD_IN_DAYS = 182
module = 'identify_non_responders.identify_non_responders'


class TestIdentifyInvitableParticipants(TestCase):
    mock_env_vars = {
        'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS',
        'NON_RESPONDERS_QUEUE': 'NON_RESPONDERS_QUEUE',
        'NON_RESPONDER_THRESHOLD_IN_DAYS': str(NON_RESPONDER_THRESHOLD_IN_DAYS)
    }

    @patch('boto3.client', MagicMock())
    @patch('boto3.resource', MagicMock())
    @patch.dict(os.environ, mock_env_vars)
    def setUp(self):
        import identify_non_responders.identify_non_responders as _identify_non_responders
        self.identify_non_responders = _identify_non_responders
        self.log_patcher = patch('common.log.logger').start()
        self.batch_send_messages_patcher = patch(f'{module}.batch_send_messages').start()
        self.context = Mock()
        self.context.function_name = ''

        self.batch_send_messages_patcher.return_value = []

    def tearDown(self):
        self.log_patcher.stop()

    @patch(f'{module}.send_to_non_responder_queue')
    @patch(f'{module}.episode_state_change')
    def test_identify_non_responders(self, state_change_mock, message_mock):
        state_change_mock.return_value = QUERY_RESPONSE
        invite_date = datetime.now(timezone.utc) - timedelta(NON_RESPONDER_THRESHOLD_IN_DAYS)
        invite_timestamp = invite_date.date().isoformat()

        response = self.identify_non_responders.lambda_handler({}, self.context)

        state_change_mock.assert_called_with(
            [EpisodeStatus.REMINDED.value],
            invite_timestamp,
            message_mock,
            required_fields=['sort_key', 'participant_id', 'test_due_date', 'proposed_next_test_due_date']
        )
        self.assertEqual(response['statusCode'], 200)

    def test_send_to_non_responder_queue(self):
        mock_records = [
            {
                'sort_key': 'sort_key_1',
                'participant_id': 'participant_id_1',
                'test_due_date': 'test_due_date_1',
                'proposed_next_test_due_date': 'proposed_next_test_due_date_1'
            },
            {
                'sort_key': 'sort_key_2',
                'participant_id': 'participant_id_2',
                'test_due_date': 'test_due_date_2',
                'proposed_next_test_due_date': 'proposed_next_test_due_date_2'
            },
            {
                'sort_key': 'sort_key_3',
                'participant_id': 'participant_id_3',
                'test_due_date': 'test_due_date_3',
                'proposed_next_test_due_date': 'proposed_next_test_due_date_3'
            },
        ]

        actual = self.identify_non_responders.send_to_non_responder_queue(mock_records)

        self.batch_send_messages_patcher.assert_has_calls([
            call('NON_RESPONDERS_QUEUE', mock_records)])

        self.assertEqual(actual, 3)
