import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    IDENTINV0001 = (logging.INFO, 'No invitable episodes found')
    IDENTINV0002 = (logging.INFO, 'Successfully sent invites')
