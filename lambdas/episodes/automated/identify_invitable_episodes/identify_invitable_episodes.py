import json
import os
from datetime import datetime, timedelta, timezone

from common.log import log, get_internal_id
from common.utils.participant_episode_utils import episode_state_change
from common.utils.sqs_utils import send_new_message_batch
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils import json_return_object
from common.models.participant import EpisodeStatus
from identify_invitable_episodes.log_references import LogReference

INVITE_QUEUE_URL = os.environ['INVITE_QUEUE_URL']
INVITE_DAYS = int(os.environ['INVITE_THRESHOLD_IN_DAYS'])


@lambda_entry_point
def lambda_handler(event, context):
    invite_date = datetime.now(timezone.utc) + timedelta(INVITE_DAYS)
    invite_timestamp = invite_date.date().isoformat()

    record_count = episode_state_change([EpisodeStatus.ACCEPTED.value, EpisodeStatus.PNL.value], invite_timestamp, send_to_invite_queue, 
                                        required_fields=['sort_key', 'participant_id'])

    if record_count == 0:
        log({'log_reference': LogReference.IDENTINV0001})
    else:
        log({'log_reference': LogReference.IDENTINV0002, 'invite_count': record_count})
    return json_return_object(200, '')


def send_to_invite_queue(records):
    messages = []
    for idx, participant in enumerate(records):
        messages.append({
            'Id': str(idx),
            'MessageBody': json.dumps({
                'sort_key': participant['sort_key'],
                'participant_id': participant['participant_id'],
                'internal_id': get_internal_id()
            })
        })
        if len(messages) == 10:
            send_new_message_batch(INVITE_QUEUE_URL, messages)
            messages = []
    if len(messages) > 0:
        send_new_message_batch(INVITE_QUEUE_URL, messages)

    return len(records)
