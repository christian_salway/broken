import os
from datetime import datetime, timedelta, timezone
from unittest import TestCase
from common.models.participant import EpisodeStatus

from mock import patch, Mock, call, MagicMock

QUERY_RESPONSE = {'Items': [{'participant_id': '123456',
                             'live_record_status': EpisodeStatus.INVITED.value,
                             'sort_key': 'EPISODE#2020-10-01'}]}
INVITE_DAYS = 84


@patch('identify_invitable_episodes.identify_invitable_episodes.get_internal_id', Mock(return_value='1'))
class TestIdentifyInvitableParticipants(TestCase):
    mock_env_vars = {
        'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS',
        'INVITE_QUEUE_URL': 'INVITE_QUEUE_URL',
        'INVITE_THRESHOLD_IN_DAYS': '84'
    }

    @patch('boto3.client', MagicMock())
    @patch('boto3.resource', MagicMock())
    @patch.dict(os.environ, mock_env_vars)
    def setUp(self):
        import identify_invitable_episodes.identify_invitable_episodes as _identify_invitable_episodes
        self.identify_invitable_episodes_module = _identify_invitable_episodes
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = ''

    def tearDown(self):
        self.log_patcher.stop()

    @patch('identify_invitable_episodes.identify_invitable_episodes.send_to_invite_queue')
    @patch('identify_invitable_episodes.identify_invitable_episodes.episode_state_change')
    def test_identify_invitable_episodes(self, state_change_mock, message_mock):
        state_change_mock.return_value = QUERY_RESPONSE
        invite_date = datetime.now(timezone.utc) + timedelta(INVITE_DAYS)
        invite_timestamp = invite_date.date().isoformat()

        response = self.identify_invitable_episodes_module.lambda_handler({}, self.context)

        state_change_mock.assert_called_with(
            [EpisodeStatus.ACCEPTED.value, EpisodeStatus.PNL.value],
            invite_timestamp,
            message_mock,
            required_fields=['sort_key', 'participant_id']
        )
        self.assertEqual(response['statusCode'], 200)

    @patch('identify_invitable_episodes.identify_invitable_episodes.send_new_message_batch')
    def test_send_to_invite_queue(self, message_mock):
        mock_records = [
            {
                'sort_key': 'sort_key_1',
                'participant_id': 'participant_id_1',
                'internal_id': '1'
            },
            {
                'sort_key': 'sort_key_2',
                'participant_id': 'participant_id_2',
                'internal_id': '1'
            },
            {
                'sort_key': 'sort_key_3',
                'participant_id': 'participant_id_3',
                'internal_id': '1'
            },
        ]

        actual = self.identify_invitable_episodes_module.send_to_invite_queue(mock_records)

        message_mock.assert_has_calls(
            [call('INVITE_QUEUE_URL',
                  [{'Id': '0',
                    'MessageBody':
                        '{"sort_key": "sort_key_1", "participant_id": "participant_id_1", "internal_id": "1"}'},
                   {'Id': '1',
                    'MessageBody':
                        '{"sort_key": "sort_key_2", "participant_id": "participant_id_2", "internal_id": "1"}'},
                   {'Id': '2',
                    'MessageBody':
                        '{"sort_key": "sort_key_3", "participant_id": "participant_id_3", "internal_id": "1"}'}
                   ])
             ])

        self.assertEqual(actual, 3)
