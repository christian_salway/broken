# Identify invitable participants

Thi lambda is invoked under a cron cycle through cloudwatch, and determines which participants have their episodes test_due_date within a date range specified in terraform, and sends
invite letters to these participants.


This lambda specifically will send a message to the `participants-to-get-an-invite` queue for each participant identified as needing an invite, from which the `create-invite` lambda consumes the message. This message will contain the following:
```
{
  'participant_id': '<current_participant_id>',
  'sort_key': 'PARTICIPANT'
}
```


## Eligibility criteria
* next test due-date is in 6 weeks
 
See https://nhsd-confluence.digital.nhs.uk/display/CSP/4+Selection+into+Invite+process for more info

