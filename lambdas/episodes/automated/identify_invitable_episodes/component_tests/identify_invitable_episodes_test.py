from datetime import datetime, timedelta, timezone
from unittest import TestCase

from boto3.dynamodb.conditions import Key, And
from mock import patch, Mock, call
from common.models.participant import EpisodeStatus

participants_table_mock = Mock()
sqs_client_mock = Mock()
mock_env_vars = {
    'INVITE_QUEUE_URL': 'INVITE_QUEUE_URL',
    'INVITE_THRESHOLD_IN_DAYS': '84'
}

with patch('os.environ', mock_env_vars):
    from identify_invitable_episodes import identify_invitable_episodes


@patch('identify_invitable_episodes.identify_invitable_episodes.get_internal_id', Mock(return_value='1'))
class TestIdentifyInvitableParticipants(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        participants_table_mock.query.return_value = {'Items': [
            {
                'participant_id': '123456',
                'sort_key': 'EPISODE#2020-01-01',
            }
        ]}
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
           Mock(return_value=participants_table_mock))
    def test_identify_invitable_episodes(self):
        # Setup the function invocation
        context = Mock()
        context.function_name = ''
        invite_date = datetime.now(timezone.utc) + timedelta(int(mock_env_vars['INVITE_THRESHOLD_IN_DAYS']))
        invite_timestamp = invite_date.date().isoformat()

        identify_invitable_episodes.lambda_handler({}, context)

        # Asserts that invitable episodes have been queried for
        pnl_condition_expression = And(Key('live_record_status').eq(EpisodeStatus.PNL.value),
                                       (Key('test_due_date').lte(invite_timestamp)))
        accepted_condition_expression = And(
            Key('live_record_status').eq(EpisodeStatus.ACCEPTED.value), (Key('test_due_date').lte(invite_timestamp))
        )

        accepted_call = call(
            IndexName='episode-test-due-date',
            KeyConditionExpression=accepted_condition_expression,
            ProjectionExpression='#sort_key, #participant_id',
            ExpressionAttributeNames={'#sort_key': 'sort_key', '#participant_id': 'participant_id'}
        )

        pnl_call = call(
            IndexName='episode-test-due-date',
            KeyConditionExpression=pnl_condition_expression,
            ProjectionExpression='#sort_key, #participant_id',
            ExpressionAttributeNames={'#sort_key': 'sort_key', '#participant_id': 'participant_id'}
        )

        participants_table_mock.query.assert_has_calls([accepted_call, pnl_call])

        # Asserts the record has been sent to the invite queue
        sqs_client_mock.send_message_batch.assert_has_calls(
            [call(QueueUrl='INVITE_QUEUE_URL',
                  Entries=[
                      {'Id': '0',
                       'MessageBody':
                           '{"sort_key": "EPISODE#2020-01-01", "participant_id": "123456", "internal_id": "1"}'}
                  ]),
             call(QueueUrl='INVITE_QUEUE_URL',
                  Entries=[
                      {'Id': '0',
                       'MessageBody':
                           '{"sort_key": "EPISODE#2020-01-01", "participant_id": "123456", "internal_id": "1"}'}
                  ])])
