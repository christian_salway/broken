import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    CREREM0001 = (logging.INFO, 'Received records to create reminders for')
    CREREM0002 = (logging.INFO, 'Successfully created reminders for records')
