from unittest import TestCase
from mock import patch, Mock, call
import json
from datetime import datetime, timezone
from ddt import ddt, data, unpack
from common.models.participant import EpisodeStatus


participants_table_mock = Mock()
sqs_client_mock = Mock()
example_date = datetime(2020, 1, 23, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date
mock_env_vars = {
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'NOTIFICATION_QUEUE_URL': 'NOTIFICATION_QUEUE_URL',
    'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS'
}

with patch('datetime.datetime', datetime_mock):
    with patch('os.environ', mock_env_vars):
        from create_reminder import create_reminder
        from common.test_mocks.mock_events import sqs_mock_event


@ddt
class TestCreateReminder(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @unpack
    @data(('X09', 'PA2'), (None, 'PA4'))
    @patch('create_reminder.create_reminder.os.environ', mock_env_vars)
    @patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
    @patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
           Mock(return_value=participants_table_mock))
    def test_create_reminder(self, registered_gp_practice_code, template_code):
        # Setup the function invocation
        context = Mock()
        context.function_name = ''
        event = sqs_mock_event({
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-01-01',
            'registered_gp_practice_code': registered_gp_practice_code
        })

        create_reminder.lambda_handler(event, context)

        # Asserts that episode has been updated
        participants_table_mock.update_item.assert_called_with(
            Key={'participant_id': '123456', 'sort_key': 'EPISODE#2020-01-01'},
            UpdateExpression='SET #status = :status, #live_record_status = :live_record_status, #reminded_date = :reminded_date',  
            ExpressionAttributeValues={
                ':status': EpisodeStatus.REMINDED.value,
                ':live_record_status': EpisodeStatus.REMINDED.value,
                ':reminded_date': example_date.date().isoformat()
            },
            ExpressionAttributeNames={
                '#status': 'status',
                '#live_record_status': 'live_record_status',
                '#reminded_date': 'reminded_date'
            },
            ReturnValues='NONE'
        )

        # Asserts the record has been sent to the notification queue
        # Asserts the record has been sent to the letter queue
        # Asserts audit has been sent to the audit queue
        notification_call = call(
            QueueUrl='NOTIFICATION_QUEUE_URL',
            MessageBody=json.dumps({
                'participant_id': '123456',
                'registered_gp_practice_code': registered_gp_practice_code,
                'sort_key': 'EPISODE#2020-01-01',
                'template': template_code,
                'type': 'Reminder letter'
            }, indent=4, sort_keys=True, default=str)
        )
        audit_call = call(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody=json.dumps({
                "action": "REMINDER_CREATED",
                "timestamp": "2020-01-23T13:48:08+00:00",
                "internal_id": "1",
                "nhsid_useruid": "SYSTEM",
                "session_id": "NONE",
                "participant_ids": ["123456"]
            })
        )
        sqs_client_mock.send_message.assert_has_calls([notification_call, audit_call])
