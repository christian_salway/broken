# Create Reminder

This lambda will collate all relevant information for a participant identified in identify-remindable-episodes to generate the csv for letter printing and update the episode to transition it to the REMINDER state
 
See https://nhsd-confluence.digital.nhs.uk/display/CSP/5+Selection+into+Remind+process for more info

This will also send a message to the `notifictions-to-create` queue when needing to create an notification. Containing the following data:

```
{
  'participant_id': <current_participant_id>,
  'type':'Reminder'
}
```

See the `create_notification` lambda  for more details on the structure and function of a notification record