from unittest import TestCase
from datetime import datetime, timezone
from mock import patch, Mock
from common.test_mocks.mock_events import sqs_mock_event
from common.models.participant import EpisodeStatus
from ddt import ddt, data, unpack


@ddt
@patch('common.utils.participant_episode_utils.dynamodb_update_item', Mock())
class TestCreateReminder(TestCase):
    mock_env_vars = {
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
        'NOTIFICATION_QUEUE_URL': 'NOTIFICATION_QUEUE_URL'
    }
    MODULE = 'create_reminder.create_reminder'

    def setUp(self):
        import create_reminder.create_reminder as _create_reminder
        import common.utils.audit_utils as _audit_utils
        self.create_reminder_module = _create_reminder
        self.audit_utils = _audit_utils
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.env_patcher = patch('os.environ', self.mock_env_vars)
        self.env_patcher.start()
        self.context = Mock()
        self.context.function_name = ''

        self.mock_event = sqs_mock_event({
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-01-01',
            'registered_gp_practice_code': 'X09'
        })

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    @patch(f'{MODULE}.send_new_message', Mock())
    @patch(f'{MODULE}.audit')
    def test_lambda_happy_path(self, audit_mock):
        # Act
        response = self.create_reminder_module.lambda_handler(self.mock_event, self.context)

        # Assert
        audit_mock.assert_called_with(
            self.audit_utils.AuditActions.REMINDER_CREATED,
            user=self.audit_utils.AuditUsers.SYSTEM,
            participant_ids=['123456']
        )
        self.assertEqual(response['statusCode'], 200)

    @patch(f'{MODULE}.update_participant_episode_record')
    @patch(f'{MODULE}.datetime')
    def test_update_episode_record(self, datetime_mock, update_mock):
        record = {
            'participant_id': '123456',
            'sort_key': 'EPISODE#whenever'
        }
        datetime_mock.now.return_value = datetime(2020, 1, 1, tzinfo=timezone.utc)
        self.create_reminder_module.update_episode_record(record)
        update_mock.assert_called_with(record, {
            'status': EpisodeStatus.REMINDED.value,
            'live_record_status': EpisodeStatus.REMINDED.value,
            'reminded_date': '2020-01-01'
        })

    @patch(f'{MODULE}.send_new_message')
    @unpack
    @data(('X09', 'PA2'), (None, 'PA4'))
    def test_send_notification_record(self, registered_gp_practice_code, template_code, msg_mock):
        # Arrange
        record = {
            'participant_id': '123456',
            'sort_key': 'EPISODE#whenever',
            'type': 'Reminder letter',
            'registered_gp_practice_code': registered_gp_practice_code
        }

        message = {**record, **{'template': template_code}}

        # Act
        self.create_reminder_module.send_notification_record(record)

        # Assert
        msg_mock.assert_called_with('NOTIFICATION_QUEUE_URL', message)
