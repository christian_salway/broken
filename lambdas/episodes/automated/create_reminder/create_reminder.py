import os
import json
from datetime import datetime, timezone
from common.utils import json_return_object
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.utils.participant_episode_utils import (
    update_participant_episode_record
)
from common.utils.sqs_utils import send_new_message
from create_reminder.log_references import LogReference
from common.log import log
from common.models.letters import NotificationType
from common.models.participant import EpisodeStatus


@lambda_entry_point
def lambda_handler(event, context):
    records = event.get('Records', [{}])
    log({'log_reference': LogReference.CREREM0001, 'record_count': len(records)})
    for msg in records:
        record = json.loads(msg['body'])
        update_episode_record(record)
        send_notification_record(record)
        audit(AuditActions.REMINDER_CREATED, user=AuditUsers.SYSTEM, participant_ids=[record['participant_id']])
    log({'log_reference': LogReference.CREREM0002})
    return json_return_object(200, '')


def update_episode_record(record):
    now = datetime.now(timezone.utc)
    date = now.date().isoformat()
    key = {
        'participant_id': record['participant_id'],
        'sort_key': record['sort_key']
    }
    update_participant_episode_record(key, {
        'status': EpisodeStatus.REMINDED.value,
        'live_record_status': EpisodeStatus.REMINDED.value,
        'reminded_date': date
    })


def send_notification_record(record):
    notification_url = os.environ.get('NOTIFICATION_QUEUE_URL')
    record['type'] = NotificationType.REMINDER
    record['template'] = 'PA2' if record['registered_gp_practice_code'] else 'PA4'
    send_new_message(notification_url, record)
