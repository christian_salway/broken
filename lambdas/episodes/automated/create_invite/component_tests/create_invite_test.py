from unittest import TestCase
from mock import patch, Mock, call
import json
from datetime import datetime, timezone
from common.test_mocks.mock_events import sqs_mock_event
from common.models.participant import EpisodeStatus

mock_env_vars = {
    'LETTER_QUEUE_URL': 'LETTER_QUEUE_URL',
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'NOTIFICATION_QUEUE_URL': 'NOTIFICATION_QUEUE_URL',
    'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS'
}
example_date = datetime(2020, 1, 23, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date
participants_table_mock = Mock()
sqs_client_mock = Mock()
MODULE = 'create_invite.create_invite'
MODULE_UTILS = 'common.utils'


with patch('datetime.datetime', datetime_mock):
    with patch('common.log.get_internal_id', Mock(return_value='1')):
        with patch('os.environ', mock_env_vars):
            from create_invite import create_invite


@patch(f'{MODULE_UTILS}.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch(f'{MODULE_UTILS}.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(return_value=participants_table_mock))
class TestCreateInvite(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        self.env_patcher = patch('os.environ', mock_env_vars)
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        self.log_patcher.start()
        self.env_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    @patch(f'{MODULE}.get_participant_by_participant_id')
    def test_create_invite(self, get_participant_mock):
        # Setup the function invocation
        context = Mock()
        context.function_name = ''
        event = {
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-01-01',
            'nhs_number': '9999999999'
        }
        get_participant_mock.return_value = {
            'registered_gp_practice_code': 'ABC123'
        }

        create_invite.lambda_handler(sqs_mock_event(event), context)

        # asserts the participant has been updated
        participants_table_mock.update_item.assert_called_with(
            Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'},
            UpdateExpression='SET #invited_date = :invited_date',
            ExpressionAttributeValues={':invited_date': '2020-01-23'},
            ExpressionAttributeNames={'#invited_date': 'invited_date'},
            ReturnValues='NONE'
        )

        # Asserts that episode has been updated
        participants_table_mock.update_item.assert_any_call(
            Key={'participant_id': '123456', 'sort_key': 'EPISODE#2020-01-01'},
            UpdateExpression='SET #status = :status, #live_record_status = :live_record_status, '
                             '#invited_date = :invited_date',
            ExpressionAttributeValues={
                ':status': EpisodeStatus.INVITED.value, ':live_record_status': EpisodeStatus.INVITED.value, ':invited_date': '2020-01-23' 
            },
            ExpressionAttributeNames={
                '#status': 'status', '#live_record_status': 'live_record_status', '#invited_date': 'invited_date'
            },
            ReturnValues='NONE'
        )

        # Asserts that the get participant method has been called
        get_participant_mock.assert_called_with('123456')

        # Asserts the record has been sent to the notification queue
        # Asserts the record has been sent to the letter queue
        # Asserts audit has been sent to the audit queue
        notification_call = call(
            QueueUrl='NOTIFICATION_QUEUE_URL',
            MessageBody=json.dumps({
                'participant_id': '123456',
                'sort_key': 'EPISODE#2020-01-01',
                'nhs_number': '9999999999',
                'type': 'Invitation letter',
                'template': 'PA1'
            }, indent=4, sort_keys=True, default=str)
        )
        audit_call = call(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody=json.dumps({
                "action": "INVITE_CREATED",
                "timestamp": "2020-01-23T13:48:08+00:00",
                "internal_id": "1",
                "nhsid_useruid": "SYSTEM",
                "session_id": "NONE",
                "participant_ids": ["123456"]
            })
        )
        sqs_client_mock.send_message.assert_has_calls([notification_call, audit_call])
