# Create Invite

This lambda will collate all relevant information for a participant identified in identify-eligible-episodes to generate the csv for letter printing and update the episode to transition it to the INVITED state
 
See https://nhsd-confluence.digital.nhs.uk/display/CSP/4+Selection+into+Invite+process for more info

This will also send a message to the `notifictions-to-create` queue when needing to create an notification. Containing the following data:

```
{
  'participant_id': <current_participant_id>,
  'type':'Invitation'
}
```

See the `create_notification` lambda  for more details on the structure and function of a notification record