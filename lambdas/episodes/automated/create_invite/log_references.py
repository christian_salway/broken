import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    CREINV0001 = (logging.INFO, 'Received records to create reminders for')
    CREINV0002 = (logging.INFO, 'Successfully created reminders for records')
    CREINVTEMP = (logging.INFO, 'Template issue debugging')
