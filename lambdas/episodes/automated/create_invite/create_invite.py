import json
import os
from datetime import datetime, timezone
from common.utils import json_return_object
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.utils.participant_utils import (
    get_participant_by_participant_id,
    update_participant_record
)
from common.utils.participant_episode_utils import (
    update_participant_episode_record
)
from common.utils.sqs_utils import send_new_message
from create_invite.log_references import LogReference
from common.log import log
from common.models.letters import NotificationType
from common.models.participant import EpisodeStatus

INVITATION_LETTER_CODE = 'PA1'
NOT_REGISTERED_INVITATION_LETTER_CODE = 'PA3'


@lambda_entry_point
def lambda_handler(event, context):
    records = event.get('Records', [{}])
    log({'log_reference': LogReference.CREINV0001, 'record_count': len(records)})
    for msg in records:
        record = json.loads(msg['body'])
        update_episode_record(record)
        update_participant(record)
        send_notification_record(record)
        audit(AuditActions.INVITE_CREATED, user=AuditUsers.SYSTEM, participant_ids=[record['participant_id']])
    log({'log_reference': LogReference.CREINV0002})
    return json_return_object(200, '')


def update_participant(record):
    key = {
        'participant_id': record['participant_id'],
        'sort_key': 'PARTICIPANT'
    }
    update_participant_record(key, {
        'invited_date': datetime.now(timezone.utc).date().isoformat()
    })


def update_episode_record(record):
    key = {
        'participant_id': record['participant_id'],
        'sort_key': record['sort_key']
    }
    update_participant_episode_record(key, {
        'status': EpisodeStatus.INVITED.value,
        'live_record_status': EpisodeStatus.INVITED.value,
        'invited_date': datetime.now(timezone.utc).date().isoformat()
    })


def send_notification_record(record):
    notification_url = os.environ['NOTIFICATION_QUEUE_URL']
    record['type'] = NotificationType.INVITATION
    record['template'] = determine_template_code(record)
    send_new_message(notification_url, record)


def determine_template_code(record):
    if is_participant_registered(record):
        return INVITATION_LETTER_CODE
    return NOT_REGISTERED_INVITATION_LETTER_CODE


def is_participant_registered(record):
    participant_id = record['participant_id']
    participant = get_participant_by_participant_id(participant_id)
    if participant.get('registered_gp_practice_code'):
        return True
    return False
