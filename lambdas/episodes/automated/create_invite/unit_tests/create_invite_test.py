from unittest import TestCase
from datetime import datetime, timezone
from mock import patch, Mock
from common.test_mocks.mock_events import sqs_mock_event
from common.models.participant import EpisodeStatus


@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock())
class TestCreateInvite(TestCase):
    module = 'create_invite.create_invite'
    mock_env_vars = {
        'LETTER_QUEUE_URL': 'LETTER_QUEUE_URL',
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
        'NOTIFICATION_QUEUE_URL': 'NOTIFICATION_QUEUE_URL'
    }

    @patch('boto3.client', Mock())
    @patch('boto3.resource', Mock())
    def setUp(self):
        import create_invite.create_invite as _create_invite
        import common.utils.audit_utils as _audit_utils
        self.create_invite_module = _create_invite
        self.audit_utils = _audit_utils
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.env_patcher = patch('os.environ', self.mock_env_vars)
        self.env_patcher.start()
        self.context = Mock()
        self.context.function_name = ''
        event = {
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-01-01'
        }
        self.mock_event = sqs_mock_event(event)

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    @patch(f'{module}.send_new_message', Mock())
    @patch(f'{module}.audit')
    @patch(f'{module}.get_participant_by_participant_id')
    def test_lambda_happy_path(self, get_participant_mock, audit_mock):
        # Arrange
        participant = {
            'participant_id': '12345',
            'registered_gp_practice_code': 'ABCDEF'
        }

        get_participant_mock.return_value = participant

        # Act
        response = self.create_invite_module.lambda_handler(self.mock_event, self.context)

        # Assert
        audit_mock.assert_called_with(
            self.audit_utils.AuditActions.INVITE_CREATED,
            user=self.audit_utils.AuditUsers.SYSTEM,
            participant_ids=['123456']
        )
        self.assertEqual(response['statusCode'], 200)

    @patch(f'{module}.update_participant_episode_record')
    @patch(f'{module}.datetime')
    def test_update_episode_record(self, datetime_mock, update_mock):
        record = {
            'participant_id': '123456',
            'sort_key': 'EPISODE#whenever',
            'notification_sequence': '1',
        }
        datetime_mock.now.return_value = datetime(2020, 1, 1, tzinfo=timezone.utc)
        self.create_invite_module.update_episode_record(record)
        record.pop('notification_sequence')
        update_mock.assert_called_with(
            record, {'status': EpisodeStatus.INVITED.value, 'live_record_status': EpisodeStatus.INVITED.value, 'invited_date': '2020-01-01'} 
        )

    @patch(f'{module}.send_new_message')
    @patch(f'{module}.determine_template_code')
    def test_send_notification_record(self, template_mock, msg_mock):
        # Arrange
        base_record = {
            'participant_id': '123456',
            'sort_key': 'EPISODE#whenever',
            'nhs_number': '9999999999',
        }
        template_mock.return_value = 'abcd'

        # Act
        self.create_invite_module.send_notification_record(base_record)

        # Assert
        msg_mock.assert_called_with('NOTIFICATION_QUEUE_URL', {
            'participant_id': '123456',
            'sort_key': 'EPISODE#whenever',
            'nhs_number': '9999999999',
            'type': 'Invitation letter',
            'template': 'abcd'
        })

    @patch(f'{module}.is_participant_registered')
    def test_determine_template_code_when_registered_returns_pa1(self, participant_registered_mock):
        participant_registered_mock.return_value = True
        record = {'hello': 'world'}
        result = self.create_invite_module.determine_template_code(record)
        participant_registered_mock.assert_called_with(record)
        self.assertEqual(result, 'PA1')

    @patch(f'{module}.is_participant_registered')
    def test_determine_template_code_when_unregistered_returns_pa3(self, participant_registered_mock):
        participant_registered_mock.return_value = False
        record = {'hello': 'world'}
        result = self.create_invite_module.determine_template_code(record)
        participant_registered_mock.assert_called_with(record)
        self.assertEqual(result, 'PA3')

    @patch(f'{module}.get_participant_by_participant_id')
    def test_is_participant_registered_no_practice_field(self, get_participant_mock):
        participant = {'participant_id': '12345'}
        record = {'participant_id': '12345'}
        get_participant_mock.return_value = participant
        result = self.create_invite_module.is_participant_registered(record)
        get_participant_mock.assert_called_with('12345')
        self.assertFalse(result)

    @patch(f'{module}.get_participant_by_participant_id')
    def test_is_participant_registered_with_practice_field(self, get_participant_mock):
        participant = {
            'participant_id': '12345',
            'registered_gp_practice_code': 'ABCDEF'
        }

        record = {'participant_id': '12345'}
        get_participant_mock.return_value = participant
        result = self.create_invite_module.is_participant_registered(record)
        get_participant_mock.assert_called_with('12345')
        self.assertTrue(result)

    @patch(f'{module}.update_participant_record')
    @patch(f'{module}.datetime')
    def test_update_participant(self, datetime_mock, participant_mock):
        record = {
            'participant_id': '123456',
            'sort_key': 'PARTICIPANT',
            'type': 'Invitation letter'
        }
        datetime_mock.now.return_value = datetime(2020, 1, 1, tzinfo=timezone.utc)
        self.create_invite_module.update_participant(record)
        participant_mock.assert_called_with(
            {
                'participant_id': record['participant_id'],
                'sort_key': 'PARTICIPANT'
            },
            {
                'invited_date': '2020-01-01'
            }
        )
