from common.log import log
from update_notifications.log_references import LogReference
from common.utils.notification_utils import (query_print_file_notifications,
                                             query_print_file_notifications_and_filter,
                                             update_notification_status)
from common.utils.print_file_utils import (lock_printfile, unlock_printfile)
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.models.letters import NotificationStatus


NOTIFICATION_STATUS_AUDIT_LOOKUP = {
    NotificationStatus.IN_PRINT_FILE: AuditActions.NOTIFICATION_IN_PRINT_FILE,
    NotificationStatus.WITH_PRINTER: AuditActions.LETTER_SENT,
    NotificationStatus.FAILED: AuditActions.LETTER_NOT_SENT
}


@lock_printfile
def update_all_notifications_in_print_file(print_file_name, sort_key, notification_lookup):
    notification_status = notification_lookup['query_status']
    new_notification_status = notification_lookup['new_status']

    if notification_status == 'ANY':
        log({'log_reference': LogReference.UPDATENOT0010})

        query_print_file_notifications(
            print_file_name,
            process_records_callback,
            {
                'new_status': new_notification_status
            }
        )
    else:
        log({'log_reference': LogReference.UPDATENOT0011})
        query_print_file_notifications_and_filter(
            print_file_name,
            notification_status,
            process_records_callback,
            {
                'new_status': new_notification_status,
            }
        )


def process_records_callback(notification_records, new_status):
    log({'log_reference': LogReference.UPDATENOT0003})

    try:
        for notification_record in notification_records:
            update_notification_status(notification_record, new_status)
            audit(
                action=NOTIFICATION_STATUS_AUDIT_LOOKUP[new_status],
                user=AuditUsers.LETTERS,
                participant_ids=[notification_record['participant_id']],
                additional_information={
                    'print_file_name': notification_record['file_name'],
                    'notification_type': notification_record['type'],
                    'address': parse_address(notification_record['data'])
                }
            )

    except Exception:
        log({
            'log_reference': LogReference.UPDATENOTEX0002,
            'notification_id': {
                'participant_id': notification_record['participant_id'],
                'sort_key': notification_record['sort_key']
            },
            'add_exception_info': True
        })


@unlock_printfile
def release_printfile_lock(file_name, sort_key):
    log({'log_reference': LogReference.UPDATENOT0005, 'file_name': file_name})


def parse_address(letter_row):
    address_keys = {
        'DEST_ADD1': 'address_line_1',
        'DEST_ADD2': 'address_line_2',
        'DEST_ADD3': 'address_line_3',
        'DEST_ADD4': 'address_line_4',
        'DEST_ADD5': 'address_line_5',
        'DEST_POSTCODE': 'DEST_POSTCODE',
    }
    return {address_keys[k]: letter_row[k] for k in address_keys if k in letter_row}
