import os
import json
from unittest import TestCase
from datetime import datetime, timezone
from mock import Mock, patch, call
from boto3.dynamodb.conditions import Key, Attr
from common.utils.dynamodb_access.table_names import TableNames
from common.models.participant import ParticipantSortKey


EXAMPLE_FILE_NAME = "CSAS_EU-WEST-2_RP_210823095144.dat"
EXAMPLE_PRINT_FILE = {
    "file_name": EXAMPLE_FILE_NAME,
    "sort_key":  "CIC",
    "type":      "invite-reminder",
    "status":    "PENDING",
    "row_count": "15000",
    "created":   "2020-01-23T13:48:08.000+00:00",
    "lock":      False
}
EXAMPLE_NOTIFICATION = {
    "participant_id":           "unique-uuid",
    "sort_key":                 "NOTIFICATION#2001-01-01#1#2020-08-25T13:48:08.000Z",
    "nhs_number":               "9234567890",
    "file_name":                f'CIC/{EXAMPLE_FILE_NAME}',
    "notification_date":        "2001-01-01",
    "notification_sequence":    1,
    "created":                  "2020-08-25T13:48:08.000Z",
    "type":                     "First Non-Responder",
    "status":                   "PENDING_PRINT_FILE",
    "data": {
    }
}


def read_stream(file_name):
    with open(os.path.join(os.path.dirname(__file__), f'test_data/{file_name}')) as file:
        return json.loads(file.read())


sqs_client_mock = Mock()


@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
class TestUpdateNotifications(TestCase):

    mock_env_vars = {
        'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
        'DYNAMODB_PRINT_FILES': 'PRINT_FILES_TABLE',
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL'
    }

    example_date = datetime(2020, 9, 29, 14, 26, 1, tzinfo=timezone.utc)

    @classmethod
    @patch('boto3.client')
    @patch('boto3.resource')
    @patch('os.environ', mock_env_vars)
    def setUpClass(cls, boto3_resource, boto3_client):
        cls.log_patcher = patch('common.log.log')
        cls.env_patcher = patch('os.environ', cls.mock_env_vars)
        cls.context = Mock()
        cls.context.function_name = 'update_notification'

        table_mock = Mock()
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = table_mock
        boto3_resource.return_value = boto3_table_mock
        cls.table_mock = table_mock

        cls.sqs_client_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: cls.sqs_client_mock if args and args[0] == 'sqs' else Mock()
        cls.sqs_client_mock = sqs_client_mock

        cls.create_internal_id_patcher = patch('common.utils.create_internal_id').start()
        cls.create_internal_id_patcher.return_value = 'internal_id'

        import update_notifications.update_notifications as _update_notifications
        global update_notifications_module
        update_notifications_module = _update_notifications

    def setUp(self):
        self.create_internal_id_patcher.reset_mock()
        self.table_mock.reset_mock()
        self.sqs_client_mock.reset_mock()
        self.log_patcher.start()
        self.env_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    @patch('common.utils.print_file_utils.dynamodb_get_item')
    @patch('common.utils.participant_utils.dynamodb_query')
    @patch('common.utils.audit_utils.datetime')
    def test_insert_stream_returns_200_status(self, audit_datetime_mock, dynamodb_query, print_file_mock):
        audit_datetime_mock.today.return_value = self.example_date
        audit_datetime_mock.now.return_value = self.example_date

        print_file_mock.return_value = EXAMPLE_PRINT_FILE
        dynamodb_query.return_value = {
            'Items': [EXAMPLE_NOTIFICATION]
        }
        insert_stream_event = read_stream('notification_insert_stream_example.json')

        expected_response = update_notifications_module.lambda_handler(insert_stream_event, self.context)
        self.assertEqual(expected_response['statusCode'], 200)

        expected_conditions = (
            Key('file_name').eq(f'CIC/{EXAMPLE_FILE_NAME}')
        )

        expected_filter_expression = Attr('sort_key').begins_with(ParticipantSortKey.NOTIFICATION) & (
            Attr('status').eq('IN_PRINT_FILE'))

        expected_dynamodb_call_list = [
            call(
                TableNames.PARTICIPANTS, dict(
                    IndexName='file-name',
                    KeyConditionExpression=expected_conditions,
                    FilterExpression=expected_filter_expression
                ),
                return_all=True
            )
        ]
        dynamodb_query.assert_has_calls(expected_dynamodb_call_list)

        print_file_call_list = [
            call(
                TableNames.PRINT_FILES, {
                    'file_name': f'CIC/{EXAMPLE_FILE_NAME}',
                    'sort_key': EXAMPLE_PRINT_FILE['sort_key']
                }
            )
        ]
        print_file_mock.assert_has_calls(print_file_call_list)

        expected_audit_call = [call.send_message(
            QueueUrl='AUDIT_QUEUE_URL', MessageBody=json.dumps({
                "action": "LETTER_SENT",
                "timestamp": "2020-09-29T14:26:01+00:00",
                "internal_id": "internal_id",
                "nhsid_useruid": "LETTERS",
                "session_id": "NONE",
                "participant_ids": ["unique-uuid"],
                "additional_information": {
                    "print_file_name": "CIC/CSAS_EU-WEST-2_RP_210823095144.dat",
                    "notification_type": "First Non-Responder",
                    "address": {}
                }
            })
        )]
        self.sqs_client_mock.assert_has_calls(expected_audit_call)

    @patch('common.utils.print_file_utils.dynamodb_get_item')
    @patch('common.utils.participant_utils.dynamodb_query')
    @patch('common.utils.audit_utils.datetime')
    def test_modify_stream_returns_200_status(self, audit_datetime_mock, dynamodb_query, print_file_mock):
        audit_datetime_mock.today.return_value = self.example_date
        audit_datetime_mock.now.return_value = self.example_date
        print_file_mock.return_value = EXAMPLE_PRINT_FILE
        dynamodb_query.return_value = {
            'Items': [EXAMPLE_NOTIFICATION]
        }
        modify_stream_event = read_stream('notification_insert_stream_example.json')
        expected_response = update_notifications_module.lambda_handler(modify_stream_event, self.context)
        self.assertEqual(expected_response['statusCode'], 200)

        expected_conditions = (
            Key('file_name').eq(f'CIC/{EXAMPLE_FILE_NAME}')
        )

        expected_filter_expression = Attr('sort_key').begins_with(ParticipantSortKey.NOTIFICATION) & (
            Attr('status').eq('IN_PRINT_FILE'))

        expected_dynamodb_call_list = [
            call(
                TableNames.PARTICIPANTS, dict(
                    IndexName='file-name',
                    KeyConditionExpression=expected_conditions,
                    FilterExpression=expected_filter_expression
                ),
                return_all=True
            )
        ]
        dynamodb_query.assert_has_calls(expected_dynamodb_call_list)

        print_file_call_list = [
            call(
                TableNames.PRINT_FILES, {
                    'file_name': f'CIC/{EXAMPLE_FILE_NAME}',
                    'sort_key': EXAMPLE_PRINT_FILE['sort_key']
                }
            )
        ]
        print_file_mock.assert_has_calls(print_file_call_list)

        expected_audit_call = [call.send_message(
            QueueUrl='AUDIT_QUEUE_URL', MessageBody=json.dumps({
                "action": "LETTER_SENT",
                "timestamp": "2020-09-29T14:26:01+00:00",
                "internal_id": "internal_id",
                "nhsid_useruid": "LETTERS",
                "session_id": "NONE",
                "participant_ids": ["unique-uuid"],
                "additional_information": {
                    "print_file_name": "CIC/CSAS_EU-WEST-2_RP_210823095144.dat",
                    "notification_type": "First Non-Responder",
                    "address": {}
                }
            })
        )]
        self.sqs_client_mock.assert_has_calls(expected_audit_call)

    @patch('common.utils.print_file_utils.dynamodb_get_item')
    @patch('common.utils.participant_utils.dynamodb_query')
    @patch('common.utils.audit_utils.datetime')
    def test_modify_failure_stream_returns_200_status(self, audit_datetime_mock, dynamodb_query, print_file_mock):
        audit_datetime_mock.today.return_value = self.example_date
        audit_datetime_mock.now.return_value = self.example_date
        print_file_mock.return_value = EXAMPLE_PRINT_FILE
        dynamodb_query.return_value = {
            'Items': [EXAMPLE_NOTIFICATION]
        }
        modify_stream_event = read_stream('notification_modify_failure_stream_example.json')
        expected_response = update_notifications_module.lambda_handler(modify_stream_event, self.context)
        self.assertEqual(expected_response['statusCode'], 200)

        expected_conditions = (
            Key('file_name').eq(f'CIC/{EXAMPLE_FILE_NAME}')
        )

        expected_filter_expression = Attr('sort_key').begins_with(ParticipantSortKey.NOTIFICATION)

        expected_dynamodb_call_list = [
            call(
                TableNames.PARTICIPANTS, dict(
                    IndexName='file-name',
                    KeyConditionExpression=expected_conditions,
                    FilterExpression=expected_filter_expression
                ),
                return_all=True
            )
        ]
        dynamodb_query.assert_has_calls(expected_dynamodb_call_list)

        print_file_call_list = [
            call(
                TableNames.PRINT_FILES, {
                    'file_name': f'CIC/{EXAMPLE_FILE_NAME}',
                    'sort_key': EXAMPLE_PRINT_FILE['sort_key']
                }
            )
        ]
        print_file_mock.assert_has_calls(print_file_call_list)

        expected_audit_call = [call.send_message(
            QueueUrl='AUDIT_QUEUE_URL', MessageBody=json.dumps({
                "action": "LETTER_NOT_SENT",
                "timestamp": "2020-09-29T14:26:01+00:00",
                "internal_id": "internal_id",
                "nhsid_useruid": "LETTERS",
                "session_id": "NONE",
                "participant_ids": ["unique-uuid"],
                "additional_information": {
                    "print_file_name": "CIC/CSAS_EU-WEST-2_RP_210823095144.dat",
                    "notification_type": "First Non-Responder",
                    "address": {}
                }
            })
        )]
        self.sqs_client_mock.assert_has_calls(expected_audit_call)
