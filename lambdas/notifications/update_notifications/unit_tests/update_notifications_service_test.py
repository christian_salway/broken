import os
import json
from unittest import TestCase
from mock import Mock, patch, call
from ..log_references import LogReference
from common.models.letters import NotificationStatus
from common.utils.audit_utils import AuditActions, AuditUsers


EXAMPLE_FILE_NAME = "CSAS_EU-WEST-2_CP_2106030622.dat"
EXAMPLE_PRINT_FILE = {
    "file_name": EXAMPLE_FILE_NAME,
    "sort_key":  "CIC",
    "type":      "invite-reminder",
    "status":    "PENDING",
    "row_count": "15000",
    "created":   "2020-01-23T13:48:08.000+00:00",
    "lock":      False
}
MOCK_ENV_VARS = {
    'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
    'DYNAMODB_PRINT_FILES': 'PRINT_FILES_TABLE'
}


def read_stream(file_name):
    with open(os.path.join(os.path.dirname(__file__), f'test_data/{file_name}')) as file:
        return json.loads(file.read())


@patch('os.environ', MOCK_ENV_VARS)
class TestUpdateNotificationsService(TestCase):

    @patch('boto3.client')
    @patch('boto3.resource')
    def setUp(self, boto3_resource, boto3_client):
        self.context = Mock()
        self.context.function_name = ''

        # Setup database mock
        table_mock = Mock()
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = table_mock
        boto3_resource.return_value = boto3_table_mock
        self.table_mock = table_mock

        import update_notifications.update_notifications_service as _update_notifications_service
        self.update_notifications_service_module = _update_notifications_service

    @patch('common.utils.print_file_utils.dynamodb_get_item')
    @patch('common.utils.participant_utils.dynamodb_query')
    @patch('common.utils.print_file_utils.set_print_file_lock')
    @patch('update_notifications.update_notifications_service.process_records_callback')
    @patch('update_notifications.update_notifications_service.log')
    def test_printfile_locked_before_we_update_notifications(self,
                                                             log_mock,
                                                             process_records_callback_mock,
                                                             set_print_file_lock_mock,
                                                             dynamodb_query,
                                                             print_file_mock):
        mock_return_query = [
            {
                'file_name': EXAMPLE_FILE_NAME,
                'participant_id': '1',
                'sort_key': 'CIC'
            },
            {
                'file_name': EXAMPLE_FILE_NAME,
                'participant_id': '2',
                'sort_key': 'CIC'
            }
        ]
        print_file_mock.return_value = EXAMPLE_PRINT_FILE
        dynamodb_query.return_value = {
            'Items': mock_return_query
        }
        notification_lookup = {
            'query_status': NotificationStatus.PENDING_PRINT_FILE,
            'new_status': NotificationStatus.IN_PRINT_FILE,
        }
        self.update_notifications_service_module.update_all_notifications_in_print_file(
            EXAMPLE_PRINT_FILE, 'CIC', notification_lookup
        )

        process_records_callback_mock.assert_has_calls([
            call([
                mock_return_query[0],
                mock_return_query[1]
            ], new_status='IN_PRINT_FILE')]
        )
        set_print_file_lock_mock.assert_has_calls([call(
            EXAMPLE_PRINT_FILE, 'CIC', True)
        ])
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.UPDATENOT0011})
        ])

    @patch('update_notifications.update_notifications_service.audit')
    @patch('update_notifications.update_notifications_service.log')
    @patch('update_notifications.update_notifications_service.update_notification_status')
    def test_process_records_callback_updates_notifications_correctly(self,
                                                                      update_notification_status_mock,
                                                                      log_mock,
                                                                      audit_mock):
        notification_records = [
            {
                'file_name': EXAMPLE_FILE_NAME,
                'participant_id': '1',
                'sort_key': 'CIC',
                'type': 'PA1',
                'data': {}
            },
            {
                'file_name': EXAMPLE_FILE_NAME,
                'participant_id': '2',
                'sort_key': 'CIC',
                'type': 'PA1',
                'data': {}
            }
        ]
        new_status = 'IN_PRINT_FILE'
        self.update_notifications_service_module.process_records_callback(notification_records, new_status)

        update_notification_status_mock.assert_has_calls([
            call(notification_records[0], new_status),
            call(notification_records[1], new_status)
        ])
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.UPDATENOT0003})
        ])
        audit_mock.assert_has_calls(
            [
                call(
                    action=AuditActions.NOTIFICATION_IN_PRINT_FILE,
                    user=AuditUsers.LETTERS,
                    participant_ids=['1'],
                    additional_information={
                        'print_file_name': EXAMPLE_FILE_NAME,
                        'address': {},
                        'notification_type': 'PA1'
                    }
                ),
                call(
                    action=AuditActions.NOTIFICATION_IN_PRINT_FILE,
                    user=AuditUsers.LETTERS,
                    participant_ids=['2'],
                    additional_information={
                        'print_file_name': EXAMPLE_FILE_NAME,
                        'address': {},
                        'notification_type': 'PA1'
                    }
                )
            ]
        )

    def test_parse_line_address(self):
        address = {
            'DEST_ADD1': 'line_1',
            'DEST_ADD2': 'line_2',
            'DEST_ADD3': 'line_3',
            'DEST_POSTCODE': 'postcode'
        }
        actual_address = self.update_notifications_service_module.parse_address(address)
        expected_address = {
            'address_line_1': 'line_1',
            'address_line_2': 'line_2',
            'address_line_3': 'line_3',
            'DEST_POSTCODE': 'postcode'
        }

        self.assertEqual(actual_address, expected_address)
