import os
import json
from unittest import TestCase
from mock import Mock, patch, call
from ..log_references import LogReference
from ddt import ddt, data, unpack


EXAMPLE_FILE_NAME = "CSAS_EU-WEST-2_CP_2106030622.dat"
EXAMPLE_PRINT_FILE = {
    "file_name": EXAMPLE_FILE_NAME,
    "sort_key":  "CIC",
    "type":      "invite-reminder",
    "status":    "PENDING",
    "row_count": "15000",
    "created":   "2020-01-23T13:48:08.000+00:00",
    "lock":      False
}
MOCK_ENV_VARS = {
    'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
    'DYNAMODB_PRINT_FILES': 'PRINT_FILES_TABLE'
}


def read_stream(file_name):
    with open(os.path.join(os.path.dirname(__file__), f'test_data/{file_name}')) as file:
        return json.loads(file.read())


@ddt
@patch('os.environ', MOCK_ENV_VARS)
class TestUpdateNotifications(TestCase):

    @patch('boto3.client')
    @patch('boto3.resource')
    def setUp(self, boto3_resource, boto3_client):
        self.context = Mock()
        self.context.function_name = ''

        # Setup database mock
        table_mock = Mock()
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = table_mock
        boto3_resource.return_value = boto3_table_mock
        self.table_mock = table_mock

        import update_notifications.update_notifications as _update_notifications
        self.update_notifications_module = _update_notifications

    @patch('update_notifications.update_notifications.log')
    @patch('common.utils.print_file_utils.dynamodb_get_item')
    @patch('common.utils.participant_utils.dynamodb_query')
    def test_notifications_updated_modify_stream_returns_200_status_code(self,
                                                                         dynamodb_query,
                                                                         print_file_mock,
                                                                         log_mock):
        print_file_mock.return_value = EXAMPLE_PRINT_FILE
        dynamodb_query.return_value = {
            'Items': []
        }

        modify_stream_event = read_stream('notification_modify_stream_example.json')
        expected_response = self.update_notifications_module.lambda_handler(modify_stream_event, self.context)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.UPDATENOT0001, 'record_count': 1}),
            call({'log_reference': LogReference.UPDATENOT0003}),
            call({'log_reference': LogReference.UPDATENOT0009})
        ])
        self.assertEqual(expected_response['statusCode'], 200)

    @patch('update_notifications.update_notifications.log')
    @patch('common.utils.print_file_utils.dynamodb_get_item')
    @patch('common.utils.participant_utils.dynamodb_query')
    def test_notifications_updated_insert_stream_returns_200_status_code(self,
                                                                         dynamodb_query,
                                                                         print_file_mock,
                                                                         log_mock):
        print_file_mock.return_value = EXAMPLE_PRINT_FILE
        dynamodb_query.return_value = {
            'Items': []
        }

        modify_stream_event = read_stream('notification_insert_stream_example.json')
        expected_response = self.update_notifications_module.lambda_handler(modify_stream_event, self.context)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.UPDATENOT0001, 'record_count': 1}),
            call({'log_reference': LogReference.UPDATENOT0002})
        ])
        self.assertEqual(expected_response['statusCode'], 200)

    def test_get_image_delta_returns_list_of_delta_attributes(self):
        modify_stream_event = read_stream('notification_modify_stream_example.json')
        dynamodb_record = modify_stream_event['Records'][0]['dynamodb']
        test_old_image = dynamodb_record['OldImage']
        test_new_image = dynamodb_record['NewImage']

        actual_response = self.update_notifications_module.get_image_delta(test_new_image, test_old_image)
        expected_response = ['status']

        self.assertEqual(actual_response, expected_response)

    @patch('update_notifications.update_notifications.log')
    @patch('common.utils.print_file_utils.dynamodb_get_item')
    @patch('common.utils.participant_utils.dynamodb_query')
    def test_notifications_exists_when_print_file_missing(self,
                                                          dynamodb_query,
                                                          print_file_mock,
                                                          log_mock):
        print_file_mock.return_value = []
        dynamodb_query.return_value = {
            'Items': []
        }

        modify_stream_event = read_stream('notification_insert_stream_example.json')
        expected_response = self.update_notifications_module.lambda_handler(modify_stream_event, self.context)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.UPDATENOT0001, 'record_count': 1}),
            call({'log_reference': LogReference.UPDATENOT0002}),
            call({'log_reference': LogReference.UPDATENOT0012, 'print_file': 'CIC/CSAS_EU-WEST-2_RP_210823095144.dat'})
        ])
        self.assertEqual(expected_response['statusCode'], 200)

    def test_get_image_delta_returns_list_of_no_attributes(self):
        modify_stream_event = read_stream('notification_modify_stream_no_change.json')
        dynamodb_record = modify_stream_event['Records'][0]['dynamodb']
        test_old_image = dynamodb_record['OldImage']
        test_new_image = dynamodb_record['NewImage']

        actual_response = self.update_notifications_module.get_image_delta(test_new_image, test_old_image)
        expected_response = []

        self.assertEqual(actual_response, expected_response)

    def test_is_valid_image_comparison_returns_true_when_keys_match(self):
        modify_stream_event = read_stream('notification_modify_stream_example.json')
        dynamodb_record = modify_stream_event['Records'][0]['dynamodb']
        test_old_image = dynamodb_record['OldImage']
        test_new_image = dynamodb_record['NewImage']

        actual_response = self.update_notifications_module.is_valid_image_comparison(test_new_image, test_old_image)
        expected_response = True

        self.assertEqual(actual_response, expected_response)

    def test_is_valid_image_comparison_returns_false_when_keys_do_not_match(self):
        modify_stream_event = read_stream('notification_modify_stream_different_keys.json')
        dynamodb_record = modify_stream_event['Records'][0]['dynamodb']
        test_old_image = dynamodb_record['OldImage']
        test_new_image = dynamodb_record['NewImage']

        actual_response = self.update_notifications_module.is_valid_image_comparison(test_new_image, test_old_image)
        expected_response = False

        self.assertEqual(actual_response, expected_response)

    @unpack
    @data(
        (['status'], True),
        (['lock', 'row_count', 'status'], True),
        (['lock', 'row_count'], False),
        (['lock'], False),
        ([], False),
    )
    def test_is_watched_data_returns_expected_response(self, test_image_delta, expected_response):
        actual_response = self.update_notifications_module.is_watched_delta(test_image_delta)
        self.assertEqual(actual_response, expected_response)
