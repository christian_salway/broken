import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    UPDATENOT0001 = (logging.INFO, 'Processing all record updates in print file stream')
    UPDATENOT0002 = (logging.INFO, 'Processing INSERT event type')
    UPDATENOT0003 = (logging.INFO, 'Processing MODIFY event type')
    UPDATENOT0004 = (logging.INFO, 'Getting notification records')
    UPDATENOT0005 = (logging.INFO, 'Locking print file row for processing')
    UPDATENOT0006 = (logging.INFO, 'Lock released on print file row')
    UPDATENOT0007 = (logging.INFO, 'Exiting as modified attribute is not on watch list')
    UPDATENOT0008 = (logging.INFO, 'Exiting as new and old image keys do not match')
    UPDATENOT0009 = (logging.INFO, 'Continuing as modified attribute is on watch list')
    UPDATENOT0010 = (logging.INFO, 'Print file status has reported a failure. Querying for all notifications')
    UPDATENOT0011 = (logging.INFO, 'Querying for all notifications via a specific status')
    UPDATENOT0012 = (logging.WARNING, 'Print file is missing. Skipping processing')
    UPDATENOTEX0001 = (logging.ERROR, 'Unknown print file status')
    UPDATENOTEX0002 = (logging.ERROR, 'Failed to update notification status')
