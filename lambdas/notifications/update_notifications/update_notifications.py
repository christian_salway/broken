from common.models.letters import NotificationStatus, PrintFileStatus
from common.log import log
from update_notifications.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from update_notifications.update_notifications_service import (
    update_all_notifications_in_print_file, release_printfile_lock
)
from common.exceptions import PrintFileMissingException


NOTIFICATION_STATUS_LOOKUP = {
    PrintFileStatus.PENDING: {
        'query_status': NotificationStatus.PENDING_PRINT_FILE,
        'new_status': NotificationStatus.IN_PRINT_FILE,
    },
    PrintFileStatus.WITH_PRINTER: {
        'query_status': NotificationStatus.IN_PRINT_FILE,
        'new_status': NotificationStatus.WITH_PRINTER,
    },
    PrintFileStatus.TRANSPORT_FAILURE: {
        'query_status': 'ANY',
        'new_status': NotificationStatus.FAILED
    }
}


WATCHED_ATTRIBUTES = [
    'status'
]


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.UPDATENOT0001, 'record_count': len(event['Records'])})

    record_handler_types = {
        'INSERT': handle_new_image,
        'MODIFY': handle_existing_image
    }

    for record in event['Records']:
        event_name = record['eventName']

        record_handler = record_handler_types.get(event_name)
        if record_handler:
            dynamodb_record = record['dynamodb']
            try:
                record_handler(dynamodb_record)
            except PrintFileMissingException:
                log({
                    'log_reference': LogReference.UPDATENOT0012,
                    'print_file': dynamodb_record['Keys']['file_name']['S']
                    })

    return {
        'statusCode': 200,
        'event': event
    }


def handle_new_image(dynamodb_record):
    log({'log_reference': LogReference.UPDATENOT0002})
    dynamodb_record = dynamodb_record['NewImage']
    process_image_record(dynamodb_record)


def handle_existing_image(dynamodb_record):
    log({'log_reference': LogReference.UPDATENOT0003})
    previous_dynamodb_record = dynamodb_record['OldImage']
    current_dynamodb_record = dynamodb_record['NewImage']

    is_valid_comparison = is_valid_image_comparison(previous_dynamodb_record, current_dynamodb_record)
    if not is_valid_comparison:
        log({'log_reference': LogReference.UPDATENOT0007})
        return

    image_delta = get_image_delta(previous_dynamodb_record, current_dynamodb_record)
    if not is_watched_delta(image_delta):
        log({'log_reference': LogReference.UPDATENOT0008, 'modified_attributes': image_delta})
        return

    log({'log_reference': LogReference.UPDATENOT0009})
    process_image_record(current_dynamodb_record)


def is_valid_image_comparison(new_image, old_image):
    old_image_keys = old_image.keys()
    new_image_keys = new_image.keys()
    return old_image_keys == new_image_keys


def get_image_delta(new_image, old_image):
    changed_attributes = []
    for attribute in new_image.keys():
        new_attr = list(new_image[attribute].values())
        old_attr = list(old_image[attribute].values())
        if new_attr != old_attr:
            changed_attributes.append(attribute)

    return changed_attributes


def process_image_record(dynamodb_record):
    file_name = dynamodb_record['file_name']['S']
    sort_key = dynamodb_record['sort_key']['S']
    print_file_status = dynamodb_record['status']['S']
    notification_lookup = NOTIFICATION_STATUS_LOOKUP.get(print_file_status)
    if not notification_lookup:
        log({'log_reference': LogReference.UPDATENOTEX0001, 'status': print_file_status})
    else:
        update_all_notifications_in_print_file(file_name, sort_key, notification_lookup)
        release_printfile_lock(file_name, sort_key)


def is_watched_delta(image_delta):
    for delta in image_delta:
        if delta in WATCHED_ATTRIBUTES:
            return True
    return False
