from unittest import TestCase
from mock import patch


class TestSendPrintFile(TestCase):

    module = 'send_print_file.send_print_file'

    @patch('boto3.resource')
    def setUp(self, *args):
        import send_print_file.send_print_file as _send_print_file
        self.send_print_file_module = _send_print_file
        self.unwrapped_handler = self.send_print_file_module.lambda_handler.__wrapped__

    def test_generate_file_list(self):
        test_event = {
            'Records': [
                {
                    'body': '{"print_file_name": "test_file.dat"}'
                }
            ]
        }

        expected_file_list = 'test_file.dat'
        actual_file_list = self.send_print_file_module.generate_file_list(test_event)

        self.assertEqual(actual_file_list, expected_file_list)
