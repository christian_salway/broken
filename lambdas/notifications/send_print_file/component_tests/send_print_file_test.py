from unittest import TestCase
import json
from datetime import datetime, timezone
from mock import patch, Mock, MagicMock
from common.test_mocks.mock_events import sqs_mock_event
from common.models.letters import PrintFileStatus, PrintDestination


@patch('os.environ', {'DYNAMODB_PRINT_FILES': 'print-files-table', 'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL'})
class TestSendPrintFile(TestCase):

    FIXED_NOW_TIME = datetime(2020, 1, 1, tzinfo=timezone.utc)
    mock_event = sqs_mock_event({
        'print_file_name': 'test_file.dat'
    })

    mock_env_vars = {
        'SFTP_SECRET_KEY': 'SFTP_SECRET',
        'SFTP_IN_DIR': 'SFTP_DIR',
        'PRINT_FILE_BUCKET': 'PRINT_FILE_BUCKET',
        'DYNAMODB_PRINT_FILES': 'DYNAMODB_PRINT_FILES'
    }

    @classmethod
    @patch('boto3.client')
    @patch('boto3.resource')
    @patch('boto3.session.Session')
    @patch('os.environ', mock_env_vars)
    def setUpClass(cls, boto3_session, boto3_resource, boto3_client, *args):
        cls.log_patcher = patch('common.log.logger')

        # Setup Secrets Manager Mock
        secret_client_mock = Mock()
        secret_session_mock = Mock()
        secret_session_mock.client.return_value = secret_client_mock
        boto3_session.return_value = secret_session_mock
        cls.secret_client_mock = secret_client_mock

        # Setup database mocks
        boto3_table_mock = Mock()
        organisation_supplemental_table_mock = Mock()
        boto3_table_mock.Table.return_value = organisation_supplemental_table_mock
        boto3_resource.return_value = boto3_table_mock
        cls.organisation_supplemental_table_mock = organisation_supplemental_table_mock

        # Setup Client Mocks
        cls.s3_client_mock = Mock()
        cls.sqs_client_mock = Mock()

        def client_mocks(client):
            if client == 'sqs':
                return cls.sqs_client_mock
            elif client == 's3':
                return cls.s3_client_mock

        boto3_client.side_effect = lambda *args, **kwargs: client_mocks(args[0])

        # Setup lambda module
        import send_print_file.send_print_file as _send_print_file
        global send_print_file_module
        send_print_file_module = _send_print_file

        import common.utils.audit_utils as _audit
        global audit_module
        audit_module = _audit
        cls.audit_date_time_patcher = patch.object(audit_module, 'datetime', Mock(wraps=datetime))

    def setUp(self):
        self.organisation_supplemental_table_mock.reset_mock()
        self.s3_client_mock.reset_mock()
        self.sqs_client_mock.reset_mock()
        self.log_patcher.start()
        self.audit_date_time_patcher.start()
        self.maxDiff = None

    def tearDown(self):
        self.log_patcher.stop()
        self.audit_date_time_patcher.stop()

    @patch('send_print_file.send_print_file.create_sftp_client')
    @patch('send_print_file.send_print_file.update_print_file_record')
    def test_successful_send(self, update_print_file_record_mock, create_sftp_mock):
        context = Mock()
        context.function_name = ''
        sftp_client_mock = MagicMock()
        create_sftp_mock.return_value = sftp_client_mock
        self.secret_client_mock.get_secret_value.return_value = {
            'ARN': 'string',
            'Name': 'string',
            'VersionId': 'string',
            'SecretString': '{"private-key": "test_private_key", "host": "test_host", "user_id": "test_user_id"}',
            'VersionStages': [
                'string',
            ],
            'CreatedDate': datetime(2015, 1, 1, tzinfo=timezone.utc)
        }
        send_print_file_module.lambda_handler(self.mock_event, context)
        sftp_client_mock.open.assert_called_with('test_file.dat', 'wb', 32768)
        self.s3_client_mock.download_fileobj.assert_called_once_with(
            'PRINT_FILE_BUCKET', 'test_file.dat', sftp_client_mock.open().__enter__()
        )

        update_print_file_record_mock.assert_called_once_with(
            'test_file.dat', PrintDestination.CIC, PrintFileStatus.WITH_PRINTER
        )

        # Asserts the audit record has been sent to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'SEND_PRINT_FILE_SUCCESS')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'SYSTEM')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(actual_audit_record_sent.get('file_name'), 'test_file.dat')
        self.assertEqual(actual_audit_record_sent.get('host_name'), PrintDestination.CIC)
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @patch('send_print_file.send_print_file.create_sftp_client')
    def test_successful_send_on_retry(self, create_sftp_mock):
        context = Mock()
        context.function_name = ''
        sftp_client_mock = MagicMock()
        create_sftp_mock.side_effect = [Exception('Failure making SFTP connection'), sftp_client_mock]
        self.secret_client_mock.get_secret_value.return_value = {
            'ARN': 'string',
            'Name': 'string',
            'VersionId': 'string',
            'SecretString': '{"private-key": "test_private_key", "host": "test_host", "user_id": "test_user_id"}',
            'VersionStages': [
                'string',
            ],
            'CreatedDate': datetime(2015, 1, 1, tzinfo=timezone.utc)
        }
        send_print_file_module.lambda_handler(self.mock_event, context)
        sftp_client_mock.open.assert_called_with('test_file.dat', 'wb', 32768)
        self.s3_client_mock.download_fileobj.assert_called_once_with(
            'PRINT_FILE_BUCKET', 'test_file.dat', sftp_client_mock.open().__enter__()
        )
        self.assertEqual(create_sftp_mock.call_count, 2)
        # Asserts the audit record has been sent to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'SEND_PRINT_FILE_SUCCESS')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'SYSTEM')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(actual_audit_record_sent.get('file_name'), 'test_file.dat')
        self.assertEqual(actual_audit_record_sent.get('host_name'), 'CIC')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @patch('send_print_file.send_print_file.create_sftp_client')
    @patch('send_print_file.send_print_file.update_print_file_record')
    def test_failed_send(self, update_print_file_record_mock, create_sftp_mock):
        context = Mock()
        context.function_name = ''
        create_sftp_mock.side_effect = Exception('Failure making SFTP connection')
        self.secret_client_mock.get_secret_value.return_value = {
            'ARN': 'string',
            'Name': 'string',
            'VersionId': 'string',
            'SecretString': '{"private-key": "test_private_key", "host": "test_host", "user_id": "test_user_id"}',
            'VersionStages': [
                'string',
            ],
            'CreatedDate': datetime(2015, 1, 1, tzinfo=timezone.utc)
        }
        with self.assertRaises(Exception):
            send_print_file_module.lambda_handler(self.mock_event, context)
        self.assertEqual(create_sftp_mock.call_count, 3)

        update_print_file_record_mock.assert_called_once_with(
            'test_file.dat', PrintDestination.CIC, PrintFileStatus.TRANSPORT_FAILURE
        )

        # Asserts the audit record has been sent to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'SEND_PRINT_FILE_FAILURE')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'SYSTEM')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(actual_audit_record_sent.get('file_name'), 'test_file.dat')
        self.assertEqual(actual_audit_record_sent.get('host_name'), PrintDestination.CIC)
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')
