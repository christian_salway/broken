# Send Print File

## Function

This lambda will be invoked by the `print-files-to-send` queue. It will read the `print_file_name` attribute of the body of this event and stream the contents of the file with that name (found in the `print-files-to-send` bucket) directly to an sftp server. This sftp server connection details can be found in the `sftp_secrets` secret for each type of environment

## SFTP Communication

The secret will require a `public-key`, `private-key`, `host` and `user_id` to be used correctly, these details will be used to create the connection. See the sftp_stub in terraform for more details about our AWS Transfer stub to mock out the final capita destination