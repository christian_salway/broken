import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    SENDPRINT0001 = (logging.INFO, 'Generating file list')
    SENDPRINT0002 = (logging.INFO, 'Getting SFTP secret')
    SENDPRINT0003 = (logging.INFO, 'Creating SFTP client')
    SENDPRINT0004 = (logging.INFO, 'Streaming file to SFTP server')
    SENDPRINT0005 = (logging.INFO, 'Successfully sent file SFTP server')

    SENDPRINTWARN0001 = (logging.WARNING, 'An exception was encountered, retrying as below retry threshold')

    SENDPRINTEX0001 = (logging.ERROR, 'Invalid event received from SQS')
    SENDPRINTEX0004 = (logging.ERROR, 'Exception encountered during file streaming')
    SENDPRINTEX0005 = (logging.ERROR, 'An exception was encountered and the maximum retry amount has been met, moving record to failed bucket')  
    SENDPRINTEX0006 = (logging.ERROR, 'Problem setting status on print file')
