import json
import os
from boto3 import client
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.utils.s3_utils import move_object_to_bucket
from common.log import log
from send_print_file.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.print_file_utils import update_print_file_by_file_name
from common.models.letters import (PrintFileStatus, PrintDestination)
from common.utils.sftp_utils import exception_handler, get_sftp_secrets, create_sftp_client


SFTP_SECRET_KEY = os.environ.get('SFTP_SECRET_KEY')
SFTP_DIR = os.environ.get('SFTP_IN_DIR')
PRINT_FILE_BUCKET = os.environ.get('PRINT_FILE_BUCKET')
PRINT_FILE_COMPLETED_BUCKET = os.environ.get('PRINT_FILE_COMPLETED_BUCKET')
PRINT_FILE_FAILED_BUCKET = os.environ.get('PRINT_FILE_FAILED_BUCKET')
WRITE_MODE = 'wb'
BUFFER_SIZE = 32768  # Gives a 32kb buffer for file throughput
TRIES = 3
# Rather than accessing this global directly use the get_s3_client() function which aids in testability
_S3_CLIENT = None


def get_s3_client():
    global _S3_CLIENT
    if not _S3_CLIENT:
        _S3_CLIENT = client('s3')
    return _S3_CLIENT


@lambda_entry_point
def lambda_handler(event, context):
    for attempt in range(TRIES):
        try:
            log({'log_reference': LogReference.SENDPRINT0001})
            file_name = generate_file_list(event)

            log({'log_reference': LogReference.SENDPRINT0002})
            private_key, host, user_id = get_sftp_secrets(SFTP_SECRET_KEY)

            log({'log_reference': LogReference.SENDPRINT0003})
            sftp_client = create_sftp_client(private_key, host, user_id)
            sftp_client.chdir(SFTP_DIR)

            # Open a connection to the SFTP server and write to the file with the same name as the s3 object
            with sftp_client.open(file_name, WRITE_MODE, BUFFER_SIZE) as sftp_file:
                try:
                    log({'log_reference': LogReference.SENDPRINT0004, 'file_name': file_name})
                    #  Download the object and write it to the sftp file-like object 'sftp_file'
                    get_s3_client().download_fileobj(PRINT_FILE_BUCKET, file_name, sftp_file)
                    #  Move the s3 object to the completed bucket
                    move_object_to_bucket(PRINT_FILE_BUCKET, PRINT_FILE_COMPLETED_BUCKET, file_name)
                    update_print_file_record(file_name, PrintDestination.CIC, PrintFileStatus.WITH_PRINTER)
                    audit(
                        AuditActions.SEND_PRINT_FILE_SUCCESS,
                        user=AuditUsers.SYSTEM,
                        file_name=file_name,
                        host_name=PrintDestination.CIC
                    )
                except Exception as e:
                    log({'log_reference': LogReference.SENDPRINTEX0004, 'add_exception_info': True})
                    raise e
        except Exception as e:
            if attempt < (TRIES-1):
                log({'log_reference': LogReference.SENDPRINTWARN0001, 'add_exception_info': True,
                     'attempt_number': attempt})
                continue
            else:
                log({'log_reference': LogReference.SENDPRINTEX0005, 'add_exception_info': True, 'file_name': file_name})
                move_object_to_bucket(PRINT_FILE_BUCKET, PRINT_FILE_FAILED_BUCKET, file_name)
                update_print_file_record(file_name, PrintDestination.CIC, PrintFileStatus.TRANSPORT_FAILURE)
                audit(
                    AuditActions.SEND_PRINT_FILE_FAILURE, user=AuditUsers.SYSTEM, file_name=file_name,
                    host_name=PrintDestination.CIC
                )
                raise e
        break


def update_print_file_record(file_name, sort_key, status):
    try:
        update_print_file_by_file_name(file_name, sort_key, status)
    except Exception as e:
        log({
            'log_reference': LogReference.SENDPRINTEX0006,
            'file_name': file_name,
            'status': status,
            'add_exception_info': True
        })
        raise e


@exception_handler(LogReference.SENDPRINTEX0001)
def generate_file_list(event):
    return [json.loads(record['body'])['print_file_name'] for record in event['Records']][0]
