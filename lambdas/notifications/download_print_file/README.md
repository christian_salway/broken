# Download Print File Lambda

This lambda will provide a print file from the 'print_files_to_send' bucket in the form of a base 64 encoded octet stream. No further action needs to be carried out on the data once it has reached the browser, as browsers are equipped to deal with this binary data.

## Input

In order to access this lambda you will need to have a valid session as a CSAS user. With this you can then access the endpoint 'api/download-print-file', which will need a query parameter of 'file_name' which will be the string format of whatever file name is required

## Output

In a successful request you will receive a 200 response with a base64 encoded data stream containing the file

Should some required data be missing, or the session you are using be invalid, a 400 response and a corresponding error will be created

Should an unexpected error occur, a 500 response will be returned with a generic exception message. This is to mask any error specifics to the user. The error itself will be logged in the backend should you wish to view it

## Dependencies

This lambda requires access to the 'print_files_to_send' S3 bucket, the audit queue, and the sessions table. There are no external library dependencies needed to run this lambda