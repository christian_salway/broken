from unittest import TestCase
from mock import patch, Mock, call
from common.log_references import CommonLogReference
from common.utils.audit_utils import AuditActions
from common.utils.file_download_utils import ProcessingException

module = 'download_print_file.download_print_file'

test_selected_role = {
    "organisation_code": "A60",
    "organisation_name": "YORKSHIRE AND THE HUMBER",
    "role_id": "2001",
    "role_name": "CSAS Team Member",
    "workgroups": [
        "cervicalscreening", "cervicalscreening_DMS"
    ],
    'access_groups': {
        'csas': True,
        'csas_dms': True,
        'csas_iom': False
    }
}


class TestDownloadPrintFile(TestCase):

    @classmethod
    @patch('boto3.resource', Mock())
    def setUp(self):
        import download_print_file.download_print_file as _download_print_file_module
        from common.test_assertions.api_assertions import assertApiResponse as _assert_api_response

        self.download_print_file_module = _download_print_file_module
        self.assert_api_response = _assert_api_response

    @patch(f'{module}.log')
    def test_check_user_access_allowed_bad_session_dms(self, log_mock):
        with self.assertRaises(ProcessingException) as context:
            self.download_print_file_module.check_user_access_allowed(
                'DMS/test_file_name', {'csas_iom': True}
            )
        self.assertTrue(CommonLogReference.DOWNLOADFILEEX0005.message in context.exception.message)
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILEEX0005})
        ])

    @patch(f'{module}.log')
    def test_check_user_access_allowed_bad_session_iom(self, log_mock):
        with self.assertRaises(ProcessingException) as context:
            self.download_print_file_module.check_user_access_allowed(
                'IOM/test_file_name', test_selected_role['access_groups']
            )
        self.assertTrue(CommonLogReference.DOWNLOADFILEEX0005.message in context.exception.message)
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILEEX0005})
        ])

    @patch(f'{module}.get_session_from_lambda_event', Mock())
    @patch(f'{module}.verify_and_parse_session', Mock(side_effect=ProcessingException(999, 'test')))
    @patch(f'{module}.json_return_message')
    def test_lambda_handler_process_exception(self, json_return_mock):
        context_mock = Mock()
        context_mock.function_name.return_value = 'test'

        json_return_mock.return_value = 'error response'

        return_message = self.download_print_file_module.lambda_handler.__wrapped__.__wrapped__(
            {}, context_mock
        )

        #  We don't assert for logs here as they are raised in child function, see component tests
        self.assertEqual(return_message, 'error response')
        json_return_mock.assert_called_with(999, 'test')

    @patch(f'{module}.get_session_from_lambda_event', Mock())
    @patch(f'{module}.verify_and_parse_session', Mock(side_effect=Exception()))
    @patch(f'{module}.json_return_message')
    @patch(f'{module}.log')
    def test_lambda_handler_general_exception(self, log_mock, json_return_mock):
        context_mock = Mock()
        context_mock.function_name.return_value = 'test'

        json_return_mock.return_value = 'error response'

        return_message = self.download_print_file_module.lambda_handler.__wrapped__.__wrapped__(
            {}, context_mock
        )

        self.assertEqual(return_message, 'error response')
        json_return_mock.assert_called_with(500, 'Cannot complete request')
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILEEX0004, 'error': ''})
        ])

    @patch(f'{module}.get_session_from_lambda_event', Mock(return_value='test_session'))
    @patch(f'{module}.update_metadata', Mock())
    @patch(f'{module}.get_file_body', Mock(return_value='test_print_file_body'))
    @patch(f'{module}.get_file_name', Mock(return_value='DMS/test_file_name'))
    @patch(f'{module}.audit')
    @patch(f'{module}.verify_and_parse_session')
    @patch(f'{module}.update_print_file_status')
    def test_lambda_handler_success(self, update_mock, verify_and_parse_mock, audit_mock):
        context_mock = Mock()
        context_mock.function_name.return_value = 'test'
        verify_and_parse_mock.return_value = 'test_data', {'csas_dms': True}
        expected_print_file_body = {
            'statusCode': 200,
            'headers': {'Content-Type': 'text/plain',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800',
                        'Content-Disposition': 'attachment; filename="DMS/test_file_name"'},
            'body': 'test_print_file_body',
            'isBase64Encoded': False
        }

        actual_return_message = self.download_print_file_module.lambda_handler.__wrapped__.__wrapped__(
            {}, context_mock
        )

        update_mock.assert_called_with('DMS/test_file_name', 'WITH_PRINTER')

        self.assertEqual(expected_print_file_body, actual_return_message)
        audit_mock.assert_called_with(
            session='test_session',
            action=AuditActions.DMS_PRINT_FILE_DOWNLOADED,
            additional_information={'file_name': 'DMS/test_file_name'}
        )
