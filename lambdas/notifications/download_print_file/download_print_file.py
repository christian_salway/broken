import os

from common.log import log
from common.log_references import CommonLogReference
from common.utils import json_return_message
from common.utils.audit_utils import audit, AuditActions
from common.utils.file_download_utils import verify_and_parse_session, get_file_name, get_file_body, update_metadata, \
    ProcessingException
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils.session_utils import get_session_from_lambda_event
from common.utils.print_file_utils import update_print_file_by_file_name
from common.models.letters import PrintDestination, PrintFileStatus
from download_print_file.log_references import LogReference

PRINT_FILE_BUCKET_NAME = os.environ.get('PRINT_FILE_BUCKET_NAME')


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    try:
        session = get_session_from_lambda_event(event)
        session_user_data, session_selected_access_groups = verify_and_parse_session(session)
        file_name = get_file_name(event)
        check_user_access_allowed(file_name, session_selected_access_groups)
        print_file_body = get_file_body(PRINT_FILE_BUCKET_NAME, file_name)
        update_metadata(session_user_data, file_name, PRINT_FILE_BUCKET_NAME)
        update_print_file_status(file_name, PrintFileStatus.WITH_PRINTER)
    except ProcessingException as e:
        return json_return_message(e.response_code, e.message)
    except Exception as e:
        log({'log_reference': CommonLogReference.DOWNLOADFILEEX0004, 'error': str(e)})
        return json_return_message(500, 'Cannot complete request')

    audit_action = AuditActions.DMS_PRINT_FILE_DOWNLOADED if file_name[:3].lower() == 'dms' else AuditActions.IOM_PRINT_FILE_DOWNLOADED  
    audit(
        session=session,
        action=audit_action,
        additional_information={'file_name': file_name}
    )

    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'text/plain',
                    'X-Content-Type-Options': 'nosniff',
                    'Strict-Transport-Security': 'max-age=1576800',
                    'Content-Disposition': f'attachment; filename="{file_name}"'},
        'body': print_file_body,
        'isBase64Encoded': False
    }


def check_user_access_allowed(print_file_name, session_selected_access_groups):
    if not session_selected_access_groups.get(f'csas_{print_file_name[:3].lower()}'):
        log({'log_reference': CommonLogReference.DOWNLOADFILEEX0005})
        raise ProcessingException(400, CommonLogReference.DOWNLOADFILEEX0005.message)


def update_print_file_status(file_name, new_status):
    sort_key = derive_print_destination_from_file_name(file_name)
    log({'log_reference': LogReference.DOWNLOADPRINT0001,
         'file_name': file_name,
         'sort_key': sort_key,
         'new_status': new_status})
    update_print_file_by_file_name(file_name,
                                   sort_key,
                                   new_status)


def derive_print_destination_from_file_name(file_name):
    return PrintDestination.DMS if file_name[:3].lower() == 'dms' else PrintDestination.IOM
