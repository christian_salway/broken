import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    DOWNLOADPRINT0001 = (logging.INFO, 'Updating file status')
