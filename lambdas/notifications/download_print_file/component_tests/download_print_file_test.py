import json
from unittest import TestCase
from mock import patch, Mock, call
from datetime import datetime
from time import time
from common.utils.dynamodb_access.table_names import TableNames
from common.models.letters import PrintDestination, PrintFileStatus

from common.log_references import CommonLogReference

mock_env_vars = {
    'PRINT_FILE_TYPE': 'DMS',
    'PRINT_FILE_BUCKET_NAME': 'PRINT_FILES_SUCCEEDED'
}


example_date = datetime(2020, 10, 18, 13, 48, 8)
datetime_mock = Mock(wraps=datetime)
datetime_mock.utcnow.return_value = example_date
datetime_mock.now.return_value = example_date
datetime_mock.today.return_value = example_date

with patch('os.environ', mock_env_vars):
    from download_print_file.download_print_file import lambda_handler

module = 'download_print_file'
module_utils = 'common.utils'

session_table_mock = Mock()
sqs_client_mock = Mock()
s3_mock = Mock()
log_mock = Mock()
print_files_table_mock = Mock()


@patch(f'{module_utils}.print_file_utils.dynamodb_update_item', print_files_table_mock)
@patch(f'{module_utils}.session_utils.dynamodb_query', session_table_mock)
@patch(f'{module_utils}.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch(f'{module_utils}.s3_utils.get_s3_client', Mock(return_value=s3_mock))
@patch(f'{module_utils}.audit_utils.datetime', datetime_mock)
@patch(f'{module_utils}.file_download_utils.datetime', datetime_mock)
@patch(f'{module_utils}.file_download_utils.log', log_mock)
class TestDownloadPrintFile(TestCase):

    mock_session_record = [
        {
            'reauthenticate_timestamp': time() + 10,
            'expires': time() + 10,
            'user_data': {
                'first_name': 'firsty',
                'last_name': 'lasty',
                'nhsid_useruid': '78'
            },
            'selected_role': {
                'workgroups': ['cervical_screening', 'cervical_screening_DMS'],
                'role_id': 'test',
                'organisation_code': 'test'
            },
            'access_groups': {
                'csas': True,
                'csas_dms': True,
                'csas_iom': False
            }
        }
    ]

    mock_file = 'this is a test file'

    def setUp(self):
        session_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        log_mock.reset_mock()

    def test_download_print_file(self):
        context = Mock()
        context.function_name = ''

        file_stream_mock = Mock()
        file_stream_mock.read.return_value = b'test_file_line'
        object_mock = {
            'Body': file_stream_mock
        }

        session_table_mock.return_value = self.mock_session_record
        s3_mock.get_object.return_value = object_mock
        s3_mock.head_object.return_value = {
            'Metadata': {
                'existing_test_data': 'test'
            }
        }

        event = self._build_event(
            'DMS/test_file',
            'test_uid',
            'session_id',
            'GET'
        )

        expected_response = {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'text/plain',
                'X-Content-Type-Options': 'nosniff',
                'Strict-Transport-Security': 'max-age=1576800',
                'Content-Disposition': 'attachment; filename="DMS/test_file"'
            },
            'body': 'test_file_line',
            'isBase64Encoded': False
        }

        response = lambda_handler(event, context)

        s3_mock.get_object.assert_called_with(Bucket='PRINT_FILES_SUCCEEDED', Key='DMS/test_file')
        s3_mock.copy_object.assert_called_with(
            Bucket='PRINT_FILES_SUCCEEDED',
            Key='DMS/test_file',
            CopySource='PRINT_FILES_SUCCEEDED/DMS/test_file',
            Metadata={
                'existing_test_data': 'test',
                'downloaded_by': 'firsty lasty',
                'downloaded_date': '2020-10-18'
            },
            MetadataDirective='REPLACE'
        )
        print_files_table_mock.assert_called_once_with(
            TableNames.PRINT_FILES,
            {
                'Key': {'file_name': 'DMS/test_file', 'sort_key': PrintDestination.DMS},
                'UpdateExpression': 'SET #status = :status',
                'ExpressionAttributeValues': {':status': PrintFileStatus.WITH_PRINTER},
                'ExpressionAttributeNames': {'#status': 'status'},
                'ReturnValues': 'NONE'
            }
        )
        sqs_client_mock.send_message.assert_called_with(
            QueueUrl=None,
            MessageBody=json.dumps(
                {
                    'action': 'DMS_PRINT_FILE_DOWNLOADED',
                    'timestamp': '2020-10-18T13:48:08',
                    'internal_id': 'requestID',
                    'nhsid_useruid': 'test_uid',
                    'session_id': 'session_id',
                    'first_name': 'firsty',
                    'last_name': 'lasty',
                    'role_id': 'test',
                    'user_organisation_code': 'test',
                    'additional_information': {'file_name': 'DMS/test_file'}
                }
            )
        )
        self.assertEqual(expected_response, response)
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILE0001}),
            call({'log_reference': CommonLogReference.DOWNLOADFILE0004, 'file_name': 'DMS/test_file'}),
            call({'log_reference': CommonLogReference.DOWNLOADFILE0002}),
            call({'log_reference': CommonLogReference.DOWNLOADFILE0003})
        ])

    def _build_event(
            self,
            file_name,
            user_uid,
            session_id,
            http_method,
            body=None,
            resource='/api/download_print_file/dms'):
        if not body:
            body = '{}'
        return {
            'httpMethod': http_method,
            'resource': resource,
            'queryStringParameters': {
                'file_name': file_name
            },
            'path': resource,
            'headers': {
                'request_id': 'requestID',
                'cookie': {
                    'sp_session_cookie': 'choc_chip'
                }
            },
            'body': body,
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': json.dumps(
                        {
                            'user_data': {'nhsid_useruid': user_uid, 'first_name': 'firsty', 'last_name': 'lasty'},
                            'session_id': f'{session_id}',
                            'selected_role': {
                                'workgroups': ['cervical_screening', 'cervical_screening_DMS'],
                                'role_id': 'test',
                                'organisation_code': 'test'
                            },
                            'access_groups': {
                                'csas': True,
                                'csas_dms': True,
                                'csas_iom': False
                            }
                        }
                    )
                }
            }
        }
