from unittest import TestCase
from datetime import datetime, timezone
from mock import patch, Mock, MagicMock, call
from archive_gp_notifications.log_references import LogReference


class TestArchiveGPNotification(TestCase):

    example_date = datetime(2021, 1, 1, 0, 0, 0, 123456, tzinfo=timezone.utc)
    example_date_as_string = '2021-01-01T00:00:00.123456+00:00'
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.now.return_value = example_date

    mock_env_vars = {
        'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    }

    @patch('datetime.datetime', datetime_mock)
    @patch('os.environ', mock_env_vars)
    @patch('boto3.resource', MagicMock())
    @patch('boto3.client', MagicMock())
    @patch('common.log.get_internal_id', Mock(return_value='1'))
    def setUp(self, *args):
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = ''

        import archive_gp_notifications.archive_gp_notifications as _archive_gp_notifications
        import common.utils.audit_utils as _audit_utils

        self.archive_gp_notifications_module = _archive_gp_notifications
        self.audit_utils = _audit_utils
        self.mock_event = {}

    def tearDown(self):
        self.log_patcher.stop()

    @patch('archive_gp_notifications.archive_gp_notifications.archive_gp_notification')
    @patch('archive_gp_notifications.archive_gp_notifications.log')
    def test_handle_gp_notification_processes_gp_notification_list(self, log_mock, archive_gp_notification_mock):
        gp_notification_list = [
            {
                'participant_id': 'expired',
                'sort_key': 'sort_key',
                'expires': '2020-09-01T12:15:08.132000+00:00'
            }
        ]
        self.archive_gp_notifications_module.handle_gp_notification_chunk(gp_notification_list)

        archive_gp_notification_mock.assert_has_calls(
            [call({'participant_id': 'expired', 'sort_key': 'sort_key'}, self.example_date_as_string)]
        )
        log_mock.assert_has_calls(
            [
                call({'log_reference': LogReference.ARCHGPNOTIF2, 'expired_gp_notifications_count': 1}),
                call({'log_reference': LogReference.ARCHGPNOTIF3, 'count': '1/1'}),
                call({'log_reference': LogReference.ARCHGPNOTIF4, 'count': '1/1'})
            ]
        )

    @patch('archive_gp_notifications.archive_gp_notifications.archive_gp_notification')
    @patch('archive_gp_notifications.archive_gp_notifications.log')
    def test_handle_gp_notification_processes_gp_notification_list_with_many_records(self,
                                                                                     log_mock,
                                                                                     archive_gp_notification_mock):
        gp_notification_list = [
            {
                'participant_id': 'expired',
                'sort_key': 'sort_key',
                'expires': '2020-09-01T12:15:08.132000+00:00'
            },
            {
                'participant_id': 'expired1',
                'sort_key': 'sort_key',
                'expires': '2020-09-01T12:15:08.132000+00:00'
            }
        ]
        self.archive_gp_notifications_module.handle_gp_notification_chunk(gp_notification_list)

        archive_gp_notification_mock.assert_has_calls(
            [
                call({'participant_id': 'expired', 'sort_key': 'sort_key'}, self.example_date_as_string),
                call({'participant_id': 'expired1', 'sort_key': 'sort_key'}, self.example_date_as_string)
            ]
        )
        log_mock.assert_has_calls(
            [
                call({'log_reference': LogReference.ARCHGPNOTIF2, 'expired_gp_notifications_count': 2}),
                call({'log_reference': LogReference.ARCHGPNOTIF3, 'count': '1/2'}),
                call({'log_reference': LogReference.ARCHGPNOTIF4, 'count': '1/2'}),
                call({'log_reference': LogReference.ARCHGPNOTIF3, 'count': '2/2'}),
                call({'log_reference': LogReference.ARCHGPNOTIF4, 'count': '2/2'})
            ]
        )
