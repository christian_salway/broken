import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    ARCHGPNOTIF1 = (logging.INFO, 'Starting archive process for GP notifications')
    ARCHGPNOTIF2 = (logging.INFO, 'Processing expired GP notifications')
    ARCHGPNOTIF3 = (logging.INFO, 'Updating record as expired')
    ARCHGPNOTIF4 = (logging.INFO, 'Creating audit record')
