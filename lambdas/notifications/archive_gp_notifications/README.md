# Archive GP notifications
This lambda archives gp notifications if their expiry timestamp is less then todays date. If this occurs, the following parameters are set on the notification record:

* Update Status to ARCHIVED_NOTIFICATION
* Remove live_record_status
* add archived_date set to todays date/time as a timestamp

Then we record an audit event