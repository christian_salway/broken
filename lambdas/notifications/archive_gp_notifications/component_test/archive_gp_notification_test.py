from unittest import TestCase
from datetime import datetime, timezone
from mock import patch, Mock, call
from archive_gp_notifications.log_references import LogReference
from boto3.dynamodb.conditions import Key

participant_table_mock = Mock()


@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(return_value=participant_table_mock))
class TestArchiveGPNotification(TestCase):

    example_date = datetime(2021, 1, 1, 0, 0, 0, 123456, tzinfo=timezone.utc)
    example_date_as_string = '2021-01-01T00:00:00.123456+00:00'
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.now.return_value = example_date

    mock_env_vars = {
        'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    }

    @patch('datetime.datetime', datetime_mock)
    @patch('os.environ', mock_env_vars)
    @patch('boto3.client', Mock())
    @patch('common.log.get_internal_id', Mock(return_value='1'))
    def setUp(self, *args):
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = ''

        import archive_gp_notifications.archive_gp_notifications as _archive_gp_notifications
        import common.utils.audit_utils as _audit_utils

        self.archive_gp_notifications_module = _archive_gp_notifications
        self.audit_utils = _audit_utils
        self.mock_event = {}

    def tearDown(self):
        self.log_patcher.stop()

    @patch('archive_gp_notifications.archive_gp_notifications.audit')
    @patch('archive_gp_notifications.archive_gp_notifications.log')
    def test_lambda_archives_expired_gp_notifications_and_records_audit_events(self,
                                                                               log_mock,
                                                                               audit_mock):
        participant_table_mock.query.return_value = {
            'Items': [
                {
                    'participant_id': 'participant_id',
                    'sort_key': 'sort_key',
                    'expires': '2020-09-01T12:15:08.132000'
                }
            ]
        }
        response = self.archive_gp_notifications_module.lambda_handler(self.mock_event, self.context)
        self.assertTrue(response)

        audit_mock.assert_called_with(
            action=self.audit_utils.AuditActions.ARCHIVED_GP_NOTIFICATION,
            user=self.audit_utils.AuditUsers.SYSTEM,
            participant_ids=['participant_id']
        )
        participant_table_mock.query.assert_has_calls([
            call(
                IndexName='live-record-status-only',
                KeyConditionExpression=Key('live_record_status').eq('NEW_NOTIFICATION'),
                FilterExpression=Key('expires').lte(self.example_date_as_string)
            )
        ])
        participant_table_mock.update_item.assert_has_calls([
            call(
                Key={
                    'participant_id': 'participant_id',
                    'sort_key': 'sort_key'
                },
                UpdateExpression='SET #status = :status, #archived_date = :archived_date REMOVE #live_record_status',
                ExpressionAttributeValues={
                    ':status': 'ARCHIVED_NOTIFICATION',
                    ':archived_date': '2021-01-01T00:00:00.123456+00:00'
                }, ExpressionAttributeNames={
                    '#status': 'status',
                    '#archived_date': 'archived_date',
                    '#live_record_status': 'live_record_status'
                },
                ReturnValues='NONE'
            )
        ])
        log_mock.assert_has_calls(
            [
                call({'log_reference': LogReference.ARCHGPNOTIF1}),
                call({'log_reference': LogReference.ARCHGPNOTIF2, 'expired_gp_notifications_count': 1}),
                call({'log_reference': LogReference.ARCHGPNOTIF3, 'count': '1/1'}),
                call({'log_reference': LogReference.ARCHGPNOTIF4, 'count': '1/1'})
            ]
        )
