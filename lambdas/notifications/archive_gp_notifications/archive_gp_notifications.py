from datetime import datetime, timezone
from common.log import log
from archive_gp_notifications.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_gp_notification_utils import (
    archive_gp_notification, query_for_gp_notifications)
from common.utils.audit_utils import audit, AuditActions, AuditUsers


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.ARCHGPNOTIF1})
    expiry_data = datetime.now(timezone.utc).isoformat()
    query_for_gp_notifications(expiry_data, handle_gp_notification_chunk)
    return True


def handle_gp_notification_chunk(expired_gp_notifications):
    log({'log_reference': LogReference.ARCHGPNOTIF2, 'expired_gp_notifications_count': len(expired_gp_notifications)})

    for index, expired_notification in enumerate(expired_gp_notifications):
        record_key = {
            'participant_id': expired_notification['participant_id'],
            'sort_key': expired_notification['sort_key']
        }
        archive_time = datetime.now(timezone.utc).isoformat()
        log({'log_reference': LogReference.ARCHGPNOTIF3, 'count': f'{index + 1}/{len(expired_gp_notifications)}'})
        archive_gp_notification(record_key, archive_time)

        log({'log_reference': LogReference.ARCHGPNOTIF4, 'count': f'{index + 1}/{len(expired_gp_notifications)}'})
        audit(
            action=AuditActions.ARCHIVED_GP_NOTIFICATION,
            user=AuditUsers.SYSTEM,
            participant_ids=[expired_notification['participant_id']]
        )
