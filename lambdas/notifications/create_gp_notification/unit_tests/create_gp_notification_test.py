from unittest import TestCase
from mock import patch
from datetime import datetime, timedelta, timezone
from common.models.participant import EpisodeStatus
import os


class TestCreateInvite(TestCase):

    mock_env_vars = {
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
        'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
        'NOTIFICATION_EXPIRY_WEEKS': '8'
    }

    module = 'create_gp_notification.create_gp_notification'

    @patch.dict(os.environ, mock_env_vars)
    @patch('boto3.resource')
    def setUp(self, *args):
        import create_gp_notification.create_gp_notification as _create_gp_notification
        self.create_gp_notification_module = _create_gp_notification
        self.unwrapped_handler = self.create_gp_notification_module.lambda_handler.__wrapped__
        self.maxDiff = None

    @patch(f'{module}.datetime')
    def test_construct_notification_record_cease(self, datetime_mock):
        test_participant_id = 'test_id'
        gp_code = 'test_gp_code'
        notification_type = 'Recently Ceased'
        notification_origin = EpisodeStatus.CEASED.value

        datetime_mock.now.return_value = datetime(2020, 1, 1, 0, 0, 0, 123456, tzinfo=timezone.utc)
        datetime_mock.timedelta = timedelta(weeks=8)

        actual_result = self.create_gp_notification_module.construct_notification_record(
            test_participant_id, gp_code, notification_type, notification_origin
        )

        expected_result = {
            'participant_id': test_participant_id,
            'sort_key': f'GP-NOTIFICATION#{gp_code}#2020-01-01T00:00:00.123456+00:00',
            'created': '2020-01-01T00:00:00.123456+00:00',
            'expires': '2020-02-26T00:00:00.123456+00:00',
            'registered_gp_practice_code': gp_code,
            'registered_gp_practice_code_and_test_due_date': 'test_gp_code',
            'user_id': 'cease_participant',
            'status': 'NEW_NOTIFICATION',
            'live_record_status': 'NEW_NOTIFICATION',
            'notification_type': notification_type,
            'notification_origin': notification_origin
        }

        self.assertEqual(expected_result, actual_result)

    @patch(f'{module}.datetime')
    def test_construct_notification_record_import_demographics(self, datetime_mock):
        test_participant_id = 'test_id'
        gp_code = 'test_gp_code'
        notification_type = 'Recently Ceased'
        notification_origin = 'import_demographics'

        datetime_mock.now.return_value = datetime(2020, 1, 1, 0, 0, 0, 123456, tzinfo=timezone.utc)

        actual_result = self.create_gp_notification_module.construct_notification_record(
            test_participant_id, gp_code, notification_type, notification_origin
        )

        expected_result = {
            'participant_id': test_participant_id,
            'sort_key': f'GP-NOTIFICATION#{gp_code}#2020-01-01T00:00:00.123456+00:00',
            'created': '2020-01-01T00:00:00.123456+00:00',
            'expires': '2020-02-26T00:00:00.123456+00:00',
            'registered_gp_practice_code': gp_code,
            'registered_gp_practice_code_and_test_due_date': 'test_gp_code',
            'user_id': 'import_demographics',
            'status': 'NEW_NOTIFICATION',
            'live_record_status': 'NEW_NOTIFICATION',
            'notification_type': notification_type,
            'notification_origin': notification_origin
        }

        self.assertEqual(expected_result, actual_result)

    def test_derive_notification_type_ceased(self):
        test_participant_id = 'test_id'
        test_notification_origin = EpisodeStatus.CEASED.value

        actual_result = self.create_gp_notification_module.derive_notification_type(
            test_participant_id, test_notification_origin
        )

        expected_result = 'Recently Ceased'

        self.assertEqual(expected_result, actual_result)

    @patch(f'{module}.get_participant_and_test_results')
    def test_derive_notification_type_gp_registration_ceased(self, get_participant_and_test_results_mock):
        test_participant_id = 'test_id'
        test_notification_origin = 'GP_REGISTRATION'

        get_participant_and_test_results_mock.return_value = [
            {
                'sort_key': 'PARTICIPANT',
                'is_ceased': True
            },
            {
                'sort_key': 'RESULT',
                'test_date': '2020-01-01'
            }
        ]

        actual_result = self.create_gp_notification_module.derive_notification_type(
            test_participant_id, test_notification_origin
        )

        expected_result = 'Registered Ceased'

        self.assertEqual(expected_result, actual_result)

    @patch(f'{module}.get_participant_and_test_results')
    def test_derive_notification_type_gp_registration_abnormal_followup(self, get_participant_and_test_results_mock):
        test_participant_id = 'test_id'
        test_notification_origin = 'GP_REGISTRATION'

        get_participant_and_test_results_mock.return_value = [
            {
                'sort_key': 'PARTICIPANT',
                'is_ceased': False
            },
            {
                'sort_key': 'RESULT',
                'test_date': '2020-01-01',
                'action_code': 'S'
            }
        ]

        actual_result = self.create_gp_notification_module.derive_notification_type(
            test_participant_id, test_notification_origin
        )

        expected_result = 'Abnormal Follow-up'

        self.assertEqual(expected_result, actual_result)

    @patch(f'{module}.get_participant_and_test_results')
    def test_derive_notification_type_gp_registration_h_then_s_r(self, get_participant_and_test_results_mock):
        test_participant_id = 'test_id'
        test_notification_origin = 'GP_REGISTRATION'

        get_participant_and_test_results_mock.return_value = [
            {
                'sort_key': 'PARTICIPANT',
                'is_ceased': False
            },
            {
                'sort_key': 'RESULT',
                'test_date': '2020-01-05',
                'action_code': 'H'
            },
            {
                'sort_key': 'RESULT',
                'test_date': '2020-01-01',
                'action_code': 'R'
            }
        ]

        actual_result = self.create_gp_notification_module.derive_notification_type(
            test_participant_id, test_notification_origin
        )

        expected_result = 'Abnormal Follow-up'

        self.assertEqual(expected_result, actual_result)

    @patch(f'{module}.get_participant_and_test_results')
    def test_derive_notification_type_gp_registration_h_then_not_s_r(self, get_participant_and_test_results_mock):
        test_participant_id = 'test_id'
        test_notification_origin = 'GP_REGISTRATION'

        get_participant_and_test_results_mock.return_value = [
            {
                'sort_key': 'PARTICIPANT',
                'is_ceased': False
            },
            {
                'sort_key': 'RESULT',
                'test_date': '2020-01-05',
                'action_code': 'H'
            },
            {
                'sort_key': 'RESULT',
                'test_date': '2020-01-01',
                'action_code': 'Q'
            }
        ]

        with self.assertRaises(Exception) as context:
            self.create_gp_notification_module.derive_notification_type(
                test_participant_id, test_notification_origin
            )

        self.assertTrue('Second most recent result has invalid action code' in str(context.exception))

    @patch(f'{module}.get_participant_and_test_results')
    def test_derive_notification_type_gp_registration_only_h(self, get_participant_and_test_results_mock):
        test_participant_id = 'test_id'
        test_notification_origin = 'GP_REGISTRATION'

        get_participant_and_test_results_mock.return_value = [
            {
                'sort_key': 'PARTICIPANT',
                'is_ceased': False
            },
            {
                'sort_key': 'RESULT',
                'test_date': '2020-01-05',
                'action_code': 'H'
            }
        ]

        with self.assertRaises(Exception) as context:
            self.create_gp_notification_module.derive_notification_type(
                test_participant_id, test_notification_origin
            )

        self.assertTrue('Most recent result has code H but no second result to refer to' in str(context.exception))

    @patch(f'{module}.get_participant_and_test_results')
    def test_derive_notification_type_gp_registration_invalid_code(self, get_participant_and_test_results_mock):
        test_participant_id = 'test_id'
        test_notification_origin = 'GP_REGISTRATION'

        get_participant_and_test_results_mock.return_value = [
            {
                'sort_key': 'PARTICIPANT',
                'is_ceased': False
            },
            {
                'sort_key': 'RESULT',
                'test_date': '2020-01-05',
                'action_code': 'Q'
            }
        ]

        with self.assertRaises(Exception) as context:
            self.create_gp_notification_module.derive_notification_type(
                test_participant_id, test_notification_origin
            )

        self.assertTrue('Most recent result has invalid action code' in str(context.exception))

    @patch(f'{module}.get_participant_and_test_results')
    def test_derive_notification_type_gp_registration_no_results(self, get_participant_and_test_results_mock):
        test_participant_id = 'test_id'
        test_notification_origin = 'GP_REGISTRATION'

        get_participant_and_test_results_mock.return_value = [
            {
                'sort_key': 'PARTICIPANT',
                'is_ceased': False
            }
        ]

        result = self.create_gp_notification_module.derive_notification_type(
            test_participant_id, test_notification_origin
        )

        self.assertEqual(result, None)

    def test_derive_notification_type_invalid_origin(self):
        test_participant_id = 'test_id'
        test_notification_origin = 'INVALID_ORIGIN'

        with self.assertRaises(Exception) as context:
            self.create_gp_notification_module.derive_notification_type(
                test_participant_id, test_notification_origin
            )

        self.assertTrue('Invalid notification origin' in str(context.exception))

    def test_validate_message(self):
        test_message = {
            'participant_id': 'test_participant_id',
            'registered_gp_practice_code': 'test_registered_gp_practice_code',
            'notification_origin': 'notification_origin'
        }

        actual_id, actual_gp_code, actual_notification_origin = self.create_gp_notification_module.validate_message(
            test_message
        )

        self.assertEqual(actual_id, test_message['participant_id'])
        self.assertEqual(actual_gp_code, test_message['registered_gp_practice_code'])
        self.assertEqual(actual_notification_origin, test_message['notification_origin'])

    def test_validate_message_key_error(self):
        test_message = {
            'participant_id': 'test_participant_id',
            'registered_gp_practice_code': 'test_registered_gp_practice_code'
        }

        with self.assertRaises(Exception) as context:
            actual_id, actual_gp_code, actual_notification_origin = self.create_gp_notification_module.validate_message(
                test_message
            )

        self.assertTrue('Message body is missing mandatory data' in str(context.exception))
