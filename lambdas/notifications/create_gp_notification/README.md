# Create Gp Notification

## Function

This lambda receives messages from the `gp-notifications-to-create` queue. This is to reduce the amount of processing done in a single lambda for the print process. Using notification records to store print data means we can get all necessary print data in a single query further down the print process. An example input data structure can be found below

## Example input data

```
{
    "participant_id": "UUID",
    "registered_gp_practice_code": "ABC1234",
    "notification_origin": "CEASED" // This can be CEASED or GP_REGISTRATION , anything else should be rejected
}
```

These three fields are mandatory, any messages without these three will be rejected upon processing. The only two valid notification origins are CEASED and GP_REGISTRATION currently. These come from `cease_participant` and `load_demographics` respectively.