import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    CREGPNOTIF0001 = (logging.INFO, 'Validating incoming message')
    CREGPNOTIF0002 = (logging.INFO, 'Deriving gp_notification type')
    CREGPNOTIF0003 = (logging.INFO, 'Identified gp_notification type')
    CREGPNOTIF0004 = (logging.INFO, 'Building gp_notification record')
    CREGPNOTIF0005 = (logging.INFO, 'Record created successfully')
    CREGPNOTIF0006 = (logging.INFO, 'No results for participant so no notification record created')

    CREGPNOTIFEX0001 = (logging.ERROR, 'Message validation failed')
    CREGPNOTIFEX0002 = (logging.ERROR, 'Problem deriving notification type')
