import json
import os
from datetime import datetime, timedelta, timezone
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.sqs_utils import read_message_from_queue
from create_gp_notification.log_references import LogReference
from common.log import log
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.utils.participant_utils import (
    get_participant_and_test_results, create_replace_record_in_participant_table
)
from common.models.participant import ParticipantSortKey, EpisodeStatus

notification_expiry_weeks = int(os.environ.get('NOTIFICATION_EXPIRY_WEEKS'))


@lambda_entry_point
def lambda_handler(event, context):
    try:
        log({'log_reference': LogReference.CREGPNOTIF0001})
        participant_id, registered_gp_practice_code, notification_origin = validate_message(
            json.loads(read_message_from_queue(event))
        )
    except Exception as e:
        log({'log_reference': LogReference.CREGPNOTIFEX0001})
        raise e

    try:
        log({'log_reference': LogReference.CREGPNOTIF0002})
        notification_type = derive_notification_type(participant_id, notification_origin)
        if not notification_type:
            log({'log_reference': LogReference.CREGPNOTIF0006})
            return
    except Exception as e:
        log({'log_reference': LogReference.CREGPNOTIFEX0002})
        raise e

    log({'log_reference': LogReference.CREGPNOTIF0004})
    create_replace_record_in_participant_table(
        construct_notification_record(
            participant_id,
            registered_gp_practice_code,
            notification_type,
            notification_origin
        )
    )
    log({'log_reference': LogReference.CREGPNOTIF0005})

    audit(action=AuditActions.CREATE_GP_NOTIFICATION, user=AuditUsers.SYSTEM, participant_ids=[participant_id])


def validate_message(message):
    try:
        return message['participant_id'], message['registered_gp_practice_code'], message['notification_origin']
    except KeyError as e:
        raise Exception('Message body is missing mandatory data') from e


def derive_notification_type(participant_id, notification_origin):
    if notification_origin == EpisodeStatus.CEASED.value:
        return 'Recently Ceased'
    if notification_origin == 'GP_REGISTRATION':
        participant_and_results = get_participant_and_test_results(participant_id)
        participant = [
            record for record in participant_and_results if record['sort_key'] == ParticipantSortKey.PARTICIPANT
        ][0]
        if participant.get('is_ceased'):
            return 'Registered Ceased'

        results_by_date = sorted([
            record for record in participant_and_results if record['sort_key'].startswith(ParticipantSortKey.RESULT)
        ], key=lambda record: record.get('test_date'), reverse=True)

        if not results_by_date:
            return None

        most_recent_result = results_by_date[0]
        most_recent_action_code = most_recent_result['action_code']

        if most_recent_action_code not in ['S', 'R', 'H']:
            raise Exception('Most recent result has invalid action code')

        if most_recent_result['action_code'] == 'H' and len(results_by_date) < 2:
            raise Exception('Most recent result has code H but no second result to refer to')

        if most_recent_result['action_code'] in ['S', 'R']:
            return 'Abnormal Follow-up'

        if most_recent_result['action_code'] == 'H':
            second_most_recent_action_code = results_by_date[1]['action_code']
            if second_most_recent_action_code in ['S', 'R']:
                return 'Abnormal Follow-up'
            else:
                raise Exception('Second most recent result has invalid action code')

    raise Exception('Invalid notification origin')


def construct_notification_record(participant_id, gp_code, notification_type, notification_origin):
    created_time = datetime.now(timezone.utc)
    expiry_time = created_time + timedelta(weeks=notification_expiry_weeks)
    return {
        'participant_id': participant_id,
        'sort_key': f'{ParticipantSortKey.GP_NOTIFICATION}#{gp_code}#{created_time.isoformat()}',
        'created': f'{created_time.isoformat()}',
        'expires': f'{expiry_time.isoformat()}',
        'registered_gp_practice_code': gp_code,
        'user_id': 'cease_participant' if notification_origin == EpisodeStatus.CEASED.value else 'import_demographics',
        'status': 'NEW_NOTIFICATION',
        'live_record_status': 'NEW_NOTIFICATION',
        'notification_type': notification_type,
        'notification_origin': notification_origin,
        'registered_gp_practice_code_and_test_due_date': gp_code  # This allows us to use the existing index with field
    }
