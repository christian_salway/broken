import json
import os
from unittest import TestCase
from mock import patch, Mock, MagicMock, call
from datetime import datetime, timezone
from create_gp_notification.log_references import LogReference
from common.models.participant import EpisodeStatus


class TestCreateInvite(TestCase):

    mock_env_vars = {
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
        'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
        'NOTIFICATION_EXPIRY_WEEKS': '8',
        'SESSIONS_TABLE_NAME': 'TEST'
    }

    participant_record = {}

    example_date = datetime(2020, 1, 23, 13, 48, 8, 123456, tzinfo=timezone.utc)
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.today.return_value = example_date
    datetime_mock.now.return_value = example_date

    @classmethod
    @patch('datetime.datetime', datetime_mock)
    @patch('boto3.client')
    @patch('boto3.resource')
    @patch.dict(os.environ, mock_env_vars)
    def setUpClass(cls, boto3_resource, boto3_client):
        cls.log_mock = MagicMock()
        cls.log_patcher = patch('create_gp_notification.create_gp_notification.log', cls.log_mock)
        cls.env_patcher = patch('os.environ', cls.mock_env_vars)

        cls.participants_table_mock = MagicMock()

        # Setup database mock
        participants_table_mock = Mock()
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = participants_table_mock
        boto3_resource.return_value = boto3_table_mock
        cls.participants_table_mock = participants_table_mock

        sqs_client_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: sqs_client_mock if args and args[0] == 'sqs' else Mock()
        cls.sqs_client_mock = sqs_client_mock

        # Setup lambda module
        import create_gp_notification.create_gp_notification as _create_gp_notification
        global create_gp_notification_module
        create_gp_notification_module = _create_gp_notification

    def setUp(self):
        self.participants_table_mock.reset_mock()
        self.sqs_client_mock.reset_mock()
        self.log_mock.reset_mock()
        self.log_patcher.start()
        self.env_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    def test_create_gp_notification(self):

        context = Mock()
        context.function_name = ''

        mock_event = {
            'Records': [{
                'eventSource': 'aws:sqs',
                'body': json.dumps({
                    'internal_id': '123456',
                    'participant_id': 'test_participant_id',
                    'registered_gp_practice_code': 'test_registered_gp_practice_code',
                    'notification_origin': EpisodeStatus.CEASED.value
                }, indent=4, sort_keys=True, default=str)
            }]
        }

        create_gp_notification_module.lambda_handler(mock_event, context)

        self.sqs_client_mock.send_message.assert_called_with(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody='{"action": "CREATE_GP_NOTIFICATION", "timestamp": "2020-01-23T13:48:08.123456+00:00", "internal_id": "123456", "nhsid_useruid": "SYSTEM", "session_id": "NONE", "participant_ids": ["test_participant_id"]}' 
        )
        self.participants_table_mock.put_item.assert_called_with(
            Item={
                'participant_id': 'test_participant_id',
                'sort_key': 'GP-NOTIFICATION#test_registered_gp_practice_code#2020-01-23T13:48:08.123456+00:00',
                'created': '2020-01-23T13:48:08.123456+00:00',
                'expires': '2020-03-19T13:48:08.123456+00:00',
                'registered_gp_practice_code': 'test_registered_gp_practice_code',
                'user_id': 'cease_participant',
                'status': 'NEW_NOTIFICATION',
                'live_record_status': 'NEW_NOTIFICATION',
                'notification_type': 'Recently Ceased',
                'notification_origin': EpisodeStatus.CEASED.value,
                'registered_gp_practice_code_and_test_due_date': 'test_registered_gp_practice_code',
            }
        )
        self.log_mock.assert_has_calls([
            call({'log_reference': LogReference.CREGPNOTIF0001}),
            call({'log_reference': LogReference.CREGPNOTIF0002}),
            call({'log_reference': LogReference.CREGPNOTIF0004}),
            call({'log_reference': LogReference.CREGPNOTIF0005})
        ])

    def test_create_gp_notification_invalid_message(self):
        function_context = Mock()
        function_context.function_name = ''

        mock_event = {
            'Records': [{
                'eventSource': 'aws:sqs',
                'body': json.dumps({
                    'internal_id': '123456',
                    'participant_id': 'test_participant_id',
                    'registered_gp_practice_code': 'test_registered_gp_practice_code'
                }, indent=4, sort_keys=True, default=str)
            }]
        }

        with self.assertRaises(Exception) as context:
            create_gp_notification_module.lambda_handler(mock_event, function_context)

        self.assertTrue('Message body is missing mandatory data' in str(context.exception))

        self.log_mock.assert_has_calls([
            call({'log_reference': LogReference.CREGPNOTIF0001}),
            call({'log_reference': LogReference.CREGPNOTIFEX0001})
        ])

    def test_create_gp_notification_invalid_notification_type(self):
        function_context = Mock()
        function_context.function_name = ''

        mock_event = {
            'Records': [{
                'eventSource': 'aws:sqs',
                'body': json.dumps({
                    'internal_id': '123456',
                    'participant_id': 'test_participant_id',
                    'registered_gp_practice_code': 'test_registered_gp_practice_code',
                    'notification_origin': 'WRONG'
                }, indent=4, sort_keys=True, default=str)
            }]
        }

        with self.assertRaises(Exception) as context:
            create_gp_notification_module.lambda_handler(mock_event, function_context)

        self.assertTrue('Invalid notification origin' in str(context.exception))

        self.log_mock.assert_has_calls([
            call({'log_reference': LogReference.CREGPNOTIF0001}),
            call({'log_reference': LogReference.CREGPNOTIF0002}),
            call({'log_reference': LogReference.CREGPNOTIFEX0002})
        ])
