from unittest.case import TestCase
from ddt import ddt, data, unpack
from mock import patch

module = 'get_confirmation_report_list.get_confirmation_report_list_service'

mock_env_vars = {
    'ENVIRONMENT_NAME': 'THE_ENVIRONMENT_NAME',
    'CONFIRMATION_REPORTS_BUCKET': 'THE_BUCKET_NAME'
}


@ddt
class TestGetConfirmationReportListService(TestCase):
    @classmethod
    @patch('boto3.resource')
    @patch('boto3.client')
    def setUpClass(cls, client_mock, resource_mock):
        global get_confirmation_report_list_service_module
        import get_confirmation_report_list.get_confirmation_report_list_service as get_confirmation_report_list_service_module   

    def setUp(self):
        super(TestGetConfirmationReportListService, self).setUp()

    @unpack
    @data(
        (
            {},
            {}
        ),
        (
            {
                'Contents': [
                    {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-02-2021/a_report_DDMMYY_10.csv'},
                    {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-02-2021/b_report_DDMMYY_10.csv'},
                    {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-02-2021/a_report_DDMMYY_00.csv'},
                    {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-02-2021/b_report_DDMMYY_00.csv'},
                    {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-02-2021/a_report_DDMMYY_20.csv'},
                    {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-02-2021/b_report_DDMMYY_20.csv'}
                ],
                'CommonPrefixes': [
                    {'Prefix': 'filename/'}
                ]
            },
            {
                '00': ['a_report_DDMMYY_00.csv', 'b_report_DDMMYY_00.csv'],
                '10': ['a_report_DDMMYY_10.csv', 'b_report_DDMMYY_10.csv'],
                '20': ['a_report_DDMMYY_20.csv', 'b_report_DDMMYY_20.csv']
            }
        )
    )
    @patch('os.environ', mock_env_vars)
    @patch(f'{module}.read_bucket_with_subfolders')
    def test_get_report_names_for_report_date(self,
                                              file_paths, expected_result,
                                              subfolder_mock):
        # Given
        report_date = '01-02-2021'
        subfolder_mock.return_value = file_paths
        # When
        actual_result = get_confirmation_report_list_service_module.get_report_names_for_report_date(report_date)
        # Then
        subfolder_mock.assert_called_once_with('THE_BUCKET_NAME', 'THE_ENVIRONMENT_NAME/01-02-2021/')
        self.assertEqual(expected_result, actual_result)

    @patch('os.environ', mock_env_vars)
    def test_get_bucket_name(self):
        # Given
        expected_result = 'THE_BUCKET_NAME'
        # When
        actual_result = get_confirmation_report_list_service_module._get_bucket_name()
        # Then
        self.assertEqual(expected_result, actual_result)

    @patch('os.environ', mock_env_vars)
    def test_get_folder_name(self):
        # Given
        report_date = '01-02-2021'
        expected_result = f'THE_ENVIRONMENT_NAME/{report_date}/'
        # When
        actual_result = get_confirmation_report_list_service_module._get_folder_name(report_date)
        # Then
        self.assertEqual(expected_result, actual_result)
