from unittest.case import TestCase
from mock import patch, call

from get_confirmation_report_list.log_references import LogReference

module = 'get_confirmation_report_list.get_confirmation_report_list'


class TestGetConfirmationReportList(TestCase):
    @classmethod
    @patch('boto3.resource')
    @patch('boto3.client')
    def setUpClass(cls, client_mock, resource_mock):
        global get_confirmation_report_list_module
        import get_confirmation_report_list.get_confirmation_report_list as get_confirmation_report_list_module               

    def setUp(self):
        super(TestGetConfirmationReportList, self).setUp()
        self.unwrapped_handler = get_confirmation_report_list_module.lambda_handler.__wrapped__.__wrapped__

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_get_confirmation_report_list_returns_400_if_no_report_date(self,
                                                                        get_api_request_mock,
                                                                        log_mock):
        # Given
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {},
                'body': '',
                'session': {}
            },
            '',
            '')
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Report date not supplied"}',
            'isBase64Encoded': False
        }
        # When
        response = self.unwrapped_handler('event', 'context')
        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with('event')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETCONFIRMREPLIST0001}),
            call({'log_reference': LogReference.GETCONFIRMREPLISTER0001})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.get_report_names_for_report_date')
    def test_get_confirmation_report_list_returns_500_if_exception_on_get_report_names(self,
                                                                                       get_report_names_mock,
                                                                                       get_api_request_mock,
                                                                                       log_mock):
        # Given
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'report_date': '01-02-2021'},
                'body': '',
                'session': {}
            },
            '',
            '')
        expected_response = {
            'statusCode': 500,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Cannot complete request"}',
            'isBase64Encoded': False
        }
        get_report_names_mock.side_effect = Exception('Something went wrong')

        # When
        response = self.unwrapped_handler('event', 'context')
        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with('event')
        get_report_names_mock.assert_called_once_with('01-02-2021')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETCONFIRMREPLIST0001}),
            call({'log_reference': LogReference.GETCONFIRMREPLIST0002, 'report_date': '01-02-2021'}),
            call({'log_reference': LogReference.GETCONFIRMREPLISTEX0001, 'error': 'Something went wrong'})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.get_report_names_for_report_date')
    def test_get_confirmation_report_list_returns_200_for_happy_path(self,
                                                                     get_report_names_mock,
                                                                     get_api_request_mock,
                                                                     log_mock):
        # Given
        get_api_request_mock.return_value = (
            {
                'path_parameters': {},
                'query_parameters': {'report_date': '01-02-2021'},
                'body': '',
                'session': {}
            },
            '',
            '')
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": {"00": ["report_DDMMYY_00.csv"], "01": ["report_DDMMYY_01.csv"]}}',
            'isBase64Encoded': False
        }
        get_report_names_mock.return_value = {'00': ['report_DDMMYY_00.csv'], '01': ['report_DDMMYY_01.csv']}

        # When
        response = self.unwrapped_handler('event', 'context')
        # Then
        self.assertEqual(expected_response, response)
        get_api_request_mock.assert_called_once_with('event')
        get_report_names_mock.assert_called_once_with('01-02-2021')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETCONFIRMREPLIST0001}),
            call({'log_reference': LogReference.GETCONFIRMREPLIST0002, 'report_date': '01-02-2021'})
        ])
