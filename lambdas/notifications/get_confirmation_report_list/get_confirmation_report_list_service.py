import os
from common.utils.s3_utils import (
    read_bucket_with_subfolders
)


def get_report_names_for_report_date(report_date):
    bucket_contents = read_bucket_with_subfolders(_get_bucket_name(), _get_folder_name(report_date))
    files_in_bucket = bucket_contents.get('Contents', [])
    file_paths = [file['Key'] for file in files_in_bucket]
    times_dict = {}
    for path in file_paths:
        file_name = path.split('/')[-1]
        hour = file_name.split('_')[-1][:2]
        if hour != "":
            times_dict.setdefault(hour, []).append(file_name)

    return {k: times_dict[k] for k in sorted(times_dict.keys())}


def _get_bucket_name():
    return os.environ.get('CONFIRMATION_REPORTS_BUCKET')


def _get_folder_name(report_date):
    ENVIRONMENT_NAME = os.environ.get('ENVIRONMENT_NAME')
    return f'{ENVIRONMENT_NAME}/{report_date}/'
