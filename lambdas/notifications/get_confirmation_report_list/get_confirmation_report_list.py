from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.log import log
from common.utils import json_return_data, json_return_message
from common.utils.api_utils import get_api_request_info_from_event
from .log_references import LogReference
from .get_confirmation_report_list_service import (
    get_report_names_for_report_date
)


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):

    log({'log_reference': LogReference.GETCONFIRMREPLIST0001})

    event_data = get_api_request_info_from_event(event)[0]

    report_date = event_data.get('query_parameters', {}).get('report_date')
    if not report_date:
        log({'log_reference': LogReference.GETCONFIRMREPLISTER0001})
        return json_return_message(400, 'Report date not supplied')

    log({'log_reference': LogReference.GETCONFIRMREPLIST0002, 'report_date': report_date})

    try:
        report_names = get_report_names_for_report_date(report_date)
    except Exception as e:
        log({'log_reference': LogReference.GETCONFIRMREPLISTEX0001, 'error': str(e)})
        return json_return_message(500, 'Cannot complete request')

    return json_return_data(200, report_names)
