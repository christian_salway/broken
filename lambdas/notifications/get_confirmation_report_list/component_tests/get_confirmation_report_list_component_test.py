from datetime import datetime, timezone
from unittest import TestCase
from mock import patch, Mock, MagicMock, call
from json import loads
from get_confirmation_report_list.log_references import LogReference

module = 'get_confirmation_report_list.get_confirmation_report_list'


class TestGetConfirmationReportListComponent(TestCase):

    example_date = datetime(2020, 10, 20, tzinfo=timezone.utc)
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.now.return_value = example_date

    mock_env_vars = {
        'ENVIRONMENT_NAME': 'THE_ENVIRONMENT_NAME',
        'CONFIRMATION_REPORTS_BUCKET': 'THE_BUCKET_NAME'
    }

    @patch('datetime.datetime', datetime_mock)
    @patch('os.environ', mock_env_vars)
    @patch('boto3.resource', MagicMock())
    @patch('boto3.client', MagicMock())
    @patch('common.log.get_internal_id', Mock(return_value='1'))
    def setUp(self, *args):
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.env_patcher = patch('os.environ', self.mock_env_vars)
        self.env_patcher.start()
        self.context = Mock()
        self.context.function_name = ''

        # Setup lambda module
        import get_confirmation_report_list.get_confirmation_report_list as _get_confirmation_report_list
        self.get_confirmation_report_list_module = _get_confirmation_report_list

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    @patch('common.utils.s3_utils.get_s3_client')
    @patch(f'{module}.log')
    def test_lambda_returns_failure_if_report_date_missing(self, log_mock, s3_mock):
        self.event = _build_event()

        s3_client_mock = Mock()
        s3_client_mock.get_object.return_value = {}
        s3_client_mock.list_objects_v2.return_value = {}

        s3_mock.return_value = s3_client_mock

        response = self.get_confirmation_report_list_module.lambda_handler(self.event, self.context)

        self.assertEqual(response['statusCode'], 400)
        body = loads(response['body'])
        self.assertEqual(body['message'], 'Report date not supplied')

        s3_client_mock.list_objects_v2.assert_not_called()
        s3_client_mock.get_object.assert_not_called()

        log_mock.assert_has_calls(
            [
                call({'log_reference': LogReference.GETCONFIRMREPLIST0001}),
                call({'log_reference': LogReference.GETCONFIRMREPLISTER0001}),
            ]
        )

    @patch('common.utils.s3_utils.get_s3_client')
    @patch(f'{module}.log')
    def test_lambda_returns_list_of_reports(self, log_mock, s3_mock):
        self.event = _build_event()
        self.event['queryStringParameters'].update({'report_date': '01-01-2021'})

        s3_client_mock = Mock()
        s3_client_mock.get_object.return_value = {}
        s3_client_mock.list_objects_v2.return_value = {
            'Contents': [
                {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-01-2021/a_report_DDMMYY_10.csv'},
                {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-01-2021/b_report_DDMMYY_10.csv'},
                {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-01-2021/a_report_DDMMYY_00.csv'},
                {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-01-2021/b_report_DDMMYY_00.csv'},
                {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-01-2021/a_report_DDMMYY_20.csv'},
                {'Key': '/THE_BUCKET_NAME/THE_ENVIRONMENT_NAME/01-01-2021/b_report_DDMMYY_20.csv'}
            ],
            'CommonPrefixes': [
                {'Prefix': 'filename/'}
            ]
        }

        s3_mock.return_value = s3_client_mock

        response = self.get_confirmation_report_list_module.lambda_handler(self.event, self.context)

        print(response)

        self.assertEqual(response['statusCode'], 200)
        body = loads(response['body'])
        self.assertEqual(body['data'], {'00': ['a_report_DDMMYY_00.csv', 'b_report_DDMMYY_00.csv'],
                                        '10': ['a_report_DDMMYY_10.csv', 'b_report_DDMMYY_10.csv'],
                                        '20': ['a_report_DDMMYY_20.csv', 'b_report_DDMMYY_20.csv']})

        s3_client_mock.list_objects_v2.assert_called_once_with(
            Bucket='THE_BUCKET_NAME', Prefix='THE_ENVIRONMENT_NAME/01-01-2021/', Delimiter='/')
        s3_client_mock.get_object.assert_not_called()

        log_mock.assert_has_calls(
            [
                call({'log_reference': LogReference.GETCONFIRMREPLIST0001}),
                call({'log_reference': LogReference.GETCONFIRMREPLIST0002, 'report_date': '01-01-2021'}),
            ]
        )


def _build_event():
    return {
        'queryStringParameters': {
        },
        'headers': {
            'request_id': 'requestID',
        },
        'requestContext': {
            'authorizer': {
                'principalId': 'blah',
                'session': "{\"user_data\": {\"nhsid_useruid\": \"user_uid\"}, \"session_id\": \"the_session_id\", "
                           "\"session_token\": \"the_session_token\", \"hashed_session_token\": \"the_hashed_token\"}"
            }
        }
    }
