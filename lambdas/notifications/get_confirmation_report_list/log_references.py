import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    GETCONFIRMREPLIST0001 = (logging.INFO, 'Received request to get confirmation report list')
    GETCONFIRMREPLIST0002 = (logging.INFO, 'Request is for date')

    GETCONFIRMREPLISTER0001 = (logging.ERROR, 'Report date not supplied')

    GETCONFIRMREPLISTEX0001 = (logging.ERROR, 'Could not retrieve file name list ')
