This lambda returns a list of print confirmation reports to be displayed on the web-app. The user will then be able to choose
a specific report to download.