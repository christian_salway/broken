This lambda is an api lambda it will be triggered by an api endpoint
```
POST /participants/{participant_id}/notifications/{notification_sort_key}/resend
```

The body will be like
```
{
    "crm": {
        "crm_number" : "123456",
        "comments" : "Some Text"
    }
}
```

It will check if the invitation/reminder notification is 12 months old if not return an error

Creates a new notification with the same address was used before, and add original sent date

Audits the action with CRM and comments
