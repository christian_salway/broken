import json
from unittest import TestCase
from datetime import datetime, date
from mock import patch, Mock, ANY, call
from common.test_assertions.api_assertions import assertApiResponse
from common.models.letters import NotificationType, NotificationStatus
from common.models.participant import EpisodeStatus

mock_env_vars = {
    'RESULT_LETTER_CUT_OFF_MONTHS': '3',
    'AUDIT_QUEUE_URL': 'MY_AUDIT_QUEUE_URL'
}

with patch('os.environ', mock_env_vars):
    from manual_resend_notification.manual_resend_notification import lambda_handler, LogReference


class DateTimeMock(datetime):
    @classmethod
    def now(cls, tz):
        return cls(2021, 1, 1, 0, 0, 0, 0, tzinfo=tz)


mock_session = {
    'session_id': 'the_session',
    'user_data': {'nhsid_useruid': 'the_user', 'first_name': 'Tom', 'last_name': 'Smith'}
}

date_mock = Mock(wraps=date)
date_mock.today.return_value = date(2021, 1, 1)


@patch('manual_resend_notification.manual_resend_notification.log')
@patch('os.environ', mock_env_vars)
@patch('common.utils.result_letter_utils.date', date_mock)
class TestManualResendNotificationUnhappyPath(TestCase):

    def get_event(self):
        return {
            'body': json.dumps(self.body),
            'pathParameters': self.path_parameters,
            'requestContext': {
                'authorizer': {
                    'session': json.dumps(mock_session)
                }
            }
        }

    def setUp(self):
        self.body = {}
        self.path_parameters = {
            'participant_id': '123',
            'sort_key': 'NOTIFICATION#2021-01-01T00:00:00+00:00'
        }
        self.context = Mock()
        self.context.function_name = 'test'
        self.get_db_table = patch('common.utils.dynamodb_access.operations.get_dynamodb_table').start()
        self.audit_patcher = patch('common.utils.audit_utils.get_sqs_client').start()

    def act(self):
        return lambda_handler(self.get_event(), self.context)

    def test_sort_key_is_not_for_notification(self, log_mock):
        self.path_parameters['sort_key'] = 'EPISODE#123456'
        expected_response = {"message": "Bad Request"}
        expected_status_code = 400

        response = self.act()

        assertApiResponse(self, expected_status_code, expected_response, response)
        log_mock.assert_called_with({'log_reference': LogReference.MANRESENDNOTIF0001, 'sort_key': 'EPISODE#123456'})

    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_notification_not_found(self, get_db_table_mock, log_mock):
        table_mock = Mock()
        get_db_table_mock.return_value = table_mock
        table_mock.get_item.return_value = {'Item': None}
        expected_response = {'body': '{"message": "Notification not found"}',
                             'headers': ANY,
                             'isBase64Encoded': ANY,
                             'statusCode': 404}

        response = self.act()
        self.assertEqual(response, expected_response)
        log_mock.assert_called_with(LogReference.MANRESENDNOTIF0002)

    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_result_notification_missing_record_id(self, get_db_table_mock, log_mock):
        table_mock = Mock()
        get_db_table_mock.return_value = table_mock
        table_mock.get_item.return_value = {'Item': {'type': 'Result letter'}}
        expected_body = {'message': 'Notification is missing attribute "record_id"'}
        expected_status_code = 500

        response = self.act()
        assertApiResponse(self, expected_status_code, expected_body, response)
        log_mock.assert_called_with(LogReference.MANRESENDNOTIF0010)

    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_result_for_notification_is_missing(self, get_db_table_mock, log_mock):
        table_mock = Mock()
        get_db_table_mock.return_value = table_mock
        notification = {'Item': {'type': NotificationType.RESULT, 'record_id': 'result_sort_key'}}
        result = {'Item': {}}
        table_mock.get_item.side_effect = [notification, result]
        expected_body = {'message': 'Unable to find result with sort key result_sort_key'}
        expected_response = {'body': json.dumps(expected_body),
                             'headers': ANY,
                             'isBase64Encoded': False,
                             'statusCode': 400}

        response = self.act()
        self.assertEqual(expected_response, response)
        log_mock.assert_called_with({
            'log_reference': LogReference.MANRESENDNOTIF0011,
            'result_sort_key': 'result_sort_key'})

    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_result_too_old(self, get_db_table_mock, log_mock):
        self.maxDiff = None
        table_mock = Mock()
        get_db_table_mock.return_value = table_mock
        notification = {'Item': {'type': NotificationType.RESULT, 'record_id': 'result_sort_key'}}
        result = {'Item': {'test_date': '2020-01-01'}}
        table_mock.get_item.side_effect = [notification, result]
        expected_body = {'message': "Unable to resend result notification. The result is older than 3 months"}
        expected_status_code = 400

        response = self.act()
        assertApiResponse(self, expected_status_code, expected_body, response)
        log_mock.assert_called_with({
            'log_reference': LogReference.MANRESENDNOTIF0012,
            'result_sort_key': 'result_sort_key',
            'test_date': '2020-01-01'})

    @patch('manual_resend_notification.manual_resend_notification.datetime', DateTimeMock)
    @patch('common.utils.audit_utils.datetime', DateTimeMock)
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_overrides_with_address_in_body(self, get_db_table_mock, log_mock):
        table_mock = Mock()
        self.maxDiff = None
        get_db_table_mock.return_value = table_mock
        self.body = {'address': {
            'address_line_1': 'override_address_1',
            'address_line_2': 'override_address_2',
            'address_line_3': 'override_address_3',
            'address_line_4': 'override_address_4',
            'address_line_5': 'override_address_5',
            'postcode': 'override_postcode'
        }}
        notification = {'Item': {
            'type': NotificationType.RESULT,
            'record_id': 'result_sort_key',
            'created': '2021-01-01',
            'status': NotificationStatus.SENT,
            'participant_id': '123',
            'data': {}
        }}
        result = {'Item': {'test_date': '2021-01-01'}}
        participant = {'Item': {'address': {
            'address_line_1': 'participant_address_1',
            'address_line_2': 'participant_address_2',
            'address_line_3': 'participant_address_3',
            'address_line_4': 'participant_address_4',
            'address_line_5': 'participant_address_5',
            'postcode': 'participant_postcode'
        }, 'nhs_number': '99999999'}}
        table_mock.get_item.side_effect = [notification, result, participant]
        expected_body = {}
        expected_status_code = 200

        response = self.act()
        assertApiResponse(self, expected_status_code, expected_body, response)
        log_mock.assert_called_with(LogReference.MANRESENDNOTIF0009)
        table_mock.put_item.assert_called_with(Item={'type': NotificationType.RESULT,
                                                     'participant_id': '123',
                                                     'sort_key': 'NOTIFICATION#2021-01-01T00:00:00+00:00',
                                                     'record_id': 'result_sort_key',
                                                     'created': '2021-01-01T00:00:00+00:00',
                                                     'status': NotificationStatus.PENDING_PRINT_FILE,
                                                     'data': {'PAT_ADD1': 'override_address_1',
                                                              'PAT_ADD2': 'override_address_2',
                                                              'PAT_ADD3': 'override_address_3',
                                                              'PAT_ADD4': 'override_address_4',
                                                              'PAT_ADD5': 'override_address_5',
                                                              'PAT_POSTCODE': 'override_postcode',
                                                              'DEST_ADD1': 'override_address_1',
                                                              'DEST_ADD2': 'override_address_2',
                                                              'DEST_ADD3': 'override_address_3',
                                                              'DEST_ADD4': 'override_address_4',
                                                              'DEST_ADD5': 'override_address_5',
                                                              'DEST_POSTCODE': 'override_postcode'},
                                                     'live_record_status':  'PENDING_PRINT_FILE',
                                                     'original_send_date': '2021-01-01T00:00:00'})

    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_notification_of_type_not_supported(self, get_db_table_mock, log_mock):
        table_mock = Mock()
        get_db_table_mock.return_value = table_mock
        table_mock.get_item.return_value = {'Item': {
            'type': 'Cease', 'record_id': '1234'
        }}
        expected_body = {"message": "Unsupported letter type for resend \"Cease\""}
        expected_status_code = 400

        response = self.act()
        assertApiResponse(self, expected_status_code, expected_body, response)
        log_mock.assert_called_with({'log_reference': LogReference.MANRESENDNOTIF0005,
                                     'letter_type': 'Cease'})

    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_no_live_episode(self, get_db_table_mock, log_mock):
        def _query_mock(Key):
            if Key['sort_key'].startswith('NOTIFICATION'):
                return {'Item': {'type': 'Invitation letter', 'record_id': 'EPISODE#2021-01-01T00:00:00+00:00'}}
            if Key['sort_key'].startswith('EPISODE'):
                return {'Item': {}}

        self.maxDiff = None
        table_mock = Mock()
        get_db_table_mock.return_value = table_mock
        table_mock.get_item.side_effect = _query_mock
        expected_body = {
            "message": "Notification can not be re-sent. Episode has a live_record_status of \"None\""
        }
        expected_status_code = 400

        response = self.act()
        assertApiResponse(self, expected_status_code, expected_body, response)
        log_mock.assert_called_with({'log_reference': LogReference.MANRESENDNOTIF0006,
                                     'episode_status': None})

    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_live_episode_not_in_INVITED_or_REMINDED_status(self, get_db_table_mock, log_mock):
        def _query_mock(Key):
            if Key['sort_key'].startswith('NOTIFICATION'):
                return {'Item': {'type': 'Invitation letter', 'record_id': 'EPISODE#2021-01-01T00:00:00+00:00'}}
            if Key['sort_key'].startswith('EPISODE'):
                return {'Item': {'live_record_status': EpisodeStatus.WALKIN.value}}

        table_mock = Mock()
        get_db_table_mock.return_value = table_mock
        table_mock.get_item.side_effect = _query_mock
        expected_body = {
            "message": f'Notification can not be re-sent. Episode has a live_record_status of "{EpisodeStatus.WALKIN}"'
        }
        expected_response_code = 400

        response = self.act()
        assertApiResponse(self, expected_response_code, expected_body, response)
        log_mock.assert_called_with({'log_reference': LogReference.MANRESENDNOTIF0006,
                                     'episode_status': EpisodeStatus.WALKIN.value})

    @patch('manual_resend_notification.manual_resend_notification.datetime', DateTimeMock)
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_notification_older_than_12_months(self, get_db_table_mock, log_mock):
        def _query_mock(Key):
            if Key['sort_key'].startswith('NOTIFICATION'):
                return {'Item': {
                    'type': 'Invitation letter',
                    'record_id': 'EPISODE#2019-12-30T00:00:00+00:00',
                    'created': '2019-12-30T00:00:00+00:00'}}
            if Key['sort_key'].startswith('EPISODE'):
                return {'Item': {'live_record_status': EpisodeStatus.INVITED.value}}

        table_mock = Mock()
        get_db_table_mock.return_value = table_mock
        table_mock.get_item.side_effect = _query_mock
        expected_response = {'body': '{"message": "Notification is too old to be re-sent"}',
                             'headers': ANY,
                             'isBase64Encoded': ANY,
                             'statusCode': 400}

        response = self.act()
        self.assertEqual(response, expected_response)
        log_mock.assert_called_with({'log_reference': LogReference.MANRESENDNOTIF0007,
                                     'original_send_date': '2019-12-30T00:00:00+00:00'})

    @patch('manual_resend_notification.manual_resend_notification.datetime', DateTimeMock)
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_origional_sent_date_is_older_than_12_months(self, get_db_table_mock, log_mock):
        def _query_mock(Key):
            if Key['sort_key'].startswith('NOTIFICATION'):
                return {'Item': {
                    'type': 'Invitation letter',
                    'record_id': 'EPISODE#2020-12-30T00:00:00+00:00',
                    'created': '2020-12-30T00:00:00+00:00',
                    'original_send_date': '2019-12-30T00:00:00+00:00', }}
            if Key['sort_key'].startswith('EPISODE'):
                return {'Item': {'live_record_status': EpisodeStatus.INVITED.value}}

        table_mock = Mock()
        get_db_table_mock.return_value = table_mock
        table_mock.get_item.side_effect = _query_mock
        expected_response = {'body': '{"message": "Notification is too old to be re-sent"}',
                             'headers': ANY,
                             'isBase64Encoded': ANY,
                             'statusCode': 400}

        response = self.act()
        self.assertEqual(response, expected_response)
        log_mock.assert_called_with({'log_reference': LogReference.MANRESENDNOTIF0007,
                                     'original_send_date': '2019-12-30T00:00:00+00:00'})


@patch('common.utils.audit_utils.get_internal_id', return_value='123456')
@patch('common.utils.audit_utils.log', Mock())
@patch('manual_resend_notification.manual_resend_notification.log')
@patch('os.environ', mock_env_vars)
class TestManualResendNotificationHappyPath(TestCase):

    def setUp(self):
        self.EVENT = {
            'body': json.dumps({}),
            'pathParameters': {
                'participant_id': '123',
                'sort_key': 'NOTIFICATION#2021-01-01T00:00:00+00:00'
            },
            'requestContext': {
                'authorizer': {
                    'session': json.dumps({'session_id': 'the_session',
                                           'user_data': {
                                               'nhsid_useruid': 'the_user',
                                               'first_name': 'Tom',
                                               'last_name': 'Smith'}})
                }
            }
        }

    def act(self):
        return lambda_handler.__wrapped__(self.EVENT, {})

    @patch('manual_resend_notification.manual_resend_notification.datetime', DateTimeMock)
    @patch('common.utils.audit_utils.datetime', DateTimeMock)
    @patch('common.utils.audit_utils.get_sqs_client')
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_current_notification_in_flight(self, get_ddb_table_mock, get_sqs_client_mock, log_mock, _):
        def _query_mock(Key):
            if Key['sort_key'].startswith('NOTIFICATION'):
                return {'Item': {
                    'participant_id': '123',
                    'sort_key': 'NOTIFICATION#2020-12-30T00:00:00+00:00',
                    'type': 'Invitation letter',
                    'record_id': 'EPISODE#2020-12-30T00:00:00+00:00',
                    'created': '2020-12-30T00:00:00+00:00',
                    'status': 'PENDING_PRINT_FILE',
                    'data': {
                        'PAT_ADD1': 'Flat 1A',
                        'PAT_ADD2': '1 WALESBY LANE COTTAGES',
                        'PAT_ADD3': '',
                        'PAT_ADD4': 'NEWARK',
                        'PAT_ADD5': '',
                        'PAT_FORENAME': 'JOHN',
                        'PAT_OTH_FORE': '',
                        'PAT_POSTCODE': 'NG22 9RD',
                        'PAT_SURNAME': 'WATSON',
                        'PAT_TITLE': 'DR'
                    }}}
            if Key['sort_key'].startswith('EPISODE'):
                return {'Item': {'live_record_status': EpisodeStatus.INVITED.value}}
            if Key['sort_key'].startswith('PARTICIPANT'):
                return {
                    'Item': {
                        'nhs_number': '99999999',
                        'address': {
                            'address_line_1': '221B',
                            'address_line_2': 'Baker Street',
                            'address_line_3': 'London',
                            'postcode': 'NW16XE'
                        }
                    }
                }

        table_mock = Mock()
        get_ddb_table_mock.return_value = table_mock
        table_mock.get_item.side_effect = _query_mock

        sqs_client_mock = Mock()
        get_sqs_client_mock.return_value = sqs_client_mock

        expected_response = {'body': '{}',
                             'headers': ANY,
                             'isBase64Encoded': ANY,
                             'statusCode': 200}

        response = self.act()
        self.assertEqual(response, expected_response)

        table_mock.put_item.assert_called_with(Item={'type': 'Invitation letter',
                                                     'participant_id': '123',
                                                     'sort_key': 'NOTIFICATION#2020-12-30T00:00:00+00:00',
                                                     'record_id': 'EPISODE#2020-12-30T00:00:00+00:00',
                                                     'created': '2020-12-30T00:00:00+00:00',
                                                     'status': 'PENDING_PRINT_FILE',
                                                     'data': {'PAT_ADD1': '221B',
                                                              'PAT_ADD2': 'Baker Street',
                                                              'PAT_ADD3': 'London',
                                                              'PAT_ADD4': '',
                                                              'PAT_ADD5': '',
                                                              'PAT_POSTCODE': 'NW16XE',
                                                              'DEST_ADD1': '221B',
                                                              'DEST_ADD2': 'Baker Street',
                                                              'DEST_ADD3': 'London',
                                                              'DEST_ADD4': '',
                                                              'DEST_ADD5': '',
                                                              'DEST_POSTCODE': 'NW16XE',
                                                              'PAT_FORENAME': 'JOHN',
                                                              'PAT_OTH_FORE': '',
                                                              'PAT_SURNAME': 'WATSON',
                                                              'PAT_TITLE': 'DR'},
                                                     'live_record_status':  'PENDING_PRINT_FILE',
                                                     'original_send_date': '2020-12-30T00:00:00+00:00'})

        self.assertEqual(log_mock.call_args_list, [
            call(LogReference.MANRESENDNOTIF0014),
            call({
                'log_reference': LogReference.MANRESENDNOTIF0015,
                'participant_id': '123',
                'sort_key': 'NOTIFICATION#2021-01-01T00:00:00+00:00'
            }),
            call(LogReference.MANRESENDNOTIF0003),
            call(LogReference.MANRESENDNOTIF0008),
            call(LogReference.MANRESENDNOTIF0009)
        ])

        expected_audit_message = json.dumps({'action': 'RESEND_NOTIFICATION',
                                             'timestamp': '2021-01-01T00:00:00+00:00',
                                             'internal_id': '123456',
                                             'nhsid_useruid': 'the_user',
                                             'session_id': 'the_session',
                                             'first_name': 'Tom',
                                             'last_name': 'Smith',
                                             'participant_ids': ['123'],
                                             'nhs_numbers': ['99999999'],
                                             'additional_information': {'notification_type': 'Invitation letter',
                                                                        'address': {
                                                                            'address_line_1': '221B',
                                                                            'address_line_2': 'Baker Street',
                                                                            'address_line_3': 'London',
                                                                            'postcode': 'NW16XE'
                                                                            }},
                                             }
                                            )
        sqs_client_mock.send_message.assert_called_with(QueueUrl='MY_AUDIT_QUEUE_URL',
                                                        MessageBody=expected_audit_message)

    @patch('manual_resend_notification.manual_resend_notification.datetime', DateTimeMock)
    @patch('common.utils.audit_utils.datetime', DateTimeMock)
    @patch('common.utils.audit_utils.get_sqs_client')
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_current_notification_not_in_flight(self, get_db_table_mock, get_sqs_client_mock, log_mock, _):
        def _query_mock(Key):
            if Key['sort_key'].startswith('NOTIFICATION'):
                return {'Item': {
                    'participant_id': '123',
                    'sort_key': 'NOTIFICATION#2020-12-30T00:00:00+00:00',
                    'type': 'Invitation letter',
                    'record_id': 'EPISODE#2020-12-30T00:00:00+00:00',
                    'created': '2020-12-30T00:00:00+00:00',
                    'status': 'SENT',
                    'data': {
                        'PAT_ADD1': 'Flat 1A',
                        'PAT_ADD2': '1 WALESBY LANE COTTAGES',
                        'PAT_ADD3': '',
                        'PAT_ADD4': 'NEWARK',
                        'PAT_ADD5': '',
                        'PAT_FORENAME': 'JOHN',
                        'PAT_OTH_FORE': '',
                        'PAT_POSTCODE': 'NG22 9RD',
                        'PAT_SURNAME': 'WATSON',
                        'PAT_TITLE': 'DR'
                    }}}
            if Key['sort_key'].startswith('EPISODE'):
                return {'Item': {'live_record_status': EpisodeStatus.REMINDED.value}}
            if Key['sort_key'].startswith('PARTICIPANT'):
                return {
                    'Item': {
                        'nhs_number': '99999999',
                        'address': {
                            'address_line_1': '221B',
                            'address_line_2': 'Baker Street',
                            'address_line_3': 'London',
                            'postcode': 'NW16XE'
                        }
                    }
                }

        table_mock = Mock()
        get_db_table_mock.return_value = table_mock
        table_mock.get_item.side_effect = _query_mock

        sqs_client_mock = Mock()
        get_sqs_client_mock.return_value = sqs_client_mock

        expected_response = {}
        expected_status_code = 200

        response = self.act()
        assertApiResponse(self, expected_status_code, expected_response, response)

        table_mock.put_item.assert_called_with(Item={'type': 'Invitation letter',
                                                     'record_id': 'EPISODE#2020-12-30T00:00:00+00:00',
                                                     'participant_id': '123',
                                                     'sort_key': 'NOTIFICATION#2021-01-01T00:00:00+00:00',
                                                     'created': '2021-01-01T00:00:00+00:00',
                                                     'status': 'PENDING_PRINT_FILE',
                                                     'data': {'PAT_ADD1': '221B',
                                                              'PAT_ADD2': 'Baker Street',
                                                              'PAT_ADD3': 'London',
                                                              'PAT_ADD4': '',
                                                              'PAT_ADD5': '',
                                                              'PAT_POSTCODE': 'NW16XE',
                                                              'DEST_ADD1': '221B',
                                                              'DEST_ADD2': 'Baker Street',
                                                              'DEST_ADD3': 'London',
                                                              'DEST_ADD4': '',
                                                              'DEST_ADD5': '',
                                                              'DEST_POSTCODE': 'NW16XE',
                                                              'PAT_FORENAME': 'JOHN',
                                                              'PAT_OTH_FORE': '',
                                                              'PAT_SURNAME': 'WATSON',
                                                              'PAT_TITLE': 'DR'},
                                                     'live_record_status':  'PENDING_PRINT_FILE',
                                                     'original_send_date': '2020-12-30T00:00:00+00:00'})

        self.assertEqual(log_mock.call_args_list, [
            call(LogReference.MANRESENDNOTIF0014),
            call({
                'log_reference': LogReference.MANRESENDNOTIF0015,
                'participant_id': '123',
                'sort_key': 'NOTIFICATION#2021-01-01T00:00:00+00:00'
            }),
            call(LogReference.MANRESENDNOTIF0003),
            call(LogReference.MANRESENDNOTIF0008),
            call(LogReference.MANRESENDNOTIF0009)
        ])

        expected_audit_message = json.dumps({'action': 'RESEND_NOTIFICATION',
                                             'timestamp': '2021-01-01T00:00:00+00:00',
                                             'internal_id': '123456',
                                             'nhsid_useruid': 'the_user',
                                             'session_id': 'the_session',
                                             'first_name': 'Tom',
                                             'last_name': 'Smith',
                                             'participant_ids': ['123'],
                                             'nhs_numbers': ['99999999'],
                                             'additional_information': {'notification_type': 'Invitation letter',
                                                                        'address': {
                                                                            'address_line_1': '221B',
                                                                            'address_line_2': 'Baker Street',
                                                                            'address_line_3': 'London',
                                                                            'postcode': 'NW16XE'}}
                                             })
        sqs_client_mock.send_message.assert_called_with(QueueUrl='MY_AUDIT_QUEUE_URL',
                                                        MessageBody=expected_audit_message)
