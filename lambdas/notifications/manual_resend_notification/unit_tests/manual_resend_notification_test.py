from unittest import TestCase
from datetime import datetime
from mock import patch, Mock
from manual_resend_notification.manual_resend_notification import (
    update_notification_with_address, create_notification_record)


class DateTimeMock(datetime):
    @classmethod
    def now(cls, tz):
        return cls(2021, 1, 1, 0, 0, 0, 0)


mock_env_vars = {
    'RESULT_LETTER_CUT_OFF_MONTHS': '3'
}
cut_off_mock = Mock()


@patch('os.environ', mock_env_vars)
@patch('resend_notifications.resend_notifications.result_is_older_than_cut_off', cut_off_mock)
class TestUpdateNotificationWithParticipantAddress(TestCase):

    def test_update_notification_with_address(self):
        notification = {'data': {
            'PAT_ADD1': 'Flat 1A',
            'PAT_ADD2': '1 WALESBY LANE COTTAGES',
            'PAT_ADD3': '',
            'PAT_ADD4': 'NEWARK',
            'PAT_ADD5': '',
            'PAT_FORENAME': 'JONAH',
            'PAT_OTH_FORE': '',
            'PAT_POSTCODE': 'NG22 9RD',
            'PAT_SURNAME': 'RAHMAN',
            'PAT_TITLE': 'MR'
        }}

        address = {
            'address_line_1': '221B',
            'address_line_2': 'Baker Street',
            'address_line_3': 'London',
            'address_line_4': 'address_4',
            'address_line_5': 'address_5',
            'postcode': 'NW16XE'
        }

        update_notification_with_address(notification, address)

        self.assertEqual(notification, {
            'data': {
                'PAT_ADD1': '221B',
                'PAT_ADD2': 'Baker Street',
                'PAT_ADD3': 'London',
                'PAT_ADD4': 'address_4',
                'PAT_ADD5': 'address_5',
                'PAT_POSTCODE': 'NW16XE',
                'DEST_ADD1': '221B',
                'DEST_ADD2': 'Baker Street',
                'DEST_ADD3': 'London',
                'DEST_ADD4': 'address_4',
                'DEST_ADD5': 'address_5',
                'DEST_POSTCODE': 'NW16XE',
                'PAT_FORENAME': 'JONAH',
                'PAT_OTH_FORE': '',
                'PAT_SURNAME': 'RAHMAN',
                'PAT_TITLE': 'MR'
            }
        })


@patch('os.environ', mock_env_vars)
@patch('resend_notifications.resend_notifications.result_is_older_than_cut_off', cut_off_mock)
@patch('manual_resend_notification.manual_resend_notification.log', Mock())
class TestCreateNotificationRecord(TestCase):

    address = {
        'address_line_1': '221B',
        'address_line_2': 'Baker Street',
        'address_line_3': 'London',
        'postcode': 'NW16XE'
    }

    @patch('manual_resend_notification.manual_resend_notification.datetime', DateTimeMock)
    @patch('manual_resend_notification.manual_resend_notification.create_replace_record_in_participant_table')
    def test_create_notification_record_overwrite_existing_PENDING_PRINT_FILE_notification(
            self, create_replace_record_in_participant_table_mock):

        notification_to_resend = {'participant_id': '123456',
                                  'sort_key': 'NOTIFICATION#2020-11-09T10:41:22.953',
                                  'status': 'PENDING_PRINT_FILE',
                                  'created': '2020-11-09T10:41:22.953',
                                  'data': {'k': 'v'}}

        original_send_date = datetime(2020, 12, 1, 0, 0, 0, 0)
        expected_new_notification = {'participant_id': '123456',
                                     'sort_key': 'NOTIFICATION#2020-11-09T10:41:22.953',
                                     'status': 'PENDING_PRINT_FILE',
                                     'created': '2020-11-09T10:41:22.953',
                                     'live_record_status': 'PENDING_PRINT_FILE',
                                     'original_send_date': '2020-12-01T00:00:00',
                                     'data': {
                                         'k': 'v',
                                         'PAT_ADD1': '221B',
                                         'PAT_ADD2': 'Baker Street',
                                         'PAT_ADD3': 'London',
                                         'PAT_ADD4': '',
                                         'PAT_ADD5': '',
                                         'PAT_POSTCODE': 'NW16XE',
                                         'DEST_ADD1': '221B',
                                         'DEST_ADD2': 'Baker Street',
                                         'DEST_ADD3': 'London',
                                         'DEST_ADD4': '',
                                         'DEST_ADD5': '',
                                         'DEST_POSTCODE': 'NW16XE'}}

        create_notification_record(notification_to_resend, original_send_date, self.address)

        create_replace_record_in_participant_table_mock.assert_called_with(expected_new_notification)

    @patch('manual_resend_notification.manual_resend_notification.datetime', DateTimeMock)
    @patch('manual_resend_notification.manual_resend_notification.create_replace_record_in_participant_table')
    def test_create_notification_record_creates_new_notification_if_status_not_PENDING_PRINT_FILE(
            self, create_replace_record_in_participant_table_mock):

        notification_to_resend = {'participant_id': '123456',
                                  'sort_key': 'NOTIFICATION#2020-11-09T10:41:22.953',
                                  'status': 'SENT',
                                  'created': '2020-11-09T10:41:22.953',
                                  'data': {'k': 'v'}}

        original_send_date = datetime(2020, 12, 1, 0, 0, 0, 0)
        expected_new_notification = {'participant_id': '123456',
                                     'sort_key': 'NOTIFICATION#2021-01-01T00:00:00',
                                     'status': 'PENDING_PRINT_FILE',
                                     'created': '2021-01-01T00:00:00',
                                     'live_record_status': 'PENDING_PRINT_FILE',
                                     'original_send_date': '2020-12-01T00:00:00',
                                     'data': {
                                         'k': 'v',
                                         'PAT_ADD1': '221B',
                                         'PAT_ADD2': 'Baker Street',
                                         'PAT_ADD3': 'London',
                                         'PAT_ADD4': '',
                                         'PAT_ADD5': '',
                                         'PAT_POSTCODE': 'NW16XE',
                                         'DEST_ADD1': '221B',
                                         'DEST_ADD2': 'Baker Street',
                                         'DEST_ADD3': 'London',
                                         'DEST_ADD4': '',
                                         'DEST_ADD5': '',
                                         'DEST_POSTCODE': 'NW16XE'}}

        create_notification_record(notification_to_resend, original_send_date, self.address)

        create_replace_record_in_participant_table_mock.assert_called_with(expected_new_notification)
