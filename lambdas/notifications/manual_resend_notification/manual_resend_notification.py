import os
from datetime import datetime, timezone
from urllib.parse import unquote
from dateutil.relativedelta import relativedelta
from common.utils import json_return_message, json_return_object
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from manual_resend_notification.log_references import LogReference
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.participant_utils import (
    get_record_from_participant_table, get_participant_by_participant_id, create_replace_record_in_participant_table)
from common.models.letters import NotificationType, NotificationStatus
from common.models.participant import ParticipantSortKey, EpisodeStatus
from common.utils.audit_utils import audit, AuditActions
from common.utils.session_utils import get_session_from_lambda_event
from common.utils.result_letter_utils import result_is_older_than_cut_off


@lambda_entry_point
def lambda_handler(event, context):
    notification_handlers = {
        NotificationType.INVITATION: handle_invitation,
        NotificationType.REMINDER: handle_invitation,
        NotificationType.RESULT: handle_result,
    }

    event_data, *_ = get_api_request_info_from_event(event)
    participant_id = event_data['path_parameters']['participant_id']
    sort_key = unquote(event_data['path_parameters']['sort_key'])
    body = event_data.get('body')

    log(LogReference.MANRESENDNOTIF0014)

    if not sort_key.startswith(ParticipantSortKey.NOTIFICATION):
        log({'log_reference': LogReference.MANRESENDNOTIF0001, 'sort_key': sort_key})
        return json_return_message(400, 'Bad Request')

    log({'log_reference': LogReference.MANRESENDNOTIF0015, 'participant_id': participant_id, 'sort_key': sort_key})

    notification = get_record_from_participant_table(participant_id=participant_id, sort_key=sort_key)
    if not notification:
        log(LogReference.MANRESENDNOTIF0002)
        return json_return_message(404, 'Notification not found')

    if 'record_id' not in notification:
        log(LogReference.MANRESENDNOTIF0010)
        return json_return_message(500, 'Notification is missing attribute "record_id"')

    notification_type = notification.get('type')
    handler = notification_handlers.get(notification_type)

    if not handler:
        log({'log_reference': LogReference.MANRESENDNOTIF0005, 'letter_type': notification_type})
        return json_return_message(400, f'Unsupported letter type for resend "{notification_type}"')

    error_message = handler(participant_id, notification)

    if error_message:
        return json_return_message(400, error_message)

    log(LogReference.MANRESENDNOTIF0008)

    participant = get_participant_by_participant_id(participant_id)
    if not participant:
        log({'log_reference': LogReference.MANRESENDNOTIF0005, 'letter_type': notification_type})
        return json_return_message(400, f'Unable to get participant with id {participant_id}')

    address = notification_type == NotificationType.RESULT and body.get('address') or participant.get('address')

    if not address:
        log(LogReference.MANRESENDNOTIF0013)
        return json_return_message(400, 'Missing address in request body and participant record')

    create_notification_record(
        original_notification_record=notification,
        original_send_date=datetime.fromisoformat(notification.get('original_send_date') or notification['created']),
        send_address=address)

    crm = body.get('crm', {})
    additional_information_for_audit = {**{'notification_type': notification['type'], 'address': address},
                                        **{k: v for k, v in crm.items() if k in ['crm_number', 'comments']}}

    audit(
        action=AuditActions.RESEND_NOTIFICATION,
        session=get_session_from_lambda_event(event),
        participant_ids=[participant_id],
        nhs_numbers=[participant['nhs_number']],
        additional_information=additional_information_for_audit
    )
    return json_return_object(200)


def handle_result(participant_id, notification):
    record_id = notification['record_id']
    log(LogReference.MANRESENDNOTIF0004)

    result = get_record_from_participant_table(participant_id=participant_id, sort_key=record_id)
    if not result:
        log({'log_reference': LogReference.MANRESENDNOTIF0011, 'result_sort_key': record_id})
        return f'Unable to find result with sort key {record_id}'

    test_date = datetime.strptime(result['test_date'], '%Y-%m-%d').date()
    if result_is_older_than_cut_off(test_date):
        log({
            'log_reference': LogReference.MANRESENDNOTIF0012,
            'result_sort_key': record_id,
            'test_date': result['test_date']
        })
        cut_off = os.environ.get('RESULT_LETTER_CUT_OFF_MONTHS')
        return "".join([
            'Unable to resend result notification. ',
            f'The result is older than {cut_off} months'
        ])


def handle_invitation(participant_id, notification):
    log(LogReference.MANRESENDNOTIF0003)
    episode = get_record_from_participant_table(participant_id=participant_id, sort_key=notification['record_id'])
    live_record_status = episode.get('live_record_status')
    if not live_record_status \
            or live_record_status not in [EpisodeStatus.INVITED.value, EpisodeStatus.REMINDED.value]:
        log({'log_reference': LogReference.MANRESENDNOTIF0006, 'episode_status': live_record_status})
        return f'Notification can not be re-sent. Episode has a live_record_status of "{live_record_status}"'

    original_send_date = datetime.fromisoformat(notification.get('original_send_date') or notification['created'])
    if datetime.now(timezone.utc) > original_send_date + relativedelta(months=12):
        log({'log_reference': LogReference.MANRESENDNOTIF0007, 'original_send_date': original_send_date.isoformat()})
        return 'Notification is too old to be re-sent'


def create_notification_record(original_notification_record, original_send_date, send_address):
    if original_notification_record['status'] == NotificationStatus.PENDING_PRINT_FILE:
        timestamp = original_notification_record['created']
    else:
        timestamp = datetime.now(timezone.utc).isoformat()

    notification = {}

    notification.update(original_notification_record)

    notification.update({
        'sort_key': f'{ParticipantSortKey.NOTIFICATION}#{timestamp}',
        'created': timestamp,
        'status': NotificationStatus.PENDING_PRINT_FILE,
        'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
        'original_send_date': original_send_date.isoformat()
    })

    update_notification_with_address(notification, send_address)

    create_replace_record_in_participant_table(notification)

    log(LogReference.MANRESENDNOTIF0009)


def update_notification_with_address(notification, address):
    notification['data'].update({
        'PAT_ADD1': address.get('address_line_1', ''),
        'PAT_ADD2': address.get('address_line_2', ''),
        'PAT_ADD3': address.get('address_line_3', ''),
        'PAT_ADD4': address.get('address_line_4', ''),
        'PAT_ADD5': address.get('address_line_5', ''),
        'PAT_POSTCODE': address.get('postcode', ''),
        'DEST_ADD1': address.get('address_line_1', ''),
        'DEST_ADD2': address.get('address_line_2', ''),
        'DEST_ADD3': address.get('address_line_3', ''),
        'DEST_ADD4': address.get('address_line_4', ''),
        'DEST_ADD5': address.get('address_line_5', ''),
        'DEST_POSTCODE': address.get('postcode', ''),
    })
