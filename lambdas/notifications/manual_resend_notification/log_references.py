import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    MANRESENDNOTIF0001 = (logging.WARNING, 'Illegal request the sort key is not for notification.')
    MANRESENDNOTIF0002 = (logging.WARNING, 'Notification record not found.')
    MANRESENDNOTIF0003 = (logging.INFO, 'Received a request to resend an invitation/reminder letter.')
    MANRESENDNOTIF0004 = (logging.INFO, 'Received a request to resend a result letter.')
    MANRESENDNOTIF0005 = (logging.WARNING, 'Unsupported letter type for resend.')
    MANRESENDNOTIF0006 = (logging.WARNING, 'Episode has wrong status.')
    MANRESENDNOTIF0007 = (logging.WARNING, 'Notification is too old to be resent.')
    MANRESENDNOTIF0008 = (logging.INFO, 'Valid request and valid notification, proceeding to create the resend.')
    MANRESENDNOTIF0009 = (logging.INFO, 'Successfully created the resend notification.')
    MANRESENDNOTIF0010 = (logging.INFO, 'Notification is missing attribute "record_id"')
    MANRESENDNOTIF0011 = (logging.INFO, 'Unable to find result with sort key')
    MANRESENDNOTIF0012 = (logging.INFO, 'Unable to resend result notification. The result is older than 3 months')
    MANRESENDNOTIF0013 = (
        logging.WARNING, 'Missing address in request body and participant record. Not updating address fields')
    MANRESENDNOTIF0014 = (logging.INFO, 'Received a request to resend a notification')
    MANRESENDNOTIF0015 = (logging.INFO, 'Fetching notification from dynamodb')
