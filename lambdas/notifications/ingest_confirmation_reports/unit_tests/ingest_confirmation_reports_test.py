from unittest import TestCase
from mock import patch, Mock, MagicMock, call
from ingest_confirmation_reports.log_references import LogReference
from datetime import datetime, timezone

mock_env_vars = {
    'CONFIRMATION_REPORTS_BUCKET': 'CONFIRMATION_REPORTS_BUCKET',
    'SFTP_SECRET_KEY': 'SFTP_SECRET_KEY',
    'SFTP_OUT_DIR': 'SFTP_OUT_DIR',
    'ENVIRONMENT_NAME': 'ENVIRONMENT_NAME'
}
datetime_mock = Mock(wraps=datetime)
example_date = datetime(2018, 4, 12, 10, 0, 0, 123456, tzinfo=timezone.utc)

module = 'ingest_confirmation_reports.ingest_confirmation_reports'


@patch('ingest_confirmation_reports.ingest_confirmation_reports.datetime', datetime_mock)
class TestIngestConfirmationReports(TestCase):

    mock_event_any = {
        "schedule": "* * * * ? *",
        "timezone": "Europe/London"
    }

    @patch('os.environ', mock_env_vars)
    def setUp(self, *args):
        import ingest_confirmation_reports.ingest_confirmation_reports as _ingest_confirmation_reports
        self.ingest_confirmation_reports_module = _ingest_confirmation_reports
        self.unwrapped_handler = self.ingest_confirmation_reports_module.lambda_handler.__wrapped__
        datetime_mock.now.return_value = example_date

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.put_object')
    @patch(f'{module}.create_sftp_client')
    @patch(f'{module}.get_sftp_secrets')
    @patch(f'{module}.check_schedule')
    def test_schedule_fail(self, schedule_mock, secrets_mock, create_client_mock, put_mock, audit_mock, log_mock):
        schedule_mock.return_value = False

        self.unwrapped_handler(self.mock_event_any, {})

        schedule_mock.assert_called_once_with("* * * * ? *", "Europe/London")
        secrets_mock.assert_not_called()
        create_client_mock.assert_not_called()
        put_mock.assert_not_called()
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.INGESTCR0001, 'schedule': '* * * * ? *', 'timezone': 'Europe/London'}),
            call({'log_reference': LogReference.INGESTCR0002}),
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.put_object')
    @patch(f'{module}.create_sftp_client')
    @patch(f'{module}.get_sftp_secrets')
    @patch(f'{module}.check_schedule')
    def test_secrets_fail(self, schedule_mock, secrets_mock, create_client_mock, put_mock, audit_mock, log_mock):
        schedule_mock.return_value = True
        secrets_mock.side_effect = Exception("wah")

        with self.assertRaises(Exception) as e:
            self.unwrapped_handler(self.mock_event_any, {})
            self.assertEquals(e.message, 'wah')

        schedule_mock.assert_called_once_with("* * * * ? *", "Europe/London")
        secrets_mock.called_times(3)
        create_client_mock.assert_not_called()
        put_mock.assert_not_called()
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.INGESTCR0001, 'schedule': '* * * * ? *', 'timezone': 'Europe/London'}),
            call({'log_reference': LogReference.INGESTCR0003, 'folder': 'ENVIRONMENT_NAME/12-04-2018/'}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 0}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 0}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 1}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 1}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 2}),
            call({'log_reference': LogReference.INGESTCREX0003, 'add_exception_info': True}),
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.put_object')
    @patch(f'{module}.create_sftp_client')
    @patch(f'{module}.get_sftp_secrets')
    @patch(f'{module}.check_schedule')
    def test_sftp_fail(self, schedule_mock, secrets_mock, create_client_mock, put_mock, audit_mock, log_mock):
        schedule_mock.return_value = True
        secrets_mock.return_value = ('a', 'b', 'c')
        create_client_mock.side_effect = Exception("blah")

        with self.assertRaises(Exception) as e:
            self.unwrapped_handler(self.mock_event_any, {})
            self.assertEquals(e.message, 'blah')

        schedule_mock.assert_called_once_with("* * * * ? *", "Europe/London")
        secrets_mock.called_times(3)
        create_client_mock.called_times(3)
        put_mock.assert_not_called()
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.INGESTCR0001, 'schedule': '* * * * ? *', 'timezone': 'Europe/London'}),
            call({'log_reference': LogReference.INGESTCR0003, 'folder': 'ENVIRONMENT_NAME/12-04-2018/'}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 0}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 0}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 1}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 1}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 2}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCREX0003, 'add_exception_info': True}),
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.put_object')
    @patch(f'{module}.create_sftp_client')
    @patch(f'{module}.get_sftp_secrets')
    @patch(f'{module}.check_schedule')
    def test_put_fail(self, schedule_mock, secrets_mock, create_client_mock, put_mock, audit_mock, log_mock):
        schedule_mock.return_value = True
        secrets_mock.return_value = ('a', 'b', 'c')
        client_mock = MagicMock()
        create_client_mock.return_value = client_mock
        client_mock.listdir.return_value = ['a']
        put_mock.side_effect = Exception("dah")

        with self.assertRaises(Exception) as e:
            self.unwrapped_handler(self.mock_event_any, {})
            self.assertEquals(e.message, 'dah')

        schedule_mock.assert_called_once_with("* * * * ? *", "Europe/London")
        secrets_mock.called_times(3)
        create_client_mock.called_times(3)
        put_mock.called_times(3)
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.INGESTCR0001, 'schedule': '* * * * ? *', 'timezone': 'Europe/London'}),
            call({'log_reference': LogReference.INGESTCR0003, 'folder': 'ENVIRONMENT_NAME/12-04-2018/'}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 0}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCR0006, 'out_dir': 'SFTP_OUT_DIR'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCREX0001, 'add_exception_info': True}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 0}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 1}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCR0006, 'out_dir': 'SFTP_OUT_DIR'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCREX0001, 'add_exception_info': True}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 1}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 2}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCR0006, 'out_dir': 'SFTP_OUT_DIR'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCREX0001, 'add_exception_info': True}),
            call({'log_reference': LogReference.INGESTCREX0003, 'add_exception_info': True}),
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.put_object')
    @patch(f'{module}.create_sftp_client')
    @patch(f'{module}.get_sftp_secrets')
    @patch(f'{module}.check_schedule')
    def test_remove_fail(self, schedule_mock, secrets_mock, create_client_mock, put_mock, audit_mock, log_mock):
        schedule_mock.return_value = True
        secrets_mock.return_value = ('a', 'b', 'c')
        client_mock = MagicMock()
        create_client_mock.return_value = client_mock
        client_mock.listdir.return_value = ['a']
        client_mock.remove.side_effect = Exception("gah")

        with self.assertRaises(Exception) as e:
            self.unwrapped_handler(self.mock_event_any, {})
            self.assertEquals(e.message, 'gah')

        schedule_mock.assert_called_once_with("* * * * ? *", "Europe/London")
        secrets_mock.called_times(3)
        create_client_mock.called_times(3)
        put_mock.called_times(3)
        client_mock.remove.called_times(3)
        audit_mock.called_times(3)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.INGESTCR0001, 'schedule': '* * * * ? *', 'timezone': 'Europe/London'}),
            call({'log_reference': LogReference.INGESTCR0003, 'folder': 'ENVIRONMENT_NAME/12-04-2018/'}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 0}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCR0006, 'out_dir': 'SFTP_OUT_DIR'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCR0008}),
            call({'log_reference': LogReference.INGESTCR0009, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCREX0002, 'add_exception_info': True}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 0}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 1}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCR0006, 'out_dir': 'SFTP_OUT_DIR'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCR0008}),
            call({'log_reference': LogReference.INGESTCR0009, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCREX0002, 'add_exception_info': True}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 1}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 2}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCR0006, 'out_dir': 'SFTP_OUT_DIR'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCR0008}),
            call({'log_reference': LogReference.INGESTCR0009, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCREX0002, 'add_exception_info': True}),
            call({'log_reference': LogReference.INGESTCREX0003, 'add_exception_info': True}),
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.put_object')
    @patch(f'{module}.create_sftp_client')
    @patch(f'{module}.get_sftp_secrets')
    @patch(f'{module}.check_schedule')
    def test_happy_path(self, schedule_mock, secrets_mock, create_client_mock, put_mock, audit_mock, log_mock):
        schedule_mock.return_value = True
        secrets_mock.return_value = ('a', 'b', 'c')
        client_mock = MagicMock()
        create_client_mock.return_value = client_mock
        client_mock.open.__enter__.return_value = MagicMock()
        client_mock.listdir.return_value = ['a']

        self.unwrapped_handler(self.mock_event_any, {})

        schedule_mock.assert_called_once_with("* * * * ? *", "Europe/London")
        secrets_mock.called_once()
        create_client_mock.called_once()
        put_mock.assert_called_once()
        audit_mock.called_times(2)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.INGESTCR0001, 'schedule': '* * * * ? *', 'timezone': 'Europe/London'}),
            call({'log_reference': LogReference.INGESTCR0003, 'folder': 'ENVIRONMENT_NAME/12-04-2018/'}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 0}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCR0006, 'out_dir': 'SFTP_OUT_DIR'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCR0008}),
            call({'log_reference': LogReference.INGESTCR0009, 'file': 'a'}),
            call({'log_reference': LogReference.INGESTCR0010})
        ])
