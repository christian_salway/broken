from unittest import TestCase
from datetime import datetime, timezone
from mock import patch, Mock, MagicMock, call
from ingest_confirmation_reports.log_references import LogReference

datetime_mock = Mock(wraps=datetime)
example_date = datetime(2020, 1, 1, 10, 0, 0, 123456, tzinfo=timezone.utc)


@patch('ingest_confirmation_reports.ingest_confirmation_reports.datetime', datetime_mock)
@patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
@patch('os.environ', {'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL'})
class TestIngestConfirmationReports(TestCase):

    FIXED_NOW_TIME = datetime(2020, 1, 1, tzinfo=timezone.utc)
    mock_event = {
        "schedule": "* * * * ? *",
        "timezone": "Europe/London"
    }

    mock_env_vars = {
        'CONFIRMATION_REPORTS_BUCKET': 'CONFIRMATION_REPORTS_BUCKET',
        'SFTP_SECRET_KEY': 'SFTP_SECRET_KEY',
        'SFTP_OUT_DIR': 'SFTP_OUT_DIR',
        'ENVIRONMENT_NAME': 'ENVIRONMENT_NAME'
    }

    @classmethod
    @patch('boto3.client')
    @patch('boto3.resource')
    @patch('boto3.session.Session')
    @patch('os.environ', mock_env_vars)
    def setUpClass(cls, boto3_session, boto3_resource, boto3_client, *args):
        cls.log_patcher = patch('common.log.logger')

        # Setup Secrets Manager Mock
        secret_client_mock = Mock()
        secret_session_mock = Mock()
        secret_session_mock.client.return_value = secret_client_mock
        boto3_session.return_value = secret_session_mock
        cls.secret_client_mock = secret_client_mock

        # Setup database mocks
        boto3_table_mock = Mock()
        organisation_supplemental_table_mock = Mock()
        boto3_table_mock.Table.return_value = organisation_supplemental_table_mock
        boto3_resource.return_value = boto3_table_mock
        cls.organisation_supplemental_table_mock = organisation_supplemental_table_mock

        # Setup Client Mocks
        cls.s3_client_mock = Mock()
        cls.sqs_client_mock = Mock()

        def client_mocks(client):
            if client == 'sqs':
                return cls.sqs_client_mock
            elif client == 's3':
                return cls.s3_client_mock

        boto3_client.side_effect = lambda *args, **kwargs: client_mocks(args[0])

        # Setup lambda module
        import ingest_confirmation_reports.ingest_confirmation_reports as _ingest_confirmation_reports
        global ingest_confirmation_reports_module
        ingest_confirmation_reports_module = _ingest_confirmation_reports

        import common.utils.audit_utils as _audit
        global audit_module
        audit_module = _audit
        cls.audit_date_time_patcher = patch.object(audit_module, 'datetime', Mock(wraps=datetime))

    def setUp(self):
        self.organisation_supplemental_table_mock.reset_mock()
        self.s3_client_mock.reset_mock()
        self.sqs_client_mock.reset_mock()
        self.log_patcher.start()
        self.audit_date_time_patcher.start()
        datetime_mock.now.return_value = example_date

    def tearDown(self):
        self.log_patcher.stop()
        self.audit_date_time_patcher.stop()

    @patch('ingest_confirmation_reports.ingest_confirmation_reports.datetime', datetime_mock)
    @patch('common.utils.audit_utils.datetime', datetime_mock)
    @patch('ingest_confirmation_reports.ingest_confirmation_reports.log')
    @patch('ingest_confirmation_reports.ingest_confirmation_reports.create_sftp_client')
    def test_successful_send(self, create_sftp_mock, log_mock):
        context = Mock()
        context.function_name = ''
        sftp_client_mock = MagicMock()
        create_sftp_mock.return_value = sftp_client_mock
        sftp_client_mock.listdir.return_value = [
            'abcd.txt',
            'blah.txt'
        ]
        abcd_file = Mock()
        blah_file = Mock()
        sftp_client_mock.open.__enter__.side_effect = [
            abcd_file,
            blah_file
        ]
        self.secret_client_mock.get_secret_value.return_value = {
            'ARN': 'string',
            'Name': 'string',
            'VersionId': 'string',
            'SecretString': '{"private-key": "test_private_key", "host": "test_host", "user_id": "test_user_id"}',
            'VersionStages': [
                'string',
            ],
            'CreatedDate': datetime(2015, 1, 1, tzinfo=timezone.utc)
        }
        ingest_confirmation_reports_module.lambda_handler(self.mock_event, context)

        sftp_client_mock.open.assert_any_call('abcd.txt', bufsize=32768)
        sftp_client_mock.open.assert_any_call('blah.txt', bufsize=32768)
        self.s3_client_mock.put_object.assert_has_calls([
            call(
                Bucket='CONFIRMATION_REPORTS_BUCKET',
                Key='ENVIRONMENT_NAME/01-01-2020/abcd.txt',
                Metadata={},
                Body=sftp_client_mock.open().__enter__()
            ),
            call(
                Bucket='CONFIRMATION_REPORTS_BUCKET',
                Key='ENVIRONMENT_NAME/01-01-2020/blah.txt',
                Metadata={},
                Body=sftp_client_mock.open().__enter__()
            )
        ])

        # Asserts the audit record has been sent to the audit queue
        self.sqs_client_mock.send_message.assert_has_calls([
            call(
                    QueueUrl='AUDIT_QUEUE_URL',
                    MessageBody='{"action": "DOWNLOAD_CONFIRMATION_REPORT", '
                    + '"timestamp": "2020-01-01T10:00:00.123456+00:00", "internal_id": "1", '
                    + '"nhsid_useruid": "SYSTEM", "session_id": "NONE", "file_name": "abcd.txt"}'
                ),
            call(
                QueueUrl='AUDIT_QUEUE_URL',
                MessageBody='{"action": "DOWNLOAD_CONFIRMATION_REPORT", '
                + '"timestamp": "2020-01-01T10:00:00.123456+00:00", "internal_id": "1", '
                + '"nhsid_useruid": "SYSTEM", "session_id": "NONE", "file_name": "blah.txt"}'
            ),
            call(
                QueueUrl='AUDIT_QUEUE_URL',
                MessageBody='{"action": "DELETE_CONFIRMATION_REPORT", '
                + '"timestamp": "2020-01-01T10:00:00.123456+00:00", "internal_id": "1", '
                + '"nhsid_useruid": "SYSTEM", "session_id": "NONE", "file_name": "abcd.txt"}'
            ),
            call(
                QueueUrl='AUDIT_QUEUE_URL',
                MessageBody='{"action": "DELETE_CONFIRMATION_REPORT", '
                + '"timestamp": "2020-01-01T10:00:00.123456+00:00", "internal_id": "1", '
                + '"nhsid_useruid": "SYSTEM", "session_id": "NONE", "file_name": "blah.txt"}'
            )
        ])

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.INGESTCR0001, 'schedule': '* * * * ? *', 'timezone': 'Europe/London'}),
            call({'log_reference': LogReference.INGESTCR0003, 'folder': 'ENVIRONMENT_NAME/01-01-2020/'}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 0}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCR0006, 'out_dir': 'SFTP_OUT_DIR'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'abcd.txt'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'blah.txt'}),
            call({'log_reference': LogReference.INGESTCR0008}),
            call({'log_reference': LogReference.INGESTCR0009, 'file': 'abcd.txt'}),
            call({'log_reference': LogReference.INGESTCR0009, 'file': 'blah.txt'}),
            call({'log_reference': LogReference.INGESTCR0010}),
        ])

    @patch('ingest_confirmation_reports.ingest_confirmation_reports.datetime', datetime_mock)
    @patch('common.utils.audit_utils.datetime', datetime_mock)
    @patch('ingest_confirmation_reports.ingest_confirmation_reports.create_sftp_client')
    @patch('ingest_confirmation_reports.ingest_confirmation_reports.log')
    def test_successful_send_on_retry(self, log_mock, create_sftp_mock):
        context = Mock()
        context.function_name = ''
        sftp_client_mock = MagicMock()
        create_sftp_mock.side_effect = [Exception('Failure making SFTP connection'), sftp_client_mock]
        sftp_client_mock.listdir.return_value = [
            'wads.txt',
            'fxsd.txt'
        ]
        abcd_file = Mock()
        blah_file = Mock()
        sftp_client_mock.open.__enter__.side_effect = [
            abcd_file,
            blah_file
        ]
        self.secret_client_mock.get_secret_value.return_value = {
            'ARN': 'string',
            'Name': 'string',
            'VersionId': 'string',
            'SecretString': '{"private-key": "test_private_key", "host": "test_host", "user_id": "test_user_id"}',
            'VersionStages': [
                'string',
            ],
            'CreatedDate': datetime(2015, 1, 1, tzinfo=timezone.utc)
        }
        ingest_confirmation_reports_module.lambda_handler(self.mock_event, context)

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.INGESTCR0001, 'schedule': '* * * * ? *', 'timezone': 'Europe/London'}),
            call({'log_reference': LogReference.INGESTCR0003, 'folder': 'ENVIRONMENT_NAME/01-01-2020/'}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 0}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 0}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 1}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCR0006, 'out_dir': 'SFTP_OUT_DIR'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'wads.txt'}),
            call({'log_reference': LogReference.INGESTCR0007, 'file': 'fxsd.txt'}),
            call({'log_reference': LogReference.INGESTCR0008}),
            call({'log_reference': LogReference.INGESTCR0009, 'file': 'wads.txt'}),
            call({'log_reference': LogReference.INGESTCR0009, 'file': 'fxsd.txt'}),
            call({'log_reference': LogReference.INGESTCR0010}),
        ])

        sftp_client_mock.open.assert_any_call('wads.txt', bufsize=32768)
        sftp_client_mock.open.assert_any_call('fxsd.txt', bufsize=32768)
        self.s3_client_mock.put_object.assert_has_calls([
            call(
                Bucket='CONFIRMATION_REPORTS_BUCKET',
                Key='ENVIRONMENT_NAME/01-01-2020/wads.txt',
                Metadata={},
                Body=sftp_client_mock.open().__enter__()
            ),
            call(
                Bucket='CONFIRMATION_REPORTS_BUCKET',
                Key='ENVIRONMENT_NAME/01-01-2020/fxsd.txt',
                Metadata={},
                Body=sftp_client_mock.open().__enter__()
            )
        ])
        self.assertEqual(create_sftp_mock.call_count, 2)
        # Asserts the audit record has been sent to the audit queue
        self.sqs_client_mock.send_message.assert_has_calls([
            call(
                    QueueUrl='AUDIT_QUEUE_URL',
                    MessageBody='{"action": "DOWNLOAD_CONFIRMATION_REPORT", '
                    + '"timestamp": "2020-01-01T10:00:00.123456+00:00", "internal_id": "1", '
                    + '"nhsid_useruid": "SYSTEM", "session_id": "NONE", "file_name": "wads.txt"}'
                ),
            call(
                QueueUrl='AUDIT_QUEUE_URL',
                MessageBody='{"action": "DOWNLOAD_CONFIRMATION_REPORT", '
                + '"timestamp": "2020-01-01T10:00:00.123456+00:00", "internal_id": "1", '
                + '"nhsid_useruid": "SYSTEM", "session_id": "NONE", "file_name": "fxsd.txt"}'
            ),
            call(
                QueueUrl='AUDIT_QUEUE_URL',
                MessageBody='{"action": "DELETE_CONFIRMATION_REPORT", '
                + '"timestamp": "2020-01-01T10:00:00.123456+00:00", "internal_id": "1", '
                + '"nhsid_useruid": "SYSTEM", "session_id": "NONE", "file_name": "wads.txt"}'
            ),
            call(
                QueueUrl='AUDIT_QUEUE_URL',
                MessageBody='{"action": "DELETE_CONFIRMATION_REPORT", '
                + '"timestamp": "2020-01-01T10:00:00.123456+00:00", "internal_id": "1", '
                + '"nhsid_useruid": "SYSTEM", "session_id": "NONE", "file_name": "fxsd.txt"}'
            )
        ])

    @patch('ingest_confirmation_reports.ingest_confirmation_reports.create_sftp_client')
    @patch('ingest_confirmation_reports.ingest_confirmation_reports.log')
    def test_failed_send(self, log_mock, create_sftp_mock):
        context = Mock()
        context.function_name = ''
        create_sftp_mock.side_effect = Exception('Failure making SFTP connection')
        self.secret_client_mock.get_secret_value.return_value = {
            'ARN': 'string',
            'Name': 'string',
            'VersionId': 'string',
            'SecretString': '{"private-key": "test_private_key", "host": "test_host", "user_id": "test_user_id"}',
            'VersionStages': [
                'string',
            ],
            'CreatedDate': datetime(2015, 1, 1, tzinfo=timezone.utc)
        }
        with self.assertRaises(Exception):
            ingest_confirmation_reports_module.lambda_handler(self.mock_event, context)
        self.assertEqual(create_sftp_mock.call_count, 3)

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.INGESTCR0001, 'schedule': '* * * * ? *', 'timezone': 'Europe/London'}),
            call({'log_reference': LogReference.INGESTCR0003, 'folder': 'ENVIRONMENT_NAME/01-01-2020/'}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 0}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 0}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 1}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True, 'attempt_number': 1}),
            call({'log_reference': LogReference.INGESTCR0004, 'attempt': 2}),
            call({'log_reference': LogReference.INGESTCR0005}),
            call({'log_reference': LogReference.INGESTCREX0003, 'add_exception_info': True})
        ])
