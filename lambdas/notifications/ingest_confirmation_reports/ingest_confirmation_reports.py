import os
from datetime import datetime, timezone
from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.sftp_utils import get_sftp_secrets, create_sftp_client
from common.utils.s3_utils import put_object
from common.utils.schedule_utils import check_schedule
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from .log_references import LogReference

CONFIRMATION_REPORTS_BUCKET = os.environ.get('CONFIRMATION_REPORTS_BUCKET')
SFTP_SECRET_KEY = os.environ.get('SFTP_SECRET_KEY')
SFTP_OUT_DIR = os.environ.get('SFTP_OUT_DIR')
ENVIRONMENT_NAME = os.environ.get('ENVIRONMENT_NAME')
BUFFER_SIZE = 32768
TRIES = 3


@lambda_entry_point
def lambda_handler(event, context):
    schedule = event.get('schedule')
    schedule_timezone = event.get('timezone')

    log({'log_reference': LogReference.INGESTCR0001, 'schedule': schedule, 'timezone': schedule_timezone})
    if not check_schedule(schedule, schedule_timezone):
        log({'log_reference': LogReference.INGESTCR0002})
        return

    folder_name = ENVIRONMENT_NAME + '/' + datetime.now(timezone.utc).strftime('%d-%m-%Y') + '/'
    log({'log_reference': LogReference.INGESTCR0003, 'folder': folder_name})

    for attempt in range(TRIES):
        try:
            log({'log_reference': LogReference.INGESTCR0004, 'attempt': attempt})
            private_key, host, user_id = get_sftp_secrets(SFTP_SECRET_KEY)
            log({'log_reference': LogReference.INGESTCR0005})
            sftp_client = create_sftp_client(private_key, host, user_id)

            log({'log_reference': LogReference.INGESTCR0006, 'out_dir': SFTP_OUT_DIR})
            sftp_client.chdir(SFTP_OUT_DIR)
            file_list = sftp_client.listdir(path='.')

            try:
                for file_name in file_list:
                    # Open a connection to the SFTP file and move the file to our S3 bucket
                    log({'log_reference': LogReference.INGESTCR0007, 'file': file_name})
                    with sftp_client.open(file_name, bufsize=BUFFER_SIZE) as sftp_file:
                        move_file_to_s3(sftp_file, folder_name, file_name)
                    audit(AuditActions.DOWNLOAD_CONFIRMATION_REPORT, user=AuditUsers.SYSTEM, file_name=file_name)
            except Exception as e:
                log({'log_reference': LogReference.INGESTCREX0001, 'add_exception_info': True})
                raise e
            log({'log_reference': LogReference.INGESTCR0008})
            try:
                for file_name in file_list:
                    log({'log_reference': LogReference.INGESTCR0009, 'file': file_name})
                    sftp_client.remove('./' + file_name)
                    audit(AuditActions.DELETE_CONFIRMATION_REPORT, user=AuditUsers.SYSTEM, file_name=file_name)
            except Exception as e:
                log({'log_reference': LogReference.INGESTCREX0002, 'add_exception_info': True})
                raise e
        except Exception as e:
            if attempt < (TRIES-1):
                log({'log_reference': LogReference.INGESTCRWARN0001, 'add_exception_info': True,
                     'attempt_number': attempt})
                continue
            else:
                log({'log_reference': LogReference.INGESTCREX0003, 'add_exception_info': True})
                raise e
        log({'log_reference': LogReference.INGESTCR0010})
        break


def move_file_to_s3(file_object, folder_name, file_name):
    file_object.prefetch()
    put_object(CONFIRMATION_REPORTS_BUCKET, file_object, folder_name + file_name)
