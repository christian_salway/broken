import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    INGESTCR0001 = (logging.INFO, 'Checking schedule expression')
    INGESTCR0002 = (logging.INFO, 'Schedule expression does not match, finishing')
    INGESTCR0003 = (logging.INFO, 'Established folder to add reports to')
    INGESTCR0004 = (logging.INFO, 'Attempting to connect to sftp server')
    INGESTCR0005 = (logging.INFO, 'retrieved sftp credentials')
    INGESTCR0006 = (logging.INFO, 'Generating file list')
    INGESTCR0007 = (logging.INFO, 'Moving file to confirmation reports bucket')
    INGESTCR0008 = (logging.INFO, 'All files moved. Beginning file deletion.')
    INGESTCR0009 = (logging.INFO, 'Deleting file from sftp server')
    INGESTCR0010 = (logging.INFO, 'All files deleted from sftp server')

    INGESTCRWARN0001 = (logging.WARNING, 'An exception was encountered, retrying as below retry threshold')

    INGESTCREX0001 = (logging.ERROR, 'Exception encountered during downloading sftp file')
    INGESTCREX0002 = (logging.ERROR, 'Exception encountered during deleting sftp file')
    INGESTCREX0003 = (logging.ERROR, 'An exception was encountered and the maximum retry amount has been met, moving record to failed bucket')  
