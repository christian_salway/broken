# Create Notification

## Function

This lambda receives messages from the `create-reminder` and `create-invite` lambdas. This is to reduce the amount of processing done in a single lambda for the print process. Using notification records to store print data means we can get all necessary print data in a single query further down the print process
 
In order to send messages to this lambda, you must go through the `notifications-to-create` queue. An example input data structure can be found below

Note that `notification_sequence` is no longer used when creating notification records. This will be generated later when the notification records are read from the dynamodb table and merged into a singular print file.

## Example input data

```
{
  "participant_id": "1234",
  "type": "Invitation"
}
```

As it stands, the letter type this is either `Invitation` or `Reminder`. But will be extended to include result letters in milestone 3

See [here](https://nhsd-confluence.digital.nhs.uk/display/CSP/Notification+record) for an example of the output notification record with descriptions of each field, and see [here](https://nhsd-confluence.digital.nhs.uk/display/CSP/Sending+files+to+CIC+for+printing+V2) (specifically the last entry on the page) for a more in depth description of the print data stored in the `data` field

## Tables used

This lambda uses both the participant table and the letters config table. 
The participant's `registered_gp_practice_code` is used to find the correct letter config - if none is found it then uses the default.