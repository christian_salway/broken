import json
from datetime import datetime, timezone
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.dynamodb_access.operations import dynamodb_get_item
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_utils import (
    get_participant_by_participant_id
)
from common.utils.sqs_utils import read_message_from_queue
from common.utils.participant_utils import create_replace_record_in_participant_table
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.utils.organisation_directory_utils import \
    get_organisation_information_by_organisation_code
from create_notification.log_references import LogReference
from common.log import log
from common.models.participant import ParticipantSortKey
from common.models.letters import PrintDestination, JobType, NotificationStatus, NotificationType

letter_to_scenario_map = {
    NotificationType.INVITATION: 'Invite',
    NotificationType.REMINDER: 'Reminder',
    NotificationType.CEASE: 'Ceased',
    NotificationType.RESULT: 'Result'
}

UNREGISTERED_GP_PRACTICE_CODE = 'default'


@lambda_entry_point
def lambda_handler(event, context):
    sqs_message = json.loads(read_message_from_queue(event))
    letter_type = sqs_message['type']
    template = sqs_message['template']

    participant_id = sqs_message['participant_id']
    log({'log_reference': LogReference.CRENOTIF0001, 'participant_id': participant_id})
    participant_record = get_participant_by_participant_id(participant_id)

    if not participant_record:
        log({'log_reference': LogReference.CRENOTIFEX0001})
        raise Exception(LogReference.CRENOTIFEX0001.message)

    tailored_letter_text = get_tailored_letter_text_by_gp_practice_code(
        participant_record.get('registered_gp_practice_code', UNREGISTERED_GP_PRACTICE_CODE))

    log({'log_reference': LogReference.CRENOTIF0007})
    prioritised_address = get_prioritised_address(sqs_message, participant_record)

    sort_key = sqs_message.get('sort_key')
    if not sort_key:
        sort_key = participant_record['sort_key']

    create_notification_record(
        participant_record, sort_key, tailored_letter_text,
        prioritised_address, letter_type, template)

    audit(
        action=AuditActions.NOTIFICATION_CREATED,
        user=AuditUsers.LETTERS,
        participant_ids=[participant_id],
        additional_information={
            'notification_type': letter_type,
            'address': prioritised_address})

    log({'log_reference': LogReference.CRENOTIF0006})


def get_letter_text_by_gp_practice_code(gp_practice_code):
    return dynamodb_get_item(TableNames.LETTERS_CONFIG, key={'gp_practice_code': gp_practice_code})


def get_prioritised_address(sqs_message, participant):
    sqs_address = sqs_message.get('address')
    if sqs_address:
        log({'log_reference': LogReference.CRENOTIF0008})
        return sqs_address
    if derive_print_destination(participant.get('nhais_cipher')) != PrintDestination.CIC:
        log({'log_reference': LogReference.CRENOTIF0013})
        organisation = get_organisation_information_by_organisation_code(
            participant.get('registered_gp_practice_code', UNREGISTERED_GP_PRACTICE_CODE)
        )
        return organisation['address']
    log({'log_reference': LogReference.CRENOTIF0010})
    return participant.get('address')


def derive_print_destination(nhais_cipher):
    if nhais_cipher == 'IM':
        return PrintDestination.IOM
    elif nhais_cipher == 'DMS':
        return PrintDestination.DMS
    else:
        return PrintDestination.CIC


def get_tailored_letter_text_by_gp_practice_code(gp_practice_code):
    log({'log_reference': LogReference.CRENOTIF0011, 'gp_practice_code': gp_practice_code})

    letter_text = get_letter_text_by_gp_practice_code(gp_practice_code)

    if not letter_text and gp_practice_code != UNREGISTERED_GP_PRACTICE_CODE:
        log({'log_reference': LogReference.CRENOTIF0012})
        letter_text = get_letter_text_by_gp_practice_code(UNREGISTERED_GP_PRACTICE_CODE)

    if not letter_text:
        log({'log_reference': LogReference.CRENOTIFEX0004})
        raise Exception(LogReference.CRENOTIFEX0004.message)

    return letter_text


def create_notification_record(
        participant_record, sort_key, tailored_letter_text,
        prioritised_address, letter_type, template):
    log({'log_reference': LogReference.CRENOTIF0004})
    timestamp = datetime.now(timezone.utc).isoformat()

    notification = {
        'participant_id': participant_record['participant_id'],
        'sort_key': f'{ParticipantSortKey.NOTIFICATION}#{timestamp}',
        'created': timestamp,
        'status': NotificationStatus.PENDING_PRINT_FILE,
        'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
        'type': letter_type,
        'print_destination': derive_print_destination(participant_record.get('nhais_cipher')),
        'record_id': sort_key,
        'data': generate_notification_data(letter_type, participant_record, tailored_letter_text,
                                           prioritised_address, template)
    }
    create_replace_record_in_participant_table(notification)


def generate_notification_data(letter_type, participant, tailored_letter_text, prioritised_address, template):
    log({'log_reference': LogReference.CRENOTIF0005})

    # Data here is left blank if it is either not needed, or we do not know what it is

    job_id = {
        NotificationType.INVITATION: JobType.INVITE_REMINDER,
        NotificationType.REMINDER: JobType.INVITE_REMINDER,
        NotificationType.CEASE: JobType.RESULT_CEASE,
        NotificationType.RESULT: JobType.RESULT_CEASE
    }

    data = {
        'TEMPLATE': template,
        'JOBID': job_id[letter_type],
        'JOBNO': '',
        'RECNO': '',
        'LETTCODE': template,
        'AJRUNDATE': datetime.now(timezone.utc).date().strftime("%d.%m.%y"),
        'GP_PART_CODE': '',
        'LETT_TYPE': '',
        'RECALL_TYPE': participant['status'],
        'RECALL_STATUS': '',
        'DHA': '',
        'SENDER': '',
        'ORIG_TYPE': '',
        'DEST_TYPE': '',
        'REASON1': '',
        'REASON2': '',
        'REASON3': '',
        'REASON4': '',
        'REASON5': '',
        'PAT_TITLE': participant.get('title', ''),
        'PAT_SURNAME': participant.get('last_name', ''),
        'PAT_FORENAME': participant.get('first_name', ''),
        'PAT_OTH_FORE': '',
        'PAT_PREV_SURN': '',
        'PAT_DOB': '',
        'PAT_LTEST_DATE': '',
        'PAT_NHS': participant.get('nhs_number', ''),
        'PAT_REC_DATE': '',
        'PAT_ADD1': prioritised_address.get('address_line_1', ''),
        'PAT_ADD2': prioritised_address.get('address_line_2', ''),
        'PAT_ADD3': prioritised_address.get('address_line_3', ''),
        'PAT_ADD4': prioritised_address.get('address_line_4', ''),
        'PAT_ADD5': prioritised_address.get('address_line_5', ''),
        'PAT_POSTCODE': prioritised_address.get('postcode', ''),
        'HA_CIPHER': 'EU-WEST-2',
        'HA_NAME': '',
        'PRINT_DATE': '',
        'ORIG_NAME': '',
        'ORIG_ADD1': '',
        'ORIG_ADD2': '',
        'ORIG_ADD3': '',
        'ORIG_ADD4': '',
        'ORIG_ADD5': '',
        'ORIG_POSTCODE': '',
        'ORIG_NAT_CODE': '',
        'ORIG_LOC_CODE': '',
        'ORIG_TEL_NO': '',
        'DEST_NAME': '',
        'DEST_ADD1': prioritised_address.get('address_line_1', ''),
        'DEST_ADD2': prioritised_address.get('address_line_2', ''),
        'DEST_ADD3': prioritised_address.get('address_line_3', ''),
        'DEST_ADD4': prioritised_address.get('address_line_4', ''),
        'DEST_ADD5': prioritised_address.get('address_line_5', ''),
        'DEST_POSTCODE': prioritised_address.get('postcode', ''),
        'DEST_NAT_CODE': '',
        'DEST_LOC_CODE': '',
        'GP_NAME': '',
        'GP_ADD1': '',
        'GP_ADD2': '',
        'GP_ADD3': '',
        'GP_ADD4': '',
        'GP_ADD5': '',
        'GP_POSTCODE': '',
        'GP_NAT_CODE': '',
        'GP_LOC_CODE': '',
        'GP_TEL_NO': '',
        'GP_TEXT1': tailored_letter_text.get('gp_text1', ''),
        'GP_TEXT2': tailored_letter_text.get('gp_text2', ''),
        'GP_TEXT3': tailored_letter_text.get('gp_text3', ''),
        'GP_TEXT4': tailored_letter_text.get('gp_text4', ''),
        'GP_TEXT5': tailored_letter_text.get('gp_text5', ''),
        'HMR_REQ': '',
        'PAT_SLIDE_NO': '',
        'LAB_NAT_CODE': '',
        'M_2_NON_NEGS': '',
        'LAST_TEST_DATE': '',
        'LAST_TEST_RESULT': '',
        'LAST_TEST_RECALL_TYPE': '',
        'PRIOR_TEST_DATE': '',
        'PRIOR_TEST_RESULT': '',
        'PRIOR_TEST_RECALL_TYPE': '',
        'INV1_TEXT1': '',
        'INV1_TEXT2': '',
        'INV1_TEXT3': '',
        'INV1_TEXT4': '',
        'INV1_TEXT5': '',
        'INV2_TEXT1': '',
        'INV2_TEXT2': '',
        'INV2_TEXT3': '',
        'INV2_TEXT4': '',
        'INV2_TEXT5': '',
        'RES1_TEXT1': '',
        'RES1_TEXT2': '',
        'RES1_TEXT3': '',
        'RES1_TEXT4': '',
        'RES1_TEXT5': '',
        'RES2_TEXT1': '',
        'RES2_TEXT2': '',
        'RES2_TEXT3': '',
        'RES2_TEXT4': '',
        'RES2_TEXT5': ''
    }
    if letter_type.upper() in ['INVITATION LETTER', 'REMINDER LETTER']:
        data.update({
            'INV1_TEXT1': tailored_letter_text.get('inv1_text1', ''),
            'INV1_TEXT2': tailored_letter_text.get('inv1_text2', ''),
            'INV1_TEXT3': tailored_letter_text.get('inv1_text3', ''),
            'INV1_TEXT4': tailored_letter_text.get('inv1_text4', ''),
            'INV1_TEXT5': tailored_letter_text.get('inv1_text5', ''),
            'INV2_TEXT1': tailored_letter_text.get('inv2_text1', ''),
            'INV2_TEXT2': tailored_letter_text.get('inv2_text2', ''),
            'INV2_TEXT3': tailored_letter_text.get('inv2_text3', ''),
            'INV2_TEXT4': tailored_letter_text.get('inv2_text4', ''),
            'INV2_TEXT5': tailored_letter_text.get('inv2_text5', '')
        })
    if letter_type.upper() == 'RESULT LETTER':
        data.update({
            'RES1_TEXT1': tailored_letter_text.get('res1_text1', ''),
            'RES1_TEXT2': tailored_letter_text.get('res1_text2', ''),
            'RES1_TEXT3': tailored_letter_text.get('res1_text3', ''),
            'RES1_TEXT4': tailored_letter_text.get('res1_text4', ''),
            'RES1_TEXT5': tailored_letter_text.get('res1_text5', ''),
            'RES2_TEXT1': tailored_letter_text.get('res2_text1', ''),
            'RES2_TEXT2': tailored_letter_text.get('res2_text2', ''),
            'RES2_TEXT3': tailored_letter_text.get('res2_text3', ''),
            'RES2_TEXT4': tailored_letter_text.get('res2_text4', ''),
            'RES2_TEXT5': tailored_letter_text.get('res2_text5', ''),
        })
    return data
