import json
from unittest import TestCase
from mock import patch, Mock
from datetime import datetime, timezone
from common.test_resources.letter_files import (
    expected_invitation_notification_record,
    expected_reminder_notification_record,
    expected_cease_notification_record,
    expected_result_notification_record,
    sample_participant_record,
    sample_letter_config
)
from common.test_mocks.mock_events import sqs_mock_event


sqs_client_mock = Mock()


@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
class TestCreateNotification(TestCase):
    mock_env_vars = {
        'LETTER_QUEUE_URL': 'LETTER_QUEUE_URL',
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
        'DYNAMODB_LETTERS_CONFIG': 'DYNAMODB_LETTERS_CONFIG',
        'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS'
    }
    participant_record = sample_participant_record

    example_date = datetime(2020, 1, 23, 13, 48, 8, 123456, tzinfo=timezone.utc)
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.today.return_value = example_date
    datetime_mock.now.return_value = example_date

    @classmethod
    @patch('datetime.datetime', datetime_mock)
    @patch('boto3.client')
    @patch('boto3.resource')
    @patch('os.environ', mock_env_vars)
    def setUpClass(cls, boto3_resource, boto3_client):
        cls.log_patcher = patch('common.log.log')
        cls.env_patcher = patch('os.environ', cls.mock_env_vars)

        cls.participants_table_mock = Mock()
        cls.letter_config_table_mock = Mock()

        def database_table_mocks(table_name):
            if table_name == 'DYNAMODB_PARTICIPANTS':
                return cls.participants_table_mock

            elif table_name == 'DYNAMODB_LETTERS_CONFIG':
                return cls.letter_config_table_mock

        cls.letter_config_table_mock = Mock()
        cls.letter_config_table_mock.get_item.return_value = {'Item': sample_letter_config}

        boto3_table_mock = Mock()
        boto3_table_mock.Table.side_effect = database_table_mocks
        boto3_resource.return_value = boto3_table_mock
        cls.participants_table_mock.get_item.return_value = {'Item': cls.participant_record}

        cls.sqs_client_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: cls.sqs_client_mock if args and args[0] == 'sqs' else Mock()
        cls.sqs_client_mock = sqs_client_mock

        cls.invitation_mock_event = sqs_mock_event({
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-01-01',
            'type': 'Invitation letter',
            'template': 'Test template',
            'internal_id': 'test internal id'
        })
        cls.reminder_mock_event = sqs_mock_event({
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-01-01',
            'type': 'Reminder letter',
            'template': 'Test template',
            'internal_id': 'test internal id'
        })
        cls.cease_mock_event = sqs_mock_event({
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-01-01',
            'type': 'Cease letter',
            'template': 'Test template',
            'internal_id': 'test internal id'
        })
        cls.result_mock_event = sqs_mock_event({
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-01-01',
            'type': 'Result letter',
            'template': 'Test template',
            'internal_id': 'test internal id'
        })
        # Setup lambda module
        import create_notification.create_notification as _create_notification
        global create_notification_module
        create_notification_module = _create_notification

    def setUp(self):
        self.participants_table_mock.reset_mock()
        self.letter_config_table_mock.reset_mock()
        self.sqs_client_mock.reset_mock()
        self.log_patcher.start()
        self.env_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    def test_create_invitation_notification(self):
        # Setup the participant
        context = Mock()
        context.function_name = ''
        AUDIT_BODY = {
            'action': 'NOTIFICATION_CREATED',
            'timestamp': '2020-01-23T13:48:08.123456+00:00',
            'internal_id': 'test internal id',
            'nhsid_useruid': 'LETTERS',
            'session_id': 'NONE',
            'participant_ids': ['123456'],
            'additional_information': {
                'notification_type': 'Invitation letter',
                'address': {
                    'address_line_1': 'test_address_line_1',
                    'address_line_2': 'test_address_line_2',
                    'address_line_3': 'test_address_line_3',
                    'address_line_4': 'test_address_line_4',
                    'address_line_5': 'test_address_line_5',
                    'postcode': 'test_postcode'
                }
            }
        }
        create_notification_module.lambda_handler(self.invitation_mock_event, context)

        self.participants_table_mock.put_item.assert_called_with(Item=expected_invitation_notification_record)
        self.sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody=json.dumps(AUDIT_BODY))

    def test_create_reminder_notification(self):
        # Setup the participant
        context = Mock()
        context.function_name = ''
        AUDIT_BODY = {
            'action': 'NOTIFICATION_CREATED',
            'timestamp': '2020-01-23T13:48:08.123456+00:00',
            'internal_id': 'test internal id',
            'nhsid_useruid': 'LETTERS',
            'session_id': 'NONE',
            'participant_ids': ['123456'],
            'additional_information': {
                'notification_type': 'Reminder letter',
                'address': {
                    'address_line_1': 'test_address_line_1',
                    'address_line_2': 'test_address_line_2',
                    'address_line_3': 'test_address_line_3',
                    'address_line_4': 'test_address_line_4',
                    'address_line_5': 'test_address_line_5',
                    'postcode': 'test_postcode'
                }
            }
        }
        create_notification_module.lambda_handler(self.reminder_mock_event, context)

        self.participants_table_mock.put_item.assert_called_with(Item=expected_reminder_notification_record)
        self.sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody=json.dumps(AUDIT_BODY))

    def test_create_ceased_notification(self):
        # Setup the participant
        context = Mock()
        context.function_name = ''
        AUDIT_BODY = {
            'action': 'NOTIFICATION_CREATED',
            'timestamp': '2020-01-23T13:48:08.123456+00:00',
            'internal_id': 'test internal id',
            'nhsid_useruid': 'LETTERS',
            'session_id': 'NONE',
            'participant_ids': ['123456'],
            'additional_information': {
                'notification_type': 'Cease letter',
                'address': {
                    'address_line_1': 'test_address_line_1',
                    'address_line_2': 'test_address_line_2',
                    'address_line_3': 'test_address_line_3',
                    'address_line_4': 'test_address_line_4',
                    'address_line_5': 'test_address_line_5',
                    'postcode': 'test_postcode'
                }
            }
        }
        create_notification_module.lambda_handler(self.cease_mock_event, context)

        self.sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody=json.dumps(AUDIT_BODY))
        self.participants_table_mock.put_item.assert_called_with(Item=expected_cease_notification_record)

    def test_create_result_notification(self):
        # Setup the participant
        context = Mock()
        context.function_name = ''
        AUDIT_BODY = {
            'action': 'NOTIFICATION_CREATED',
            'timestamp': '2020-01-23T13:48:08.123456+00:00',
            'internal_id': 'test internal id',
            'nhsid_useruid': 'LETTERS',
            'session_id': 'NONE',
            'participant_ids': ['123456'],
            'additional_information': {
                'notification_type': 'Result letter',
                'address': {
                    'address_line_1': 'test_address_line_1',
                    'address_line_2': 'test_address_line_2',
                    'address_line_3': 'test_address_line_3',
                    'address_line_4': 'test_address_line_4',
                    'address_line_5': 'test_address_line_5',
                    'postcode': 'test_postcode'
                }
            }
        }
        create_notification_module.lambda_handler(self.result_mock_event, context)

        self.sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody=json.dumps(AUDIT_BODY))
        self.participants_table_mock.put_item.assert_called_with(Item=expected_result_notification_record)
