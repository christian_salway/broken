import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    CRENOTIF0001 = (logging.INFO, 'Fetching participant record')
    CRENOTIF0004 = (logging.INFO, 'Generating notification record')
    CRENOTIF0005 = (logging.INFO, 'Populating print file data')
    CRENOTIF0006 = (logging.INFO, 'Notification record created successfully')
    CRENOTIF0007 = (logging.INFO, 'Getting prioritised address')
    CRENOTIF0008 = (logging.INFO, 'Using address provided in sqs event')
    CRENOTIF0010 = (logging.INFO, 'Using participant record address')
    CRENOTIF0011 = (logging.INFO, 'Fetching record from letters config table using gp_practice_code')
    CRENOTIF0012 = (logging.INFO, 'No letter config for participant gp_practice_code found. '
                                  'Fetching default config instead.')
    CRENOTIF0013 = (logging.INFO, 'Retrieving DMS/IOM address information from ODS/cache')

    CRENOTIFEX0001 = (logging.ERROR, 'Could not find participant with provided participant_id')
    CRENOTIFEX0004 = (logging.ERROR, 'Could not find any letter config including default')
