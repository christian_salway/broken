from unittest import TestCase
from mock import patch, Mock, call
from datetime import datetime, timezone
from common.test_resources.letter_files import (
    expected_invitation_notification_record,
    expected_invitation_notification_record_with_episode_address,
    sample_participant_record,
    sample_episode_address,
    sample_letter_config
)
from common.test_mocks.mock_events import sqs_mock_event
from common.models.letters import NotificationStatus, PrintDestination
from create_notification.log_references import LogReference

module = 'create_notification.create_notification'


@patch(f'{module}.log')
class TestCreateNotification(TestCase):

    mock_env_vars = {
        'PARTICIPANTS_TABLE_NAME': 'DYNAMODB_PARTICIPANTS'
    }

    FIXED_NOW_TIME = datetime(2020, 1, 23, 13, 48, 8, 123456, tzinfo=timezone.utc)

    @classmethod
    @patch('boto3.client', Mock())
    @patch('boto3.resource', Mock())
    def setUpClass(cls):
        cls.log_patcher = patch('common.log.logger')
        cls.env_patcher = patch('os.environ', cls.mock_env_vars)
        import create_notification.create_notification as _create_notification
        import common.utils.audit_utils as _audit_utils
        global create_notification_module, audit_utils
        create_notification_module = _create_notification
        audit_utils = _audit_utils

        cls.context = Mock()

        cls.mock_event = sqs_mock_event({
            'participant_id': '123456',
            'sort_key': 'EPISODE#2020-01-01',
            'type': 'Invitation letter',
            'template': 'Test template',
            'address':  {
                'address_line_1': 'address line 1',
                'address_line_2': 'address line 2',
                'address_line_3': 'address line 3',
                'address_line_4': 'address line 4',
                'address_line_5': 'address line 5',
                'postcode': 'post code',
            }
        })
        cls.date_time_patcher = patch.object(create_notification_module, 'datetime', Mock(wraps=datetime))

    def setUp(self):
        self.date_time_patcher.start()
        self.log_patcher.start()
        self.env_patcher.start()
        self.context = Mock()
        self.context.function_name = ''

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()
        self.date_time_patcher.start()

    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_tailored_letter_text_by_gp_practice_code')
    @patch(f'{module}.create_notification_record')
    @patch('os.environ', mock_env_vars)
    def test_lambda_handler(self, get_participant_mock, get_letter_config_mock, create_notification_mock, log_mock):
        create_notification_module.lambda_handler(self.mock_event, self.context)

    @patch(f'{module}.get_participant_by_participant_id')
    @patch('os.environ', mock_env_vars)
    def test_lambda_handler_no_participant(self, get_participant_mock, log_mock):
        log_mock.return_value = None
        get_participant_mock.return_value = None

        self.assertRaises(Exception, create_notification_module.lambda_handler, self.mock_event, self.context)

    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_tailored_letter_text_by_gp_practice_code')
    @patch('os.environ', mock_env_vars)
    def test_lambda_handler_catches_get_config_error(self, get_participant_mock, get_letter_config_mock, log_mock):
        get_letter_config_mock.return_value = Exception

        self.assertRaises(Exception, create_notification_module.lambda_handler, self.mock_event, self.context)

    @patch(f'{module}.get_letter_text_by_gp_practice_code')
    def test_get_tailored_letter_text_by_gp_practice_code_given_config_for_code_exists(self, get_config_mock, log_mock):
        letter_config = {'config_key_1': 'config_value_1', 'config_key_2': 'config_value_2'}
        get_config_mock.return_value = letter_config
        actual_response = create_notification_module.get_tailored_letter_text_by_gp_practice_code('a code')
        get_config_mock.assert_called_once_with('a code')
        self.assertDictEqual(letter_config, actual_response)
        log_mock.assert_has_calls([call({'log_reference': LogReference.CRENOTIF0011, 'gp_practice_code': 'a code'})])

    @patch(f'{module}.get_letter_text_by_gp_practice_code')
    def test_get_tailored_letter_text_by_gp_practice_code_given_no_config_for_code_exists(
            self, get_config_mock, log_mock):
        default_letter_config = {'config_key_1': 'config_value_1', 'config_key_2': 'config_value_2'}
        get_config_mock.side_effect = [None, default_letter_config]
        actual_response = create_notification_module.get_tailored_letter_text_by_gp_practice_code('a code')
        get_config_mock.assert_has_calls([call('a code'), call('default')])
        self.assertDictEqual(default_letter_config, actual_response)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.CRENOTIF0011, 'gp_practice_code': 'a code'}),
            call({'log_reference': LogReference.CRENOTIF0012})
        ])

    @patch(f'{module}.get_letter_text_by_gp_practice_code')
    def test_get_tailored_letter_text_by_gp_practice_code_given_no_default_config_exists(
            self, get_config_mock, log_mock):
        get_config_mock.return_value = None

        with self.assertRaises(Exception) as context:
            create_notification_module.get_tailored_letter_text_by_gp_practice_code('default')

        self.assertEqual('Could not find any letter config including default', context.exception.args[0])
        get_config_mock.assert_called_once_with('default')
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.CRENOTIF0011, 'gp_practice_code': 'default'}),
            call({'log_reference': LogReference.CRENOTIFEX0004})
        ])

    def test_get_prioritised_address_returns_event_address(self, log_mock):
        record_data = {
            'address': sample_episode_address
        }
        actual_address = create_notification_module.get_prioritised_address(record_data, {})
        self.assertEqual(actual_address, sample_episode_address)
        log_mock.assert_has_calls([call({'log_reference': LogReference.CRENOTIF0008})])

    def test_get_prioritised_address_returns_participant_address(self, log_mock):
        actual_address = create_notification_module.get_prioritised_address({}, sample_participant_record)
        self.assertEqual(actual_address, sample_participant_record['address'])
        log_mock.assert_has_calls([call({'log_reference': LogReference.CRENOTIF0010})])

    @patch(f'{module}.get_organisation_information_by_organisation_code')
    @patch(f'{module}.derive_print_destination')
    def test_get_prioritised_address_gets_DMS_address(self, destination_mock, organisation_mock, log_mock):
        expected_address = {
            'address_line_1': 'a',
            'postcode': 'b'
        }
        organisation_mock.return_value = {'address': expected_address}
        destination_mock.return_value = PrintDestination.DMS
        actual_address = create_notification_module.get_prioritised_address(
            {}, {'registered_gp_practice_code': 'abcde', 'nhais_cipher': 'BAA'}
        )
        organisation_mock.assert_called_once_with('abcde')
        destination_mock.assert_called_once_with('BAA')
        self.assertEqual(actual_address, expected_address)
        log_mock.assert_called_once_with({'log_reference': LogReference.CRENOTIF0013})

    @patch(f'{module}.derive_print_destination')
    @patch(f'{module}.generate_notification_data')
    @patch(f'{module}.create_replace_record_in_participant_table')
    def test_create_notification_record(
            self, create_replace_record_in_participant_table_mock, generate_notification_data_mock,
            derive_print_destination_mock, log_mock):
        episode_address = {}
        participant_record = {'participant_id': 'test_id', 'sort_key': 'PARTICIPANT',
                              'registered_gp_practice_code': 'A91114',
                              'nhais_cipher': 'BAA'}
        letters_config_record = {'gp_practice_code': 'default'}
        letter_type = 'Invitation'
        create_notification_module.datetime.now.return_value = self.FIXED_NOW_TIME
        template = 'Test template'
        derive_print_destination_mock.return_value = 'CIC'
        generate_notification_data_mock.return_value = {'notification_test_data': 'value'}
        create_notification_module.create_notification_record(
            participant_record, 'RESULT#', letters_config_record,
            episode_address, letter_type, template)

        derive_print_destination_mock.assert_called_once_with('BAA')
        generate_notification_data_mock.assert_called_once_with(letter_type, participant_record, letters_config_record,
                                                                {}, template)
        create_replace_record_in_participant_table_mock.assert_called_with({
            'participant_id': 'test_id',
            'sort_key': 'NOTIFICATION#2020-01-23T13:48:08.123456+00:00',
            'created': '2020-01-23T13:48:08.123456+00:00',
            'status': NotificationStatus.PENDING_PRINT_FILE,
            'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
            'type': 'Invitation',
            'print_destination': 'CIC',
            'record_id': 'RESULT#',
            'data': {'notification_test_data': 'value'}
        })

    @patch(f'{module}.derive_print_destination')
    @patch(f'{module}.generate_notification_data')
    @patch(f'{module}.create_replace_record_in_participant_table')
    def test_create_notification_record_for_invitation_or_reminder(
            self, create_replace_record_in_participant_table_mock,
            generate_notification_data_mock, derive_print_destination_mock, log_mock):
        # Arrange
        episode_address = {}
        participant_record = {'participant_id': 'test_id', 'sort_key': 'PARTICIPANT',
                              'registered_gp_practice_code': 'A91114',
                              'nhais_cipher': 'BAA'}
        letters_config_record = {'gp_practice_code': 'default'}
        letter_type = 'Reminder'
        create_notification_module.datetime.now.return_value = self.FIXED_NOW_TIME
        template = 'Test template'
        derive_print_destination_mock.return_value = 'CIC'
        generate_notification_data_mock.return_value = {'notification_test_data': 'value'}

        # Act
        create_notification_module.create_notification_record(
            participant_record, 'EPISODE#', letters_config_record,
            episode_address, letter_type, template)

        # Assert
        derive_print_destination_mock.assert_called_once_with('BAA')
        generate_notification_data_mock.assert_called_once_with(letter_type, participant_record, letters_config_record,
                                                                {}, template)
        create_replace_record_in_participant_table_mock.assert_called_with({
            'participant_id': 'test_id',
            'sort_key': 'NOTIFICATION#2020-01-23T13:48:08.123456+00:00',
            'created': '2020-01-23T13:48:08.123456+00:00',
            'status': NotificationStatus.PENDING_PRINT_FILE,
            'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
            'type': 'Reminder',
            'print_destination': 'CIC',
            'record_id': 'EPISODE#',
            'data': {'notification_test_data': 'value'}
        })

    def test_derive_print_destination(self, log_mock):

        iom_postcode_destination = create_notification_module.derive_print_destination('IM')
        dms_postcode_destination = create_notification_module.derive_print_destination('DMS')
        cic_postcode_destination = create_notification_module.derive_print_destination('None of the above')

        self.assertEqual(iom_postcode_destination, 'IOM')
        self.assertEqual(dms_postcode_destination, 'DMS')
        self.assertEqual(cic_postcode_destination, 'CIC')

    def test_generate_notification_data(self, log_mock):
        self.maxDiff = None
        letter_type = 'Invitation letter'
        create_notification_module.datetime.now.return_value = self.FIXED_NOW_TIME
        template = 'Test template'

        participant_record = sample_participant_record
        episode_address = participant_record['address']

        tailored_letter_text = sample_letter_config

        expected_notification_data = expected_invitation_notification_record['data']

        actual_notification_data = create_notification_module.generate_notification_data(
            letter_type, participant_record, tailored_letter_text, episode_address, template
        )

        self.assertEqual(expected_notification_data, actual_notification_data)

    def test_generate_notification_data_uses_episode_address_if_exists(self, log_mock):
        self.maxDiff = None
        episode_address = sample_episode_address
        letter_type = 'Invitation letter'
        create_notification_module.datetime.now.return_value = self.FIXED_NOW_TIME
        template = 'Test template'

        participant_record = sample_participant_record

        tailored_letter_text = sample_letter_config

        expected_notification_data = expected_invitation_notification_record_with_episode_address['data']

        actual_notification_data = create_notification_module.generate_notification_data(
            letter_type, participant_record, tailored_letter_text, episode_address, template
        )

        self.assertEqual(actual_notification_data, expected_notification_data)

    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_get_letter_text_by_gp_practice_code_given_no_config_exists(self, get_table_mock, _):
        table_mock = Mock()
        table_mock.get_item.return_value = {'ConsumedCapacity': 1}
        get_table_mock.return_value = table_mock

        actual_response = create_notification_module.get_letter_text_by_gp_practice_code('a practice code')

        self.assertIsNone(actual_response)
        table_mock.get_item.assert_called_once_with(Key={'gp_practice_code': 'a practice code'})

    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table')
    def test_get_letter_text_by_gp_practice_code_given_config_exists(self, get_table_mock, _):
        table_mock = Mock()
        gp_practice = {'gp_practice_code': 'a practice code', 'k2': 'v2'}
        table_mock.get_item.return_value = {'Item': {'gp_practice_code': 'a practice code', 'k2': 'v2'}}
        get_table_mock.return_value = table_mock

        actual_response = create_notification_module.get_letter_text_by_gp_practice_code('a practice code')

        self.assertDictEqual(gp_practice, actual_response)
        table_mock.get_item.assert_called_once_with(Key={'gp_practice_code': 'a practice code'})
