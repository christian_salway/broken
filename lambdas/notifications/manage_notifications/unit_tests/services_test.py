from unittest import TestCase
from mock import patch

from manage_notifications.services import get_latest_valid_invite_reminder_notification, \
    get_latest_valid_result_notification

PARTICIPANT_ID = '1234567'
EXAMPLE_NOTIFICATION = {
    "participant_id":           "unique-uuid",
    "sort_key":                 "NOTIFICATION#2001-01-01#1#2020-08-25T13:48:08.000Z",
    "nhs_number":               "9234567890",
    "notification_date":        "2001-01-01",
    "notification_sequence":    1,
    "created":                  "2020-08-25T13:48:08.000Z",
    "type":                     "Anything because its a mock",
    "migrated_8_data": {
        "received": "2020-02-14T13:26:25.432",
        "value":    "LS|9234567890|20010101|1|RP|N1|||"
    }
}
NOTIFICATIONS = [
    EXAMPLE_NOTIFICATION,
    EXAMPLE_NOTIFICATION
]


class TestServices(TestCase):
    module = 'manage_notifications'

    @patch(f'{module}.services.query_latest_resendable_invite_reminder')
    def test_get_latest_valid_invite_reminder_notification(self, query_latest_resendable_invite_reminder_mock):
        query_latest_resendable_invite_reminder_mock.return_value = NOTIFICATIONS

        result = get_latest_valid_invite_reminder_notification(PARTICIPANT_ID)

        query_latest_resendable_invite_reminder_mock.assert_called_with('1234567')

        self.assertEqual(result, NOTIFICATIONS[0])

    @patch(f'{module}.services.query_latest_resendable_result_letter')
    def test_get_latest_valid_result_notification(self, query_latest_resendable_result_letter_mock):
        query_latest_resendable_result_letter_mock.return_value = NOTIFICATIONS

        result = get_latest_valid_result_notification(PARTICIPANT_ID)

        query_latest_resendable_result_letter_mock.assert_called_with('1234567')

        self.assertEqual(result, NOTIFICATIONS[0])
