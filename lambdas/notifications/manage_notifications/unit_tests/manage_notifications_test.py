from unittest import TestCase
from mock import patch, call
from manage_notifications.log_references import LogReference
from common.test_assertions.api_assertions import assertApiResponse
from manage_notifications import manage_notifications

PARTICIPANT_ID = '123'
NHS_NUMBER = '9999999999'
PARTICIPANT = {
    'participant_id': PARTICIPANT_ID,
    'nhs_number': NHS_NUMBER
}
SESSION = {'session_key': '1234567890'}
NOTIFICATION = {
    'todo': 'stub notification record'
}

module = 'manage_notifications.manage_notifications'


@patch(f'{module}.log')
@patch(f'{module}.get_participant_by_participant_id')
@patch(f'{module}.get_api_request_info_from_event')
class TestLambdaHandler(TestCase):

    # Happy Path Tests
    @patch(f'{module}.get_latest_valid_result_notification')
    def test_handles_requests_for_result(
        self, get_latest_valid_result_notification_mock, request_info_mock, get_participant_mock, log_mock
    ):
        request_info_mock.return_value = get_request_info('result')
        get_participant_mock.return_value = {'Item': PARTICIPANT}
        get_latest_valid_result_notification_mock.return_value = NOTIFICATION

        actual_response = manage_notifications.lambda_handler.__wrapped__.__wrapped__({}, {})

        get_participant_mock.assert_called_with('123')
        get_latest_valid_result_notification_mock.assert_called_with(PARTICIPANT['participant_id'])

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANAGENOTIFICATIONS001}),
        ])

        assertApiResponse(self, 200, {'data': NOTIFICATION}, actual_response)

    @patch(f'{module}.get_latest_valid_invite_reminder_notification')
    def test_handles_requests_for_invite_reminder(
        self, get_latest_valid_invite_reminder_notification_mock, request_info_mock, get_participant_mock, log_mock
    ):
        request_info_mock.return_value = get_request_info('invite-reminder')
        get_participant_mock.return_value = {'Item': PARTICIPANT}
        get_latest_valid_invite_reminder_notification_mock.return_value = NOTIFICATION

        actual_response = manage_notifications.lambda_handler.__wrapped__.__wrapped__({}, {})

        get_participant_mock.assert_called_with('123')
        get_latest_valid_invite_reminder_notification_mock.assert_called_with(PARTICIPANT['participant_id'])

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANAGENOTIFICATIONS001}),
        ])

        assertApiResponse(self, 200, {'data': NOTIFICATION}, actual_response)
    # Sad Path Tests

    def test_manage_notifications_unknown_participant(
            self, request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = get_request_info()
        get_participant_mock.return_value = {}

        actual_response = manage_notifications.lambda_handler.__wrapped__.__wrapped__({}, {})

        assertApiResponse(self, 404, {'message': 'Participant does not exist'}, actual_response)
        get_participant_mock.assert_called_with('123')

        expected_log_calls = [
            call({'log_reference': LogReference.MANAGENOTIFICATIONS001}),
            call({'log_reference': LogReference.MANAGENOTIFICATIONS002}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.get_latest_valid_invite_reminder_notification')
    def test_notification_not_found(
        self, get_latest_valid_invite_reminder_notification_mock, request_info_mock, get_participant_mock, log_mock
    ):
        request_info_mock.return_value = get_request_info('invite-reminder')
        get_participant_mock.return_value = {'Item': PARTICIPANT}
        get_latest_valid_invite_reminder_notification_mock.return_value = {}

        actual_response = manage_notifications.lambda_handler.__wrapped__.__wrapped__({}, {})

        get_participant_mock.assert_called_with('123')
        get_latest_valid_invite_reminder_notification_mock.assert_called_with(PARTICIPANT['participant_id'])

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MANAGENOTIFICATIONS001}),
            call({'log_reference': LogReference.MANAGENOTIFICATIONS004}),
        ])

        assertApiResponse(self, 404, {'message': 'Notification does not exist'}, actual_response)

    def test_manage_notifications_bad_request(self, request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = {
            'path_parameters': {'participant_id': '123'},
            'session': SESSION,
        }, 'INVALID', '/api/participants/{participant_id}/notifications'
        get_participant_mock.return_value = {'Item': PARTICIPANT}

        actual_response = manage_notifications.lambda_handler.__wrapped__.__wrapped__({}, {})

        assertApiResponse(self, 400, {'message': 'Unrecognised method/resource combination'}, actual_response)


def get_request_info(service='any'):
    return {
        'path_parameters': {'participant_id': '123'},
        'session': SESSION
    }, 'GET', '/api/participants/{participant_id}/notifications' + f'/{service}'
