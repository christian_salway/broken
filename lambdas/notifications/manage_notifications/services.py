from common.utils.notification_utils import (
    query_latest_resendable_invite_reminder, query_latest_resendable_result_letter
)

# Type alias can be elaborated or replaced for better global alias
Notification = dict


def get_latest_valid_invite_reminder_notification(participant_id: str) -> Notification:
    # Fetch the participants latest invite reminder within 12 months
    notification_items = query_latest_resendable_invite_reminder(participant_id)

    if len(notification_items) > 0:
        return notification_items[0]

    # Return empty dict to maintain function type signature
    return {}


def get_latest_valid_result_notification(participant_id: str) -> Notification:
    # Fetch the participants latest result notification within 3 months
    notification_items = query_latest_resendable_result_letter(participant_id)

    if len(notification_items) > 0:
        return notification_items[0]

    # Return empty dict to maintain function type signature
    return {}
