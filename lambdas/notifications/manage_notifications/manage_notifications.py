from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_message, json_return_data
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.participant_utils import get_participant_by_participant_id
from manage_notifications.log_references import LogReference

from manage_notifications.services import get_latest_valid_invite_reminder_notification, \
    get_latest_valid_result_notification


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    BASE_URL = '/api/participants/{participant_id}/notifications'
    REQUEST_FUNCTIONS = {
        ('GET', BASE_URL + '/invite-reminder'): get_latest_valid_invite_reminder_notification,
        ('GET', BASE_URL + '/result'): get_latest_valid_result_notification,
    }

    event_data, http_method, resource = get_api_request_info_from_event(event)

    log({'log_reference': LogReference.MANAGENOTIFICATIONS001})

    participant_id = event_data['path_parameters']['participant_id']
    participant = get_participant_by_participant_id(participant_id)

    if not participant:
        log({'log_reference': LogReference.MANAGENOTIFICATIONS002})
        return json_return_message(404, 'Participant does not exist')

    service = REQUEST_FUNCTIONS.get((http_method, resource))

    if not service:
        log({'log_reference': LogReference.MANAGENOTIFICATIONS003})
        return json_return_message(400, 'Unrecognised method/resource combination')

    data = service(participant_id)

    if not bool(data):
        log({'log_reference': LogReference.MANAGENOTIFICATIONS004})
        return json_return_message(404, 'Notification does not exist')

    log({'log_reference': LogReference.MANAGENOTIFICATIONS005})
    return json_return_data(200, data)
