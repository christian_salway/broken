import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    MANAGENOTIFICATIONS001 = (logging.INFO, 'Received request for notifications')
    MANAGENOTIFICATIONS002 = (logging.WARNING, 'Participant id not found in the database')
    MANAGENOTIFICATIONS003 = (logging.ERROR, 'Requested method/resource not supported')
    MANAGENOTIFICATIONS004 = (logging.ERROR, 'No notification found in the database')
    MANAGENOTIFICATIONS005 = (logging.INFO, 'Found notification for participant')
