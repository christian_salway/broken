# Manage Notifications

This is an API lambda.

It is used to return notification information for the current participant.

It is accessible only when used by someone has the CSAS Role in the screening platform.

## [BASE_URL] `/api/participants/{participant_id}/notifications`

### [GET] `/result`

Returns the latest result

### [GET] `/invite-reminder`

Returns the latest invite reminder
