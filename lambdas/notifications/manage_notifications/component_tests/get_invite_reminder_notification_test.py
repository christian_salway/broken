# Component tests are used to test everything only mocking interfaces outside the application domain e.g. HTTP
import json
from unittest import TestCase
from mock import patch, Mock
from datetime import datetime, timezone

example_date = datetime(2020, 10, 18, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

mock_env_vars = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE',
}

participants_table_mock = Mock()
notifications_table_mock = Mock()

EXAMPLE_NOTIFICATION = {
    "participant_id":           "unique-uuid",
    "sort_key":                 "NOTIFICATION#2001-01-01#1#2020-08-25T13:48:08.000Z",
    "nhs_number":               "9234567890",
    "notification_date":        "2001-01-01",
    "notification_sequence":    1,
    "created":                  "2020-08-25T13:48:08.000Z",
    "type":                     "Anything because its a mock",
    "migrated_8_data": {
        "received": "2020-02-14T13:26:25.432",
        "value":    "LS|9234567890|20010101|1|RP|N1|||"
    }
}


with patch('os.environ', mock_env_vars):
    with patch('datetime.datetime', datetime_mock):
        from manage_notifications.manage_notifications import lambda_handler


@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(return_value=notifications_table_mock))
class TestGetInviteReminderNotification(TestCase):
    mock_participant_get_item = {
        'participant_id': '123456',
        'sort_key': 'PARTICIPANT',
    }

    mock_notification_query = [
        EXAMPLE_NOTIFICATION,
        EXAMPLE_NOTIFICATION
    ]

    def setUp(self):
        self.maxDiff = 1000

        self.logger_patcher = patch('common.log.logging')
        participants_table_mock.reset_mock()
        self.logger_patcher.start()

    def tearDown(self):
        self.logger_patcher.stop()

    def test_GET_invite_reminder(self):
        participants_table_mock.get_item.return_value = {'Item': self.mock_participant_get_item}
        notifications_table_mock.query.return_value = {'Items': self.mock_notification_query}

        event = self._build_event('p-12345', 'u-12345', 's-12345', 'GET')
        context = Mock()
        context.function_name = ''
        actual_response = lambda_handler(event, context)

        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': json.dumps({'data': EXAMPLE_NOTIFICATION}),
            'isBase64Encoded': False
        }

        self.assertEqual(expected_response, actual_response)

    def _build_event(
            self,
            participant_id,
            user_uid,
            session_id,
            http_method,
            body=None,
            resource='/api/participants/{participant_id}/notifications/invite-reminder'):
        if not body:
            body = '{}'
        return {
            'httpMethod': http_method,
            'resource': resource,
            'pathParameters': {
                'participant_id': participant_id
            },
            'headers': {
                'request_id': 'requestID',
            },
            'body': body,
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': f"{{\"user_data\": {{\"nhsid_useruid\": \"{user_uid}\", \"first_name\": \"firsty\", \"last_name\": \"lasty\"}}, \"session_id\": \"{session_id}\"}}"  
                }
            }
        }
