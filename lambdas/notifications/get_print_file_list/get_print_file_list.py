import re
import os
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_data, json_return_message
from get_print_file_list.log_references import LogReference
from common.log import log
from common.utils.s3_utils import read_bucket_with_subfolders, get_object_from_bucket
from common.utils.session_utils import get_session_from_lambda_event
from common.utils.print_file_utils import CAPITA_DATE_FORMAT
from datetime import datetime, timedelta, timezone

BUCKET_PATH = os.environ.get('BUCKET_PATH')
ARCHIVE_DAYS = int(os.environ.get('ARCHIVE_DAYS'))
TODAY_NOW = datetime.now(timezone.utc)
CUT_OFF_DATE = TODAY_NOW - timedelta(ARCHIVE_DAYS)
DMS_GROUP = 'csas_dms'
IOM_GROUP = 'csas_iom'


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    log({'log_reference': LogReference.GETPRINTFILE0001})

    session = get_session_from_lambda_event(event)
    if not session or not session.get('access_groups'):
        log({'log_reference': LogReference.GETPRINTFILEEX0001})
        return json_return_message(403, 'Session missing, expired or not logged in.')

    file_type = determine_file_type_from_session(session)
    if file_type == 'NOT_KNOWN':
        log({'log_reference': LogReference.GETPRINTFILEEX0002})
        return json_return_message(403, 'User does not have the correct role to download files.')

    response = prepare_list_of_files(file_type)

    log({'log_reference': LogReference.GETPRINTFILE0007})
    return json_return_data(200, response)


def determine_file_type_from_session(session):
    access_groups = session['access_groups']

    if access_groups[DMS_GROUP]:
        return 'DMS'
    if access_groups[IOM_GROUP]:
        return 'IOM'
    return 'NOT_KNOWN'


def prepare_list_of_files(file_type):
    file_list = get_list_of_files(file_type)

    log({'log_reference': LogReference.GETPRINTFILE0004, 'matching_items': len(file_list)})
    sorted_list = sort_file_list(file_list)

    log({'log_reference': LogReference.GETPRINTFILE0006, 'sorted_files': len(sorted_list)})
    return [create_response_object(file) for file in sorted_list]


def get_list_of_files(file_type):
    log({'log_reference': LogReference.GETPRINTFILE0002, 'bucket_path': BUCKET_PATH})
    file_prefix = file_type + '/CSAS'
    response = read_bucket_with_subfolders(BUCKET_PATH, file_prefix)
    return response.get('Contents', [])


def sort_file_list(file_list):
    log({'log_reference': LogReference.GETPRINTFILE0005})
    return sorted([
        file for file in file_list if not is_file_archived(file.get('Key'))
    ], key=lambda file: get_file_created_date(file.get('Key')), reverse=True)


def create_response_object(file_record):
    response_object = {
        'creation_date': get_file_created_date(file_record['Key']),
        'letter_type': get_letter_type(file_record),
        'file_name': file_record['Key']
    }
    return {**response_object, **get_metadata_information(file_record)}


def get_file_created_date(file_name):
    pattern = re.compile(r'.*_(\d{12})(\.dat)')  # 12 digits from CAPITA_DATE_FORMAT
    result = pattern.search(file_name)
    return result.group(1)


def get_letter_type(file_record):
    file_name = file_record.get('Key')
    file_parts = file_name.split('_')

    # The file name is in the following format:
    # CSAS_EU-WEST-2_{job_type}_{now_time_capita_format}.dat'
    return file_parts[2]


def get_metadata_information(file_record):
    file_name = file_record.get('Key')
    file_metadata = get_object_from_bucket(BUCKET_PATH, file_name).get('Metadata', {})

    return {
        'downloaded_by': file_metadata.get('downloaded_by', ''),
        'downloaded_date': file_metadata.get('downloaded_date', '')
    }


def is_file_archived(file_name):
    file_created_date_string = get_file_created_date(file_name)
    file_created_date = datetime.strptime(file_created_date_string, CAPITA_DATE_FORMAT).replace(tzinfo=timezone.utc)
    return file_created_date < CUT_OFF_DATE
