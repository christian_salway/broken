from datetime import datetime, timezone
from unittest import TestCase
from mock import patch, Mock, MagicMock, call
from get_print_file_list.log_references import LogReference
from json import loads


class TestGetPrintFileList(TestCase):
    module = 'get_print_file_list'
    module_self = f'{module}.{module}'

    example_date = datetime(2020, 10, 20, tzinfo=timezone.utc)
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.now.return_value = example_date

    mock_env_vars = {
        'BUCKET_PATH': 'print_files_to_send',
        'ARCHIVE_DAYS': '10'
    }

    @patch('datetime.datetime', datetime_mock)
    @patch('os.environ', mock_env_vars)
    @patch('boto3.resource', MagicMock())
    @patch('boto3.client', MagicMock())
    @patch('common.log.get_internal_id', Mock(return_value='1'))
    def setUp(self, *args):
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.env_patcher = patch('os.environ', self.mock_env_vars)
        self.env_patcher.start()
        self.context = Mock()
        self.context.function_name = ''

        # Setup lambda module
        import get_print_file_list.get_print_file_list as _get_print_file_list
        self.get_print_file_list_module = _get_print_file_list

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    @patch('common.utils.s3_utils.get_s3_client')
    @patch(f'{module_self}.log')
    def test_lambda_returns_sorted_unexpired_print_file_list(self, log_mock, s3_mock):
        access_groups = '{"csas": true, "csas_dms": true, "csas_iom": false}'
        self.event = _build_event('user_id', 'session_id', access_groups)

        s3_client_mock = Mock()
        s3_client_mock.get_object.return_value = {
            'Metadata': {
                'downloaded_by': 'Struan',
                'downloaded_date': '20191015214500'
            }
        }
        s3_client_mock.list_objects_v2.return_value = {
            'Contents': [
                {'Key': 'DMS/CSAS_EU-WEST-2_CP_201015214500.dat'},
                {'Key': 'DMS/CSAS_EU-WEST-2_RP_201001214500.dat'},
                {'Key': 'DMS/CSAS_EU-WEST-2_CP_201013214500.dat'},
                {'Key': 'DMS/CSAS_EU-WEST-2_CP_200910214500.dat'},
                {'Key': 'DMS/CSAS_EU-WEST-2_RP_201009214500.dat'},
                {'Key': 'DMS/CSAS_EU-WEST-2_RP_201019214500.dat'},
            ]
        }
        s3_mock.return_value = s3_client_mock

        response = self.get_print_file_list_module.lambda_handler(self.event, self.context)
        body = loads(response['body'])

        self.assertEqual(body['data'], [
            {
                'creation_date': '201019214500',
                'letter_type': 'RP',
                'file_name': 'DMS/CSAS_EU-WEST-2_RP_201019214500.dat',
                'downloaded_by': 'Struan',
                'downloaded_date': '20191015214500'
            },
            {
                'creation_date': '201015214500',
                'letter_type': 'CP',
                'file_name': 'DMS/CSAS_EU-WEST-2_CP_201015214500.dat',
                'downloaded_by': 'Struan',
                'downloaded_date': '20191015214500'
            },
            {
                'creation_date': '201013214500',
                'letter_type': 'CP',
                'file_name': 'DMS/CSAS_EU-WEST-2_CP_201013214500.dat',
                'downloaded_by': 'Struan',
                'downloaded_date': '20191015214500'
            }
        ])
        self.assertEqual(response['statusCode'], 200)

        s3_client_mock.list_objects_v2.assert_called_with(
            Bucket='print_files_to_send', Prefix='DMS/CSAS', Delimiter='/'
        )

        s3_client_mock.get_object.assert_has_calls(
            [
                call(Bucket='print_files_to_send', Key='DMS/CSAS_EU-WEST-2_RP_201019214500.dat'),
                call(Bucket='print_files_to_send', Key='DMS/CSAS_EU-WEST-2_CP_201015214500.dat'),
                call(Bucket='print_files_to_send', Key='DMS/CSAS_EU-WEST-2_CP_201013214500.dat'),
            ]
        )

        log_mock.assert_has_calls(
            [
                call({'log_reference': LogReference.GETPRINTFILE0001}),
                call({'log_reference': LogReference.GETPRINTFILE0002, 'bucket_path': 'print_files_to_send'}),
                call({'log_reference': LogReference.GETPRINTFILE0004, 'matching_items': 6}),
                call({'log_reference': LogReference.GETPRINTFILE0005}),
                call({'log_reference': LogReference.GETPRINTFILE0006, 'sorted_files': 3}),
                call({'log_reference': LogReference.GETPRINTFILE0007}),
            ]
        )

    @patch('common.utils.s3_utils.get_s3_client')
    @patch(f'{module_self}.log')
    def test_lambda_returns_only_files_matching_IOM(self, log_mock, s3_mock):
        access_groups = '{"csas": true, "csas_dms": false, "csas_iom": true}'
        self.event = _build_event('user_id', 'session_id', access_groups)
        s3_client_mock = Mock()
        s3_client_mock.get_object.return_value = {}
        s3_client_mock.list_objects_v2.return_value = {
            'Contents': [
                {'Key': 'IOM/CSAS_EU-WEST-2_CP_201015214500.dat'},
                {'Key': 'IOM/CSAS_EU-WEST-2_RP_201014214500.dat'},
                {'Key': 'IOM/CSAS_EU-WEST-2_RP_201011214500.dat'},
            ]
        }

        s3_mock.return_value = s3_client_mock

        response = self.get_print_file_list_module.lambda_handler(self.event, self.context)
        body = loads(response['body'])

        self.assertEqual(body['data'], [
            {
                'creation_date': '201015214500',
                'letter_type': 'CP',
                'file_name': 'IOM/CSAS_EU-WEST-2_CP_201015214500.dat',
                'downloaded_by': '',
                'downloaded_date': ''
            },
            {
                'creation_date': '201014214500',
                'letter_type': 'RP',
                'file_name': 'IOM/CSAS_EU-WEST-2_RP_201014214500.dat',
                'downloaded_by': '',
                'downloaded_date': ''
            },
            {
                'creation_date': '201011214500',
                'letter_type': 'RP',
                'file_name': 'IOM/CSAS_EU-WEST-2_RP_201011214500.dat',
                'downloaded_by': '',
                'downloaded_date': ''
            }
        ])
        self.assertEqual(response['statusCode'], 200)
        log_mock.assert_has_calls(
            [
                call({'log_reference': LogReference.GETPRINTFILE0001}),
                call({'log_reference': LogReference.GETPRINTFILE0002, 'bucket_path': 'print_files_to_send'}),
                call({'log_reference': LogReference.GETPRINTFILE0004, 'matching_items': 3}),
                call({'log_reference': LogReference.GETPRINTFILE0005}),
                call({'log_reference': LogReference.GETPRINTFILE0006, 'sorted_files': 3}),
                call({'log_reference': LogReference.GETPRINTFILE0007}),
            ]
        )

    @patch('common.utils.s3_utils.get_s3_client')
    @patch(f'{module_self}.log')
    def test_lambda_returns_failure_if_session_missing(self, log_mock, s3_mock):
        self.event = _build_event('', '', '', True)
        s3_client_mock = Mock()
        s3_client_mock.get_object.return_value = {}
        s3_client_mock.list_objects_v2.return_value = {}

        s3_mock.return_value = s3_client_mock

        response = self.get_print_file_list_module.lambda_handler(self.event, self.context)

        self.assertEqual(response['statusCode'], 403)
        body = loads(response['body'])
        self.assertEqual(body['message'], 'Session missing, expired or not logged in.')

        s3_client_mock.list_objects_v2.assert_not_called()
        s3_client_mock.get_object.assert_not_called()

        log_mock.assert_has_calls(
            [
                call({'log_reference': LogReference.GETPRINTFILE0001}),
                call({'log_reference': LogReference.GETPRINTFILEEX0001}),
            ]
        )

    @patch('common.utils.s3_utils.get_s3_client')
    @patch(f'{module_self}.log')
    def test_lambda_returns_failure_if_no_matching_access_group(self, log_mock, s3_mock):
        access_groups = '{"csas": true, "csas_dms": false, "csas_iom": false}'
        self.event = _build_event('user_id', 'session_id', access_groups)
        s3_client_mock = Mock()
        s3_client_mock.get_object.return_value = {}
        s3_client_mock.list_objects_v2.return_value = {}

        s3_mock.return_value = s3_client_mock

        response = self.get_print_file_list_module.lambda_handler(self.event, self.context)

        self.assertEqual(response['statusCode'], 403)
        body = loads(response['body'])
        self.assertEqual(body['message'], 'User does not have the correct role to download files.')

        log_mock.assert_has_calls(
            [
                call({'log_reference': LogReference.GETPRINTFILE0001}),
                call({'log_reference': LogReference.GETPRINTFILEEX0002}),
            ]
        )


def _build_event(user_uid, session_id, access_groups, no_session=False):
    if no_session:
        session = "{}"
    else:
        session = f"{{\"user_data\": {{\"nhsid_useruid\": \"{user_uid}\", \"first_name\": \"John\", \"last_name\": \"Doe\"}}, \"session_id\": \"{session_id}\", \"access_groups\": {access_groups}}}" 

    return {
        'headers': {
            'request_id': 'request_id',
        },
        'requestContext': {
            'authorizer': {
                'principalId': 'blah',
                'session': session
            }
        }
    }
