This lambda returns a list of print files to be displayed on the web-app. The user will then be able to choose
a specific file to print. This lambda has been set up to accept an environment variable `FILE_TYPE`, which will
then be used to determine if this lambda serves DMS or IOM files.

The `FILE_TYPE` variable can either be `IOM` or `DMS`, and can be extended for further types easily.
