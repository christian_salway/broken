import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    GETPRINTFILE0001 = (logging.INFO, 'Received request to get print file list')
    GETPRINTFILE0002 = (logging.INFO, 'Retrieving file list from bucket')
    GETPRINTFILE0003 = (logging.INFO, 'Filtering out archived files and files not matching FILE_TYPE')
    GETPRINTFILE0004 = (logging.INFO, 'Finished filtering files')
    GETPRINTFILE0005 = (logging.INFO, 'Sorting the files based on their created date')
    GETPRINTFILE0006 = (logging.INFO, 'Retrieving metadata for files and preparing response')
    GETPRINTFILE0007 = (logging.INFO, 'Returning print file list')

    GETPRINTFILEEX0001 = (logging.ERROR, 'Unable to retrieve user groups from the user\'s session')
    GETPRINTFILEEX0002 = (logging.ERROR, 'Unable to find csas_iom or csas_dms access role in user\'s session')
