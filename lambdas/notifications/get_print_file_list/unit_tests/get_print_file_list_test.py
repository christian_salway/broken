from unittest.case import TestCase
from datetime import datetime, timezone
from mock import patch, Mock


class TestGetPrintFileListLambdaHandler(TestCase):
    module = 'get_print_file_list.get_print_file_list'

    example_date = datetime(2020, 10, 20, 0, 0, 0, 0, tzinfo=timezone.utc)
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.now.return_value = example_date

    mock_env_vars = {
        'BUCKET_PATH': 'print_files_to_send',
        'ARCHIVE_DAYS': '10'
    }

    @patch('datetime.datetime', datetime_mock)
    @patch('boto3.client', Mock())
    @patch('boto3.resource', Mock())
    def setUp(self, *args):
        self.env_patcher = patch('os.environ', self.mock_env_vars)
        self.env_patcher.start()
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()

        # Setup lambda module
        import get_print_file_list.get_print_file_list as _get_print_file_list
        self.get_print_file_list_module = _get_print_file_list

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    def test_determine_file_type_from_session_DMS(self):
        session = {
            'access_groups': {
                'csas': True,
                'csas_dms': True,
                'csas_iom': False
            }
        }

        file_type = self.get_print_file_list_module.determine_file_type_from_session(session)

        self.assertEqual('DMS', file_type)

    def test_determine_file_type_from_session_IOM(self):
        session = {
            'access_groups': {
                'csas': True,
                'csas_dms': False,
                'csas_iom': True
            }
        }

        file_type = self.get_print_file_list_module.determine_file_type_from_session(session)

        self.assertEqual('IOM', file_type)

    def test_determine_file_type_from_session_no_known_group(self):
        session = {
            'access_groups': {
                'csas': True,
                'csas_dms': False,
                'csas_iom': False
            }
        }

        file_type = self.get_print_file_list_module.determine_file_type_from_session(session)

        self.assertEqual('NOT_KNOWN', file_type)

    @patch(f'{module}.read_bucket_with_subfolders')
    def test_get_list_of_files(self, read_bucket_mock):
        file_type = 'ABC123'
        read_bucket_mock.return_value = {'Contents': [
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201001214500.dat'},
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201019214500.dat'},
        ]}

        file_list = self.get_print_file_list_module.get_list_of_files(file_type)

        read_bucket_mock.assert_called_with('print_files_to_send', 'ABC123/CSAS')
        expected_file_list = [
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201001214500.dat'},
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201019214500.dat'}
        ]
        self.assertEqual(file_list, expected_file_list)

    def test_sort_file_list_should_filter_and_sort_entries(self):
        unsorted_list = [
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201015214500.dat', 'test': 'should remain'},
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201001214500.dat', 'test': 'should filter'},
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201013214500.dat', 'test': 'should remain'},
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_200910214500.dat', 'test': 'should filter'},
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201009214500.dat', 'test': 'should filter'},
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201019214500.dat', 'test': 'should remain'}
        ]

        sorted_list = self.get_print_file_list_module.sort_file_list(unsorted_list)

        expected_list = [
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201019214500.dat', 'test': 'should remain'},
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201015214500.dat', 'test': 'should remain'},
            {'Key': 'ABC123/CSAS_EU-WEST-2_PD_201013214500.dat', 'test': 'should remain'},
        ]
        self.assertEqual(sorted_list, expected_list)

    @patch(f'{module}.get_file_created_date')
    @patch(f'{module}.get_letter_type')
    @patch(f'{module}.get_metadata_information')
    def test_create_response_object(self, get_metadata_mock, get_type_mock, get_created_mock):
        file = {
            'Key': 'CSAS_EU-WEST-2_PD_201015214500.dat'
        }
        get_created_mock.return_value = '20201015214500'
        get_type_mock.return_value = 'ABC'
        get_metadata_mock.return_value = {
            'downloaded_by': 'Struan',
            'downloaded_date': '20191015214500'
        }

        response = self.get_print_file_list_module.create_response_object(file)

        self.assertEqual(response, {
            'creation_date': '20201015214500',
            'letter_type': 'ABC',
            'file_name': 'CSAS_EU-WEST-2_PD_201015214500.dat',
            'downloaded_by': 'Struan',
            'downloaded_date': '20191015214500'
        })

    def test_get_file_created_date(self):
        file_name = 'CSAS_EU-WEST-2_PD_201015214500.dat'

        date = self.get_print_file_list_module.get_file_created_date(file_name)

        self.assertEqual(date, '201015214500')

    def test_get_letter_type(self):
        file = {
            'Key': 'ABC123/CSAS_EU-WEST-2_PD_201015214500.dat'
        }

        letter_type = self.get_print_file_list_module.get_letter_type(file)

        self.assertEqual(letter_type, 'PD')

    @patch(f'{module}.get_object_from_bucket')
    def test_get_metadata_information(self, get_object_mock):
        file = {
            'Key': 'ABC123/CSAS_EU-WEST-2_PD_201015214500.dat'
        }
        get_object_mock.return_value = {
            'Metadata': {
                'downloaded_by': 'Struan',
                'downloaded_date': '20201015214500'
            }
        }

        metadata = self.get_print_file_list_module.get_metadata_information(file)

        self.assertEqual(metadata, {
            'downloaded_by': 'Struan',
            'downloaded_date': '20201015214500'
        })
        get_object_mock.assert_called_with('print_files_to_send', 'ABC123/CSAS_EU-WEST-2_PD_201015214500.dat')

    def test_is_file_archived_returns_true_greater_than_10_days(self):
        file_name = 'ABC123/CSAS_EU-WEST-2_PD_201001214500.dat'

        file_archived = self.get_print_file_list_module.is_file_archived(file_name)

        self.assertTrue(file_archived)

    def test_is_file_archived_returns_false_less_than_10_days(self):
        file_name = 'ABC123/CSAS_EU-WEST-2_PD_201015214500.dat'

        file_archived = self.get_print_file_list_module.is_file_archived(file_name)

        self.assertFalse(file_archived)
