import os
from datetime import datetime

from common.log import log
from common.log_references import CommonLogReference
from common.utils import json_return_message
from common.utils.audit_utils import audit, AuditActions
from common.utils.file_download_utils import ProcessingException, verify_and_parse_session, \
    get_file_name, update_metadata, get_file_body
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils.session_utils import get_session_from_lambda_event

CONFIRMATION_REPORT_BUCKET_NAME = os.environ.get('CONFIRMATION_REPORT_BUCKET_NAME')
ENVIRONMENT_NAME = os.environ.get('ENVIRONMENT_NAME')


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):

    try:
        session = get_session_from_lambda_event(event)
        session_user_data, session_selected_access_groups = verify_and_parse_session(session)
        file_name = get_file_name(event)
        file_date_string = _get_file_date(file_name)
        check_user_access_allowed(session_selected_access_groups)
        confirmation_report_body = get_file_body(CONFIRMATION_REPORT_BUCKET_NAME, f'{ENVIRONMENT_NAME}/{file_date_string}/{file_name}') 
        update_metadata(session_user_data, f'{ENVIRONMENT_NAME}/{file_date_string}/{file_name}', CONFIRMATION_REPORT_BUCKET_NAME) 
    except ProcessingException as e:
        return json_return_message(e.response_code, e.message)
    except Exception as e:
        log({'log_reference': CommonLogReference.DOWNLOADFILEEX0004, 'error': str(e)})
        return json_return_message(500, 'Cannot complete request')

    audit(
        session=session,
        action=AuditActions.CONFIRMATION_REPORT_DOWNLOADED,
        additional_information={'file_name': file_name}
    )

    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'text/plain',
                    'X-Content-Type-Options': 'nosniff',
                    'Strict-Transport-Security': 'max-age=1576800',
                    'Content-Disposition': f'attachment; filename="{file_name}"'},
        'body': confirmation_report_body,
        'isBase64Encoded': False
    }


def _get_file_date(filename):
    try:
        name_components = filename.split('_')
        required_date = datetime.strptime(name_components[len(name_components)-2], '%d%m%Y')
        return required_date.strftime('%d-%m-%Y')
    except Exception:
        log({'log_reference': CommonLogReference.DOWNLOADFILEEX0003})
        raise ProcessingException(400, f'Object not found with name {filename}')


def check_user_access_allowed(session_selected_access_groups):
    if not session_selected_access_groups.get('csas'):
        log({'log_reference': CommonLogReference.DOWNLOADFILEEX0005})
        raise ProcessingException(400, CommonLogReference.DOWNLOADFILEEX0005.message)
