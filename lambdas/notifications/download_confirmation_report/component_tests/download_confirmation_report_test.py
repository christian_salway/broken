import json
from datetime import datetime
from time import time
from unittest import TestCase
from unittest.mock import patch, Mock,  call

from common.log_references import CommonLogReference

VALID_REPORT_FILE_NAME = 'test_file_name_01011990_11.txt'
VALID_DATE = '01-01-1990'
BUCKET_NAME = 'confirmation-reports'
ENVIRONMENT_NAME = 'test-env'

mock_env_vars = {
    'CONFIRMATION_REPORT_BUCKET_NAME': BUCKET_NAME,
    'ENVIRONMENT_NAME': ENVIRONMENT_NAME
}

example_date = datetime(2020, 10, 18, 13, 48, 8)
datetime_mock = Mock(wraps=datetime)
datetime_mock.utcnow.return_value = example_date
datetime_mock.now.return_value = example_date
datetime_mock.today.return_value = example_date

with patch('os.environ', mock_env_vars):
    from download_confirmation_report.download_confirmation_report import lambda_handler

module = 'download_confirmation_report'
module_utils = 'common.utils'

session_table_mock = Mock()
sqs_client_mock = Mock()
s3_mock = Mock()
log_mock = Mock()


@patch(f'{module_utils}.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch(f'{module_utils}.s3_utils.get_s3_client', Mock(return_value=s3_mock))
@patch(f'{module_utils}.audit_utils.datetime', datetime_mock)
@patch(f'{module_utils}.file_download_utils.datetime', datetime_mock)
@patch(f'{module_utils}.file_download_utils.log', log_mock)
class TestDownloadConfirmationReport(TestCase):

    mock_session_record = {
        'Items': [
            {
                'reauthenticate_timestamp': time() + 10,
                'expires': time() + 10,
                'user_data': {
                    'first_name': 'firsty',
                    'last_name': 'lasty',
                    'nhsid_useruid': '78'
                },
                'selected_role': {
                    'workgroups': ['cervical_screening'],
                    'role_id': 'test',
                    'organisation_code': 'test'
                },
                'access_groups': {
                    'csas': True
                }
            }
        ]
    }

    mock_file = 'this is a test file'

    def setUp(self):
        session_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        log_mock.reset_mock()

    def test_download_confirmation_report(self):
        context = Mock()
        context.function_name = ''

        file_stream_mock = Mock()
        file_stream_mock.read.return_value = b'test_file_line'
        object_mock = {
            'Body': file_stream_mock
        }

        session_table_mock.query.return_value = self.mock_session_record
        s3_mock.get_object.return_value = object_mock
        s3_mock.head_object.return_value = {
            'Metadata': {
                'existing_test_data': 'test'
            }
        }

        event = self._build_event(
            'test_uid',
            'session_id',
            'GET'
        )

        expected_response = {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'text/plain',
                'X-Content-Type-Options': 'nosniff',
                'Strict-Transport-Security': 'max-age=1576800',
                'Content-Disposition': f'attachment; filename="{VALID_REPORT_FILE_NAME}"'
            },
            'body': 'test_file_line',
            'isBase64Encoded': False
        }

        response = lambda_handler(event, context)

        s3_mock.get_object.assert_called_with(Bucket=BUCKET_NAME, Key=f'{ENVIRONMENT_NAME}/{VALID_DATE}/{VALID_REPORT_FILE_NAME}') 
        s3_mock.copy_object.assert_called_with(
            Bucket=BUCKET_NAME,
            Key=f'{ENVIRONMENT_NAME}/{VALID_DATE}/{VALID_REPORT_FILE_NAME}',
            CopySource=f'confirmation-reports/{ENVIRONMENT_NAME}/{VALID_DATE}/{VALID_REPORT_FILE_NAME}',
            Metadata={
                'existing_test_data': 'test',
                'downloaded_by': 'firsty lasty',
                'downloaded_date': '2020-10-18'
            },
            MetadataDirective='REPLACE'
        )
        sqs_client_mock.send_message.assert_called_with(
            QueueUrl=None,
            MessageBody=json.dumps(
                {
                    'action': 'CONFIRMATION_REPORT_DOWNLOADED',
                    'timestamp': '2020-10-18T13:48:08',
                    'internal_id': 'requestID',
                    'nhsid_useruid': 'test_uid',
                    'session_id': 'session_id',
                    'first_name': 'firsty',
                    'last_name': 'lasty',
                    'role_id': 'test',
                    'user_organisation_code': 'test',
                    'additional_information': {'file_name': VALID_REPORT_FILE_NAME}
                }
            )
        )
        self.assertEqual(expected_response, response)
        log_mock.assert_has_calls([
             call({'log_reference': CommonLogReference.DOWNLOADFILE0001}),
             call({'log_reference': CommonLogReference.DOWNLOADFILE0004,
                   'file_name': 'test_file_name_01011990_11.txt'}),
             call({'log_reference': CommonLogReference.DOWNLOADFILE0002}),
             call({'log_reference': CommonLogReference.DOWNLOADFILE0003})
        ])

    def _build_event(
            self,
            user_uid,
            session_id,
            http_method,
            body=None,
            resource='/api/download-confirmation-report'):
        if not body:
            body = '{}'
        return {
            'httpMethod': http_method,
            'resource': resource,
            'queryStringParameters': {
                'file_name': VALID_REPORT_FILE_NAME
            },
            'path': resource,
            'headers': {
                'request_id': 'requestID',
                'cookie': {
                    'sp_session_cookie': 'choc_chip'
                }
            },
            'body': body,
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': json.dumps(
                        {
                            'user_data': {'nhsid_useruid': user_uid, 'first_name': 'firsty', 'last_name': 'lasty'},
                            'session_id': f'{session_id}',
                            'selected_role': {
                                'workgroups': ['cervical_screening'],
                                'role_id': 'test',
                                'organisation_code': 'test'
                            },
                            'access_groups': {
                                'csas': True
                            }
                        }
                    )
                }
            }
        }
