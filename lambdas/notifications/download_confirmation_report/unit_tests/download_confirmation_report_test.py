from unittest import TestCase
from unittest.mock import patch, Mock, call

from common.log_references import CommonLogReference
from common.utils.audit_utils import AuditActions
from common.utils.file_download_utils import ProcessingException

module = 'download_confirmation_report.download_confirmation_report'

test_selected_role = {
    "organisation_code": "A60",
    "organisation_name": "YORKSHIRE AND THE HUMBER",
    "role_id": "2001",
    "role_name": "CSAS Team Member",
    "workgroups": [
      "cervicalscreening"
    ],
    'access_groups': {
                'csas': True
    }
}

VALID_REPORT_FILE_NAME = 'test_file_name_01011990_11.txt'
INVALID_REPORT_FILE_NAME = 'test-file-name.txt'
BUCKET_NAME = 'confirmation-reports'


class TestDownloadConfirmationReport(TestCase):

    @classmethod
    @patch('boto3.resource', Mock())
    @patch('os.environ', {
        'CONFIRMATION_REPORT_BUCKET_NAME': BUCKET_NAME
    })
    def setUp(self):
        import download_confirmation_report.download_confirmation_report as _download_confirmation_report_module
        from common.test_assertions.api_assertions import assertApiResponse as _assert_api_response

        self.download_confirmation_report_module = _download_confirmation_report_module
        self.assert_api_response = _assert_api_response

    def test_get_file_date_valid_filename(self):
        actual_file_date = self.download_confirmation_report_module._get_file_date(VALID_REPORT_FILE_NAME)
        expected_date_string = '01-01-1990'
        self.assertEqual(actual_file_date, expected_date_string)

    @patch(f'{module}.log')
    def test_get_file_date_invalid_filename(self, log_mock):
        with self.assertRaises(ProcessingException) as context:
            self.download_confirmation_report_module._get_file_date(INVALID_REPORT_FILE_NAME)
        self.assertTrue(f'Object not found with name {INVALID_REPORT_FILE_NAME}' in context.exception.message)
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILEEX0003})
        ])

    @patch(f'{module}.log')
    def test_check_user_access_allowed_bad_session(self, log_mock):
        with self.assertRaises(ProcessingException) as context:
            self.download_confirmation_report_module.check_user_access_allowed({'csas': False})
        self.assertTrue(CommonLogReference.DOWNLOADFILEEX0005.message in context.exception.message)
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILEEX0005})
        ])

    @patch(f'{module}.get_session_from_lambda_event', Mock())
    @patch(f'{module}.verify_and_parse_session', Mock(side_effect=ProcessingException(999, 'test')))
    @patch(f'{module}.json_return_message')
    def test_lambda_handler_process_exception(self, json_return_mock):
        context_mock = Mock()
        context_mock.function_name.return_value = 'test'

        json_return_mock.return_value = 'error response'

        return_message = self.download_confirmation_report_module.lambda_handler.__wrapped__.__wrapped__(
            {}, context_mock
        )

        #  We don't assert for logs here as they are raised in child function, see component tests
        self.assertEqual(return_message, 'error response')
        json_return_mock.assert_called_with(999, 'test')

    @patch(f'{module}.get_session_from_lambda_event', Mock())
    @patch(f'{module}.verify_and_parse_session', Mock(side_effect=Exception()))
    @patch(f'{module}.json_return_message')
    @patch(f'{module}.log')
    def test_lambda_handler_general_exception(self, log_mock, json_return_mock):
        context_mock = Mock()
        context_mock.function_name.return_value = 'test'

        json_return_mock.return_value = 'error response'

        return_message = self.download_confirmation_report_module.lambda_handler.__wrapped__.__wrapped__(
            {}, context_mock
        )

        self.assertEqual(return_message, 'error response')
        json_return_mock.assert_called_with(500, 'Cannot complete request')
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILEEX0004, 'error': ''})
        ])

    @patch(f'{module}.get_session_from_lambda_event', Mock(return_value='test_session'))
    @patch(f'{module}.update_metadata', Mock())
    @patch(f'{module}.get_file_body', Mock(return_value='test_confirmation_report_body'))
    @patch(f'{module}.get_file_name', Mock(return_value=VALID_REPORT_FILE_NAME))
    @patch(f'{module}.audit')
    @patch(f'{module}.verify_and_parse_session')
    def test_lambda_handler_success(self, verify_and_parse_mock, audit_mock):
        context_mock = Mock()
        context_mock.function_name.return_value = 'test'
        verify_and_parse_mock.return_value = 'test_data', {'csas': True}
        expected_confirmation_report_body = {
            'statusCode': 200,
            'headers': {'Content-Type': 'text/plain',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800',
                        'Content-Disposition': f'attachment; filename="{VALID_REPORT_FILE_NAME}"'},
            'body': 'test_confirmation_report_body',
            'isBase64Encoded': False
        }

        actual_return_message = self.download_confirmation_report_module.lambda_handler.__wrapped__.__wrapped__(
            {}, context_mock
        )

        #  We don't assert for logs here as they are raised in child function, see component tests

        self.assertEqual(expected_confirmation_report_body, actual_return_message)
        audit_mock.assert_called_with(
            session='test_session',
            action=AuditActions.CONFIRMATION_REPORT_DOWNLOADED,
            additional_information={'file_name': VALID_REPORT_FILE_NAME}
        )
