from unittest import TestCase
import json
from datetime import datetime, timezone
from mock import patch, Mock, call
from boto3.dynamodb.conditions import Or, Attr
from remind_gp_practices.email_body import pnl_nrl_reminder_email

mock_env_vars = {
    'DYNAMODB_ORGANISATION_SUPPLEMENTAL': 'DYNAMODB_TABLE',
    'EMAILS_TO_BE_SENT_QUEUE_URL': 'EMAILS_TO_BE_SENT_QUEUE_URL',
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'ENVIRONMENT_NAME': 'ENVIRONMENT_NAME',
    'SCREENING_DOMAIN_NAME': 'SCREENING_DOMAIN_NAME'
}

with patch('os.environ', mock_env_vars):
    from remind_gp_practices import remind_gp_practices

organisation_supplemental_table_mock = Mock()
sqs_client_mock = Mock()


@patch('common.utils.audit_utils.get_sqs_client', Mock())
class TestRemindGpPractices(TestCase):

    FIXED_NOW_TIME = datetime(2020, 1, 1, tzinfo=timezone.utc)
    mock_items = [
        {
            'organisation_code': 'test_org_code',
            'type_value': 'email_PNL#test@example.com',
            'organisation_supplemental_id': 'test_id'
        },
        {
            'organisation_code': 'test_org_code',
            'type_value': 'email_NRL#test@example.com',
            'organisation_supplemental_id': 'test_id'
        }
    ]

    def setUp(self):
        self.log_patcher = patch('common.log.logger')
        organisation_supplemental_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        self.log_patcher.start()
        self.maxDiff = None

    def tearDown(self):
        self.log_patcher.stop()

    @patch('datetime.datetime')
    @patch('remind_gp_practices.remind_gp_practices.get_internal_id', Mock(return_value='internal_id'))
    @patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
           Mock(return_value=organisation_supplemental_table_mock))
    def test_remind_gp_practice_test(self, audit_datetime):
        context = Mock()
        context.function_name = ''
        audit_datetime.datetime.now.return_value = self.FIXED_NOW_TIME
        organisation_supplemental_table_mock.scan.return_value = {'Items': self.mock_items}
        sqs_client_mock.send_message_batch.return_value = {
            'Successful': [
                {
                    'Id': 'test--test'
                }
            ]
        }
        remind_gp_practices.lambda_handler({}, context)

        # Check correct query is made
        organisation_supplemental_table_mock.scan.assert_has_calls([
            call(FilterExpression=Or(
                Attr('type_value').begins_with('email_PNL'),
                Attr('type_value').begins_with('email_NRL')
            )
            )
        ])
        test_email_content = pnl_nrl_reminder_email('ENVIRONMENT_NAME', 'SCREENING_DOMAIN_NAME')
        message_body_pnl = {"email_addresses": ["test@example.com"], "email_type": "email_PNL", "organisation_code": "test_org_code", "subject": f"{test_email_content.subject}", "body": f"{test_email_content.body}", "internal_id": "internal_id"}  
        message_body_nrl = {"email_addresses": ["test@example.com"], "email_type": "email_NRL", "organisation_code": "test_org_code", "subject": f"{test_email_content.subject}", "body": f"{test_email_content.body}", "internal_id": "internal_id"}  

        # Check correct messages are sent
        sqs_client_mock.send_message_batch.assert_called_once()
        _, kwargs = sqs_client_mock.send_message_batch.call_args
        expected_queue_url = 'EMAILS_TO_BE_SENT_QUEUE_URL'
        expected_entries = [
            {'Id': 'test_org_code--email_PNL', 'MessageBody': json.dumps(message_body_pnl)},
            {'Id': 'test_org_code--email_NRL', 'MessageBody': json.dumps(message_body_nrl)}
        ]
        self.assertEqual(expected_queue_url, kwargs['QueueUrl'])
        self.assertEqual(expected_entries, kwargs['Entries'])
