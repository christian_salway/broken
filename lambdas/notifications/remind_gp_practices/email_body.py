# flake8: noqa E501
import os

email_path = os.path.join(os.path.dirname(__file__), 'email_body.html')
class pnl_nrl_reminder_email:

    def __init__(self, environment_name, screeing_domain_name):
        url_base = f"{environment_name + '.' if environment_name else ''}{screeing_domain_name}"
        email_body_file = open(email_path, 'r')
        email_body_string = email_body_file.read()
        formatted_email_body = email_body_string.replace('[url]', url_base)
        self.subject = 'Screening Notification Reminder'
        self.body = formatted_email_body
