# Remind Gp Practices

## Function

This lambda will identify all records in the `organisation_supplemental_table` that are either of the type email_NRL or email_PNL and construct an email to be sent via the `send-email` lambda through the `emails-to-be-sent` queue

## Example input data

This will not require any input data as the lambda is invoked by a cloudwatch rule

## Tables used

This lambda uses the `organisation_supplemental_table` to find the relevant email records. As we only know the sort key of the index, a scan will be performed to find the relevant records followed by a filter. Keep in mind that this is a resource intensive operation