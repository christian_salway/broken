import json

from unittest import TestCase

from mock import patch, call, Mock

from remind_gp_practices.log_references import LogReference
from common.utils.audit_utils import AuditActions, AuditUsers


@patch('common.log.LOG_RECORD_META_DATA', {'internal_id': 1})
class TestRemindGpPractices(TestCase):

    module = 'remind_gp_practices.remind_gp_practices'

    test_email_record = {
      'organisation_code': 'test_org_code',
      'type_value': 'email_NRL#test@example.com'
    }

    test_email_data = {
        'email_addresses': ['test@example.com'],
        'email_type': 'email_NRL',
        'organisation_code': 'test_org_code',
        'subject': 'test_subject',
        'body': 'test_body',
        'internal_id': 'internal_id'
    }

    test_sqs_response = {
      'Successful': [
          {
            'Id': 'test_org_code--email_NRL'
          }
      ]
    }

    test_sqs_response_failures = {
        'Successful': [
            {
              'Id': 'test_org_code--email_NRL'
            }
        ],
        'Failed': [
            {
              'Id': 'fail_org_code--email_NRL'
            }
        ]
    }

    @patch('boto3.resource')
    def setUp(self, *args):
        import remind_gp_practices.remind_gp_practices as _remind_gp_practices
        self.remind_gp_practices_module = _remind_gp_practices
        self.unwrapped_handler = self.remind_gp_practices_module.lambda_handler.__wrapped__
        self.maxDiff = None

    @patch(f'{module}.pnl_nrl_reminder_email')
    def test_generate_email_data(self, email_content_mock):
        email_record = self.test_email_record
        expected_email_data = self.test_email_data

        email_object = Mock()
        email_object.body = 'test_body'
        email_object.subject = 'test_subject'
        email_content_mock.return_value = email_object
        actual_email_data = self.remind_gp_practices_module.generate_email_data(email_record, 'internal_id')

        self.assertEqual(expected_email_data, actual_email_data)

    @patch(f'{module}.log')
    def test_log_response_with_failures(self, log_mock):

        expected_log_response = 'Failure'
        expected_log_calls = [
            call({
                'log_reference': LogReference.REMINDGPEX0001,
                'organisation_code': 'fail_org_code',
                'email_type': 'email_NRL'
            }),
            call({
                'log_reference': LogReference.REMINDGP0003,
                'organisation_code': 'test_org_code',
                'email_type': 'email_NRL'
            }),
        ]
        actual_log_response = self.remind_gp_practices_module.log_response(self.test_sqs_response_failures)

        self.assertEqual(expected_log_response, actual_log_response)
        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    def test_log_response_no_failures(self, log_mock):
        expected_log_response = 'Success'
        expected_log_calls = [
            call({
                'log_reference': LogReference.REMINDGP0003,
                'organisation_code': 'test_org_code',
                'email_type': 'email_NRL'
            })
        ]
        actual_log_response = self.remind_gp_practices_module.log_response(self.test_sqs_response)

        self.assertEqual(expected_log_response, actual_log_response)
        log_mock.assert_has_calls(expected_log_calls)

    def test_build_sqs_message(self):
        test_email_object = self.test_email_data
        expected_sqs_message = [{
          'Id': 'test_org_code--email_NRL',
          'MessageBody': json.dumps(test_email_object)
        }]

        actual_sqs_message = self.remind_gp_practices_module.build_sqs_message([test_email_object])
        self.assertEqual(expected_sqs_message, actual_sqs_message)
        pass

    @patch(f'{module}.query_for_pnl_nrl_email_records')
    @patch(f'{module}.log')
    def test_lambda_handler_no_email_records(self, log_mock, query_for_pnl_nrl_email_records_mock):
        query_for_pnl_nrl_email_records_mock.return_value = {}
        expected_log_calls = [
          call({'log_reference': LogReference.REMINDGPWARN0001})
        ]

        self.unwrapped_handler({}, {})
        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.query_for_pnl_nrl_email_records')
    @patch(f'{module}.generate_email_data')
    @patch(f'{module}.build_sqs_message')
    @patch(f'{module}.send_new_message_batch')
    @patch(f'{module}.log_response')
    @patch(f'{module}.log')
    def test_lambda_handler_sent_records_with_failures(
      self, log_mock, log_response_mock, send_message_mock,
      build_message_mock, generate_email_mock, query_records_mock):
        query_records_mock.return_value = {'Items': [self.test_email_record]}
        generate_email_mock.return_value = self.test_email_data
        build_message_mock.return_value = {
          'Id': 'test_org_code--email_NRL',
          'MessageBody': json.dumps(self.test_email_data)
        }
        send_message_mock.return_value = self.test_sqs_response_failures
        log_response_mock.return_value = 'Failure'

        # Note we don't expect the failed message exception here, this happens in log_response so is mocked
        expected_log_calls = [
          call({'log_reference': LogReference.REMINDGP0001}),
          call({'log_reference': LogReference.REMINDGP0002}),
          call({'log_reference': LogReference.REMINDGP0003}),
        ]

        with self.assertRaises(Exception) as context:
            self.unwrapped_handler({}, {})

        self.assertTrue('A number of email addresses could not be put on the queue' in str(context.exception))
        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.query_for_pnl_nrl_email_records')
    @patch(f'{module}.generate_email_data')
    @patch(f'{module}.build_sqs_message')
    @patch(f'{module}.send_new_message_batch')
    @patch(f'{module}.log_response')
    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    def test_lambda_handler_send_records_no_failures(
      self, audit_mock, log_mock, log_response_mock, send_message_mock,
      build_message_mock, generate_email_mock, query_records_mock):
        query_records_mock.return_value = {'Items': [self.test_email_record]}
        generate_email_mock.return_value = self.test_email_data
        build_message_mock.return_value = {
          'Id': 'test_org_code--email_NRL',
          'MessageBody': json.dumps(self.test_email_data)
        }
        send_message_mock.return_value = self.test_sqs_response
        log_response_mock.return_value = 'Success'

        expected_log_calls = [
          call({'log_reference': LogReference.REMINDGP0001}),
          call({'log_reference': LogReference.REMINDGP0002}),
          call({'log_reference': LogReference.REMINDGP0003}),
        ]

        self.unwrapped_handler({}, {})

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_called_once()
        audit_mock.assert_called_with(AuditActions.REMIND_GP_PRACTICES, user=AuditUsers.SYSTEM)
