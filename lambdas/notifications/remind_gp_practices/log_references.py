import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    REMINDGP0001 = (logging.INFO, 'Collecting pnl and nrl organisation supplemental records')
    REMINDGP0002 = (logging.INFO, 'Generating full email data for each address')
    REMINDGP0003 = (logging.INFO, 'Sending email data to email queue')

    REMINDGPWARN0001 = (logging.WARNING, 'No pnl or nrl supplemental records')

    REMINDGPEX0001 = (logging.ERROR, 'Problem sending message to email queue')
