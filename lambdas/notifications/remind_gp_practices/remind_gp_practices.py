import json
import os
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.organisation_supplementary_utils import query_for_pnl_nrl_email_records
from common.utils.sqs_utils import send_new_message_batch
from common.log import log, get_internal_id
from remind_gp_practices.log_references import LogReference
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from remind_gp_practices.email_body import pnl_nrl_reminder_email

EMAILS_TO_BE_SENT_QUEUE_URL = os.environ.get('EMAILS_TO_BE_SENT_QUEUE_URL')
ENVIRONMENT_NAME = os.environ.get('ENVIRONMENT_NAME')
SCREENING_DOMAIN_NAME = os.environ.get('SCREENING_DOMAIN_NAME')
EMAIL_SUBJECT = 'Screening Notification Reminder'


@lambda_entry_point
def lambda_handler(event, context):

    log({'log_reference': LogReference.REMINDGP0001})
    email_records = query_for_pnl_nrl_email_records()
    if not email_records:
        log({'log_reference': LogReference.REMINDGPWARN0001})
        return

    log({'log_reference': LogReference.REMINDGP0002})
    internal_id = get_internal_id()
    email_objects = [generate_email_data(email, internal_id) for email in email_records]
    email_object_batches = [email_objects[i:i + 10] for i in range(0, len(email_objects), 10)]

    log({'log_reference': LogReference.REMINDGP0003})
    sqs_message_generator = (
        send_new_message_batch(
            EMAILS_TO_BE_SENT_QUEUE_URL,
            build_sqs_message(email_object_batch)
        )
        for email_object_batch in email_object_batches
    )

    sent_sqs_responses = [log_response(response) for response in sqs_message_generator]
    if 'Failure' in sent_sqs_responses:
        raise Exception('A number of email addresses could not be put on the queue')
    audit(AuditActions.REMIND_GP_PRACTICES, user=AuditUsers.SYSTEM)


def generate_email_data(email_record, internal_id):
    email_type, email_address = email_record['type_value'].split('#')
    email_content = pnl_nrl_reminder_email(ENVIRONMENT_NAME, SCREENING_DOMAIN_NAME)
    return {
        'email_addresses': [email_address],
        'email_type': email_type,
        'organisation_code': email_record['organisation_code'],
        'subject': email_content.subject,
        'body': email_content.body,
        'internal_id': internal_id
    }


def log_response(response_object):
    failed_messages = response_object.get('Failed')
    success_messages = response_object.get('Successful')
    for message in failed_messages if failed_messages else []:
        organisation_code, email_type = message['Id'].split('--')
        log({
            'log_reference': LogReference.REMINDGPEX0001,
            'organisation_code': organisation_code,
            'email_type': email_type
        })
    for message in success_messages if success_messages else []:
        organisation_code, email_type = message['Id'].split('--')
        log({
            'log_reference': LogReference.REMINDGP0003,
            'organisation_code': organisation_code,
            'email_type': email_type
        })
    return 'Failure' if failed_messages else 'Success'


def build_sqs_message(email_objects):
    return [
        {
            'Id': f'{email_object["organisation_code"]}--{email_object["email_type"]}',
            'MessageBody': json.dumps(email_object)
        } for email_object in email_objects
    ]
