from boto3.dynamodb.conditions import Key, Attr
import os
import tempfile
import pytz
from datetime import datetime, timezone
from uuid import uuid4

from common.utils.participant_utils import paginated_query
from common.utils.print_file_utils import (
    create_print_file_record,
    CAPITA_DATE_FORMAT
)
from common.utils.dynamodb_helper_utils import build_update_query
from create_print_file.log_references import LogReference
from common.log import log

from create_print_file.schema import CSV_DATA_SCHEMA
from common.models.letters import AWSLetterType, JobType, NotificationStatus, NotificationType
from common.utils.dynamodb_access.operations import dynamodb_update_item
from common.utils.dynamodb_access.table_names import TableNames


NEWLINE_CHARACTER = '\n'
LETTER_TO_JOB_TYPE_MAP = {
    AWSLetterType.INVITE_REMINDER: JobType.INVITE_REMINDER,
    AWSLetterType.RESULT_CEASE: JobType.RESULT_CEASE
}
JOB_NUM_UUID = uuid4()
PRINT_FILES_TO_SEND_BUCKET = os.environ.get('PRINT_FILES_TO_SEND_BUCKET')


def update_notification_record(record, s3_file_name):
    try:
        update_attributes = {
            'file_name': s3_file_name,
        }
        update_expression, expression_names, expression_values = build_update_query(update_attributes)
        key = {
            'participant_id': record['participant_id'],
            'sort_key': record['sort_key']
        }
        # Ideally we'd do some kind of batch update here, but that doesn't exist yet
        return dynamodb_update_item(
            TableNames.PARTICIPANTS,
            dict(
                Key=key,
                UpdateExpression=update_expression,
                ExpressionAttributeValues=expression_values,
                ExpressionAttributeNames=expression_names
            ))

    except Exception:
        log({
            'log_reference': LogReference.MAKEPRINTEX0004,
            'notification_id': {'participant_id': record['participant_id'], 'sort_key': record['sort_key']},
            'add_exception_info': True
        })


def query_for_invite_reminder_notifications(print_destination, callback_function, callback_args):
    condition_expression = Key('live_record_status').eq(NotificationStatus.PENDING_PRINT_FILE)
    query_arguments = {
        'IndexName': 'live-record-status-only',
        'FilterExpression': (Attr('print_destination').eq(print_destination) &
                             Attr('type').is_in([NotificationType.INVITATION, NotificationType.REMINDER]) &
                             Attr('reason_not_produced_code').not_exists()),
        'KeyConditionExpression': condition_expression
    }
    paginated_query(query_arguments, callback_function, callback_args)


def query_for_cease_result_notifications(print_destination, callback_function, callback_args):
    condition_expression = Key('live_record_status').eq(NotificationStatus.PENDING_PRINT_FILE)
    query_arguments = {
        'IndexName': 'live-record-status-only',
        'FilterExpression': (Attr('print_destination').eq(print_destination) &
                             Attr('type').is_in([NotificationType.CEASE, NotificationType.RESULT]) &
                             Attr('reason_not_produced_code').not_exists()),
        'KeyConditionExpression': condition_expression
    }
    paginated_query(query_arguments, callback_function, callback_args)


def notification_query_callback(records, participant_key_records, s3_multipart_uploader):
    with tempfile.TemporaryFile() as csvfile:
        if s3_multipart_uploader.record_count == 0:
            header_row = ','.join(CSV_DATA_SCHEMA) + NEWLINE_CHARACTER
            csvfile.write(header_row.encode('utf-8'))

        for (index, record) in enumerate(records):
            record_count = index + (s3_multipart_uploader.record_count + 1)  # record count is 1 based index
            if should_skip_test_participants() and is_test_record(record):
                log({'log_reference': LogReference.MAKEPRINTWARN0002})
                continue

            try:
                row = generate_row(record, str(record_count)).encode('utf-8')
                csvfile.write(row)
            except Exception:
                log({
                    'log_reference': LogReference.MAKEPRINTEX0001,
                    'notification_id': {'participant_id': record['participant_id'], 'sort_key': record['sort_key']},
                    'add_exception_info': True
                })
            else:
                participant_key_records.append({
                    'participant_id': record['participant_id'],
                    'sort_key': record['sort_key']
                })

        csvfile.seek(0)
        s3_multipart_uploader.upload_multipart_object(csvfile, len(records))


def generate_row(record, recno_count):
    def wrap_field(name, value):
        if name in ['JOBID', 'JOBNO', 'RECNO', 'LETTCODE', 'AJRUNDATE']:
            return value
        return f'"{value}"'

    data_object = record.get('data')
    data_object['JOBNO'] = str(JOB_NUM_UUID)
    data_object['RECNO'] = recno_count
    output_string = ','.join([wrap_field(field, data_object.get(field)) for field in CSV_DATA_SCHEMA])
    return output_string + NEWLINE_CHARACTER


def get_time_in_local_timezone(timezone_str):
    local_timezone = pytz.timezone(timezone_str)
    utc_now = datetime.now(timezone.utc)
    return utc_now.astimezone(local_timezone)


def create_s3_file_name(letter_type, print_destination, timezone):
    now_time_capita_format = get_time_in_local_timezone(timezone).strftime(CAPITA_DATE_FORMAT)
    job_type = LETTER_TO_JOB_TYPE_MAP[letter_type]
    return f'{print_destination}/CSAS_EU-WEST-2_{job_type}_{now_time_capita_format}.dat'


def should_skip_test_participants():
    return os.getenv('SKIP_TEST_PARTICIPANTS', 'TRUE') == 'TRUE'


def is_test_record(record):
    NHS_NUMBER_FIELD = 'PAT_NHS'
    return record['data'][NHS_NUMBER_FIELD].startswith('9')


def add_print_file_record(file_name, print_destination, notification_type, status, row_count):
    print_file_record = {
        'file_name': file_name,
        'sort_key': print_destination,
        'type': notification_type,
        'status': status,
        'row_count': str(row_count),
        'created': datetime.now(timezone.utc).isoformat(),
        'lock': False
    }
    try:
        create_print_file_record(print_file_record)
    except Exception:
        log({
            'log_reference': LogReference.MAKEPRINTEX0006,
            'file_name': file_name, 'type': notification_type, 'status': status, 'row_count': row_count,
            'add_exception_info': True
        })
