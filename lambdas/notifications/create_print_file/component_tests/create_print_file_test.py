import json
from unittest import TestCase
from mock import patch, Mock, MagicMock, call
from datetime import datetime, timezone
from ddt import ddt, data, unpack
from common.utils.dynamodb_access.table_names import TableNames
from create_print_file.log_references import LogReference
from common.test_resources.letter_files import (
    expected_invitation_notification_record as sample_notification_record
)


s3_client_mock = MagicMock()
datetime_mock = Mock(wraps=datetime)

participants_table_mock = Mock()
print_files_table_mock = Mock()
sqs_client_mock = Mock()
audit_sqs_client_mock = Mock()

module = 'create_print_file.create_print_file'
service_module = 'create_print_file.create_print_file_service'
print_files_table_mock = Mock()

example_date = datetime(2020, 1, 1, 10, 0, 0, 123456, tzinfo=timezone.utc)

mock_event_cic_result_cease = {
    "letter-type": "result-cease",
    "print-destination": "CIC",
    "schedule": "0 10 * * ? *",
    "timezone": "Europe/London"
}

mock_event_cic_invite_reminder = {
    "letter-type": "invite-reminder",
    "print-destination": "CIC",
    "schedule": "0 10 * * ? *",
    "timezone": "Europe/London"
}

mock_event_iom_invite_reminder = {
    "letter-type": "invite-reminder",
    "print-destination": "IOM",
    "schedule": "0 10 * * ? *",
    "timezone": "Europe/London"
}

mock_event_dms_result_cease = {
    "letter-type": "result-cease",
    "print-destination": "DMS",
    "schedule": "0 10 * * ? *",
    "timezone": "Europe/London"
}

mock_event_dms_invite_reminder = {
    "letter-type": "invite-reminder",
    "print-destination": "DMS",
    "schedule": "0 10 * * ? *",
    "timezone": "Europe/London"
}

mock_event_iom_result_cease = {
    "letter-type": "result-cease",
    "print-destination": "IOM",
    "schedule": "0 10 * * ? *",
    "timezone": "Europe/London"
}

mock_env_vars = {
    'PRINT_FILE_QUEUE_URL': 'PRINT_FILE_QUEUE_URL',
    'PRINT_FILES_TO_SEND_BUCKET': 'PRINT_FILES_TO_SEND_BUCKET',
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'NOTIFICATION_QUEUE_URL': 'NOTIFICATION_QUEUE_URL',
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE',
    'DYNAMODB_PRINT_FILES': 'PRINT_FILES_TABLE'
}


def wrapped_call(**options):
    if 'ExclusiveStartKey' in options:
        return {'Items': [sample_notification_record]}
    else:
        return {
            'Items': [sample_notification_record],
            'LastEvaluatedKey': {'some': 'key'}
        }


def get_table_side_effect(table_enum):
    tables = {
        TableNames.PARTICIPANTS: participants_table_mock,
        TableNames.PRINT_FILES: print_files_table_mock,
    }
    return tables[table_enum]


@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(side_effect=get_table_side_effect))
@patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=audit_sqs_client_mock))
@patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
@patch('common.utils.create_internal_id', Mock(return_value='1'))
@patch('common.utils.audit_utils.datetime', datetime_mock)
@patch('common.utils.schedule_utils.datetime', datetime_mock)
@patch('create_print_file.create_print_file_service.datetime', datetime_mock)
@ddt
class TestCreatePrintFile(TestCase):

    @classmethod
    @patch('os.environ', mock_env_vars)
    def setUpClass(cls):
        cls.env_patcher = patch('os.environ', mock_env_vars)

    @patch('boto3.client')
    def setUp(self, client_mock):
        self.env_patcher.start()
        self.env_patcher = patch('os.environ', mock_env_vars).start()
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = 'create_print_file'
        participants_table_mock.reset_mock()
        print_files_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        client_mock.reset_mock()
        print_files_table_mock.reset_mock()
        datetime_mock.now.return_value = example_date

        import create_print_file.create_print_file as _create_print_file
        self.create_print_file_module = _create_print_file
        self.create_print_file_module.s3_multipart_uploader.__init__('PRINT_FILES_TO_SEND_BUCKET')
        self.create_print_file_module.s3_multipart_uploader.s3_client.list_parts.return_value = {
            'Parts': [
                {
                    'PartNumber': 0,
                    'ETag': 'tag'
                }
            ]
        }

    @patch(f'{service_module}.datetime', datetime_mock)
    @patch(f'{module}.log')
    def test_lambda_handler_happy_path(self, log_mock):
        participants_table_mock.query.side_effect = wrapped_call
        participants_table_mock.update_item.return_value = {'success': 'true'}

        self.create_print_file_module.lambda_handler(mock_event_cic_result_cease, self.context)

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MAKEPRINT0001,
                  'letter_type': 'result-cease', 'print_destination': 'CIC'}),
            call({'log_reference': LogReference.MAKEPRINT0002}),
            call({'log_reference': LogReference.MAKEPRINT0003}),
            call({'log_reference': LogReference.MAKEPRINT0005}),
            call({'log_reference': LogReference.MAKEPRINT0004}),
        ])

    @patch(f'{service_module}.datetime', datetime_mock)
    @patch(f'{module}.log')
    def test_lambda_handler_incorrect_schedule_exits(self, log_mock):
        participants_table_mock.query.side_effect = wrapped_call
        participants_table_mock.update_item.return_value = {'success': 'true'}
        datetime_mock.now.return_value = datetime(2020, 5, 1, 3, 5, 1)

        self.create_print_file_module.lambda_handler(mock_event_cic_result_cease, self.context)

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MAKEPRINTSCHED0001,
                  'schedule': '0 10 * * ? *', 'timezone': 'Europe/London'}),
            call({'log_reference': LogReference.MAKEPRINTSCHED0002})
        ])

    @unpack
    @data(
        (mock_event_dms_invite_reminder, 'invite-reminder', 'DMS'),
        (mock_event_dms_result_cease, 'result-cease', 'DMS'),
        (mock_event_iom_invite_reminder, 'invite-reminder', 'IOM'),
        (mock_event_iom_result_cease, 'result-cease', 'IOM')
    )
    @patch(f'{service_module}.datetime', datetime_mock)
    @patch(f'{module}.log')
    def test_lambda_handler_non_cic(self, event, letter_type, print_destination, log_mock):
        participants_table_mock.query.side_effect = wrapped_call
        participants_table_mock.update_item.return_value = {'success': 'true'}

        self.create_print_file_module.lambda_handler(
            event,
            self.context
        )

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MAKEPRINT0001,
                  'letter_type': letter_type, 'print_destination': print_destination}),
            call({'log_reference': LogReference.MAKEPRINT0002}),
            call({'log_reference': LogReference.MAKEPRINT0003}),
            call({'log_reference': LogReference.MAKEPRINT0008, 'print_destination': print_destination}),
            call({'log_reference': LogReference.MAKEPRINT0004}),
        ])

    def test_fail_bad_s3(self):
        mock_context = Mock()
        mock_context.function_name = 'test'
        event = mock_event_cic_invite_reminder

        s3_client_mock.put_object.side_effect = RuntimeError()
        participants_table_mock.query.return_value = {'Items': [sample_notification_record]}
        self.create_print_file_module.s3_multipart_uploader.s3_client.list_parts.return_value = RuntimeError()

        with self.assertRaises(RuntimeError) as context:
            self.create_print_file_module.lambda_handler(event, mock_context)

        self.assertTrue('Failed to store CSV' in str(context.exception))
        s3_client_mock.put_object.side_effect = None

    def test_fail_bad_send_print_file_sqs(self):
        mock_context = Mock()
        mock_context.function_name = 'test'
        event = mock_event_cic_invite_reminder

        participants_table_mock.query.return_value = {'Items': [sample_notification_record]}
        sqs_client_mock.send_message.side_effect = RecursionError()

        with self.assertRaises(RuntimeError) as context:
            self.create_print_file_module.lambda_handler(event, mock_context)
        self.assertTrue('Failed to send message' in str(context.exception))

        sqs_client_mock.send_message.side_effect = None

    @patch(f'{service_module}.datetime', datetime_mock)
    @patch(f'{module}.log')
    def test_dynamo_failure(self, log_mock):
        mock_context = Mock()
        mock_context.function_name = 'test'
        event = mock_event_cic_invite_reminder

        participants_table_mock.put_item.side_effect = RuntimeError
        participants_table_mock.query.return_value = {'Items': [sample_notification_record]}

        self.create_print_file_module.lambda_handler(event, mock_context)

        sqs_client_mock.send_message.assert_has_calls([
            call(
                QueueUrl='PRINT_FILE_QUEUE_URL',
                MessageBody=json.dumps(
                    {
                        'print_file_name': 'CIC/CSAS_EU-WEST-2_RP_200101100000.dat',
                        'internal_id': '1'
                    },
                    indent=4,
                    sort_keys=True,
                    default=str
                )
            )
        ])

        audit_sqs_client_mock.send_message.assert_called_once()
        audit_sqs_client_mock.send_message.assert_called_with(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody=json.dumps({
                "action": "GENERATE_PRINT_FILE",
                "timestamp": "2020-01-01T10:00:00.123456+00:00",
                "internal_id": "1",
                "nhsid_useruid": "SYSTEM",
                "session_id": "NONE",
                "file_name": "CIC/CSAS_EU-WEST-2_RP_200101100000.dat"
            })
        )

        s3_client_mock.put_object.side_effect = None
