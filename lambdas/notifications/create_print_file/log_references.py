import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    MAKEPRINT0001 = (logging.INFO, 'Querying for notification in PENDING_PRINT_FILE status')
    MAKEPRINT0002 = (logging.INFO, 'Starting print file creation')
    MAKEPRINT0003 = (logging.INFO, 'Print file creation finished')
    MAKEPRINT0004 = (logging.INFO, 'Sending file to print bucket')
    MAKEPRINT0005 = (logging.INFO, 'Print destination is CIC, sending message to print queue')
    MAKEPRINT0006 = (logging.INFO, 'Updating notification record')
    MAKEPRINT0007 = (logging.INFO, 'Finished notification record updates')
    MAKEPRINT0008 = (logging.INFO, 'Print destination is not CIC, not sending to queue')
    MAKEPRINT0009 = (logging.INFO, 'Successfully aborted multipart upload')

    MAKEPRINTWARN0001 = (logging.WARNING, 'No notification records in PENDING_PRINT_FILE status')
    MAKEPRINTWARN0002 = (logging.WARNING, 'Is test participant record, not writing to print file')

    MAKEPRINTEX0001 = (logging.ERROR, 'Could not write record to print file')
    MAKEPRINTEX0002 = (logging.ERROR, 'Could not store print file')
    MAKEPRINTEX0003 = (logging.ERROR, 'Could not send message to print queue')
    MAKEPRINTEX0004 = (logging.ERROR, 'Could not update notification record')
    MAKEPRINTEX0005 = (logging.ERROR, 'Invalid letter type')
    MAKEPRINTEX0006 = (logging.ERROR, 'Could not create print file record')
    MAKEPRINTEX0007 = (logging.ERROR, 'Could not update print file tracking record')

    MAKEPRINTSCHED0001 = (logging.INFO, 'Checking schedule expression')
    MAKEPRINTSCHED0002 = (logging.INFO, 'Schedule expression does not match, finishing')
