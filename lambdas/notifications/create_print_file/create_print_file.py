import os
from common.log import log, get_internal_id
from create_print_file.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.sqs_utils import send_new_message
from common.utils.s3_multipart_upload_utils import S3MultiPartUpload
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.models.letters import AWSLetterType, PrintDestination, PrintFileStatus
from create_print_file.create_print_file_service import (
    query_for_invite_reminder_notifications,
    query_for_cease_result_notifications,
    create_s3_file_name,
    notification_query_callback,
    update_notification_record,
    add_print_file_record
)
from common.utils.schedule_utils import check_schedule

PRINT_FILES_TO_SEND_BUCKET = os.environ.get('PRINT_FILES_TO_SEND_BUCKET')
PRINT_FILE_QUEUE_URL = os.environ.get('PRINT_FILE_QUEUE_URL')
s3_multipart_uploader = S3MultiPartUpload(bucket=PRINT_FILES_TO_SEND_BUCKET)


@lambda_entry_point
def lambda_handler(event, context):
    s3_multipart_uploader.meta_data = {
        'internal_id': get_internal_id()
    }

    schedule = event.get('schedule')
    timezone = event.get('timezone')

    log({'log_reference': LogReference.MAKEPRINTSCHED0001, 'schedule': schedule, 'timezone': timezone})
    if not check_schedule(schedule, timezone):
        log({'log_reference': LogReference.MAKEPRINTSCHED0002})
        return

    QUERY_FUNCTION_MAP = {
        AWSLetterType.INVITE_REMINDER: query_for_invite_reminder_notifications,
        AWSLetterType.RESULT_CEASE: query_for_cease_result_notifications
    }

    letter_type = event.get('letter-type')
    print_destination = event.get('print-destination')
    log({
        'log_reference': LogReference.MAKEPRINT0001,
        'letter_type': letter_type,
        'print_destination': print_destination
    })

    s3_file_name = create_s3_file_name(letter_type, print_destination, timezone)
    participant_key_records = []
    s3_multipart_uploader.object_key = s3_file_name

    query_function = QUERY_FUNCTION_MAP.get(letter_type)
    if not query_function:
        log({'log_reference': LogReference.MAKEPRINTEX0005, 'event': letter_type})
        raise Exception('Invalid invocation event for resource')

    s3_multipart_uploader.create_multipart_upload()

    log({'log_reference': LogReference.MAKEPRINT0002})
    query_function(
        print_destination,
        notification_query_callback,
        {
            'participant_key_records': participant_key_records,
            's3_multipart_uploader': s3_multipart_uploader
        }
    )
    log({'log_reference': LogReference.MAKEPRINT0003})

    try:
        add_print_file_record(s3_file_name, print_destination, letter_type, PrintFileStatus.PENDING,
                              len(participant_key_records))
        if print_destination == PrintDestination.CIC:
            log({'log_reference': LogReference.MAKEPRINT0005})
            send_new_message(PRINT_FILE_QUEUE_URL, {'print_file_name': s3_file_name, 'internal_id': get_internal_id()})
        else:
            log({'log_reference': LogReference.MAKEPRINT0008, 'print_destination': print_destination})
    except Exception as e:
        # If we have a failure here then there is no point continuing as the entire lambda must retry
        s3_multipart_uploader.abort_multipart_upload()
        log({'log_reference': LogReference.MAKEPRINTEX0003, 'add_exception_info': True})
        raise RuntimeError('Failed to send message') from e

    log({'log_reference': LogReference.MAKEPRINT0004})
    try:
        # Try and mark the multipart upload as complete
        s3_multipart_uploader.complete_multipart_upload()
    except Exception as e:
        # If we cant send the CSV then there is no point continuing as the entire lambda must retry
        log({'log_reference': LogReference.MAKEPRINTEX0002, 'add_exception_info': True})
        s3_multipart_uploader.abort_multipart_upload()
        log({'log_reference': LogReference.MAKEPRINT0009})
        raise RuntimeError('Failed to store CSV') from e

    # Update notification records
    log({'log_reference': LogReference.MAKEPRINT0006})
    [update_notification_record(record, s3_file_name) for record in participant_key_records]
    log({'log_reference': LogReference.MAKEPRINT0007})

    audit(AuditActions.GENERATE_PRINT_FILE, user=AuditUsers.SYSTEM, file_name=s3_file_name)
