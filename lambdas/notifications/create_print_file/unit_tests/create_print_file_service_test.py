from unittest import TestCase
from datetime import datetime, timezone
from mock import patch, Mock, call, PropertyMock
from boto3.dynamodb.conditions import Key, Attr
from create_print_file.create_print_file_service import (
    update_notification_record,
    query_for_cease_result_notifications,
    query_for_invite_reminder_notifications,
    generate_row,
    should_skip_test_participants,
    get_time_in_local_timezone,
    create_s3_file_name,
    notification_query_callback,
    add_print_file_record
)
from common.models.letters import (
    AWSLetterType,
    PrintFileStatus, PrintDestination
)
from common.utils.dynamodb_access.table_names import TableNames
from ..log_references import LogReference
from common.test_resources.letter_files import (
    sample_notification_record,
    sample_csv_row as sample_processed_row
)
from ddt import ddt, data, unpack


module = 'create_print_file.create_print_file_service'
example_record = {
    'participant_id': '1234',
    'sort_key': 'NOTIFICATION',
    'data': {'PAT_NHS': '2'}
}

example_record_key = {
    'participant_id': '1234',
    'sort_key': 'NOTIFICATION'
}

test_participant_notification = {
    'participant_id': '1',
    'sort_key': 'NOTIFICATION',
    'data': {'PAT_NHS': '9'}
}

test_participant_notification_key = {
    'participant_id': '1',
    'sort_key': 'NOTIFICATION'
}


@ddt
class TestCreatePrintFileService(TestCase):

    @patch(f'{module}.datetime')
    @patch(f'{module}.dynamodb_update_item')
    @patch(f'{module}.build_update_query')
    @patch(f'{module}.log')
    def test_update_notification_record_exception(self, log_mock, build_query_mock, update_item_mock, datetime_mock):
        datetime_mock.now = Mock(return_value=datetime(2020, 7, 3, 6, 22, 23, 233, tzinfo=timezone.utc))
        build_query_mock.return_value = 'a', 'b', 'c'
        update_item_mock.side_effect = RecursionError()

        update_notification_record(example_record, 'file_name')

        update_item_mock.assert_called_with(
            TableNames.PARTICIPANTS,
            dict(
                Key=example_record_key,
                UpdateExpression='a',
                ExpressionAttributeValues='c',
                ExpressionAttributeNames='b'
            )
        )
        build_query_mock.assert_called_with({
            'file_name': 'file_name',
        })
        log_mock.assert_called_with({
            'log_reference': LogReference.MAKEPRINTEX0004,
            'notification_id': {'participant_id': '1234', 'sort_key': 'NOTIFICATION'},
            'add_exception_info': True
        })

    @patch(f'{module}.datetime')
    @patch(f'{module}.dynamodb_update_item')
    @patch(f'{module}.build_update_query')
    @patch(f'{module}.log')
    def test_update_notification_record_happy_path(self, log_mock, build_query_mock, update_item_mock, datetime_mock):
        datetime_mock.now = Mock(return_value=datetime(2020, 7, 3, 6, 22, 23, 233, tzinfo=timezone.utc))
        build_query_mock.return_value = 'a', 'b', 'c'
        update_item_mock.return_value = {
            'Attributes': {
                "string": {
                    "S": "dummy_string"
                }
            }
        }

        actual_response = update_notification_record(example_record, 'file_name')

        self.assertEqual(actual_response, {
            'Attributes': {
                "string": {
                    "S": "dummy_string"
                }
            }
        })

        update_item_mock.assert_called_with(
            TableNames.PARTICIPANTS,
            dict(
                Key=example_record_key,
                UpdateExpression='a',
                ExpressionAttributeValues='c',
                ExpressionAttributeNames='b'
            )
        )
        build_query_mock.assert_called_with({
            'file_name': 'file_name',
        })
        log_mock.assert_not_called()

    @patch(f'{module}.paginated_query')
    def test_query_for_invite_reminder_notification(self, query_mock):
        query_for_invite_reminder_notifications("abcf", 'callback function', 'args')

        query_mock.assert_called_with(
            {
                'IndexName': 'live-record-status-only',
                'KeyConditionExpression': Key('live_record_status').eq('PENDING_PRINT_FILE'),
                'FilterExpression': Attr('print_destination').eq('abcf') &
                Attr('type').is_in(['Invitation letter', 'Reminder letter']) &
                Attr('reason_not_produced_code').not_exists()
            },
            'callback function',
            'args'
        )

    @patch(f'{module}.paginated_query')
    def test_query_for_cease_result_notification(self, query_mock):
        query_for_cease_result_notifications("quebec", 'callback function', 'args')

        query_mock.assert_called_with(
            {
                'IndexName': 'live-record-status-only',
                'KeyConditionExpression': Key('live_record_status').eq('PENDING_PRINT_FILE'),
                'FilterExpression': Attr('print_destination').eq('quebec') &
                Attr('type').is_in(['Cease letter', 'Result letter']) &
                Attr('reason_not_produced_code').not_exists()
            },
            'callback function',
            'args'
        )

    @patch(f'{module}.JOB_NUM_UUID', 'test')
    def test_generate_row(self):
        csv_row = generate_row(sample_notification_record, '1')
        self.assertEqual(sample_processed_row, csv_row)

    @patch(f'{module}.should_skip_test_participants', lambda: True)
    @patch(f'{module}.generate_row')
    @patch(f'{module}.tempfile.TemporaryFile')
    @patch(f'{module}.CSV_DATA_SCHEMA', ['a', 'b'])
    def test_notification_query_callback_skip_test_participants(self, tempfile_mock, row_mock):
        csv_mock = Mock()
        tempfile_mock.return_value.__enter__.return_value = csv_mock
        multipart_mock = Mock()
        type(multipart_mock).record_count = PropertyMock(return_value=0)
        key_records = []

        records = []

        notification_query_callback(records, key_records, multipart_mock)

        multipart_mock.upload_multipart_object.assert_called_once_with(csv_mock, 0)
        csv_mock.write.assert_called_once_with('a,b\n'.encode('utf-8'))

    @patch(f'{module}.should_skip_test_participants', lambda: True)
    @patch(f'{module}.generate_row')
    @patch(f'{module}.tempfile.TemporaryFile')
    @patch(f'{module}.CSV_DATA_SCHEMA', ['a', 'b'])
    def test_notification_query_callback_no_records(self, tempfile_mock, row_mock):
        csv_mock = Mock()
        tempfile_mock.return_value.__enter__.return_value = csv_mock
        multipart_mock = Mock()
        type(multipart_mock).record_count = PropertyMock(return_value=0)
        key_records = []

        records = []

        notification_query_callback(records, key_records, multipart_mock)

        multipart_mock.upload_multipart_object.assert_called_once_with(csv_mock, 0)
        csv_mock.write.assert_called_once_with('a,b\n'.encode('utf-8'))

    @patch(f'{module}.should_skip_test_participants', lambda: True)
    @patch(f'{module}.generate_row')
    @patch(f'{module}.tempfile.TemporaryFile')
    @patch(f'{module}.CSV_DATA_SCHEMA', ['a', 'b'])
    @patch(f'{module}.log')
    def test_notification_query_callback_happy_path_header_row(self, log_mock, tempfile_mock, row_mock):
        csv_mock = Mock()
        tempfile_mock.return_value.__enter__.return_value = csv_mock
        multipart_mock = Mock()
        type(multipart_mock).record_count = PropertyMock(return_value=0)
        row_mock.return_value = 'spujb'
        key_records = []

        records = [test_participant_notification, example_record]

        notification_query_callback(records, key_records, multipart_mock)

        multipart_mock.upload_multipart_object.assert_called_once_with(csv_mock, 2)
        csv_mock.write.assert_has_calls([call(b'a,b\n'), call(b'spujb')])
        self.assertEqual(csv_mock.write.call_count, 2)
        key_records = [example_record_key]
        log_mock.assert_called_once_with({'log_reference': LogReference.MAKEPRINTWARN0002})

    @patch(f'{module}.should_skip_test_participants', lambda: False)
    @patch(f'{module}.generate_row')
    @patch(f'{module}.tempfile.TemporaryFile')
    @patch(f'{module}.CSV_DATA_SCHEMA', ['a', 'b'])
    @patch(f'{module}.log')
    def test_notification_query_callback_happy_path_no_header_row(self, log_mock, tempfile_mock, row_mock):
        csv_mock = Mock()
        tempfile_mock.return_value.__enter__.return_value = csv_mock
        multipart_mock = Mock()
        type(multipart_mock).record_count = PropertyMock(return_value=1)
        row_mock.side_effect = ['spujb1', 'spujb2']
        key_records = [{'participant_id': 'a', 'sort_key': 'n'}]

        records = [test_participant_notification, example_record]

        notification_query_callback(records, key_records, multipart_mock)

        multipart_mock.upload_multipart_object.assert_called_once_with(csv_mock, 2)
        csv_mock.write.assert_has_calls([call(b'spujb1'), call(b'spujb2')])
        self.assertEqual(csv_mock.write.call_count, 2)
        key_records = [example_record_key]
        log_mock.assert_not_called()

    @patch(f'{module}.should_skip_test_participants', lambda: False)
    @patch(f'{module}.generate_row')
    @patch(f'{module}.tempfile.TemporaryFile')
    @patch(f'{module}.CSV_DATA_SCHEMA', ['a', 'b'])
    @patch(f'{module}.log')
    def test_notification_query_callback_write_failure_exception(self, log_mock, tempfile_mock, row_mock):
        csv_mock = Mock()
        tempfile_mock.return_value.__enter__.return_value = csv_mock
        multipart_mock = Mock()
        type(multipart_mock).record_count = PropertyMock(return_value=1)
        row_mock.side_effect = [Exception('failure'), 'spujb']
        key_records = [{'participant_id': 'a', 'sort_key': 'b'}]

        records = [example_record, test_participant_notification]

        notification_query_callback(records, key_records, multipart_mock)

        multipart_mock.upload_multipart_object.assert_called_once_with(csv_mock, 2)
        csv_mock.write.assert_has_calls([call(b'spujb')])
        self.assertEqual(csv_mock.write.call_count, 1)
        self.assertEqual(key_records, [{'participant_id': 'a', 'sort_key': 'b'}, test_participant_notification_key])
        log_mock.assert_called_once_with({
            'log_reference': LogReference.MAKEPRINTEX0001,
            'notification_id': example_record_key,
            'add_exception_info': True
        })

    @unpack
    @data((2020, 1, 1, 10, 0, 0, 'Europe/London', 10),
          (2020, 5, 1, 10, 0, 0, 'Europe/London', 11))
    @patch(f'{module}.datetime')
    def test_get_time_in_local_timezone_observes_daylight_saving(self,
                                                                 year, month, day, hour, minute, second, timezone_str,
                                                                 expected_hour,  datetime_mock):
        simulated_time = datetime(year=year, month=month, day=day, hour=hour, minute=minute, second=second,
                                  tzinfo=timezone.utc)
        datetime_mock.now.return_value = simulated_time
        local_time = get_time_in_local_timezone(timezone_str)
        self.assertEqual(local_time.hour, expected_hour)

    @unpack
    @data((2020, 1, 1, 10, 0, 0, 'Europe/London', "CP/CSAS_EU-WEST-2_RP_200101100000.dat"),
          (2020, 5, 1, 10, 0, 0, 'Europe/London', "CP/CSAS_EU-WEST-2_RP_200501110000.dat"))
    @patch(f'{module}.datetime')
    def test_create_s3_file_name_invite_reminder(self,
                                                 year, month, day, hour, minute, second, timezone_str,
                                                 expected_file_name, datetime_mock):
        datetime_mock.now.return_value = datetime(year=year, month=month, day=day, hour=hour, minute=minute,
                                                  second=second, tzinfo=timezone.utc)
        file_name = create_s3_file_name(AWSLetterType.INVITE_REMINDER, 'CP', timezone_str)
        self.assertEqual(file_name, expected_file_name)

    @unpack
    @data((2020, 1, 1, 10, 0, 0, 'Europe/London', "CP/CSAS_EU-WEST-2_CP_200101100000.dat"),
          (2020, 5, 1, 10, 0, 0, 'Europe/London', "CP/CSAS_EU-WEST-2_CP_200501110000.dat"))
    @patch(f'{module}.datetime')
    def test_create_s3_file_name_result_cease(self,
                                              year, month, day, hour, minute, second, timezone_str,
                                              expected_file_name, datetime_mock):
        datetime_mock.now.return_value = datetime(year=year, month=month, day=day, hour=hour, minute=minute,
                                                  second=second, tzinfo=timezone.utc)
        file_name = create_s3_file_name(AWSLetterType.RESULT_CEASE, 'CP', timezone_str)
        self.assertEqual(file_name, expected_file_name)

    @patch(f'{module}.os.getenv', lambda *_: 'TRUE')
    def test_should_skip_test_participants_true(self):
        self.assertEqual(True, should_skip_test_participants())

    @patch(f'{module}.os.getenv', lambda *_: 'FALSE')
    def test_should_skip_test_participants_false(self):
        self.assertEqual(False, should_skip_test_participants())

    @unpack
    @data(('file1', PrintDestination.CIC, 'invite-reminder', PrintFileStatus.PENDING, 1))
    @data(('file2', PrintDestination.DMS, 'invite-reminder', PrintFileStatus.PENDING, 2))
    @data(('file3', PrintDestination.IOM, 'invite-reminder', PrintFileStatus.PENDING, 3))
    @patch(f'{module}.datetime')
    @patch(f'{module}.create_print_file_record')
    @patch(f'{module}.log')
    def test_add_print_file_record(self,
                                   file_name,
                                   print_destination,
                                   letter_type,
                                   status,
                                   row_count,
                                   log_mock,
                                   create_print_file_record_mock,
                                   datetime_mock):
        # Given
        datetime_mock.now.return_value = datetime(year=2021, month=1, day=1, hour=0,
                                                  minute=0, second=0, tzinfo=timezone.utc)
        expected_record = {
            'file_name': file_name,
            'sort_key': print_destination,
            'type': letter_type,
            'status': status,
            'row_count': str(row_count),
            'created': datetime_mock.now.return_value.isoformat(),
            'lock': False
        }
        # When
        add_print_file_record(file_name, print_destination, letter_type, status, row_count)
        # Then
        create_print_file_record_mock.assert_called_with(expected_record)
        log_mock.assert_not_called()

    @unpack
    @data(('file1', PrintDestination.CIC, 'invite-reminder', PrintFileStatus.PENDING, 1))
    @data(('file2', PrintDestination.DMS, 'invite-reminder', PrintFileStatus.PENDING, 2))
    @data(('file3', PrintDestination.IOM, 'invite-reminder', PrintFileStatus.PENDING, 3))
    @patch(f'{module}.datetime')
    @patch(f'{module}.create_print_file_record')
    @patch(f'{module}.log')
    def test_add_print_file_record_handles_exception(self,
                                                     file_name,
                                                     print_destination,
                                                     letter_type,
                                                     status,
                                                     row_count,
                                                     log_mock,
                                                     create_print_file_record_mock,
                                                     datetime_mock):
        # Given
        datetime_mock.now.return_value = datetime(year=2021, month=1, day=1, hour=0,
                                                  minute=0, second=0, tzinfo=timezone.utc)
        expected_record = {
            'file_name': file_name,
            'sort_key': print_destination,
            'type': letter_type,
            'status': status,
            'row_count': str(row_count),
            'created': datetime_mock.now.return_value.isoformat(),
            'lock': False
        }
        create_print_file_record_mock.side_effect = Exception("Something went wrong")
        # When
        add_print_file_record(file_name, print_destination, letter_type, status, row_count)
        # Then
        create_print_file_record_mock.assert_called_with(expected_record)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MAKEPRINTEX0006,
                  'file_name': file_name,
                  'type': letter_type,
                  'status': status,
                  'row_count': row_count,
                  'add_exception_info': True})
        ])
