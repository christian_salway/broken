from unittest import TestCase
from datetime import datetime, timezone
from mock import patch, Mock, call
from create_print_file.log_references import LogReference
from common.utils.audit_utils import AuditActions, AuditUsers


class TestCreatePrintFile(TestCase):

    module = 'create_print_file.create_print_file'
    module_utils = 'common.utils'
    module_service = 'create_print_file.create_print_file_service'

    example_date = datetime(2020, 1, 1, 0, 0, 0, 123456, tzinfo=timezone.utc)
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.now.return_value = example_date

    mock_env_vars = {
        'PRINT_FILE_QUEUE_URL': 'PRINT_FILE_QUEUE_URL',
        'PRINT_FILES_TO_SEND_BUCKET': 'PRINT_FILES_TO_SEND_BUCKET',
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
        'NOTIFICATION_QUEUE_URL': 'NOTIFICATION_QUEUE_URL',
        'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
        'DYNAMODB_PRINT_FILES': 'DYNAMODB_PRINT_FILES'
    }

    mock_env_vars_dev = {
        'SKIP_TEST_PARTICIPANTS': 'FALSE'
    }

    mock_env_vars_prod = {
        'SKIP_TEST_PARTICIPANTS': 'TRUE'
    }

    @patch('os.environ', mock_env_vars)
    def setUp(self, *args):
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = ''
        with patch('create_print_file.create_print_file.S3MultiPartUpload', Mock()):
            import create_print_file.create_print_file as _create_print_file
        self.create_print_file_module = _create_print_file
        self.unwrapped_handler = self.create_print_file_module.lambda_handler

        self.mock_reminder_with_schedule = {
            "schedule": "some_schedule",
            "timezone": "some_timezone"
        }

        self.mock_event_cic_result_cease = {
            "letter-type": "result-cease",
            "print-destination": "CIC"
        }

        self.mock_event_cic_invite_reminder = {
            "letter-type": "invite-reminder",
            "print-destination": "CIC",
        }

        self.mock_event_dms_result_cease = {
            "letter-type": "result-cease",
            "print-destination": "DMS"
        }

        self.mock_event_dms_invite_reminder = {
            "letter-type": "invite-reminder",
            "print-destination": "DMS"
        }

        self.mock_event_iom_result_cease = {
            "letter-type": "result-cease",
            "print-destination": "IOM"
        }

        self.mock_event_iom_invite_reminder = {
            "letter-type": "invite-reminder",
            "print-destination": "IOM"
        }

    def tearDown(self):
        self.log_patcher.stop()

    @patch(f'{module}.create_s3_file_name')
    @patch(f'{module}.get_internal_id')
    @patch(f'{module}.log')
    @patch(f'{module}.query_for_invite_reminder_notifications')
    @patch(f'{module}.send_new_message')
    @patch(f'{module}.audit')
    @patch(f'{module}.notification_query_callback')
    @patch(f'{module}.add_print_file_record')
    @patch(f'{module}.check_schedule')
    def test_lambda_handler_observes_schedule(self, check_schedule_mock, add_print_file_record_mock,
                                              notification_query_callback_mock, audit_mock,
                                              send_new_message_mock,
                                              query_for_invite_reminder_notifications_mock, log_mock,
                                              get_internal_id_mock, create_s3_file_name_mock):
        check_schedule_mock.return_value = False
        context = Mock()
        context.function_name = ''
        MultipartMock = Mock()
        self.create_print_file_module.s3_multipart_uploader = MultipartMock

        self.create_print_file_module.lambda_handler(self.mock_reminder_with_schedule, context)

        check_schedule_mock.assert_called_once()

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MAKEPRINTSCHED0001,
                  'schedule': 'some_schedule',
                  'timezone': 'some_timezone'}),
            call({'log_reference': LogReference.MAKEPRINTSCHED0002})
        ])

        MultipartMock.create_multipart_upload.assert_not_called()
        MultipartMock.complete_multipart_upload.assert_not_called()

        add_print_file_record_mock.assert_not_called()
        notification_query_callback_mock.assert_not_called()
        audit_mock.assert_not_called()
        send_new_message_mock.assert_not_called()
        query_for_invite_reminder_notifications_mock.assert_not_called()
        get_internal_id_mock.assert_called_once()
        create_s3_file_name_mock.assert_not_called()

    @patch(f'{module}.create_s3_file_name', Mock(return_value='test_file_name'))
    @patch(f'{module}.get_internal_id', Mock(return_value='1'))
    @patch(f'{module}.log')
    @patch(f'{module}.query_for_invite_reminder_notifications')
    @patch(f'{module}.send_new_message')
    @patch(f'{module}.audit')
    @patch(f'{module}.notification_query_callback')
    @patch(f'{module}.add_print_file_record')
    @patch(f'{module}.check_schedule')
    def test_lambda_handler_happy_path_cic(self, check_schedule_mock, add_print_file_record_mock,
                                           notification_query_callback_mock, audit_mock, send_new_message_mock,
                                           query_for_invite_reminder_notifications_mock, log_mock):
        check_schedule_mock.return_value = True
        context = Mock()
        context.function_name = ''
        MultipartMock = Mock()
        self.create_print_file_module.s3_multipart_uploader = MultipartMock

        self.create_print_file_module.lambda_handler(self.mock_event_cic_invite_reminder, context)

        self.assertEqual(self.create_print_file_module.s3_multipart_uploader.object_key, 'test_file_name')
        MultipartMock.create_multipart_upload.assert_called_once()
        query_for_invite_reminder_notifications_mock.assert_called_once_with(
            "CIC",
            notification_query_callback_mock,
            {
                'participant_key_records': [],
                's3_multipart_uploader': MultipartMock
            }
        )
        send_new_message_mock.assert_called_once_with(
            'PRINT_FILE_QUEUE_URL',
            {
                'print_file_name': 'test_file_name',
                'internal_id': '1'
            }
        )
        add_print_file_record_mock.assert_called_once_with('test_file_name', 'CIC', 'invite-reminder', 'PENDING', 0)

        MultipartMock.complete_multipart_upload.assert_called_once()
        log_mock.assert_has_calls([
            call({
                'log_reference': LogReference.MAKEPRINT0001,
                'letter_type': 'invite-reminder',
                'print_destination': 'CIC'
            }),
            call({'log_reference': LogReference.MAKEPRINT0002}),
            call({'log_reference': LogReference.MAKEPRINT0003}),
            call({'log_reference': LogReference.MAKEPRINT0005}),
            call({'log_reference': LogReference.MAKEPRINT0004})
        ])

        audit_mock.assert_called_once_with(
            AuditActions.GENERATE_PRINT_FILE,
            user=AuditUsers.SYSTEM,
            file_name='test_file_name'
        )

    @patch(f'{module}.create_s3_file_name', Mock(return_value='test_file_name'))
    @patch(f'{module}.get_internal_id', Mock(return_value='1'))
    @patch(f'{module}.log')
    @patch(f'{module}.query_for_invite_reminder_notifications')
    @patch(f'{module}.send_new_message')
    @patch(f'{module}.audit')
    @patch(f'{module}.notification_query_callback')
    @patch(f'{module}.add_print_file_record')
    @patch(f'{module}.check_schedule')
    def test_lambda_handler_sends_no_message_for_dms(self, check_schedule_mock, add_print_file_record_mock,
                                                     callback_mock, audit_mock, send_new_message_mock,
                                                     query_for_invite_reminder_notifications_mock, log_mock):
        check_schedule_mock.return_value = True
        context = Mock()
        context.function_name = ''
        MultipartMock = Mock()
        self.create_print_file_module.s3_multipart_uploader = MultipartMock

        self.create_print_file_module.lambda_handler(self.mock_event_dms_invite_reminder, context)

        self.assertEqual(self.create_print_file_module.s3_multipart_uploader.object_key, 'test_file_name')
        MultipartMock.create_multipart_upload.assert_called_once()
        query_for_invite_reminder_notifications_mock.assert_called_once_with(
            "DMS",
            callback_mock,
            {
                'participant_key_records': [],
                's3_multipart_uploader': MultipartMock
            }
        )
        send_new_message_mock.assert_not_called()
        add_print_file_record_mock.assert_called_once_with('test_file_name', 'DMS', 'invite-reminder', 'PENDING', 0)
        MultipartMock.complete_multipart_upload.assert_called_once()
        log_mock.assert_has_calls([
            call({
                'log_reference': LogReference.MAKEPRINT0001,
                'letter_type': 'invite-reminder',
                'print_destination': 'DMS'
            }),
            call({'log_reference': LogReference.MAKEPRINT0002}),
            call({'log_reference': LogReference.MAKEPRINT0003}),
            call({'log_reference': LogReference.MAKEPRINT0008, 'print_destination': 'DMS'}),
            call({'log_reference': LogReference.MAKEPRINT0004})
        ])

        audit_mock.assert_called_once_with(
            AuditActions.GENERATE_PRINT_FILE,
            user=AuditUsers.SYSTEM,
            file_name='test_file_name'
        )

    @patch(f'{module}.create_s3_file_name', Mock(return_value='test_file_name'))
    @patch(f'{module}.get_internal_id', Mock(return_value='1'))
    @patch(f'{module}.log')
    @patch(f'{module}.check_schedule')
    def test_lambda_handler_exception_on_invalid_letter_type(self, check_schedule_mock, log_mock):
        check_schedule_mock.return_value = True
        test_context = Mock()
        test_context.function_name = ''
        MultipartMock = Mock()
        self.create_print_file_module.s3_multipart_uploader = MultipartMock

        with self.assertRaises(Exception) as context:
            self.create_print_file_module.lambda_handler(
                {
                    "letter-type": "invalid",
                    "print-destination": "INVALID"
                },
                test_context
            )
        self.assertTrue('Invalid invocation event for resource' in str(context.exception))
        self.assertEqual(self.create_print_file_module.s3_multipart_uploader.object_key, 'test_file_name')
        log_mock.assert_has_calls([
            call({
                'log_reference': LogReference.MAKEPRINT0001,
                'letter_type': 'invalid',
                'print_destination': 'INVALID'
            }),
            call({'log_reference': LogReference.MAKEPRINTEX0005, 'event': 'invalid'})
        ])

    @patch(f'{module}.create_s3_file_name', Mock(return_value='test_file_name'))
    @patch(f'{module}.get_internal_id', Mock(return_value='1'))
    @patch(f'{module}.log')
    @patch(f'{module}.check_schedule')
    @patch(f'{module}.send_new_message', Mock())
    @patch(f'{module}.query_for_invite_reminder_notifications', Mock())
    @patch(f'{module}.notification_query_callback', Mock())
    @patch(f'{module}.add_print_file_record', Mock())
    def test_lambda_handler_exception_on_multipart_upload_failure(self, check_schedule_mock, log_mock):
        check_schedule_mock.return_value = True
        test_context = Mock()
        test_context.function_name = ''
        MultipartMock = Mock()
        MultipartMock.complete_multipart_upload.side_effect = Exception()
        self.create_print_file_module.s3_multipart_uploader = MultipartMock

        with self.assertRaises(Exception) as context:
            self.create_print_file_module.lambda_handler(self.mock_event_cic_invite_reminder, test_context)
        self.assertTrue('Failed to store CSV' in str(context.exception))
        self.assertEqual(self.create_print_file_module.s3_multipart_uploader.object_key, 'test_file_name')
        MultipartMock.abort_multipart_upload.assert_called_once()
        log_mock.assert_has_calls([
            call({
                'log_reference': LogReference.MAKEPRINT0001,
                'letter_type': 'invite-reminder',
                'print_destination': 'CIC'
            }),
            call({'log_reference': LogReference.MAKEPRINT0002}),
            call({'log_reference': LogReference.MAKEPRINT0003}),
            call({'log_reference': LogReference.MAKEPRINT0005}),
            call({'log_reference': LogReference.MAKEPRINT0004}),
            call({'log_reference': LogReference.MAKEPRINTEX0002, 'add_exception_info': True}),
            call({'log_reference': LogReference.MAKEPRINT0009})
        ])
        MultipartMock.complete_multipart_upload.side_effect = None
