import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    RESENDNOTIF0001 = (logging.INFO, 'Searching for notification with PENDING_PRINT_FILE status.')
    RESENDNOTIF0002 = (logging.INFO, 'No notification with status PENDING_PRINT_FILE found. '
                                     'Finding most recent SENT notification instead.')
    RESENDNOTIF0003 = (logging.INFO, 'Resending most recent notification.')
    RESENDNOTIF0004 = (logging.INFO, 'Most recent notification is beyond maximum age to resend.')
    RESENDNOTIF0005 = (logging.ERROR, 'No sent letters found.')
