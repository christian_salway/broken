import json
from datetime import datetime, date, timezone

from boto3.dynamodb.conditions import Key, Attr
from dateutil.relativedelta import relativedelta

from common.log import log
from common.models.letters import NotificationStatus
from common.models.participant import ParticipantSortKey
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.utils.dynamodb_helper_utils import build_filter_expression
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_utils import get_participant_by_participant_id
from common.utils.sqs_utils import read_message_from_queue
from common.utils.result_letter_utils import result_is_older_than_cut_off
from common.utils.dynamodb_access.operations import dynamodb_query, dynamodb_put_item
from resend_notifications.log_references import LogReference
from common.utils.dynamodb_access.table_names import TableNames

NON_RESULT_LETTER_MAXIMUM_AGE_IN_MONTHS = 12


@lambda_entry_point
def lambda_handler(event, context):
    event_json = json.loads(read_message_from_queue(event))
    participant_id = event_json.get('participant_id')
    participant = get_participant_by_participant_id(participant_id)

    log({'log_reference': LogReference.RESENDNOTIF0001})
    letter_to_resend_record = get_to_send_letter(participant_id)

    if not letter_to_resend_record:
        log({'log_reference': LogReference.RESENDNOTIF0002})
        letter_to_resend_record = get_last_sent_letter(participant_id)

    if not letter_to_resend_record:
        log({'log_reference': LogReference.RESENDNOTIF0005})
        return

    original_send_date = get_original_send_date(letter_to_resend_record)

    can_resend = get_can_letter_be_resent(letter_to_resend_record['record_id'], original_send_date)

    if can_resend:
        log({'log_reference': LogReference.RESENDNOTIF0003})
        create_notification_record(letter_to_resend_record, original_send_date, participant)

        additional_information_for_audit = {'notification_type': letter_to_resend_record['type'],
                                            'address': participant.get('address', {})}

        audit(
            action=AuditActions.AUTOMATED_RESEND_NOTIFICATION,
            user=AuditUsers.SYSTEM,
            participant_ids=[participant_id],
            additional_information=additional_information_for_audit
        )
    else:
        log({'log_reference': LogReference.RESENDNOTIF0004})


def get_original_send_date(letter_to_resend_record):
    return date.fromisoformat(letter_to_resend_record['original_send_date']) \
        if letter_to_resend_record.get('original_send_date') \
        else datetime.fromisoformat(letter_to_resend_record['notification_date']).date()


def get_can_letter_be_resent(record_id, original_send_date):
    today = datetime.now(timezone.utc).date()
    if record_id.startswith('RESULT'):
        test_date_string = record_id.split('#')[1]
        test_date = datetime.strptime(test_date_string, '%Y-%m-%d').date()
        return not result_is_older_than_cut_off(test_date)
    latest_date_to_resend = original_send_date + relativedelta(months=NON_RESULT_LETTER_MAXIMUM_AGE_IN_MONTHS)
    return today <= latest_date_to_resend


# ensures only one letter resent per participant per print file run
def get_to_send_letter(participant_id):
    condition_expression = Key('participant_id').eq(participant_id) & Key('sort_key').begins_with('NOTIFICATION')
    filter_expression = [
        Attr('status').eq(NotificationStatus.PENDING_PRINT_FILE),
        Attr('original_send_date').exists()
    ]
    records = dynamodb_query(TableNames.PARTICIPANTS, dict(
        KeyConditionExpression=condition_expression,
        FilterExpression=build_filter_expression(filter_expression),
        ScanIndexForward=False
    ))
    return records[0] if records else None


def get_last_sent_letter(participant_id):
    condition_expression = Key('participant_id').eq(participant_id) & Key('sort_key').begins_with('NOTIFICATION')
    filter_expression = [Attr('status').eq(NotificationStatus.SENT)]

    records = dynamodb_query(TableNames.PARTICIPANTS, {
        "KeyConditionExpression": condition_expression,
        "FilterExpression": build_filter_expression(filter_expression),
        "ScanIndexForward": False
    })

    return records[0] if records else None


def create_notification_record(original_notification_record, original_send_date, participant):
    if original_notification_record['status'] == NotificationStatus.PENDING_PRINT_FILE:
        # ensures existing PENDING_PRINT_FILE resend notification is replaced
        timestamp = original_notification_record['created']
    else:
        timestamp = datetime.now(timezone.utc).isoformat()

    notification = {
        'participant_id': original_notification_record['participant_id'],
        'sort_key': f'{ParticipantSortKey.NOTIFICATION}#{timestamp}',
        'created': timestamp,
        'status': NotificationStatus.PENDING_PRINT_FILE,
        'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
        'type': original_notification_record['type'],
        'print_destination': original_notification_record['print_destination'],
        'record_id': original_notification_record['record_id'],
        'data': update_notification_data_participant_information(original_notification_record, participant),
        'original_send_date': original_send_date.isoformat()
    }
    dynamodb_put_item(TableNames.PARTICIPANTS, notification)


def update_notification_data_participant_information(original_notification_record, participant):
    original_notification_data = original_notification_record['data']
    address = participant.get('address', {})
    updated_data = {
        'PAT_TITLE': participant.get('title', ''),
        'PAT_SURNAME': participant.get('last_name', ''),
        'PAT_FORENAME': participant.get('first_name', ''),
        'PAT_ADD1': address.get('address_line_1', ''),
        'PAT_ADD2': address.get('address_line_2', ''),
        'PAT_ADD3': address.get('address_line_3', ''),
        'PAT_ADD4': address.get('address_line_4', ''),
        'PAT_ADD5': address.get('address_line_5', ''),
        'PAT_POSTCODE': address.get('postcode', ''),
        'DEST_ADD1': address.get('address_line_1', ''),
        'DEST_ADD2': address.get('address_line_2', ''),
        'DEST_ADD3': address.get('address_line_3', ''),
        'DEST_ADD4': address.get('address_line_4', ''),
        'DEST_ADD5': address.get('address_line_5', ''),
        'DEST_POSTCODE': address.get('postcode', ''),
    }

    return {**original_notification_data, **updated_data}
