This lambda recreates notification records so that letters can be resent. 

It is triggered by a message containing participant_id from the SQS queue notifications to resend. 

It finds the most recent letter sent by:
- First checking there are no letters in PENDING_PRINT_FILE status with original_send_date field (as this indicates a resend and we only resend once per letter generation cycle)
- Otherwise find the most recent letter with IN_PRINT_FILE status

It then determines if it can resend this letter by:
- If it is a result letter, as indicated by the record_id starting with a result sort_key RESULT#, the test_date of the result cannot be older than 3 months,
  otherwise it cannot be older than 12 months based on the notification date
- If it is a resend then the above rules apply to original_send_date

If the letter can be resent, then a new notification record is created, copying many fields from the original notification.
Beofre the notification record is saved, the 'data' object is updated with the participants current name and address details. 