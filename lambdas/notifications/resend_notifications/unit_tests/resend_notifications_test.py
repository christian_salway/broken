from datetime import datetime, date
from unittest import TestCase

from boto3.dynamodb.conditions import Key, Attr
from ddt import ddt, data, unpack
from mock import patch, Mock, call

from common.models.letters import NotificationStatus
from common.utils.audit_utils import AuditActions, AuditUsers
from resend_notifications.log_references import LogReference
from resend_notifications import resend_notifications
from common.utils.dynamodb_access.table_names import TableNames

FIXED_NOW_TIME = datetime(2020, 12, 27, 9, 45, 23, 123456)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = FIXED_NOW_TIME
cut_off_mock = Mock()


@patch('boto3.resource', Mock())
@patch('resend_notifications.resend_notifications.datetime', datetime_mock)
@patch('resend_notifications.resend_notifications.result_is_older_than_cut_off', cut_off_mock)
@ddt
class TestResendNotifications(TestCase):

    module = 'resend_notifications.resend_notifications'

    @patch(f'{module}.log')
    @patch(f'{module}.create_notification_record')
    @patch(f'{module}.get_can_letter_be_resent')
    @patch(f'{module}.get_original_send_date')
    @patch(f'{module}.get_last_sent_letter')
    @patch(f'{module}.get_to_send_letter')
    @patch(f'{module}.get_participant_by_participant_id')
    def test_resend_notifications_exits_if_no_letters_to_resend(
            self, get_participant_mock, get_to_send_letter_mock, get_last_sent_letter_mock, get_original_send_date_mock,
            get_can_letter_be_resent_mock, create_notification_record_mock, log_mock):
        event = {'Records': [{'body': '{"participant_id": "123456"}'}]}
        get_participant_mock.return_value = {'Item': {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}}
        get_to_send_letter_mock.return_value = None
        get_last_sent_letter_mock.return_value = None

        resend_notifications.lambda_handler.__wrapped__(event, {})

        get_participant_mock.assert_called_with('123456')
        get_to_send_letter_mock.assert_called_with('123456')
        get_last_sent_letter_mock.assert_called_with('123456')
        get_original_send_date_mock.assert_not_called()
        get_can_letter_be_resent_mock.assert_not_called()
        create_notification_record_mock.assert_not_called()

        expected_log_calls = [
            call({'log_reference': LogReference.RESENDNOTIF0001}),
            call({'log_reference': LogReference.RESENDNOTIF0002}),
            call({'log_reference': LogReference.RESENDNOTIF0005})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.create_notification_record')
    @patch(f'{module}.get_can_letter_be_resent')
    @patch(f'{module}.get_original_send_date')
    @patch(f'{module}.get_last_sent_letter')
    @patch(f'{module}.get_to_send_letter')
    @patch(f'{module}.get_participant_by_participant_id')
    def test_resend_notifications_does_not_resend_if_past_allowed_time_limit(
            self, get_participant_mock, get_to_send_letter_mock, get_last_sent_letter_mock, get_original_send_date_mock,
            get_can_letter_be_resent_mock, create_notification_record_mock, log_mock):
        event = {'Records': [{'body': '{"participant_id": "123456"}'}]}
        participant = {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        notification_record = {'sort_key': 'NOTIFICATION', 'record_id': 'original_sort_key'}
        get_participant_mock.return_value = {'Item': participant}
        get_to_send_letter_mock.return_value = notification_record
        get_original_send_date_mock.return_value = 'the_original_send_date'
        get_can_letter_be_resent_mock.return_value = False

        resend_notifications.lambda_handler.__wrapped__(event, {})

        get_participant_mock.assert_called_with('123456')
        get_to_send_letter_mock.assert_called_with('123456')
        get_last_sent_letter_mock.assert_not_called()
        get_original_send_date_mock.assert_called_with(notification_record)
        get_can_letter_be_resent_mock.assert_called_with('original_sort_key', 'the_original_send_date')
        create_notification_record_mock.assert_not_called()

        expected_log_calls = [
            call({'log_reference': LogReference.RESENDNOTIF0001}),
            call({'log_reference': LogReference.RESENDNOTIF0004})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.create_notification_record')
    @patch(f'{module}.get_can_letter_be_resent')
    @patch(f'{module}.get_original_send_date')
    @patch(f'{module}.get_last_sent_letter')
    @patch(f'{module}.get_to_send_letter')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.audit')
    def test_resend_notifications_can_resend_a_notification_with_PENDING_PRINT_FILE_status(
            self, audit_mock, get_participant_mock, get_to_send_letter_mock, get_last_sent_letter_mock,
            get_original_send_date_mock, get_can_letter_be_resent_mock, create_notification_record_mock, log_mock):
        event = {'Records': [{'body': '{"participant_id": "123456"}'}]}
        participant = {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        notification_record = {'sort_key': 'NOTIFICATION',
                               'record_id': 'original_sort_key',
                               'type': 'a letter type'}
        get_participant_mock.return_value = participant
        get_to_send_letter_mock.return_value = notification_record
        get_original_send_date_mock.return_value = 'the_original_send_date'
        get_can_letter_be_resent_mock.return_value = True

        resend_notifications.lambda_handler.__wrapped__(event, {})

        get_participant_mock.assert_called_with('123456')
        get_to_send_letter_mock.assert_called_with('123456')
        get_last_sent_letter_mock.assert_not_called()
        get_original_send_date_mock.assert_called_with(notification_record)
        get_can_letter_be_resent_mock.assert_called_with('original_sort_key', 'the_original_send_date')
        create_notification_record_mock.assert_called_with(notification_record, 'the_original_send_date', participant)
        audit_mock.assert_called_once_with(
            action=AuditActions.AUTOMATED_RESEND_NOTIFICATION,
            user=AuditUsers.SYSTEM,
            participant_ids=['123456'],
            additional_information={'notification_type': 'a letter type',
                                    'address': {}}
        )

        expected_log_calls = [
            call({'log_reference': LogReference.RESENDNOTIF0001}),
            call({'log_reference': LogReference.RESENDNOTIF0003})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.create_notification_record')
    @patch(f'{module}.get_can_letter_be_resent')
    @patch(f'{module}.get_original_send_date')
    @patch(f'{module}.get_last_sent_letter')
    @patch(f'{module}.get_to_send_letter')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.audit')
    def test_resend_notifications_can_resend_a_notification_with_sent_status(
            self, audit_mock, get_participant_mock, get_to_send_letter_mock, get_last_sent_letter_mock,
            get_original_send_date_mock, get_can_letter_be_resent_mock, create_notification_record_mock, log_mock):
        event = {'Records': [{'body': '{"participant_id": "123456"}'}]}
        participant = {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        notification_record = {'sort_key': 'NOTIFICATION',
                               'record_id': 'original_sort_key',
                               'type': 'a letter type'}
        get_participant_mock.return_value = participant
        get_to_send_letter_mock.return_value = None
        get_last_sent_letter_mock.return_value = notification_record
        get_original_send_date_mock.return_value = 'the_original_send_date'
        get_can_letter_be_resent_mock.return_value = True

        resend_notifications.lambda_handler.__wrapped__(event, {})

        get_participant_mock.assert_called_with('123456')
        get_to_send_letter_mock.assert_called_with('123456')
        get_last_sent_letter_mock.assert_called_with('123456')
        get_original_send_date_mock.assert_called_with(notification_record)
        get_can_letter_be_resent_mock.assert_called_with('original_sort_key', 'the_original_send_date')
        create_notification_record_mock.assert_called_with(notification_record, 'the_original_send_date', participant)
        audit_mock.assert_called_once_with(
            action=AuditActions.AUTOMATED_RESEND_NOTIFICATION,
            user=AuditUsers.SYSTEM,
            participant_ids=['123456'],
            additional_information={'notification_type': 'a letter type',
                                    'address': {}}
        )

        expected_log_calls = [
            call({'log_reference': LogReference.RESENDNOTIF0001}),
            call({'log_reference': LogReference.RESENDNOTIF0002}),
            call({'log_reference': LogReference.RESENDNOTIF0003})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    def test_get_original_send_date_returns_original_send_date_if_letter_resending_was_a_resend_itself(self):
        letter_to_resend_record = {'original_send_date': '2020-06-21'}
        expected_original_send_date = date(2020, 6, 21)
        actual_original_send_date = resend_notifications.get_original_send_date(letter_to_resend_record)
        self.assertEqual(expected_original_send_date, actual_original_send_date)

    def test_get_original_send_date_returns_notification_date_if_letter_resending_was_not_a_resend_itself(self):
        letter_to_resend_record = {'notification_date': '2020-06-23T09:45:23.123'}
        expected_original_send_date = date(2020, 6, 23)
        actual_original_send_date = resend_notifications.get_original_send_date(letter_to_resend_record)
        self.assertEqual(expected_original_send_date, actual_original_send_date)

    @unpack
    @data(
        ('PARTICIPANT', date(2019, 12, 26), False),
        ('PARTICIPANT', date(2019, 12, 27), True),
        ('PARTICIPANT', date(2019, 12, 28), True),
        ('RESULT#2020-09-26#2020-09-28T12:01:35.45612', date(2018, 12, 25), False),
        ('RESULT#2020-09-27#2020-09-28T12:01:35.45612', date(2018, 12, 25), True),
        ('RESULT#2020-09-28#2020-09-28T12:01:35.45612', date(2018, 12, 25), True)
    )
    def test_can_letter_be_resent_obeys_time_limits_for_resending_based_on_record_type(
            self, record_id, original_send_date, expected_can_be_resent):
        # Arrange
        cut_off_mock.return_value = not expected_can_be_resent
        # Act
        actual_can_be_resent = resend_notifications.get_can_letter_be_resent(record_id, original_send_date)
        # Assert
        self.assertEqual(expected_can_be_resent, actual_can_be_resent)

    @patch(f'{module}.dynamodb_query')
    def test_get_to_send_letter_returns_letter_with_PENDING_PRINT_FILE_status_if_exists(self, dynamodb_query_mock):
        participant_id = '123456'
        items = [{'sort_key': 'letter 1', 'status': NotificationStatus.PENDING_PRINT_FILE}]
        expected_record = {'sort_key': 'letter 1', 'status': NotificationStatus.PENDING_PRINT_FILE}

        dynamodb_query_mock.return_value = items

        actual_record = resend_notifications.get_to_send_letter(participant_id)

        self.assertDictEqual(expected_record, actual_record)

        dynamodb_query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=Key('participant_id').eq(participant_id) & Key('sort_key')
            .begins_with('NOTIFICATION'),
            FilterExpression=Attr('status').eq(NotificationStatus.PENDING_PRINT_FILE) & Attr('original_send_date')
            .exists(),
            ScanIndexForward=False,
        ))

    @patch(f'{module}.dynamodb_query')
    def test_get_to_send_letter_returns_none_if_no_letter_with_PENDING_PRINT_FILE_status(self, dynamodb_query_mock):
        participant_id = '123456'
        dynamodb_query_mock.return_value = []

        actual_record = resend_notifications.get_to_send_letter(participant_id)

        self.assertEqual(None, actual_record)
        dynamodb_query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=Key('participant_id').eq(participant_id) & Key('sort_key')
            .begins_with('NOTIFICATION'),
            FilterExpression=Attr('status').eq(NotificationStatus.PENDING_PRINT_FILE) & Attr('original_send_date')
            .exists(),
            ScanIndexForward=False,
        ))

    @patch(f'{module}.dynamodb_query')
    def test_get_last_sent_letter_returns_most_recent_sent_letter_if_exists(self, dynamodb_query_mock):
        participant_id = '123456'
        items = [{'sort_key': 'notification 1', 'status': NotificationStatus.SENT.value},
                 {'sort_key': 'notification 2', 'status': NotificationStatus.SENT.value}]
        dynamodb_query_mock.return_value = items
        expected_record = {'sort_key': 'notification 1', 'status': NotificationStatus.SENT.value}

        actual_record = resend_notifications.get_last_sent_letter(participant_id)

        self.assertDictEqual(expected_record, actual_record)

        dynamodb_query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=Key('participant_id').eq(participant_id) & Key('sort_key')
            .begins_with('NOTIFICATION'),
            FilterExpression=Attr('status').eq(NotificationStatus.SENT),
            ScanIndexForward=False,
        ))

    @patch(f'{module}.dynamodb_query')
    def test_get_last_sent_letter_returns_none_if_no_most_recent_sent_letter(self, dynamodb_query_mock):
        participant_id = '123456'
        dynamodb_query_mock.return_value = []

        actual_record = resend_notifications.get_last_sent_letter(participant_id)

        self.assertEqual(None, actual_record)

        dynamodb_query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=Key('participant_id').eq(participant_id) & Key('sort_key')
            .begins_with('NOTIFICATION'),
            FilterExpression=Attr('status').eq(NotificationStatus.SENT),
            ScanIndexForward=False,
        ))

    @patch(f'{module}.dynamodb_put_item')
    @patch(f'{module}.update_notification_data_participant_information')
    def test_create_notification_record_builds_and_saves_record_for_sent_notifications(
            self, update_data_mock, dynamodb_put_mock):
        original_notification_record = {'participant_id': '123456', 'type': 'a_letter_type',
                                        'print_destination': 'the_printer', 'record_id': 'a_sort_key',
                                        'data': {'original_data_key': 'original_data_value'},
                                        'status': NotificationStatus.SENT.value}
        original_send_date = date(2020, 12, 3)
        participant = {'participant_key': 'participant_value'}
        update_data_mock.return_value = {'updated_key': 'updated_value'}

        expected_notification_record = {'participant_id': '123456',
                                        'sort_key': 'NOTIFICATION#2020-12-27T09:45:23.123456',
                                        'created': '2020-12-27T09:45:23.123456',
                                        'status': NotificationStatus.PENDING_PRINT_FILE,
                                        'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
                                        'type': 'a_letter_type',
                                        'print_destination': 'the_printer', 'record_id': 'a_sort_key',
                                        'data': {'updated_key': 'updated_value'},
                                        'original_send_date': '2020-12-03'}

        resend_notifications.create_notification_record(original_notification_record,
                                                        original_send_date,
                                                        participant)

        dynamodb_put_mock.assert_called_with(TableNames.PARTICIPANTS, expected_notification_record)

    @patch(f'{module}.dynamodb_put_item')
    @patch(f'{module}.update_notification_data_participant_information')
    def test_create_notification_record_builds_and_saves_record_for_PENDING_PRINT_FILE_notifications(
            self, update_data_mock, dynamodb_put_mock):
        original_notification_record = {'participant_id': '123456', 'type': 'a_letter_type',
                                        'print_destination': 'the_printer', 'record_id': 'a_sort_key',
                                        'data': {'original_data_key': 'original_data_value'},
                                        'status': NotificationStatus.PENDING_PRINT_FILE,
                                        'created': '2020-12-25T13:36:18.000'}
        original_send_date = date(2020, 12, 3)
        participant = {'participant_key': 'participant_value'}
        update_data_mock.return_value = {'updated_key': 'updated_value'}

        expected_notification_record = {'participant_id': '123456', 'sort_key': 'NOTIFICATION#2020-12-25T13:36:18.000',
                                        'created': '2020-12-25T13:36:18.000',
                                        'status': NotificationStatus.PENDING_PRINT_FILE,
                                        'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
                                        'type': 'a_letter_type',
                                        'print_destination': 'the_printer', 'record_id': 'a_sort_key',
                                        'data': {'updated_key': 'updated_value'},
                                        'original_send_date': '2020-12-03'}

        resend_notifications.create_notification_record(original_notification_record,
                                                        original_send_date,
                                                        participant)

        dynamodb_put_mock.assert_called_with(TableNames.PARTICIPANTS, expected_notification_record)

    def test_update_notification_data_participant_information_updates_name_and_address(self):
        original_notification_data = {'PAT_TITLE': 'old_title', 'PAT_SURNAME': 'old_surname',
                                      'PAT_FORENAME': 'old_forename', 'PAT_ADD1': 'old_address_1',
                                      'PAT_ADD2': 'old_address_2', 'PAT_ADD3': 'old_address_3',
                                      'PAT_ADD4': 'old_address_4', 'PAT_ADD5': 'old_address_5',
                                      'PAT_POSTCODE': 'old_postcode', 'OTHER_FIELD': 'OTHER_VALUE'}
        original_notification_record = {'data': original_notification_data}
        participant = {'title': 'new_title', 'first_name': 'new_first_name', 'last_name': 'new_last_name',
                       'address': {'address_line_1': 'new_line_1', 'address_line_2': 'new_line_2',
                                   'address_line_3': 'new_line_3', 'address_line_4': 'new_line_4',
                                   'address_line_5': 'new_line_5', 'postcode': 'new_postcode'}}

        expected_notification_data = {'PAT_TITLE': 'new_title', 'PAT_SURNAME': 'new_last_name',
                                      'PAT_FORENAME': 'new_first_name', 'PAT_ADD1': 'new_line_1',
                                      'PAT_ADD2': 'new_line_2', 'PAT_ADD3': 'new_line_3',
                                      'PAT_ADD4': 'new_line_4', 'PAT_ADD5': 'new_line_5',
                                      'PAT_POSTCODE': 'new_postcode', 'OTHER_FIELD': 'OTHER_VALUE',
                                      'DEST_ADD1': 'new_line_1', 'DEST_ADD2': 'new_line_2',
                                      'DEST_ADD3': 'new_line_3', 'DEST_ADD4': 'new_line_4',
                                      'DEST_ADD5': 'new_line_5', 'DEST_POSTCODE': 'new_postcode', }

        actual_notification_data = resend_notifications\
            .update_notification_data_participant_information(original_notification_record, participant)

        self.assertDictEqual(expected_notification_data, actual_notification_data)

    def test_update_notification_data_participant_information_sets_fields_to_empty_string_if_not_present(self):
        original_notification_data = {'PAT_TITLE': 'old_title', 'PAT_SURNAME': 'old_surname',
                                      'PAT_FORENAME': 'old_forename', 'PAT_ADD1': 'old_address_1',
                                      'PAT_ADD2': 'old_address_2', 'PAT_ADD3': 'old_address_3',
                                      'PAT_ADD4': 'old_address_4', 'PAT_ADD5': 'old_address_5',
                                      'PAT_POSTCODE': 'old_postcode', 'OTHER_FIELD': 'OTHER_VALUE'}
        original_notification_record = {'data': original_notification_data}
        participant = {}

        expected_notification_data = {'PAT_TITLE': '', 'PAT_SURNAME': '', 'PAT_FORENAME': '', 'PAT_ADD1': '',
                                      'PAT_ADD2': '', 'PAT_ADD3': '', 'PAT_ADD4': '', 'PAT_ADD5': '',
                                      'PAT_POSTCODE': '', 'OTHER_FIELD': 'OTHER_VALUE', 'DEST_ADD1': '',
                                      'DEST_ADD2': '', 'DEST_ADD3': '', 'DEST_ADD4': '', 'DEST_ADD5': '',
                                      'DEST_POSTCODE': ''}

        actual_notification_data = resend_notifications\
            .update_notification_data_participant_information(original_notification_record, participant)

        self.assertDictEqual(expected_notification_data, actual_notification_data)
