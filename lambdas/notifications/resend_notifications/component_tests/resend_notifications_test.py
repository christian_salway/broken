import json
from unittest import TestCase

from boto3.dynamodb.conditions import Key, Attr
from mock import patch, Mock, call
from datetime import datetime, timezone, date
from common.models.letters import NotificationStatus
from resend_notifications import resend_notifications

mock_env_vars = {
    'AUDIT_QUEUE_URL': 'MY_AUDIT_QUEUE_URL',
    'DYNAMODB_PARTICIPANTS': 'the_participants_table',
    'RESULT_LETTER_CUT_OFF_MONTHS': '3'
}

mock_participant_record = {
    'title': 'Mrs', 'first_name': 'first', 'last_name': 'last',
    'address': {
        'address_line_1': '1 The Road', 'address_line_2': 'Townsville', 'postcode': 'AB1 2CD'
    }
}

mock_notification_data = {'other_key': 'other_value'}

mock_notification_record = {
    'record_id': 'RESULT#2020-12-05#2020-12-07T08:29:11.123+00:00', 'type': 'a letter type',
    'notification_date': '2020-12-08T14:26:04.052+00:00', 'participant_id': '123456',
    'print_destination': 'the printers', 'data': mock_notification_data, 'status': 'SENT'
}

FIXED_NOW_TIME = datetime(2020, 12, 26, 13, 48, 8, 123456, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = FIXED_NOW_TIME
datetime_util_mock = Mock()
datetime_util_mock.now.return_value = FIXED_NOW_TIME
date_mock = Mock(wraps=date)
date_mock.today.return_value = FIXED_NOW_TIME.date()

# Setup database mock
participants_table_mock = Mock()
participants_table_mock.get_item.return_value = {'Item': mock_participant_record}
participants_table_mock.query.side_effect = [
    {'Items': []},
    {'Items': [mock_notification_record]}
]


@patch('datetime.datetime', datetime_mock)
@patch('resend_notifications.resend_notifications.datetime', datetime_mock)
@patch('boto3.client', Mock())
@patch('os.environ', mock_env_vars)
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(return_value=participants_table_mock))
class TestResendNotifications(TestCase):

    def setUp(self):
        participants_table_mock.reset_mock()
        self.log_patcher = patch('resend_notifications.resend_notifications.log')
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.audit_utils.get_internal_id', Mock(return_value='123'))
    @patch('common.utils.audit_utils.get_sqs_client')
    @patch('common.utils.audit_utils.datetime', datetime_mock)
    @patch('common.utils.result_letter_utils.date', date_mock)
    def test_resend_notifications(self, get_sqs_client_mock):
        event = {'Records': [{'body': '{"participant_id": "123456"}'}]}

        context = Mock()
        context.function_name = ''

        sqs_client_mock = Mock()
        get_sqs_client_mock.return_value = sqs_client_mock

        resend_notifications.lambda_handler(event, context)

        # Assert participant record looked up from DynamoDB
        participants_table_mock.get_item.assert_called_once_with(
            Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        )

        # Assert DynamoDB checked for PENDING_PRINT_FILE and then SENT notifications
        self.assertEqual(2, len(participants_table_mock.query.call_args_list))
        expected_calls = [
            call(
                KeyConditionExpression=Key('participant_id').eq('123456') & Key('sort_key')
                .begins_with('NOTIFICATION'),
                FilterExpression=Attr('status').eq(NotificationStatus.PENDING_PRINT_FILE) &
                Attr('original_send_date').exists(),
                ScanIndexForward=False
            ),
            call(
                KeyConditionExpression=Key('participant_id').eq('123456') &
                Key('sort_key').begins_with('NOTIFICATION'),
                FilterExpression=Attr('status').eq('SENT'),
                ScanIndexForward=False
            )
        ]
        participants_table_mock.query.assert_has_calls(expected_calls)

        # Assert notification record saved
        participants_table_mock.put_item.assert_called_with(Item={
            'participant_id': '123456', 'sort_key': 'NOTIFICATION#2020-12-26T13:48:08.123456+00:00',
            'created': '2020-12-26T13:48:08.123456+00:00',
            'status': NotificationStatus.PENDING_PRINT_FILE,
            'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
            'type': 'a letter type', 'print_destination': 'the printers',
            'record_id': 'RESULT#2020-12-05#2020-12-07T08:29:11.123+00:00',
            'data': {
                'other_key': 'other_value', 'PAT_TITLE': 'Mrs', 'PAT_SURNAME': 'last',
                'PAT_FORENAME': 'first', 'PAT_ADD1': '1 The Road', 'PAT_ADD2': 'Townsville', 'PAT_ADD3': '',
                'PAT_ADD4': '', 'PAT_ADD5': '', 'PAT_POSTCODE': 'AB1 2CD', 'DEST_ADD1': '1 The Road',
                'DEST_ADD2': 'Townsville', 'DEST_ADD3': '',
                'DEST_ADD4': '', 'DEST_ADD5': '', 'DEST_POSTCODE': 'AB1 2CD'
            },
            'original_send_date': '2020-12-08'
        })

        expected_audit_message = json.dumps({'action': 'AUTOMATED_RESEND_NOTIFICATION',
                                             'timestamp': '2020-12-26T13:48:08.123456+00:00',
                                             'internal_id': '123',
                                             "nhsid_useruid": "SYSTEM",
                                             'session_id': 'NONE',
                                             'participant_ids': ['123456'],
                                             'additional_information': {'notification_type': 'a letter type',
                                                                        'address': {
                                                                            'address_line_1': '1 The Road',
                                                                            'address_line_2': 'Townsville',
                                                                            'postcode': 'AB1 2CD'}}
                                             })

        sqs_client_mock.send_message.assert_called_with(QueueUrl='MY_AUDIT_QUEUE_URL',
                                                        MessageBody=expected_audit_message)
