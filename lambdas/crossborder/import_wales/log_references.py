import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    XBWALES0001 = (logging.INFO, 'Reading S3 bucket to locate transfer files')
    XBWALES0002 = (logging.INFO, 'Found no cross-border transfer files')
    XBWALES0003 = (logging.INFO, 'Found cross-border transfer files')
    XBWALES0004 = (logging.ERROR, 'Error processing files')
    XBWALES0005 = (logging.ERROR, 'Failed to read file contents')
    XBWALES0006 = (logging.INFO, 'Flushed buffer to SQS queue')
    XBWALES0007 = (logging.INFO, 'Processing of files completed')
    XBWALES0008 = (logging.WARNING, 'Column count does not match one of the expected options')
    XBWALES0009 = (logging.INFO, 'Started processing file')
    XBWALES0010 = (logging.INFO, 'Finished processing file')
