Scans the cross-border-wales S3 bucket for files then:
* Determines they are CSV
* Which shape they match (participants or history)
* Splits the lines into individual SQS messages and sends to the appropriate queue for processing