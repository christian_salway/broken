from unittest import TestCase
from mock import patch, call, Mock
from import_wales.log_references import LogReference

MODULE_NAME = 'import_wales.import_wales'


@patch(f'{MODULE_NAME}.log')
class TestImportCrossBorderWales(TestCase):
    @patch('os.environ', {
        'REJECTED_BUCKET': 'Rejected',
        'COMPLETED_BUCKET': 'Completed',
        'WALES_BUCKET': 'Source',
        'HISTORY_URL': 'SQS_History',
        'PARTICIPANTS_URL': 'SQS_Participants'
    })
    def setUp(self):
        import import_wales.import_wales as _module
        self.module = _module
        self.unwrapped_function = self.module.lambda_handler.__wrapped__

    @patch(f'{MODULE_NAME}.move_object_to_bucket_and_transition_to_infrequent_access_class')
    def test_move_files_to_bucket(
        self,
        mock_move_object,
        mock_log
    ):
        files = [{'Key': 'FileA'}, {'Key': 'FileB'}]
        expected_calls = [
            call('SOURCE', 'DESTINATION', 'FileA'),
            call('SOURCE', 'DESTINATION', 'FileB')
        ]

        self.module.move_files_to_bucket(files, 'SOURCE', 'DESTINATION')

        mock_move_object.assert_has_calls(expected_calls)
        self.assertEqual(mock_move_object.call_count, 2)

    @patch(f'{MODULE_NAME}.move_object_to_bucket_and_transition_to_infrequent_access_class')
    def test_move_files_to_bucket_empty(
        self,
        mock_move_object,
        mock_log
    ):
        self.module.move_files_to_bucket([], 'SOURCE', 'DESTINATION')
        mock_move_object.assert_not_called()

    @patch(f'{MODULE_NAME}.get_object_from_bucket')
    def test_extract_lines(
        self,
        mock_get_object_from_bucket,
        mock_log
    ):
        file_name = 'FileA'
        bucket = 'Bucket1'
        expected_lines = ['Line1', 'Line2', 'Line3']
        mock_body = Mock()
        mock_body.iter_lines.return_value = iter([b'Line1', b'Line2', b'Line3'])

        mock_get_object_from_bucket.return_value = {
            'Body': mock_body
        }

        actual_lines = self.module.extract_lines(file_name, bucket)

        self.assertEqual(expected_lines, actual_lines)
        mock_get_object_from_bucket.assert_called_with(bucket, file_name)

    @patch(f'{MODULE_NAME}.move_files_to_bucket')
    @patch(f'{MODULE_NAME}.get_object_from_bucket')
    def test_extract_lines_empty(
        self,
        mock_get_object_from_bucket,
        mock_move_files_to_bucket,
        mock_log
    ):
        file_name = 'FileA'
        bucket = 'Bucket1'
        expected_lines = None
        expected_log_call = {
            'log_reference': LogReference.XBWALES0005,
            'file_name': file_name
        }

        mock_get_object_from_bucket.return_value = {}

        actual_lines = self.module.extract_lines(file_name, bucket)

        self.assertEqual(expected_lines, actual_lines)
        mock_get_object_from_bucket.assert_called_with(bucket, file_name)
        mock_log.assert_called_with(expected_log_call)
        mock_move_files_to_bucket.assert_called_with([{'Key': file_name}], bucket, 'Rejected')

    @patch(f'{MODULE_NAME}.send_new_message_batch')
    def test_flush_buffer_to_sqs(
        self,
        mock_send_new_message_batch,
        mock_log
    ):
        buffer = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10'], ['11']]
        sqs_url = 'SQS1'
        file_name = 'FileA'
        exepcted_response = []
        expected_batch_calls = [
            call(
                'SQS1',
                [
                    {'Id': '0', 'MessageBody': '{"file_name": "FileA", "line": ["1"], "line_number": "0"}'},
                    {'Id': '1', 'MessageBody': '{"file_name": "FileA", "line": ["2"], "line_number": "1"}'},
                    {'Id': '2', 'MessageBody': '{"file_name": "FileA", "line": ["3"], "line_number": "2"}'},
                    {'Id': '3', 'MessageBody': '{"file_name": "FileA", "line": ["4"], "line_number": "3"}'},
                    {'Id': '4', 'MessageBody': '{"file_name": "FileA", "line": ["5"], "line_number": "4"}'},
                    {'Id': '5', 'MessageBody': '{"file_name": "FileA", "line": ["6"], "line_number": "5"}'},
                    {'Id': '6', 'MessageBody': '{"file_name": "FileA", "line": ["7"], "line_number": "6"}'},
                    {'Id': '7', 'MessageBody': '{"file_name": "FileA", "line": ["8"], "line_number": "7"}'},
                    {'Id': '8', 'MessageBody': '{"file_name": "FileA", "line": ["9"], "line_number": "8"}'},
                    {'Id': '9', 'MessageBody': '{"file_name": "FileA", "line": ["10"], "line_number": "9"}'}
                ]
            ),
            call(
                'SQS1',
                [
                    {'Id': '10', 'MessageBody': '{"file_name": "FileA", "line": ["11"], "line_number": "10"}'}
                ]
            )
        ]

        expected_log_call = {
            'log_reference': LogReference.XBWALES0006,
            'buffer': sqs_url,
            'message_count': '11'
        }

        actual_response = self.module.flush_buffer_to_sqs(buffer, sqs_url, file_name)

        self.assertEqual(exepcted_response, actual_response)
        self.assertEqual(mock_send_new_message_batch.call_count, 2)
        mock_send_new_message_batch.assert_has_calls(expected_batch_calls)
        mock_log.assert_called_with(expected_log_call)

    @patch(f'{MODULE_NAME}.read_bucket')
    @patch(f'{MODULE_NAME}.extract_lines')
    @patch(f'{MODULE_NAME}.flush_buffer_to_sqs')
    @patch(f'{MODULE_NAME}.move_files_to_bucket')
    def test_import_process(
        self,
        mock_move_files_to_bucket,
        mock_flush_buffer_to_sqs,
        mock_extract_lines,
        mock_read_bucket,
        mock_log
    ):
        mock_read_bucket.return_value = {
            'Contents': [
                {
                    'Key': 'FileA'
                }
            ]
        }

        mock_extract_lines.return_value = [
            '1,2,3,4,5,6,7,8,9,10,11',
            '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22',
            '1,2',
            '1'
        ]

        expected_flush_buffer_calls = [
            call(
                [
                    ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'],
                ],
                'SQS_Participants',
                'FileA'
            ),
            call(
                [
                    ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22'], 
                    ['1', '2']
                ],
                'SQS_History',
                'FileA'
            )
        ]

        mock_flush_buffer_to_sqs.return_value = []

        self.unwrapped_function({}, {})

        mock_read_bucket.assert_called_with('Source')
        mock_extract_lines.assert_called_with('FileA', 'Source')
        mock_flush_buffer_to_sqs.assert_has_calls(expected_flush_buffer_calls)
        mock_move_files_to_bucket.assert_called_with([{'Key': 'FileA'}], 'Source', 'Completed')
