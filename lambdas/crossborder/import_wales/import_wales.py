import csv
import json
import os
from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.s3_utils import (
    get_object_from_bucket,
    move_object_to_bucket_and_transition_to_infrequent_access_class,
    read_bucket
)
from common.utils.sqs_utils import send_new_message_batch
from import_wales.log_references import LogReference
from typing import List

WALES_BUCKET = os.environ.get('WALES_BUCKET')
REJECTED_BUCKET = os.environ.get('REJECTED_BUCKET')
COMPLETED_BUCKET = os.environ.get('COMPLETED_BUCKET')
PARTICIPANTS_URL = os.environ.get('PARTICIPANTS_URL')
HISTORY_URL = os.environ.get('HISTORY_URL')
BATCH_SIZE = os.environ.get('BATCH_SIZE', 10)

COL_COUNT_PARTICIPANT = 11
COL_COUNT_HISTORY_SHORT = 2
COL_COUNT_HISTORY_LONG = 22
COL_COUNTS_HISTORY = [COL_COUNT_HISTORY_SHORT, COL_COUNT_HISTORY_LONG]


@lambda_entry_point
def lambda_handler(event, context):
    bucket_files = []
    buffer_participant = []
    buffer_history = []

    try:
        log({'log_reference': LogReference.XBWALES0001})
        bucket_files_response = read_bucket(WALES_BUCKET)

        bucket_files = bucket_files_response.get('Contents', [])
        if not bucket_files:
            log({'log_reference': LogReference.XBWALES0002})
            return

        for file in bucket_files:
            log({'log_reference': LogReference.XBWALES0009, 'file': file['Key']})
            file_lines = extract_lines(file['Key'], WALES_BUCKET)

            if not file_lines:
                continue

            reader = csv.reader(file_lines)
            for index, row in enumerate(reader, start=1):
                if len(row) == COL_COUNT_PARTICIPANT:
                    buffer_participant.append(row)
                elif len(row) in COL_COUNTS_HISTORY:
                    buffer_history.append(row)
                else:
                    log(
                        {
                            'log_reference': LogReference.XBWALES0008,
                            'col_count': str(len(row)),
                            'line_number': str(index + 1),
                            'file': file['Key']
                        }
                    )

            buffer_participant = flush_buffer_to_sqs(
                buffer_participant,
                PARTICIPANTS_URL,
                file['Key']
            )

            buffer_history = flush_buffer_to_sqs(
                buffer_history,
                HISTORY_URL,
                file['Key']
            )

            move_files_to_bucket(
                [file],
                WALES_BUCKET,
                COMPLETED_BUCKET
            )

            log({'log_reference': LogReference.XBWALES0010, 'file': file['Key']})

        log({'log_reference': LogReference.XBWALES0007})

    except Exception as ex:
        log({
            'log_reference': LogReference.XBWALES0004,
            'error': str(ex)
        })
        move_files_to_bucket(
            bucket_files,
            WALES_BUCKET,
            REJECTED_BUCKET
        )


def move_files_to_bucket(files: List, source_bucket: str, target_bucket: str):
    for file in files:
        move_object_to_bucket_and_transition_to_infrequent_access_class(
            source_bucket,
            target_bucket,
            file['Key']
        )


def extract_lines(file_name: str, bucket: str):
    s3_object_data = get_object_from_bucket(bucket, file_name)
    file_content = s3_object_data.get('Body')

    if not file_content:
        log(
            {
                'log_reference': LogReference.XBWALES0005,
                'file_name': file_name
            }
        )
        move_files_to_bucket([{'Key': file_name}], bucket, REJECTED_BUCKET)
        return

    return [line.decode("utf-8") for line in file_content.iter_lines()]


def flush_buffer_to_sqs(buffer: List, sqs_url: str, file_name: str) -> List:
    batch_size = int(BATCH_SIZE)
    index = 0

    chunks = [buffer[x:x+batch_size] for x in range(0, len(buffer), batch_size)]
    for chunk in chunks:
        messages = []
        for line in chunk:
            messages.append(
                {
                    'Id': str(index),
                    'MessageBody': json.dumps(
                        {
                            'file_name': file_name,
                            'line': line,
                            'line_number': str(index)
                        }
                    )
                }
            )
            index += 1
        send_new_message_batch(sqs_url, messages)

    log(
        {
            'log_reference': LogReference.XBWALES0006,
            'buffer': sqs_url,
            'message_count': str(index)
        }
    )
    return []
