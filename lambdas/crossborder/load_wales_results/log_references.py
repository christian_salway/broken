import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    XBWALESRES0001 = (logging.INFO, 'Consumed message from the SQS queue')
    XBWALESRES0002 = (logging.WARNING, 'Encountered a mapping error')
    XBWALESRES0003 = (logging.WARNING, 'No matching participant found')
    XBWALESRES0004 = (logging.WARNING, 'Too many matching participants found')
    XBWALESRES0005 = (logging.WARNING, 'Result already exists in the system')
    XBWALESRES0006 = (logging.INFO, 'Successfully imported result')
