from distutils import util
from common.utils.edifact_dictionaries import ACTIONS, CYTOLOGY_RESULTS, INFECTION_CODES
from common.utils.participant_utils import sanitise_nhs_number
from datetime import datetime, timezone
from typing import Dict, List, Optional, Tuple


COLUMNS_WALES_RESULT = [
    'NhsNumber',
    'TestId',
    'TestDate',
    'InvitationDate',
    'NotificationDate',
    'DateTestEntered',
    'RecallTypeAtTest',
    'RecallStatusAtTest',
    'ResultCode',
    'ResultCodeDesc',
    'ActionCode',
    'ActionCodeDesc',
    'RepeatMonths',
    'HpvResultCode',
    'NationalLabCode',
    'SlideNumber',
    'SourceType',
    'Comment1',
    'Comment2',
    'Comment3',
    'HPVPrimary',
    'SelfSample'
]


COLUMNS_WALES_RESULT_INFECTION_CODE_A = [
    'NhsNumber',
    'TestDate'
]


def _map_nhs_number(nhs_number: str) -> Tuple[str, Optional[str]]:
    sanitised_nhs_number = sanitise_nhs_number(nhs_number)
    if not sanitised_nhs_number:
        return None, 'No NHS number supplied'
    return sanitised_nhs_number, None


def _map_date(date_raw: str) -> Tuple[str, Optional[str]]:
    if not date_raw or not len(date_raw) == 8:
        return None, 'Date not of correct length'
    try:
        date = datetime.strptime(date_raw, '%Y%m%d').date()
        return date.strftime("%Y-%m-%d"), None
    except ValueError:
        return None, 'Date is not in a valid format'


def _map_result_code(result_code: str) -> Tuple[str, Optional[str]]:
    forced_string_code = str(result_code)
    if forced_string_code in CYTOLOGY_RESULTS:
        return forced_string_code, None
    return None, f'Result code {forced_string_code} is not a valid result code'


def _map_action_code(action_code: str) -> Tuple[str, Optional[str]]:
    forced_string_code = str(action_code)
    if forced_string_code in ACTIONS:
        return forced_string_code, None
    return None, f'Action code {forced_string_code} is not a valid action'


def _map_infection_code(infection_code: str) -> Tuple[str, Optional[str]]:
    forced_string_code = str(infection_code)
    if forced_string_code in INFECTION_CODES:
        return forced_string_code, None
    return None, f'Infection code {forced_string_code} is not a valid infection code'


def _map_number(value: str) -> Tuple[int, Optional[str]]:
    try:
        return int(value), None
    except ValueError:
        return None, 'Value does not map to a number'


def _map_non_empty_string(value: str) -> Tuple[str, Optional[str]]:
    if value:
        return str(value), None
    return None, 'Required string field is empty'


def _map_string(value: str) -> Tuple[str, Optional[str]]:
    return value, None


def _map_boolean(value: str) -> Tuple[bool, Optional[str]]:
    if not value:
        return False, None
    try:
        return bool(util.strtobool(value)), None
    except ValueError:
        return None, 'Value does not map to a boolean'


FIELD_MAP = {
    'NhsNumber': ('result_nhs_number', _map_nhs_number),
    'TestDate': ('test_date', _map_date),
    'DateTestEntered': ('result_date', _map_date),
    'ResultCode': ('result_code', _map_result_code),
    'ActionCode': ('action_code', _map_action_code),
    'RepeatMonths': ('recall_months', _map_number),
    'HpvResultCode': ('infection_code', _map_infection_code),
    'SourceType': ('source_code', _map_non_empty_string),
    'NationalLabCode': ('sending_lab', _map_non_empty_string),
    'SlideNumber': ('slide_number', _map_non_empty_string),
    'HPVPrimary': ('hpv_primary', _map_string),
    'SelfSample': ('self_sample', _map_boolean)
}


FIELD_MAP_INFECTION_CODE_A = {
    'NhsNumber': ('result_nhs_number', _map_nhs_number)
}


def _map_validate(row: List[str], field_map: Dict, field_list: List[str]) -> Tuple[Dict, List[str]]:
    mapped_result = {}
    validation_errors = []

    for field_name, (mapped_field_name, mapping_function) in field_map.items():
        field_index = field_list.index(field_name)
        mapped_value, field_validation_error = mapping_function(row[field_index])
        if mapped_value is not None:
            mapped_result[mapped_field_name] = mapped_value
        if field_validation_error:
            validation_errors.append(field_validation_error)

    return mapped_result, validation_errors


def _map_full_result(row: List[str]) -> Tuple[Dict, List[str]]:
    mapped_result, validation_errors = _map_validate(row, FIELD_MAP, COLUMNS_WALES_RESULT)

    if mapped_result.get('action_code'):
        mapped_result.update({'action': ACTIONS[mapped_result['action_code']]})
    if mapped_result.get('result_code'):
        mapped_result.update({'result': CYTOLOGY_RESULTS[mapped_result['result_code']]})

    comments = []
    for i in range(1, 4):
        comment_index = COLUMNS_WALES_RESULT.index(f'Comment{i}')
        comment = row[comment_index]
        if comment:
            comments.append(comment)

    if comments:
        mapped_result['crm'] = {
            "comments": comments
        }

    if not validation_errors:
        return {
            **mapped_result,
            **_map_dynamic_fields(mapped_result)
        }, validation_errors

    return mapped_result, validation_errors


def _map_dynamic_fields(result: Dict) -> Dict:
    test_date = result['test_date']
    created = datetime.now(timezone.utc).isoformat(timespec='microseconds')
    result.update({
        'sort_key': f'RESULT#{test_date}#{created}',
        'created': created,
    })
    return result


def map_validate_wales_result(row: List[str]) -> Tuple[Dict, List[str]]:
    field_count = len(row)
    if field_count == len(COLUMNS_WALES_RESULT):
        return _map_full_result(row)
    if field_count == len(COLUMNS_WALES_RESULT_INFECTION_CODE_A):
        return _map_validate(row, FIELD_MAP_INFECTION_CODE_A, COLUMNS_WALES_RESULT_INFECTION_CODE_A)
    return {}, [f'Field count wrong at {field_count}']
