import json

from ddt import data, ddt, unpack
from load_wales_results.log_references import LogReference
from mock import call, patch
from typing import Dict, List
from unittest import TestCase


MODULE_NAME = 'load_wales_results.load_wales_results'


@ddt
@patch(f'{MODULE_NAME}.log')
@patch(f'{MODULE_NAME}.query_participants_by_nhs_number')
@patch(f'{MODULE_NAME}.query_results_by_participant_id')
class TestLoadCrossBorderWalesResults(TestCase):
    def setUp(self):
        from load_wales_results.load_wales_results import lambda_handler
        self.unwrapped_handler = lambda_handler.__wrapped__

    def generate_event(self, line: List[str]) -> Dict:
        return {
            'Records': [
                {
                    'body': json.dumps(
                        {
                            'line_number': '7',
                            'file_name': 'TestFile1',
                            'line': line
                        }
                    )
                }
            ]
        }

    @unpack
    @data(
        (
            [],
            ['Field count wrong at 0']
        ),
        (
            [
                '####',
                'ABC123',
                '20210230',
                '',
                '',
                '20210230',
                '',
                '',
                'FIVE',
                'Five Cytology tests undertaken',
                'ABS',
                'Absolute Nonsense',
                36,
                0,
                'MCR',
                '654254123',
                'A',
                'Comment 1',
                'Comment 2!',
                'Comment 3?',
                'Y',
                '0'
            ],
            [
                'No NHS number supplied',
                'Date is not in a valid format',
                'Date is not in a valid format',
                'Result code FIVE is not a valid result code',
                'Action code ABS is not a valid action'
            ]
        )
    )
    def test_mapping_errors_logged(
        self,
        row,
        errors,
        mock_query_results_by_participant_id,
        mock_query_participants_by_nhs_number,
        mock_log
    ):
        event = self.generate_event(row)
        expected_log_calls = [
            call(
                {'log_reference': LogReference.XBWALESRES0001}
            )
        ]
        expected_log_calls += [
            call(
                {
                    'log_reference': LogReference.XBWALESRES0002,
                    'error': error
                }
            )
            for error in errors
        ]

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, len(errors) + 1)
        mock_log.assert_has_calls(expected_log_calls)

    @unpack
    @data(
        (
            [],
            LogReference.XBWALESRES0003
        ),
        (
            [{}, {}],
            LogReference.XBWALESRES0004
        )
    )
    @patch(f'{MODULE_NAME}.map_validate_wales_result')
    def test_participants_invalid_match(
        self,
        returned_participants,
        expected_log_entry,
        mock_map_validate_wales_result,
        mock_query_results_by_participant_id,
        mock_query_participants_by_nhs_number,
        mock_log
    ):
        dummy_nhs_number = '1234567890'
        dummy_row = ['TEST123']
        event = self.generate_event(dummy_row)
        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESRES0001}),
            call({'log_reference': expected_log_entry})
        ]

        mock_query_participants_by_nhs_number.return_value = returned_participants
        mock_map_validate_wales_result.return_value = (
            {'result_nhs_number': dummy_nhs_number},
            []
        )

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, 2)
        mock_log.assert_has_calls(expected_log_calls)
        mock_map_validate_wales_result.assert_called_with(dummy_row)
        mock_query_participants_by_nhs_number.assert_called_with(dummy_nhs_number)

    @patch(f'{MODULE_NAME}.map_validate_wales_result')
    def test_result_already_exists(
        self,
        mock_map_validate_wales_result,
        mock_query_results_by_participant_id,
        mock_query_participants_by_nhs_number,
        mock_log
    ):
        dummy_nhs_number = '1234567890'
        dummy_row = ['TEST123']

        event = self.generate_event(dummy_row)

        new_result = {
            'result_nhs_number': dummy_nhs_number,
            'sending_lab': 'LAB1',
            'slide_number': 'SLIDE1',
            'test_date': '2021-11-05'
        }

        existing_results = [
            {
                'sending_lab': 'LAB1',
                'slide_number': 'SLIDE1',
                'test_date': '2021-11-05'
            }
        ]

        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESRES0001}),
            call({'log_reference': LogReference.XBWALESRES0005})
        ]

        mock_map_validate_wales_result.return_value = (new_result, [])
        mock_query_participants_by_nhs_number.return_value = [{'participant_id': 'PART1'}]
        mock_query_results_by_participant_id.return_value = existing_results

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, 2)
        mock_log.assert_has_calls(expected_log_calls)
        mock_map_validate_wales_result.assert_called_with(dummy_row)
        mock_query_participants_by_nhs_number.assert_called_with(dummy_nhs_number)
        mock_query_results_by_participant_id.assert_called_with('PART1')

    @patch(f'{MODULE_NAME}.create_replace_record_in_participant_table')
    @patch('load_wales_results.map_validate_wales_result._map_dynamic_fields')
    def test_import_valid_result(
        self,
        mock_map_dynamic_fields,
        mock_create_record,
        mock_query_results_by_participant_id,
        mock_query_participants_by_nhs_number,
        mock_log
    ):
        test_row = [
            '8658272044',
            'ABC123',
            '20210226',
            '',
            '',
            '20210227',
            '',
            '',
            'X',
            'No Cytology test undertaken',
            'A',
            'Routine',
            36,
            0,
            'MCR',
            '654254123',
            '000001',
            'Comment 1',
            'Comment 2!',
            'Comment 3?',
            'Y',
            '0'
        ]
        event = self.generate_event(test_row)

        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESRES0001}),
            call({'log_reference': LogReference.XBWALESRES0006})
        ]

        expected_create_call = {
            'result_nhs_number': '8658272044',
            'test_date': '2021-02-26',
            'result_date': '2021-02-27',
            'result_code': 'X',
            'result': 'No Cytology test undertaken',
            'action_code': 'A',
            'action': 'Routine',
            'recall_months': 36,
            'infection_code': '0',
            'source_code': '000001',
            'sending_lab': 'MCR',
            'slide_number': '654254123',
            'hpv_primary': 'Y',
            'self_sample': False,
            'crm': {
                'comments': ['Comment 1', 'Comment 2!', 'Comment 3?']
            },
            'sort_key': 'RESULT#2021-02-26#2021-08-20T11:36:13.146544+00:00',
            'created': '2021-08-20T11:36:13.146544+00:00',
            'participant_id': 'PART1'
        }

        mock_query_participants_by_nhs_number.return_value = [{'participant_id': 'PART1'}]
        mock_query_results_by_participant_id.return_value = []
        mock_map_dynamic_fields.return_value = {
            'sort_key': 'RESULT#2021-02-26#2021-08-20T11:36:13.146544+00:00',
            'created': '2021-08-20T11:36:13.146544+00:00',
        }

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, 2)
        mock_log.assert_has_calls(expected_log_calls)
        mock_query_participants_by_nhs_number.assert_called_with('8658272044')
        mock_query_results_by_participant_id.assert_called_with('PART1')
        mock_create_record.assert_called_with(expected_create_call)
