from datetime import datetime, timezone
from ddt import data, ddt, unpack
from load_wales_results.map_validate_wales_result import (
    _map_action_code,
    _map_date,
    _map_dynamic_fields,
    _map_infection_code,
    _map_nhs_number,
    _map_non_empty_string,
    _map_number,
    _map_result_code,
    map_validate_wales_result
)
from mock import patch
from unittest import TestCase


MODULE_NAME = 'load_wales_results.map_validate_wales_result'


@ddt
class TestMapValidateWalesResult(TestCase):

    @unpack
    @data(
        ('1234567890', '1234567890'),
        ('AB-1234', 'AB1234')
    )
    def test_map_nhs_number(self, nhs_number, expected_nhs_number):
        actual_nhs_number, error = _map_nhs_number(nhs_number)
        self.assertEqual(actual_nhs_number, expected_nhs_number)
        self.assertIsNone(error)

    @unpack
    @data(
        ('',),
        ('-',),
    )
    def test_map_nhs_number_invalid(self, nhs_number):
        actual_nhs_number, error = _map_nhs_number(nhs_number)
        self.assertIsNone(actual_nhs_number)
        self.assertEqual(error, 'No NHS number supplied')

    @unpack
    @data(
        ('20200229', '2020-02-29'),
        ('13140624', '1314-06-24'),
    )
    def test_map_date(self, date, expected_date):
        actual_date, error = _map_date(date)
        self.assertEqual(actual_date, expected_date)
        self.assertIsNone(error)

    @unpack
    @data(
        ('20200230', 'Date is not in a valid format'),
        ('1234', 'Date not of correct length'),
    )
    def test_map_date_invalid(self, date, expected_error):
        actual_date, error = _map_date(date)
        self.assertIsNone(actual_date)
        self.assertEqual(error, expected_error)

    @unpack
    @data(
        ('', ''),
        ('X', 'X'),
        (1, '1')
    )
    def test_map_result_code(self, result_code, expected_result_code):
        actual_result_code, error = _map_result_code(result_code)
        self.assertEqual(actual_result_code, expected_result_code)
        self.assertIsNone(error)

    @unpack
    @data(
        ('?',),
        (None,),
    )
    def test_map_result_code_invalid(self, result_code):
        actual_result_code, error = _map_result_code(result_code)
        self.assertIsNone(actual_result_code)
        self.assertEqual(error, f'Result code {result_code} is not a valid result code')

    @unpack
    @data(
        ('A', 'A'),
        ('H', 'H'),
    )
    def test_map_action_code(self, action_code, expected_action_code):
        actual_action_code, error = _map_action_code(action_code)
        self.assertEqual(actual_action_code, expected_action_code)
        self.assertIsNone(error)

    @unpack
    @data(
        ('?',),
        (None,),
    )
    def test_map_action_code_invalid(self, action_code):
        actual_action_code, error = _map_action_code(action_code)
        self.assertIsNone(actual_action_code)
        self.assertEqual(error, f'Action code {action_code} is not a valid action')

    @unpack
    @data(
        (0, '0'),
        ('9', '9'),
        ('Q', 'Q'),
    )
    def test_map_infection_code(self, infection_code, expected_infection_code):
        actual_infection_code, error = _map_infection_code(infection_code)
        self.assertEqual(actual_infection_code, expected_infection_code)
        self.assertIsNone(error)

    @unpack
    @data(
        ('?',),
        (None,),
    )
    def test_map_infection_code_invalid(self, infection_code):
        actual_infection_code, error = _map_infection_code(infection_code)
        self.assertIsNone(actual_infection_code)
        self.assertEqual(error, f'Infection code {infection_code} is not a valid infection code')

    @unpack
    @data(
        (0, 0),
        ('-1', -1),
        ('5', 5)
    )
    def test_map_number(self, number, expected_number):
        actual_number, error = _map_number(number)
        self.assertEqual(actual_number, expected_number)
        self.assertIsNone(error)

    @unpack
    @data(
        ('',),
        ('Text',)
    )
    def test_map_number_invalid(self, number):
        actual_number, error = _map_number(number)
        self.assertIsNone(actual_number)
        self.assertEqual(error, 'Value does not map to a number')

    @unpack
    @data(
        ('Test 123!', 'Test 123!'),
        (1234567890, '1234567890')
    )
    def test_map_non_empty_string(self, string, expected_string):
        actual_string, error = _map_non_empty_string(string)
        self.assertEqual(actual_string, expected_string)
        self.assertIsNone(error)

    @unpack
    @data(
        ('',),
        (None,)
    )
    def test_map_non_empty_string_error(self, string):
        actual_string, error = _map_non_empty_string(string)
        self.assertIsNone(actual_string)
        self.assertEqual(error, 'Required string field is empty')

    @patch(f'{MODULE_NAME}.datetime')
    def test_map_dynamic_fields(
        self,
        mock_datetime
    ):
        mock_datetime.now.return_value = datetime(2021, 8, 17, 11, 22, 33, 444, tzinfo=timezone.utc)
        expected_response = {
            'created': '2021-08-17T11:22:33.000444+00:00',
            'sort_key': 'RESULT#2021-02-28#2021-08-17T11:22:33.000444+00:00',
            'test_date': '2021-02-28'
        }

        actual_response = _map_dynamic_fields({'test_date': '2021-02-28'})

        self.assertDictEqual(actual_response, expected_response)

    @unpack
    @data(
        (
            ['8658272044', '20210228'],
            {'result_nhs_number': '8658272044'},
            []
        ),
        (
            ['###', '20210228'],
            {},
            ['No NHS number supplied']
        ),
        (
            [],
            {},
            ['Field count wrong at 0']
        ),
        (
            [
                '8658272044',
                'ABC123',
                '20210226',
                '',
                '',
                '20210227',
                '',
                '',
                'X',
                'No Cytology test undertaken',
                'A',
                'Routine',
                36,
                0,
                'MCR',
                '654254123',
                '000001',
                'Comment 1',
                'Comment 2!',
                'Comment 3?',
                'Y',
                '0'
            ],
            {
                'result_nhs_number': '8658272044',
                'test_date': '2021-02-26',
                'result_date': '2021-02-27',
                'result_code': 'X',
                'result': 'No Cytology test undertaken',
                'action_code': 'A',
                'action': 'Routine',
                'recall_months': 36,
                'hpv_primary': 'Y',
                'infection_code': '0',
                'sending_lab': 'MCR',
                'slide_number': '654254123',
                'source_code': '000001',
                'sort_key': 'RESULT#TEST#123',
                'self_sample': False,
                'crm': {
                    'comments': ['Comment 1', 'Comment 2!', 'Comment 3?']
                }
            },
            []
        ),
        (
            [
                '####',
                'ABC123',
                '20210230',
                '',
                '',
                '20210230',
                '',
                '',
                'FIVE',
                'Five Cytology tests undertaken',
                'ABS',
                'Absolute Nonsense',
                36,
                0,
                'MCR',
                '654254123',
                '000001',
                'Comment 1',
                'Comment 2!',
                'Comment 3?',
                'Y',
                '0'
            ],
            {
                'recall_months': 36,
                'infection_code': '0',
                'source_code': '000001',
                'sending_lab': 'MCR',
                'slide_number': '654254123',
                'hpv_primary': 'Y',
                'self_sample': False,
                'crm': {
                    'comments': ['Comment 1', 'Comment 2!', 'Comment 3?']
                }
            },
            [
                'No NHS number supplied',
                'Date is not in a valid format',
                'Date is not in a valid format',
                'Result code FIVE is not a valid result code',
                'Action code ABS is not a valid action'
            ]
        ),
        (
            [
                '4963967139',
                '1',
                '20190101',
                '20181201',
                '20181201',
                '20190201',
                'K',
                '1',
                '2',
                'Negative',
                'A',
                'Routine',
                '36',
                '0',
                '123456',
                'SLIDE NUM',
                'G',
                'Comment 1',
                'Comment 2',
                'Comment 3',
                'N',
                'Y'
            ],
            {
                'action': 'Routine',
                'action_code': 'A',
                'crm': {'comments': ['Comment 1', 'Comment 2', 'Comment 3']},
                'hpv_primary': 'N',
                'infection_code': '0',
                'recall_months': 36,
                'result': 'Negative',
                'result_code': '2',
                'result_date': '2019-02-01',
                'result_nhs_number': '4963967139',
                'self_sample': True,
                'sending_lab': '123456',
                'slide_number': 'SLIDE NUM',
                'sort_key': 'RESULT#TEST#123',
                'source_code': 'G',
                'test_date': '2019-01-01'
            },
            []
        ),
        (
            [
                '4963967139',
                '1',
                '20190101',
                '20181201',
                '20181201',
                '20190201',
                'K',
                '1',
                '2',
                'Negative',
                'A',
                'Routine',
                '36',
                '0',
                '123456',
                'SLIDE NUM',
                'G',
                'Comment 1',
                'Comment 2',
                'Comment 3',
                'N',
                ''
            ],
            {
                'action': 'Routine',
                'action_code': 'A',
                'crm': {'comments': ['Comment 1', 'Comment 2', 'Comment 3']},
                'hpv_primary': 'N',
                'infection_code': '0',
                'recall_months': 36,
                'result': 'Negative',
                'result_code': '2',
                'result_date': '2019-02-01',
                'result_nhs_number': '4963967139',
                'self_sample': False,
                'sending_lab': '123456',
                'slide_number': 'SLIDE NUM',
                'sort_key': 'RESULT#TEST#123',
                'source_code': 'G',
                'test_date': '2019-01-01'
            },
            []
        )
    )
    @patch(f'{MODULE_NAME}._map_dynamic_fields')
    def test_map_validate_wales_result(
        self,
        row,
        expected_mapping,
        expected_errors,
        mock_map_dynamic_fields
    ):
        mock_map_dynamic_fields.return_value = {'sort_key': 'RESULT#TEST#123'}
        actual_mapping, actual_errors = map_validate_wales_result(row)
        self.assertDictEqual(actual_mapping, expected_mapping)
        self.assertEqual(actual_errors, expected_errors)
