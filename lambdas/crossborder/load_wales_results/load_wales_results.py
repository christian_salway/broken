from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_utils import (
    create_replace_record_in_participant_table,
    query_participants_by_nhs_number,
    query_results_by_participant_id
)
from common.utils.transform_and_store_utils import (
    get_message_contents_from_event,
    update_log_metadata_from_record_event
)
from load_wales_results.log_references import LogReference
from load_wales_results.map_validate_wales_result import map_validate_wales_result
from typing import Dict


RESULT_MATCHING_FIELDS = ['sending_lab', 'slide_number', 'test_date']


@lambda_entry_point
def lambda_handler(event, context):
    update_log_metadata_from_record_event(event)
    message_contents = get_message_contents_from_event(event)

    log({'log_reference': LogReference.XBWALESRES0001})
    mapped_result, errors = map_validate_wales_result(message_contents.get('record_line', []))

    if errors:
        for error in errors:
            log({'log_reference': LogReference.XBWALESRES0002, 'error': error})
        return

    matching_participants = query_participants_by_nhs_number(mapped_result['result_nhs_number'])
    if not matching_participants:
        log({'log_reference': LogReference.XBWALESRES0003})
        return
    if len(matching_participants) > 1:
        log({'log_reference': LogReference.XBWALESRES0004})
        return

    participant = matching_participants[0]
    participant_id = participant['participant_id']
    if result_already_exists(participant_id, mapped_result):
        log({'log_reference': LogReference.XBWALESRES0005})
        return

    mapped_result['participant_id'] = participant_id
    create_replace_record_in_participant_table(mapped_result)
    log({'log_reference': LogReference.XBWALESRES0006})


def result_already_exists(participant_id: str, new_result: Dict) -> bool:
    existing_results = query_results_by_participant_id(participant_id)

    for existing_result in existing_results:
        field_matches = [
            existing_result.get(field_name) == new_result[field_name]
            for field_name in RESULT_MATCHING_FIELDS
        ]
        if all(field_matches):
            return True

    return False
