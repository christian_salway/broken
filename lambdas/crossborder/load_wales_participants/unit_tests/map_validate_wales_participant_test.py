from ddt import data, ddt, unpack
from unittest import TestCase
from common.models.participant import CeaseReason, EpisodeStatus, ParticipantStatus

from load_wales_participants.map_validate_wales_participant import (
    _map_cease_reason, _map_date,
    _map_episode_status, _map_nhs_number,
    _map_participant_status,
    map_validate_wales_participant)


MODULE_NAME = 'load_wales_participants.map_validate_wales_participants'


@ddt
class MapValidateWalesParticipantTest(TestCase):
    @unpack
    @data(
        ('1234567890', '1234567890'),
        ('AB-1234', 'AB1234')
    )
    def test_map_nhs_number(self, nhs_number, expected_nhs_number):
        actual_nhs_number, error = _map_nhs_number(nhs_number)
        self.assertEqual(actual_nhs_number, expected_nhs_number)
        self.assertIsNone(error)

    @unpack
    @data(
        ('',),
        ('-',),
    )
    def test_map_nhs_number_invalid(self, nhs_number):
        actual_nhs_number, error = _map_nhs_number(nhs_number)
        self.assertIsNone(actual_nhs_number)
        self.assertEqual(error, 'No NHS number supplied')

    @unpack
    @data(
        ('20200229', '2020-02-29'),
        ('13140624', '1314-06-24'),
    )
    def test_map_date(self, date, expected_date):
        actual_date, error = _map_date(date)
        self.assertEqual(actual_date, expected_date)
        self.assertIsNone(error)

    @unpack
    @data(
        ('20200230', 'Date is not in a valid format'),
        ('1234', 'Date not of correct length'),
    )
    def test_map_date_invalid(self, date, expected_error):
        actual_date, error = _map_date(date)
        self.assertIsNone(actual_date)
        self.assertEqual(error, expected_error)

    @unpack
    @data(
        ('L', ParticipantStatus.CALLED),
        ('K', ParticipantStatus.CALLED),
        ('A', ParticipantStatus.ROUTINE),
        ('C', ParticipantStatus.CEASED),
        ('R', ParticipantStatus.REPEAT_ADVISED),
        ('S', ParticipantStatus.SUSPENDED),
        ('D', ParticipantStatus.SUSPENDED)
    )
    def test_map_participant_status(self, value, expected_status):
        mapped_status, error = _map_participant_status(value)
        self.assertEqual(mapped_status, expected_status)
        self.assertIsNone(error)

    def test_map_participant_invalid(self):
        mapped_participant_status, error = _map_participant_status('nonsense')
        self.assertIsNone(mapped_participant_status)
        self.assertEqual(error, 'Participant status code nonsense is not valid')

    @unpack
    @data(
        ('G', EpisodeStatus.ACCEPTED),
        ('X', EpisodeStatus.PNL),
        ('P', EpisodeStatus.INVITED),
        ('1', EpisodeStatus.INVITED),
        ('2', EpisodeStatus.REMINDED),
        ('F', EpisodeStatus.NRL),
        ('H', EpisodeStatus.NRL),
        ('3', EpisodeStatus.NRL),
        ('4', EpisodeStatus.NRL),
        ('C', EpisodeStatus.CEASED),
        ('S', EpisodeStatus.DEFERRED),
        ('L', EpisodeStatus.LOGGED),
        ('Z', EpisodeStatus.WALKIN)
    )
    def test_map_episode_status(self, value, expected_episode_status):
        mapped_episode_status, error = _map_episode_status(value)
        self.assertEqual(mapped_episode_status, expected_episode_status)
        self.assertIsNone(error)

    def test_map_episode_status_invalid(self):
        mapped_episode_status, error = _map_episode_status('nonsense')
        self.assertIsNone(mapped_episode_status)
        self.assertEqual(error, 'Episode status code nonsense is not valid')

    @unpack
    @data(
        ('6', CeaseReason.DUE_TO_AGE),
        ('7', CeaseReason.NO_CERVIX),
        ('8', CeaseReason.PATIENT_INFORMED_CHOICE),
        ('9', CeaseReason.OTHER),
        ('9C', CeaseReason.OTHER),
        ('TAH', CeaseReason.NO_CERVIX),
        ('CAC', CeaseReason.NO_CERVIX),
        ('TRAH', CeaseReason.NO_CERVIX),
        ('MANCH', CeaseReason.NO_CERVIX)
    )
    def test_map_cease_reason(self, value, expected_cease_reason):
        mapped_cease_reason, error = _map_cease_reason(value)
        self.assertEqual(mapped_cease_reason, expected_cease_reason)
        self.assertIsNone(error)

    def test_map_cease_reason_invalid(self):
        mapped_cease_reason, error = _map_cease_reason('nonsense')
        self.assertIsNone(mapped_cease_reason)
        self.assertEqual(error, 'Cease code nonsense is not valid')

    @unpack
    @data(
        (
            [],
            {},
            {},
            ['Incorrect field count. Expected 11, found  0']
        ),
        (
            [
                '0123456789',
                '',
                '',
                '',
                '',
                '20210301',
                '20210101',
                '20210215',
                'L',
                'P',
                '6'
            ],
            {
                'next_test_due_date': '2021-03-01',
                'nhs_number': '0123456789',
                'status': 'CALLED'
            },
            {
                'cease_reason': CeaseReason.DUE_TO_AGE,
                'episode_status': EpisodeStatus.INVITED,
                'invited_date': '2021-01-01',
                'notification_date': '2021-02-15'
            },
            []
        ),
        (
            [
                '###',
                '',
                '',
                '',
                '',
                '202103001',
                '2021-01',
                'ABCDEFGh',
                'BL',
                'PX',
                'FF'
            ],
            {},
            {},
            [
                'No NHS number supplied',
                'Date not of correct length',
                'Participant status code BL is not valid',
                'Episode status code PX is not valid',
                'Date is not in a valid format',
                'Cease code FF is not valid',
                'Date not of correct length'
            ]
        )
    )
    def test_map_validate_wales_participant(self, row, expected_mapping, expected_additional_fields, expected_errors):
        mapped_participant, additional_fields, errors = map_validate_wales_participant(row)
        self.assertDictEqual(mapped_participant, expected_mapping)
        self.assertDictEqual(additional_fields, expected_additional_fields)
        self.assertEqual(errors, expected_errors)
