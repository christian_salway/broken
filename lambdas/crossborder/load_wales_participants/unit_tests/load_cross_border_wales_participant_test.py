from datetime import datetime, timezone
import json
from ddt import data, ddt, unpack
from mock import call, patch
from typing import Dict, List
from unittest import TestCase

from common.models.participant import CeaseReason, EpisodeStatus
from common.utils.audit_utils import AuditActions, AuditUsers

from load_wales_participants.log_references import LogReference
with patch('boto3.resource'):
    import load_wales_participants.load_wales_participants \
        as load_wales_participants_module

MODULE_NAME = 'load_wales_participants.load_wales_participants'


@ddt
@patch(f'{MODULE_NAME}.log')
@patch(f'{MODULE_NAME}.query_participants_by_nhs_number')
class TestLoadCrossBorderWalesResults(TestCase):
    def setUp(self):
        self.unwrapped_handler = load_wales_participants_module.lambda_handler.__wrapped__

    def generate_event(self, line: List[str]) -> Dict:
        return {
            'Records': [
                {
                    'body': json.dumps(
                        {
                            'line_number': '7',
                            'file_name': 'TestFile1',
                            'line': line
                        }
                    )
                }
            ]
        }

    @unpack
    @data(
        (
            [],
            ['Incorrect field count. Expected 11, found  0']
        ),
        (
            [
                '###',
                '',
                '',
                '',
                '',
                '202103001',
                '2021-01',
                'ABCDEFGh',
                'BL',
                'PX',
                'FF'
            ],
            [
                'No NHS number supplied',
                'Date not of correct length',
                'Participant status code BL is not valid',
                'Episode status code PX is not valid',
                'Date is not in a valid format',
                'Cease code FF is not valid',
                'Date not of correct length'
            ]
        )
    )
    def test_mapping_errors_logged(self,
                                   row,
                                   errors,
                                   mock_query_participants_by_nhs_number,
                                   mock_log):

        event = self.generate_event(row)

        expected_log_calls = [
            call(
                {'log_reference': LogReference.XBWALESPART0001}
            )
        ]

        expected_log_calls += [
            call(
                {
                    'log_reference': LogReference.XBWALESPART0002,
                    'error': error
                }
            )
            for error in errors
        ]

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, len(errors) + 1)
        mock_log.assert_has_calls(expected_log_calls)

    @unpack
    @data(
        (
            [],
            LogReference.XBWALESPART0003
        ),
        (
            [{}, {}],
            LogReference.XBWALESPART0013
        )
    )
    @patch(f'{MODULE_NAME}.map_validate_wales_participant')
    def test_participant_not_found(
            self,
            participants,
            expected_log_entry,
            mock_map_validate_wales_participant,
            mock_query_participants_by_nhs_number,
            mock_log):

        event = self.generate_event([])

        nhs_number = '0123456789'
        mock_map_validate_wales_participant.return_value = [{'nhs_number': nhs_number}, {}, []]
        mock_query_participants_by_nhs_number.return_value = participants

        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESPART0001}),
            call({'log_reference': expected_log_entry, 'nhs_number': nhs_number})
        ]

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, 2)
        mock_log.assert_has_calls(expected_log_calls)
        mock_map_validate_wales_participant.assert_called_with([])
        mock_query_participants_by_nhs_number.assert_called_with(nhs_number)

    @patch(f'{MODULE_NAME}.map_validate_wales_participant')
    @patch(f'{MODULE_NAME}.create_replace_record_in_participant_table')
    def test_participant_without_episode_status_created_or_updated(
            self,
            mock_create_replace_record_in_participant_table,
            mock_map_validate_wales_participant,
            mock_query_participants_by_nhs_number,
            mock_log):

        event = self.generate_event([])

        nhs_number = '0123456789'
        mock_map_validate_wales_participant.return_value = [{'nhs_number': nhs_number}, {'episode_status': None}, []]
        mock_query_participants_by_nhs_number.return_value = [{'participant_id': 'some-id', 'sort_key': 'PARTICIPANT'}]

        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESPART0001}),
            call({'log_reference': LogReference.XBWALESPART0004})
        ]

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, 2)
        mock_log.assert_has_calls(expected_log_calls)
        mock_map_validate_wales_participant.assert_called_with([])
        mock_query_participants_by_nhs_number.assert_called_with(nhs_number)
        mock_create_replace_record_in_participant_table.assert_called_with(
            {'participant_id': 'some-id', 'nhs_number': nhs_number, 'sort_key': 'PARTICIPANT'})

    @data(EpisodeStatus.CEASED, EpisodeStatus.DEFERRED)
    @patch(f'{MODULE_NAME}.map_validate_wales_participant')
    @patch(f'{MODULE_NAME}.create_replace_record_in_participant_table')
    @patch(f'{MODULE_NAME}.cease_or_defer_participant')
    def test_participant_ceased_or_deferred_when_episode_status_ceased_or_deferred(
            self,
            episode_status,
            mock_cease_or_defer_participant,
            mock_create_replace_record_in_participant_table,
            mock_map_validate_wales_participant,
            mock_query_participants_by_nhs_number,
            mock_log):

        data_list = ['raw data']
        event = self.generate_event(data_list)

        nhs_number = '0123456789'
        mock_map_validate_wales_participant.return_value = [
            {'nhs_number': nhs_number},
            {
                'episode_status': episode_status,
                'notification_date': 'notification-date',
                'cease_reason': CeaseReason.DUE_TO_AGE
            },
            []
        ]

        participant_id = 'some-id'
        participant = {'participant_id': participant_id, 'sort_key': 'PARTICIPANT'}
        mock_query_participants_by_nhs_number.return_value = [participant]
        mock_create_replace_record_in_participant_table.return_value = {'nhs_number': nhs_number}

        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESPART0001}),
            call({'log_reference': LogReference.XBWALESPART0004}),
        ]

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, 2)
        mock_log.assert_has_calls(expected_log_calls)
        mock_map_validate_wales_participant.assert_called_with(data_list)
        mock_query_participants_by_nhs_number.assert_called_with(nhs_number)
        mock_create_replace_record_in_participant_table.has_calls([
            {'participant_id': 'some-id', 'nhs_number': nhs_number},
            {'episode_key': 'value'}
        ])
        mock_cease_or_defer_participant.assert_called_with(
            episode_status, participant, 'notification-date', CeaseReason.DUE_TO_AGE)

    @patch(f'{MODULE_NAME}.map_validate_wales_participant')
    @patch(f'{MODULE_NAME}.create_replace_record_in_participant_table')
    @patch(f'{MODULE_NAME}.create_episode')
    @patch(f'{MODULE_NAME}.audit')
    @patch(f'{MODULE_NAME}.update_episode_status')
    def test_episode_created_for_participant_with_episode_status_logged(
            self,
            mock_update_episode_status,
            mock_audit,
            mock_create_episode,
            mock_create_replace_record_in_participant_table,
            mock_map_validate_wales_participant,
            mock_query_participants_by_nhs_number,
            mock_log):

        data_list = ['raw data']
        event = self.generate_event(data_list)

        nhs_number = '0123456789'
        participant = {'participant_id': 'some-id', 'nhs_number': nhs_number,
                       'next_test_due_date': 'next-test-due-date'}
        mock_map_validate_wales_participant.return_value = [
            participant,
            {'episode_status': EpisodeStatus.LOGGED, 'invited_date': 'invited-date'},
            []
        ]

        participant_id = 'some-id'
        mock_query_participants_by_nhs_number.return_value = [{'participant_id': participant_id}]
        mock_create_replace_record_in_participant_table.return_value = participant
        mock_create_episode.return_value = {'sort_key': 'value'}

        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESPART0001}),
            call({'log_reference': LogReference.XBWALESPART0004}),
            call({'log_reference': LogReference.XBWALESPART0005}),
            call({'log_reference': LogReference.XBWALESPART0006})
        ]

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, 4)
        mock_log.assert_has_calls(expected_log_calls)
        mock_map_validate_wales_participant.assert_called_with(data_list)
        mock_query_participants_by_nhs_number.assert_called_with(nhs_number)
        mock_create_episode.assert_called_with(participant, 'invited-date', EpisodeStatus.LOGGED)
        mock_create_replace_record_in_participant_table.has_calls([
            {'participant_id': 'some-id', 'nhs_number': nhs_number},
            {'episode_key': 'value'}
        ])
        mock_audit.assert_called_with(action=AuditActions.CREATE_EPISODE, user=AuditUsers.SYSTEM,
                                      participant_ids=[participant_id], nhs_numbers=[nhs_number])
        mock_update_episode_status.assert_not_called()

    @patch(f'{MODULE_NAME}.map_validate_wales_participant')
    @patch(f'{MODULE_NAME}.create_replace_record_in_participant_table')
    @patch(f'{MODULE_NAME}.create_episode')
    @patch(f'{MODULE_NAME}.audit')
    @patch(f'{MODULE_NAME}.update_episode_status')
    def test_episode_created_for_participant_with_episode_status_pnl(
            self,
            mock_update_episode_status,
            mock_audit,
            mock_create_episode,
            mock_create_replace_record_in_participant_table,
            mock_map_validate_wales_participant,
            mock_query_participants_by_nhs_number,
            mock_log):

        data_list = ['raw data']
        event = self.generate_event(data_list)

        nhs_number = '0123456789'
        participant = {'participant_id': 'some-id', 'sort_key': 'PARTICIPANT',
                       'nhs_number': nhs_number, 'next_test_due_date': 'next-test-due-date'}
        mock_map_validate_wales_participant.return_value = [
            participant,
            {'episode_status': EpisodeStatus.PNL, 'invited_date': 'invited-date'},
            []
        ]

        participant_id = 'some-id'
        mock_query_participants_by_nhs_number.return_value = [participant]
        mock_create_replace_record_in_participant_table.return_value = participant
        mock_create_episode.return_value = {'sort_key': 'value'}

        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESPART0001}),
            call({'log_reference': LogReference.XBWALESPART0004}),
            call({'log_reference': LogReference.XBWALESPART0005}),
            call({'log_reference': LogReference.XBWALESPART0006})
        ]

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, 4)
        mock_log.assert_has_calls(expected_log_calls)
        mock_map_validate_wales_participant.assert_called_with(data_list)
        mock_query_participants_by_nhs_number.assert_called_with(nhs_number)
        mock_create_episode.assert_called_with(participant, 'invited-date')
        mock_create_replace_record_in_participant_table.has_calls([
            {'participant_id': 'some-id', 'nhs_number': nhs_number},
            {'episode_key': 'value'}
        ])
        mock_audit.assert_called_with(action=AuditActions.CREATE_EPISODE, user=AuditUsers.SYSTEM,
                                      participant_ids=[participant_id], nhs_numbers=[nhs_number])
        mock_update_episode_status.assert_not_called()

    @patch(f'{MODULE_NAME}.map_validate_wales_participant')
    @patch(f'{MODULE_NAME}.create_replace_record_in_participant_table')
    @patch(f'{MODULE_NAME}.create_episode')
    @patch(f'{MODULE_NAME}.audit')
    @patch(f'{MODULE_NAME}.update_episode_status')
    def test_episode_created_and_updated_for_participant_with_episode_status_non_pnl(
            self,
            mock_update_episode_status,
            mock_audit,
            mock_create_episode,
            mock_create_replace_record_in_participant_table,
            mock_map_validate_wales_participant,
            mock_query_participants_by_nhs_number,
            mock_log):

        data_list = ['raw data']
        event = self.generate_event(data_list)

        nhs_number = '0123456789'
        participant = {'nhs_number': nhs_number, 'next_test_due_date': 'next-test-due-date'}
        mock_map_validate_wales_participant.return_value = [
            participant,
            {'episode_status': EpisodeStatus.INVITED, 'invited_date': 'invited-date'},
            []
        ]

        participant_id = 'some-id'
        mock_query_participants_by_nhs_number.return_value = [{'participant_id': participant_id}]
        mock_create_replace_record_in_participant_table.return_value = participant
        mock_create_episode.return_value = {'sort_key': 'value'}

        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESPART0001}),
            call({'log_reference': LogReference.XBWALESPART0004}),
            call({'log_reference': LogReference.XBWALESPART0005}),
            call({'log_reference': LogReference.XBWALESPART0006}),
            call({'log_reference': LogReference.XBWALESPART0007}),
            call({'log_reference': LogReference.XBWALESPART0008})
        ]

        self.unwrapped_handler(event, {})

        self.assertEqual(mock_log.call_count, 6)
        mock_log.assert_has_calls(expected_log_calls)
        mock_map_validate_wales_participant.assert_called_with(data_list)
        mock_query_participants_by_nhs_number.assert_called_with(nhs_number)
        mock_create_replace_record_in_participant_table.has_calls([
            {'participant_id': 'some-id', 'nhs_number': nhs_number},
            {'episode_key': 'value'}
        ])
        mock_audit.assert_called_with(action=AuditActions.CREATE_EPISODE, user=AuditUsers.SYSTEM,
                                      participant_ids=[participant_id], nhs_numbers=[nhs_number])
        mock_update_episode_status.assert_called_with('some-id', 'value', EpisodeStatus.INVITED)

    @patch(f'{MODULE_NAME}.datetime')
    def test_episode_created_successfully(
            self,
            mock_datetime,
            mock_query_participants_by_nhs_number,
            mock_log):
        mock_datetime.now.return_value = datetime(2021, 8, 17, 11, 22, 33, 444, tzinfo=timezone.utc)

        expected_episode = {
            'created': '2021-08-17T11:22:33.000444+00:00',
            'invited_date': 'invited-date',
            'live_record_status': 'PNL',
            'participant_id': 'id',
            'pnl_date': '2021-08-17',
            'registered_gp_practice_code_and_test_due_date': '#next-test-due-date',
            'sort_key': 'EPISODE#next-test-due-date',
            'status': 'PNL',
            'test_due_date': 'next-test-due-date',
            'user_id': 'create_participant_episode'
        }

        participant = {'participant_id': 'id', 'next_test_due_date': 'next-test-due-date'}
        episode = load_wales_participants_module.create_episode(participant, 'invited-date')

        self.assertEqual(episode, expected_episode)

    def test_episode_not_created_when_no_next_test_due_date(
            self,
            mock_query_participants_by_nhs_number,
            mock_log):
        participant = {'participant_id': 'id'}
        with self.assertRaises(KeyError):
            load_wales_participants_module.create_episode(participant, 'invited-date')

    @patch(f'{MODULE_NAME}.update_participant_episode_record')
    def test_episode_updated_successfully(self,
                                          mock_update_participant_episode_record,
                                          mock_query_participants_by_nhs_number,
                                          mock_log):
        load_wales_participants_module.update_episode_status(
            'participant-id', 'episode-sort-key', EpisodeStatus.INVITED)

        mock_update_participant_episode_record.assert_called_with(
            {
                'participant_id': 'participant-id',
                'sort_key': 'episode-sort-key'
            },
            {
                'status': EpisodeStatus.INVITED.value,
                'user_id': 'SYSTEM'
            }
        )

    @patch(f'{MODULE_NAME}.datetime')
    @patch(f'{MODULE_NAME}.create_replace_record_in_participant_table')
    def test_cease_participant(
            self,
            mock_create_replace_record_in_participant_table,
            mock_datetime,
            mock_query_participants_by_nhs_number,
            mock_log):
        mock_datetime.strptime.return_value = datetime(2021, 8, 17, 11, 22, 33, 444, tzinfo=timezone.utc)

        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESPART0009}),
            call({'log_reference': LogReference.XBWALESPART0010})
        ]

        participant = {'participant_id': 'participant-id'}
        load_wales_participants_module.cease_or_defer_participant(
            EpisodeStatus.CEASED, participant, '2021-08-17', CeaseReason.DUE_TO_AGE)

        self.assertEqual(mock_log.call_count, 2)
        mock_log.assert_has_calls(expected_log_calls)
        mock_create_replace_record_in_participant_table.assert_called_with(
            {
                'participant_id': 'participant-id',
                'sort_key': 'CEASE#2021-08-17',
                'reason': CeaseReason.DUE_TO_AGE,
                'user_id': 'SYSTEM',
                'date_from': '2021-08-17T11:22:33.000444+00:00'
            })

    @patch(f'{MODULE_NAME}.datetime')
    @patch(f'{MODULE_NAME}.create_replace_record_in_participant_table')
    def test_defer_participant(
            self,
            mock_create_replace_record_in_participant_table,
            mock_datetime,
            mock_query_participants_by_nhs_number,
            mock_log):
        mock_datetime.strptime.return_value = datetime(2021, 8, 17, 11, 22, 33, 444, tzinfo=timezone.utc)

        expected_log_calls = [
            call({'log_reference': LogReference.XBWALESPART0011}),
            call({'log_reference': LogReference.XBWALESPART0012})
        ]

        participant = {'participant_id': 'participant-id', 'next_test_due_date': 'next-test-due-date'}
        load_wales_participants_module.cease_or_defer_participant(
            EpisodeStatus.DEFERRED, participant, '2021-08-17', None)

        self.assertEqual(mock_log.call_count, 2)
        mock_log.assert_has_calls(expected_log_calls)
        mock_create_replace_record_in_participant_table.assert_called_with(
            {
                'participant_id': 'participant-id',
                'sort_key': 'DEFER#2021-08-17',
                'user_id': 'SYSTEM',
                'date_from': '2021-08-17T11:22:33.000444+00:00',
                'previous_test_due_date': 'next-test-due-date'
            })
