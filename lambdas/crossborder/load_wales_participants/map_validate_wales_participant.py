
from datetime import datetime
from typing import Dict, List, Optional, Tuple
from common.models.participant import CeaseReason, EpisodeStatus, ParticipantStatus

from common.utils.participant_utils import sanitise_nhs_number
from common.utils.transform_and_store_utils import remove_empty_values


COLUMNS_WALES_PARTICIPANT = [
    'NhsNumber',
    'Forename1',
    'Forename2',
    'Surname',
    'DoB',
    'RecallDate',
    'InvitationDate',
    'NotificationDate',
    'RecallType',
    'RecallStatus',
    'CeaseCode'
]


def map_validate_wales_participant(row: List[str]) -> Tuple[Dict, Dict, List[str]]:
    field_count = len(row)
    expected_field_count = len(COLUMNS_WALES_PARTICIPANT)
    if field_count == len(COLUMNS_WALES_PARTICIPANT):
        return _map_participant(row)
    return {}, {}, [f'Incorrect field count. Expected {expected_field_count}, found  {field_count}']


def _map_participant(row: List[str]) -> Tuple[Dict, Dict, List[str]]:
    field_map = {
        'NhsNumber': ('nhs_number', _map_nhs_number),
        'RecallDate': ('next_test_due_date', _map_date),
        'RecallType': ('status', _map_participant_status)
    }

    mapped_participant, additional_fields, validation_errors = _map_validate(row, field_map,
                                                                             COLUMNS_WALES_PARTICIPANT)

    if not validation_errors:
        return mapped_participant, additional_fields, validation_errors

    return mapped_participant, additional_fields, validation_errors


def _map_validate(row: List[str], field_map: Dict, field_list: List[str]) -> Tuple[Dict, Dict, List[str]]:
    mapped_participant = {}
    validation_errors = []

    for field_name, (mapped_field_name, mapping_function) in field_map.items():
        field_index = field_list.index(field_name)
        mapped_value, field_validation_error = mapping_function(row[field_index])
        if mapped_value:
            mapped_participant[mapped_field_name] = mapped_value
        if field_validation_error:
            validation_errors.append(field_validation_error)

    additional_fields, validation_errors = _map_additional_fields(row, field_list, validation_errors)

    return remove_empty_values(mapped_participant), remove_empty_values(additional_fields), validation_errors


def _map_nhs_number(nhs_number: str) -> Tuple[str, Optional[str]]:
    sanitised_nhs_number = sanitise_nhs_number(nhs_number)
    if not sanitised_nhs_number:
        return None, 'No NHS number supplied'
    return sanitised_nhs_number, None


def _map_date(date_raw: str) -> Tuple[str, Optional[str]]:
    if not date_raw:
        return None, None
    if not len(date_raw) == 8:
        return None, 'Date not of correct length'
    try:
        date = datetime.strptime(date_raw, '%Y%m%d').date()
        return date.strftime("%Y-%m-%d"), None
    except ValueError:
        return None, 'Date is not in a valid format'


def _map_participant_status(recall_type_value: str) -> Tuple[str, Optional[str]]:
    if not recall_type_value:
        return None, None

    recall_type_mapping = {
        'L': ParticipantStatus.CALLED,
        'K': ParticipantStatus.CALLED,
        'A': ParticipantStatus.ROUTINE,
        'C': ParticipantStatus.CEASED,
        'R': ParticipantStatus.REPEAT_ADVISED,
        'S': ParticipantStatus.SUSPENDED,
        'D': ParticipantStatus.SUSPENDED
    }

    mapped_status = recall_type_mapping.get(recall_type_value)
    if mapped_status:
        return mapped_status.value, None
    return None, f'Participant status code {recall_type_value} is not valid'


def _map_episode_status(recall_status_value: str) -> Tuple[EpisodeStatus, Optional[str]]:
    if not recall_status_value:
        return None, None

    recall_status_mapping = {
        'G': EpisodeStatus.ACCEPTED,
        'X': EpisodeStatus.PNL,
        'P': EpisodeStatus.INVITED,
        '1': EpisodeStatus.INVITED,
        '2': EpisodeStatus.REMINDED,
        'F': EpisodeStatus.NRL,
        'H': EpisodeStatus.NRL,
        '3': EpisodeStatus.NRL,
        '4': EpisodeStatus.NRL,
        'C': EpisodeStatus.CEASED,
        'S': EpisodeStatus.DEFERRED,
        'L': EpisodeStatus.LOGGED,
        'Z': EpisodeStatus.WALKIN,
    }

    mapped_episode_status = recall_status_mapping.get(recall_status_value)
    if mapped_episode_status:
        return mapped_episode_status, None
    return None, f'Episode status code {recall_status_value} is not valid'


def _map_cease_reason(cease_code_value: str) -> Tuple[CeaseReason, Optional[str]]:
    if not cease_code_value:
        return None, None

    cease_code_mapping = {
        '6': CeaseReason.DUE_TO_AGE,
        '7': CeaseReason.NO_CERVIX,
        '8': CeaseReason.PATIENT_INFORMED_CHOICE,
        '9': CeaseReason.OTHER,
        '9C': CeaseReason.OTHER,
        'TAH': CeaseReason.NO_CERVIX,
        'CAC': CeaseReason.NO_CERVIX,
        'TRAH': CeaseReason.NO_CERVIX,
        'MANCH': CeaseReason.NO_CERVIX
    }

    mapped_cease_reason = cease_code_mapping.get(cease_code_value)
    if mapped_cease_reason:
        return mapped_cease_reason, None
    return None, f'Cease code {cease_code_value} is not valid'


def _map_additional_fields(row: List[str], field_list: Dict, validation_errors: Dict) -> Tuple[Dict, List[str]]:
    recall_type_index = field_list.index('RecallStatus')
    mapped_episode_status, episode_status_validation_error = _map_episode_status(row[recall_type_index])

    if episode_status_validation_error:
        validation_errors.append(episode_status_validation_error)

    notification_date_index = field_list.index('NotificationDate')
    mapped_notification_date, notification_date_validation_error = _map_date(row[notification_date_index])

    if notification_date_validation_error:
        validation_errors.append(notification_date_validation_error)

    cease_code_index = field_list.index('CeaseCode')
    mapped_cease_reason, cease_reason_validation_error = _map_cease_reason(row[cease_code_index])

    if cease_reason_validation_error:
        validation_errors.append(cease_reason_validation_error)

    invitation_date_index = field_list.index('InvitationDate')
    mapped_invited_date, invitation_date_validation_error = _map_date(row[invitation_date_index])

    if invitation_date_validation_error:
        validation_errors.append(invitation_date_validation_error)

    additional_fields = {
        'episode_status': mapped_episode_status,
        'notification_date': mapped_notification_date,
        'cease_reason': mapped_cease_reason,
        'invited_date': mapped_invited_date
    }

    return additional_fields, validation_errors
