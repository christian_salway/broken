import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    XBWALESPART0001 = (logging.INFO, 'Consumed message from the SQS queue')
    XBWALESPART0002 = (logging.WARNING, 'Participant mapping failed')
    XBWALESPART0003 = (logging.WARNING, 'Participant not found')
    XBWALESPART0004 = (logging.INFO, 'Participant successfully updated')
    XBWALESPART0005 = (logging.INFO, 'Creating participant episode')
    XBWALESPART0006 = (logging.INFO, 'Participant episode created successfully')
    XBWALESPART0007 = (logging.INFO, 'Updating participant episode')
    XBWALESPART0008 = (logging.INFO, 'Participant episode updated successfully')
    XBWALESPART0009 = (logging.INFO, 'Updating participant to ceased status')
    XBWALESPART0010 = (logging.INFO, 'Participant updated to ceased status successfully')
    XBWALESPART0011 = (logging.INFO, 'Updating participant to deferred status')
    XBWALESPART0012 = (logging.INFO, 'Participant updated to deferred status successfully')
    XBWALESPART0013 = (logging.WARNING, 'Too many matching participants')
    XBWALESPART0014 = (logging.WARNING, 'Could not create episode due to missing next test due date')
