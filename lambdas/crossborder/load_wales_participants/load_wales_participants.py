
from datetime import datetime, timezone
from typing import Dict
from common.log import log
from common.models.participant import CeaseReason, EpisodeStatus
from common.utils.audit_utils import AuditActions, AuditUsers, audit
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_episode_utils import update_participant_episode_record
from common.utils.participant_utils import create_replace_record_in_participant_table, query_participants_by_nhs_number
from common.utils.transform_and_store_utils import (get_message_contents_from_event,
                                                    update_log_metadata_from_record_event)
from load_wales_participants.log_references import LogReference
from load_wales_participants.map_validate_wales_participant \
    import map_validate_wales_participant


@lambda_entry_point
def lambda_handler(event, context):
    update_log_metadata_from_record_event(event)
    message_contents = get_message_contents_from_event(event)

    log({'log_reference': LogReference.XBWALESPART0001})
    mapped_participant, additional_fields, errors = map_validate_wales_participant(
        message_contents.get('record_line', []))

    if errors:
        for error in errors:
            log({'log_reference': LogReference.XBWALESPART0002, 'error': error})
        return

    nhs_number = mapped_participant['nhs_number']
    matching_participants = query_participants_by_nhs_number(mapped_participant['nhs_number'])

    if not matching_participants:
        log({'log_reference': LogReference.XBWALESPART0003, 'nhs_number': nhs_number})
        return
    if len(matching_participants) > 1:
        log({'log_reference': LogReference.XBWALESPART0013, 'nhs_number': nhs_number})
        return

    matched_participant = matching_participants[0]
    matched_participant.update(mapped_participant)
    create_replace_record_in_participant_table(matched_participant)
    log({'log_reference': LogReference.XBWALESPART0004})

    episode_status = additional_fields.get('episode_status')
    if episode_status:
        if episode_status in [EpisodeStatus.CEASED, EpisodeStatus.DEFERRED]:
            cease_or_defer_participant(episode_status, matched_participant, additional_fields.get(
                'notification_date'), additional_fields.get('cease_reason'))
            return

        log({'log_reference': LogReference.XBWALESPART0005})

        invited_date = additional_fields.get('invited_date')
        next_test_due_date = matched_participant.get('next_test_due_date')

        if next_test_due_date:
            if episode_status == EpisodeStatus.LOGGED:
                episode = create_episode(matched_participant, invited_date, episode_status)
            else:
                episode = create_episode(matched_participant, invited_date)

            create_replace_record_in_participant_table(episode)
            log({'log_reference': LogReference.XBWALESPART0006})

            participant_id = matched_participant['participant_id']
            audit(
                action=AuditActions.CREATE_EPISODE,
                user=AuditUsers.SYSTEM,
                participant_ids=[participant_id],
                nhs_numbers=[matched_participant.get('nhs_number', '')]
            )

            if episode_status not in [EpisodeStatus.LOGGED, EpisodeStatus.PNL]:
                log({'log_reference': LogReference.XBWALESPART0007})
                update_episode_status(participant_id, episode['sort_key'], episode_status)
                log({'log_reference': LogReference.XBWALESPART0008})
        else:
            log({'log_reference': LogReference.XBWALESPART0014})
            return


def create_episode(participant: Dict, invited_date, episode_status=EpisodeStatus.PNL):
    episode = {}
    next_test_due_date = participant['next_test_due_date']
    registered_gp_practice_code = participant.get('registered_gp_practice_code', '')
    participant_id = participant['participant_id']
    sort_key = f'EPISODE#{next_test_due_date}'
    registered_gp_practice_code_and_test_due_date = f'{registered_gp_practice_code}#{next_test_due_date}'
    utc_date = datetime.now(timezone.utc)
    create_date = utc_date.isoformat()

    episode = {
        'participant_id': participant_id,
        'sort_key': sort_key,
        'registered_gp_practice_code_and_test_due_date': registered_gp_practice_code_and_test_due_date,
        'created': create_date,
        'pnl_date': utc_date.strftime("%Y-%m-%d"),
        'test_due_date': next_test_due_date,
        'user_id': 'create_participant_episode',
        'status': episode_status.value,
        'live_record_status': episode_status.value,
        'invited_date': invited_date
    }

    return episode


def update_episode_status(participant_id: str, episode_sort_key: str, episode_status: EpisodeStatus):
    key = {
        'participant_id': participant_id,
        'sort_key': episode_sort_key
    }

    update_participant_episode_record(
        key,
        {
            'status': episode_status.value,
            'user_id': 'SYSTEM'
        }
    )


def cease_or_defer_participant(episode_status: EpisodeStatus,
                               participant: Dict, notification_date: str, cease_reason: CeaseReason):
    participant_id = participant['participant_id']
    date_from = datetime.strptime(notification_date, "%Y-%m-%d")

    if episode_status == EpisodeStatus.CEASED:
        log({'log_reference': LogReference.XBWALESPART0009})

        cease_record = {
            'participant_id': participant_id,
            'sort_key': f'CEASE#{date_from.date().isoformat()}',
            'reason': cease_reason,
            'user_id': 'SYSTEM',
            'date_from': date_from.isoformat(),
        }

        create_replace_record_in_participant_table(cease_record)
        log({'log_reference': LogReference.XBWALESPART0010})

        return

    if episode_status == EpisodeStatus.DEFERRED:
        log({'log_reference': LogReference.XBWALESPART0011})

        defer_record = {
            'participant_id': participant_id,
            'sort_key': f'DEFER#{date_from.date().isoformat()}',
            'date_from': date_from.isoformat(),
            'previous_test_due_date': participant['next_test_due_date'],
            'user_id': 'SYSTEM'
        }

        create_replace_record_in_participant_table(defer_record)
        log({'log_reference': LogReference.XBWALESPART0012})

        return
