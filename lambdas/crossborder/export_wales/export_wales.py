from boto3.dynamodb.conditions import Attr, Key
import csv
from common.log import log
from common.models.participant import CeaseReason, EpisodeStatus, ParticipantSortKey
from common.utils import chunk_list
from common.utils.audit_utils import AuditActions, AuditUsers, audit
from common.utils.dynamodb_access.operations import dynamodb_paginated_query
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.participant_utils import get_participant_and_test_results
from common.utils.s3_utils import put_object
from datetime import datetime, timezone
from export_wales.log_references import LogReference
from io import StringIO
from os import environ
from typing import Dict, List, Set

DATE_FORMAT = '%Y-%m-%d'
COMPLETED_EPISODE_STATUSES = [EpisodeStatus.COMPLETED, EpisodeStatus.CEASED, EpisodeStatus.CLOSED]
MAP_CEASE_CODES_TO_WALES = {
    CeaseReason.NO_CERVIX: '7',
    CeaseReason.DUE_TO_AGE: '6',
    CeaseReason.RADIOTHERAPY: '9C',
    CeaseReason.PATIENT_INFORMED_CHOICE: '8',
    CeaseReason.MENTAL_CAPACITY_ACT: '?',
    CeaseReason.AUTOCEASE_DUE_TO_AGE: '6',
    CeaseReason.OTHER: '9'
}
NOTFICATION_DATE_FIELDS = ['invited_date', 'reminded_date']
EXPORT_BUCKET = environ.get('EXPORT_BUCKET')
LIMIT_LINE_COUNT = environ.get('LINE_COUNT', 50000)


@lambda_entry_point
def lambda_handler(event, context):
    timestamp = generate_search_timestamp(event)
    log({'log_reference': LogReference.EXPWALES0001, 'date': timestamp})

    removal_participant_ids = get_reason_for_removal_participant_ids(timestamp)
    filtered_participants = get_participants_moving_to_wales(removal_participant_ids)
    export_participants(filtered_participants, EXPORT_BUCKET, timestamp)
    export_results(filtered_participants, EXPORT_BUCKET, timestamp)


def generate_search_timestamp(event: Dict) -> str:
    if 'date' in event:
        try:
            datetime.strptime(event['date'], DATE_FORMAT)
            return event['date']
        except ValueError:
            return datetime.now(timezone.utc).strftime(DATE_FORMAT)

    return datetime.now(timezone.utc).strftime(DATE_FORMAT)


def get_reason_for_removal_participant_ids(timestamp: str) -> Set[str]:
    log({'log_reference': LogReference.EXPWALES0002})
    audit_entries = dynamodb_paginated_query(
        TableNames.AUDIT,
        {
            'KeyConditionExpression': (
                Key('nhsid_useruid').eq('LOAD_DEMOGRAPHICS')
                & Key('sort_key').begins_with(timestamp)
            ),
            'FilterExpression': (
                Attr('action').begins_with('COHORT_')
                & Attr('changed_fields').contains('reason_for_removal_code')
            )
        }
    )

    participant_ids = [
        participant_id for entry in audit_entries for participant_id in entry.get('participant_ids', [])
    ]
    unique_participant_ids = set(participant_ids)

    log({'log_reference': LogReference.EXPWALES0003, 'unique_ids': len(unique_participant_ids)})
    return unique_participant_ids


def get_participants_moving_to_wales(participant_ids: Set[str]) -> List[Dict]:
    log({'log_reference': LogReference.EXPWALES0004})
    participants = []
    sorted_participant_ids = sorted(participant_ids)

    for participant_id in sorted_participant_ids:
        records = get_participant_and_test_results(participant_id)
        participant_records = [
            record for record in records
            if record['sort_key'] == ParticipantSortKey.PARTICIPANT
        ]

        if not participant_records:
            log({'log_reference': LogReference.EXPWALES0011, 'participant_id': participant_id})
            continue

        episode_records = [
            record for record in records
            if ParticipantSortKey.EPISODE in record['sort_key']
            and record.get('status') not in COMPLETED_EPISODE_STATUSES
        ]
        cease_records = [
            record for record in records
            if ParticipantSortKey.CEASE in record['sort_key']
        ]
        results = [
            record for record in records
            if ParticipantSortKey.RESULT in record['sort_key']
        ]

        participant = participant_records[0]
        if not participant.get('reason_for_removal_code') == 'CYM':
            log({'log_reference': LogReference.EXPWALES0012, 'participant_id': participant_id})
            continue

        participants.append(
            {
                'participant_id': participant_id,
                'participant': participant,
                'cease': get_most_recent(cease_records, 'date_from'),
                'episode': get_most_recent(episode_records, 'pnl_date'),
                'results': results
            }
        )

    log({'log_reference': LogReference.EXPWALES0005, 'participants': len(participants)})
    return participants


def get_most_recent(records: List[Dict], field: str) -> Dict:
    selected_record = None
    for record in records:
        if not selected_record or not selected_record.get(field) or record.get(field) > selected_record.get(field):
            selected_record = record

    return selected_record


def export_participants(participants: List[Dict], bucket: str, timestamp: str):

    lines = []
    participant_ids = []
    nhs_numbers = []
    log({'log_reference': LogReference.EXPWALES0006})

    for participant in participants:
        lines.append(participant_to_row(participant))
        participant_ids.append(participant['participant_id'])
        nhs_numbers.append(participant['participant']['nhs_number'])

    log(
        {
            'log_reference': LogReference.EXPWALES0008,
            'participant_ids': participant_ids
        }
    )
    audit(
        AuditActions.EXPORT_WALES_PARTICIPANT,
        user=AuditUsers.CROSSBORDER,
        participant_ids=participant_ids,
        nhs_numbers=nhs_numbers
    )

    line_chunks = chunk_list(lines, LIMIT_LINE_COUNT)
    for index, chunk in enumerate(line_chunks):
        file_content = chunk_to_csv_string(chunk)
        put_object(bucket, file_content, f'participants_{timestamp}_part{index + 1}.csv')
        log({'log_reference': LogReference.EXPWALES0007, 'part': index + 1})


def participant_to_row(participant_wrapper: Dict) -> List:
    participant = participant_wrapper['participant']
    episode = participant_wrapper['episode']
    cease = participant_wrapper['cease']

    if episode:
        return [
            participant.get('nhs_number'),
            participant.get('first_name'),
            participant.get('middle_names'),
            participant.get('last_name'),
            participant.get('date_of_birth'),
            participant.get('next_test_due_date'),
            episode.get('invited_date'),
            get_notification_date(episode),
            participant.get('status'),
            episode.get('status'),
            get_cease_reason(participant, cease)
        ]

    return [
        participant.get('nhs_number'),
        participant.get('first_name'),
        participant.get('middle_names'),
        participant.get('last_name'),
        participant.get('date_of_birth'),
        participant.get('next_test_due_date'),
        None,
        None,
        participant.get('status'),
        None,
        get_cease_reason(participant, cease)
    ]


def get_cease_reason(participant: Dict, cease: Dict) -> str:
    if participant.get('is_ceased') and cease:
        return MAP_CEASE_CODES_TO_WALES.get(cease['reason']) or '9'


def get_notification_date(episode: Dict) -> str:
    if episode:
        dates = [episode[field] for field in NOTFICATION_DATE_FIELDS if field in episode]
        return max(dates)


def export_results(participants: List[Dict], bucket: str, timestamp: str):

    lines = []
    log({'log_reference': LogReference.EXPWALES0009})

    for participant in participants:
        participant_lines = participant_to_result_lines(participant)
        if participant_lines:
            lines += participant_lines

            participant_id = participant['participant_id']
            nhs_number = participant['participant']['nhs_number']

            audit(
                AuditActions.EXPORT_WALES_RESULTS,
                user=AuditUsers.CROSSBORDER,
                participant_ids=[participant_id],
                nhs_numbers=[nhs_number],
                additional_information={
                    'result_count': len(participant_lines)
                }
            )

            log(
                {
                    'log_reference': LogReference.EXPWALES0014,
                    'participant_id': participant_id,
                    'result_count': len(participant_lines)
                }
            )

    line_chunks = chunk_list(lines, LIMIT_LINE_COUNT)
    for index, chunk in enumerate(line_chunks):
        file_content = chunk_to_csv_string(chunk)
        put_object(bucket, file_content, f'results_{timestamp}_part{index + 1}.csv')
        log({'log_reference': LogReference.EXPWALES0013})


def chunk_to_csv_string(chunk: List[str]) -> str:
    csv_string = StringIO()
    csv_writer = csv.writer(csv_string)
    csv_writer.writerows(chunk)
    return csv_string.getvalue()


def participant_to_result_lines(participant: Dict) -> List[List[str]]:
    episode = participant.get('episode')
    result_lines = []
    for result in participant.get('results', []):
        if episode and result['sort_key'] in episode.get('result_ids', []):
            result_lines.append(
                [
                    participant['participant']['nhs_number'],
                    result.get('result_id'),
                    result.get('test_date'),
                    episode.get('invited_date'),
                    None,
                    result.get('result_date'),
                    None,
                    None,
                    result.get('result_code'),
                    result.get('result'),
                    result.get('action_code'),
                    result.get('action'),
                    result.get('recall_months'),
                    result.get('infection_code'),
                    result.get('sending_lab'),
                    result.get('slide_number'),
                    result.get('source_code'),
                    crm_to_comment_string(result),
                    None,
                    None,
                    result.get('hpv_primary'),
                    1 if result.get('self_sample') else 0
                ]
            )
        else:
            result_lines.append(
                [
                    participant['participant']['nhs_number'],
                    result.get('result_id'),
                    result.get('test_date'),
                    None,
                    None,
                    result.get('result_date'),
                    None,
                    None,
                    result.get('result_code'),
                    result.get('result'),
                    result.get('action_code'),
                    result.get('action'),
                    result.get('recall_months'),
                    result.get('infection_code'),
                    result.get('sending_lab'),
                    result.get('slide_number'),
                    result.get('source_code'),
                    crm_to_comment_string(result),
                    None,
                    None,
                    result.get('hpv_primary'),
                    1 if result.get('self_sample') else 0
                ]
            )

    return result_lines


def crm_to_comment_string(result: Dict):
    crm = result.get('crm')
    if crm:
        return crm.get('comments')
