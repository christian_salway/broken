import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    EXPWALES0001 = (logging.INFO, 'Starting Welsh export process')
    EXPWALES0002 = (
        logging.INFO,
        'Starting search for participant IDs where reason_for_removal_code was amended by PDS'
    )
    EXPWALES0003 = (
        logging.INFO,
        'Completed search for participant IDs where reason_for_removal_code was amended by PDS'
    )
    EXPWALES0004 = (logging.INFO, 'Started filtering participants for move to Wales')
    EXPWALES0005 = (logging.INFO, 'Completed filtering participants for move to Wales')
    EXPWALES0006 = (logging.INFO, 'Started writing participants CSV file')
    EXPWALES0007 = (logging.INFO, 'Completed writing participants CSV file')
    EXPWALES0008 = (logging.INFO, 'Wrote participant to CSV file')
    EXPWALES0009 = (logging.INFO, 'Started writing results CSV file')
    EXPWALES0010 = (logging.INFO, 'Retrieving cease reason for participant')
    EXPWALES0011 = (logging.WARNING, 'No participant found matching a participant ID from audit')
    EXPWALES0012 = (logging.INFO, 'Participant did not move to Wales')
    EXPWALES0013 = (logging.INFO, 'Completed writing results CSV file')
    EXPWALES0014 = (logging.INFO, 'Wrote result to CSV file')
