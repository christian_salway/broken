from boto3.dynamodb.conditions import Attr, Key
from common.utils.audit_utils import AuditActions, AuditUsers
from common.utils.dynamodb_access.table_names import TableNames
from datetime import datetime, timezone
from ddt import data, ddt, unpack
from export_wales.log_references import LogReference
import json
from mock import call, patch
from pathlib import Path
from unittest.case import TestCase

MODULE_NAME = 'export_wales.export_wales'
DIRECTORY_NAME = Path(__file__).resolve().parent


@ddt
@patch(f'{MODULE_NAME}.log')
@patch(f'{MODULE_NAME}.audit')
class TestExportCrossBorderWales(TestCase):
    @patch(
        'os.environ',
        {'EXPORT_BUCKET': 'BucketName'}
    )
    def setUp(self):
        import export_wales.export_wales as _module
        self.module = _module
        self.unwrapped_function = self.module.lambda_handler.__wrapped__

    @unpack
    @data(
        ({}, '2021-08-17'),
        ({'date': '2021-02-28'}, '2021-02-28')
    )
    @patch(f'{MODULE_NAME}.datetime')
    def test_generate_search_timestamp(
        self,
        event,
        expected_timestamp,
        mock_datetime,
        mock_audit,
        mock_log
    ):
        mock_datetime.now.return_value = datetime(2021, 8, 17, 11, 22, 33, 444, tzinfo=timezone.utc)
        actual_timestamp = self.module.generate_search_timestamp(event)
        self.assertEqual(expected_timestamp, actual_timestamp)

    @patch(f'{MODULE_NAME}.dynamodb_paginated_query')
    def test_get_reason_for_removal_participant_ids(
        self,
        mock_dynamodb_query,
        mock_audit,
        mock_log
    ):
        mock_dynamodb_query.return_value = [
            {},
            {'participant_ids': ['PART1']},
            {'participant_ids': ['PART2', 'PART3', 'PART4', 'PART5']},
        ]

        expected_participant_ids = set(['PART1', 'PART2', 'PART3', 'PART4', 'PART5'])

        expected_log_calls = [
            call({'log_reference': LogReference.EXPWALES0002}),
            call({'log_reference': LogReference.EXPWALES0003, 'unique_ids': 5})
        ]

        actual_participant_ids = self.module.get_reason_for_removal_participant_ids('2021-02-28')

        self.assertEqual(expected_participant_ids, actual_participant_ids)
        mock_log.assert_has_calls(expected_log_calls)
        mock_dynamodb_query.assert_called_with(
            TableNames.AUDIT,
            {
                'KeyConditionExpression': (
                    Key('nhsid_useruid').eq('LOAD_DEMOGRAPHICS')
                    & Key('sort_key').begins_with('2021-02-28')
                ),
                'FilterExpression': (
                    Attr('action').begins_with('COHORT_')
                    & Attr('changed_fields').contains('reason_for_removal_code')
                )
            }
        )

    @unpack
    @data(
        (
            'full_record_set.json',
            [
                call({'log_reference': LogReference.EXPWALES0004}),
                call({'log_reference': LogReference.EXPWALES0005, 'participants': 1})
            ]
        ),
        (
            'participant_other_removal.json',
            [
                call({'log_reference': LogReference.EXPWALES0004}),
                call({'log_reference': LogReference.EXPWALES0012, 'participant_id': '1'}),
                call({'log_reference': LogReference.EXPWALES0005, 'participants': 0})
            ]
        ),
        (
            'no_records.json',
            [
                call({'log_reference': LogReference.EXPWALES0004}),
                call({'log_reference': LogReference.EXPWALES0011, 'participant_id': '1'}),
                call({'log_reference': LogReference.EXPWALES0005, 'participants': 0})
            ]
        )
    )
    @patch(f'{MODULE_NAME}.get_participant_and_test_results')
    def test_get_participants_moving_to_wales(
        self,
        test_records_file_name,
        expected_log_calls,
        mock_get_participants,
        mock_audit,
        mock_log
    ):
        complete_test_file_name = DIRECTORY_NAME / 'fixtures' / test_records_file_name
        with open(complete_test_file_name) as json_file:
            mock_get_participants.return_value = json.load(json_file)

        complete_expected_file_name = DIRECTORY_NAME / 'expected' / test_records_file_name
        with open(complete_expected_file_name) as json_file:
            expected_participants = json.load(json_file)

        actual_participants = self.module.get_participants_moving_to_wales(['1'])

        self.assertEqual(expected_participants, actual_participants)
        mock_log.assert_has_calls(expected_log_calls)
        mock_get_participants.assert_called_with('1')

    @unpack
    @data(
        (
            [{'date': '2021-01-01'}, {'date': '2021-01-02'}],
            'date',
            {'date': '2021-01-02'}
        ),
        (
            [{'date': '2021-01-01'}, {'date': '2021-01-02'}],
            'foo',
            {'date': '2021-01-02'}
        ),
        (
            [{'fudge': '2021-01-01'}, {'date': '2021-01-02'}],
            'date',
            {'date': '2021-01-02'}
        ),
        (
            [],
            'date',
            None
        )
    )
    def test_get_most_recent(
        self,
        records,
        field,
        expected_result,
        mock_audit,
        mock_log
    ):
        actual_result = self.module.get_most_recent(records, field)
        self.assertEqual(expected_result, actual_result)

    @unpack
    @data(
        (
            'participants.json',
            'participants.csv',
            [
                '8ca262a7-b734-48b6-a41e-59f78ca0d937',
                '11f8f288-ce71-499d-9cb6-8fcba978360d',
                '2849009f-6d68-4a02-9169-4fb02fccee67'
            ],
            [
                '5064294220',
                '1055329536',
                '7859022311'
            ]
        )
    )
    @patch(f'{MODULE_NAME}.put_object')
    def test_export_participants(
        self,
        input_file_name,
        output_file_name,
        participant_ids,
        nhs_numbers,
        mock_put_object,
        mock_audit,
        mock_log
    ):
        full_input_file_name = DIRECTORY_NAME / 'fixtures' / input_file_name
        with open(full_input_file_name) as json_file:
            participants = json.load(json_file)

        full_output_file_name = DIRECTORY_NAME / 'expected' / output_file_name
        with open(full_output_file_name, newline='\r\n') as out_file:
            expected_output = out_file.read()

        expected_log_calls = [
            call(
                {
                    'log_reference': LogReference.EXPWALES0008,
                    'participant_ids': participant_ids
                }
            ),
            call(
                {'log_reference': LogReference.EXPWALES0007, 'part': 1}
            )
        ]

        expected_audit_calls = [
            call(
                AuditActions.EXPORT_WALES_PARTICIPANT,
                user=AuditUsers.CROSSBORDER,
                participant_ids=participant_ids,
                nhs_numbers=nhs_numbers
            )
        ]

        self.module.export_participants(participants, 'BUCKET_NAME', '2021-09-10')

        mock_put_object.assert_called_with(
            'BUCKET_NAME',
            expected_output,
            'participants_2021-09-10_part1.csv'
        )

        mock_log.assert_has_calls(expected_log_calls)
        mock_audit.assert_has_calls(expected_audit_calls)

    @unpack
    @data(
        (
            'participants.json',
            'results.csv',
            [
                '8ca262a7-b734-48b6-a41e-59f78ca0d937',
                '11f8f288-ce71-499d-9cb6-8fcba978360d',
                '2849009f-6d68-4a02-9169-4fb02fccee67'
            ],
            [
                '5064294220',
                '1055329536',
                '7859022311'
            ],
            [
                3,
                1,
                1
            ]
        )
    )
    @patch(f'{MODULE_NAME}.put_object')
    def test_export_results(
        self,
        input_file_name,
        output_file_name,
        participant_ids,
        nhs_numbers,
        line_counts,
        mock_put_object,
        mock_audit,
        mock_log
    ):
        full_input_file_name = DIRECTORY_NAME / 'fixtures' / input_file_name
        with open(full_input_file_name) as json_file:
            participants = json.load(json_file)

        full_output_file_name = DIRECTORY_NAME / 'expected' / output_file_name
        with open(full_output_file_name, newline='\r\n') as out_file:
            expected_output = out_file.read()

        participant_count = len(participant_ids)

        expected_log_calls = [
            call(
                {
                    'log_reference': LogReference.EXPWALES0014,
                    'participant_id': participant_ids[i],
                    'result_count': line_counts[i]
                }
            )
            for i in range(participant_count)
        ]
        expected_audit_calls = []

        expected_log_calls = [
            call({'log_reference': LogReference.EXPWALES0009}),
            *expected_log_calls,
            call({'log_reference': LogReference.EXPWALES0013})
        ]

        expected_audit_calls = [
            call(
                AuditActions.EXPORT_WALES_RESULTS,
                user=AuditUsers.CROSSBORDER,
                participant_ids=[participant_ids[i]],
                nhs_numbers=[nhs_numbers[i]],
                additional_information={'result_count': line_counts[i]}
            )
            for i in range(participant_count)
        ]

        self.module.export_results(participants, 'BUCKET_NAME', '2021-09-10')

        mock_put_object.assert_called_with(
            'BUCKET_NAME',
            expected_output,
            'results_2021-09-10_part1.csv'
        )

        mock_log.assert_has_calls(expected_log_calls)
        mock_audit.assert_has_calls(expected_audit_calls)
