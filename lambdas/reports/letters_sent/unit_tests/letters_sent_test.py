import json
from unittest import TestCase
from mock import patch, MagicMock, Mock
from botocore.response import StreamingBody
from io import BytesIO
from datetime import datetime

from letters_sent.letters_sent import letters_sent_ROOT


@patch('letters_sent.letters_sent.log', MagicMock())
@patch('common.utils.log', MagicMock())
@patch('common.utils.lambda_wrappers.log', MagicMock())
@patch('common.log', MagicMock())
class TestLetterSent(TestCase):
    mock_env_vars = {
        'ATHENA_BUCKET': None
    }

    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    @patch('os.environ', mock_env_vars)
    def setUp(self):
        import letters_sent.letters_sent as _letters_sent
        import common.utils.session_utils as _session_utils
        self.letters_sent_module = _letters_sent
        self.session_utils = _session_utils
        self.context = Mock()
        self.context.function_name = ''
        self.event = create_event_fixture()

    @patch('letters_sent.letters_sent.read_bucket_with_subfolders')
    @patch('letters_sent.letters_sent.get_athena_bucket')
    def test_lambda_returns_500_if_csv_has_wrong_format_CIC(self, athena_get_bucket_mock, subfolder_mock):

        self.event = create_event_fixture(role='csas')

        # prepare mock for the s3 bucket
        subfolder_mock.return_value = {
            'Contents': [
                {'Key': '/s3_path/2021/04/30/filename/someresult.csv'},
                {'Key': '/s3_path/2021/04/30/filename/someresult.csv.metadata'}
            ],
            'CommonPrefixes': [
                {'Prefix': 'filename/'}
            ]
        }

        body = '\n'.join([
            'filePath,fileSendDate,fileSendTime,letterType,letterCode,called,cancelled,routine,repeat,inadequate,\
suspended,total',
            's3://path/folder/filename.csv,14/04/2021,15:30,RP,XA-1,10'
        ]).encode()

        bucket_mock = Mock()
        bucket_mock.objects.filter.return_value = [
            _create_object_summary('/s3_path/2021/04/30/filename/someresult.csv', datetime(year=2020, month=1, day=1))]
        s3_object = Mock()
        s3_object.get.return_value = {'Body': StreamingBody(BytesIO(body), len(body))}
        bucket_mock.Object.return_value = s3_object
        athena_get_bucket_mock.return_value = bucket_mock
        response = self.letters_sent_module.lambda_handler(self.event, self.context)

        subfolder_mock.assert_called_with(None, f'{letters_sent_ROOT}CIC/2021/04/30/')
        athena_get_bucket_mock.assert_called()

        self.assertEqual(500, response['statusCode'])

    @patch('letters_sent.letters_sent.read_bucket_with_subfolders')
    @patch('letters_sent.letters_sent.get_athena_bucket')
    def test_lambda_returns_500_if_csv_has_wrong_types_IOM(self, athena_get_bucket_mock, subfolder_mock):
        self.event = create_event_fixture(role='csas_iom')

        # prepare mock for the s3 bucket
        subfolder_mock.return_value = {
            'Contents': [
                {'Key': '/s3_path/2021/04/30/filename/someresult.csv'},
                {'Key': '/s3_path/2021/04/30/filename/someresult.csv.metadata'}
            ],
            'CommonPrefixes': [
                {'Prefix': 'filename/'}
            ]
        }

        body = '\n'.join([
            'filePath,fileSendDate,fileSendTime,letterType,letterCode,called,cancelled,routine,repeat,inadequate,\
suspended,total',
            's3://path/folder/filename.csv,14/04/2021,15:30,RP,XA-1letterType,10,0,1,4,6,7,28',
            's3://path/folder/filename.csv,14/04/2021,15:30,RP,XA-1letterType,4,0,Bee,4,6,7,21',
            's3://path/folder/filename.csv,14/04/2021,15:30,RP,XA-1letterType,6,1,2,4,11,7,31'
        ]).encode()

        bucket_mock = Mock()
        bucket_mock.objects.filter.return_value = [
            _create_object_summary('/s3_path/2021/04/30/filename/someresult.csv', datetime(year=2020, month=1, day=1))]
        s3_object = Mock()
        s3_object.get.return_value = {'Body': StreamingBody(BytesIO(body), len(body))}
        bucket_mock.Object.return_value = s3_object
        athena_get_bucket_mock.return_value = bucket_mock
        response = self.letters_sent_module.lambda_handler(self.event, self.context)

        subfolder_mock.assert_called_with(None, f'{letters_sent_ROOT}IOM/2021/04/30/')
        athena_get_bucket_mock.assert_called()

        self.assertEqual(500, response['statusCode'])

    @patch('letters_sent.letters_sent.read_bucket_with_subfolders')
    @patch('letters_sent.letters_sent.get_athena_bucket')
    def test_lambda_returns_400_if_date_is_incorrect(self, athena_bucket_mock, subfolder_mock):
        self.event['pathParameters'] = {'date': '2021-30'}

        response = self.letters_sent_module.lambda_handler(self.event, self.context)

        subfolder_mock.asset_not_called()
        athena_bucket_mock.assert_not_called()

        self.assertEqual(400, response['statusCode'])

    @patch('letters_sent.letters_sent.read_bucket_with_subfolders')
    @patch('letters_sent.letters_sent.get_athena_bucket')
    def test_lambda_returns_200_if_operation_successful_with_no_csv_DMS(self, athena_bucket_mock, subfolder_mock):
        self.event = create_event_fixture(role='csas_dms')

        # prepare mock for the s3 bucket
        subfolder_mock.return_value = {
            'Contents': [],
            'CommonPrefixes': []
        }

        response = self.letters_sent_module.lambda_handler(self.event, self.context)

        subfolder_mock.assert_called_with(None, f'{letters_sent_ROOT}DMS/2021/04/30/')
        athena_bucket_mock.assert_not_called()

        self.assertEqual(200, response['statusCode'])

    @patch('letters_sent.letters_sent.read_bucket_with_subfolders')
    @patch('letters_sent.letters_sent.get_athena_bucket')
    def test_lambda_returns_200_if_operation_successful_with_file_CIC(self, athena_get_bucket_mock, subfolder_mock):
        self.event = create_event_fixture(role='csas')

        # prepare mock for the s3 bucket
        subfolder_mock.return_value = {
            'Contents': [
                {'Key': '/s3_path/2021/04/30/filename/someresult.csv'},
                {'Key': '/s3_path/2021/04/30/filename/someresult.csv.metadata'}
            ],
            'CommonPrefixes': [
                {'Prefix': 'filename/'}
            ]
        }

        body = '\n'.join([
            'filePath,fileSendDate,fileSendTime,letterType,letterCode,called,cancelled,routine,repeat,inadequate,\
suspended,total',
            's3://path/folder/filename.csv,14/04/2021,15:30,RP,XA-1letterType,10,0,1,4,6,7,28'
        ]).encode()

        bucket_mock = Mock()
        bucket_mock.objects.filter.return_value = [
            _create_object_summary('/s3_path/2021/04/30/filename/someresult.csv', datetime(year=2020, month=1, day=1))]
        s3_object = Mock()
        s3_object.get.return_value = {'Body': StreamingBody(BytesIO(body), len(body))}
        bucket_mock.Object.return_value = s3_object
        athena_get_bucket_mock.return_value = bucket_mock

        response = self.letters_sent_module.lambda_handler(self.event, self.context)

        subfolder_mock.assert_called_with(None, f'{letters_sent_ROOT}CIC/2021/04/30/')
        athena_get_bucket_mock.assert_called()

        self.assertEqual(200, response['statusCode'])

    @patch('letters_sent.letters_sent.read_bucket_with_subfolders')
    @patch('letters_sent.letters_sent.get_athena_bucket')
    def test_lambda_returns_200_with_cp_letter_type_IOM(self, athena_get_bucket_mock, subfolder_mock):
        self.event = create_event_fixture(role='csas_iom', letterTypes='CP')

        # prepare mock for the s3 bucket
        subfolder_mock.return_value = {
            'Contents': [
                {'Key': '/s3_path/2021/04/30/filename/someresult.csv'},
                {'Key': '/s3_path/2021/04/30/filename/someresult.csv.metadata'}
            ],
            'CommonPrefixes': [
                {'Prefix': 'filename/'}
            ]
        }

        body = '\n'.join([
            'filePath,fileSendDate,fileSendTime,letterType,letterCode,called,cancelled,routine,repeat,inadequate,\
suspended,total',
            's3://path/folder/filename.csv,14/04/2021,15:30,RP,XA-1letterType,10,0,1,4,6,7,28',
            's3://path/folder/filename.csv,14/04/2021,15:30,CP,XA-1letterType,10,0,1,4,6,7,28'
        ]).encode()

        bucket_mock = Mock()
        bucket_mock.objects.filter.return_value = [
            _create_object_summary('/s3_path/2021/04/30/filename/someresult.csv', datetime(year=2020, month=1, day=1))]
        s3_object = Mock()
        s3_object.get.return_value = {'Body': StreamingBody(BytesIO(body), len(body))}
        bucket_mock.Object.return_value = s3_object
        athena_get_bucket_mock.return_value = bucket_mock

        response = self.letters_sent_module.lambda_handler(self.event, self.context)

        subfolder_mock.assert_called_with(None, f'{letters_sent_ROOT}IOM/2021/04/30/')
        athena_get_bucket_mock.assert_called()

        parsed_response = json.loads(response['body'])

        self.assertEqual(200, response['statusCode'])
        self.assertEqual(1, len(parsed_response['data']))
        self.assertEqual('CP', parsed_response['data'][0]['letterType'])

    @patch('letters_sent.letters_sent.read_bucket_with_subfolders')
    @patch('letters_sent.letters_sent.get_athena_bucket')
    def test_lambda_returns_200_with_invalid_letter_type_DMS(self, athena_get_bucket_mock, subfolder_mock):
        self.event = create_event_fixture(role='csas_dms', letterTypes='fake')

        # prepare mock for the s3 bucket
        subfolder_mock.return_value = {
            'Contents': [
                {'Key': '/s3_path/2021/04/30/filename/someresult.csv'},
                {'Key': '/s3_path/2021/04/30/filename/someresult.csv.metadata'}
            ],
            'CommonPrefixes': [
                {'Prefix': 'filename/'}
            ]
        }

        body = '\n'.join([
            'filePath,fileSendDate,fileSendTime,letterType,letterCode,called,cancelled,routine,repeat,inadequate,\
suspended,total',
            's3://path/folder/filename.csv,14/04/2021,15:30,RP,XA-1letterType,10,0,1,4,6,7,28',
            's3://path/folder/filename.csv,14/04/2021,15:30,CP,XA-1letterType,10,0,1,4,6,7,28'
        ]).encode()

        bucket_mock = Mock()
        bucket_mock.objects.filter.return_value = [
            _create_object_summary('/s3_path/2021/04/30/filename/someresult.csv', datetime(year=2020, month=1, day=1))]
        s3_object = Mock()
        s3_object.get.return_value = {'Body': StreamingBody(BytesIO(body), len(body))}
        bucket_mock.Object.return_value = s3_object
        athena_get_bucket_mock.return_value = bucket_mock

        response = self.letters_sent_module.lambda_handler(self.event, self.context)

        subfolder_mock.assert_called_with(None, f'{letters_sent_ROOT}DMS/2021/04/30/')
        athena_get_bucket_mock.assert_called()

        parsed_response = json.loads(response['body'])

        self.assertEqual(200, response['statusCode'])
        self.assertEqual(2, len(parsed_response['data']))


def _create_object_summary(key, last_modified):
    object_summary = Mock()
    object_summary.key = key
    object_summary.last_modified = last_modified
    return object_summary


def create_event_fixture(role: str = 'csas', letterTypes: str = None):
    event_mock = {
        'pathParameters': {
            'date': '2021-04-30'
        },
        'requestContext': {
            'authorizer': {
                'principalId': 'blah'
            }
        },
        'headers': {}
    }
    session = {
        'session_id': '12345678',
        'access_groups': {
            'csas': True,
            'csas_dms': False,
            'csas_iom': False
        }
    }
    if letterTypes is not None:
        event_mock['queryStringParameters'] = {"letterTypes": letterTypes}

    session['access_groups'][role] = True
    event_mock['requestContext']['authorizer']['session'] = json.dumps(session)
    return event_mock
