import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    LSR0001 = (logging.INFO, 'Received API call for letter report')
    LSR0002 = (logging.INFO, 'No CSV file on that date - no CommonPrefixes')
    LSR0005 = (logging.INFO, 'No CSV file on that date - no files')
    LSR0008 = (logging.INFO, 'Successfully fetched the session and the role')

    LSR0003 = (logging.ERROR, 'Date provided is invalid')
    LSR0004 = (logging.ERROR, 'CSV file processed is invalid')
    LSR0006 = (logging.ERROR, 'CSV error in converting an element')
    LSR0007 = (logging.ERROR, 'Unable to retrieve user groups from the user\'s session')
    LSR0009 = (logging.ERROR, 'The role does not have access to this resource')
