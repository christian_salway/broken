import codecs
import csv
import os
from datetime import datetime

from boto3 import resource
from botocore.client import Config
from common.log import log
from common.utils.lambda_wrappers import api_lambda, lambda_entry_point
from common.utils.session_utils import get_session_from_lambda_event
from common.utils.roles_utils import determine_access_group_from_session
from common.utils.s3_utils import read_bucket_with_subfolders
from common.utils import json_return_message, json_return_object

from letters_sent.log_references import LogReference

ATHENA_BUCKET_NAME = os.environ.get('ATHENA_BUCKET')
letters_sent_ROOT = 'reports/letters_sent/'
ATHENA_BUCKET = None


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):

    log({'log_reference': LogReference.LSR0001})

    session = get_session_from_lambda_event(event)
    if not session or not session.get('access_groups'):
        log({'log_reference': LogReference.LSR0007})
        return json_return_message(403, 'Session missing, expired or not logged in.')

    access_group = determine_access_group_from_session(session)
    if access_group is None:
        log({'log_reference': LogReference.LSR0009})
        return json_return_message(403, 'Your access group does not have access to this resource.')
    log({'log_reference': LogReference.LSR0008, 'access_group': access_group})

    request_date = event['pathParameters'].get('date')
    parsed_date = parse_date(request_date)
    if not parsed_date:
        log({'log_reference': LogReference.LSR0003, 'date': request_date})
        return json_return_message(400, 'Date provided has no valid format')

    folder_filter = f'{letters_sent_ROOT}{access_group}/{parsed_date:%Y/%m/%d}/'

    query_parameters = event.get('queryStringParameters') if event.get('queryStringParameters') is not None else {}
    letter_filter_type = query_parameters.get('letterTypes')
    if letter_filter_type not in ('RP', 'CP'):
        letter_filter_type = ''

    # get the print files list from s3
    files_in_bucket = read_bucket_with_subfolders(ATHENA_BUCKET_NAME, folder_filter)
    if not files_in_bucket.get('CommonPrefixes'):
        log({'log_reference': LogReference.LSR0002, 'bucket': ATHENA_BUCKET_NAME, 'subfolder': folder_filter})
        return json_return_object(200, {'data': []})

    print_files = [item['Prefix'] for item in files_in_bucket.get('CommonPrefixes')]
    if not print_files:
        log({'log_reference': LogReference.LSR0005, 'bucket': ATHENA_BUCKET_NAME, 'subfolder': folder_filter})
        return json_return_object(200, {'data': []})

    # filter the latest version of report for each print file
    latest_versions = {}
    for print_file in print_files:
        objects = get_athena_bucket().objects.filter(Prefix=print_file)
        objects_csv = filter(lambda obj: obj.key.endswith('.csv'), objects)
        if objects_csv:
            latest_report = sorted(objects_csv, key=lambda obj: obj.last_modified, reverse=True)[0]
            latest_versions[print_file] = latest_report.key

    letter_list = []

    for key, value in latest_versions.items():
        # get S3 file as stream
        data = get_athena_bucket().Object(value).get()

        line = 1
        for row in csv.DictReader(codecs.getreader('utf-8')(data['Body']), delimiter=','):
            if any(not v for v in row.values()):
                log({'log_reference': LogReference.LSR0004, 'filename': key})
                return json_return_message(500, 'Internal Server Error')
            try:
                line += 1
                if letter_filter_type == '' or letter_filter_type == row['letterType']:
                    element = {
                        'letterType': row['letterType'],
                        'fileSendDate': row['fileSendDate'],
                        'fileSendTime': row['fileSendTime'],
                        'letterCode': row['letterCode'],
                        'filePath': row['filePath'],
                        'called': int(row['called']),
                        'cancelled': int(row['cancelled']),
                        'inadequate': int(row['inadequate']),
                        'repeat': int(row['repeat']),
                        'routine': int(row['routine']),
                        'suspended': int(row['suspended']),
                        'total': int(row['total'])
                    }
                    letter_list.append(element)
            except ValueError:
                log({'log_reference': LogReference.LSR0006, 'filename': key, 'line': line})
                return json_return_message(500, 'Internal Server Error')

    return json_return_object(200, {'data': letter_list})


def parse_date(date):
    try:
        parsed_date = datetime.strptime(date, "%Y-%m-%d")
    except ValueError:
        return None
    return parsed_date


def get_athena_bucket():
    global ATHENA_BUCKET
    if not ATHENA_BUCKET:
        s3_resource = resource('s3', config=Config(retries={'max_attempts': 3}))
        ATHENA_BUCKET = s3_resource.Bucket(ATHENA_BUCKET_NAME)
    return ATHENA_BUCKET
