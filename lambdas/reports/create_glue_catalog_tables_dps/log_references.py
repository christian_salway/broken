import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    CGTDPS001 = (logging.INFO, 'Starting the create_glue_catalog_tables_dps lambda')
    CGTDPS002 = (logging.ERROR, 'Create table failed')
    CGTDPS003 = (logging.INFO, 'Proceeding to creating table with a valid arn input')
    CGTDPS004 = (logging.DEBUG, 'Generating SQL for a query')
    CGTDPS005 = (logging.INFO, 'Setting the variables for create table function')
    CGTDPS006 = (logging.INFO, 'Create glue table completed successfuly')
    CGTDPS007 = (logging.INFO, 'Finished runing the lambda')
