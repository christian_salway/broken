CREATE TABLE {dps_table}
    WITH ( format = 'parquet', external_location = '{csms_output_s3}' ) AS  
        SELECT 
            item.participant_id.s AS participant_id,  
            item.sort_key.s AS sort_key,  
            item.nhs_number.s AS nhs_number,  
            item.action.s AS action,  
            item.hpv_primary.bool AS hpv_primary,  
            item.sending_lab.s AS sending_lab,  
            item.result_date.s AS result_date,  
            item.result_code.s AS result_code,  
            item.self_sample.bool AS self_sample,  
            item.result.s AS result,  
            item.source_code.s AS source_code,  
            item.recall_months.s AS recall_months,  
            item.test_date.s AS test_date,  
            item.action_code.s AS action_code,  
            item.slide_number.s AS slide_number  
        FROM 
            {source_table}
        WHERE 
            item.sort_key.s LIKE 'RESULT%'
