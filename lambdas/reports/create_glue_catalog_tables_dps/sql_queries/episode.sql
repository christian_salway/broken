CREATE TABLE {dps_table}
    WITH ( format = 'parquet', external_location = '{csms_output_s3}' ) AS  
        SELECT
            item.participant_id.s AS "participant_id",
            item.sort_key.s AS "sort_key",
            item.created.s AS "created",
            item.test_due_date.s AS "test_due_date",
            item.registered_gp_practice_code.s AS "registered_gp_practice_code",
            item.pnl_date.s AS "pnl_date",
            item.invited_date.s AS "invited_date",
            item.test_date.s AS "test_date",
            item.result_date.s AS "result_date",
            item.status.s AS "status"
        FROM 
            {source_table}
        WHERE 
            item.sort_key.s like 'EPISODE%'
