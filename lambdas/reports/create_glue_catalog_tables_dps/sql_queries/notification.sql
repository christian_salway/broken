CREATE TABLE {dps_table}
    WITH ( format = 'parquet', external_location = '{csms_output_s3}' ) AS  
        SELECT
            item.participant_id.s AS "participant_id",
            item.sort_key.s AS "sort_key",
            item.notification_date.s AS "notification_date",
            item.created.s AS "created",
            item.type.s AS "type", item.subtype.s AS "subtype",
            item.letter_template.s AS "letter_template",
            item.status.s AS "status",
            item.reason_not_produced_code.s AS "reason_not_produced_code",
            item.reason_not_produced.s AS "reason_not_produced"
        FROM 
            {source_table}
        WHERE 
            item.sort_key.s LIKE 'NOTIFICATION%'
