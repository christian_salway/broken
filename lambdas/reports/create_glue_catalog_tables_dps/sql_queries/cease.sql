CREATE TABLE {dps_table} 
    WITH ( format = 'parquet', external_location = '{csms_output_s3}' ) AS 
        SELECT
            item.participant_id.s AS "participant_id",
            item.sort_key.s AS "sort_key",
            item.reason.s AS "reason",
            item.date_from.s AS "date_from"
        FROM 
            {source_table}
        WHERE 
            item.sort_key.s LIKE 'CEASE%'
