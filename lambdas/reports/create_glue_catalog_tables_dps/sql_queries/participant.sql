CREATE TABLE {dps_table}
    WITH ( format = 'parquet', external_location = '{csms_output_s3}' ) AS
        SELECT 
            item.participant_id.s AS participant_id,
            item.nhs_number.s AS nhs_number,
            item.title.s AS title,
            item.first_name.s AS first_name,
            item.last_name.s AS last_name,
            item.date_of_birth.s AS date_of_birth,
            item.gender.s AS gender,
            item.active.bool AS active,
            item.is_ceased.bool AS is_ceased,
            item.status.s AS status,
            item.next_test_due_date.s AS next_test_due_date,
            item.registered_gp_practice_code.s AS registered_gp_practice_code,
            item.address.m.address_line_1.s AS address_line_1,
            item.address.m.address_line_2.s AS address_line_2,
            item.address.m.address_line_3.s AS address_line_3,
            item.address.m.address_line_4.s AS address_line_4,
            item.address.m.postcode.s AS postcode
        FROM 
            {source_table}
        WHERE 
            item.sort_key.s = 'PARTICIPANT'
