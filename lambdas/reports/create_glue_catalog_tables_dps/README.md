# Lambda: Create Glue catalogue tables for DPS

This function will create Glue catalog table for DPS pipeline.
It will also generate and return 6 SQL CTAS queries:
1. participant
2. result
3. episode
4. notification
5. defer
6. ceased

arguments:
    event['export_arn']
outputs:
    dict_keys([‘athena_workgroup’, ‘output_location’, ‘participant_SQL’, ‘result_SQL’, ‘episode_SQL’, ‘notification_SQL’, ‘defer_SQL’, ‘cease_SQL’])

## Testing:
* component test: only test the boto3 function: create_glue_table()
* unit test: patch create_glue_table() and boto3. Test the lambda hanler under happy and sad path.