import os
from unittest import TestCase
from mock import Mock, patch, call
from create_glue_catalog_tables_dps.log_references import LogReference

module = 'create_glue_catalog_tables_dps.create_glue_catalog_tables_dps'
env_vars = {
    'ENVIRONMENT_NAME': 'cerss-1428',
    'AWS_ACCOUNT_ID': '092130162833',
    'CTAS_OUTPUT_BUCKET': 'CSMS_BUCKET_ID'
}

with patch('os.environ', env_vars):
    from create_glue_catalog_tables_dps.create_glue_catalog_tables_dps import create_glue_table


@patch('boto3.resource', Mock())
@patch('boto3.client', Mock())
class TestCreateGlueTable(TestCase):

    def setUp(self):
        print("Setting up the test")

        self.file_path = os.path.dirname(__file__)
        self.log_patcher = patch(f'{module}.log').start()
        self.client_mock = patch(f'{module}.client').start()

        self.test_function_paramters = {
            'exported_table_unique_id': '01628161335184-d7fd425d',
            'exported_table_name': 'export_01628161335184-d7fd425d',
            'output_location': 'FAKE/LOCATION/'
        }

        self.expected_create_table_return = {
            'ResponseMetadata': {
                'RequestId': '368785eb-2ed2-4b70-83b1-4a3f12f01037',
                'HTTPStatusCode': 200,
                'HTTPHeaders': {'date':
                                'Fri, 03 Sep 2021 11:02:21 GMT',
                                'content-type': 'application/x-amz-json-1.1',
                                'content-length': '2',
                                'connection': 'keep-alive',
                                'x-amzn-requestid': '368785eb-2ed2-4b70-83b1-4a3f12f01037'
                                },
                'RetryAttempts': 0
            }
        }

    def tearDown(self):
        patch.stopall()

    def test_create_glue_table(self):
        glue_client_mock = Mock()
        glue_client_mock.create_table.return_value = self.expected_create_table_return
        self.client_mock.return_value = glue_client_mock

        glue_response = create_glue_table(**self.test_function_paramters)
        with open(os.path.join(self.file_path, 'struct_columns.txt')) as f:
            struct_columns = f.read()

        # Assert create_glue_table called with test_function_paramters
        glue_client_mock.create_table.assert_called_once_with(
            CatalogId=env_vars['AWS_ACCOUNT_ID'],
            DatabaseName=f"{env_vars['ENVIRONMENT_NAME']}-dps-glue-catalog",
            TableInput={
                'Name': self.test_function_paramters['exported_table_name'],
                'Description': 'This table describes the exported participant table data',
                'Owner': 'Reports Value Stream',
                'Retention': 1,
                'StorageDescriptor': {
                    'Columns': [
                        {
                            'Name': 'item',
                            'Type': struct_columns,
                            'Comment': 'string'
                        },
                    ],
                    'Location': 's3://cerss-1428-092130162833-reports/FAKE/'
                                'LOCATION/AWSDynamoDB/01628161335184-d7fd425d/data/',
                    'InputFormat': 'org.apache.hadoop.mapred.TextInputFormat',
                    'OutputFormat': 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat',
                    'Compressed': True,
                    'NumberOfBuckets': -1,
                    'SerdeInfo': {
                        'Name': 'JSO',
                        'SerializationLibrary': 'org.openx.data.jsonserde.JsonSerDe',
                        'Parameters': {'paths': 'Item'}
                    },
                    'BucketColumns': [],
                    'SortColumns': [],
                    'SkewedInfo': {},
                    'StoredAsSubDirectories': False
                }
            }
        )
        # Assert output matches the expected
        self.assertEqual(glue_response, self.expected_create_table_return)

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.CGTDPS006})
        ])
