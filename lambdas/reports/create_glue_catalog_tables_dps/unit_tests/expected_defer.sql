CREATE TABLE "cerss-1428-dps-glue-catalog"."dps_defer_01628161335184-d7fd425d" 
    WITH ( format = 'parquet', external_location = 's3://CSMS_BUCKET_ID/dps-pipeline-exports/s3/csms/2020/07/03/defer/tables/01628161335184-d7fd425d' ) AS  
        SELECT
            item.participant_id.s AS "participant_id",
            item.sort_key.s AS "sort_key",
            item.reason.s AS "reason",
            item.date_from.s AS "date_from",
            item.new_test_due_date.s AS "new_test_due_date",
            item.previous_test_due_date.s AS "previous_test_due_date"
        FROM
            "cerss-1428-dps-glue-catalog"."export_01628161335184-d7fd425d"
        WHERE 
            item.sort_key.s LIKE 'DEFER%'
