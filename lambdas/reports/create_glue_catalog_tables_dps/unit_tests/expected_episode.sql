CREATE TABLE "cerss-1428-dps-glue-catalog"."dps_episode_01628161335184-d7fd425d"
    WITH ( format = 'parquet', external_location = 's3://CSMS_BUCKET_ID/dps-pipeline-exports/s3/csms/2020/07/03/episode/tables/01628161335184-d7fd425d' ) AS  
        SELECT
            item.participant_id.s AS "participant_id",
            item.sort_key.s AS "sort_key",
            item.created.s AS "created",
            item.test_due_date.s AS "test_due_date",
            item.registered_gp_practice_code.s AS "registered_gp_practice_code",
            item.pnl_date.s AS "pnl_date",
            item.invited_date.s AS "invited_date",
            item.test_date.s AS "test_date",
            item.result_date.s AS "result_date",
            item.status.s AS "status"
        FROM 
            "cerss-1428-dps-glue-catalog"."export_01628161335184-d7fd425d"
        WHERE 
            item.sort_key.s like 'EPISODE%'
