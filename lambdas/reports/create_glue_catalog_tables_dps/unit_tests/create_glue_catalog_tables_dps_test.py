import os
from unittest import TestCase
from mock import Mock, patch, call
from datetime import datetime, timezone
from botocore.exceptions import ClientError
from create_glue_catalog_tables_dps.log_references import LogReference

module = 'create_glue_catalog_tables_dps.create_glue_catalog_tables_dps'
env_vars = {
    'ENVIRONMENT_NAME': 'cerss-1428',
    'AWS_ACCOUNT_ID': '092130162833',
    'CTAS_OUTPUT_BUCKET': 'CSMS_BUCKET_ID',
    'ACCOUNT_ID': '092130162833'
}


@patch('boto3.resource', Mock())
@patch('boto3.client', Mock())
class CreateGlueCatalogTablesDps(TestCase):
    maxDiff = None

    @patch('os.environ', env_vars)
    def setUp(self):

        self.file_path = os.path.dirname(__file__)
        self.log_patcher = patch(f'{module}.log').start()
        self.client_mock = patch(f'{module}.client').start()
        self.datetime_mock = patch(f'{module}.datetime').start()
        self.datetime_mock.now.return_value = datetime(2020, 7, 3, 6, 22, 23, 233, tzinfo=timezone.utc)

        self.event = {
            'export_arn': 'arn:aws:dynamodb:eu-west-2:092130162833:'
            'table/training-participants/export/01628161335184-d7fd425d',
            'output_location': 'FAKE/LOCATION/'
        }

        self.expected_return = {
            'athena_workgroup': 'cerss-1428-reports-workgroup',
            'output_location': 's3://CSMS_BUCKET_ID/dps-pipeline-exports/s3/csms/2020/07/03',
            'file_prefix': 's3/csms/2020/07/03',
            'participant_SQL': '',
            'result_SQL': '',
            'episode_SQL': '',
            'notification_SQL': '',
            'defer_SQL': '',
            'cease_SQL': ''
        }

        # Read expected queries and update the expected return dictionary
        tables_to_export = ['participant', 'result', 'episode', 'notification', 'defer', 'cease']
        for table in tables_to_export:
            with open(os.path.join(self.file_path, f'expected_{table}.sql')) as f:
                sql_query = f.read()
                self.expected_return[f'{table}_SQL'] = sql_query

        self.happypath_create_table_return = {
            'ResponseMetadata': {
                'RequestId': '368785eb-2ed2-4b70-83b1-4a3f12f01037',
                'HTTPStatusCode': 200,
                'HTTPHeaders': {'date':
                                'Fri, 03 Sep 2021 11:02:21 GMT',
                                'content-type': 'application/x-amz-json-1.1',
                                'content-length': '2',
                                'connection': 'keep-alive',
                                'x-amzn-requestid': '368785eb-2ed2-4b70-83b1-4a3f12f01037'
                                },
                'RetryAttempts': 0
            }
        }

        from create_glue_catalog_tables_dps.create_glue_catalog_tables_dps import lambda_handler
        self.lambda_handler = lambda_handler

    def tearDown(self):
        patch.stopall()

    @patch(f'{module}.create_glue_table')
    def test_lambda_handler_without_already_existing_glue_table(self, mock_create_glue_table):

        create_table_mock = Mock()
        create_table_mock.create_table.return_value = self.happypath_create_table_return
        mock_create_glue_table.return_value = create_table_mock

        result = self.lambda_handler.__wrapped__(event=self.event, context="")
        self.assertDictEqual(self.expected_return, result)

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.CGTDPS006})])

    @patch(f'{module}.create_glue_table', side_effect=ClientError({'Error': {'Code': '404'}}, 'Table already exist'))
    def test_lambda_handler_with_already_existing_glue_table(self, mock_create_glue_table):

        self.client_mock.return_value = mock_create_glue_table

        with self.assertRaises(ClientError):
            self.lambda_handler.__wrapped__(event=self.event, context="")
            self.log_patcher.assert_has_calls([call({'log_reference': LogReference.CGTDPS002})])
