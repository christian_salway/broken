CREATE TABLE "cerss-1428-dps-glue-catalog"."dps_result_01628161335184-d7fd425d"
    WITH ( format = 'parquet', external_location = 's3://CSMS_BUCKET_ID/dps-pipeline-exports/s3/csms/2020/07/03/result/tables/01628161335184-d7fd425d' ) AS  
        SELECT 
            item.participant_id.s AS participant_id,  
            item.sort_key.s AS sort_key,  
            item.nhs_number.s AS nhs_number,  
            item.action.s AS action,  
            item.hpv_primary.bool AS hpv_primary,  
            item.sending_lab.s AS sending_lab,  
            item.result_date.s AS result_date,  
            item.result_code.s AS result_code,  
            item.self_sample.bool AS self_sample,  
            item.result.s AS result,  
            item.source_code.s AS source_code,  
            item.recall_months.s AS recall_months,  
            item.test_date.s AS test_date,  
            item.action_code.s AS action_code,  
            item.slide_number.s AS slide_number  
        FROM 
            "cerss-1428-dps-glue-catalog"."export_01628161335184-d7fd425d"
        WHERE 
            item.sort_key.s LIKE 'RESULT%'
