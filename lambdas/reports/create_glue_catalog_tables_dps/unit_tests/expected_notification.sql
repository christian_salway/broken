CREATE TABLE "cerss-1428-dps-glue-catalog"."dps_notification_01628161335184-d7fd425d"
    WITH ( format = 'parquet', external_location = 's3://CSMS_BUCKET_ID/dps-pipeline-exports/s3/csms/2020/07/03/notification/tables/01628161335184-d7fd425d' ) AS  
        SELECT
            item.participant_id.s AS "participant_id",
            item.sort_key.s AS "sort_key",
            item.notification_date.s AS "notification_date",
            item.created.s AS "created",
            item.type.s AS "type", item.subtype.s AS "subtype",
            item.letter_template.s AS "letter_template",
            item.status.s AS "status",
            item.reason_not_produced_code.s AS "reason_not_produced_code",
            item.reason_not_produced.s AS "reason_not_produced"
        FROM 
            "cerss-1428-dps-glue-catalog"."export_01628161335184-d7fd425d"
        WHERE 
            item.sort_key.s LIKE 'NOTIFICATION%'
