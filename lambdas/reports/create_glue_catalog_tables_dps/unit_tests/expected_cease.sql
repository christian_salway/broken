CREATE TABLE "cerss-1428-dps-glue-catalog"."dps_cease_01628161335184-d7fd425d" 
    WITH ( format = 'parquet', external_location = 's3://CSMS_BUCKET_ID/dps-pipeline-exports/s3/csms/2020/07/03/cease/tables/01628161335184-d7fd425d' ) AS 
        SELECT
            item.participant_id.s AS "participant_id",
            item.sort_key.s AS "sort_key",
            item.reason.s AS "reason",
            item.date_from.s AS "date_from"
        FROM 
            "cerss-1428-dps-glue-catalog"."export_01628161335184-d7fd425d"
        WHERE 
            item.sort_key.s LIKE 'CEASE%'
