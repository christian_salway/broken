import os
from datetime import datetime, timezone

from boto3 import client
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from create_glue_catalog_tables_dps.log_references import LogReference

ENVIRONMENT_NAME = os.getenv('ENVIRONMENT_NAME')
ATHENA_WORKGROUP = f'{ENVIRONMENT_NAME}-reports-workgroup'
AWS_ACCOUNT_ID = os.getenv('AWS_ACCOUNT_ID')
FILE_PATH = os.path.dirname(__file__)
GLUE_DATABASE = f'{ENVIRONMENT_NAME}-dps-glue-catalog'
CSMS_BUCKET = os.environ.get('CTAS_OUTPUT_BUCKET')


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.CGTDPS001})

    # Get the arn of the exported table
    if 'export_arn' not in event:
        log({'log_reference': LogReference.CGTDPS002, 'Invalid export arn': event})
        raise AssertionError(f'Invalid event: {event}')
    else:
        exported_table_arn = event['export_arn']
        log({'log_reference': LogReference.CGTDPS003, 'Exported table arn': exported_table_arn})

    # Get the uniqe identifier of the exported table
    exported_table_unique_id = exported_table_arn.split('/')[-1]
    log({'log_reference': LogReference.CGTDPS005, 'Exported table unique identifier': exported_table_unique_id})

    if exported_table_unique_id == exported_table_arn:
        raise AssertionError(f'Failed to extract name from export arn: {exported_table_unique_id}')

    file_prefix = f's3/csms/{datetime.now(timezone.utc):%Y/%m/%d}'
    csms_output_location = f's3://{CSMS_BUCKET}/dps-pipeline-exports/{file_prefix}'

    # Setting constants to format the SQL queries
    exported_table_name = f'export_{exported_table_unique_id}'
    log({'log_reference': LogReference.CGTDPS005, 'Exported table name': exported_table_name})

    source_table = f'"{GLUE_DATABASE}"."{exported_table_name}"'
    log({'log_reference': LogReference.CGTDPS005, 'Source table name': source_table})

    output_location = event['output_location']
    log({'log_reference': LogReference.CGTDPS005, 'Output location': source_table})

    create_glue_table(exported_table_unique_id, exported_table_name, output_location)
    log({'log_reference': LogReference.CGTDPS006})

    output_dict = {
        'athena_workgroup': ATHENA_WORKGROUP,
        'output_location': csms_output_location,
        'file_prefix': file_prefix
    }

    sql_queries = generate_sql_queries(exported_table_unique_id, source_table, csms_output_location)
    output_dict.update(sql_queries)
    log({'log_reference': LogReference.CGTDPS007, 'Lambda return': output_dict})

    return output_dict


def create_glue_table(exported_table_unique_id, exported_table_name, output_location):
    with open(os.path.join(FILE_PATH, 'struct_columns.txt')) as f:
        struct_columns = f.read()

    exported_table_path = f's3://{ENVIRONMENT_NAME}-{AWS_ACCOUNT_ID}-reports/'\
                          f'{output_location}AWSDynamoDB/{exported_table_unique_id}/data/'
    glue_client = client('glue')
    glue_response = glue_client.create_table(
        CatalogId=AWS_ACCOUNT_ID,
        DatabaseName=GLUE_DATABASE,
        TableInput={
            'Name': exported_table_name,
            'Description': 'This table describes the exported participant table data',
            'Owner': 'Reports Value Stream',
            'Retention': 1,
            'StorageDescriptor': {
                'Columns': [
                    {
                        'Name': 'item',
                        'Type': struct_columns,
                        'Comment': 'string'
                    },
                ],
                'Location': exported_table_path,
                'InputFormat': 'org.apache.hadoop.mapred.TextInputFormat',
                'OutputFormat': 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat',
                'Compressed': True,
                'NumberOfBuckets': -1,
                'SerdeInfo': {
                    'Name': 'JSO',
                    'SerializationLibrary': 'org.openx.data.jsonserde.JsonSerDe',
                    'Parameters': {'paths': 'Item'}
                },
                'BucketColumns': [],
                'SortColumns': [],
                'SkewedInfo': {},
                'StoredAsSubDirectories': False
            }
        }
    )

    response_status = glue_response.get('ResponseMetadata', {}).get('HTTPStatusCode')
    response_metadata = glue_response.get('ResponseMetadata', "No response found")
    if response_status != 200:
        log({'log_reference': LogReference.CGTDPS002, 'Glue response': response_metadata})
        raise AssertionError('Create glue catalog table failed.')
    else:
        log({'log_reference': LogReference.CGTDPS006})

    return glue_response


def generate_sql_queries(exported_table_unique_id, source_table, csms_output_location):
    tables_to_export = ['participant', 'result', 'episode', 'notification', 'defer', 'cease']
    sql_queries = {}

    for table in tables_to_export:
        log({'log_reference': LogReference.CGTDPS004, 'Generating SQL query for table:': table})
        dps_table = f'"{GLUE_DATABASE}"."dps_{table}_{exported_table_unique_id}"'
        csms_output_s3 = f'{csms_output_location}/{table}/tables/{exported_table_unique_id}'
        with open(os.path.join(FILE_PATH, f'sql_queries/{table}.sql')) as f:
            sql_query = f.read()
            query_formated = sql_query.format(source_table=source_table,
                                              dps_table=dps_table,
                                              csms_output_s3=csms_output_s3)
            sql_queries[f'{table}_SQL'] = query_formated

    return sql_queries
