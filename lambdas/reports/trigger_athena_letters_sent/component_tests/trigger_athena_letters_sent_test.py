from unittest import TestCase
from mock import Mock, patch, call
import json
from trigger_athena_letters_sent.log_references import LogReference

env_vars = {
    'ENVIRONMENT_NAME': 'my-env',
    'ATHENA_WORKGROUP_NAME': 'my-wg',
    'MASTER_KEY_ARN': 'my-key',
    'ATHENA_RESULT_OUTPUT_BUCKET': 'my-bucket'
}
with patch('os.environ', env_vars):
    from trigger_athena_letters_sent.trigger_athena_letters_sent import lambda_handler


class TestTriggerAthenaForLettersSent(TestCase):

    @patch('trigger_athena_letters_sent.trigger_athena_letters_sent.get_internal_id', Mock(return_value='1123'))
    @patch('trigger_athena_letters_sent.trigger_athena_letters_sent.log')
    @patch('trigger_athena_letters_sent.trigger_athena_letters_sent.boto3')
    def test_lambda_handler(self, boto3_mock, log_mock):
        athena_client_mock = Mock()
        boto3_mock.client.return_value = athena_client_mock

        athena_client_mock.start_query_execution.return_value = {'QueryExecutionId': '1114'}
        event = {'Records': [{'body': json.dumps({'Records': [{
            's3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'CIC/CSAS_EU-WEST-2_CP_2105261128.dat'}}
        }]})}]}
        lambda_handler.__wrapped__(event, Mock())
        athena_client_mock.start_query_execution.assert_called_with(
            QueryString='    SELECT "filePath", "fileSendDate","fileSendTime","letterType","lettcode" AS "letterCode"'  
            ',        SUM(CASE WHEN "recall_type" = \'CALLED\' THEN "lettcount" ELSE 0 END) AS "called",        '
            'SUM(CASE WHEN "recall_type" = \'ROUTINE\' THEN "lettcount" ELSE 0 END) AS "routine",        '
            'SUM(CASE WHEN "recall_type" = \'REPEAT_ADVISED\' THEN "lettcount" ELSE 0 END) AS "repeat",        '
            'SUM(CASE WHEN "recall_type" = \'INADEQUATE\' THEN "lettcount" ELSE 0 END) AS "inadequate",        '
            'SUM(CASE WHEN "recall_type" = \'SUSPENDED\' THEN "lettcount" ELSE 0 END) AS "suspended",        '
            'SUM(CASE WHEN "recall_type" = \'CEASED\' THEN "lettcount" ELSE 0 END) AS "cancelled",        '
            'SUM(CASE WHEN "recall_type" = \'CALLED\' THEN "lettcount" ELSE 0 END) +        '
            'SUM(CASE WHEN "recall_type" = \'ROUTINE\' THEN "lettcount" ELSE 0 END) +        '
            'SUM(CASE WHEN "recall_type" = \'REPEAT_ADVISED\' THEN "lettcount" ELSE 0 END) +        '
            'SUM(CASE WHEN "recall_type" = \'INADEQUATE\' THEN "lettcount" ELSE 0 END) +        '
            'SUM(CASE WHEN "recall_type" = \'SUSPENDED\' THEN "lettcount" ELSE 0 END) +        '
            'SUM(CASE WHEN "recall_type" = \'CEASED\' THEN "lettcount" ELSE 0 END) As total    '
            'FROM (        SELECT split("$path", \'/\')[5] "filePath",            '
            'split("$path", \'_\')[3] "letterType",            '
            'date_format(date_parse(substr(split("$path", \'_\')[4], 1, 6), \'%y%m%d\'), \'%Y-%m-%d\')               '
            ' "fileSendDate",            '
            'date_format(date_parse(substr(split("$path", \'_\')[4], 7, 6), \'%H%i%s\'), \'%H:%i:%s\') "fileSendTime",'
            '            "lettcode" , "recall_type" , "count"(*) "lettcount"            '
            'FROM "my-env-reports"."my-env-letters-sent-cic-table"            '
            'WHERE "$path" =  \'s3://my-bucket/CIC/CSAS_EU-WEST-2_CP_2105261128.dat\'            '
            'GROUP BY "$path", "lettcode","recall_type"        )    GROUP BY "filePath", "lettcode","letterType",'
            '"fileSendDate","fileSendTime"    ORDER BY "lettcode" ASC    ',
            ClientRequestToken='LETTERS_SENT1123',
            QueryExecutionContext={
                'Database': 'my-env-reports', 'Catalog': 'AWSDataCatalog'
            },
            ResultConfiguration={
                'OutputLocation':
                    's3://my-bucket/reports/letters_sent/CIC/2021/05/26/CSAS_EU-WEST-2_CP_2105261128.dat',
                'EncryptionConfiguration': {'EncryptionOption': 'SSE_KMS', 'KmsKey': 'my-key'}
            },
            WorkGroup='my-wg')

        expected_log_calls = [call({'log_reference': LogReference.REPORTS_LETTERSSENT001,
                                    'file_path': 'CIC/CSAS_EU-WEST-2_CP_2105261128.dat'}),
                              call({'log_reference': LogReference.REPORTS_LETTERSSENT002, 'QueryExecutionId': '1114'})]

        log_mock.assert_has_calls(expected_log_calls)
