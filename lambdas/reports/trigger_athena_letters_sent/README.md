Fill this in



# trigger_athena_letters_sent

- [Split Result File](#trigger-athen-letters-sent)
  - [Process](#process)
  - [Architecture](#architecture)
## Process

>Previous step is [../notifications/send_print_file](../../notifications/send_print_file/README.md)

>Next step is [](..//README.md)

Triggers the athena query to generate the letters sent report upon arrival of new file in the print-files-completed bucket.

Stores the output of the query in the athena-output bucket

[JIRA](https://nhsd-jira.digital.nhs.uk/browse/CERSS-2048)

## Architecture

See the technical overview for more information: https://nhsd-confluence.digital.nhs.uk/display/CSP/Letters+Sent+Report+SDD
