import os
import json
import boto3
from datetime import datetime
from botocore.client import Config
from common.log import log, get_internal_id
from common.utils.lambda_wrappers import lambda_entry_point
from common.models.participant import ParticipantStatus
from trigger_athena_letters_sent.log_references import LogReference


_ATHENA_CLIENT = None
ENVIRONMENT_NAME = os.environ.get('ENVIRONMENT_NAME')
ATHENA_RESULT_OUTPUT_BUCKET = os.environ.get('ATHENA_RESULT_OUTPUT_BUCKET')
ATHENA_WORKGROUP_NAME = os.environ.get('ATHENA_WORKGROUP_NAME')
MASTER_KEY_ARN = os.environ.get('MASTER_KEY_ARN')


def get_athena_client():
    global _ATHENA_CLIENT
    if not _ATHENA_CLIENT:
        _ATHENA_CLIENT = boto3.client('athena', region_name='eu-west-2', config=Config(retries={'max_attempts': 3}))
    return _ATHENA_CLIENT


@lambda_entry_point
def lambda_handler(event, context):
    internal_id = get_internal_id()
    message_body = json.loads(event['Records'][0]['body'])
    bucket_name = message_body['Records'][0]['s3']['bucket']['name']
    object_key = message_body['Records'][0]['s3']['object']['key']
    region_folder, *_, file_name = object_key.split('/')
    file_time = datetime.strptime(file_name.split('_')[3][:-4], '%y%m%d%H%M%S')
    LETTERS_SENT_QUERY_TEXT = f"""
    SELECT "filePath", "fileSendDate","fileSendTime","letterType","lettcode" AS "letterCode",
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.CALLED}\' THEN "lettcount" ELSE 0 END) AS "called",
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.ROUTINE}\' THEN "lettcount" ELSE 0 END) AS "routine",
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.REPEAT_ADVISED}\' THEN "lettcount" ELSE 0 END) AS "repeat",
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.INADEQUATE}\' THEN "lettcount" ELSE 0 END) AS "inadequate",
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.SUSPENDED}\' THEN "lettcount" ELSE 0 END) AS "suspended",
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.CEASED}\' THEN "lettcount" ELSE 0 END) AS "cancelled",

        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.CALLED}\' THEN "lettcount" ELSE 0 END) +
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.ROUTINE}\' THEN "lettcount" ELSE 0 END) +
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.REPEAT_ADVISED}\' THEN "lettcount" ELSE 0 END) +
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.INADEQUATE}\' THEN "lettcount" ELSE 0 END) +
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.SUSPENDED}\' THEN "lettcount" ELSE 0 END) +
        SUM(CASE WHEN "recall_type" = \'{ParticipantStatus.CEASED}\' THEN "lettcount" ELSE 0 END) As total

    FROM (
        SELECT split("$path", \'/\')[5] "filePath",
            split("$path", \'_\')[3] "letterType",
            date_format(date_parse(substr(split("$path", \'_\')[4], 1, 6), \'%y%m%d\'), \'%Y-%m-%d\')
                "fileSendDate",
            date_format(date_parse(substr(split("$path", \'_\')[4], 7, 6), \'%H%i%s\'), \'%H:%i:%s\') "fileSendTime",
            "lettcode" , "recall_type" , "count"(*) "lettcount"
            FROM "{ENVIRONMENT_NAME}-reports"."{ENVIRONMENT_NAME}-letters-sent-{region_folder.lower()}-table"
            WHERE "$path" =  \'s3://{bucket_name}/{object_key}\'
            GROUP BY "$path", "lettcode","recall_type"
        )
    GROUP BY "filePath", "lettcode","letterType","fileSendDate","fileSendTime"
    ORDER BY "lettcode" ASC
    """

    log({'log_reference': LogReference.REPORTS_LETTERSSENT001, 'file_path': object_key})

    result = get_athena_client().start_query_execution(
        QueryString=LETTERS_SENT_QUERY_TEXT.replace('\n', ''),
        ClientRequestToken='LETTERS_SENT' + internal_id,
        QueryExecutionContext={
            'Database': f'{ENVIRONMENT_NAME}-reports',
            'Catalog': 'AWSDataCatalog'
        },
        ResultConfiguration={
            'OutputLocation': f's3://{ATHENA_RESULT_OUTPUT_BUCKET}/reports/letters_sent/'
                              f'{region_folder}/{file_time:%Y/%m/%d}/{file_name}',
            'EncryptionConfiguration': {
                'EncryptionOption': 'SSE_KMS',
                'KmsKey': MASTER_KEY_ARN
            }
        },
        WorkGroup=ATHENA_WORKGROUP_NAME
    )

    log({'log_reference': LogReference.REPORTS_LETTERSSENT002, 'QueryExecutionId': result['QueryExecutionId']})
