import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    REPORTS_LETTERSSENT001 = (logging.INFO, 'Triggring athena query')
    REPORTS_LETTERSSENT002 = (logging.INFO, 'Successfully triggered athena query')
