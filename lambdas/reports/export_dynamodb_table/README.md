# Start/Check a Export job. 
### This is part of the DPS Pipeline

This function will start exporting the DynamoDB table_name has been entered as parameter or check the status of a existing job if the export_arn is present.
