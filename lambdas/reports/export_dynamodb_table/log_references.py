import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    EXPORT001 = (logging.INFO, 'Received request to export participants table')
    EXPORT002 = (logging.INFO, 'Received request to check export status')
    EXPORT003 = (logging.INFO, 'Export table to point in time started')
    EXPORT004 = (logging.INFO, 'Status of the export job')
    EXPORT005 = (logging.ERROR, 'Table name not specified')
    EXPORT006 = (logging.ERROR, 'Export is not allowed on table')
