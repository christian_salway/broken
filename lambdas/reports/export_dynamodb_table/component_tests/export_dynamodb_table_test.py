from datetime import date
from unittest import TestCase
from mock import Mock, patch, call
from export_dynamodb_table.log_references import LogReference

module = 'export_dynamodb_table.export_dynamodb_table'
env_vars = {
    'DYNAMODB_PARTICIPANTS_ARN': 'participants-table-arn',
    'DYNAMODB_AUDIT_ARN': 'audit-table-arn',
    'REPORTS_BUCKET': 'reports-bucket',
    'MASTER_KEY_ID': 'fake-master-key-id'
}


class TestExportDynamodbTable(TestCase):

    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    @patch('os.environ', env_vars)
    def setUp(self):
        internal_id = '1123'
        self.response = {
            'ExportDescription': {
                'ExportStatus': 'IN_PROGRESS',
                'ExportArn': 'FAKE-ARN',
                'S3Prefix': 'FAKE/LOCATION/'
            }
        }
        self.expected_result = {
            'internal_id': internal_id,
            'export_arn': self.response['ExportDescription']['ExportArn'],
            'job_status': self.response['ExportDescription']['ExportStatus'],
            'output_location': self.response['ExportDescription']['S3Prefix']
        }
        patch(f'{module}.get_internal_id', Mock(return_value=internal_id)).start()
        self.get_dynamodb_client_mock = patch(f'{module}.get_dynamodb_client').start()
        self.log_patcher = patch(f'{module}.log').start()

        from export_dynamodb_table.export_dynamodb_table import lambda_handler
        self.lambda_handler = lambda_handler

    def tearDown(self):
        patch.stopall()

    def test_lambda_handler_with_export_arn(self):
        event = {'export_arn': 'FAKE-ARN'}
        context = Mock()
        client_mock = Mock()
        client_mock.describe_export.return_value = self.response
        self.get_dynamodb_client_mock.return_value = client_mock

        result = self.lambda_handler.__wrapped__(event, context)
        client_mock.describe_export.assert_called_once_with(ExportArn=event['export_arn'])

        self.assertEquals(self.expected_result, result)

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.EXPORT002, 'export_arn': event['export_arn']}),
            call({'log_reference': LogReference.EXPORT004,
                  'job_status': self.response['ExportDescription']['ExportStatus']})
        ])

    def test_lambda_handler_without_export_arn_and_with_table_name(self):
        event = {}
        context = Mock()
        client_mock = Mock()
        client_mock.export_table_to_point_in_time.return_value = self.response
        self.get_dynamodb_client_mock.return_value = client_mock

        result = self.lambda_handler.__wrapped__(event, context)

        client_mock.export_table_to_point_in_time.assert_called_once_with(
            TableArn=env_vars['DYNAMODB_PARTICIPANTS_ARN'],
            S3Bucket=env_vars['REPORTS_BUCKET'],
            S3Prefix=f'CSMS/{date.today():%Y/%m/%d}/participant/',
            S3SseAlgorithm='KMS',
            S3SseKmsKeyId=env_vars['MASTER_KEY_ID'],
            ExportFormat='DYNAMODB_JSON'
        )
        self.assertEquals(self.expected_result, result)

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.EXPORT001}),
            call({'log_reference': LogReference.EXPORT003, 'export_arn': self.expected_result['export_arn']}),
            call({'log_reference': LogReference.EXPORT004,
                  'job_status': self.response['ExportDescription']['ExportStatus']})
        ])
