import os
from datetime import date

from common.log import get_internal_id, log
from common.utils.lambda_wrappers import lambda_entry_point
from export_dynamodb_table.log_references import LogReference
from common.utils.dynamodb_access.dynamodb_boto3 import get_dynamodb_client


REPORTS_BUCKET = os.environ.get('REPORTS_BUCKET')
MASTER_KEY_ID = os.environ.get('MASTER_KEY_ID')
PARTICIPANT_TABLE_ARN = os.environ.get('DYNAMODB_PARTICIPANTS_ARN')


@lambda_entry_point
def lambda_handler(event, context):
    client = get_dynamodb_client()
    response = None
    if 'export_arn' in event:
        log({'log_reference': LogReference.EXPORT002, 'export_arn': event['export_arn']})
        response = client.describe_export(ExportArn=event['export_arn'])
    else:
        log({'log_reference': LogReference.EXPORT001})

        response = client.export_table_to_point_in_time(
            TableArn=PARTICIPANT_TABLE_ARN,
            S3Bucket=REPORTS_BUCKET,
            S3Prefix=f'CSMS/{date.today():%Y/%m/%d}/participant/',
            S3SseAlgorithm='KMS',
            S3SseKmsKeyId=MASTER_KEY_ID,
            ExportFormat='DYNAMODB_JSON'
        )
        log({'log_reference': LogReference.EXPORT003, 'export_arn': response['ExportDescription']['ExportArn']})

    log({'log_reference': LogReference.EXPORT004, 'job_status': response['ExportDescription']['ExportStatus']})
    return {
        'internal_id': get_internal_id(),
        'export_arn': response['ExportDescription']['ExportArn'],
        'job_status': response['ExportDescription']['ExportStatus'],
        'output_location': response['ExportDescription']['S3Prefix']
    }
