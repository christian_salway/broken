import os
import boto3
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from transfer_extract_to_dps.log_references import LogReference

AWS_ACCOUNT_ID = os.environ.get('AWS_ACCOUNT_ID')
SOURCE_BUCKET_NAME = os.environ.get('CTAS_OUTPUT_BUCKET')
DESTINATION_BUCKET_NAME = os.environ.get('DPS_BUCKET')
S3_RESOURCE = boto3.resource('s3')


@lambda_entry_point
def lambda_handler(event, context):
    file_prefix = event['file_prefix']
    log({
        'log_reference': LogReference.AFTD001,
        'source_bucket_name': SOURCE_BUCKET_NAME,
        'destination_bucket_name': DESTINATION_BUCKET_NAME,
        'file_prefix': file_prefix
    })

    required_values = {
        "source": SOURCE_BUCKET_NAME,
        "destination": DESTINATION_BUCKET_NAME,
        "path": file_prefix
    }

    for key, value in required_values.items():
        if not value:
            log({'log_reference': LogReference.AFTD002})
            raise ValueError(f"Missing required variable for {key}")

    csms_files_prefix = 'dps-pipeline-exports/'+file_prefix
    source_bucket = get_bucket(SOURCE_BUCKET_NAME)
    files_to_export = source_bucket.objects.filter(Prefix=csms_files_prefix)

    file_path_list = [f.key for f in files_to_export]
    copy_and_delete_files(file_path_list)

    log({'log_reference': LogReference.AFTD005})


def copy_and_delete_files(file_path_list):
    """
    Loop through the files found in the bucket, copy them to DPS bucket.

    Once we have copied the files then they can be deleted on our side.
    """
    dps_s3_client = get_s3_client_with_dps_access()
    extra_args = {
        'ACL': 'bucket-owner-full-control'
    }
    for file_path in file_path_list:
        source_file = {
            'Bucket': SOURCE_BUCKET_NAME,
            'Key': file_path
        }

        # Remove CSMS prefix when writing to DPS
        destination_path = file_path.replace('dps-pipeline-exports/', '')
        dps_s3_client.copy(
            source_file,
            DESTINATION_BUCKET_NAME,
            destination_path,
            ExtraArgs=extra_args
        )
        log({
            'log_reference': LogReference.AFTD003,
            'File exported:': file_path,
            'Destination path:': destination_path
        })

        S3_RESOURCE.Object(SOURCE_BUCKET_NAME, file_path).delete()
        log({'log_reference': LogReference.AFTD004, 'File deleted': file_path})


def get_bucket(bucket_name):
    """
    Return the bucket object from S3.
    """
    return S3_RESOURCE.Bucket(bucket_name)


def get_s3_client_with_dps_access():
    """
    Assume the "dps-role" role for permissions to interact with DPS bucket,
    returning the authenticated S3 client.
    """
    sts_connection = boto3.client('sts')
    assumed_role = sts_connection.assume_role(
        RoleArn=f"arn:aws:iam::{AWS_ACCOUNT_ID}:role/dps-role",
        RoleSessionName="dps_transfer_lambda",
        DurationSeconds=900
    )
    ACCESS_KEY = assumed_role['Credentials']['AccessKeyId']
    SECRET_KEY = assumed_role['Credentials']['SecretAccessKey']
    SESSION_TOKEN = assumed_role['Credentials']['SessionToken']

    # create service client using the assumed role credentials, e.g. S3
    return boto3.client(
        's3',
        aws_access_key_id=ACCESS_KEY,
        aws_secret_access_key=SECRET_KEY,
        aws_session_token=SESSION_TOKEN,
    )
