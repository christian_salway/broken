from unittest import TestCase
from mock import Mock, patch, call
from transfer_extract_to_dps.log_references import LogReference
from boto3.s3.inject import ClientError

module = 'transfer_extract_to_dps.transfer_extract_to_dps'
env_vars = {
    'CTAS_OUTPUT_BUCKET': 'screening-dps-stub',
    'DPS_BUCKET': 'nhsd-testbucket',
}


class TestAclCopyToDPS(TestCase):
    @patch('os.environ', env_vars)
    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    def setUp(self):
        self.log_patcher = patch(f'{module}.log').start()
        from transfer_extract_to_dps.transfer_extract_to_dps import lambda_handler
        self.lambda_handler = lambda_handler

        self.s3_data = [
            'dps-pipeline-exports/s3/csms/2021/09/28/defer/tables/01632829145164-0d198e91/20210928_145519_00109_vyqw5',
            'dps-pipeline-exports/s3/csms/2021/09/28/defer/tables/c528f64f-08ee-44f9-a801-925e1476e746-manifest.csv',
            'dps-pipeline-exports/s3/csms/2021/09/28/defer/tables/c528f64f-08ee-44f9-a801-925e1476e746.metadata',
        ]

        self.mock_s3_return = [
            self._create_mock_s3_object(item) for item in self.s3_data
        ]

    def tearDown(self):
        patch.stopall()

    def _create_mock_s3_object(self, key):
        s3_object = Mock()
        s3_object.key = key
        return s3_object

    @patch(f'{module}.get_bucket')
    @patch(f'{module}.get_s3_client_with_dps_access')
    def test_lambda_handler_with_valid_path_prefix_and_s3_permission(
        self,
        mock_assume_role,
        mock_get_bucket
    ):
        event = {'file_prefix': 's3/csms/2021/09/28/'}
        context = Mock()

        mock_assume_role.copy = Mock()
        source_bucket_mock = Mock()
        source_bucket_mock.objects.filter.return_value = self.mock_s3_return
        mock_get_bucket.return_value = source_bucket_mock

        self.lambda_handler.__wrapped__(event, context)

        expected_calls = [
            call
            (
                {
                    'log_reference': LogReference.AFTD001,
                    'source_bucket_name': env_vars['CTAS_OUTPUT_BUCKET'],
                    'destination_bucket_name': env_vars['DPS_BUCKET'],
                    'file_prefix': event['file_prefix']
                }
            )
        ]

        for item in self.mock_s3_return:
            expected_calls.append(call({
                'log_reference': LogReference.AFTD003,
                'File exported:': item.key,
                'Destination path:': item.key.replace('dps-pipeline-exports/', '')
            }))
            expected_calls.append(call({
                'log_reference': LogReference.AFTD004,
                'File deleted': item.key
            }))
        expected_calls.append(
            call
            (
                {
                    'log_reference': LogReference.AFTD005
                }
            ))

        self.log_patcher.assert_has_calls(expected_calls)
        self.assertEqual(self.log_patcher.call_count, 8)
        self.assertEqual(mock_assume_role.return_value.copy.call_count, 3)

        # Check if copy function called with right arguments
        extra_args = {
            'ACL': 'bucket-owner-full-control'
        }

        def _get_expected_copy_function_calls():
            number_of_records = len(self.s3_data)
            expected_copy_function_calls = []
            for expected_record in range(number_of_records):
                expected_calls.append(
                    call(
                        {
                            'Bucket': env_vars['CTAS_OUTPUT_BUCKET'],
                            'Key': self.s3_data[expected_record]
                        },
                        env_vars['DPS_BUCKET'],
                        self.s3_data[expected_record],
                        ExtraArgs=extra_args
                        )
                )
            return expected_copy_function_calls

        expected_copy_function_calls = _get_expected_copy_function_calls()
        mock_assume_role.return_value.copy.assert_has_calls(expected_copy_function_calls)

    def test_lambda_handler_with_invalid_event_key(self):
        event = {'file': 's3/csms/2021/09/28/'}
        context = Mock()

        with self.assertRaises(KeyError):
            self.lambda_handler.__wrapped__(event, context)

    def test_lambda_handler_with_invalid_event_value(self):
        event = {'file_prefix': ''}
        context = Mock()

        with self.assertRaises(ValueError):
            self.lambda_handler.__wrapped__(event, context)

            expected_calls = [
                call
                (
                    {
                        'log_reference': LogReference.AFTD001,
                        'source_bucket_name': env_vars['CTAS_OUTPUT_BUCKET'],
                        'destination_bucket_name': env_vars['DPS_BUCKET'],
                        'file_prefix': event['file_prefix']
                    }
                ),
                call
                (
                    {
                        'log_reference': LogReference.AFTD002
                    }
                )
            ]

            self.log_patcher.assert_has_calls(expected_calls)
            self.assertEqual(self.log_patcher.call_count, 2)

    @patch(f'{module}.get_bucket')
    @patch(f'{module}.get_s3_client_with_dps_access')
    def test_lambda_handler_with_s3_denied(
        self,
        mock_assume_role,
        mock_get_bucket
    ):
        event = {'file_prefix': 's3/csms/2021/09/28/'}
        context = Mock()

        source_bucket_mock = Mock()
        source_bucket_mock.objects.filter.return_value = self.mock_s3_return
        mock_get_bucket.return_value = source_bucket_mock

        mock_assume_role.side_effect = ClientError({'Error': {'Code': 'AccessDenied'}}, Mock())

        with self.assertRaises(ClientError):
            self.lambda_handler.__wrapped__(event, context)

            expected_calls = [
                call
                (
                    {
                        'log_reference': LogReference.AFTD001,
                        'source_bucket_name': env_vars['CTAS_OUTPUT_BUCKET'],
                        'destination_bucket_name': env_vars['DPS_BUCKET'],
                        'file_prefix': event['file_prefix']
                    }
                )
            ]

            self.log_patcher.assert_has_calls(expected_calls)
            self.assertEqual(self.log_patcher.call_count, 1)
