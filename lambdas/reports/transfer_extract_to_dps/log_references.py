import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    AFTD001 = (logging.INFO, 'Starting the transfer_extract_to_dps lambda')
    AFTD002 = (logging.ERROR, 'Invalid variable passed to the lambda')
    AFTD003 = (logging.INFO, 'File copied and given ownership to the bucket owner')
    AFTD004 = (logging.INFO, 'Source file deleted')
    AFTD005 = (logging.INFO, 'Finished successfully running the lambda')
