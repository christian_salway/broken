import os
from datetime import datetime

from boto3.dynamodb.conditions import Key

from common.log import log
from common.utils.s3_utils import put_object
from trigger_lab_files.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.dynamodb_access.operations import dynamodb_paginated_query


CSV_FIELD_DELIMITER = ','
CSV_RECORD_DELIMITER = '\n'
ATHENA_RESULT_OUTPUT_BUCKET = os.environ.get('ATHENA_RESULT_OUTPUT_BUCKET')

VALID_REJECTIONS_DUE_TO_ERRORS = ['MISSING DATA', 'INVALID DATA', 'DUPLICATE DATA', 'MULTIPLE ERRORS']
VALID_REJECTIONS_DUE_TO_POLICY = [
    '3RD NEGATIVE COLP RESULT', '2ND INADEQUATE COLP RESULT', 'INVALID RETURN TO ROUTINE RECALL', 'CEASED',
    'INACTIVE', 'REGISTERED AS MALE', 'DUPLICATE DATA', 'MULTIPLE ERRORS'
]
PROJECTION_EXPRESSION = ', '.join([
    'result_id', 'matched_to_participant', 'matched_time', 'workflow_state', 'is_automatic_match',
    'secondary_status', 'possible_match_participants', 'rejected_reason'
])


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.LFR0001})

    parameters = get_parameters_from_event(event)
    if parameters is None:
        return
    source_file, lab, date_and_time = parameters

    results = get_results_of_the_source_file(source_file)
    automatic_matches = get_automatic_matches(results)
    partial_hits_to_process, non_hits_to_process = get_partial_hits_and_non_hits_to_process(results)
    rejections_due_to_errors, rejections_due_to_policy = get_automatic_rejections_due_to_errors_and_policy(results)

    csv_content = generate_csv(
        results, lab, date_and_time, source_file, automatic_matches,
        partial_hits_to_process, non_hits_to_process, rejections_due_to_errors, rejections_due_to_policy
    )
    output_file_path = f'reports/lab-files/{lab}/{date_and_time:%Y/%m/%d}/summary_for_{source_file}.csv'
    log({'log_reference': LogReference.LFR0005, 'file_path': output_file_path, 'bucket': ATHENA_RESULT_OUTPUT_BUCKET})
    put_object(ATHENA_RESULT_OUTPUT_BUCKET, csv_content, output_file_path)


def get_parameters_from_event(event):
    source_file = event['pathParameters'].get('lab_file_name')
    if source_file is None:
        log({'log_reference': LogReference.LFR0006})
        return

    query_parameters = event.get('queryStringParameters', dict())
    lab = query_parameters.get('lab')
    if lab is None:
        log({'log_reference': LogReference.LFR0007})
        return

    date_and_time = query_parameters.get('date_and_time')
    if date_and_time is None:
        log({'log_reference': LogReference.LFR0008})
        return
    try:
        parsed_date_and_time = datetime.fromisoformat(date_and_time)
    except ValueError:
        log({'log_reference': LogReference.LFR0009, 'date_and_time': date_and_time})
        return

    return source_file, lab, parsed_date_and_time


def get_results_of_the_source_file(source_file: str) -> list:
    query_args = dict(
        KeyConditionExpression=Key('source_file').eq(source_file),
        ProjectionExpression=PROJECTION_EXPRESSION,
        IndexName='source-file'
    )
    log({'log_reference': LogReference.LFR0002,
         'parameters': {**query_args, 'KeyConditionExpression': f"Key('source_file').eq('{source_file}')"}})
    results = dynamodb_paginated_query(TableNames.RESULTS, query_args)
    if (count_results := len(results)) > 0:
        log({'log_reference': LogReference.LFR0003, 'count': count_results})
    else:
        log({'log_reference': LogReference.LFR0004})
    return results


def get_automatic_matches(results: list) -> int:
    return len([
        result for result in results
        if result.get('matched_to_participant', None) is not None
        and result.get('matched_time', None) is not None
        and result.get('is_automatic_match', False)
    ])


def get_partial_hits_and_non_hits_to_process(results: list):
    hits = [
        result for result in results
        if result.get('workflow_state') == 'process'
        and result.get('secondary_status') == 'NOT STARTED'
        and 'matched_time' not in result
        and 'matched_to_participant' not in result
    ]
    partial_hits = len([result for result in hits if len(result.get('possible_match_participants', [])) == 0])
    non_hits = len(hits) - partial_hits
    return partial_hits, non_hits


def get_automatic_rejections_due_to_errors_and_policy(results: list):
    rejections = [
        result for result in results
        if result.get('workflow_state') == 'rejected'
        and 'matched_time' not in result
        and 'matched_to_participant' not in result
    ]
    rejection_errors = len([
        result for result in rejections
        if result.get('rejected_reason') in VALID_REJECTIONS_DUE_TO_ERRORS
    ])
    rejection_policy = len([
        result for result in rejections
        if result.get('rejected_reason') in VALID_REJECTIONS_DUE_TO_POLICY
    ])
    return rejection_errors, rejection_policy


def generate_csv(
        results: list,
        lab: str,
        date_and_time: datetime,
        source_file: str,
        automatic_matches: int,
        partial_hits_to_process: int,
        non_hits_to_process: int,
        rejections_due_to_errors: int,
        rejections_due_to_policy: int
        ):
    sum_of_totals = automatic_matches + partial_hits_to_process + non_hits_to_process
    sum_of_totals += rejections_due_to_errors + rejections_due_to_policy
    if sum_of_totals == len(results):
        qa_check = 'CORRECT'
    else:
        qa_check = 'ERROR'

    csv_header = CSV_FIELD_DELIMITER.join([
        'Lab', 'Date & Time', 'File Name', 'Automatic Matches', 'Partial hits to Process',
        'Non-Hits to Process', 'Automatic Rejection (Errors)', 'Automatic Rejection (Policy)', 'Automatic QA Check'
    ])
    csv_values = CSV_FIELD_DELIMITER.join([
        lab, f'{date_and_time:%Y-%m-%d %H:%M:%S}', source_file, str(automatic_matches), str(partial_hits_to_process),
        str(non_hits_to_process), str(rejections_due_to_errors), str(rejections_due_to_policy), qa_check
    ])
    return CSV_RECORD_DELIMITER.join([csv_header, csv_values])
