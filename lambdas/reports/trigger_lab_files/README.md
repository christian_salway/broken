# Lambda to generate the CSV summary of Lab Files # 

### Trigger not defined yet

### How it works

Given a source file, a lab and a date and time (to be confirmed), we query DynamoDB by source_file and generate a summary according to the [Lab Files Reports SDD](https://nhsd-confluence.digital.nhs.uk/display/CSP/Lab+Files+Reports+SDD)

**THIS LAMBDA IS NOT COMPLETED AS WE NEED TO DISSCUSS MORE DETAILS ABOUT IT**

The result is stored on the Athena Bucket under the path "lab-files/pre-mm/lab/year/month/date/name_of_file.csv"
