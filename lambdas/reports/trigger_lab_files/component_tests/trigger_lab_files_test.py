from datetime import datetime
import json
import os

from boto3.dynamodb.conditions import Key
from unittest import TestCase
from mock import Mock, patch, call

from trigger_lab_files.log_references import LogReference
from common.utils.dynamodb_access.table_names import TableNames


module = 'trigger_lab_files.trigger_lab_files'
FAKE_TEST_FILE = 'test_file.dat'
FAKE_LAB = 'Test Lab'
FAKE_DATE_TIME = datetime(year=2021, month=6, day=9, hour=4, minute=50, second=21)

env_vars = {
    'ENVIRONMENT_NAME': 'my-env',
    'ATHENA_RESULT_OUTPUT_BUCKET': 'my-bucket'
}


class TestTriggerLabFiles(TestCase):

    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    @patch('os.environ', env_vars)
    def setUp(self):
        self.dynamodb_paginated_query = patch(f'{module}.dynamodb_paginated_query').start()
        self.log_patcher = patch(f'{module}.log').start()
        self.put_object_patcher = patch(f'{module}.put_object').start()

        from trigger_lab_files.trigger_lab_files import lambda_handler
        self.PROJECTION_EXPRESSION = 'result_id, matched_to_participant, matched_time, workflow_state, ' \
            'is_automatic_match, secondary_status, possible_match_participants, rejected_reason'
        self.lambda_handler = lambda_handler
        self.event = {
            'pathParameters': {'lab_file_name': FAKE_TEST_FILE},
            'queryStringParameters': {'lab': FAKE_LAB, 'date_and_time': FAKE_DATE_TIME.strftime('%Y-%m-%d %H:%M:%S')}
        }

    def test_lambda_handler(self):
        test_files_path = os.path.join(os.path.dirname(__file__), 'files')

        with open(os.path.join(test_files_path, 'results_query_response.json')) as f:
            self.dynamodb_paginated_query.return_value = json.load(f)['Items']

        self.lambda_handler.__wrapped__(self.event, Mock())
        query_parameters = dict(
            KeyConditionExpression=Key('source_file').eq(FAKE_TEST_FILE),
            ProjectionExpression=self.PROJECTION_EXPRESSION,
            IndexName='source-file'
        )
        self.dynamodb_paginated_query.assert_called_once_with(TableNames.RESULTS, query_parameters)
        result = f'Lab,Date & Time,File Name,Automatic Matches,Partial hits to Process,Non-Hits to Process,' \
                 f'Automatic Rejection (Errors),Automatic Rejection (Policy),Automatic QA Check' \
                 f'\n{FAKE_LAB},{FAKE_DATE_TIME:%Y-%m-%d %H:%M:%S},{FAKE_TEST_FILE},1,1,1,1,1,CORRECT'
        output_file = f'reports/lab-files/{FAKE_LAB}/{FAKE_DATE_TIME:%Y/%m/%d}/summary_for_{FAKE_TEST_FILE}.csv'
        self.put_object_patcher.assert_called_once_with(env_vars['ATHENA_RESULT_OUTPUT_BUCKET'], result, output_file)

        query_args_serializable = query_parameters.copy()
        query_args_serializable['KeyConditionExpression'] = f"Key('source_file').eq('{FAKE_TEST_FILE}')"

        expected_log_calls = [
            call({'log_reference': LogReference.LFR0001}),
            call({'log_reference': LogReference.LFR0002, 'parameters': query_args_serializable}),
            call({'log_reference': LogReference.LFR0003, 'count': 5}),
            call({'log_reference': LogReference.LFR0005, 'file_path': output_file,
                  'bucket': env_vars['ATHENA_RESULT_OUTPUT_BUCKET']})
            ]
        self.log_patcher.assert_has_calls(expected_log_calls)

    def test_lambda_handler_without_lab_file_name_parameter(self):
        self.event['pathParameters'].pop('lab_file_name')
        event = {
            'pathParameters': {},
            'queryStringParameters': {'lab': FAKE_LAB, 'date_and_time': FAKE_DATE_TIME.strftime('%Y-%m-%d %H:%M:%S')}
        }

        self.lambda_handler.__wrapped__(event, Mock())
        expected_log_calls = [
            call({'log_reference': LogReference.LFR0001}),
            call({'log_reference': LogReference.LFR0006})
        ]
        self.log_patcher.assert_has_calls(expected_log_calls)

    def test_lambda_handler_without_lab_parameter(self):
        self.event['queryStringParameters'].pop('lab')

        self.lambda_handler.__wrapped__(self.event, Mock())
        expected_log_calls = [
            call({'log_reference': LogReference.LFR0001}),
            call({'log_reference': LogReference.LFR0007})
        ]
        self.log_patcher.assert_has_calls(expected_log_calls)

    def test_lambda_handler_without_date_and_time_parameter(self):
        self.event['queryStringParameters'].pop('date_and_time')

        self.lambda_handler.__wrapped__(self.event, Mock())
        expected_log_calls = [
            call({'log_reference': LogReference.LFR0001}),
            call({'log_reference': LogReference.LFR0008})
        ]
        self.log_patcher.assert_has_calls(expected_log_calls)

    def test_lambda_handler_with_invalid_date_and_time_parameter(self):
        self.event['queryStringParameters']['date_and_time'] = 'invalid_date'

        self.lambda_handler.__wrapped__(self.event, Mock())
        expected_log_calls = [
            call({'log_reference': LogReference.LFR0001}),
            call({'log_reference': LogReference.LFR0009, 'date_and_time': 'invalid_date'})
        ]
        self.log_patcher.assert_has_calls(expected_log_calls)

    def test_lambda_handler_with_zero_results(self):
        self.dynamodb_paginated_query.return_value = []

        self.lambda_handler.__wrapped__(self.event, Mock())
        query_parameters = dict(
            KeyConditionExpression=Key('source_file').eq(FAKE_TEST_FILE),
            ProjectionExpression=self.PROJECTION_EXPRESSION,
            IndexName='source-file'
        )
        self.dynamodb_paginated_query.assert_called_once_with(TableNames.RESULTS, query_parameters)
        result = f'Lab,Date & Time,File Name,Automatic Matches,Partial hits to Process,Non-Hits to Process,' \
                 f'Automatic Rejection (Errors),Automatic Rejection (Policy),Automatic QA Check' \
                 f'\n{FAKE_LAB},{FAKE_DATE_TIME:%Y-%m-%d %H:%M:%S},{FAKE_TEST_FILE},0,0,0,0,0,CORRECT'
        output_file = f'reports/lab-files/{FAKE_LAB}/{FAKE_DATE_TIME:%Y/%m/%d}/summary_for_{FAKE_TEST_FILE}.csv'
        self.put_object_patcher.assert_called_once_with(env_vars['ATHENA_RESULT_OUTPUT_BUCKET'], result, output_file)

        query_args_serializable = query_parameters.copy()
        query_args_serializable['KeyConditionExpression'] = f"Key('source_file').eq('{FAKE_TEST_FILE}')"

        expected_log_calls = [
            call({'log_reference': LogReference.LFR0001}),
            call({'log_reference': LogReference.LFR0002, 'parameters': query_args_serializable}),
            call({'log_reference': LogReference.LFR0004}),
            call({'log_reference': LogReference.LFR0005, 'file_path': output_file,
                  'bucket': env_vars['ATHENA_RESULT_OUTPUT_BUCKET']})
            ]
        self.log_patcher.assert_has_calls(expected_log_calls)
