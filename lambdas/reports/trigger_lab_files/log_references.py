import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    LFR0001 = (logging.INFO, 'Received API call for lab files report')
    LFR0002 = (logging.INFO, 'Querying results table with the following parameters')
    LFR0003 = (logging.INFO, 'Number of results of the query')
    LFR0005 = (logging.INFO, 'Uploading CSV file to S3 bucket')

    LFR0004 = (logging.WARNING, 'The query did not return any result')

    LFR0006 = (logging.ERROR, 'lab_file_name parameter no specified')
    LFR0007 = (logging.ERROR, 'lab parameter no specified')
    LFR0008 = (logging.ERROR, 'date_and_time parameter no specified')
    LFR0009 = (logging.ERROR, 'date_and_time parameter has an invalid format')
