from datetime import datetime

from unittest import TestCase


FAKE_TEST_FILE = 'test_file.dat'
FAKE_LAB = 'Test Lab'
FAKE_DATE_TIME = datetime(year=2021, month=6, day=9, hour=4, minute=50, second=21)
TEST_RESULTS = [
        {
            "result_id": "AUTOMATIC_MATCH",
            "matched_time": "2019-01-31T17:43:29+00:00",
            "matched_to_participant": "51c9261b-5567-4b25-b516-e1b1ff05a9e6",
            "is_automatic_match": True,
            "possible_match_participants": [
                {
                    "participant_id": "37f4524d-e59f-41d3-9071-97eec87955c10"
                }
            ]
        },
        {
            "result_id": "PARTIAL_HIT_TO_PROCESS",
            "possible_match_participants": [
                {
                    "participant_id": "37f4524d-e59f-41d3-9071-97eec87955c10"
                }
            ],
            "secondary_status": "NOT STARTED",
            "workflow_state": "process"
        },
        {
            "result_id": "NON_HIT_TO_PROCESS",
            "possible_match_participants": [],
            "secondary_status": "NOT STARTED",
            "workflow_state": "process"
        },
        {
            "result_id": "AUTOMATIC_REJECTION_DUE_TO_ERROR",
            "possible_match_participants": [
                {
                    "participant_id": "37f4524d-e59f-41d3-9071-97eec87955c10"
                }
            ],
            "workflow_state": "rejected",
            "rejected_reason": "MISSING DATA"
        },
        {
            "result_id": "AUTOMATIC_REJECTION_DUE_TO_POLICY",
            "possible_match_participants": [
                {
                    "participant_id": "37f4524d-e59f-41d3-9071-97eec87955c10"
                }
            ],
            "workflow_state": "rejected",
            "rejected_reason": "INACTIVE"
        }
    ]


class TestTrigerLabFiles(TestCase):

    def setUp(self):
        import trigger_lab_files.trigger_lab_files as _trigger_lab_files
        self.trigger_lab_files_module = _trigger_lab_files

    def test_get_parameters_from_event(self):
        event = {
            'pathParameters': {'lab_file_name': FAKE_TEST_FILE},
            'queryStringParameters': {'lab': FAKE_LAB, 'date_and_time': FAKE_DATE_TIME.strftime('%Y-%m-%d %H:%M:%S')}
        }
        file, lab, date = self.trigger_lab_files_module.get_parameters_from_event(event)
        self.assertEqual(file, FAKE_TEST_FILE)
        self.assertEqual(lab, FAKE_LAB)
        self.assertEqual(date, FAKE_DATE_TIME)

    def test_get_automatic_matches(self):
        total = self.trigger_lab_files_module.get_automatic_matches(TEST_RESULTS)
        self.assertEqual(total, 1)

    def test_get_partial_hits_and_non_hits_to_process(self):
        partial_hits, non_hits = self.trigger_lab_files_module.get_partial_hits_and_non_hits_to_process(TEST_RESULTS)
        self.assertEqual(partial_hits, 1)
        self.assertEqual(non_hits, 1)

    def test_get_automatic_rejections_due_to_errors_and_policy(self):
        errors, policy = self.trigger_lab_files_module.get_automatic_rejections_due_to_errors_and_policy(TEST_RESULTS)
        self.assertEqual(errors, 1)
        self.assertEqual(policy, 1)

    def test_generate_csv(self):
        expected = f'Lab,Date & Time,File Name,Automatic Matches,Partial hits to Process,Non-Hits to Process,' \
                   f'Automatic Rejection (Errors),Automatic Rejection (Policy),Automatic QA Check' \
                   f'\n{FAKE_LAB},{FAKE_DATE_TIME:%Y-%m-%d %H:%M:%S},{FAKE_TEST_FILE},1,1,1,1,1,CORRECT'
        csv_content = self.trigger_lab_files_module.generate_csv(
            TEST_RESULTS, FAKE_LAB, FAKE_DATE_TIME, FAKE_TEST_FILE, 1, 1, 1, 1, 1
        )
        self.assertEqual(csv_content, expected)
