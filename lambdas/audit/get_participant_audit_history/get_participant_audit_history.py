import re

from boto3.dynamodb.conditions import Key
from common.utils.dynamodb_access.segregated_operations import segregated_query
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import (
    json_return_data,
    json_return_message
)
from common.utils.participant_utils import get_nhs_number_by_participant_id
from get_participant_audit_history.log_references import LogReference
from common.log import log
from common.utils.session_utils import get_session_from_lambda_event
from common.utils.audit_utils import audit, AuditActions
from common.utils.dynamodb_access.table_names import TableNames

AUDIT_LOG_READ_ATTRIBUTES = [
    'first_name', 'last_name', 'role_id', 'user_organisation_code', 'additional_information', 'action', 'timestamp'
]


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    log({'log_reference': LogReference.GETAUDIT1})

    path_parameters = event.get('pathParameters')
    participant_id = path_parameters.get('participant_id') if path_parameters else None

    if not participant_id:
        log({'log_reference': LogReference.GETAUDIT2})
        return json_return_message(400, 'Required parameter participant_id not supplied')

    log({'log_reference': LogReference.GETAUDIT4})
    nhs_number = get_nhs_number_by_participant_id(participant_id, segregate=True)
    if nhs_number == 'participant not found':
        log({'log_reference': LogReference.GETAUDIT6})
        return json_return_message(404, 'Participant not found.')

    log({'log_reference': LogReference.GETAUDIT3, 'participant_id': participant_id})
    audit_history = segregated_query(TableNames.AUDIT, {
        'IndexName': 'participant-id-timestamp',
        'KeyConditionExpression': Key('participant_id').eq(participant_id)
    })

    response = prepare_response(audit_history)

    session = get_session_from_lambda_event(event)
    audit(session=session,
          action=AuditActions.VIEW_PARTICIPANT_AUDIT_HISTORY,
          participant_ids=[participant_id],
          nhs_numbers=[nhs_number])

    log({'log_reference': LogReference.GETAUDIT5})
    return json_return_data(200, response)


def prepare_response(audit_history):
    system_events = 'SYSTEM|MIGRATION|LOAD_DEMOGRAPHICS|MATCH_RESULT|NHS_IDENTITY'
    response = []

    for audit_entry in audit_history:
        entry = {}

        is_system_event = bool(re.match(system_events, audit_entry.get('nhsid_useruid', '')))
        if is_system_event:
            entry['nhsid_useruid'] = 'SYSTEM'
        elif audit_entry.get('nhsid_useruid'):
            entry['nhsid_useruid'] = audit_entry['nhsid_useruid']

        readable_attributes = {
            k: v for k, v in audit_entry.items() if k in AUDIT_LOG_READ_ATTRIBUTES
        }

        entry.update(readable_attributes)

        response.append(entry)

    return response
