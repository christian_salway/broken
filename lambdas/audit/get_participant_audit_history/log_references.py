import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    GETAUDIT1 = (logging.INFO,  'Received resquest to get audit history')
    GETAUDIT2 = (logging.ERROR, 'Missing required parameter participant_id')
    GETAUDIT3 = (logging.INFO,  'Retrieving audit history')
    GETAUDIT4 = (logging.INFO,  'Retrieving NHS number')
    GETAUDIT5 = (logging.INFO,  'Returning audit history')
    GETAUDIT6 = (logging.WARNING, 'Participant not found')
