from datetime import datetime, timezone
from unittest import TestCase
from unittest.mock import patch, Mock
from boto3.dynamodb.conditions import Key
from common.utils.data_segregation.nhais_ciphers import Cohorts

from common.utils.dynamodb_access.table_names import TableNames
from get_participant_audit_history import get_participant_audit_history


env_vars = {
    'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
    'DYNAMODB_AUDIT': 'DYNAMODB_AUDIT',
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL'
}

example_date = datetime(2020, 12, 23, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

audit_table_mock = Mock()
participant_table_mock = Mock()
dynamodb_resource_mock = Mock()

SUCCESS_RESPONSE = {
    'body': '{"data": [{"nhsid_useruid": "555249831105", "action": '
            '"SEARCH_PARTICIPANTS", "role_id": "R8010", "timestamp": '
            '"[TIMESTAMP]"}]}',
    'headers': {
        'Content-Type': 'application/json',
        'Strict-Transport-Security': 'max-age=1576800',
        'X-Content-Type-Options': 'nosniff'
    },
    'isBase64Encoded': False,
    'statusCode': 200
}

NOT_FOUND_RESPONSE = {
    'body': '{"message": "Participant not found."}',
    'headers': {
        'Content-Type': 'application/json',
        'Strict-Transport-Security': 'max-age=1576800',
        'X-Content-Type-Options': 'nosniff'
    },
    'isBase64Encoded': False,
    'statusCode': 404
}


module = 'get_participant_audit_history.get_participant_audit_history'


def get_table_side_effect(table_enum):
    tables = {
        TableNames.PARTICIPANTS: participant_table_mock,
        TableNames.AUDIT: audit_table_mock,
    }
    return tables[table_enum]


@patch('common.utils.audit_utils.datetime', datetime_mock)
@patch('os.environ', env_vars)
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table', Mock(side_effect=get_table_side_effect))
@patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_resource',
       Mock(return_value=dynamodb_resource_mock))
@patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
       Mock(return_value=[Cohorts.ENGLISH]))
class TestGetAuditHistory(TestCase):

    participant_records = [
        {
            'participant_id': '567',
            'sort_key': 'PARTICIPANT#2020-01-01',
            'nhs_number': 'nhs_number',
        }
    ]

    audit_events = [{
        "action": "SEARCH_PARTICIPANTS",
        "nhsid_useruid": "555249831105",
        "participant_id": "37f4524d-e59f-41d3-9071-97eec87955c8",
        "nhs_number": "nhs_number",
        "role_id": "R8010",
        "session_id": "9999999",
        "sort_key": "[TIMESTAMP]#37f4524d-e59f-41d3-9071-97eec87955c8",
        "timestamp": "[TIMESTAMP]"
    }]

    @classmethod
    @patch('datetime.datetime', datetime_mock)
    def setUpClass(cls, *args):
        cls.log_patcher = patch('common.log.log')

        sqs_client_mock = patch('common.utils.audit_utils._SQS_CLIENT').start()
        cls.sqs_client_mock = sqs_client_mock

    def setUp(self):
        self.sqs_client_mock.reset_mock()
        audit_table_mock.reset_mock()
        dynamodb_resource_mock.reset_mock()
        # Setup database mocks for audit table
        audit_table_mock.query.return_value = {'Items': self.audit_events}
        # Setup database mocks for participant table
        participant_table_mock.get_item.return_value = {'Item': self.participant_records[0]}

        self.event = _build_event('participant_id', 'user_id', 'session_id')
        self.context = Mock()
        self.context.function_name = ''
        self.log_patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.log_patcher.stop()
        cls.sqs_client_mock.stop()

    def test_get_participant_audit_history_makes_correct_calls(self):
        # Arrange
        dynamodb_resource_mock.batch_get_item.return_value = {
            'Responses': {
                'DYNAMODB_PARTICIPANTS': [
                    {'participant_id': '567', 'nhais_cipher': 'ENG', 'nhs_number': 'nhs_number'}
                ]
            }
        }

        # Act
        response = get_participant_audit_history.lambda_handler(self.event, self.context)

        # Assert that dynamodb is called with the correct args
        audit_table_mock.query.assert_called_with(
            IndexName='participant-id-timestamp',
            KeyConditionExpression=Key('participant_id').eq('participant_id')
        )
        dynamodb_resource_mock.batch_get_item.assert_called_once_with(RequestItems={
            'DYNAMODB_PARTICIPANTS': {
                'Keys': [
                    {'participant_id': '567', 'sort_key': 'PARTICIPANT'}
                ],
                'ProjectionExpression': 'participant_id, nhais_cipher'
            }
        })
        # Asserts the audit record has been sent to the audit queue
        self.sqs_client_mock.send_message.assert_called_with(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody='{"action": "VIEW_PARTICIPANT_AUDIT_HISTORY", "timestamp": "2020-12-23T13:48:08+00:00", "internal_id": "request_id", "nhsid_useruid": "user_id", "session_id": "session_id", "first_name": "John", "last_name": "Doe", "participant_ids": ["participant_id"], "nhs_numbers": ["nhs_number"]}'  
        )
        # Assert the response is as expected
        self.assertEqual(response, SUCCESS_RESPONSE)

    def test_cannot_get_participant_audit_history_across_cohorts(self):
        # Arrange
        dynamodb_resource_mock.batch_get_item.return_value = {
            'Responses': {
                'DYNAMODB_PARTICIPANTS': [
                    {'participant_id': '567', 'nhais_cipher': 'IM', 'nhs_number': 'nhs_number'}
                ]
            }
        }

        # Act
        response = get_participant_audit_history.lambda_handler(self.event, self.context)

        # Assert that dynamodb is called with the correct args
        audit_table_mock.query.assert_not_called()
        dynamodb_resource_mock.batch_get_item.assert_called_once_with(RequestItems={
            'DYNAMODB_PARTICIPANTS': {
                'Keys': [
                    {'participant_id': '567', 'sort_key': 'PARTICIPANT'}
                ],
                'ProjectionExpression': 'participant_id, nhais_cipher'
            }
        })
        # Asserts no audit record has been sent to the audit queue
        self.sqs_client_mock.send_message.assert_not_called()
        # Assert the response is as expected
        self.assertEqual(response, NOT_FOUND_RESPONSE)


def _build_event(participant_id, user_uid, session_id):
    return {
        'pathParameters': {
            'participant_id': participant_id
        },
        'headers': {
            'request_id': 'request_id',
        },
        'requestContext': {
            'authorizer': {
                'principalId': 'blah',
                'session': f"{{\"user_data\": {{\"nhsid_useruid\": \"{user_uid}\", \"first_name\": \"John\", \"last_name\": \"Doe\"}}, \"session_id\": \"{session_id}\"}}"  
            }
        }
    }
