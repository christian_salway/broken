from unittest import TestCase
from unittest.mock import patch, Mock, call

from boto3.dynamodb.conditions import Key

from common.test_assertions.api_assertions import assertApiResponse
from get_participant_audit_history.log_references import LogReference
from common.utils.dynamodb_access.table_names import TableNames
from common.utils import audit_utils
from get_participant_audit_history import get_participant_audit_history

VALID_EVENT = {
    'pathParameters': {'participant_id': '565'},
    'requestContext': {'authorizer': {'session': '{"session_id": "12345678"}', 'principalId': 'pid'}},
    'headers': {'request_id': 'reid'}
}

INVALID_EVENT = {
    'pathParameters': {'participant_id': None},
    'requestContext': {'authorizer': {'session': '{"session_id": "12345678"}', 'principalId': 'pid'}},
    'headers': {'request_id': 'reid'}
}

AUDIT_HISTORY = [{
    "action": "DEFER_PARTICIPANT",
    "nhsid_useruid": "555249831105",
    "timestamp": "[TIMESTAMP]",
    "session_id": "9999999",
    "participant_id": "565",
    "nhs_number": "999",
    "internal_id": "20201229174833097156_8794B7",
    "corresponding_sort_key": "DEFER#2020-01-01",
    "first_name": "John",
    "last_name": "Doe",
    "additional_information": {
        "reason": "Cease reason from lambda",
        "crm_number": "123456789",
        "comments": "I was asked to cease this participant"
    }
}]

AUDIT_HISTORY_WITHOUT_OPTIONAL_FIELDS = [{
    "action": "SEARCH_PARTICIPANTS",
    "nhsid_useruid": "123",
    "timestamp": "[TIMESTAMP]",
    "session_id": "9999999",
    "participant_id": "565",
    "internal_id": "20201229174833097156_8794B7",
}]

EXPECTED_LOG_CALLS_SUCCESS = [
    call({'log_reference': LogReference.GETAUDIT1}),
    call({'log_reference': LogReference.GETAUDIT4}),
    call({'log_reference': LogReference.GETAUDIT3, 'participant_id': '565'}),
    call({'log_reference': LogReference.GETAUDIT5})
]

EXPECTED_LOG_CALLS_ERROR = [
    call({'log_reference': LogReference.GETAUDIT1}),
    call({'log_reference': LogReference.GETAUDIT2})
]

EXPECTED_LOG_CALLS_NOT_FOUND = [
    call({'log_reference': LogReference.GETAUDIT1}),
    call({'log_reference': LogReference.GETAUDIT4}),
    call({'log_reference': LogReference.GETAUDIT6}),
]

module = 'get_participant_audit_history.get_participant_audit_history'


class TestGetAuditHistory(TestCase):

    def setUp(self):
        self.context = Mock()
        self.context.function_name = ''

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.get_session_from_lambda_event')
    @patch(f'{module}.prepare_response')
    @patch(f'{module}.segregated_query')
    @patch(f'{module}.get_nhs_number_by_participant_id')
    def test_lambda_handler_returns_correct_response(
            self,
            get_nhs_number_by_participant_id_mock,
            get_audit_history_mock,
            prepare_response_mock,
            get_session_from_lambda_event_mock,
            audit_mock,
            log_mock
    ):
        # Arrange
        expected_log_calls = EXPECTED_LOG_CALLS_SUCCESS
        actual_prepare_response = [{
            'action': 'DEFER_PARTICIPANT',
            'timestamp': '[TIMESTAMP]',
            'nhsid_useruid': '123',
            'reason': 'Result received',
            'crm_number': 'CAS-12345-ABCDE',
            "first_name": "John",
            "last_name": "Doe"
        }]
        expected_prepare_response = [{
            'action': 'DEFER_PARTICIPANT',
            'timestamp': '[TIMESTAMP]',
            'nhsid_useruid': '123',
            'reason': 'Result received',
            'crm_number': 'CAS-12345-ABCDE',
            "first_name": "John",
            "last_name": "Doe"
        }]
        self.event = VALID_EVENT
        get_nhs_number_by_participant_id_mock.return_value = "999"
        get_audit_history_mock.return_value = AUDIT_HISTORY
        prepare_response_mock.return_value = actual_prepare_response
        # Act
        result = get_participant_audit_history.lambda_handler(self.event, self.context)
        # Assert
        get_nhs_number_by_participant_id_mock.assert_called_with('565', segregate=True)
        get_audit_history_mock.assert_called()
        prepare_response_mock.assert_called_with(AUDIT_HISTORY)
        get_session_from_lambda_event_mock.assert_called()
        audit_mock.assert_called()
        log_mock.assert_has_calls(expected_log_calls)
        assertApiResponse(self, 200, {'data': expected_prepare_response}, result)

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.get_session_from_lambda_event')
    @patch(f'{module}.prepare_response')
    @patch(f'{module}.segregated_query')
    @patch(f'{module}.get_nhs_number_by_participant_id')
    def test_lambda_handler_returns_200_if_given_participant_id(
            self,
            get_nhs_number_by_participant_id_mock,
            get_audit_history_mock,
            prepare_response_mock,
            get_session_from_lambda_event_mock,
            audit_mock,
            log_mock
    ):
        # Arrange
        expected_log_calls = EXPECTED_LOG_CALLS_SUCCESS
        self.event = VALID_EVENT
        get_nhs_number_by_participant_id_mock.return_value = "999"
        get_audit_history_mock.return_value = [{'action': 'TEST'}]
        prepare_response_mock.return_value = []
        get_session_from_lambda_event_mock.return_value = {'session_id': 'the_session'}
        # Act
        result = get_participant_audit_history.lambda_handler(self.event, self.context)
        # Assert
        get_nhs_number_by_participant_id_mock.assert_called_with('565', segregate=True)
        get_audit_history_mock.assert_called_with(TableNames.AUDIT, {
            'IndexName': 'participant-id-timestamp', 'KeyConditionExpression': Key('participant_id').eq('565')
        })
        prepare_response_mock.assert_called_with([{'action': 'TEST'}])
        get_session_from_lambda_event_mock.assert_called()
        audit_mock.assert_called_with(session={'session_id': 'the_session'},
                                      action=audit_utils.AuditActions.VIEW_PARTICIPANT_AUDIT_HISTORY,
                                      participant_ids=['565'],
                                      nhs_numbers=['999'])
        log_mock.assert_has_calls(expected_log_calls)
        assertApiResponse(self, 200, {'data': []}, result)

    @patch(f'{module}.log')
    def test_lambda_handler_returns_400_if_missing_participant_id(
            self,
            log_mock
    ):
        # Arrange
        expected_log_calls = EXPECTED_LOG_CALLS_ERROR
        self.event = INVALID_EVENT
        # Act
        result = get_participant_audit_history.lambda_handler(self.event, self.context)
        # Assert
        log_mock.assert_has_calls(expected_log_calls)
        assertApiResponse(self, 400, {"message": "Required parameter participant_id not supplied"}, result)

    def test_prepare_response_returns_empty_response_if_input_is_empty(self):
        # Arrange
        expected_response = []
        # Act
        actual_response = get_participant_audit_history.prepare_response([])
        # Assert
        self.assertEqual(expected_response, actual_response)

    def test_prepare_response_returns_correct_data_if_corresponding_sort_key_not_provided(self):
        # Arrange
        expected_response = [{
            'action': 'SEARCH_PARTICIPANTS',
            'timestamp': '[TIMESTAMP]',
            'nhsid_useruid': '123',
        }]
        # Act
        actual_response = get_participant_audit_history.prepare_response(
            AUDIT_HISTORY_WITHOUT_OPTIONAL_FIELDS
        )
        # Assert
        self.assertEqual(expected_response, actual_response)

    def test_prepare_response_returns_correct_data_if_additional_fields_are_present(self):
        # Arrange
        expected_response = [{
            'action': 'DEFER_PARTICIPANT',
            'timestamp': '[TIMESTAMP]',
            'nhsid_useruid': '555249831105',
            "first_name": "John",
            "last_name": "Doe",
            "additional_information": {
                "reason": "Cease reason from lambda",
                "crm_number": "123456789",
                "comments": "I was asked to cease this participant"
            }
        }]
        # Act
        actual_response = get_participant_audit_history.prepare_response(
            AUDIT_HISTORY
        )
        # Assert
        self.assertEqual(expected_response, actual_response)

    @patch(f'{module}.log')
    @patch(f'{module}.get_nhs_number_by_participant_id')
    def test_participant_not_found(
            self,
            get_nhs_number_by_participant_id_mock,
            log_mock
    ):
        # Arrange
        expected_log_calls = EXPECTED_LOG_CALLS_NOT_FOUND
        self.event = VALID_EVENT
        get_nhs_number_by_participant_id_mock.return_value = "participant not found"
        # Act
        result = get_participant_audit_history.lambda_handler(self.event, self.context)
        # Assert
        get_nhs_number_by_participant_id_mock.assert_called_with('565', segregate=True)
        log_mock.assert_has_calls(expected_log_calls)
        assertApiResponse(self, 404, {"message": "Participant not found."}, result)
