This lambda returns the audit history for a participant

* Successful response in the 'data' field of the response body:

See the [Audit Model](https://git.digital.nhs.uk/screening/cervical-screening/-/blob/master/web_app/src/api/models/Audit.model.ts) for an example of the expected response body
