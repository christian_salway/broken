from common.utils.lambda_wrappers import lambda_entry_point
from save_audit.log_references import LogReference
from common.log import log
import json
from datetime import datetime, timezone

from common.utils.dynamodb_access.operations import dynamodb_batch_write_records
from common.utils.dynamodb_access.table_names import TableNames


@lambda_entry_point
def lambda_handler(event, context):
    try:
        message_from_queue = event
        message_records = message_from_queue.get('Records', [{}])

        audit_records = []

        for message_record in message_records:
            message_id = str(message_record.get('messageId', 'NotProvided'))
            log({'log_reference': LogReference.SAVEAUDIT0001, 'message_id': message_id})
            processed_audit_records = build_audit_records(json.loads(message_record.get('body')))
            audit_records += processed_audit_records

        dynamodb_batch_write_records(TableNames.AUDIT, audit_records)

        log({'log_reference': LogReference.SAVEAUDIT0002})

        return {"statusCode": 200, "body": json.dumps({"message": "success"})}
    except Exception as e:
        log({'log_reference': LogReference.SAVEAUDIT0003, 'error': str(e)})
        raise e
    """ process events from sqs
    if event has multiple patients create separate audit entry for each participant"""


def build_audit_records(audit_message):

    audit_records = []

    participant_ids = audit_message.get('participant_ids')

    if not participant_ids:
        sort_key_prefix = audit_message['organisation_code'] + '#' if audit_message.get('organisation_code') else ''
        audit_message['sort_key'] = sort_key_prefix + str(audit_message['timestamp'])
        add_logged_time_and_expires(audit_message)
        audit_records.append(audit_message)
        return audit_records

    nhs_numbers = audit_message.get('nhs_numbers')
    if nhs_numbers:
        participants = [{'participant_id': participant_id, 'nhs_number': nhs_number}
                        for participant_id, nhs_number in zip(participant_ids, nhs_numbers)]
        del audit_message['nhs_numbers']
    else:
        participants = [{'participant_id': participant_id} for participant_id in participant_ids]
    del audit_message['participant_ids']

    for participant in participants:
        participant_id = participant['participant_id']
        record = audit_message.copy()
        record['sort_key'] = str(record['timestamp']) + '#' + participant_id
        record['participant_id'] = participant_id
        nhs_number = participant.get('nhs_number')
        if nhs_number:
            record['nhs_number'] = nhs_number
        add_logged_time_and_expires(record)
        audit_records.append(record)

    return audit_records


def add_logged_time_and_expires(audit_record):
    now = datetime.now(timezone.utc)
    now_plus_ten_years = now.replace(year=now.year + 10)
    now_plus_ten_years_utc_zone = now_plus_ten_years.replace(tzinfo=timezone.utc)
    audit_record['expires'] = int(now_plus_ten_years_utc_zone.timestamp())
    audit_record['logged_time'] = now.isoformat()
