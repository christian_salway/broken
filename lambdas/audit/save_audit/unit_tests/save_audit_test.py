from unittest.case import TestCase
from unittest.mock import patch, DEFAULT
from datetime import datetime, timezone, timedelta
from mock import Mock
from copy import deepcopy

from common.utils.dynamodb_access.table_names import TableNames


with patch("common.log.log") as mock_log:
    from save_audit.save_audit import lambda_handler, add_logged_time_and_expires

module = 'save_audit.save_audit'


def create_event(body):
    return {"Records": [{"body": body}]}


def create_multi_record_event(body1, body2):
    return {"Records": [{"body": body1}, {"body": body2}]}


@patch("save_audit.save_audit.datetime")
@patch("save_audit.save_audit.dynamodb_batch_write_records")
class TestSaveAudit(TestCase):
    def setUp(self):
        self.FIXED_NOW_TIME = datetime(2019, 12, 21, 12, 23, 23, 233, tzinfo=timezone.utc)

        self.single_view_participant_sqs = '''{
            "nhsid_useruid": "user123",
            "session_id": "9999999",
            "action": "VIEW_PARTICIPANT",
            "timestamp": "2020-03-09T13:35:41.696446+00:00",
            "role_id": "role123",
            "participant_ids": ["participant_id_123"],
            "nhs_numbers": ["nhs_number"]
        }'''

        self.single_view_participant_sqs_no_nhs_number = '''{
            "nhsid_useruid": "user123",
            "session_id": "9999999",
            "action": "VIEW_PARTICIPANT",
            "timestamp": "2020-03-09T13:35:41.696446+00:00",
            "role_id": "role123",
            "participant_ids": ["participant_id_123"]
        }'''

        self.expected_participant_audit_record = {
            "nhsid_useruid": "user123",
            "session_id": "9999999",
            "action": "VIEW_PARTICIPANT",
            "timestamp": "2020-03-09T13:35:41.696446+00:00",
            "role_id": "role123",
            "sort_key": "2020-03-09T13:35:41.696446+00:00#participant_id_123",
            "participant_id": "participant_id_123",
            'nhs_number': 'nhs_number',
            "expires": 1892550203,
            "logged_time": "2019-12-21T12:23:23.000233+00:00"
        }

        self.expected_participant_audit_record_no_nhs_number = {
            "nhsid_useruid": "user123",
            "session_id": "9999999",
            "action": "VIEW_PARTICIPANT",
            "timestamp": "2020-03-09T13:35:41.696446+00:00",
            "role_id": "role123",
            "sort_key": "2020-03-09T13:35:41.696446+00:00#participant_id_123",
            "participant_id": "participant_id_123",
            "expires": 1892550203,
            "logged_time": "2019-12-21T12:23:23.000233+00:00"
        }

        self.select_role_sqs = '''{
            "nhsid_useruid": "555249831105",
            "session_id": "9999999",
            "action": "SELECT_ROLE",
            "timestamp": "2020-03-11T18:26:15.751367+00:00",
            "role_id": "20001"
            }'''

        self.expected_role_audit_record = {
            "nhsid_useruid": "555249831105",
            "session_id": "9999999",
            "action": "SELECT_ROLE",
            "timestamp": "2020-03-11T18:26:15.751367+00:00",
            "role_id": "20001",
            "sort_key": "2020-03-11T18:26:15.751367+00:00",
            "expires": 1892550203,
            "logged_time": "2019-12-21T12:23:23.000233+00:00"
        }

        self.multiple_search_participant_sqs = ''' {
            "nhsid_useruid": "user123",
            "session_id": "9999999",
            "action": "SEARCH_PARTICIPANTS",
            "timestamp": "2020-03-09T13:35:41.696446+00:00",
            "role_id": "role123",
            "participant_ids": ["123456789", "987654321", "999999999"],
            "nhs_numbers": ["nhs_number_1", "nhs_number_2", "nhs_number_3"]
            } '''

        self.expected_participant_audit_record_1 = {
            "nhsid_useruid": "user123",
            "session_id": "9999999",
            "action": "SEARCH_PARTICIPANTS",
            "timestamp": "2020-03-09T13:35:41.696446+00:00",
            "role_id": "role123",
            "sort_key": "2020-03-09T13:35:41.696446+00:00#123456789",
            "participant_id": "123456789",
            "nhs_number": "nhs_number_1",
            "expires": 1892550203,
            "logged_time": "2019-12-21T12:23:23.000233+00:00"
        }

        self.expected_participant_audit_record_2 = {
            "nhsid_useruid": "user123",
            "session_id": "9999999",
            "action": "SEARCH_PARTICIPANTS",
            "timestamp": "2020-03-09T13:35:41.696446+00:00",
            "role_id": "role123",
            "sort_key": "2020-03-09T13:35:41.696446+00:00#987654321",
            "participant_id": "987654321",
            "nhs_number": "nhs_number_2",
            "expires": 1892550203,
            "logged_time": "2019-12-21T12:23:23.000233+00:00"
        }

        self.expected_participant_audit_record_3 = {
            "nhsid_useruid": "user123",
            "session_id": "9999999",
            "action": "SEARCH_PARTICIPANTS",
            "timestamp": "2020-03-09T13:35:41.696446+00:00",
            "role_id": "role123",
            "sort_key": "2020-03-09T13:35:41.696446+00:00#999999999",
            "participant_id": "999999999",
            "nhs_number": "nhs_number_3",
            "expires": 1892550203,
            "logged_time": "2019-12-21T12:23:23.000233+00:00"
        }

        self.add_PNL_email_address_sqs = '''{
            "nhsid_useruid": "555249831105",
            "session_id": "9999999",
            "action": "ADD_GP_PNL_EMAIL_ADDRESS",
            "timestamp": "2020-03-11T18:26:15.751367+00:00",
            "role_id": "20001",
            "email_address": "test@nhs.co.uk",
            "organisation_code": "B86044"
            }'''

        self.expected_add_PNL_email_address_audit_record = {
            "nhsid_useruid": "555249831105",
            "session_id": "9999999",
            "action": "ADD_GP_PNL_EMAIL_ADDRESS",
            "timestamp": "2020-03-11T18:26:15.751367+00:00",
            "role_id": "20001",
            "sort_key": "B86044#2020-03-11T18:26:15.751367+00:00",
            "email_address": "test@nhs.co.uk",
            "organisation_code": "B86044",
            "expires": 1892550203,
            "logged_time": "2019-12-21T12:23:23.000233+00:00"
        }

    def test_creates_correct_expiry_and_logged_times_for_single_participant(self, mock_put_audit_records,
                                                                            mock_date_time):
        mock_date_time.now = Mock(return_value=self.FIXED_NOW_TIME)
        event = create_event(self.single_view_participant_sqs)
        lambda_handler.__wrapped__(event, {})
        mock_put_audit_records.assert_called_with(TableNames.AUDIT, [self.expected_participant_audit_record])

    def test_creates_audit_record_for_single_participant_with_no_nhs_number(self, mock_put_audit_records,
                                                                            mock_date_time):
        mock_date_time.now = Mock(return_value=self.FIXED_NOW_TIME)
        event = create_event(self.single_view_participant_sqs_no_nhs_number)
        lambda_handler.__wrapped__(event, {})
        mock_put_audit_records.assert_called_with(
            TableNames.AUDIT, [self.expected_participant_audit_record_no_nhs_number])

    def test_creates_single_audit_record_when_no_participant_ids(self, mock_put_audit_records, mock_date_time):
        mock_date_time.now = Mock(return_value=self.FIXED_NOW_TIME)
        event = create_event(self.select_role_sqs)
        lambda_handler.__wrapped__(event, {})
        mock_put_audit_records.assert_called_with(TableNames.AUDIT, [self.expected_role_audit_record])

    def test_creates_multiple_audit_records_when_multiple_participant_ids(self, mock_put_audit_records,
                                                                          mock_date_time):
        mock_date_time.now = Mock(return_value=self.FIXED_NOW_TIME)
        event = create_event(self.multiple_search_participant_sqs)
        lambda_handler.__wrapped__(event, {})
        mock_put_audit_records.assert_called_with(
            TableNames.AUDIT,
            [
                self.expected_participant_audit_record_1,
                self.expected_participant_audit_record_2,
                self.expected_participant_audit_record_3
            ])

    def test_creates_single_audit_record_when_organisation_code_and_email(self, mock_put_audit_records, mock_date_time):
        mock_date_time.now = Mock(return_value=self.FIXED_NOW_TIME)
        event = create_event(self.add_PNL_email_address_sqs)
        lambda_handler.__wrapped__(event, {})
        mock_put_audit_records.assert_called_with(TableNames.AUDIT, [self.expected_add_PNL_email_address_audit_record])

    @patch("save_audit.save_audit.datetime")
    def test_add_logged_time_and_expires_add_expire_correctly_with_different_zone(self, mock_put_audit_records,
                                                                                  mock_date_time, datetime_mock):
        datetime_mock.now.return_value = datetime(2020, 11, 21, 16, 30, tzinfo=timezone(-timedelta(hours=9)))
        expected_time_stamp = int(datetime(2030, 11, 21, 16, 30, tzinfo=timezone.utc).timestamp())

        record = {}
        add_logged_time_and_expires(record)

        self.assertEqual(expected_time_stamp, record['expires'])

    def _copy_call_args(self, mock):
        new_mock = Mock()

        def side_effect(*args, **kwargs):
            args = deepcopy(args)
            kwargs = deepcopy(kwargs)
            new_mock(*args, **kwargs)
            return DEFAULT
        mock.side_effect = side_effect
        return new_mock
