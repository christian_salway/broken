import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    SAVEAUDIT0001 = (logging.INFO, 'Successfully consumed from the SQS Queue')
    SAVEAUDIT0002 = (logging.INFO, 'Successfully updated table')
    SAVEAUDIT0003 = (logging.ERROR, 'Error processing audit message')
