from datetime import datetime, timezone
from unittest import TestCase
from unittest.mock import patch, Mock
from boto3.dynamodb.conditions import Key, Attr

from get_user_audit_history import get_user_audit_history

env_vars = {
    'DYNAMODB_AUDIT': 'DYNAMODB_AUDIT',
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL'
}
example_date = datetime(2020, 12, 23, 13, 48, 8, 123456, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date
sqs_client_mock = Mock()


module = 'get_user_audit_history.get_user_audit_history'


@patch('os.environ', env_vars)
class TestGetAuditHistory(TestCase):

    audit_events = [{
        "action": "SEARCH_PARTICIPANTS",
        "nhsid_useruid": "555249831105",
        "participant_id": "37f4524d-e59f-41d3-9071-97eec87955c8",
        "nhs_number": "9999999999",
        "role_id": "R8010",
        "session_id": "9999999",
        "sort_key": "2020-11-01T12:41:45.123456#37f4524d-e59f-41d3-9071-97eec87955c8",
        "timestamp": "[TIMESTAMP]"
    }]

    @classmethod
    def setUpClass(cls, *args):
        sqs_client_mock = patch('common.utils.audit_utils._SQS_CLIENT').start()
        cls.sqs_client_mock = sqs_client_mock

    def setUp(self):
        # Setup database mocks for audit table
        get_dynamodb_table_mock = patch('common.utils.dynamodb_access.operations.get_dynamodb_table').start()

        self.audit_table_mock = Mock()
        self.audit_table_mock.query.return_value = {'Items': self.audit_events}

        get_dynamodb_table_mock.return_value = self.audit_table_mock

        common_log_patcher = patch('common.log.log')
        self.module_log_patcher = patch('get_user_audit_history.get_user_audit_history.log')
        self.event = _build_event('the_user_id', 'the_session_id')
        self.context = Mock()
        self.context.function_name = ''
        common_log_patcher.start()
        self.module_log_patcher.start()

    def tearDown(self):
        patch.stopall()

    @patch('common.utils.audit_utils.datetime', datetime_mock)
    def test_get_participant_audit_history_makes_correct_calls(self):
        # Arrange is done during setUp
        # Act
        get_user_audit_history.lambda_handler(self.event, self.context)
        # Assert that dynamodb is called with the correct args
        self.audit_table_mock.query.assert_called_with(
            KeyConditionExpression=Key('nhsid_useruid').eq('123456654321'),
            FilterExpression=Attr('timestamp').begins_with('2020-06-27')
        )
        # Asserts the audit record has been sent to the audit queue
        self.sqs_client_mock.send_message.assert_called_with(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody='{"action": "VIEW_USER_AUDIT_HISTORY", "timestamp": "2020-12-23T13:48:08.123456+00:00", "internal_id": "request_id", "nhsid_useruid": "the_user_id", "session_id": "the_session_id", "first_name": "John", "last_name": "Doe", "additional_information": {"nhsid_useruid": "123456654321"}}'  
        )


def _build_event(user_uid, session_id):
    return {
        'headers': {
            'request_id': 'request_id',
        },
        'body': '{"user_id": "123456654321", "date": "2020-06-27"}',
        'requestContext': {
            'authorizer': {
                'principalId': 'blah',
                'session': f"{{\"user_data\": {{\"nhsid_useruid\": \"{user_uid}\", \"first_name\": \"John\", \"last_name\": \"Doe\"}}, \"session_id\": \"{session_id}\"}}"  
            }
        }
    }
