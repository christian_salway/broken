This lambda is being triggered by a POST request to the endpoint /users/audit

it accepts body of search parameters like this:
```
{
  "user_id": "123456789012",
  "date": "2020-12-25",
}
```

It returns a list of audit items where Audit.nhsid_useruid = Request.user_id and Audit.timestamp begins with Request.date