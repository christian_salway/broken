import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    GETUSERAUDIT001 = (logging.INFO, 'Received request to view user audit history.')
    GETUSERAUDIT002 = (logging.INFO, 'Search parameters missing or invalid.')
    GETUSERAUDIT003 = (logging.INFO, 'Returning user audit events.')
