from datetime import datetime, timezone, date
import re

from boto3.dynamodb.conditions import Key, Attr

from common.log import log
from common.utils.audit_utils import audit, AuditActions
from common.utils.session_utils import get_session_from_lambda_event
from common.utils import json_return_message, json_return_data
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.dynamodb_access.operations import dynamodb_query
from get_user_audit_history.log_references import LogReference
from common.utils.dynamodb_access.table_names import TableNames

AUDIT_LOG_READ_ATTRIBUTES = [
    'first_name', 'last_name', 'role_id', 'user_organisation_code', 'action', 'timestamp', 'nhs_number'
]


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    event_data, *_ = get_api_request_info_from_event(event)
    request_body = event_data.get('body')

    log({'log_reference': LogReference.GETUSERAUDIT001})

    is_valid_request = validate_search_parameters(request_body)

    if not is_valid_request:
        log({'log_reference': LogReference.GETUSERAUDIT002})
        return json_return_message(400, 'Search parameters missing or invalid.')

    audit_events = dynamodb_query(TableNames.AUDIT, {
        'KeyConditionExpression': Key('nhsid_useruid').eq(request_body['user_id']),
        'FilterExpression': Attr('timestamp').begins_with(request_body['date'])
    })

    audit_events = [{k: v for k, v in event.items() if k in AUDIT_LOG_READ_ATTRIBUTES} for event in audit_events]

    session = get_session_from_lambda_event(event)
    audit(session=session,
          action=AuditActions.VIEW_USER_AUDIT_HISTORY,
          additional_information={'nhsid_useruid': request_body['user_id']})

    log({'log_reference': LogReference.GETUSERAUDIT003, 'number_of_records': len(audit_events)})
    return json_return_data(200, audit_events)


def validate_search_parameters(request_body):
    if not request_body:
        return False

    user_id = request_body.get('user_id')
    date = request_body.get('date')

    if not user_id or not date:
        return False

    return is_valid_user_id(user_id) and is_valid_date(date)


def is_valid_user_id(user_id):
    is_valid_user_id_matcher = re.compile(r'\d{12}$')
    return bool(is_valid_user_id_matcher.match(user_id))


def is_valid_date(date_to_check):
    now = datetime.now(timezone.utc).date()
    try:
        converted_date = date.fromisoformat(date_to_check)
        return converted_date <= now
    except ValueError:
        return False
