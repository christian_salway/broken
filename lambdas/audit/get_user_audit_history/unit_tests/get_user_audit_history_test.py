from unittest import TestCase

from boto3.dynamodb.conditions import Key, Attr
from mock import patch, Mock, call
from datetime import datetime, timezone
from ddt import ddt, data, unpack

from get_user_audit_history.log_references import LogReference
from common.test_assertions.api_assertions import assertApiResponse
from common.utils.dynamodb_access.table_names import TableNames

FIXED_NOW_TIME = datetime(2020, 12, 25, tzinfo=timezone.utc)

env_vars = {
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL'
}


@ddt
class TestLambdaHandler(TestCase):

    module = 'get_user_audit_history.get_user_audit_history'

    @classmethod
    @patch('boto3.client', Mock())
    @patch('boto3.resource', Mock())
    def setUpClass(cls):
        with patch('os.environ', env_vars):
            import common.utils.audit_utils as audit_utils
            import get_user_audit_history.get_user_audit_history as get_user_audit_history

        cls.audit_utils = audit_utils
        cls.get_user_audit_history = get_user_audit_history
        cls.date_time_patcher = patch.object(get_user_audit_history, 'datetime', Mock(wraps=datetime))
        cls.date_time_patcher.start()
        cls.get_user_audit_history.datetime.now.return_value = FIXED_NOW_TIME

    @classmethod
    def tearDownClass(cls):
        cls.date_time_patcher.stop()

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.get_session_from_lambda_event')
    @patch(f'{module}.dynamodb_query')
    @patch(f'{module}.validate_search_parameters')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_lambda_handler_returns_200_and_data_given_valid_request(
            self, get_info_mock, request_validator_mock, get_data_mock, get_session_mock, audit_mock, log_mock):
        get_info_mock.return_value = {'body': {'date': '2020-12-25', 'user_id': '123456'}}, ''
        request_validator_mock.return_value = True
        data = [
            {'first_name': 'firsty1', 'last_name': 'lasty1', 'role_id': 'role1', 'user_organisation_code': 'org1',
             'action': 'action1', 'timestamp': 'time1', 'nhs_number': 'nhsnum1', 'other_field': 'other_value'},
            {'first_name': 'firsty2', 'last_name': 'lasty2', 'role_id': 'role2', 'user_organisation_code': 'org2',
             'action': 'action2', 'timestamp': 'time2', 'other_field': 'other_value'}
        ]
        get_data_mock.return_value = data
        session = {'session_id': 'the_session'}
        get_session_mock.return_value = session
        expected_body = {'data': [
            {'first_name': 'firsty1', 'last_name': 'lasty1', 'role_id': 'role1', 'user_organisation_code': 'org1',
             'action': 'action1', 'timestamp': 'time1', 'nhs_number': 'nhsnum1'},
            {'first_name': 'firsty2', 'last_name': 'lasty2', 'role_id': 'role2', 'user_organisation_code': 'org2',
             'action': 'action2', 'timestamp': 'time2'}
        ]}
        actual_response = self.get_user_audit_history.lambda_handler.__wrapped__.__wrapped__('the_event', {})
        assertApiResponse(self, 200, expected_body, actual_response)

        get_info_mock.assert_called_with('the_event')
        request_validator_mock.assert_called_with({'date': '2020-12-25', 'user_id': '123456'})

        get_data_mock.assert_called_with(TableNames.AUDIT, {
            'KeyConditionExpression': Key('nhsid_useruid').eq('123456'),
            'FilterExpression': Attr('timestamp').begins_with('2020-12-25')
        })

        get_session_mock.assert_called_with('the_event')
        audit_mock.assert_called_with(session=session,
                                      action=self.audit_utils.AuditActions.VIEW_USER_AUDIT_HISTORY,
                                      additional_information={'nhsid_useruid': '123456'})
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETUSERAUDIT001}),
            call({'log_reference': LogReference.GETUSERAUDIT003, 'number_of_records': 2})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.validate_search_parameters')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_lambda_handler_returns_400_given_invalid_request(self, get_info_mock, request_validator_mock, log_mock):
        get_info_mock.return_value = {'body': {'date': '2020-12-25'}}, ''
        request_validator_mock.return_value = False
        actual_response = self.get_user_audit_history.lambda_handler.__wrapped__.__wrapped__('the_event', {})
        assertApiResponse(self, 400, {'message': 'Search parameters missing or invalid.'}, actual_response)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.GETUSERAUDIT001}),
            call({'log_reference': LogReference.GETUSERAUDIT002})
        ])

    @unpack
    @data(('', False), ({'date': '2020-12-25'}, False), ({'user_id': '123456654321'}, False),
          ({'date': '2020-12-25', 'user_id': '1'}, False), ({'date': '2', 'user_id': '123456654321'}, False),
          ({'date': '2020-12-25', 'user_id': '123456654321'}, True))
    def test_validate_search_parameters(self, request_body, expected_response):
        actual_response = self.get_user_audit_history.validate_search_parameters(request_body)
        self.assertEqual(expected_response, actual_response)

    @unpack
    @data(('12345678901', False), ('1234567890123', False), ('12d4567890be', False), ('123456789012', True))
    def test_is_valid_user_id(self, user_id, expected_response):
        actual_response = self.get_user_audit_history.is_valid_user_id(user_id)
        self.assertEqual(expected_response, actual_response)

    @unpack
    @data(('02-03-2020', False), ('nonsense', False), ('2020-13-32', False), ('2020-12-26', False),
          ('2020-12-25', True), ('2020-12-24', True))
    def test_is_valid_date(self, date, expected_response):
        actual_response = self.get_user_audit_history.is_valid_date(date)
        self.assertEqual(expected_response, actual_response)
