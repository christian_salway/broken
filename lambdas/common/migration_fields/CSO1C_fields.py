# Fields within the 1C Output Record, assigned numbers are the index
# of the delimited data structure corresponding to the field
# See https://nhsd-confluence.digital.nhs.uk/display/CS/1C.+Subject+CS+Output+Record for descriptions
CSO1C_FIELDS = [
    'SOURCE_CIPHER',
    'NHSNUM',
    'NTD',
    'RECALL_TYPE',
    'RECALL_STAT',
    'FNR_COUNT',
    'POS_DOUBT',
    'CEASED_FLAG',
    'CEASED_CIPHER',
    'NOTES1',
    'NOTES2',
    'NOTES3',
    'LDN',
    'OAPD',
    'NOTIF_DATE',
    'INV_DATE',
    'PNL_DATE',
    'PATH_ID',
    'PNL_FNR'
]
