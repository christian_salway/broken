class DynamodbException(Exception):
    message = 'Unexpected error encountered reading from database'

    def __init__(self):
        super().__init__(self.message)


class MeshCredentialsNotFoundException(Exception):
    message = 'Mesh Credentials not found'

    def __init__(self):
        super().__init__(self.message)


class InvalidEventRuleException(Exception):
    message = 'Event Rule could not be determined'

    def __init__(self):
        super().__init__(self.message)


# Secrets Manager Custom Exceptions
class DecryptionFailureException(Exception):
    message = 'Secrets Manager can\'t decrypt the protected secret text using the provided KMS key'

    def __init__(self):
        super().__init__(self.message)


class InternalServiceErrorException(Exception):
    message = 'An error occurred on the server side'

    def __init__(self):
        super().__init__(self.message)


class InvalidParameterException(Exception):
    message = 'You provided an invalid value for a parameter'

    def __init__(self):
        super().__init__(self.message)


class InvalidRequestException(Exception):
    message = 'You provided a parameter value that is not valid for the current state of the resource'

    def __init__(self):
        super().__init__(self.message)


class ResourceNotFoundException(Exception):
    message = 'We can\'t find the resource that you asked for'

    def __init__(self):
        super().__init__(self.message)


class InvalidMailboxIdException(Exception):
    def __init__(self, mailbox_id):
        super().__init__(
            f"Sender's mailbox id [{mailbox_id}] is not in the list of the expected mailboxes to receive results from")


class PrintFileMissingException(Exception):
    message = 'Print file is missing from the table'

    def __init__(self):
        super().__init__(self.message)
