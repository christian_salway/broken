# Common

This is the common library to be used in all lambdas.

- [Common](#common)
  - [Adding to Lambdas](#adding-to-lambdas)
  - [Testing](#testing)

## Adding to Lambdas

It should be sym-linked into each lambda directory.

The build job will copy this code into the lambda packages, following the symlinks.

## Testing

To test we use nosetests.

```bash
cd lambdas/common && nosetests
# OR
nosetests lambdas/common
```
