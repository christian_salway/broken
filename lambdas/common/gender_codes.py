from enum import Enum


class GenderCode(Enum):

    NOT_KNOWN = '0'
    MALE = '1'
    FEMALE = '2'
    NOT_SPECIFIED = '9'
