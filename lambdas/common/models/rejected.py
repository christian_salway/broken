from enum import Enum


class RejectedStatus(str, Enum):
    UNPROCESSED = 'UNPROCESSED'
    WRITTEN_TO_CSV = 'WRITTEN_TO_CSV'
