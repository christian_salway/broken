from enum import Enum

SELF_SAMPLE_SENDER_CODE = 'SLFSMP'


class ResultRejectionReason(str, Enum):
    CEASED_DUE_TO_RADIOTHERAPY = 'Ceased - RADIOTHERAPY'
    CEASED_NO_CERVIX = 'Ceased - NO_CERVIX'
    INVALID_RETURN_TO_ROUTINE_RECALL = 'Invalid return to routine recall'
    REMOVED_DEATH = 'Removed - death'
    SECOND_CONSECUTIVE_INADEQUATE_COLPOSCOPY = '2nd (inad) Colp Res'
    THIRD_POSITIVE_HPV_RESULT = '3rd (positive) Colp Res'
    MALE_NOT_IN_COHORT = 'MALE - NOT IN COHORT'


class ResultWorkflowState(str, Enum):
    PROCESS = 'process'
    QUERY = 'query'
    CHECK = 'check'
    REJECTED = 'rejected'


class ResultSecondaryStatus(str, Enum):
    NOT_STARTED = 'NOT STARTED'
    WITH_PROCESSOR = 'WITH PROCESSOR'
    CHECKER_REJECTED = 'CHECKER REJECTED'
    WITH_CHECKER = 'WITH CHECKER'
    NOT_CHECKED = 'NOT CHECKED'
    NOT_ACTIONED = 'NOT ACTIONED'
    ACTIONED = 'ACTIONED'
    DUPLICATE_SLIDE = 'DUPLICATE SLIDE'


class ResultAction(str, Enum):
    CHECK_ACCEPT_ACTION = 'accept'
    REJECTED_DELETE_ACTION = 'delete'


class ResultAddressSelection(str, Enum):
    ACTION_SEND_TO_SYSTEM = 'Send to system address'
    ACTION_SEND_TO_LAB = 'Send to lab address'
    ACTION_SEND_TO_SENDER = 'Send to sender address'
    ACTION_NEW_ADDRESS = 'Enter a new address'


class ResultSendingLabCodes(str, Enum):
    NCL3 = "NCL3"
    RSH3 = "RSH3"
    WOL3 = "WOL3"
    QEH3 = "QEH3"
    NOR3 = "NOR3"
    CHR3 = "CHR3"
    NBT3 = "NBT3"
    DBL3 = "DBL3"
    NOB3 = "NOB3"
