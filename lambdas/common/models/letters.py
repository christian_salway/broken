from enum import Enum


class PrintDestination(str, Enum):
    CIC = 'CIC'
    DMS = 'DMS'
    IOM = 'IOM'


class AWSLetterType(str, Enum):
    INVITE_REMINDER = 'invite-reminder'
    RESULT_CEASE = 'result-cease'


class JobType(str, Enum):
    INVITE_REMINDER = 'RP'
    RESULT_CEASE = 'CP'


class NotificationStatus(str, Enum):
    SENT = 'SENT'
    FAILED = 'FAILED'
    PENDING_PRINT_FILE = 'PENDING_PRINT_FILE'
    IN_PRINT_FILE = 'IN_PRINT_FILE'
    WITH_PRINTER = 'WITH_PRINTER'
    SUPPRESSED = 'SUPPRESSED'


class NotificationType(str, Enum):
    INVITATION = 'Invitation letter'
    REMINDER = 'Reminder letter'
    RESULT = 'Result letter'
    CEASE = 'Cease letter'


class PrintFileStatus(str, Enum):
    PENDING = 'PENDING'
    WITH_PRINTER = 'WITH_PRINTER'
    TRANSPORT_FAILURE = 'TRANSPORT_FAILURE'


class SupressionReason(str, Enum):
    OLD_TEST = 'Suppressing result letter as result is older than cut-off'
    SOURCE_TYPE_7 = 'Suppressing result letter as lab will send their own letter'
    HISTORIC_RESULT = 'Suppressing result letter as more recent result on record'
