from enum import Enum


class NotificationType(str, Enum):
    INVITATION = "Invitation letter"
    REMINDER = "Reminder letter"
    RESULT = "Result letter"
