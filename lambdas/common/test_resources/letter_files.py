from common.models.letters import NotificationStatus, PrintDestination, JobType


expected_notification_data = {
    'TEMPLATE': 'Test template',
    'JOBID': 'RP',
    'JOBNO': '',
    'RECNO': '',
    'LETTCODE': 'Test template',
    'AJRUNDATE': '23.01.20',
    'GP_PART_CODE': '',
    'LETT_TYPE': '',
    'RECALL_TYPE': 'CALLED',
    'RECALL_STATUS': '',
    'DHA': '',
    'SENDER': '',
    'ORIG_TYPE': '',
    'DEST_TYPE': '',
    'REASON1': '',
    'REASON2': '',
    'REASON3': '',
    'REASON4': '',
    'REASON5': '',
    'PAT_TITLE': 'test_title',
    'PAT_SURNAME': 'test_last_name',
    'PAT_FORENAME': 'test_first_name',
    'PAT_OTH_FORE': '',
    'PAT_PREV_SURN': '',
    'PAT_DOB': '',
    'PAT_LTEST_DATE': '',
    'PAT_NHS': 'test_nhs_number',
    'PAT_REC_DATE': '',
    'PAT_ADD1': 'test_address_line_1',
    'PAT_ADD2': 'test_address_line_2',
    'PAT_ADD3': 'test_address_line_3',
    'PAT_ADD4': 'test_address_line_4',
    'PAT_ADD5': 'test_address_line_5',
    'PAT_POSTCODE': 'test_postcode',
    'HA_CIPHER': 'EU-WEST-2',
    'HA_NAME': '',  # Not used
    'PRINT_DATE': '',
    'ORIG_NAME': '',
    'ORIG_ADD1': '',
    'ORIG_ADD2': '',
    'ORIG_ADD3': '',
    'ORIG_ADD4': '',
    'ORIG_ADD5': '',
    'ORIG_POSTCODE': '',
    'ORIG_NAT_CODE': '',
    'ORIG_LOC_CODE': '',
    'ORIG_TEL_NO': '',
    'DEST_NAME': '',
    'DEST_ADD1': 'test_address_line_1',
    'DEST_ADD2': 'test_address_line_2',
    'DEST_ADD3': 'test_address_line_3',
    'DEST_ADD4': 'test_address_line_4',
    'DEST_ADD5': 'test_address_line_5',
    'DEST_POSTCODE': 'test_postcode',
    'DEST_NAT_CODE': '',
    'DEST_LOC_CODE': '',
    'GP_NAME': '',
    'GP_ADD1': '',
    'GP_ADD2': '',
    'GP_ADD3': '',
    'GP_ADD4': '',
    'GP_ADD5': '',
    'GP_POSTCODE': '',
    'GP_NAT_CODE': '',
    'GP_LOC_CODE': '',
    'GP_TEL_NO': '',
    'GP_TEXT1': 'test_gp_text1',
    'GP_TEXT2': 'test_gp_text2',
    'GP_TEXT3': 'test_gp_text3',
    'GP_TEXT4': 'test_gp_text4',
    'GP_TEXT5': 'test_gp_text5',
    'HMR_REQ': '',
    'PAT_SLIDE_NO': '',
    'LAB_NAT_CODE': '',
    'M_2_NON_NEGS': '',
    'LAST_TEST_DATE': '',
    'LAST_TEST_RESULT': '',
    'LAST_TEST_RECALL_TYPE': '',
    'PRIOR_TEST_DATE': '',
    'PRIOR_TEST_RESULT': '',
    'PRIOR_TEST_RECALL_TYPE': '',
    'INV1_TEXT1': '',
    'INV1_TEXT2': '',
    'INV1_TEXT3': '',
    'INV1_TEXT4': '',
    'INV1_TEXT5': '',
    'INV2_TEXT1': '',
    'INV2_TEXT2': '',
    'INV2_TEXT3': '',
    'INV2_TEXT4': '',
    'INV2_TEXT5': '',
    'RES1_TEXT1': '',
    'RES1_TEXT2': '',
    'RES1_TEXT3': '',
    'RES1_TEXT4': '',
    'RES1_TEXT5': '',
    'RES2_TEXT1': '',
    'RES2_TEXT2': '',
    'RES2_TEXT3': '',
    'RES2_TEXT4': '',
    'RES2_TEXT5': ''
}

invite_text = {
    'INV1_TEXT1': 'test_inv1_text1',
    'INV1_TEXT2': 'test_inv1_text2',
    'INV1_TEXT3': 'test_inv1_text3',
    'INV1_TEXT4': 'test_inv1_text4',
    'INV1_TEXT5': 'test_inv1_text5',
    'INV2_TEXT1': 'test_inv2_text1',
    'INV2_TEXT2': 'test_inv2_text2',
    'INV2_TEXT3': 'test_inv2_text3',
    'INV2_TEXT4': 'test_inv2_text4',
    'INV2_TEXT5': 'test_inv2_text5'
}

result_text = {
    'JOBID': 'CP',
    'RES1_TEXT1': 'test_res1_text1',
    'RES1_TEXT2': 'test_res1_text2',
    'RES1_TEXT3': 'test_res1_text3',
    'RES1_TEXT4': 'test_res1_text4',
    'RES1_TEXT5': 'test_res1_text5',
    'RES2_TEXT1': 'test_res2_text1',
    'RES2_TEXT2': 'test_res2_text2',
    'RES2_TEXT3': 'test_res2_text3',
    'RES2_TEXT4': 'test_res2_text4',
    'RES2_TEXT5': 'test_res2_text5'
}

notification_address = {
    'PAT_ADD1': '1 STREETY STREET',
    'PAT_ADD2': 'NEIGHBOURHOOD',
    'PAT_ADD3': 'TOWNSVILLE',
    'PAT_ADD4': 'CITYHAM',
    'PAT_ADD5': 'COUNTYSHIRE',
    'PAT_POSTCODE': '1AA AA1',
    'DEST_ADD1': '1 STREETY STREET',
    'DEST_ADD2': 'NEIGHBOURHOOD',
    'DEST_ADD3': 'TOWNSVILLE',
    'DEST_ADD4': 'CITYHAM',
    'DEST_ADD5': 'COUNTYSHIRE',
    'DEST_POSTCODE': '1AA AA1',
}

expected_invitation_notification_record = {
    'participant_id': 'test',
    'sort_key': 'NOTIFICATION#2020-01-23T13:48:08.123456+00:00',
    'created': '2020-01-23T13:48:08.123456+00:00',
    'status': NotificationStatus.PENDING_PRINT_FILE,
    'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
    'type': 'Invitation letter',
    'print_destination': 'CIC',
    'record_id': 'EPISODE#2020-01-01',
    'data': {**expected_notification_data, **invite_text}
}

sample_notification_record = {
    **expected_invitation_notification_record,
    'participant_id': 'participant',
}

sample_test_notification_record = {
    **expected_invitation_notification_record,
    'participant_id': 'test_participant',
    'data': {
        'PAT_NHS': '9565512593'
    }
}

sample_notification_record_list_with_test_record = [
    sample_notification_record,
    sample_test_notification_record
]

expected_invitation_notification_record_with_episode_address = {
    'participant_id': 'test',
    'sort_key': 'NOTIFICATION#2020-01-23T13:48:08.123456+00:00',
    'created': '2020-01-23T13:48:08.123456+00:00',
    'status': NotificationStatus.PENDING_PRINT_FILE,
    'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
    'type': 'Invitation letter',
    'print_destination': 'CIC',
    'record_id': 'test',
    'data': {**expected_notification_data, **invite_text, **notification_address}
}

expected_reminder_notification_record = {
    'participant_id': 'test',
    'sort_key': 'NOTIFICATION#2020-01-23T13:48:08.123456+00:00',
    'created': '2020-01-23T13:48:08.123456+00:00',
    'status': NotificationStatus.PENDING_PRINT_FILE,
    'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
    'type': 'Reminder letter',
    'print_destination': PrintDestination.CIC,
    'record_id': 'EPISODE#2020-01-01',
    'data': {**expected_notification_data, **invite_text}
}

expected_cease_notification_record = {
    'participant_id': 'test',
    'sort_key': 'NOTIFICATION#2020-01-23T13:48:08.123456+00:00',
    'created': '2020-01-23T13:48:08.123456+00:00',
    'status': NotificationStatus.PENDING_PRINT_FILE,
    'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
    'type': 'Cease letter',
    'print_destination': PrintDestination.CIC,
    'record_id': 'EPISODE#2020-01-01',
    'data': {**expected_notification_data, 'JOBID': JobType.RESULT_CEASE}
}

expected_result_notification_record = {
    'participant_id': 'test',
    'sort_key': 'NOTIFICATION#2020-01-23T13:48:08.123456+00:00',
    'created': '2020-01-23T13:48:08.123456+00:00',
    'status': NotificationStatus.PENDING_PRINT_FILE,
    'live_record_status': NotificationStatus.PENDING_PRINT_FILE,
    'type': 'Result letter',
    'print_destination': PrintDestination.CIC,
    'record_id': 'EPISODE#2020-01-01',
    'data': {**expected_notification_data, **result_text}
}

sample_letter_config = {
    'gp_practice_code': 'test_practice',
    'gp_practice_name': 'The Test Practice',
    'gp_practice_group_id': 'test_practice_group',
    'gp_practice_group_name': 'The Test Group',
    'gp_text1': 'test_gp_text1',
    'gp_text2': 'test_gp_text2',
    'gp_text3': 'test_gp_text3',
    'gp_text4': 'test_gp_text4',
    'gp_text5': 'test_gp_text5',
    'inv1_text1': 'test_inv1_text1',
    'inv1_text2': 'test_inv1_text2',
    'inv1_text3': 'test_inv1_text3',
    'inv1_text4': 'test_inv1_text4',
    'inv1_text5': 'test_inv1_text5',
    'inv2_text1': 'test_inv2_text1',
    'inv2_text2': 'test_inv2_text2',
    'inv2_text3': 'test_inv2_text3',
    'inv2_text4': 'test_inv2_text4',
    'inv2_text5': 'test_inv2_text5',
    'res1_text1': 'test_res1_text1',
    'res1_text2': 'test_res1_text2',
    'res1_text3': 'test_res1_text3',
    'res1_text4': 'test_res1_text4',
    'res1_text5': 'test_res1_text5',
    'res2_text1': 'test_res2_text1',
    'res2_text2': 'test_res2_text2',
    'res2_text3': 'test_res2_text3',
    'res2_text4': 'test_res2_text4',
    'res2_text5': 'test_res2_text5',
}

sample_participant_record = {
    'participant_id': 'test',
    'sort_key': 'test',
    'title': 'test_title',
    'first_name': 'test_first_name',
    'last_name': 'test_last_name',
    'nhs_number': 'test_nhs_number',
    'registered_gp_practice_code': 'test',
    'address': {
        'address_line_1': 'test_address_line_1',
        'address_line_2': 'test_address_line_2',
        'address_line_3': 'test_address_line_3',
        'address_line_4': 'test_address_line_4',
        'address_line_5': 'test_address_line_5',
        'postcode': 'test_postcode',
    },
    'status': 'CALLED'
}

sample_episode_address = {
    'address_line_1': '1 STREETY STREET',
    'address_line_2': 'NEIGHBOURHOOD',
    'address_line_3': 'TOWNSVILLE',
    'address_line_4': 'CITYHAM',
    'address_line_5': 'COUNTYSHIRE',
    'postcode': '1AA AA1'
}

sample_csv_row = 'RP,test,1,Test template,23.01.20,"","","CALLED","","","","","","","","","","","test_title","test_last_name","test_first_name","","","","","test_nhs_number","","test_address_line_1","test_address_line_2","test_address_line_3","test_address_line_4","test_address_line_5","test_postcode","EU-WEST-2","","","","","","","","","","","","","","test_address_line_1","test_address_line_2","test_address_line_3","test_address_line_4","test_address_line_5","test_postcode","","","","","","","","","","","","","test_gp_text1","test_gp_text2","test_gp_text3","test_gp_text4","test_gp_text5","","","","test_inv1_text1","test_inv1_text2","test_inv1_text3","test_inv1_text4","test_inv1_text5","test_inv2_text1","test_inv2_text2","test_inv2_text3","test_inv2_text4","test_inv2_text5","","","","","","","","","",""\n'  
