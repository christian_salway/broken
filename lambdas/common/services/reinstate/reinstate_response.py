from dataclasses import dataclass, field
from datetime import date
from typing import List


@dataclass
class ReinstateResponse:
    result: str = None
    status: str = None
    next_test_due_date: date = None
    explanation: List[str] = field(default_factory=list)
    error: bool = False

    def to_data(self):
        return {
            'result': self.result,
            'status': self.status,
            'next_test_due_date': self.next_test_due_date.strftime("%Y-%m-%d") if self.next_test_due_date else None,
            'explanation': self.explanation,
            'error': self.error
        }

    def to_json(self):
        data = {}
        if self.status:
            data['status'] = self.status
        if self.next_test_due_date:
            data['next_test_due_date'] = self.next_test_due_date.strftime("%Y-%m-%d")
        if self.explanation:
            data['explanation'] = self.explanation
        return data

    def __eq__(self, other):
        return self.result == other.result and \
            self.status == other.status and \
            self.next_test_due_date == other.next_test_due_date and \
            self.explanation == other.explanation and \
            self.error == other.error
