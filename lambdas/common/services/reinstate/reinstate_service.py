from datetime import datetime, timezone
from common.log import log
from common.utils.participant_utils import transaction_write_records
from common.models.participant import ParticipantSortKey
from common.services.reinstate.log_references import LogReference


def reinstate_participant(
        participant_id: str, nhs_number: str, status: str, next_test_due_date: str,
        user_id: str, crm_comments=None, crm_number=None):

    participant_update_object = generate_reinstate_record(participant_id, next_test_due_date, status)
    log({'log_reference': LogReference.REINSTATESER0009})

    reinstate_put_object = generate_participant_update_record(participant_id, user_id, crm_comments, crm_number)
    log({'log_reference': LogReference.REINSTATESER0010})

    log({'log_reference': LogReference.REINSTATESER0011})
    transaction_write_records(
        put_records=[reinstate_put_object],
        update_records=[participant_update_object]
    )

    return reinstate_put_object


def generate_reinstate_record(participant_id: str, next_test_due_date: str, status: str) -> dict:
    return {
        'record_keys': {
            'participant_id': participant_id,
            'sort_key': ParticipantSortKey.PARTICIPANT.value
        },
        'delete_values': [],
        'update_values': {
            'is_ceased': False,
            'next_test_due_date': next_test_due_date,
            'status': status
        }
    }


def generate_participant_update_record(participant_id, user_id, crm_comments=None, crm_number=None):
    today_date = datetime.now(timezone.utc).isoformat()
    reinstate_put_object = {
        'participant_id': participant_id,
        'sort_key': f'{ParticipantSortKey.REINSTATE.value}#{today_date}',
        'date_from': today_date,
        'user_id': user_id,
    }
    if crm_comments:
        reinstate_put_object['comments'] = crm_comments
    if crm_number:
        reinstate_put_object['crm_number'] = crm_number
    return reinstate_put_object
