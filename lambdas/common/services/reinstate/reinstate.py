from common.services.reinstate.log_references import LogReference
from common.services.reinstate.reinstate_eligibility import can_participant_be_reinstated, is_valid_cease_reason
from common.services.reinstate.reinstate_service import reinstate_participant
from common.services.reinstate.reinstate_response import ReinstateResponse
from common.services.reinstate.reinstate_result import ReinstateResult
from common.models.participant import ParticipantSortKey
from common.utils.result_codes import (
    UNCHANGED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS as ignored_results, get_concatenated_result_code_combination
)
from common.utils.next_test_due_date_calculators.reinstate_calculator import \
    calculate
from common.utils.participant_utils import get_participant_and_test_results
from common.log import log


def reinstate_participant_by_participant_id(participant_id: str, user_id: str):
    log({'log_reference': LogReference.REINSTATESER0001})

    participant_and_results = get_participant_and_test_results(participant_id)
    if not participant_and_results:
        log({'log_reference': LogReference.REINSTATESER0002})
        return ReinstateResponse(result=ReinstateResult.NO_PARTICIPANT)

    participant = filter_by_sort_key(participant_and_results, ParticipantSortKey.PARTICIPANT)[0]
    if not can_participant_be_reinstated(participant):
        return ReinstateResponse(result=ReinstateResult.CANNOT_BE_REINSTATED)

    test_results = filter_by_sort_key(participant_and_results, ParticipantSortKey.RESULT)
    cease_records = filter_by_sort_key(participant_and_results, ParticipantSortKey.CEASE)

    test_results = remove_no_recall_change_result_combinations(test_results)
    if not cease_records:
        log({'log_reference': LogReference.REINSTATESER0007})
        return ReinstateResponse(result=ReinstateResult.NOT_CEASED)

    active_cease_reason = cease_records[0]['reason']
    if not is_valid_cease_reason(active_cease_reason):
        log({'log_reference': LogReference.REINSTATESER0008})
        return ReinstateResponse(result=ReinstateResult.INVALID_CEASE_REASON)

    reinstate_response = calculate(
        participant, test_results, active_cease_reason
    )

    if not reinstate_response.error:
        reinstate_participant(
            participant['participant_id'],
            participant['nhs_number'],
            reinstate_response.status,
            reinstate_response.next_test_due_date.isoformat(),
            user_id
        )
        log({
            'log_reference': LogReference.REINSTATESER0012,
            'next_test_due_date': reinstate_response.next_test_due_date.isoformat()
        })
        reinstate_response.result = ReinstateResult.REINSTATED

    return reinstate_response


def filter_by_sort_key(participant_and_results, sort_key):
    return [record for record in participant_and_results if record['sort_key'].startswith(sort_key)]


def remove_no_recall_change_result_combinations(results):
    for result in results:
        if get_concatenated_result_code_combination(result) in ignored_results:
            results.remove(result)
    return results
