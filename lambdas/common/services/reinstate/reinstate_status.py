from nose.tools import nottest
from common.models.participant import ParticipantStatus
from common.utils.result_codes import (
    RECALL_TYPES,
    ABNORMAL_ACTION_CODE_COMBINATIONS_FOR_REINSTATE,
    UNCHANGED_ACTION_CODE_COMBINATIONS_FOR_REINSTATE,
    get_concatenated_result_code_combination
)


@nottest
def calculate_status_from_latest_test_result_for_manual_reinstate(test_results):
    if not test_results:
        return None, None

    statuses = []
    for result_index, test_result in enumerate(test_results):
        concatenated_result_and_action_codes = get_concatenated_result_code_combination(test_result)  # eg X0A
        status = None
        for recall_type, valid_combinations in RECALL_TYPES.items():
            if concatenated_result_and_action_codes in valid_combinations:
                status = recall_type
                break
        if status is None:
            if concatenated_result_and_action_codes in ABNORMAL_ACTION_CODE_COMBINATIONS_FOR_REINSTATE:
                status = ParticipantStatus.REPEAT_ADVISED
            elif concatenated_result_and_action_codes in UNCHANGED_ACTION_CODE_COMBINATIONS_FOR_REINSTATE:
                status = 'Unchanged'
        if status not in [None, 'Unchanged']:
            statuses.append((status, result_index))

    if not len(statuses):
        raise Exception('Participant status cannot be calculated from previous test results.')

    return statuses[0]


@nottest
def calculate_status_from_last_3_test_results_for_manual_reinstate(test_results):
    if not test_results:
        return None

    statuses = []
    for test_result in test_results:
        concatenated_result_and_action_codes = get_concatenated_result_code_combination(test_result)  # eg X0A
        status = None
        for recall_type, valid_combinations in RECALL_TYPES.items():
            if concatenated_result_and_action_codes in valid_combinations:
                status = recall_type
                break
        if status is None and concatenated_result_and_action_codes in ABNORMAL_ACTION_CODE_COMBINATIONS_FOR_REINSTATE:
            status = ParticipantStatus.REPEAT_ADVISED
        if status not in [None, 'Unchanged']:
            statuses.append(status)

    if not len(statuses):
        raise Exception('Participant status cannot be calculated from previous test results.')

    for status in statuses[:3]:
        if status != ParticipantStatus.ROUTINE:
            return status

    return ParticipantStatus.ROUTINE
