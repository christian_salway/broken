from common.services.reinstate.log_references import LogReference
from common.models.participant import CeaseReason
from common.log import log


VALID_REINSTATE_REASONS = [
    CeaseReason.DUE_TO_AGE,
    CeaseReason.AUTOCEASE_DUE_TO_AGE,
    CeaseReason.NO_CERVIX,
    CeaseReason.RADIOTHERAPY,
    CeaseReason.PATIENT_INFORMED_CHOICE,
    CeaseReason.MENTAL_CAPACITY_ACT
]


def can_participant_be_reinstated(participant: dict) -> bool:
    log({'log_reference': LogReference.REINSTATESER0003})

    if not participant.get('active', True):
        log({'log_reference': LogReference.REINSTATESER0004})
        return False

    if participant.get('is_ceased', False):
        log({'log_reference': LogReference.REINSTATESER0006})
        return True

    log({'log_reference': LogReference.REINSTATESER0005})
    return False


def is_valid_cease_reason(cease_reson: str) -> bool:
    try:
        return CeaseReason(cease_reson) in VALID_REINSTATE_REASONS
    except Exception:
        log({'log_reference': LogReference.REINSTATESER0013, 'cease_reason': cease_reson})
        return False
