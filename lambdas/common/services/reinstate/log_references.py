import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    REINSTATESER0001 = (logging.INFO, 'Querying for participant, including test results')
    REINSTATESER0002 = (logging.WARNING, 'Participant not found')
    REINSTATESER0003 = (logging.INFO, 'Checking if participant can be reinstated.')
    REINSTATESER0004 = (
        logging.INFO, 'Participant is not eligable for reinstation as they are deemed out-of-programme.')
    REINSTATESER0005 = (logging.INFO, 'Participant is not eligible for reinstation as they are not ceased.')
    REINSTATESER0006 = (logging.INFO, 'Participant is eligible for reinstation.')
    REINSTATESER0007 = (logging.WARNING, 'No cease records found for participant')
    REINSTATESER0008 = (logging.WARNING, 'Participants cease status is not a valid cease reason for reinstation')
    REINSTATESER0009 = (logging.INFO, 'Constructing participant update record.')
    REINSTATESER0010 = (logging.INFO, 'Constructing participant reinstate record.')
    REINSTATESER0011 = (logging.INFO, 'Transaction write for participant update and put record.')
    REINSTATESER0012 = (logging.INFO, 'Participant reinstated successfully.')
    REINSTATESER0013 = (logging.ERROR, 'Unrecognised cease reason.')
