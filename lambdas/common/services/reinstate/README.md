# Description
This service functionality determines if a participant is eligible for reinstation. If they are, this will then reinstate the participant by creating a `REINSTATE` record and updating the participants next test due date.

# Permissions
This service requires the following permissions to the `participants` table:
- Get
- Write