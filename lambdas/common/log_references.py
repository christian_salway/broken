import logging
from enum import Enum


class BaseLogReference(Enum):

    def __init__(self, level, message):
        self.level = level
        self.message = message


class CommonLogReference(BaseLogReference):

    EXCEPTION000 = (logging.ERROR, 'An unexpected exception has been caught')
    LAMBDA0000 = (logging.INFO, 'Lambda started')
    LAMBDA0001 = (logging.INFO, 'Lambda exited with status success')
    LAMBDA0002 = (logging.ERROR, 'Lambda exited with status failure')
    LAMBDA0003 = (logging.WARNING, 'SQS message does not contain an internal id, creating new one')
    LAMBDA0004 = (logging.WARNING, 'Request does not contain an internal id, creating new one')
    LAMBDA0005 = (logging.WARNING, 'S3 object does not contain an internal id, creating new one')
    LAMBDA0006 = (logging.INFO, 'Identified request as deep ping')
    LAMBDA0007 = (logging.INFO, 'Setting user workgroups from session')
    LAMBDA0008 = (logging.INFO, 'System Lambda - Using all workgroups')
    LAMBDA0009 = (logging.INFO, 'Lambda triggered by step function with internal id.')
    SESSION0001 = (logging.INFO, 'Fetching session item from table.')
    SESSION0002 = (logging.INFO, 'Session found.')
    SESSION0003 = (logging.ERROR, 'Session missing or expired')
    SESSION0004 = (logging.INFO, 'Updating session in dynamo')
    SESSION0005 = (logging.INFO, 'Session updated.')
    SESSION0006 = (logging.INFO, 'Session deleted.')
    SESSION0007 = (logging.INFO, 'Querying for duplicate sessions for user')
    SESSION0008 = (logging.INFO, 'No duplicate sessions found')
    SESSION0009 = (logging.INFO, 'Duplicate sessions for user found. Marking them as invalid')
    SESSION0010 = (logging.INFO, 'Duplicate sessions marked as invalid')
    S3EVENT0000 = (logging.INFO, 'S3 test event received. Lambda exiting.')
    AUDIT0001 = (logging.INFO, 'Successfully sent an audit record to the audit queue')
    ODS0001 = (logging.INFO, 'Connecting to DynamoDB.')
    ODS0002 = (logging.INFO, 'Returning cached organisation data.')
    ODS0003 = (logging.INFO, 'No cached organisation data found.')
    ODS0004 = (logging.INFO, 'Requesting data from Organisation Directory Service.')
    ODS0005 = (logging.INFO, 'Successfully retrieved data from Organisation Directory Service.')
    ODS0006 = (logging.INFO, 'Creating cached record for organisation.')
    ODS0007 = (logging.ERROR, 'Organisation not found in Organisation Directory Service.')
    ODS0008 = (logging.ERROR, 'Error communicating with Organisation Directory Service.')
    ODS0009 = (logging.ERROR, 'Request Error when trying to get data from Organisation Directory Service.')
    ODS0010 = (logging.INFO, 'Determined DMS status for organisation.')
    PARTICIPANT0001 = (logging.INFO, 'Found more than one participant for given NHS number')
    PARTICIPANT0002 = (logging.INFO, 'Auto-match: Single participant with high score and letter address.')
    PARTICIPANT0003 = (logging.INFO, 'Manual-match: Single participant with score 4 but no matching address.')
    PARTICIPANT0004 = (logging.INFO, 'Manual-match: Multiple participants found matching NHS number.')
    PARTICIPANT0005 = (logging.INFO, 'Manual-match: Mutltiple participants found with score 4.')
    PARTICIPANT0006 = (logging.INFO, 'Manual-match: Participant(s) found with score 3.')
    PARTICIPANT0007 = (logging.INFO, 'No participants found to manual match on.')
    PARTICIPANT0008 = (
        logging.INFO,
        'Unable to find potential matches from NHS number. Attempting using DoB, initial and lastname.'
    )
    PARTICIPANT0009 = (logging.INFO, 'Participant match ratios.')
    PARTICIPANT0010 = (logging.INFO, 'Participant match differences.')
    PARTICIPANT0011 = (logging.INFO, 'Rejected result and pausing participant letters and notifications')
    PARTICIPANT0012 = (logging.INFO, 'Pausing participant letters and notifications')
    PARTICIPANT0013 = (logging.INFO, 'Unpausing participant letters and notifications')
    CEASE0001 = (logging.INFO, 'Participant updated')
    CEASE0002 = (logging.INFO, 'GP notification sent')
    CEASE0003 = (logging.INFO, 'Print notification sent')
    CEASE0004 = (logging.INFO, 'No live episodes found')
    CEASE0005 = (logging.INFO, 'Episode updated')
    CEASE0006 = (logging.INFO, 'Cease record created')
    CEASE0007 = (logging.INFO, 'Checked if participant is eligible for auto cease')
    CEASEEX0001 = (logging.ERROR, 'Invalid cease reason')
    RESULTLOCK0001 = (logging.ERROR, 'Failed to acquire/release record lock as no user in the session.')
    RESULTLOCK0002 = (logging.ERROR, 'Failed to acquire record lock as no result_id was supplied in the request body.')
    RESULTLOCK0003 = (logging.INFO, 'Retrieved current record lock state from DynamoDB.')
    RESULTLOCK0004 = (logging.ERROR, 'Failed to find the record to check the lock on.')
    RESULTLOCK0005 = (logging.ERROR, 'Current lock on record is invalid - no timestamp set.')
    RESULTLOCK0006 = (logging.WARNING, 'Cannot lock the record to the current user as it is already locked.')
    RESULTLOCK0007 = (logging.INFO, 'Attempted to set lock on the record.')
    RESULTLOCK0008 = (logging.ERROR, 'Attempt to lock record failed.')
    RESULTLOCK0009 = (
        logging.ERROR,
        'Failed to acquire record lock as no received_time was supplied in the request body.'
    )
    RESULTLOCK0010 = (logging.INFO, 'Successfully locked the record.')
    RESULTLOCK0011 = (logging.INFO, 'Attempting to release the lock on the result.')
    RESULTLOCK0012 = (logging.WARNING, 'Cannot release the record lock as another user has it.')
    RESULTLOCK0013 = (logging.ERROR, 'Attempt to release record lock failed.')
    RESULTLOCK0014 = (logging.INFO, 'Successfully released record lock.')

    # Sender Log References
    SENDER0001 = (logging.INFO, 'Getting sender details from organisation cache')
    SENDER0002 = (logging.INFO, 'Getting sender details from reference table')
    SENDER0003 = (logging.WARNING, 'Invalid source type')
    SENDER0004 = (logging.WARNING, 'Could not retrieve sender details with given parameters')
    VALSEND0001 = (logging.INFO, 'Beginning validation of sender reference')
    VALSEND0002 = (logging.WARNING, 'Sender is inactive')
    VALSEND0003 = (logging.WARNING, 'Sender inactive at test date')
    VALSEND0004 = (logging.WARNING, 'Sender source type invalid')
    VALSEND0005 = (logging.INFO, 'Sender reference is valid')
    VALSEND0006 = (logging.WARNING, 'Exception parsing date for ODS operational periods')

    MESHCLIENT0010 = (logging.INFO, 'Getting mesh shared key')
    MESHCLIENT0011 = (logging.INFO, 'Successfully retrieved mesh shared key')
    MESHCLIENT0012 = (logging.INFO, 'Getting mesh client certificates')
    MESHCLIENT0013 = (logging.INFO, 'Successfully retrieved mesh client certificates. Getting mesh client private keys')
    MESHCLIENT0014 = (logging.INFO, 'Successfully retrieved mesh client private keys')
    MESHCLIENT0015 = (logging.INFO, 'Extracted certificate and private key for mailbox or environment')
    MESHCLIENT0016 = (logging.INFO, 'Storing certificate and key as temporary files')
    MESHCLIENT0017 = (logging.INFO, 'Successfully stored certificate and key as temporary files')
    MESHCLIENT0018 = (logging.INFO, 'Mesh ssl verification is required. Getting CA certificate.')
    MESHCLIENT0019 = (logging.INFO, 'Successfully retrieved CA certificate and stored as temporary files')
    MESHCLIENT0020 = (logging.INFO, 'Initialising mesh client')
    MESHCLIENT0021 = (logging.INFO, 'Sending handshake to mesh')
    MESHCLIENT0022 = (logging.INFO, 'Successfully sent handshake to mesh')
    MESHCLIENT0023 = (logging.INFO, 'Retrieving message from mesh')
    MESHCLIENT0024 = (logging.INFO, 'Successfully retrieved message from mesh. Reading file contents')
    MESHCLIENT0025 = (logging.INFO, 'Successfully read file contents. Closing stream.')

    MATCHRLET0001 = (logging.INFO, 'Checking letter rule for result')
    MATCHRLET0002 = (logging.INFO, 'Sending result letter')
    MATCHRLET0003 = (logging.INFO, 'Placing result letter job on queue URL')
    MATCHRLET0004 = (logging.INFO, 'Suppressing result letter')

    CONFIG0001 = (
        logging.ERROR,
        'POSITIVE_FAILSAFE_REPEAT_MONTHS environment variable not set when attempting to generate NTDD'
    )
    CONFIG0002 = (
        logging.ERROR,
        'NEGATIVE_FAILSAFE_REPEAT_MONTHS environment variable not set when attempting to generate NTDD'
    )
    CONFIG0003 = (
        logging.ERROR,
        'RESULT_LETTER_CUT_OFF_MONTHS environment variable not set when attempting to check cut off'
    )

    # PDS FHIR

    PDSFHIR0001 = (logging.WARNING, 'Failed to find live PDS FHIR access token')
    PDSFHIR0002 = (logging.WARNING, 'Failed to find live PDS FHIR refresh token')
    PDSFHIR0003 = (logging.INFO, 'Attempt to refresh PDS FHIR access token')
    PDSFHIR0004 = (logging.WARNING, 'Failed to refresh PDS FHIR access token')
    PDSFHIR0005 = (logging.INFO, 'PDS FHIR access token successfuly refreshed')
    PDSFHIR0006 = (logging.INFO, 'Starting PDS FHIR search by NHS number')
    PDSFHIR0007 = (logging.WARNING, 'PDS FHIR search by NHS number failed')
    PDSFHIR0008 = (logging.INFO, 'PDS FHIR search by NHS number succeeded')
    PDSFHIR0009 = (logging.WARNING, 'PDS FHIR mapping requested on empty object')
    PDSFHIR0010 = (logging.WARNING, 'Cannot map restricted PDS FHIR response')
    PDSFHIR0011 = (logging.WARNING, 'Failed to map date of birth for PDS FHIR record')
    PDSFHIR0012 = (logging.WARNING, 'Failed to map address for PDS FHIR record')
    PDSFHIR0013 = (logging.WARNING, 'Failed to map GP for PDS FHIR record')
    PDSFHIR0014 = (logging.INFO, 'Attempting to acquire first set of PDS FHIR tokens')
    PDSFHIR0015 = (logging.WARNING, 'Failed to acquire first set of PDS FHIR tokens')
    PDSFHIR0016 = (logging.WARNING, 'Failed to get any relevant demographics information')
    PDSFHIR0017 = (logging.WARNING, 'Registered GP is from an unexpected country')
    PDSFHIR0018 = (logging.WARNING, 'Workgroup does not have access to any country')

    # SFTP log references

    SFTPEX0001 = (logging.ERROR, 'Problem retrieving SFTP Secret')
    SFTPEX0002 = (logging.ERROR, 'Problem creating SFTP Client')

    SFTP0001 = (logging.INFO, 'Generating RSA key from keyfile')
    SFTP0002 = (logging.INFO, 'Creating SSH session from stream socket')
    SFTP0003 = (logging.INFO, 'Negotiating SSH session')
    SFTP0004 = (logging.INFO, 'Session opened successfully')

    # Data segregation
    DATASEG0001 = (logging.WARNING, 'Attempt to access data which is not in an accessible cohort')
    DATASEG0002 = (logging.WARNING, 'Participant table items filtered by data segregation')
    DATASEG0003 = (logging.WARNING, 'Result table items filtered by data segregation')

    # next test due date
    NTDDCALCULATION0001 = (logging.INFO, 'Next test due date set from lab result recall months')
    NTDDCALCULATION0002 = (logging.INFO, 'Next test due date set from lab result infection data')
    NTDDCALCULATION0003 = (logging.INFO, 'Next test due date set from failsafe')
    NTDDCALCULATION0004 = (logging.INFO, 'Next test due date can not be calculated')

    # File download log references
    DOWNLOADFILE0001 = (logging.INFO, 'Session data parsed successfully')
    DOWNLOADFILE0002 = (logging.INFO, 'Confirmation report file obtained and encoded successfully')
    DOWNLOADFILE0003 = (logging.INFO, 'Metadata obtained successfully')
    DOWNLOADFILE0004 = (logging.INFO, 'File name acquired successfully')
    DOWNLOADFILEEX0001 = (logging.ERROR, "Missing session data")
    DOWNLOADFILEEX0002 = (logging.ERROR, "Required workgroup not present to complete request")
    DOWNLOADFILEEX0003 = (logging.ERROR, "Requested file cannot be found")
    DOWNLOADFILEEX0004 = (logging.ERROR, "An uncaught exception has occurred")
    DOWNLOADFILEEX0005 = (logging.ERROR, "The access groups associated with the selected role cannot access this file")
    DOWNLOADFILEEX0006 = (logging.ERROR, "Missing query parameter")
