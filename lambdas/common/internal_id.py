from datetime import datetime, timezone
import uuid


def create_internal_id():
    """
    Creates an internal_id
    """
    DELIMITER = '_'
    process_start_time = datetime.now(timezone.utc)
    internal_id = process_start_time.strftime('%Y%m%d%H%M%S%f')
    internal_id += DELIMITER + str(uuid.uuid4())[:6].upper()
    return internal_id
