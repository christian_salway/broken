from datetime import datetime, timezone
from unittest.case import TestCase

import common.utils.import_migration_data_validation_utils as environment
from common.utils.import_migration_data_validation_utils import (
    ImportMigrationDataValidation as validation_utils)

FIXED_NOW_TIME = datetime(2020, 3, 3, 9, 45, 23, tzinfo=timezone.utc)
DATA_ERROR_KEY = 'data_error'

environment.NOW_DATE = FIXED_NOW_TIME


class TestNextTestDueDateDataValidation(TestCase):

    def test_validate_next_test_due_date_data_is_successful(self):
        # Arrange

        # Act
        response = validation_utils().validate_next_test_due_date('20200325')

        # Assert
        self.assertEqual(None, response)

    def test_validate_next_test_due_date_data_returns_custom_invalid_date_error(self):
        # Arrange
        expected_response = {DATA_ERROR_KEY: 'Date must be between 1988-01-01 and 2025-03-03'}

        # Act
        actual_response = validation_utils().validate_next_test_due_date('20260325')

        # Assert
        self.assertEqual(expected_response, actual_response)


class TestValidateSeqNumberDataValidation(TestCase):

    def test_validate_sequence_number_data_is_successful(self):
        response = validation_utils().validate_sequence_number('123456789')
        self.assertEqual(response, None)

    def test_validate_sequence_number_data_returns_error(self):
        response = validation_utils().validate_sequence_number('0')
        contains_error = DATA_ERROR_KEY in response
        self.assertTrue(contains_error)


class TestRecallTypeDataValidation(TestCase):

    def test_validate_recall_type_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Recall Type not one of the expected values: ['N', 'R', 'I', 'L', 'S', 'C']"
        }

        # Act
        actual_response = validation_utils().validate_recall_type('Y')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestValidateAmendFlagDataValidation(TestCase):

    def test_validate_amend_flag_data_is_successful(self):
        response = validation_utils().validate_amend_flag('AMEND')
        self.assertEqual(response, None)

    def test_validate_amend_flag_data_returns_error(self):
        response = validation_utils().validate_amend_flag('0')
        contains_error = DATA_ERROR_KEY in response
        self.assertTrue(contains_error)


class TestRecallStatDataValidation(TestCase):

    def test_validate_recall_stat_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Recall Status not one of the expected values: " +
            "['1', 'G', 'N', 'P', '2', 'F', 'H', 'L', 'S', 'C', 'Z', '-']"
        }

        # Act
        actual_response = validation_utils().validate_recall_stat('Y')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestPosDoubtDataValidation(TestCase):

    def test_validate_pos_doubt_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Pos Doubt not one of the expected values: ['Y', 'N']"
        }

        # Act
        actual_response = validation_utils().validate_pos_doubt('M')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestFNRCountDataValidation(TestCase):

    def test_validate_fnr_count_data_is_successful(self):
        # Act
        response = validation_utils().validate_fnr_count(19)

        # Assert
        self.assertEqual(response, None)

    def test_validate_fnr_count_data_when_missing_returns_none(self):
        # Act
        response = validation_utils().validate_fnr_count()

        # Assert
        self.assertEqual(response, None)

    def test_validate_fnr_count_data_returns_custom_invalid_count_error(self):
        # Arrange
        expected_response = {DATA_ERROR_KEY: 'FNR Count must be between 0 and 20 (inclusive)'}

        # Act
        actual_response = validation_utils().validate_fnr_count(21)

        # Assert
        self.assertEqual(expected_response, actual_response)


class TestLDNDataValidation(TestCase):

    def test_validate_ldn_data_data_is_successful(self):
        # Act
        response = validation_utils().validate_ldn('L1')

        # Assert
        self.assertEqual(response, None)

    def test_validate_ldn_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "LDN not one of the expected values: " +
            "['L1', 'L2', 'L3', 'L4', 'D1', 'D2']"
        }

        # Act
        actual_response = validation_utils().validate_ldn('B1')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestGenderDataValidation(TestCase):

    def test_validate_gender_data_data_is_successful(self):
        # Act
        response = validation_utils().validate_gender('M')

        # Assert
        self.assertEqual(response, None)

    def test_validate_gender_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Gender not one of the expected values: " +
            "['M', 'F', 'I']"
        }

        # Act
        actual_response = validation_utils().validate_gender('B1')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestTitleDataValidation(TestCase):

    def test_validate_title_data_is_successful(self):
        # Act
        response = validation_utils().validate_title('MR')

        # Assert
        self.assertEqual(response, None)

    def test_validate_title_lowercase_data_is_successful(self):
        # Act
        response = validation_utils().validate_title('Mr')

        # Assert
        self.assertEqual(response, None)

    def test_validate_title_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Title not one of the expected values: " +
            "['MRS', 'MISS', 'MS', 'DR', 'PROF', 'REV', 'LADY', 'DAME', 'MR', 'SIR', 'CAPT', 'COL', 'CMDR', 'LORD']"
        }

        # Act
        actual_response = validation_utils().validate_title('B1')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestTestResultCodeDataValidation(TestCase):

    def test_validate_test_result_code_data_is_successful(self):
        # Act
        response = validation_utils().validate_test_result_code('0')

        # Assert
        self.assertEqual(response, None)

    def test_validate_test_result_code_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Test result code not one of the expected values: " +
            "['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'B', 'E', 'M', 'N', 'G', 'X']"
        }

        # Act
        actual_response = validation_utils().validate_test_result_code('B1')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestRepeatMonthsDataValidation(TestCase):

    def test_validate_repeat_months_data_is_successful(self):
        # Act
        response = validation_utils().validate_repeat_months('12')

        # Assert
        self.assertEqual(response, None)

    def test_validate_repeat_months_no_data_is_successful(self):
        # Act
        response = validation_utils().validate_repeat_months('--')

        # Assert
        self.assertEqual(response, None)

    def test_validate_repeat_months_data_returns_invalid_data_input(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Repeat months can only contain numbers or be --"
        }

        # Act
        actual_response = validation_utils().validate_repeat_months('61s')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestLabNationalCodeDataValidation(TestCase):

    def test_validate_lab_national_code_data_is_successful(self):
        # Act
        response = validation_utils().validate_lab_national_code('12')

        # Assert
        self.assertEqual(response, None)

    def test_validate_lab_national_code_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Lab national code can only contain numbers through 0 - 9"
        }

        # Act
        actual_response = validation_utils().validate_lab_national_code('a')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestSenderExistsDataValidation(TestCase):

    def test_validate_sender_exists_positive_data_is_successful(self):
        # Act
        response = validation_utils().validate_sender_exists('Y')

        # Assert
        self.assertEqual(response, None)

    def test_validate_sender_exists_negative_data_is_successful(self):
        # Act
        response = validation_utils().validate_sender_exists('')

        # Assert
        self.assertEqual(response, None)

    def test_validate_sender_exists_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Sender exists must be empty or Y"
        }

        # Act
        actual_response = validation_utils().validate_sender_exists('N')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestSlideNumberDataValidation(TestCase):

    def test_validate_slide_number_data_is_successful(self):
        # Act
        response = validation_utils().validate_slide_number('26481765')

        # Assert
        self.assertEqual(response, None)

    def test_validate_slide_number_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Slide number can only contain numbers through 0 - 9"
        }

        # Act
        actual_response = validation_utils().validate_slide_number('0987354A')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestActionCodeDataValidation(TestCase):

    def test_validate_action_code_data_is_successful(self):
        # Act
        response = validation_utils().validate_action_code('R')

        # Assert
        self.assertEqual(response, None)

    def test_validate_action_code_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Action code not one of the expected values: " +
                            "['R', 'A', 'S', 'H']"
        }

        # Act
        actual_response = validation_utils().validate_action_code('B')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestInfectionCodeDataValidation(TestCase):

    def test_validate_infection_code_data_is_successful(self):
        # Act
        response = validation_utils().validate_infection_code('0')

        # Assert
        self.assertEqual(response, None)

    def test_validate_infection_code_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Infection code not one of the expected values: " +
                            "['0', '9', 'U', 'Q', '1', '2', '3', '4', '5', '6', '7']"
        }

        # Act
        actual_response = validation_utils().validate_infection_code('B')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestSourceCodeDataValidation(TestCase):

    def test_validate_source_code_data_is_successful(self):
        # Act
        response = validation_utils().validate_source_code('G')

        # Assert
        self.assertEqual(response, None)

    def test_validate_source_code_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Source code not one of the expected values: " +
                            "['G', 'H', 'X', 'N']"
        }

        # Act
        actual_response = validation_utils().validate_source_code('B')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestReasonDataValidation(TestCase):

    def test_validate_reason_data_is_successful(self):
        # Act
        response = validation_utils().validate_reason('1')

        # Assert
        self.assertEqual(response, None)

    def test_validate_reason_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Reason not one of the expected values: " +
                            "['1', '2', '3', '4', '5', '10', '11', '6', '7', '8', '9', '77', '99', '9C']"
        }

        # Act
        actual_response = validation_utils().validate_reason('0')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestCeaseCodeDataValidation(TestCase):

    def test_validate_cease_code_data_is_successful(self):
        # Act
        response = validation_utils().validate_cease_code('C')

        # Assert
        self.assertEqual(response, None)

    def test_validate_cease_code_data_returns_custom_unexpected_value_error(self):
        # Arrange
        expected_response = {
            DATA_ERROR_KEY: "Cease code not one of the expected values: " +
                            "['C', 'D', 'F', 'N', 'Y', 'G', 'H']"
        }

        # Act
        actual_response = validation_utils().validate_cease_code('Z')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestTextDataValidation(TestCase):

    def test_validate_text_data_is_successful(self):
        # Act
        response = validation_utils().validate_text('A valid piece of text')

        # Assert
        self.assertEqual(response, None)

    def test_validate_text_data_with_brackets_is_successful(self):
        # Act
        response = validation_utils().validate_text('Valid text as per [CERSS-1765]')

        # Assert
        self.assertEqual(response, None)

    def test_validate_text_data_returns_custom_invalid_text_error(self):
        # Arrange
        expected_response = {DATA_ERROR_KEY: 'Note text can only contains letters, numbers and punctuation'}

        # Act
        actual_response = validation_utils().validate_text('🛑')

        # Assert
        self.assertEqual(expected_response, actual_response)


class TestOAPDDataValidation(TestCase):

    def test_oapd_data_is_successful(self):
        # Act
        response = validation_utils().validate_oapd('20200302')

        # Assert
        self.assertEqual(response, None)

    def test_validate_oap_data_returns_custom_invalid_date_error_above_maximum_limit(self):
        # Arrange
        expected_response = {DATA_ERROR_KEY: 'Date must be between 1948-01-01 and 2020-03-03'}

        # Act
        actual_response = validation_utils().validate_oapd('20260325')

        # Assert
        self.assertEqual(expected_response, actual_response)

    def test_validate_oap_data_returns_custom_invalid_date_error_below_minimum_limit(self):
        # Arrange
        expected_response = {DATA_ERROR_KEY: 'Date must be between 1948-01-01 and 2020-03-03'}

        # Act
        actual_response = validation_utils().validate_oapd('19470325')

        # Assert
        self.assertEqual(expected_response, actual_response)


class TestRemdateDataValidation(TestCase):

    def test_remdate_data_is_successful(self):
        # Act
        response = validation_utils().validate_remdate('20200302')

        # Assert
        self.assertEqual(response, None)

    def test_validate_remdate_data_returns_custom_invalid_date_error(self):
        # Arrange
        expected_response = {DATA_ERROR_KEY: 'Date must be between 1988-01-01 and 2020-03-03'}

        # Act
        actual_response = validation_utils().validate_remdate('20260325')

        # Assert
        self.assertEqual(expected_response, actual_response)


class TestDOBDataValidation(TestCase):

    def test_dob_data_is_successful(self):
        # Act
        response = validation_utils().validate_dob('19960115')

        # Assert
        self.assertEqual(response, None)

    def test_validate_dob_data_returns_custom_invalid_date_error(self):
        # Arrange
        expected_response = {DATA_ERROR_KEY: 'Date must be between 1880-01-01 and 2020-03-03'}

        # Act
        actual_response = validation_utils().validate_dob('18791231')

        # Assert
        self.assertEqual(expected_response, actual_response)


class TestGPCodeAndGPPracticeCodeValidation(TestCase):

    def test_validate_gp_and_gp_pracitce_data_is_successful(self):
        # Act
        response = validation_utils().validate_gp_or_practice_code('P12345')

        # Assert
        self.assertEqual(response, None)

    def test_validate_GP_data_returns_custom_invalid_text_error(self):
        # Arrange
        expected_response = {DATA_ERROR_KEY: 'GP code and GP practice code can only contains letters and numbers'}

        # Act
        actual_response = validation_utils().validate_gp_or_practice_code('AB_12')

        # Assert
        self.assertEqual(expected_response, actual_response)


class TestDestinationDataValidation(TestCase):

    def test_validate_destination_data_is_successful(self):
        # Act
        response = validation_utils().validate_destination('ABC')

        # Assert
        self.assertEqual(response, None)

    def test_validate_destination_data_returns_custom_invalid_text_error(self):
        # Arrange
        expected_response = {DATA_ERROR_KEY: 'Destination must contain uppercase letters only'}

        # Act
        actual_response = validation_utils().validate_destination('AB_12')

        # Assert
        self.assertEqual(expected_response, actual_response)


class TestOptionalDateValidation(TestCase):

    def test_validate_optional_date_no(self):
        # Act
        response = validation_utils().validate_optional_date('')

        # Assert
        self.assertEqual(response, None)

    def test_validate_optional_date_valid_date(self):
        # Act
        response = validation_utils().validate_optional_date('19990713')

        # Assert
        self.assertEqual(response, None)


class TestDateWithDaysAfter1970Validation(TestCase):
    def test_validate_date_empty_string(self):
        # Act
        response = validation_utils().validate_date_with_days_after_1970('')

        # Assert
        self.assertEqual(None, response)

    def test_validate_date_just_dashes(self):
        # Act
        response = validation_utils().validate_date_with_days_after_1970('----')

        # Assert
        self.assertEqual(None, response)

    def test_validate_date_invalid_date_in_past(self):
        # Act
        response = validation_utils().validate_date_with_days_after_1970('19691231')

        # Assert
        self.assertEqual({'data_error': 'Date must be between 1970-01-01 and 2020-03-03'}, response)

    def test_validate_date_invalid_date_in_future(self):
        # Act
        response = validation_utils().validate_date_with_days_after_1970('20991231')

        # Assert
        self.assertEqual({'data_error': 'Date must be between 1970-01-01 and 2020-03-03'}, response)

    def test_validate_date_invalid_date_format(self):
        # Act
        response = validation_utils().validate_date_with_days_after_1970('200901')

        # Assert
        self.assertEqual({'data_error': 'Invalid date format. Expected format to be %Y%m%d'}, response)

    def test_validate_date_valid_date(self):
        # Act
        response = validation_utils().validate_date_with_days_after_1970('20090701')

        # Assert
        self.assertEqual(response, None)


class TestDateWithoutDaysAfter1970Validation(TestCase):
    def test_validate_date_empty_string(self):
        # Act
        response = validation_utils().validate_date_without_days_after_1970('')

        # Assert
        self.assertEqual(None, response)

    def test_validate_date_just_dashes(self):
        # Act
        response = validation_utils().validate_date_without_days_after_1970('----')

        # Assert
        self.assertEqual(None, response)

    def test_validate_date_invalid_date_in_past(self):
        # Act
        response = validation_utils().validate_date_without_days_after_1970('196912')

        # Assert
        self.assertEqual({'data_error': 'Date must be between 1970-01-01 and 2020-03-03'}, response)

    def test_validate_date_invalid_date_in_future(self):
        # Act
        response = validation_utils().validate_date_without_days_after_1970('209912')

        # Assert
        self.assertEqual({'data_error': 'Date must be between 1970-01-01 and 2020-03-03'}, response)

    def test_validate_date_invalid_date_format(self):
        # Act
        response = validation_utils().validate_date_without_days_after_1970('20090101')

        # Assert
        self.assertEqual({'data_error': 'Invalid date format. Expected format to be %Y%m'}, response)

    def test_validate_date_valid_date(self):
        # Act
        response = validation_utils().validate_date_without_days_after_1970('200907')

        # Assert
        self.assertEqual(response, None)


class TestHPVValidation(TestCase):
    def test_validate_hpv_empty_string(self):
        # Act
        response = validation_utils().validate_hpv_value('')

        # Assert
        self.assertEqual(None, response)

    def test_validate_hpv_invalid_value(self):
        # Arrange
        expected_response = {'data_error': "HPV not one of the expected values: ['Y', 'N']"}

        # Act
        actual_response = validation_utils().validate_hpv_value('H')

        # Assert
        self.assertEqual(expected_response, actual_response)

    def test_validate_valid_hpv_value_of_Y(self):
        # Act
        response = validation_utils().validate_hpv_value('Y')

        # Assert
        self.assertEqual(None, response)

    def test_validate_valid_hpv_value_of_N(self):
        # Act
        response = validation_utils().validate_hpv_value('N')

        # Assert
        self.assertEqual(None, response)
