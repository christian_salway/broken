import unittest

from ddt import data, ddt
from mock import patch, call

import common.utils.sender_utils as sender_utils
from common.log_references import CommonLogReference


@ddt
class TestSenderUtils(unittest.TestCase):

    @patch('common.utils.sender_utils.log')
    @patch('common.utils.sender_utils.get_organisation_information_by_organisation_code')
    def test_get_sender_details_from_org_cache(self, get_org_info_mock, log_mock):
        NHAIS_cipher = 'WIG'
        sender_code = '714522'
        sender_source_type = '000001'
        test_date = '2020-01-01'
        get_org_info_mock.return_value = {
            'organisation_code': '714522',
            'active': True,
            'address': "Address Mock",
            'name': "GP Name Mock",
            'last_updated': "2021-01-01",
            'gp_practice_status': True,
            'operational_periods': [{
                "start": "2019-01-01"
            }]
        }
        expected_sender_details = {
            'organisation_code': '714522',
            'active': True,
            'address': "Address Mock",
            'name': "GP Name Mock",
            'last_updated': "2021-01-01",
            'gp_practice_status': True,
            'operational_periods': [{
                "start": "2019-01-01"
            }]
        }
        expected_log_calls = [
            call({'log_reference': CommonLogReference.SENDER0001}),
            call({'log_reference': CommonLogReference.VALSEND0001}),
            call({'log_reference': CommonLogReference.VALSEND0005})
        ]

        actual_response = sender_utils.get_sender_details_and_validate(
            NHAIS_cipher, sender_code, sender_source_type, test_date)

        log_mock.assert_has_calls(expected_log_calls)
        get_org_info_mock.assert_called_with(sender_code)
        self.assertEqual(expected_sender_details, actual_response)

    @patch('common.utils.sender_utils.log')
    @patch('common.utils.sender_utils.get_organisation_information_by_organisation_code')
    def test_get_sender_details_from_org_cache_not_found(self, get_org_info_mock, log_mock):
        # Arrange
        NHAIS_cipher = 'WIG'
        sender_code = '714522'
        sender_source_type = '000001'
        test_date = '2020-01-01'
        organisation_code = 'A456'
        get_org_info_mock.return_value = {
            'organisation_code': organisation_code,
            'message': f'No organisation found with code {organisation_code}'
        }
        expected_response = {
            'message': 'Could not retrieve sender details with given parameters',
            'sender_details': {'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '000001'},
            'failure_message': 'Sender not found'
        }
        expected_log_calls = [
            call({'log_reference': CommonLogReference.SENDER0001}),
            call({'log_reference': CommonLogReference.VALSEND0001}),
            call({
                'log_reference': CommonLogReference.SENDER0004,
                'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '000001'
            })
        ]

        # Act
        actual_response = sender_utils.get_sender_details_and_validate(
            NHAIS_cipher, sender_code, sender_source_type, test_date)

        # Assert
        log_mock.assert_has_calls(expected_log_calls)
        get_org_info_mock.assert_called_with(sender_code)
        self.assertEqual(actual_response, expected_response)

    @patch('common.utils.sender_utils.log')
    @patch('common.utils.sender_utils.get_organisation_information_by_organisation_code')
    def test_get_sender_details_from_org_cache_error_retrieving(self, get_org_info_mock, log_mock):
        # Arrange
        self.maxDiff = None
        NHAIS_cipher = 'WIG'
        sender_code = '714522'
        sender_source_type = '000001'
        test_date = '2020-01-01'
        organisation_code = 'A456'
        get_org_info_mock.return_value = {
            'organisation_code': organisation_code,
            'message': 'Error retrieving organisation details'
        }
        expected_response = {
            'message': 'Could not retrieve sender details with given parameters',
            'sender_details': {'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '000001'},
            'failure_message': 'Sender not found'
        }
        expected_log_calls = [
            call({'log_reference': CommonLogReference.SENDER0001}),
            call({'log_reference': CommonLogReference.VALSEND0001}),
            call({
                'log_reference': CommonLogReference.SENDER0004,
                'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '000001'
            })
        ]

        # Act
        actual_response = sender_utils.get_sender_details_and_validate(
            NHAIS_cipher, sender_code, sender_source_type, test_date)

        # Assert
        log_mock.assert_has_calls(expected_log_calls)
        get_org_info_mock.assert_called_with(sender_code)
        self.assertEqual(actual_response, expected_response)

    @patch('common.utils.sender_utils.log')
    @patch('common.utils.sender_utils.get_reference_by_cipher_and_key')
    def test_get_sender_details_from_reference(self, get_reference_mock, log_mock):
        # Arrange
        NHAIS_cipher = 'WIG'
        sender_code = '714522'
        sender_source_type = '000005'
        test_date = '2020-01-01'
        expected_sender_details = {
            'active': True
        }
        get_reference_mock.return_value = expected_sender_details
        key = 'WIG#Sender#714522'
        expected_log_calls = [
            call({'log_reference': CommonLogReference.SENDER0002}),
            call({'log_reference': CommonLogReference.VALSEND0001}),
            call({'log_reference': CommonLogReference.VALSEND0005})
        ]

        # Act
        actual_response = sender_utils.get_sender_details_and_validate(
            NHAIS_cipher, sender_code, sender_source_type, test_date)

        # Assert
        log_mock.assert_has_calls(expected_log_calls)
        get_reference_mock.assert_called_with(NHAIS_cipher, key)
        self.assertEqual(actual_response, expected_sender_details)

    @patch('common.utils.sender_utils.log')
    @patch('common.utils.sender_utils.get_organisation_information_by_organisation_code')
    @patch('common.utils.sender_utils.get_reference_by_cipher_and_key')
    def test_get_sender_details_from_invalid_reference(self, get_reference_mock, org_mock, log_mock):
        self.maxDiff = None
        NHAIS_cipher = 'WIG'
        sender_code = '714522'
        sender_source_type = '000009'
        test_date = '2020-01-01'
        expected_sender_details = {
            'message': 'Could not retrieve sender details with given parameters',
            'sender_details': {'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '000009'},
            'failure_message': 'Sender not found'
        }
        actual_response = sender_utils.get_sender_details_and_validate(
            NHAIS_cipher, sender_code, sender_source_type, test_date)

        expected_log_calls = [
            call({'log_reference': CommonLogReference.SENDER0003, 'sender_source_type': '000009'}),
            call({
                'log_reference': CommonLogReference.SENDER0004, 'NHAIS_cipher': 'WIG',
                'sender_code': '714522', 'sender_source_type': '000009'
            })
        ]
        log_mock.assert_has_calls(expected_log_calls)
        org_mock.assert_not_called()
        get_reference_mock.assert_not_called()
        get_reference_mock.assert_not_called()
        self.assertEqual(actual_response, expected_sender_details)

    @patch('common.utils.sender_utils.log')
    @patch('common.utils.sender_utils.get_organisation_information_by_organisation_code')
    def test_fail_to_get_sender_details(self, get_org_info_mock, log_mock):
        NHAIS_cipher = 'WIG'
        sender_code = '714522'
        sender_source_type = '1'
        test_date = '2020-01-01'
        get_org_info_mock.return_value = ''
        expected_sender_details = {
            'message': 'Could not retrieve sender details with given parameters',
            'sender_details': {'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '1'},
            'failure_message': 'Sender not found'
        }

        actual_response = sender_utils.get_sender_details_and_validate(
            NHAIS_cipher, sender_code, sender_source_type, test_date)

        expected_log_calls = [
            call({'log_reference': CommonLogReference.SENDER0001}),
            call({
                'log_reference': CommonLogReference.SENDER0004,
                'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '1'
            })
        ]
        log_mock.assert_has_calls(expected_log_calls)
        get_org_info_mock.assert_called_with(sender_code)
        self.assertEqual(actual_response, expected_sender_details)

    @patch('common.utils.sender_utils.log')
    @patch('common.utils.sender_utils.get_reference_by_cipher_and_key')
    def test_sender_reference_is_valid(self, get_reference_mock, log_mock):
        # Arrange
        NHAIS_cipher = 'WIG'
        sender_code = '714522'
        key = f'{NHAIS_cipher}#Sender#{sender_code}'
        sender_source_type = '2'
        test_date = '2020-01-01'
        expected_reference_details = {
            'active': True
        }
        get_reference_mock.return_value = expected_reference_details

        expected_log_calls = [
            call({'log_reference': CommonLogReference.SENDER0002})
        ]

        # Act
        actual_response = sender_utils.get_sender_details_and_validate(
            NHAIS_cipher, sender_code, sender_source_type, test_date)

        # Assert
        log_mock.assert_has_calls(expected_log_calls)
        get_reference_mock.assert_called_with(NHAIS_cipher, key)
        self.assertEqual(actual_response, expected_reference_details)

    @patch('common.utils.sender_utils.log')
    @patch('common.utils.sender_utils.get_reference_by_cipher_and_key')
    def test_sender_reference_is_invalid_with_reference_subtype_not_equal_to_source_type(
            self, get_reference_mock, log_mock):
        # Arrange
        NHAIS_cipher = 'WIG'
        sender_code = '714522'
        key = f'{NHAIS_cipher}#Sender#{sender_code}'
        sender_source_type = '2'
        test_date = '2020-01-01'
        expected_response = {
            'message': 'Could not retrieve sender details with given parameters',
            'sender_details': {'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '2'},
            'failure_message': 'Sender source type invalid'
        }
        get_reference_mock.return_value = {
            'active': True,
            'reference_subtype': '9'
        }

        expected_log_calls = [
            call({'log_reference': CommonLogReference.SENDER0002}),
            call({'log_reference': CommonLogReference.VALSEND0001}),
            call({'log_reference': CommonLogReference.VALSEND0004}),
            call({
                'log_reference': CommonLogReference.SENDER0004,
                'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '2'
            })
        ]

        # Act
        actual_response = sender_utils.get_sender_details_and_validate(
            NHAIS_cipher, sender_code, sender_source_type, test_date)

        # Assert
        log_mock.assert_has_calls(expected_log_calls)
        get_reference_mock.assert_called_with(NHAIS_cipher, key)
        self.assertEqual(actual_response, expected_response)

    @patch('common.utils.sender_utils.log')
    @patch('common.utils.sender_utils.get_reference_by_cipher_and_key')
    def test_sender_reference_is_invalid_with_inactive_date_older_than_test_date(self, get_reference_mock, log_mock):
        # Arrange
        NHAIS_cipher = 'WIG'
        sender_code = '714522'
        key = f'{NHAIS_cipher}#Sender#{sender_code}'
        sender_source_type = '2'
        test_date = '2020-01-01'
        expected_response = {
            'message': 'Could not retrieve sender details with given parameters',
            'sender_details': {'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '2'},
            'failure_message': 'Sender inactive at test date'
        }
        get_reference_mock.return_value = {
            'active': False,
            'inactive_from': '2019-10-23'
        }

        expected_log_calls = [
            call({'log_reference': CommonLogReference.SENDER0002}),
            call({'log_reference': CommonLogReference.VALSEND0001}),
            call({'log_reference': CommonLogReference.VALSEND0003}),
            call({
                'log_reference': CommonLogReference.SENDER0004,
                'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '2'
            })
        ]

        # Act
        actual_response = sender_utils.get_sender_details_and_validate(
            NHAIS_cipher, sender_code, sender_source_type, test_date)

        # Assert
        log_mock.assert_has_calls(expected_log_calls)
        get_reference_mock.assert_called_with(NHAIS_cipher, key)
        self.assertEqual(actual_response, expected_response)

    @patch('common.utils.sender_utils.log')
    @patch('common.utils.sender_utils.get_reference_by_cipher_and_key')
    def test_sender_reference_not_found(self, get_reference_mock, log_mock):
        # Arrange
        NHAIS_cipher = 'WIG'
        sender_code = '714522'
        key = f'{NHAIS_cipher}#Sender#{sender_code}'
        sender_source_type = '2'
        test_date = '2020-01-01'
        expected_response = {
            'message': 'Could not retrieve sender details with given parameters',
            'sender_details': {
                'NHAIS_cipher': 'WIG',
                'sender_code': '714522',
                'sender_source_type': '2'
            },
            'failure_message': 'Sender not found'
        }
        get_reference_mock.return_value = None

        expected_log_calls = [
            call({'log_reference': CommonLogReference.SENDER0002}),
            call({
                'log_reference': CommonLogReference.SENDER0004,
                'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'sender_source_type': '2'
            })
        ]

        # Act
        actual_response = sender_utils.get_sender_details_and_validate(
            NHAIS_cipher, sender_code, sender_source_type, test_date)

        # Assert
        log_mock.assert_has_calls(expected_log_calls)
        get_reference_mock.assert_called_with(NHAIS_cipher, key)
        self.assertEqual(actual_response, expected_response)

    @data(
        {
            'scenario': 'No operational periods',
            'operational_periods': [],
            'test_date': '2020-01-01',
            'expected': False
        },
        {
            'scenario': 'Opened on day test was taken',
            'operational_periods': [{'start': '2020-01-01'}],
            'test_date': '2020-01-01',
            'expected': True
        },
        {
            'scenario': 'Opened and closed on day test was taken',
            'operational_periods': [{'start': '2020-01-01', 'end': '2020-01-01'}],
            'test_date': '2020-01-01',
            'expected': True
        },
        {
            'scenario': 'Opened a year earlier',
            'operational_periods': [{'start': '2019-01-01'}],
            'test_date': '2020-01-01',
            'expected': True
        },
        {
            'scenario': 'Closed before test was taken',
            'operational_periods': [{'start': '2019-01-01', 'end': '2019-11-01'}],
            'test_date': '2020-01-01',
            'expected': False
        },
        {
            'scenario': 'Closed after test was taken',
            'operational_periods': [{'start': '2019-01-01', 'end': '2020-01-02'}],
            'test_date': '2020-01-01',
            'expected': True
        },
        {
            'scenario': 'Opened, closed, and re-opened before test was taken',
            'operational_periods': [
                {'start': '2019-01-01', 'end': '2019-12-01'},
                {'start': '2020-01-01'}
            ],
            'test_date': '2020-01-01',
            'expected': True
        },
        {
            'scenario': 'Opened, closed, opened, then closed before test was taken',
            'operational_periods': [
                {'start': '2019-01-01', 'end': '2019-12-01'},
                {'start': '2019-12-02', 'end': '2019-12-31'}
            ],
            'test_date': '2020-01-01',
            'expected': False
        },
        {
            'scenario': 'Organisation with only end date',
            'operational_periods': [
                {'end': '2019-12-01'},
            ],
            'test_date': '2015-01-01',
            'expected': False
        },
        {
            'scenario': 'Organisation with invalid data',
            'operational_periods': [
                {'start': 12, 'end': '2019-12-01'},
            ],
            'test_date': '2015-01-01',
            'expected': False
        },
        {
            'scenario': 'Organisation with invalid data and valid date',
            'operational_periods': [
                {'start': 12, 'end': '2019-12-01'},
                {'start': '2011-03-01', 'end': '2019-12-01'},
            ],
            'test_date': '2015-01-01',
            'expected': True
        },
    )
    @patch('common.utils.sender_utils.log')
    def test_sender_details_with_valid_operational_periods(self, data, log_mock):

        self.shortDescription = data["scenario"]

        result = sender_utils._sender_operational_at_date_of_test(
            {'operational_periods': data["operational_periods"]}, data["test_date"])

        self.assertEqual(result, data["expected"])

    @patch('common.utils.sender_utils.log')
    def test__sender_operational_at_date_of_test_exception(self, log_mock):
        sender_details = {'operational_periods': [
            {'start': '2019-01-01', 'end': '2019-12-01'},
            {'start': '2019-12-02', 'end': '2019-12-31'}
        ]}

        result = sender_utils._sender_operational_at_date_of_test(
            sender_details, "invalid")

        self.assertEqual(result, False)
        log_mock.assert_called_with(
            {'log_reference': CommonLogReference.VALSEND0006,
             'exception': "invalid literal for int() with base 10: b'inva'"})

    @patch('common.utils.sender_utils._sender_is_gp_practice')
    @patch('common.utils.sender_utils._sender_operational_at_date_of_test')
    def test__validated_sender_type_1_when_not_gp_practice(
        self,
        test_date_check_mock,
        gp_practice_check_mock,
    ):
        # Arrange
        gp_practice_check_mock.return_value = False
        test_date_check_mock.return_value = True
        sender_details_fixture = {}
        test_date_fixture = "2020-01-01"

        # Act
        result = sender_utils._validated_sender_type_1(sender_details_fixture, test_date_fixture)

        # Assert
        self.assertEqual(result, (False, "Sender is not GP practice"))

    @patch('common.utils.sender_utils._sender_is_gp_practice')
    @patch('common.utils.sender_utils._sender_operational_at_date_of_test')
    def test__validated_sender_type_1_when_closed_on_test_date(
        self,
        test_date_check_mock,
        gp_practice_check_mock,
    ):
        print("todo")
        # Arrange
        gp_practice_check_mock.return_value = True
        test_date_check_mock.return_value = False
        sender_details_fixture = {
            'gp_practice_status': True,
            'operational_periods': [
                {'start': '2010-01-01', 'end': '2011-01-01'},
            ],
        }
        test_date_fixture = "2020-01-01"

        # Act
        result = sender_utils._validated_sender_type_1(sender_details_fixture, test_date_fixture)

        # Assert
        self.assertEqual(result, (False, 'Sender inactive at test date'))

    @patch('common.utils.sender_utils._sender_is_gp_practice')
    @patch('common.utils.sender_utils._sender_operational_at_date_of_test')
    def test__validated_sender_type_1_catch_all(
        self,
        test_date_check_mock,
        gp_practice_check_mock,
    ):
        # Arrange
        gp_practice_check_mock.return_value = True
        test_date_check_mock.return_value = True
        sender_details_fixture = {}
        test_date_fixture = "2020-01-01"

        # Act
        result = sender_utils._validated_sender_type_1(sender_details_fixture, test_date_fixture)

        # Assert
        self.assertEqual(result, (True, None))
