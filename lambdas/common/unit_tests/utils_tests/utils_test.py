import json
import types
from datetime import datetime, timezone
from unittest import TestCase
from mock import patch, MagicMock
from common.utils import (
    increment_date_by_years, chunk_list, set_internal_id,
)
from common.test_mocks.mock_events import (
    sns_mock_event, s3_mock_event, mock_api_event,
    cloudwatch_mock_event, sqs_mock_event
)

module = 'common.utils'


@patch('common.utils.log', MagicMock())
class TestUtils(TestCase):

    def test_increment_date_by_0_years(self, *args):
        today = datetime.now(timezone.utc).date()
        test_data = {
            'date': 'Today is the [Today]'
        }
        expected_data = {
            'date': f'Today is the {today}'
        }
        response = increment_date_by_years(json.dumps(test_data))
        self.assertEqual(expected_data, response)

    def test_increment_date_by_5_years(self, *args):
        increment_amount = int(5)
        today = datetime.now(timezone.utc).date()
        date_plus_5 = datetime(today.year + increment_amount, today.month, today.day).date()
        test_data = {
            'date': f'Today is the [Today+{increment_amount}]'
        }
        expected_data = {
            'date': f'Today is the {date_plus_5}'
        }
        response = increment_date_by_years(json.dumps(test_data))
        self.assertEqual(expected_data, response)

    def test_chunk_list_returns_generator_given_an_empty_list(self, *args):
        list_to_chunk = []
        chunk_size = 5
        expected_chunks = []
        actual_chunk_generator = chunk_list(list_to_chunk, chunk_size)
        self.assertIsInstance(actual_chunk_generator, types.GeneratorType)
        actual_chunks = list(actual_chunk_generator)
        self.assertEqual(expected_chunks, actual_chunks)

    def test_chunk_list_returns_generator_that_splits_list_into_smaller_chunks(self, *args):
        list_to_chunk = ['1', '2', '3']
        chunk_size = 2
        expected_chunks = [['1', '2'], ['3']]
        actual_chunk_generator = chunk_list(list_to_chunk, chunk_size)
        self.assertIsInstance(actual_chunk_generator, types.GeneratorType)
        actual_chunks = list(actual_chunk_generator)
        self.assertEqual(expected_chunks, actual_chunks)

    def test_chunk_list_defaults_chunk_size_to_10(self, *args):
        list_to_chunk = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
        chunk_size = 10
        expected_chunks = [['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'], ['11', '12']]
        actual_chunk_generator = chunk_list(list_to_chunk, chunk_size)
        self.assertIsInstance(actual_chunk_generator, types.GeneratorType)
        actual_chunks = list(actual_chunk_generator)
        self.assertEqual(expected_chunks, actual_chunks)


class TestSetInternalId(TestCase):

    def setUp(self):
        self.update_logger_metadata_fields_patcher = patch(f'{module}.update_logger_metadata_fields').start()
        self.create_internal_id_patcher = patch(f'{module}.create_internal_id').start()

        self.create_internal_id_patcher.return_value = 'internal_id'

    def test_set_internal_id_generates_new_id_for_a_starting_lambda(self):
        # Arrange
        mock_event = {}
        # Act
        set_internal_id(mock_event)
        # Assert
        self.update_logger_metadata_fields_patcher.assert_called_once_with({
            'internal_id': 'internal_id'
        })

    def test_set_internal_id_generates_new_id_from_s3_trigger(self):
        # Arrange
        mock_event = s3_mock_event({
            'Object': {
                'Key': 'Some key'
            }
        })
        # Act
        set_internal_id(mock_event)

        self.update_logger_metadata_fields_patcher.assert_called_once_with({
            'internal_id': 'internal_id'
        })

    def test_set_internal_id_generates_new_id_from_sns_trigger(self):
        # Arrange
        mock_event = sns_mock_event({'message': 'some message'})
        # Act
        set_internal_id(mock_event)
        # Assert
        self.update_logger_metadata_fields_patcher.assert_called_once_with({
            'internal_id': 'internal_id'
        })

    def test_set_internal_id_generates_new_id_from_cron_rule(self):
        # Arrange
        mock_event = cloudwatch_mock_event()
        # Act
        set_internal_id(mock_event)
        # Assert
        self.update_logger_metadata_fields_patcher.assert_called_once_with({
            'internal_id': 'internal_id'
        })

    def test_set_internal_id_uses_existing_id_from_sqs_triggered_lambda(self):
        # Arrange
        mock_event = sqs_mock_event({'internal_id': '123123123'})
        # Act
        set_internal_id(mock_event)
        # Assert
        self.update_logger_metadata_fields_patcher.assert_called_once_with({
            'internal_id': '123123123'
        })

    @patch('common.utils.log', MagicMock())
    def test_set_internal_id_uses_existing_id_from_step_triggered_lambda(self):
        # Arrange
        mock_event = {'internal_id': '123123123', 'export_arn': 'fake_export_arn'}
        # Act
        set_internal_id(mock_event)
        # Assert
        self.update_logger_metadata_fields_patcher.assert_called_once_with({
            'internal_id': '123123123'
        })

    def test_set_internal_id_uses_request_id_from_frontend_request(self):
        # Arrange
        mock_event = mock_api_event()
        # Act
        set_internal_id(mock_event)
        # Assert
        self.update_logger_metadata_fields_patcher.assert_called_once_with({
            'internal_id': 'requestId'
        })

    @patch('common.utils.s3_utils._S3_CLIENT')
    @patch('common.utils.log', MagicMock())
    def test_set_internal_id_uses_internal_id_from_s3_meta_data(self, s3_client_mock):
        # Arrange
        s3_client_mock.head_object.return_value = {
            'Metadata': {
                'internal_id': 'my_test_internal_id'
            }
        }

        s3_event = {'Records': [
            {'eventSource': 'aws:s3',
             'eventTime': '2021-06-09T07:02:06.824Z',
             'eventName': 'ObjectCreated:Put',
             's3': {'bucket': {'name': 'cerss-2048-092130162833-print-files-completed',
                               'arn': 'arn:aws:s3:::cerss-2048-092130162833-print-files-completed'},
                    'object': {'key': 'CIC/CSAS_EU-WEST-2_CP_2106080650.dat',
                               'size': 7011, 'eTag': '110a284d773d53d3ee5757858ab57302',
                               'versionId': 'e4AmO1U6nqTm46QEvBKg6RxOVgz6T2nb'}
                    }
             }
        ]}
        mock_event = sqs_mock_event(s3_event)
        # Act
        set_internal_id(mock_event)
        # Assert
        self.update_logger_metadata_fields_patcher.assert_called_once_with({
            'internal_id': 'my_test_internal_id'
        })
