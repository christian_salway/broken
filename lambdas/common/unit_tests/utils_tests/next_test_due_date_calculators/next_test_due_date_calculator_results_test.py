from ddt import ddt, data, unpack
from unittest import TestCase
from datetime import date, datetime, timezone
from mock import patch
from common.models.participant import ParticipantStatus
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator \
    import calculate_next_test_due_date_for_result
from common.utils.next_test_due_date_calculators.primary_hpv_rules \
    import get_negative_failsafe_repeat_months, get_positive_failsafe_repeat_months

test_date = date(2021, 1, 1)
date_of_birth = date(1990, 1, 1)

module = 'common.utils.next_test_due_date_calculators.next_test_due_date_calculator'

mock_env_vars = {
    "POSITIVE_FAILSAFE_REPEAT_MONTHS": '11',
    "NEGATIVE_FAILSAFE_REPEAT_MONTHS": '10'
}


@ddt
class TestNextTestDueDateCalculatorResults(TestCase):

    def setUp(self):
        self.datetime_patcher = patch(f'{module}.datetime').start()
        self.datetime_patcher.now.return_value = datetime(2021, 1, 1, tzinfo=timezone.utc)
        self.log_patcher = patch('common.log.logger').start()

    def tearDown(self):
        patch.stopall()

    @unpack
    @data(
        (("0", None, "H"), date_of_birth, False, False),
        (("1", None, "H"), date_of_birth, False, False),
        (("2", None, "H"), date_of_birth, False, False),
        (("X", "U", "H"), date_of_birth, True, True),
    )
    def test_results_to_ignore_in_calculations(self, results, date_of_birth, hpv_primary, self_sample):
        with self.assertRaises(Exception) as context:
            calculate_next_test_due_date_for_result(test_date, results, date_of_birth, hpv_primary, self_sample)

        self.assertEqual("This result code should be ignored for calculating next test due date",
                         context.exception.args[0])

    def test_invalid_result_profile(self):
        with self.assertRaises(Exception) as context:
            calculate_next_test_due_date_for_result(test_date, ('N', 'U', 'S'), date_of_birth, False, True)
        self.assertEqual("No profile for the provided combination of hpv_primary and self_sample",
                         context.exception.args[0])

    def test_invalid_results(self):
        with self.assertRaises(Exception) as context:
            calculate_next_test_due_date_for_result(test_date, ("N", "U", "L"), date_of_birth)

        self.assertEqual("No matching profile for results found when calculating next test due date",
                         context.exception.args[0])

    def test_minimum_default_next_test_due_date(self):
        result = calculate_next_test_due_date_for_result(date(2020, 12, 1), ('X', 'U', 'R'), date_of_birth)
        self.assertEqual(result, {
            "default": {
                "interval": 3,
                "next_test_due_date": date(2021, 3, 13)
            },
            "options": None,
            "range": None,
            "status": ParticipantStatus.REPEAT_ADVISED
        })

    def test_next_test_due_date_range(self):
        result = calculate_next_test_due_date_for_result(
            date(2020, 12, 1), ('B', '0', 'R'),
            date_of_birth,
            False,
            False)

        self.assertEqual(result, {
            "default": None,
            "options": None,
            "range": [
                {'min_interval': 1, 'max_interval': 36},
                {'interval': 1, 'next_test_due_date': date(2021, 3, 13)},
                {'interval': 2, 'next_test_due_date': date(2021, 3, 13)},
                {'interval': 3, 'next_test_due_date': date(2021, 3, 13)},
                {'interval': 4, 'next_test_due_date': date(2021, 4, 1)},
                {'interval': 5, 'next_test_due_date': date(2021, 5, 1)},
                {'interval': 6, 'next_test_due_date': date(2021, 6, 1)},
                {'interval': 7, 'next_test_due_date': date(2021, 7, 1)},
                {'interval': 8, 'next_test_due_date': date(2021, 8, 1)},
                {'interval': 9, 'next_test_due_date': date(2021, 9, 1)},
                {'interval': 10, 'next_test_due_date': date(2021, 10, 1)},
                {'interval': 11, 'next_test_due_date': date(2021, 11, 1)},
                {'interval': 12, 'next_test_due_date': date(2021, 12, 1)},
                {'interval': 13, 'next_test_due_date': date(2022, 1, 1)},
                {'interval': 14, 'next_test_due_date': date(2022, 2, 1)},
                {'interval': 15, 'next_test_due_date': date(2022, 3, 1)},
                {'interval': 16, 'next_test_due_date': date(2022, 4, 1)},
                {'interval': 17, 'next_test_due_date': date(2022, 5, 1)},
                {'interval': 18, 'next_test_due_date': date(2022, 6, 1)},
                {'interval': 19, 'next_test_due_date': date(2022, 7, 1)},
                {'interval': 20, 'next_test_due_date': date(2022, 8, 1)},
                {'interval': 21, 'next_test_due_date': date(2022, 9, 1)},
                {'interval': 22, 'next_test_due_date': date(2022, 10, 1)},
                {'interval': 23, 'next_test_due_date': date(2022, 11, 1)},
                {'interval': 24, 'next_test_due_date': date(2022, 12, 1)},
                {'interval': 25, 'next_test_due_date': date(2023, 1, 1)},
                {'interval': 26, 'next_test_due_date': date(2023, 2, 1)},
                {'interval': 27, 'next_test_due_date': date(2023, 3, 1)},
                {'interval': 28, 'next_test_due_date': date(2023, 4, 1)},
                {'interval': 29, 'next_test_due_date': date(2023, 5, 1)},
                {'interval': 30, 'next_test_due_date': date(2023, 6, 1)},
                {'interval': 31, 'next_test_due_date': date(2023, 7, 1)},
                {'interval': 32, 'next_test_due_date': date(2023, 8, 1)},
                {'interval': 33, 'next_test_due_date': date(2023, 9, 1)},
                {'interval': 34, 'next_test_due_date': date(2023, 10, 1)},
                {'interval': 35, 'next_test_due_date': date(2023, 11, 1)},
                {'interval': 36, 'next_test_due_date': date(2023, 12, 1)}
            ],
            "status": ParticipantStatus.REPEAT_ADVISED
        })


@patch('os.environ', mock_env_vars)
class TestFailsafe(TestCase):
    def test_get_positive_failsafe(self):
        # Act
        get_repeat_months = get_positive_failsafe_repeat_months()
        repeat_months = get_repeat_months()
        # Assert
        self.assertEqual(repeat_months, 11)

    def test_get_negative_failsafe(self):
        # Act
        get_repeat_months = get_negative_failsafe_repeat_months()
        repeat_months = get_repeat_months()
        # Assert
        self.assertEqual(repeat_months, 10)
