import os
import inspect
import hashlib
from ddt import ddt, data, unpack
from unittest import TestCase
from datetime import date, datetime, timezone
from mock import patch
from common.models.participant import ParticipantStatus
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator \
    import calculate_next_test_due_date_for_result
from common.utils.next_test_due_date_calculators.cytology_and_hpv_rules import (
    get_cytology_and_hpv_rules_digest,
    rules as cytology_and_hpv_rules
)
from utils import (
    read_rules_digest_check_lines,
    check_code_conforms_to_patterns
)


three_years = {"interval": 36, "next_test_due_date": date(2024, 1, 1)}
five_years = {"interval": 60, "next_test_due_date": date(2026, 1, 1)}
one_year = {"interval": 12, "next_test_due_date": date(2022, 1, 1)}
# This is being mocked in for the positive failsafe repeat months
eleven_months = {"interval": 11, "next_test_due_date": date(2021, 12, 1)}
# This is being mocked in for the negative failsafe repeat months
ten_months = {"interval": 10, "next_test_due_date": date(2021, 11, 1)}
test_date = date(2021, 1, 1)
date_of_birth = date(1990, 1, 1)

module = 'common.utils.next_test_due_date_calculators.next_test_due_date_calculator'

mock_env_vars = {
    "POSITIVE_FAILSAFE_REPEAT_MONTHS": '11',
    "NEGATIVE_FAILSAFE_REPEAT_MONTHS": '10'
}

RANGE_OF_6 = [{'min_interval': 1, 'max_interval': 6},
              {'interval': 1, 'next_test_due_date': date(2021, 3, 13)},
              {'interval': 2, 'next_test_due_date': date(2021, 3, 13)},
              {'interval': 3, 'next_test_due_date': date(2021, 4, 1)},
              {'interval': 4, 'next_test_due_date': date(2021, 5, 1)},
              {'interval': 5, 'next_test_due_date': date(2021, 6, 1)},
              {'interval': 6, 'next_test_due_date': date(2021, 7, 1)}]

RANGE_OF_12 = [{'min_interval': 1, 'max_interval': 12},
               {'interval': 1, 'next_test_due_date': date(2021, 3, 13)},
               {'interval': 2, 'next_test_due_date': date(2021, 3, 13)},
               {'interval': 3, 'next_test_due_date': date(2021, 4, 1)},
               {'interval': 4, 'next_test_due_date': date(2021, 5, 1)},
               {'interval': 5, 'next_test_due_date': date(2021, 6, 1)},
               {'interval': 6, 'next_test_due_date': date(2021, 7, 1)},
               {'interval': 7, 'next_test_due_date': date(2021, 8, 1)},
               {'interval': 8, 'next_test_due_date': date(2021, 9, 1)},
               {'interval': 9, 'next_test_due_date': date(2021, 10, 1)},
               {'interval': 10, 'next_test_due_date': date(2021, 11, 1)},
               {'interval': 11, 'next_test_due_date': date(2021, 12, 1)},
               {'interval': 12, 'next_test_due_date': date(2022, 1, 1)}]

RANGE_OF_36 = [{'min_interval': 1, 'max_interval': 36},
               {'interval': 1, 'next_test_due_date': date(2021, 3, 13)},
               {'interval': 2, 'next_test_due_date': date(2021, 3, 13)},
               {'interval': 3, 'next_test_due_date': date(2021, 4, 1)},
               {'interval': 4, 'next_test_due_date': date(2021, 5, 1)},
               {'interval': 5, 'next_test_due_date': date(2021, 6, 1)},
               {'interval': 6, 'next_test_due_date': date(2021, 7, 1)},
               {'interval': 7, 'next_test_due_date': date(2021, 8, 1)},
               {'interval': 8, 'next_test_due_date': date(2021, 9, 1)},
               {'interval': 9, 'next_test_due_date': date(2021, 10, 1)},
               {'interval': 10, 'next_test_due_date': date(2021, 11, 1)},
               {'interval': 11, 'next_test_due_date': date(2021, 12, 1)},
               {'interval': 12, 'next_test_due_date': date(2022, 1, 1)},
               {'interval': 13, 'next_test_due_date': date(2022, 2, 1)},
               {'interval': 14, 'next_test_due_date': date(2022, 3, 1)},
               {'interval': 15, 'next_test_due_date': date(2022, 4, 1)},
               {'interval': 16, 'next_test_due_date': date(2022, 5, 1)},
               {'interval': 17, 'next_test_due_date': date(2022, 6, 1)},
               {'interval': 18, 'next_test_due_date': date(2022, 7, 1)},
               {'interval': 19, 'next_test_due_date': date(2022, 8, 1)},
               {'interval': 20, 'next_test_due_date': date(2022, 9, 1)},
               {'interval': 21, 'next_test_due_date': date(2022, 10, 1)},
               {'interval': 22, 'next_test_due_date': date(2022, 11, 1)},
               {'interval': 23, 'next_test_due_date': date(2022, 12, 1)},
               {'interval': 24, 'next_test_due_date': date(2023, 1, 1)},
               {'interval': 25, 'next_test_due_date': date(2023, 2, 1)},
               {'interval': 26, 'next_test_due_date': date(2023, 3, 1)},
               {'interval': 27, 'next_test_due_date': date(2023, 4, 1)},
               {'interval': 28, 'next_test_due_date': date(2023, 5, 1)},
               {'interval': 29, 'next_test_due_date': date(2023, 6, 1)},
               {'interval': 30, 'next_test_due_date': date(2023, 7, 1)},
               {'interval': 31, 'next_test_due_date': date(2023, 8, 1)},
               {'interval': 32, 'next_test_due_date': date(2023, 9, 1)},
               {'interval': 33, 'next_test_due_date': date(2023, 10, 1)},
               {'interval': 34, 'next_test_due_date': date(2023, 11, 1)},
               {'interval': 35, 'next_test_due_date': date(2023, 12, 1)},
               {'interval': 36, 'next_test_due_date': date(2024, 1, 1)}]


@patch('os.environ', mock_env_vars)
@ddt
class TestCytologyAndHpvRulesResult(TestCase):

    def setUp(self):
        self.datetime_patcher = patch(f'{module}.datetime').start()
        self.datetime_patcher.now.return_value = datetime(2021, 1, 1, tzinfo=timezone.utc)
        self.log_patcher = patch('common.log.logger').start()

    def tearDown(self):
        patch.stopall()

    @classmethod
    def setUpClass(cls) -> None:
        cls.codes_tested = set()
        cls.codes_in_rules = set(cytology_and_hpv_rules.keys())

    @classmethod
    def tearDownClass(cls) -> None:
        if (cls.codes_in_rules != cls.codes_tested):
            raise Exception(f'all codes not tested: missing {cls.codes_in_rules.difference(cls.codes_tested)}')

    @unpack
    @data((('B', '0', 'A'), date_of_birth, {"default": three_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),  
          (('B', '0', 'A'), date(1950, 1, 1), {"default": five_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),  
          (('B', '0', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_36, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('B', '0', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  #
          (('B', '9', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_12, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('B', '9', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('B', 'U', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_6, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('B', 'U', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('E', '0', 'A'), date_of_birth, {"default": three_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE},),  
          (('E', '0', 'A'), date(1950, 1, 1), {"default": five_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),  
          (('E', '0', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_36, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('E', '0', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('E', '9', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('E', 'U', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_6, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('E', 'U', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('G', '0', 'A'), date_of_birth, {"default": three_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE},),  
          (('G', '0', 'A'), date(1950, 1, 1), {"default": five_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),  
          (('G', '0', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_36, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('G', '0', 'S'), date_of_birth, {"default": ten_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('G', '9', 'R'), date_of_birth, {"default": one_year, "options": None, "range": None, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('G', '9', 'S'), date_of_birth, {"default": ten_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('G', 'U', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_6, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('G', 'U', 'S'), date_of_birth, {"default": ten_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('M', '0', 'A'), date_of_birth, {"default": three_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE},),  
          (('M', '0', 'A'), date(1950, 1, 1), {"default": five_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),  
          (('M', '0', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_36, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('M', '0', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('M', '9', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_12, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('M', '9', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('M', 'U', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_6, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('M', 'U', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('N', '0', 'A'), date_of_birth, {"default": three_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE},),  
          (('N', '0', 'A'), date(1950, 1, 1), {"default": five_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),  
          (('N', '0', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_36, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('N', '0', 'S'), date_of_birth, {"default": ten_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('N', '9', 'R'), date_of_birth, {"default": one_year, "options": None, "range": None, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('N', '9', 'S'), date_of_birth, {"default": ten_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('N', 'U', 'R'), date_of_birth, {"default": None, "options": None, "range": RANGE_OF_6, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('N', 'U', 'S'), date_of_birth, {"default": ten_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('4', '9', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('4', '0', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('4', 'U', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('5', '9', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('5', '0', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('5', 'U', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('6', '9', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('6', '0', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('6', 'U', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('7', '9', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('7', '0', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('7', 'U', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}))  
    def test_calculate_valid_cytology_and_hpv_next_test_due_dates(
            self, results, date_of_birth, expected):
        result = calculate_next_test_due_date_for_result(test_date, results, date_of_birth, False, False)
        self.assertEqual(result, expected)
        self.codes_tested.add(results)


class TestCytologyAndHpvRulesDigest(TestCase):
    def test_cytology_and_hpv_rules_digest(self):
        expected_digest = get_cytology_and_hpv_rules_digest()
        file_path = os.path.abspath(inspect.getfile(get_cytology_and_hpv_rules_digest))
        rules_source = read_rules_digest_check_lines(file_path)
        actual_digest = hashlib.md5(rules_source.encode('utf-8')).hexdigest()
        self.assertEqual(actual_digest, expected_digest)


allowable_patters = [
    '[BEGMN],0,[ARS]',
    '[BGMN],[9U],[RS]',
    'E,9,S',
    'E,U,[RS]',
    '[4-7],[90U],S'
]


class TestCytologyAndHpvRulesCodes(TestCase):
    def test_cytology_and_hpv_rules_codes_match_patterns(self):
        for code in cytology_and_hpv_rules.keys():
            self.assertTrue(check_code_conforms_to_patterns(code, allowable_patters),
                            f'code {code} does not match any pattern in {allowable_patters}')
