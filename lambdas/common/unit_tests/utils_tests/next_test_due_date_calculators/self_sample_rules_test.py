import os
import inspect
import hashlib
from ddt import ddt, data, unpack
from unittest import TestCase
from datetime import date, datetime, timezone
from mock import patch
from common.models.participant import ParticipantStatus
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator \
    import calculate_next_test_due_date_for_result
from common.utils.next_test_due_date_calculators.self_sample_rules import (
    get_self_sample_rules_digest,
    rules as self_sample_rules
)
from utils import (
    read_rules_digest_check_lines,
    check_code_conforms_to_patterns
)


date_of_birth = date(1990, 1, 1)
three_years = {"interval": 36, "next_test_due_date": date(2024, 1, 1)}
five_years = {"interval": 60, "next_test_due_date": date(2026, 1, 1)}
three_months = {"interval": 3, "next_test_due_date": date(2021, 4, 1)}
test_date = date(2021, 1, 1)

module = 'common.utils.next_test_due_date_calculators.next_test_due_date_calculator'


@ddt
class TestSelfSampledRulesResult(TestCase):

    def setUp(self):
        self.datetime_patcher = patch(f'{module}.datetime').start()
        self.datetime_patcher.now.return_value = datetime(2021, 1, 1, tzinfo=timezone.utc)
        self.log_patcher = patch('common.log.logger').start()

    def tearDown(self):
        patch.stopall()

    @classmethod
    def setUpClass(cls) -> None:
        cls.codes_tested = set()
        cls.codes_in_rules = set(self_sample_rules.keys())
        cls.codes_in_rules.remove(("X", "U", "H"))  # tested elsewhere, ignored in calculations

    @classmethod
    def tearDownClass(cls) -> None:
        if (cls.codes_in_rules != cls.codes_tested):
            raise Exception(f'all codes not tested: missing {cls.codes_in_rules.difference(cls.codes_tested)}')

    @unpack
    @data((("X", "0", "A"), date_of_birth, {"default": three_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),           
          (("X", "0", "A"), date(1950, 1, 1), {"default": five_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),         
          (("X", "9", "R"), date_of_birth, {"default": three_months, "options": None, "range": None, "status": ParticipantStatus.REPEAT_ADVISED}))   
    def test_calculate_valid_self_sample_next_test_due_dates(
            self, results, date_of_birth, expected):
        result = calculate_next_test_due_date_for_result(test_date, results, date_of_birth, True, True)
        self.assertEqual(result, expected)
        self.codes_tested.add(results)


class TestSelfSampleRulesDigest(TestCase):
    def test_self_sample_rules_digest(self):
        expected_digest = get_self_sample_rules_digest()
        file_path = os.path.abspath(inspect.getfile(get_self_sample_rules_digest))
        rules_source = read_rules_digest_check_lines(file_path)
        actual_digest = hashlib.md5(rules_source.encode('utf-8')).hexdigest()
        self.assertEqual(actual_digest, expected_digest)


allowable_patters = [
    'X,0,A',
    'X,9,R',
    'X,U,H'
]


class TestSelfSampleRulesCodes(TestCase):
    def test_self_sample_rules_codes_match_patterns(self):
        for code in self_sample_rules.keys():
            self.assertTrue(check_code_conforms_to_patterns(code, allowable_patters),
                            f'code {code} does not match any pattern in {allowable_patters}')
