import re


def read_rules_digest_check_lines(file_name):
    block_of_lines = []
    with open(file_name) as input_data:
        for line in input_data:
            if 'DIGEST_CHECK_BEGIN' in line:
                break
        for line in input_data:
            if 'DIGEST_CHECK_END' in line:
                break
            if (line.strip()[0] != '#'):
                block_of_lines.append(line)
    one_string = ''.join(block_of_lines)
    return one_string[one_string.find('{'):].strip()


def _element_matches_expression(code, exp):
    if '' == exp:
        return code is None
    return re.match(exp, code) is not None


def _check_code_against_pattern(code, pattern):
    expressions = pattern.split(',')
    for element, exp in zip(code, expressions):
        if not _element_matches_expression(element, exp):
            return False
    return True


def check_code_conforms_to_patterns(code, patterns):
    for pattern in patterns:
        if _check_code_against_pattern(code, pattern):
            return True
    return False
