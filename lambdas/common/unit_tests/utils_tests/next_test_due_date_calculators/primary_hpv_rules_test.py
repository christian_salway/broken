import os
import inspect
import hashlib
from ddt import ddt, data, unpack
from unittest import TestCase
from datetime import date, datetime, timezone
from mock import patch
from common.models.participant import ParticipantStatus
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator \
    import calculate_next_test_due_date_for_result
from common.utils.next_test_due_date_calculators.primary_hpv_rules import (
    get_primary_hpv_rules_digest,
    rules as hpv_primary_rules
)
from utils import (
    read_rules_digest_check_lines,
    check_code_conforms_to_patterns
)


three_years = {"interval": 36, "next_test_due_date": date(2024, 1, 1)}
five_years = {"interval": 60, "next_test_due_date": date(2026, 1, 1)}
one_year = {"interval": 12, "next_test_due_date": date(2022, 1, 1)}
# This is being mocked in for the positive failsafe repeat months
eleven_months = {"interval": 11, "next_test_due_date": date(2021, 12, 1)}
# This is being mocked in for the negative failsafe repeat months
ten_months = {"interval": 10, "next_test_due_date": date(2021, 11, 1)}
six_months = {"interval": 6, "next_test_due_date": date(2021, 7, 1)}
three_months = {"interval": 3, "next_test_due_date": date(2021, 4, 1)}
test_date = date(2021, 1, 1)
date_of_birth = date(1990, 1, 1)

module = 'common.utils.next_test_due_date_calculators.next_test_due_date_calculator'

mock_env_vars = {
    "POSITIVE_FAILSAFE_REPEAT_MONTHS": '11',
    "NEGATIVE_FAILSAFE_REPEAT_MONTHS": '10'
}


@patch('os.environ', mock_env_vars)
@ddt
class TestHpvPrimaryRulesResult(TestCase):

    def setUp(self):
        self.datetime_patcher = patch(f'{module}.datetime').start()
        self.datetime_patcher.now.return_value = datetime(2021, 1, 1, tzinfo=timezone.utc)
        self.log_patcher = patch('common.log.logger').start()

    def tearDown(self):
        patch.stopall()

    @classmethod
    def setUpClass(cls) -> None:
        cls.codes_tested = set()
        cls.codes_in_rules = set(hpv_primary_rules.keys())

    @classmethod
    def tearDownClass(cls) -> None:
        if (cls.codes_in_rules != cls.codes_tested):
            raise Exception(f'all codes not tested: missing {cls.codes_in_rules.difference(cls.codes_tested)}')

    @unpack
    @data((("X", "0", "A"), date_of_birth, {"default": three_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),  
          (("X", "0", "A"), date(1950, 1, 1), {"default": five_years, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),  
          (("X", "0", "R"), date_of_birth, {"default": three_years, "options": [one_year, six_months], "range": None, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (("X", "0", "S"), date_of_birth, {"default": ten_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("X", "U", "R"), date_of_birth, {"default": three_months, "options": None, "range": None, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (("X", "U", "S"), date_of_birth, {"default": ten_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("X", "9", "S"), date_of_birth, {"default": ten_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("1", "9", "R"), date_of_birth, {"default": three_months, "options": None, "range": None, "status": ParticipantStatus.INADEQUATE}),  
          (("1", "9", "S"), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.INADEQUATE}),  
          (("3", "9", "S"), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("4", "9", "S"), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("5", "9", "S"), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("6", "9", "S"), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("7", "9", "S"), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("8", "9", "S"), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("9", "9", "S"), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("0", "9", "R"), date_of_birth, {"default": one_year, "options": [three_years], "range": None, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (("2", "9", "R"), date_of_birth, {"default": one_year, "options": [three_years], "range": None, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (("0", "9", "S"), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (("2", "9", "S"), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('0', 'Q', 'R'), date_of_birth, {"default": one_year, "options": None, "range": None, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('0', 'Q', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),  
          (('1', 'Q', 'R'), date_of_birth, {"default": three_months, "options": None, "range": None, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('1', 'Q', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.INADEQUATE}),  
          (('2', 'Q', 'R'), date_of_birth, {"default": one_year, "options": None, "range": None, "status": ParticipantStatus.REPEAT_ADVISED}),  
          (('2', 'Q', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.ROUTINE}),  
          (('3', 'Q', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('4', 'Q', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('5', 'Q', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('6', 'Q', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('7', 'Q', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('8', 'Q', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),  
          (('9', 'Q', 'S'), date_of_birth, {"default": eleven_months, "options": None, "range": None, "status": ParticipantStatus.SUSPENDED}),)  
    def test_calculate_valid_hpv_next_test_due_dates(
            self, results, date_of_birth, expected):
        result = calculate_next_test_due_date_for_result(test_date, results, date_of_birth)
        self.assertEqual(result, expected)
        self.codes_tested.add(results)


class TestHpvPrimaryRulesDigest(TestCase):
    def test_primary_hpv_rules_digest(self):
        expected_digest = get_primary_hpv_rules_digest()
        file_path = os.path.abspath(inspect.getfile(get_primary_hpv_rules_digest))
        rules_source = read_rules_digest_check_lines(file_path)
        actual_digest = hashlib.md5(rules_source.encode('utf-8')).hexdigest()
        self.assertEqual(actual_digest, expected_digest)


allowable_patters = [
    '[0-2],Q,R',
    '[0-9],Q,S',
    '[0-2],9,R',
    '[0-9],9,S',
    'X,0,A|R|S',
    'X,U,R|S',
    'X,9,S'
]


class TestHpvPrimaryRulesCodes(TestCase):
    def test_primary_hpv_rules_codes_match_patterns(self):
        for code in hpv_primary_rules.keys():
            self.assertTrue(check_code_conforms_to_patterns(code, allowable_patters),
                            f'code {code} does not match any pattern in {allowable_patters}')
