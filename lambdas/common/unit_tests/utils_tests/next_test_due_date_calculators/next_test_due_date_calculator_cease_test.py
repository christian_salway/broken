from datetime import datetime, timezone
from unittest import TestCase
from mock import patch, Mock
from common.models.participant import ParticipantStatus, CeaseReason
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator import \
    calculate_next_test_due_date_and_status_for_ceased_participant

module = 'common.utils.next_test_due_date_calculators.next_test_due_date_calculator'

cease_reasons = [
    CeaseReason.DUE_TO_AGE,
    CeaseReason.NO_CERVIX,
    CeaseReason.RADIOTHERAPY,
    CeaseReason.PATIENT_INFORMED_CHOICE,
    CeaseReason.MENTAL_CAPACITY_ACT
]


class TestNextTestDueDateCease(TestCase):

    @patch('common.log.log', Mock())
    def setUp(self):
        self.age_patcher = patch(f'{module}.calculate_age_in_years_at_last_test').start()
        self.status_patcher = patch(f'{module}.calculate_status_from_test_results').start()
        self.datetime_patcher = patch(f'{module}.datetime').start()
        self.calculate_next_test_due_date_for_new_participant_patcher = patch(
            f'{module}.calculate_next_test_due_date_for_new_participant').start()
        self.calculate_next_test_due_date_patcher = patch(f'{module}.calculate_next_test_due_date').start()

        self.calc = calculate_next_test_due_date_and_status_for_ceased_participant

        self.participant = {'date_of_birth': 'date_of_birth'}
        # First result is an ignored code
        self.test_results = [{
            'test_date': '2019-05-22',
            'result_code': '1',
            'action_code': 'H',
            'hpv_primary': False,
            'self_sample': False,
        }, {
            'test_date': '2019-05-21',
            'recall_months': '7',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'A'
        }]
        self.cease_reason = cease_reasons[0]

        self.now = datetime(2020, 9, 15, tzinfo=timezone.utc)
        self.status_patcher.return_value = ParticipantStatus.ROUTINE.value
        self.age_patcher.return_value = 35
        self.datetime_patcher.now.return_value = self.now
        self.datetime_patcher.strptime.return_value = self.now
        self.calculate_next_test_due_date_for_new_participant_patcher.return_value = self.now.date()
        self.calculate_next_test_due_date_patcher.return_value = self.now.date()

        self.default_ntdd = '2020-11-25'

    def setStatus(self, status):
        self.status_patcher.return_value = status

    def setAge(self, age):
        self.age_patcher.return_value = age

    def tearDown(self):
        patch.stopall()

    def act(self):
        return self.calc(self.participant, self.test_results, self.cease_reason)

    def test_ceased_due_to_age_and_no_previous_test(self):
        # Arrange
        self.test_results = []
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['CEASED_DUE_TO_AGE', 'NO_PREVIOUS_TEST'],
            'next_test_due_date': '2020-11-25',
            'status': ParticipantStatus.ROUTINE.value})

    def test_ceased_due_to_age_and_ignore_previous_test(self):
        # Arrange
        self.test_results = [{
            'test_date': '2019-05-21',
            'result_code': '1',
            'action_code': 'H',
            'hpv_primary': False,
            'self_sample': False,
        }]
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['CEASED_DUE_TO_AGE', 'NO_PREVIOUS_TEST'],
            'next_test_due_date': '2020-11-25',
            'status': ParticipantStatus.ROUTINE.value})

    def test_ceased_due_to_age_and_status_not_routine(self):
        # Arrange
        self.setStatus('A VALUE THAT IS NOT ROUTINE')
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['CEASED_DUE_TO_AGE', 'STATUS_NOT_ROUTINE'],
            'next_test_due_date': '2020-11-25',
            'status': ParticipantStatus.ROUTINE.value})

    def test_ceased_due_to_age_and_age_under_sixty(self):
        # Arrange
        self.setAge(35)
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['CEASED_DUE_TO_AGE', 'UNDER_60'],
            'next_test_due_date': '2020-11-25',
            'status': ParticipantStatus.ROUTINE.value})

    def test_ceased_due_to_age_fallback(self):
        # Arrange
        self.setAge(60)
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['CEASED_DUE_TO_AGE', 'OVER_59'],
            'status': None})

    def test_cease_reason_invalid(self):
        # Arrange
        self.cease_reason = 'AN INVALID CEASE REASON'
        # Act
        with self.assertRaises(Exception):
            self.act()

    def test_no_previous_test_result(self):
        # Arrange
        self.cease_reason = cease_reasons[3]
        self.test_results = []
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['NO_PREVIOUS_TEST'],
            'next_test_due_date': '2020-09-15',
            'status': ParticipantStatus.CALLED.value})
        self.calculate_next_test_due_date_for_new_participant_patcher.assert_called_with(self.now.date())

    def test_single_ignored_previous_test_result(self):
        # Arrange
        self.cease_reason = cease_reasons[3]
        self.test_results = [{
            'test_date': '2019-05-21',
            'result_code': '1',
            'action_code': 'H',
            'hpv_primary': False,
            'self_sample': False,
        }]
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['NO_PREVIOUS_TEST'],
            'next_test_due_date': '2020-09-15',
            'status': ParticipantStatus.CALLED.value})
        self.calculate_next_test_due_date_for_new_participant_patcher.assert_called_with(self.now.date())

    def test_suspended_status(self):
        # Arrange
        self.cease_reason = cease_reasons[3]
        self.setStatus(ParticipantStatus.SUSPENDED.value)
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['SUSPENDED_STATUS'],
            'next_test_due_date': '2020-09-15',
            'status': ParticipantStatus.SUSPENDED.value})
        self.calculate_next_test_due_date_patcher.assert_called_with(self.now.date(), 12)

    def test_inadequate_status(self):
        # Arrange
        self.cease_reason = cease_reasons[3]
        self.setStatus(ParticipantStatus.INADEQUATE.value)
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['INADEQUATE_STATUS'],
            'next_test_due_date': '2020-09-15',
            'status': ParticipantStatus.INADEQUATE.value})
        self.calculate_next_test_due_date_patcher.assert_called_with(self.now.date(), 3)

    def test_repeat_advised_status(self):
        # Arrange
        self.cease_reason = cease_reasons[3]
        self.setStatus(ParticipantStatus.REPEAT_ADVISED.value)
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['REPEAT_ADVISED_STATUS'],
            'next_test_due_date': '2020-09-15',
            'status': ParticipantStatus.REPEAT_ADVISED.value})
        self.calculate_next_test_due_date_patcher.assert_called_with(self.now.date(), 7)

    def test_routine_status_and_age_under_fifty(self):
        # Arrange
        self.cease_reason = cease_reasons[3]
        self.setStatus(ParticipantStatus.ROUTINE.value)
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['ROUTINE_STATUS', 'UNDER_50'],
            'next_test_due_date': '2020-09-15',
            'status': ParticipantStatus.ROUTINE.value})
        self.calculate_next_test_due_date_patcher.assert_called_with(self.now.date(), 36)

    def test_routine_status_and_age_fifty(self):
        # Arrange
        self.cease_reason = cease_reasons[3]
        self.setStatus(ParticipantStatus.ROUTINE.value)
        self.setAge(50)
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['ROUTINE_STATUS', 'BETWEEN_50_AND_59'],
            'next_test_due_date': '2020-09-15',
            'status': ParticipantStatus.ROUTINE.value})
        self.calculate_next_test_due_date_patcher.assert_called_with(self.now.date(), 60)

    def test_routine_status_and_age_fifty_nine(self):
        # Arrange
        self.cease_reason = cease_reasons[3]
        self.setStatus(ParticipantStatus.ROUTINE.value)
        self.setAge(59)
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['ROUTINE_STATUS', 'BETWEEN_50_AND_59'],
            'next_test_due_date': '2020-09-15',
            'status': ParticipantStatus.ROUTINE.value})
        self.calculate_next_test_due_date_patcher.assert_called_with(self.now.date(), 60)

    def test_routine_status_and_age_over_fifty_nine(self):
        # Arrange
        self.cease_reason = cease_reasons[3]
        self.setStatus(ParticipantStatus.ROUTINE.value)
        self.setAge(60)
        # Act
        result = self.act()
        # Assert
        self.assertEqual(result, {
            'explanation': ['ROUTINE_STATUS', 'OVER_59'],
            'status': None})

    def test_unhandled_route(self):
        # Arrange
        self.cease_reason = cease_reasons[3]
        self.setStatus(ParticipantStatus.CALLED.value)
        self.setAge(35)
        # Act
        with self.assertRaises(Exception):
            self.act()
