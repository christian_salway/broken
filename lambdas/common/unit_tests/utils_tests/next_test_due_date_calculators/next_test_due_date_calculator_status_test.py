from datetime import datetime, date, timezone
from unittest import TestCase
from dateutil.relativedelta import relativedelta
from mock import patch, Mock
from ddt import ddt, data, unpack
import common.utils.next_test_due_date_calculators.next_test_due_date_calculator as calculator
from common.models.participant import ParticipantStatus


@ddt
class TestNextTestDueDateCalculatorStatus(TestCase):

    module = 'common.utils.next_test_due_date_calculators.next_test_due_date_calculator'
    FIXED_NOW_TIME = datetime(2020, 9, 15, tzinfo=timezone.utc)

    # Helper functions tests

    @patch(f'{module}.datetime')
    def test_default_next_test_due_date_returns_tomorrow_plus_10_weeks(self, datetime_mock):
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        expected_date = date(2020, 11, 25)
        actual_date = calculator.default_next_test_due_date()
        self.assertEqual(expected_date, actual_date)

    @unpack
    @data((date(2020, 8, 24), 3, date(2020, 11, 25)),
          (date(2020, 8, 26), 3, date(2020, 11, 26)),
          (date(2020, 2, 29), 36, date(2023, 2, 28)))
    @patch(f'{module}.default_next_test_due_date')
    def test_calculate_next_test_due_date_uses_recall_months_and_returns_a_minimum_default_date(
            self, last_test_date, recall_months, expected_date, default_date_mock):
        default_date_mock.return_value = date(2020, 11, 25)
        actual_date = calculator.calculate_next_test_due_date(last_test_date, recall_months)
        self.assertEqual(expected_date, actual_date)

    @unpack
    @data((date(2020, 11, 24), date(1999, 11, 25), 20),
          (date(2020, 11, 24), date(1999, 11, 24), 21),
          (None, date(1999, 11, 25), None))
    def test_calculate_age_in_years_at_last_test_returns_age(
            self, last_test_date, date_of_birth, expected_age):
        actual_age = calculator.calculate_age_in_years_at_last_test(last_test_date, date_of_birth)
        self.assertEqual(expected_age, actual_age)

    def test_build_response_creates_status_and_next_test_due_date_dictionary_with_date_in_isoformat(self):
        status = ParticipantStatus.CALLED
        next_test_due_date = date(2020, 12, 31)
        expected_response = {'status': ParticipantStatus.CALLED.value,
                             'next_test_due_date': '2020-12-31', 'explanation': ['CEASED_DUE_TO_AGE']}
        explanation = ['CEASED_DUE_TO_AGE']
        actual_response = calculator.build_response(status, next_test_due_date, explanation)
        self.assertDictEqual(expected_response, actual_response)

    @unpack
    #       test_results                result_codes        expected_status
    @data(([{'id': '1'}, {'id': '2'}],  ['XUH', 'X0A'],     ParticipantStatus.ROUTINE),
          ([{'id': '1'}],               ['X0R'],            ParticipantStatus.REPEAT_ADVISED),
          ([{'id': '1'}, {'id': '2'}],  ['19R', 'X0A'],     ParticipantStatus.INADEQUATE),
          ([{'id': '1'}],               ['39S'],            ParticipantStatus.SUSPENDED),
          ([], [], None),
          (None, [], None))
    @patch(f'{module}.get_concatenated_result_code_combination')
    def test_calculate_status_from_test_results_returns_status(
            self, test_results, result_code_combos, expected_status, get_result_combo_mock):
        get_result_combo_mock.side_effect = result_code_combos
        actual_status = calculator.calculate_status_from_test_results(test_results)
        self.assertEqual(expected_status, actual_status)

    @unpack
    @data(([{'id': '1'}], ['XUH']),
          ([{'id': '1'}, {'id': '2'}], ['XUH', 'XUH']),
          ([{'id': '1'}], ['LOL']))
    @patch(f'{module}.get_concatenated_result_code_combination')
    def test_calculate_status_from_test_results_raises_exception(
            self, test_results, result_code_combos, get_result_combo_mock):
        get_result_combo_mock.side_effect = result_code_combos
        expected_exception_message = 'Participant status cannot be calculated from previous test results.'

        with self.assertRaises(Exception) as context:
            calculator.calculate_status_from_test_results(test_results)

        self.assertEqual(expected_exception_message, context.exception.args[0])

    # New participant tests

    @unpack
    @data((date(1996, 4, 13), False),
          (date(1996, 4, 14), True),
          (date(1996, 4, 15), True))
    @patch(f'{module}.datetime')
    def test_participant_is_less_than_equal_to_24_years_and_22_weeks_returns_true_or_false_correctly(
            self, date_of_birth, expected_response, datetime_mock):
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        actual_response = calculator.participant_is_less_than_equal_to_24_years_and_22_weeks(date_of_birth)
        self.assertEqual(expected_response, actual_response)

    @unpack
    @data((date(1995, 4, 14), True, date(2019, 11, 26)),
          (date(1996, 4, 13), False, date(2020, 12, 5)))
    @patch(f'{module}.default_next_test_due_date')
    @patch(f'{module}.participant_is_less_than_equal_to_24_years_and_22_weeks')
    def test_calculate_next_test_due_date_for_new_participant_calculates_date_correctly(
            self, date_of_birth, is_less_than_equal_24_and_22_weeks_old, expected_response,
            is_less_than_equal_24_and_22_weeks_old_mock, default_date_mock):
        is_less_than_equal_24_and_22_weeks_old_mock.return_value = is_less_than_equal_24_and_22_weeks_old
        default_date_mock.return_value = date(2020, 12, 5)
        actual_response = calculator.calculate_next_test_due_date_for_new_participant(date_of_birth)
        self.assertEqual(expected_response, actual_response)

    def test_calculate_next_test_due_date_for_closed_episode(self):
        participant = {
            'participant_id': '123456',
            'sort_key': 'PARTICIPANT',
            'nhs_number': '9999999999',
            'test_due_date': '2020-01-01',
            'status': 'CALLED',
            'date_of_birth': '1960-01-01'
        }
        episode = {
            'participant_id': '123456',
            'sort_key': 'EPISODE',
            'nhs_number': '9999999999',
            'test_due_date': '2020-01-01'
        }

        # See NTDD Logic Confluence page for further detail, this covers both main date logic branches
        status_age_next_test_due_date_map = [
            (ParticipantStatus.CALLED, (datetime.now(timezone.utc) - relativedelta(years=25)).date().isoformat(), datetime(2023, 1, 1, tzinfo=timezone.utc).date()),  
            (ParticipantStatus.CALLED, (datetime.now(timezone.utc) - relativedelta(years=26)).date().isoformat(), datetime(2023, 1, 1, tzinfo=timezone.utc).date()),  
            (ParticipantStatus.CALLED, (datetime.now(timezone.utc) - relativedelta(years=50)).date().isoformat(), datetime(2025, 1, 1, tzinfo=timezone.utc).date()),  
            (ParticipantStatus.CALLED, (datetime.now(timezone.utc) - relativedelta(years=80)).date().isoformat(), datetime(2025, 1, 1, tzinfo=timezone.utc).date()),  
            (ParticipantStatus.SUSPENDED, (datetime.now(timezone.utc) - relativedelta(years=50)).date().isoformat(), datetime(2021, 1, 1, tzinfo=timezone.utc).date())  
        ]

        for status, date_of_birth, expected_next_test_due_date in status_age_next_test_due_date_map:
            participant['date_of_birth'] = date_of_birth
            participant['status'] = status
            actual_next_test_due_date = calculator.calculate_next_test_due_date_for_closed_episode(participant, episode)
            self.assertEqual(expected_next_test_due_date, actual_next_test_due_date)

    @unpack
    @data(
        ({'date_of_birth': '1980-07-21'}, [], 'DUE_TO_AGE', {'status': ParticipantStatus.ROUTINE, 'next_test_due_date': '2020-11-25', 'explanation': ['CEASED_DUE_TO_AGE', 'NO_PREVIOUS_TEST']}),  
        ({'date_of_birth': '1980-07-21'}, [{'test_date': '2019-06-13', 'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'}], 'DUE_TO_AGE', {'status': ParticipantStatus.ROUTINE, 'next_test_due_date': '2020-11-25', 'explanation': ['CEASED_DUE_TO_AGE', 'STATUS_NOT_ROUTINE']}),  
        ({'date_of_birth': '1980-07-21'}, [{'test_date': '2019-06-13', 'result_code': 'X', 'infection_code': '0', 'action_code': 'A'}], 'DUE_TO_AGE', {'status': ParticipantStatus.ROUTINE, 'next_test_due_date': '2020-11-25', 'explanation': ['CEASED_DUE_TO_AGE', 'UNDER_60']}),  
        ({'date_of_birth': '1947-07-21'}, [{'test_date': '2019-06-13', 'result_code': 'X', 'infection_code': '0', 'action_code': 'A'}], 'DUE_TO_AGE',  {'status': None, 'explanation': ['CEASED_DUE_TO_AGE', 'OVER_59']}),  
        ({'date_of_birth': '1980-07-21'}, [], 'NO_CERVIX', {'status': ParticipantStatus.CALLED, 'next_test_due_date': '2020-11-25', 'explanation': ['NO_PREVIOUS_TEST']}),  
        ({'date_of_birth': '1997-07-21'}, [], 'NO_CERVIX', {'status': ParticipantStatus.CALLED, 'next_test_due_date': '2022-03-03', 'explanation': ['NO_PREVIOUS_TEST']}),  
        ({'date_of_birth': '1980-07-21'}, [{'test_date': '2019-06-13', 'result_code': '3', 'infection_code': '9', 'action_code': 'S'}], 'MENTAL_CAPACITY_ACT', {'status': ParticipantStatus.SUSPENDED, 'next_test_due_date': '2020-11-25', 'explanation': ['SUSPENDED_STATUS']}),  
        ({'date_of_birth': '1980-07-21'}, [{'test_date': '2020-08-21', 'result_code': '3', 'infection_code': '9', 'action_code': 'S'}], 'MENTAL_CAPACITY_ACT', {'status': ParticipantStatus.SUSPENDED, 'next_test_due_date': '2021-08-21', 'explanation': ['SUSPENDED_STATUS']}),  
        ({'date_of_birth': '1980-07-21'}, [{'test_date': '2020-09-14', 'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'}], 'PATIENT_INFORMED_CHOICE', {'status': ParticipantStatus.INADEQUATE, 'next_test_due_date': '2020-12-14', 'explanation': ['INADEQUATE_STATUS']}),  
        ({'date_of_birth': '1980-07-21'}, [{'test_date': '2020-09-14', 'result_code': '2', 'infection_code': '9', 'action_code': 'R', 'recall_months': '9'}], 'RADIOTHERAPY', {'status': ParticipantStatus.REPEAT_ADVISED, 'next_test_due_date': '2021-06-14', 'explanation': ['REPEAT_ADVISED_STATUS']}),  
        ({'date_of_birth': '1980-07-21'}, [{'test_date': '2019-06-13', 'result_code': 'X', 'infection_code': '0', 'action_code': 'A'}], 'NO_CERVIX', {'status': ParticipantStatus.ROUTINE, 'next_test_due_date': '2022-06-13', 'explanation': ['ROUTINE_STATUS', 'UNDER_50']}),  
        ({'date_of_birth': '1968-07-21'}, [{'test_date': '2019-06-13', 'result_code': 'X', 'infection_code': '0', 'action_code': 'A'}], 'NO_CERVIX', {'status': ParticipantStatus.ROUTINE, 'next_test_due_date': '2024-06-13', 'explanation': ['ROUTINE_STATUS', 'BETWEEN_50_AND_59']}),  
        ({'date_of_birth': '1948-07-21'}, [{'test_date': '2019-06-13', 'result_code': 'X', 'infection_code': '0', 'action_code': 'A'}], 'NO_CERVIX', {'status': None, 'explanation': ['ROUTINE_STATUS', 'OVER_59']})  
    )
    @patch.object(calculator, 'datetime', Mock(wraps=datetime))
    def test_calculate_next_test_due_date_and_status_for_ceased_participant_without_mocking_methods(
            self, participant, test_results, cease_reason, expected_response):
        calculator.datetime.now.return_value = self.FIXED_NOW_TIME
        actual_response = calculator.calculate_next_test_due_date_and_status_for_ceased_participant(participant,
                                                                                                    test_results,
                                                                                                    cease_reason)
        self.assertEqual(expected_response, actual_response)
