from datetime import datetime, date, timezone
from unittest import TestCase
from mock import patch
from ddt import ddt, data, unpack
import common.utils.next_test_due_date_calculators.reinstate_calculator as calculator
from common.models.participant import CeaseReason, ParticipantStatus
from common.services.reinstate.reinstate_response import ReinstateResponse
from common.services.reinstate.reinstate_result import ReinstateResult


MOCK_PARTICIPANT = {
    'participant_id': 'participant_1',
    'date_of_birth': '1980-01-01'
}

MOCK_ENV_VARS = {
    "POSITIVE_FAILSAFE_REPEAT_MONTHS": '11',
    "NEGATIVE_FAILSAFE_REPEAT_MONTHS": '10'
}


@patch('os.environ', MOCK_ENV_VARS)
@ddt
class TestReinstateCalculatorManualReinsate(TestCase):

    module = 'common.utils.next_test_due_date_calculators.reinstate_calculator'
    ntdd = 'common.utils.next_test_due_date_calculators.next_test_due_date_calculator'

    FIXED_NOW_TIME = datetime(2020, 9, 15, tzinfo=timezone.utc)
    FIXED_NOW_DATE = datetime.strptime('2020-09-15', '%Y-%m-%d').date()

    @patch(f'{module}.datetime')
    @patch(f'{module}.use_manual_reinstate_strategy')
    def test_calculate_for_manual_reinstate_basic_operation(
            self,
            use_manual_reinstate_strategy_mock,
            datetime_mock):
        # Given
        mock_reinstate_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED
        )
        use_manual_reinstate_strategy_mock.return_value = mock_reinstate_response
        participant = MOCK_PARTICIPANT
        test_results = []
        cease_reason = CeaseReason.DUE_TO_AGE
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        expected_result = mock_reinstate_response
        # When
        actual_result = calculator.calculate_for_manual_reinstate(participant, test_results, cease_reason)
        # Then
        self.assertEqual(expected_result, actual_result)
        use_manual_reinstate_strategy_mock.assert_called_once_with(participant, test_results, cease_reason)

    @patch(f'{module}.datetime')
    @patch(f'{module}.use_manual_reinstate_strategy')
    def test_calculate_for_manual_reinstate_handles_exception(
            self,
            use_manual_reinstate_strategy_mock,
            datetime_mock):
        # Given
        use_manual_reinstate_strategy_mock.side_effect = Exception('something went wrong')
        participant = MOCK_PARTICIPANT
        test_results = []
        cease_reason = CeaseReason.DUE_TO_AGE
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        expected_result = ReinstateResponse(
            result=ReinstateResult.ERROR,
            error=True
        )
        # When
        actual_result = calculator.calculate_for_manual_reinstate(participant, test_results, cease_reason)
        # Then
        self.assertEqual(expected_result, actual_result)
        use_manual_reinstate_strategy_mock.assert_called_once_with(participant, test_results, cease_reason)

    @unpack
    @data(
        ('1980-01-01', CeaseReason.DUE_TO_AGE,              40),
        ('1960-01-01', CeaseReason.PATIENT_INFORMED_CHOICE, 60)
    )
    @patch(f'{ntdd}.datetime')
    @patch(f'{module}.manual_suspended_strategy')
    @patch(f'{module}.manual_inadequate_strategy')
    @patch(f'{module}.manual_repeat_advised_strategy')
    @patch(f'{module}.manual_routine_normal_strategy')
    @patch(f'{module}.manual_no_previous_test_strategy')
    @patch(f'{module}.calculate_status_from_latest_test_result_for_manual_reinstate')
    @patch(f'{module}.manual_due_to_age_strategy')
    @patch(f'{module}.calculate_status_from_last_3_test_results_for_manual_reinstate')
    def test_use_manual_reinstate_strategy_calls_manual_due_to_age_strategy(
            self,
            date_of_birth, cease_reason, expected_age,
            calculate_status_from_last_3_test_results_for_manual_reinstate_mock,
            manual_due_to_age_strategy_mock,
            calculate_status_from_latest_test_result_for_manual_reinstate_mock,
            manual_no_previous_test_strategy_mock,
            manual_routine_normal_strategy_mock,
            manual_repeat_advised_strategy_mock,
            manual_inadequate_strategy_mock,
            manual_suspended_strategy_mock,
            datetime_mock):
        # Given
        mock_reinstate_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED
        )
        calculate_status_from_last_3_test_results_for_manual_reinstate_mock.return_value = ParticipantStatus.REPEAT_ADVISED  
        manual_due_to_age_strategy_mock.return_value = mock_reinstate_response
        participant = MOCK_PARTICIPANT.copy()
        participant['date_of_birth'] = date_of_birth
        test_results = []
        expected_result = mock_reinstate_response
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        # When
        actual_result = calculator.use_manual_reinstate_strategy(participant, test_results, cease_reason)
        # Then
        self.assertEqual(expected_result, actual_result)
        calculate_status_from_last_3_test_results_for_manual_reinstate_mock.assert_called_once_with(test_results)
        manual_due_to_age_strategy_mock.assert_called_once_with(ParticipantStatus.REPEAT_ADVISED, expected_age)
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.assert_not_called()
        manual_no_previous_test_strategy_mock.assert_not_called()
        manual_routine_normal_strategy_mock.assert_not_called()
        manual_repeat_advised_strategy_mock.assert_not_called()
        manual_inadequate_strategy_mock.assert_not_called()
        manual_suspended_strategy_mock.assert_not_called()

    @patch(f'{ntdd}.datetime')
    @patch(f'{module}.manual_suspended_strategy')
    @patch(f'{module}.manual_inadequate_strategy')
    @patch(f'{module}.manual_repeat_advised_strategy')
    @patch(f'{module}.manual_routine_normal_strategy')
    @patch(f'{module}.manual_no_previous_test_strategy')
    @patch(f'{module}.calculate_status_from_latest_test_result_for_manual_reinstate')
    @patch(f'{module}.manual_due_to_age_strategy')
    @patch(f'{module}.calculate_status_from_last_3_test_results_for_manual_reinstate')
    def test_use_manual_reinstate_strategy_calls_manual_no_previous_test_strategy(
            self,
            calculate_status_from_last_3_test_results_for_manual_reinstate_mock,
            manual_due_to_age_strategy_mock,
            calculate_status_from_latest_test_result_for_manual_reinstate_mock,
            manual_no_previous_test_strategy_mock,
            manual_routine_normal_strategy_mock,
            manual_repeat_advised_strategy_mock,
            manual_inadequate_strategy_mock,
            manual_suspended_strategy_mock,
            datetime_mock):
        # Given
        mock_reinstate_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED
        )
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.return_value = None, None
        manual_no_previous_test_strategy_mock.return_value = mock_reinstate_response
        cease_reason = CeaseReason.PATIENT_INFORMED_CHOICE
        participant = MOCK_PARTICIPANT
        test_results = []
        expected_result = mock_reinstate_response
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        expected_dob = date(1980, 1, 1)
        # When
        actual_result = calculator.use_manual_reinstate_strategy(participant, test_results, cease_reason)
        # Then
        self.assertEqual(expected_result, actual_result)
        calculate_status_from_last_3_test_results_for_manual_reinstate_mock.assert_not_called()
        manual_due_to_age_strategy_mock.assert_not_called()
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.assert_called_once_with(test_results)
        manual_no_previous_test_strategy_mock.assert_called_once_with(expected_dob)
        manual_routine_normal_strategy_mock.assert_not_called()
        manual_repeat_advised_strategy_mock.assert_not_called()
        manual_inadequate_strategy_mock.assert_not_called()
        manual_suspended_strategy_mock.assert_not_called()

    @patch(f'{ntdd}.datetime')
    @patch(f'{module}.manual_suspended_strategy')
    @patch(f'{module}.manual_inadequate_strategy')
    @patch(f'{module}.manual_repeat_advised_strategy')
    @patch(f'{module}.manual_routine_normal_strategy')
    @patch(f'{module}.manual_no_previous_test_strategy')
    @patch(f'{module}.calculate_status_from_latest_test_result_for_manual_reinstate')
    @patch(f'{module}.manual_due_to_age_strategy')
    @patch(f'{module}.calculate_status_from_last_3_test_results_for_manual_reinstate')
    def test_use_manual_reinstate_strategy_calls_manual_routine_normal_strategy(
            self,
            calculate_status_from_last_3_test_results_for_manual_reinstate_mock,
            manual_due_to_age_strategy_mock,
            calculate_status_from_latest_test_result_for_manual_reinstate_mock,
            manual_no_previous_test_strategy_mock,
            manual_routine_normal_strategy_mock,
            manual_repeat_advised_strategy_mock,
            manual_inadequate_strategy_mock,
            manual_suspended_strategy_mock,
            datetime_mock):
        # Given
        mock_reinstate_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED
        )
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.return_value = ParticipantStatus.ROUTINE, 0
        manual_routine_normal_strategy_mock.return_value = mock_reinstate_response
        cease_reason = CeaseReason.PATIENT_INFORMED_CHOICE
        participant = MOCK_PARTICIPANT
        RESULT_1 = {'result': '1'}
        RESULT_2 = {'result': '2'}
        test_results = [RESULT_1, RESULT_2]
        expected_result = mock_reinstate_response
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        expected_dob = date(1980, 1, 1)
        # When
        actual_result = calculator.use_manual_reinstate_strategy(participant, test_results, cease_reason)
        # Then
        self.assertEqual(expected_result, actual_result)
        calculate_status_from_last_3_test_results_for_manual_reinstate_mock.assert_not_called()
        manual_due_to_age_strategy_mock.assert_not_called()
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.assert_called_once_with(test_results)
        manual_no_previous_test_strategy_mock.assert_not_called()
        manual_routine_normal_strategy_mock.assert_called_once_with(expected_dob, RESULT_1)
        manual_repeat_advised_strategy_mock.assert_not_called()
        manual_inadequate_strategy_mock.assert_not_called()
        manual_suspended_strategy_mock.assert_not_called()

    @patch(f'{ntdd}.datetime')
    @patch(f'{module}.manual_suspended_strategy')
    @patch(f'{module}.manual_inadequate_strategy')
    @patch(f'{module}.manual_repeat_advised_strategy')
    @patch(f'{module}.manual_routine_normal_strategy')
    @patch(f'{module}.manual_no_previous_test_strategy')
    @patch(f'{module}.calculate_status_from_latest_test_result_for_manual_reinstate')
    @patch(f'{module}.manual_due_to_age_strategy')
    @patch(f'{module}.calculate_status_from_last_3_test_results_for_manual_reinstate')
    def test_use_manual_reinstate_strategy_calls_manual_repeat_advised_strategy(
            self,
            calculate_status_from_last_3_test_results_for_manual_reinstate_mock,
            manual_due_to_age_strategy_mock,
            calculate_status_from_latest_test_result_for_manual_reinstate_mock,
            manual_no_previous_test_strategy_mock,
            manual_routine_normal_strategy_mock,
            manual_repeat_advised_strategy_mock,
            manual_inadequate_strategy_mock,
            manual_suspended_strategy_mock,
            datetime_mock):
        # Given
        mock_reinstate_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED
        )
        result_index = 0
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.return_value = ParticipantStatus.REPEAT_ADVISED, result_index  
        manual_repeat_advised_strategy_mock.return_value = mock_reinstate_response
        cease_reason = CeaseReason.NO_CERVIX
        participant = MOCK_PARTICIPANT
        RESULT_1 = {'result': '1'}
        RESULT_2 = {'result': '2'}
        test_results = [RESULT_1, RESULT_2]
        expected_result = mock_reinstate_response
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        # When
        actual_result = calculator.use_manual_reinstate_strategy(participant, test_results, cease_reason)
        # Then
        self.assertEqual(expected_result, actual_result)
        calculate_status_from_last_3_test_results_for_manual_reinstate_mock.assert_not_called()
        manual_due_to_age_strategy_mock.assert_not_called()
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.assert_called_once_with(test_results)
        manual_no_previous_test_strategy_mock.assert_not_called()
        manual_routine_normal_strategy_mock.assert_not_called()
        manual_repeat_advised_strategy_mock.assert_called_once_with(test_results[result_index])
        manual_inadequate_strategy_mock.assert_not_called()
        manual_suspended_strategy_mock.assert_not_called()

    @patch(f'{ntdd}.datetime')
    @patch(f'{module}.manual_suspended_strategy')
    @patch(f'{module}.manual_inadequate_strategy')
    @patch(f'{module}.manual_repeat_advised_strategy')
    @patch(f'{module}.manual_routine_normal_strategy')
    @patch(f'{module}.manual_no_previous_test_strategy')
    @patch(f'{module}.calculate_status_from_latest_test_result_for_manual_reinstate')
    @patch(f'{module}.manual_due_to_age_strategy')
    @patch(f'{module}.calculate_status_from_last_3_test_results_for_manual_reinstate')
    def test_use_manual_reinstate_strategy_calls_manual_inadequate_strategy(
            self,
            calculate_status_from_last_3_test_results_for_manual_reinstate_mock,
            manual_due_to_age_strategy_mock,
            calculate_status_from_latest_test_result_for_manual_reinstate_mock,
            manual_no_previous_test_strategy_mock,
            manual_routine_normal_strategy_mock,
            manual_repeat_advised_strategy_mock,
            manual_inadequate_strategy_mock,
            manual_suspended_strategy_mock,
            datetime_mock):
        # Given
        mock_reinstate_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED
        )
        result_index = 0
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.return_value = ParticipantStatus.INADEQUATE, result_index  
        manual_inadequate_strategy_mock.return_value = mock_reinstate_response
        cease_reason = CeaseReason.RADIOTHERAPY
        participant = MOCK_PARTICIPANT
        RESULT_1 = {'result': '1'}
        RESULT_2 = {'result': '2'}
        test_results = [RESULT_1, RESULT_2]
        expected_result = mock_reinstate_response
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        # When
        actual_result = calculator.use_manual_reinstate_strategy(participant, test_results, cease_reason)
        # Then
        self.assertEqual(expected_result, actual_result)
        calculate_status_from_last_3_test_results_for_manual_reinstate_mock.assert_not_called()
        manual_due_to_age_strategy_mock.assert_not_called()
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.assert_called_once_with(test_results)
        manual_no_previous_test_strategy_mock.assert_not_called()
        manual_routine_normal_strategy_mock.assert_not_called()
        manual_repeat_advised_strategy_mock.assert_not_called()
        manual_inadequate_strategy_mock.assert_called_once_with(participant, test_results[result_index])
        manual_suspended_strategy_mock.assert_not_called()

    @patch(f'{ntdd}.datetime')
    @patch(f'{module}.manual_suspended_strategy')
    @patch(f'{module}.manual_inadequate_strategy')
    @patch(f'{module}.manual_repeat_advised_strategy')
    @patch(f'{module}.manual_routine_normal_strategy')
    @patch(f'{module}.manual_no_previous_test_strategy')
    @patch(f'{module}.calculate_status_from_latest_test_result_for_manual_reinstate')
    @patch(f'{module}.manual_due_to_age_strategy')
    @patch(f'{module}.calculate_status_from_last_3_test_results_for_manual_reinstate')
    def test_use_manual_reinstate_strategy_calls_manual_suspended_strategy(
            self,
            calculate_status_from_last_3_test_results_for_manual_reinstate_mock,
            manual_due_to_age_strategy_mock,
            calculate_status_from_latest_test_result_for_manual_reinstate_mock,
            manual_no_previous_test_strategy_mock,
            manual_routine_normal_strategy_mock,
            manual_repeat_advised_strategy_mock,
            manual_inadequate_strategy_mock,
            manual_suspended_strategy_mock,
            datetime_mock):
        # Given
        mock_reinstate_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED
        )
        result_index = 0
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.return_value = ParticipantStatus.SUSPENDED, result_index  
        manual_suspended_strategy_mock.return_value = mock_reinstate_response
        cease_reason = CeaseReason.MENTAL_CAPACITY_ACT
        participant = MOCK_PARTICIPANT
        RESULT_1 = {'result': '1'}
        RESULT_2 = {'result': '2'}
        test_results = [RESULT_1, RESULT_2]
        expected_result = mock_reinstate_response
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        # When
        actual_result = calculator.use_manual_reinstate_strategy(participant, test_results, cease_reason)
        # Then
        self.assertEqual(expected_result, actual_result)
        calculate_status_from_last_3_test_results_for_manual_reinstate_mock.assert_not_called()
        manual_due_to_age_strategy_mock.assert_not_called()
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.assert_called_once_with(test_results)
        manual_no_previous_test_strategy_mock.assert_not_called()
        manual_routine_normal_strategy_mock.assert_not_called()
        manual_repeat_advised_strategy_mock.assert_not_called()
        manual_inadequate_strategy_mock.assert_not_called()
        manual_suspended_strategy_mock.assert_called_once_with(participant, test_results[result_index])

    @patch(f'{ntdd}.datetime')
    @patch(f'{module}.manual_suspended_strategy')
    @patch(f'{module}.manual_inadequate_strategy')
    @patch(f'{module}.manual_repeat_advised_strategy')
    @patch(f'{module}.manual_routine_normal_strategy')
    @patch(f'{module}.manual_no_previous_test_strategy')
    @patch(f'{module}.calculate_status_from_latest_test_result_for_manual_reinstate')
    @patch(f'{module}.manual_due_to_age_strategy')
    @patch(f'{module}.calculate_status_from_last_3_test_results_for_manual_reinstate')
    def test_use_manual_reinstate_strategy_defaults_to_unknown(
            self,
            calculate_status_from_last_3_test_results_for_manual_reinstate_mock,
            manual_due_to_age_strategy_mock,
            calculate_status_from_latest_test_result_for_manual_reinstate_mock,
            manual_no_previous_test_strategy_mock,
            manual_routine_normal_strategy_mock,
            manual_repeat_advised_strategy_mock,
            manual_inadequate_strategy_mock,
            manual_suspended_strategy_mock,
            datetime_mock):
        # Given
        mock_reinstate_response = ReinstateResponse(result=ReinstateResult.UNKNOWN_STRATEGY, error=True)
        cease_reason = 'some other unknown reason'
        participant = MOCK_PARTICIPANT
        test_results = []
        expected_result = mock_reinstate_response
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        # When
        actual_result = calculator.use_manual_reinstate_strategy(participant, test_results, cease_reason)
        # Then
        self.assertEqual(expected_result, actual_result)
        calculate_status_from_last_3_test_results_for_manual_reinstate_mock.assert_not_called()
        manual_due_to_age_strategy_mock.assert_not_called()
        calculate_status_from_latest_test_result_for_manual_reinstate_mock.assert_not_called()
        manual_no_previous_test_strategy_mock.assert_not_called()
        manual_routine_normal_strategy_mock.assert_not_called()
        manual_repeat_advised_strategy_mock.assert_not_called()
        manual_inadequate_strategy_mock.assert_not_called()
        manual_suspended_strategy_mock.assert_not_called()

    @unpack
    @data(
        # def_ntdd     result_status                     age  reinstate status           explanation                                  ntdd                
        ('2021-01-01', None,                             40,  ParticipantStatus.CALLED,  ['CEASED_DUE_TO_AGE', 'NO_PREVIOUS_TEST'],   date(2021, 1, 1)),  
        ('2021-01-01', ParticipantStatus.REPEAT_ADVISED, 40,  ParticipantStatus.ROUTINE, ['CEASED_DUE_TO_AGE', 'STATUS_NOT_ROUTINE'], date(2021, 1, 1)),  
        ('2021-01-01', ParticipantStatus.ROUTINE,        60,  None,                      ['CEASED_DUE_TO_AGE', 'OVER_59'],            None)               
    )
    @patch(f'{ntdd}.datetime')
    @patch(f'{module}.default_next_test_due_date')
    def test_manual_due_to_age_strategy(
            self,
            default_ntdd, recent_test_result_status, participant_age,
            expected_status, expected_explanation, expected_ntdd,
            default_next_test_due_date_mock,
            datetime_mock):
        # Given
        default_next_test_due_date_mock.return_value = datetime.strptime(default_ntdd, '%Y-%m-%d').date()
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        # When
        actual_response = calculator.manual_due_to_age_strategy(recent_test_result_status, participant_age)
        # Then
        self.assertEqual(actual_response.status, expected_status)
        self.assertEqual(actual_response.next_test_due_date, expected_ntdd)
        self.assertEqual(actual_response.explanation, expected_explanation)

    @unpack
    @data(
        # dob          reinstate status           explanation           ntdd
        ('1996-09-15', ParticipantStatus.CALLED,  ['NO_PREVIOUS_TEST'], date(2021,  4, 28)),
        ('1995-09-15', ParticipantStatus.CALLED,  ['NO_PREVIOUS_TEST'], date(2020, 11, 25)),
    )
    @patch(f'{ntdd}.datetime')
    def test_manual_no_previous_test_strategy(
            self,
            date_of_birth, expected_status, expected_explanation, expected_ntdd,
            datetime_mock):
        # Given
        date_of_birth_date = datetime.strptime(date_of_birth, '%Y-%m-%d').date()
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        # When
        actual_response = calculator.manual_no_previous_test_strategy(date_of_birth_date)
        # Then
        self.assertEqual(actual_response.status, expected_status)
        self.assertEqual(actual_response.next_test_due_date, expected_ntdd)
        self.assertEqual(actual_response.explanation, expected_explanation)

    @unpack
    @data(
        # dob           test_result                  reinstate status            explanation                              ntdd                
        ('1980-01-01',  {'test_date': '2021-01-01'}, ParticipantStatus.ROUTINE,  ['ROUTINE_STATUS', 'UNDER_50'],          date(2024, 1, 1)),  
        ('1970-01-01',  {'test_date': '2021-01-01'}, ParticipantStatus.ROUTINE,  ['ROUTINE_STATUS', 'BETWEEN_50_AND_59'], date(2026, 1, 1)),  
        ('1960-01-01',  {'test_date': '2021-01-01'}, None,                       ['ROUTINE_STATUS', 'OVER_59'],           None)               
    )
    @patch(f'{ntdd}.datetime')
    def test_manual_routine_normal_strategy(
            self,
            date_of_birth, recent_test_result,
            expected_status, expected_explanation, expected_ntdd,
            datetime_mock):
        # Given
        date_of_birth_date = datetime.strptime(date_of_birth, '%Y-%m-%d').date()
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        # When
        actual_response = calculator.manual_routine_normal_strategy(date_of_birth_date, recent_test_result)
        # Then
        self.assertEqual(actual_response.status, expected_status)
        self.assertEqual(actual_response.next_test_due_date, expected_ntdd)
        self.assertEqual(actual_response.explanation, expected_explanation)

    @unpack
    @data(
        # test_result                                        status                             explanation                ntdd                
        ({'test_date': '2022-01-01', 'recall_months': '3'},  ParticipantStatus.REPEAT_ADVISED,  ['REPEAT_ADVISED_STATUS'], date(2022, 4, 1)),  
        ({'test_date': '2022-01-01', 'recall_months': '36'}, ParticipantStatus.REPEAT_ADVISED,  ['REPEAT_ADVISED_STATUS'], date(2025, 1, 1))   
    )
    @patch(f'{ntdd}.datetime')
    def test_manual_repeat_advised_strategy(
            self,
            recent_test_result, expected_status, expected_explanation, expected_ntdd,
            datetime_mock):
        # Given
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        # When
        actual_response = calculator.manual_repeat_advised_strategy(recent_test_result)
        # Then
        self.assertEqual(actual_response.status, expected_status)
        self.assertEqual(actual_response.next_test_due_date, expected_ntdd)
        self.assertEqual(actual_response.explanation, expected_explanation)

    @unpack
    @data(
        # test_date    result infection action status                         explanation            ntdd
        ('2022-01-01', 'X',   'U',      'R',   ParticipantStatus.INADEQUATE,  ['INADEQUATE_STATUS'], date(2022, 4, 1)),  
        ('2022-01-01', '1',   '9',      'R',   ParticipantStatus.INADEQUATE,  ['INADEQUATE_STATUS'], date(2022, 4, 1)),  
    )
    @patch(f'{ntdd}.datetime')
    def test_manual_inadequate_strategy(
            self,
            test_date, result_code, infection_code, action_code,
            expected_status, expected_explanation, expected_ntdd,
            datetime_mock):
        # Given
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        participant = MOCK_PARTICIPANT.copy()
        result = {
            'test_date': test_date,
            'result_code': result_code,
            'infection_code': infection_code,
            'action_code': action_code
        }

        # When
        actual_response = calculator.manual_inadequate_strategy(participant, result)
        # Then
        self.assertEqual(actual_response.status, expected_status)
        self.assertEqual(actual_response.next_test_due_date, expected_ntdd)
        self.assertEqual(actual_response.explanation, expected_explanation)

    @unpack
    @data(
        # test_date    result infection action status                        explanation           ntdd
        ('2022-01-01', 'X',   '0',      'S',   ParticipantStatus.SUSPENDED,  ['SUSPENDED_STATUS'], date(2022, 11, 1)),  
        ('2022-01-01', '9',   '9',      'S',   ParticipantStatus.SUSPENDED,  ['SUSPENDED_STATUS'], date(2022, 12, 1)),  
    )
    @patch(f'{ntdd}.datetime')
    def test_manual_suspended_strategy(
            self,
            test_date, result_code, infection_code, action_code,
            expected_status, expected_explanation, expected_ntdd,
            datetime_mock):
        # Given
        datetime_mock.now.return_value = self.FIXED_NOW_TIME
        participant = MOCK_PARTICIPANT.copy()
        result = {
            'test_date': test_date,
            'result_code': result_code,
            'infection_code': infection_code,
            'action_code': action_code
        }

        # When
        actual_response = calculator.manual_suspended_strategy(participant, result)
        # Then
        self.assertEqual(actual_response.status, expected_status)
        self.assertEqual(actual_response.next_test_due_date, expected_ntdd)
        self.assertEqual(actual_response.explanation, expected_explanation)
