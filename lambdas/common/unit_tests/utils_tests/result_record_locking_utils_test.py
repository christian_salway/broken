from common.log import CommonLogReference
import common.utils.result_record_locking_utils as result_record_locking_utils
from dateutil.relativedelta import relativedelta
from datetime import datetime, timezone
from ddt import ddt, data, unpack
from mock import patch
from unittest import TestCase

from common.models.result import ResultWorkflowState
from common.utils.dynamodb_access.table_names import TableNames


RESULT_ID = 'TEST_RESULT'
RECEIVED_TIME = '2021-01-27T16:14:59.874365'
USER_ID = 'testuser'
LOCK_TIMESTAMP = '2021-01-01T00:00:00'
USER_NAME = 'Test User'
USER_ROLE = 'ABC123'


@ddt
class TestResultRecordLocking(TestCase):

    module = 'common.utils.result_record_locking_utils'

    @patch(f'{module}.datetime')
    @patch(f'{module}.log')
    @patch(f'{module}.dynamodb_update_item')
    def test_lock_result(self, dynamodb_update_mock, log_mock, time_mock):

        # Arrange

        time_mock.now.return_value = datetime(2021, 1, 1)
        dynamodb_update_mock.return_value = {'success': 'true'}
        test_user = {
            'id': USER_ID,
            'name': USER_NAME,
            'role': USER_ROLE
        }

        # Act

        result_record_locking_utils._lock_result(RESULT_ID, RECEIVED_TIME, test_user)

        # Assert

        dynamodb_update_mock.assert_called_with(TableNames.RESULTS, dict(
            Key={
                'result_id': 'TEST_RESULT',
                'received_time': '2021-01-27T16:14:59.874365'
            },
            UpdateExpression='SET #user_id = :user_id, #lock_timestamp = :lock_timestamp, #role_id = :role_id, #user_name = :user_name',  
            ExpressionAttributeValues={
                ':user_id': 'testuser',
                ':lock_timestamp': '2021-01-01T00:00:00',
                ':role_id': 'ABC123',
                ':user_name': 'Test User'
            },
            ExpressionAttributeNames={
                '#user_id': 'user_id',
                '#lock_timestamp': 'lock_timestamp',
                '#role_id': 'role_id',
                '#user_name': 'user_name'
            },
            ReturnValues='NONE'
        ))
        log_mock.assert_called_with({
            'log_reference': CommonLogReference.RESULTLOCK0010
        })

    @patch(f'{module}.datetime')
    @patch(f'{module}.log')
    @patch(f'{module}.dynamodb_update_item')
    def test_lock_result_unhappy_path(self, dynamodb_update_mock, log_mock, time_mock):

        # Arrange

        time_mock.now.return_value = datetime(2021, 1, 1)
        dynamodb_update_mock.side_effect = Exception('Something went wrong!')
        test_user = {
            'id': USER_ID,
            'role': USER_ROLE,
            'name': USER_NAME
        }

        # Act

        with self.assertRaises(Exception) as context:
            result_record_locking_utils._lock_result(RESULT_ID, RECEIVED_TIME, test_user)

        # Assert

        self.assertTrue('Something went wrong!' in str(context.exception))
        dynamodb_update_mock.assert_called_with(TableNames.RESULTS, dict(
            Key={
                'result_id': 'TEST_RESULT', 'received_time': '2021-01-27T16:14:59.874365'
            },
            UpdateExpression='SET #user_id = :user_id, #lock_timestamp = :lock_timestamp, #role_id = :role_id, #user_name = :user_name',  
            ExpressionAttributeValues={
                ':user_id': 'testuser',
                ':lock_timestamp': '2021-01-01T00:00:00',
                ':role_id': 'ABC123',
                ':user_name': 'Test User'
            },
            ExpressionAttributeNames={
                '#user_id': 'user_id',
                '#lock_timestamp': 'lock_timestamp',
                '#role_id': 'role_id',
                '#user_name': 'user_name'
            },
            ReturnValues='NONE'
        ))
        log_mock.assert_called_with({
            'log_reference': CommonLogReference.RESULTLOCK0008
        })

    @patch(f'{module}.log')
    @patch(f'{module}.dynamodb_query')
    def test_get_current_lock_state(self, dynamodb_query_mock, log_mock):

        # Arrange

        dynamodb_query_mock.return_value = [
            {
                'user_id': USER_ID,
                'lock_timestamp': LOCK_TIMESTAMP,
                'received_time': RECEIVED_TIME,
                'workflow_state': ResultWorkflowState.PROCESS,
                'workflow_history': []
            }
        ]

        expected_result = {
            'user_id': USER_ID,
            'lock_timestamp': LOCK_TIMESTAMP,
            'received_time': RECEIVED_TIME,
            'workflow_state': ResultWorkflowState.PROCESS,
            'workflow_history': []
        }

        expected_log_call = {
            'log_reference': CommonLogReference.RESULTLOCK0003,
            'result_id': RESULT_ID
        }

        # Act

        actual_result = result_record_locking_utils._get_current_result_lock_state(RESULT_ID)

        # Asset

        self.assertEqual(actual_result, expected_result)
        dynamodb_query_mock.assert_called_once()
        log_mock.assert_called_with(expected_log_call)

    @unpack
    @data(
        (None, None),
        (
            RESULT_ID,
            {}
        ),
        (
            RESULT_ID,
            []
        )
    )
    @patch(f'{module}.log')
    @patch(f'{module}.dynamodb_query')
    def test_get_current_lock_state_unhappy_path(self, result_id, dynamodb_response, dynamodb_query_mock, log_mock):

        # Arrange

        dynamodb_query_mock.return_value = dynamodb_response

        # Act

        with self.assertRaises(result_record_locking_utils.LockException) as context:
            result_record_locking_utils._get_current_result_lock_state(result_id)

        # Assert

        self.assertIsNotNone(context.exception)
        log_mock.assert_called_once()

    @unpack
    @data(
        (
            USER_ID,
            {},
            True
        ),
        (
            USER_ID,
            {
                'user_id': USER_ID
            },
            True
        ),
        (
            USER_ID,
            {
                'user_id': 'other_user',
                'lock_timestamp': (datetime.now(timezone.utc) - relativedelta(minutes=3)).isoformat()
            },
            False
        ),
        (
            USER_ID,
            {
                'user_id': 'other_user',
                'lock_timestamp': (datetime.now(timezone.utc) - relativedelta(minutes=11)).isoformat()
            },
            True
        )
    )
    def test_is_lock_valid_for_user(self, user_id, lock_state, expected_result):

        # Arrange

        # Act

        actual_result = result_record_locking_utils._is_lock_valid_for_user(lock_state, {'id': USER_ID})

        # Assert

        self.assertEqual(actual_result, expected_result)

    @patch(f'{module}.log')
    def test_is_lock_valid_for_user_unhappy_path(self, log_mock):

        # Arrange

        lock_state = {
            'user_id': 'other_user'
        }

        expected_log_call = {'log_reference': CommonLogReference.RESULTLOCK0005}

        # Act

        with self.assertRaises(result_record_locking_utils.LockException) as context:
            result_record_locking_utils._is_lock_valid_for_user(lock_state, {'id': USER_ID})

        # Assert

        self.assertIsNotNone(context.exception)
        log_mock.assert_called_with(expected_log_call)

    @patch(f'{module}._lock_result')
    @patch(f'{module}._get_current_result_lock_state')
    def test_acquire_lock(self, lock_state_mock, lock_result_mock):

        # Arrange

        args = (
            {
                'session': {
                    'user_data': {
                        'nhsid_useruid': USER_ID,
                        'first_name': 'Person',
                        'last_name': 'O\'Person-Human',
                    },
                    'selected_role': {
                        'role_id': USER_ROLE
                    }
                },
                'body': {
                    'result_id': RESULT_ID
                }
            }
        )

        lock_timestamp = (datetime.now(timezone.utc) - relativedelta(minutes=3)).isoformat()
        lock_state_mock.return_value = {
            'user_id': USER_ID,
            'lock_timestamp': lock_timestamp,
            'received_time': RECEIVED_TIME
        }

        # Act

        @result_record_locking_utils.do_acquire_lock
        def dummy_function(*called_args):
            self.assertEqual(args, *called_args)

        dummy_function(args)

        # Assert

        lock_state_mock.assert_called_once_with(RESULT_ID)
        lock_result_mock.assert_called_once_with(
            RESULT_ID,
            RECEIVED_TIME,
            {
                'name': "Person O'Person-Human",
                'id': 'testuser',
                'role': 'ABC123'
            }
        )

    @unpack
    @data(
        (
            {},
        ),
        (
            {
                'session': {}
            },
        )
    )
    @patch(f'{module}.log')
    def test_acquire_lock_missing_data(self, event_data, log_mock):

        # Arrange

        args = (event_data)
        expected_log_call = {'log_reference': CommonLogReference.RESULTLOCK0001}

        # Act

        @result_record_locking_utils.do_acquire_lock
        def dummy_function(*called_args):
            pass

        with self.assertRaises(result_record_locking_utils.LockException) as context:
            dummy_function(args)

        # Assert

        self.assertIsNotNone(context.exception)
        log_mock.assert_called_with(expected_log_call)

    @patch(f'{module}.log')
    @patch(f'{module}.dynamodb_update_item')
    def test_release_lock_dynamodb(self, dynamodb_update_mock, log_mock):

        # Arrange

        dynamodb_update_mock.return_value = {'success': 'true'}
        expected_log_call = {'log_reference': CommonLogReference.RESULTLOCK0014}
        lock_state = {
            'received_time': RECEIVED_TIME
        }

        # Act

        result_record_locking_utils._release_lock(RESULT_ID, lock_state, False)

        # Assert

        dynamodb_update_mock.assert_called_once_with(TableNames.RESULTS, dict(
            Key={
                'result_id': 'TEST_RESULT',
                'received_time': '2021-01-27T16:14:59.874365'
            },
            UpdateExpression='REMOVE #user_id, #lock_timestamp, #role_id',
            ExpressionAttributeNames={
                '#user_id': 'user_id',
                '#lock_timestamp': 'lock_timestamp',
                '#role_id': 'role_id'
            },
            ReturnValues='NONE'
        ))
        log_mock.assert_called_with(expected_log_call)

    @patch(f'{module}.log')
    @patch(f'{module}.dynamodb_update_item')
    def test_release_lock_dynamodb_with_state_change(self, dynamodb_update_mock, log_mock):

        # Arrange

        dynamodb_update_mock.return_value = {'success': 'true'}
        expected_log_call = {'log_reference': CommonLogReference.RESULTLOCK0014}
        lock_state = {
            'received_time': RECEIVED_TIME,
            'workflow_state': ResultWorkflowState.PROCESS,
            'workflow_history': []
        }

        # Act

        result_record_locking_utils._release_lock(RESULT_ID, lock_state, True)

        # Assert

        dynamodb_update_mock.assert_called_once_with(TableNames.RESULTS, dict(
            Key={
                'result_id': 'TEST_RESULT',
                'received_time': '2021-01-27T16:14:59.874365'
            },
            UpdateExpression='SET #secondary_status = :secondary_status REMOVE #user_id, #lock_timestamp, #role_id, #user_name',  
            ExpressionAttributeValues={
                ':secondary_status': 'NOT STARTED'
            },
            ExpressionAttributeNames={
                '#secondary_status': 'secondary_status',
                '#user_id': 'user_id',
                '#lock_timestamp': 'lock_timestamp',
                '#user_name': 'user_name',
                '#role_id': 'role_id'
            },
            ReturnValues='NONE'
        ))
        log_mock.assert_called_with(expected_log_call)

    @patch(f'{module}.log')
    @patch(f'{module}.dynamodb_update_item')
    def test_release_lock_dynamodb_unhappy_path(self, dynamodb_update_mock, log_mock):

        # Arrange

        test_error_message = 'Test Exception'
        dynamodb_update_mock.side_effect = Exception(test_error_message)
        expected_log_call = {'log_reference': CommonLogReference.RESULTLOCK0013}
        lock_state = {
            'received_time': RECEIVED_TIME
        }

        # Act

        with self.assertRaises(Exception) as context:
            result_record_locking_utils._release_lock(RESULT_ID, lock_state, False)

        # Assert

        self.assertEqual(str(context.exception), test_error_message)
        dynamodb_update_mock.assert_called_once_with(TableNames.RESULTS, dict(
            Key={
                'result_id': 'TEST_RESULT',
                'received_time': '2021-01-27T16:14:59.874365'
            },
            UpdateExpression='REMOVE #user_id, #lock_timestamp, #role_id',
            ExpressionAttributeNames={
                '#user_id': 'user_id',
                '#lock_timestamp': 'lock_timestamp',
                '#role_id': 'role_id'
            },
            ReturnValues='NONE'
        ))
        log_mock.assert_called_with(expected_log_call)

    @patch(f'{module}._release_lock')
    @patch(f'{module}._check_event_data_for_lock_state')
    def test_do_release_lock(self, lock_state_mock, release_lock_mock):

        # Arrange

        lock_timestamp = (datetime.now(timezone.utc) - relativedelta(minutes=3)).isoformat()
        lock_state = {
            'user_id': USER_ID,
            'lock_timestamp': lock_timestamp,
            'received_time': RECEIVED_TIME
        }
        lock_state_mock.return_value = (
            lock_state,
            {
                'id': USER_ID
            },
            RESULT_ID
        )

        # Act

        result_record_locking_utils.do_release_lock({})

        # Assert

        release_lock_mock.assert_called_with(RESULT_ID, lock_state, True)

    @patch(f'{module}.log')
    @patch(f'{module}._release_lock')
    @patch(f'{module}._check_event_data_for_lock_state')
    def test_do_release_lock_unhappy_path(self, lock_state_mock, release_lock_mock, log_mock):

        # Arrange

        lock_timestamp = (datetime.now(timezone.utc) - relativedelta(minutes=3)).isoformat()
        lock_state_mock.return_value = (
            {
                'user_id': 'other_user',
                'lock_timestamp': lock_timestamp,
                'received_time': RECEIVED_TIME
            },
            {
                'id': USER_ID,
                'name': USER_NAME,
                'role': USER_ROLE
            },
            RESULT_ID
        )

        expected_log_call = {
            'log_reference': CommonLogReference.RESULTLOCK0012,
            'result_id': RESULT_ID,
            'user_id': USER_ID,
            'lock_user': 'other_user',
            'lock_timestamp': lock_timestamp
        }

        # Act

        with self.assertRaises(result_record_locking_utils.LockException) as context:
            result_record_locking_utils.do_release_lock({})

        # Assert

        self.assertIsNotNone(context.exception)
        log_mock.assert_called_with(expected_log_call)

    @patch(f'{module}.do_release_lock')
    def test_release_lock_wrapper(self, do_release_lock_mock):

        # Arrange

        event_data = {'event_data': 'goes here'}
        args = (event_data)

        # Act

        @result_record_locking_utils.release_lock
        def dummy_function(*called_args):
            self.assertEqual(args, *called_args)

        dummy_function(args)

        # Assert

        do_release_lock_mock.assert_called_with(event_data, False)

    @patch(f'{module}.do_release_lock')
    def test_release_lock_wrapper_with_reset_workflow_state(self, do_release_lock_mock):

        # Arrange

        event_data = {'event_data': 'goes here'}
        args = (event_data)

        # Act

        @result_record_locking_utils.release_lock(reset_workflow_state=True)
        def dummy_function(*called_args):
            self.assertEqual(args, *called_args)

        dummy_function(args)

        # Assert

        do_release_lock_mock.assert_called_with(event_data, True)

    @unpack
    @data(
        (
            'process',
            None,
            None,
            'NOT STARTED'
        ),
        (
            'process',
            [
                {
                    'action': 'check'
                }
            ],
            None,
            'CHECKER REJECTED'
        ),
        (
            'check',
            None,
            None,
            'NOT CHECKED'
        ),
        (
            'rejected',
            None,
            'NOT ACTIONED',
            'NOT ACTIONED'
        ),
        (
            'rejected',
            None,
            'ACTIONED',
            'ACTIONED'
        ),
        (
            'rejected',
            [],
            None,
            'NOT ACTIONED'
        ),
        (
            'rejected',
            [
                {
                    'action': 'reject',
                    'step': 'rejected'
                }
            ],
            None,
            'ACTIONED'
        ),
        (
            'rejected',
            [
                {
                    'action': 'save',
                    'step': 'rejected'
                }
            ],
            'NOT ACTIONED',
            'NOT ACTIONED'
        ),
        (
            'rejected',
            [
                {
                    'action': 'save',
                    'step': 'rejected',
                    'comment': 'commenty comment'
                }
            ],
            'NOT ACTIONED',
            'ACTIONED'
        ),
        (
            'rejected',
            [
                {
                    'action': 'save',
                    'step': 'rejected',
                    'crm_number': 'crmy crm'
                }
            ],
            'NOT ACTIONED',
            'ACTIONED'
        ),
    )
    def test_get_secondary_status(self, workflow_state, workflow_history, secondary_status, expected_result):

        # Arrange

        lock_state = {
            'workflow_state': workflow_state,
            'workflow_history': workflow_history,
            'secondary_status': secondary_status
        }

        # Act

        actual_result = result_record_locking_utils._get_secondary_status(lock_state)

        # Assert

        self.assertEqual(actual_result, expected_result)
