from common.utils.deep_ping import is_deep_ping, get_deep_ping_dependencies_from_environment
from unittest import TestCase
from unittest.mock import MagicMock, patch, Mock, call

module = 'common.utils.deep_ping'


class TestIsDeepPing(TestCase):

    def setUp(self):
        self.event = {}
        self.context = MagicMock()

    def act(self):
        return is_deep_ping(self.event)

    def test_deep_ping_api_request_id_header_starts_with_deep_ping(self):
        # Arrange
        self.event = {'headers': {'request_id': 'deep_ping_123'}}
        # Act
        result = self.act()
        # Assert
        self.assertEqual(True, result)

    def test_non_deep_ping_api_request_id_header(self):
        # Arrange
        self.event = {'headers': {'request_id': 'not_deep_ping_123'}}
        # Act
        result = self.act()
        # Assert
        self.assertEqual(False, result)

    def test_deep_ping_sqs_record_contains_deep_ping_body(self):
        # Arrange
        self.event = {'Records': [{'body': '{"message": "deep_ping"}'}]}
        # Act
        result = self.act()
        # Assert
        self.assertEqual(True, result)

    def test_non_deep_ping_sqs_record(self):
        # Arrange
        self.event = {'Records': [{'body': '{"message": "not_deep_ping"}'}]}
        # Act
        result = self.act()
        # Assert
        self.assertEqual(False, result)


class TestGetDependencies(TestCase):

    mock_environ = {
        'SQS_DEEP_PING_1': 'SQS_DEEP_PING_1',
        'SQS_DEEP_PING_2': 'SQS_DEEP_PING_2',
        'SQS_DEEP_PING_3': 'SQS_DEEP_PING_3',
        'DYNAMO_DEEP_PING_1': 'DYNAMO_DEEP_PING_1',
        'DYNAMO_DEEP_PING_2': 'DYNAMO_DEEP_PING_2',
        'DYNAMO_DEEP_PING_3': 'DYNAMO_DEEP_PING_3',
        'S3_DEEP_PING_1': 'S3_DEEP_PING_1',
        'S3_DEEP_PING_2': 'S3_DEEP_PING_2',
        'S3_DEEP_PING_3': 'S3_DEEP_PING_3',
        'HTTP_DEEP_PING_1': 'HTTP_DEEP_PING_1',
        'HTTP_DEEP_PING_2': 'HTTP_DEEP_PING_2',
        'HTTP_DEEP_PING_3': 'HTTP_DEEP_PING_3',
    }

    def setUp(self):
        self.environ_patcher = patch('os.environ', self.mock_environ).start()
        self.getenv_patcher = patch('os.getenv').start()
        self.mock_responses()

    def tearDown(self):
        patch.stopall()

    def act(self):
        return get_deep_ping_dependencies_from_environment()

    def mock_responses(self):
        self.getenv_patcher.return_value = 'deep_ping_dependency'

    def test_gets_dependencies_from_environment(self):
        # Arrange
        expected_dependencies = {
            'dynamo': ['deep_ping_dependency', 'deep_ping_dependency', 'deep_ping_dependency'],
            's3': ['deep_ping_dependency', 'deep_ping_dependency', 'deep_ping_dependency'],
            'sqs': ['deep_ping_dependency', 'deep_ping_dependency', 'deep_ping_dependency'],
            'http': ['deep_ping_dependency', 'deep_ping_dependency', 'deep_ping_dependency'],
        }
        # Act
        dependencies = self.act()
        # Assert
        self.assertEqual(expected_dependencies, dependencies)


class MockBoto3(Mock):

    def setUp(self):
        self.mock_sqs_resource = Mock()
        self.mock_cloudwatch_client = Mock()
        self.mock_s3_client = Mock()

    def resource(self, resource_type):
        if resource_type == 'sqs':
            return self.mock_sqs_resource

    def client(self, client_type):
        if client_type == 's3':
            return self.mock_s3_client

        if client_type == 'cloudwatch':
            return self.mock_cloudwatch_client


class TestHandleDeepPing(TestCase):

    def setUp(self):
        self.mock_boto3 = MockBoto3()
        self.datetime_patcher = patch(f'{module}.datetime').start()
        self.boto3_patcher = patch(f'{module}.boto3', self.mock_boto3).start()
        self.check_dynamodb_table_mock = patch(f'{module}.check_dynamodb_table').start()
        self.log_patcher = patch(f'{module}.log').start()
        self.get_deep_ping_env_var_with_prefix_patcher = patch(f'{module}.get_deep_ping_env_var_with_prefix').start()
        self.get_environment_variables_patcher = patch(f'{module}.get_environment_variables').start()
        self.requests_patcher = patch(f'{module}.requests').start()

        from common.utils.deep_ping import handle_deep_ping
        self.handle_deep_ping = handle_deep_ping

    def act(self):
        return self.handle_deep_ping()

    def tearDown(self):
        patch.stopall()

    def test_successfully_connects_to_dependencies(self):
        # Arrange
        expected_dependency = 'dependency'
        expected_message = '{"message": "deep_ping"}'
        expected_namespace = 'ENVIRONMENT_NAME/deep-ping'
        expected_success_metric_data = {
            'MetricName': 'SuccessCount',
            'Timestamp': 'timestamp',
            'Value': 1,
            'Unit': 'Count',
            'StorageResolution': 1
        }
        expected_total_metric_data = {
            'MetricName': 'TotalCount',
            'Timestamp': 'timestamp',
            'Value': 1,
            'Unit': 'Count',
            'StorageResolution': 1
        }
        self.get_environment_variables_patcher.return_value = {
            'DISABLE_DEEP_PING': 'DISABLE_DEEP_PING',
            'ENVIRONMENT_NAME': 'ENVIRONMENT_NAME',
        }
        expected_success_call = call(
            Namespace=expected_namespace,
            MetricData=[expected_success_metric_data]
        )
        expected_total_call = call(
            Namespace=expected_namespace,
            MetricData=[expected_total_metric_data]
        )
        self.get_deep_ping_env_var_with_prefix_patcher.return_value = [expected_dependency]
        self.datetime_patcher.now.return_value = 'timestamp'
        # Act
        response = self.act()
        # Assert
        self.assertEqual(response['statusCode'], 200)
        self.assertEqual(response['body'], '{"message": "OK"}')
        self.check_dynamodb_table_mock.assert_called_with(expected_dependency)
        self.mock_boto3.resource('sqs').Queue.assert_called_once_with(expected_dependency)
        self.mock_boto3.resource('sqs').Queue().send_message.assert_called_once_with(MessageBody=expected_message)
        self.mock_boto3.client('s3').list_objects_v2.assert_called_once_with(Bucket=expected_dependency)
        self.requests_patcher.get.assert_called_once_with('dependency', timeout=5)
        self.mock_boto3.client('cloudwatch').put_metric_data.assert_has_calls([
            expected_total_call,
            expected_success_call,
            expected_total_call,
            expected_success_call,
            expected_total_call,
            expected_success_call,
            expected_total_call,
            expected_success_call,
        ])

    def test_failure_connects_to_dependencies(self):
        # Arrange
        expected_dependency = 'dependency'
        expected_message = '{"message": "deep_ping"}'
        expected_namespace = 'ENVIRONMENT_NAME/deep-ping'
        expected_success_metric_data = {
            'MetricName': 'SuccessCount',
            'Timestamp': 'timestamp',
            'Value': 1,
            'Unit': 'Count',
            'StorageResolution': 1
        }
        expected_failure_metric_data = {
            'MetricName': 'FailureCount',
            'Timestamp': 'timestamp',
            'Value': 1,
            'Unit': 'Count',
            'StorageResolution': 1
        }
        expected_total_metric_data = {
            'MetricName': 'TotalCount',
            'Timestamp': 'timestamp',
            'Value': 1,
            'Unit': 'Count',
            'StorageResolution': 1
        }
        expected_success_call = call(
            Namespace=expected_namespace,
            MetricData=[expected_success_metric_data]
        )
        expected_failure_call = call(
            Namespace=expected_namespace,
            MetricData=[expected_failure_metric_data]
        )
        expected_total_call = call(
            Namespace=expected_namespace,
            MetricData=[expected_total_metric_data]
        )
        self.get_environment_variables_patcher.return_value = {
            'DISABLE_DEEP_PING': 'DISABLE_DEEP_PING',
            'ENVIRONMENT_NAME': 'ENVIRONMENT_NAME',
        }
        self.get_deep_ping_env_var_with_prefix_patcher.return_value = [expected_dependency]
        self.datetime_patcher.now.return_value = 'timestamp'
        self.requests_patcher.get.side_effect = Exception('Boom!')
        # Act
        response = self.act()
        # Assert
        self.assertEqual(response['statusCode'], 400)
        self.assertEqual(response['body'], '{"message": "ERROR"}')
        self.check_dynamodb_table_mock.assert_called_with(expected_dependency)
        self.mock_boto3.resource('sqs').Queue.assert_called_once_with(expected_dependency)
        self.mock_boto3.resource('sqs').Queue().send_message.assert_called_once_with(MessageBody=expected_message)
        self.mock_boto3.client('s3').list_objects_v2.assert_called_once_with(Bucket=expected_dependency)
        self.requests_patcher.get.assert_called_once_with('dependency', timeout=5)
        self.mock_boto3.client('cloudwatch').put_metric_data.assert_has_calls([
            expected_total_call,
            expected_success_call,
            expected_total_call,
            expected_success_call,
            expected_total_call,
            expected_success_call,
            expected_total_call,
            expected_failure_call,
        ])
