import unittest

from botocore.exceptions import ClientError
from mock import patch, Mock
with patch('boto3.resource') as resource_mock:
    from common.utils.s3_utils import (
        get_object_from_bucket,
        read_bucket,
        read_bucket_with_subfolders,
        move_object_to_bucket_and_transition_to_infrequent_access_class,
        put_object,
        delete_object_from_bucket,
        get_file_contents_from_bucket,
        update_object_metadata,
        get_object_meta_data)


@patch('common.utils.s3_utils._S3_CLIENT')
class TestS3Utils(unittest.TestCase):

    def test_should_get_file_from_s3_bucket(self, s3_mock):

        # Given
        object_key = 'test.txt'
        s3_bucket = 'TEST_BUCKET'
        copy_response = {
            'Body': 'test'
        }
        s3_mock.get_object.return_value = copy_response
        # When
        expected_response = copy_response
        actual_response = get_object_from_bucket(s3_bucket, object_key)
        # Then
        self.assertEqual(expected_response, actual_response)

    def test_should_read_all_contents_of_given_bucket(self, s3_mock):
        # Given
        s3_bucket = 'TEST_BUCKET'
        file_list = {"Contents": [{"Key": 'file1', 'LastModified': '2020/1/1'},
                                  {"Key": 'file2', 'LastModified': '2020/1/1'}]}
        s3_mock.list_objects_v2.return_value = file_list
        # When
        expected_response = file_list
        actual_response = read_bucket(s3_bucket)
        # Then
        self.assertEqual(expected_response, actual_response)

    def test_should_read_all_contents_of_given_bucket_with_subfolder(self, s3_mock):
        # Given
        s3_bucket = 'TEST_BUCKET'
        file_list = {"Contents": [{"Key": 'file1', 'LastModified': '2020/1/1'},
                                  {"Key": 'file2', 'LastModified': '2020/1/1'}]}
        s3_mock.list_objects_v2.return_value = file_list
        # When
        expected_response = file_list
        actual_response = read_bucket_with_subfolders(s3_bucket, 'subfolder')
        # Then
        s3_mock.list_objects_v2.assert_called_with(Bucket=s3_bucket, Prefix='subfolder', Delimiter='/')
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.s3_utils.delete_object_from_bucket')
    def test_should_raise_exception_and_not_call_delete_object_if_copy_fails(self, s3_delete_mock, s3_mock):
        # Given
        from_bucket = 'FROM_BUCKET'
        target_bucket = 'TARGET_BUCKET'
        s3_mock.copy.side_effect = ClientError(error_response={'Error': {'Code': 'NoSuchKey'}}, operation_name='copy')
        file = 'file.txt'

        # When
        with self.assertRaises(Exception) as context:
            move_object_to_bucket_and_transition_to_infrequent_access_class(from_bucket, target_bucket, file)

        # Then
        self.assertEqual('An error occurred (NoSuchKey) when calling the copy operation: Unknown',
                         context.exception.args[0])
        s3_delete_mock.assert_not_called()

    @patch('common.utils.s3_utils.delete_object_from_bucket')
    def test_should_call_delete_object_when_file_successfully_copied_to_target_bucket(self, s3_delete_mock, s3_mock):
        # Given
        from_bucket = 'FROM_BUCKET'
        target_bucket = 'TARGET_BUCKET'
        delete_response = {'DeleteMarker': True}
        s3_delete_mock.return_value = delete_response
        object_key = 'file.txt'

        # When
        expected_response = delete_response
        actual_response = move_object_to_bucket_and_transition_to_infrequent_access_class(
            from_bucket, target_bucket, object_key)

        # Then
        self.assertEqual(expected_response, actual_response)
        s3_mock.copy.assert_called_with(
            Bucket='TARGET_BUCKET',
            CopySource={'Bucket': 'FROM_BUCKET', 'Key': 'file.txt'},
            ExtraArgs={'StorageClass': 'STANDARD_IA'},
            Key='file.txt'
        )
        s3_delete_mock.assert_called_with('FROM_BUCKET', 'file.txt')

    def test_should_save_given_file_to_s3_bucket(self, s3_mock):
        # Given

        object_key = 'file1.txt'
        object_body = 'test'
        s3_bucket = 'TEST_BUCKET'
        response = {
            'VersionId': '001'
        }
        s3_mock.put_object.return_value = response
        # When
        expected_response = response
        actual_response = put_object(s3_bucket, object_body, object_key)
        # Then
        self.assertEqual(expected_response, actual_response)

    def test_should_delete_the_given_file(self, s3_mock):
        # Given
        object_key = 'file.txt'
        s3_bucket = 'TEST_BUCKET'
        response = {'DeleteMarker': True}
        s3_mock.delete_object.return_value = response
        expected_response = response
        # When
        actual_response = delete_object_from_bucket(s3_bucket, object_key)

        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.s3_utils.get_object_from_bucket')
    def test_get_file_contents_from_bucket(self, get_object_from_bucket_mock, _):
        # Given
        s3_bucket = 'TEST_BUCKET'
        object_key = 'file.txt'
        expected_response = {'body': 'some_contents', 'meta_data': []}
        body_mock = Mock()
        body_mock.read.return_value.decode.return_value = 'some_contents'
        get_object_from_bucket_mock.return_value = {'Body': body_mock, 'MetaData': []}
        # When
        actual_response = get_file_contents_from_bucket(s3_bucket, object_key)
        # Then
        self.assertEqual(expected_response, actual_response)

    def test_update_object_metadata(self, s3_mock):
        s3_bucket = 'TEST_BUCKET'
        s3_file = 'TEST_FILE'
        s3_mock.head_object.return_value = {
            'Metadata': {
                'existing_metadata': 'test'
            }
        }

        test_metadata = {
            'new_metadata': 'test'
        }

        update_object_metadata(s3_bucket, s3_file, test_metadata)

        s3_mock.copy_object.assert_called_once()
        s3_mock.copy_object.assert_called_with(
            Bucket=s3_bucket,
            Key=s3_file,
            CopySource=f'{s3_bucket}/{s3_file}',
            Metadata={
                'existing_metadata': 'test',
                'new_metadata': 'test'
            },
            MetadataDirective='REPLACE'
        )

    def test_get_object_meta_data(self, s3_mock):
        # Given
        s3_bucket = 'TEST_BUCKET'
        object_key = 'file.txt'
        s3_mock.head_object.return_value = {
            'Metadata': {
                'existing_metadata': 'test'
            }
        }

        expected_response = {
            'existing_metadata': 'test'
        }
        # When
        actual_response = get_object_meta_data(s3_bucket, object_key)
        # Then
        self.assertEqual(expected_response, actual_response)
