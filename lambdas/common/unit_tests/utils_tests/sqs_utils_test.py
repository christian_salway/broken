import json
from unittest import TestCase
from mock import patch
with patch('boto3.resource') as resource_mock:
    with patch('botocore.client') as client_mock:
        from common.utils.sqs_utils import (
            send_new_message,
            get_s3_data_from_queue_event,
            batch_send_messages
        )

EXAMPLE_RESPONSE = {'MessageId': '001'}
QUEUE_URL = 'SQS_QUEUE'


class TestSQSUtils(TestCase):

    @patch('common.utils.sqs_utils._SQS_CLIENT')
    def test_sqs_message_are_sent(self, sqs_mock):

        # Given
        sqs_mock.send_message.return_value = EXAMPLE_RESPONSE
        message = {
            "name": 'name'
        }
        # When
        expected_response = EXAMPLE_RESPONSE
        actual_response = send_new_message(QUEUE_URL, message)

        # Then
        sqs_mock.send_message.assert_called_with(QueueUrl=QUEUE_URL,
                                                 MessageBody=json.dumps(message, indent=4, sort_keys=True, default=str))

        self.assertEqual(expected_response, actual_response)

    def test_extracting_data_from_queue_message_when_body_is_missing(self):
        event = {'Records': [{}]}

        with self.assertRaises(Exception):
            get_s3_data_from_queue_event(event)

    def test_extracting_data_from_queue_message_when_parameters_are_missing(self):
        message = "{}"
        event = {'Records': [{'body': message}]}

        with self.assertRaises(Exception):
            get_s3_data_from_queue_event(event)


@patch('common.log.LOG_RECORD_META_DATA', {'internal_id': 'internal_id'})
class TestBatchSendMessages(TestCase):

    def setUp(self):
        self.queue_url = 'queue_url'
        self.messages = [{'participant_id': '1'}, {'participant_id': '2'}]

        self.send_new_message_batch_patcher = patch('common.utils.sqs_utils.send_new_message_batch').start()

        self.send_new_message_batch_patcher.return_value = {}

    def act(self):
        return batch_send_messages(self.queue_url, self.messages)

    def tearDown(self):
        patch.stopall()

    def test_calls_send_new_message_batch(self):
        self.send_new_message_batch_patcher.return_value = {}
        expected_messages = [
            {'Id': '0', 'MessageBody': '{"participant_id": "1", "internal_id": "internal_id"}'},
            {'Id': '1', 'MessageBody': '{"participant_id": "2", "internal_id": "internal_id"}'}
        ]
        # Act
        failed = self.act()
        # Assert
        self.assertEqual(failed, [])
        self.send_new_message_batch_patcher.assert_called_with(self.queue_url, expected_messages)

    def test_returns_failed(self):
        self.send_new_message_batch_patcher.return_value = {'Failed': 2}

        expected_failed = [
            {'Id': '0', 'MessageBody': '{"participant_id": "1", "internal_id": "internal_id"}'},
            {'Id': '1', 'MessageBody': '{"participant_id": "2", "internal_id": "internal_id"}'}
        ]
        # Act
        failed = self.act()
        # Assert
        self.assertEqual(failed, expected_failed)

    def test_sends_multiple_batches(self):
        self.messages = self.messages * 100
        self.messages.append({'final_message': 'batch_21'})
        # Act
        self.act()
        # Assert
        self.assertEqual(self.send_new_message_batch_patcher.call_count, 21)
