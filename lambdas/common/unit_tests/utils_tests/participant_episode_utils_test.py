import unittest
from datetime import datetime, timedelta, timezone

from boto3.dynamodb.conditions import Key, Attr, And
from mock import patch, call
from ddt import ddt, data
from common.utils.dynamodb_access.table_names import TableNames

from common.utils.participant_episode_utils import (
    create_replace_episode_record,
    update_episode_to_completion_state,
    update_to_nrl_closed_episode,
    get_episode_record,
    query_for_live_episode,
    query_by_state_and_test_due_date,
    episode_state_change,
    query_for_non_responders,
    query_for_participant_by_gp_id_and_episode_live_record_status,
    update_live_episodes_status_to_logged,
    update_live_episodes_status_from_logged_to_previous,
    EpisodeStatus)

PARTICIPANT_ID = 12345678
SORT_KEY = "EPISODE#2015-12-31"
RESULT_ID = "0123456789"
GP_ID = "012345"
EXAMPLE_RECORD = {'name': 'Struan'}
NHS_NUMBER = "23456789"
EXAMPLE_PARTICIPANT = {'name': 'Paul'}
EXAMPLE_PARTICIPANTS = {'Items': {'name': 'Paul'}}
EXAMPLE_RESPONSE = {'success': 'true'}
EXAMPLE_UUID = '98f1f2c2-fd40-4e75-9d36-d171479db19d'
GP_ID_INDEX_NAME = 'live-record-status'
LAST_EVALUATED_KEY = 'last evaluated key'

QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY = {'Items': [{'name': 'Alfred'}, {'name': 'Bertram'}],
                                          'LastEvaluatedKey': LAST_EVALUATED_KEY}
QUERY_RESPONSE_WITHOUT_LAST_EVALUATED_KEY = {'Items': [{'name': 'Alfred'}, {'name': 'Bertram'}]}
EMPTY_QUERY_RESPONSE = {'Items': []}


def dummy_status_change(records):
    return len(records)


@ddt
class TestEpisodeUtils(unittest.TestCase):

    @patch('common.utils.participant_episode_utils.dynamodb_put_item')
    def test_create_replace_episode_record_completes_successfully(self, put_item_mock):
        # Given
        put_item_mock.return_value = EXAMPLE_RESPONSE

        # When
        expected_response = EXAMPLE_RESPONSE
        actual_response = create_replace_episode_record(EXAMPLE_RECORD)

        # Then
        put_item_mock.assert_called_with(TableNames.PARTICIPANTS, EXAMPLE_RECORD)
        self.assertEqual(expected_response, actual_response)

    @data(EpisodeStatus.CLOSED.value, EpisodeStatus.COMPLETED.value, EpisodeStatus.DEFERRED.value, EpisodeStatus.CEASED.value) 
    @patch('common.utils.participant_episode_utils.dynamodb_update_item')
    def test_update_episode_to_completion_state_completes_successfully(self, completion_state, update_item_mock):
        # When
        update_episode_to_completion_state(PARTICIPANT_ID, SORT_KEY, '2015-12-31', RESULT_ID, False, completion_state)

        # Then
        _, kwargs = update_item_mock.call_args
        expected_update_value_expression = 'SET #result_date = :result_date, #result_id = :result_id, #status = :status REMOVE #live_record_status'  
        expected_update_name_expression = {
            '#result_date': 'result_date',
            '#result_id': 'result_id',
            '#status': 'status',
            '#live_record_status': 'live_record_status'
        }
        expected_ex_attr_vals = {
            ':result_date': '2015-12-31',
            ':result_id': RESULT_ID,
            ':status': completion_state
        }
        expected_call = {
            'Key': {'participant_id': PARTICIPANT_ID, 'sort_key': SORT_KEY},
            'UpdateExpression': expected_update_value_expression,
            'ExpressionAttributeValues': expected_ex_attr_vals,
            'ExpressionAttributeNames': expected_update_name_expression,
            'ReturnValues': 'NONE'
        }
        update_item_mock.assert_called_with(TableNames.PARTICIPANTS, expected_call)

    @data(EpisodeStatus.CLOSED.value, EpisodeStatus.COMPLETED.value, EpisodeStatus.DEFERRED.value, EpisodeStatus.CEASED.value) 
    @patch('common.utils.participant_episode_utils.dynamodb_update_item')
    def test_update_episode_to_completion_state_completes_successfully_with_autocease(self,
                                                                                      completion_state,
                                                                                      update_item_mock):
        # When
        update_episode_to_completion_state(PARTICIPANT_ID, SORT_KEY, '2015-12-31', RESULT_ID, True, completion_state)

        # Then
        _, kwargs = update_item_mock.call_args
        expected_update_value_expression = 'SET #result_date = :result_date, #result_id = :result_id'  
        expected_update_name_expression = {
            '#result_date': 'result_date',
            '#result_id': 'result_id',
        }
        expected_ex_attr_vals = {
            ':result_date': '2015-12-31',
            ':result_id': RESULT_ID
        }
        expected_call = {
            'Key': {'participant_id': PARTICIPANT_ID, 'sort_key': SORT_KEY},
            'UpdateExpression': expected_update_value_expression,
            'ExpressionAttributeValues': expected_ex_attr_vals,
            'ExpressionAttributeNames': expected_update_name_expression,
            'ReturnValues': 'NONE'
        }
        update_item_mock.assert_called_with(TableNames.PARTICIPANTS, expected_call)

    @patch('common.utils.participant_episode_utils.dynamodb_update_item')
    def test_update_to_nrl_closed_episode_completes_successfully(self, update_item_mock):
        # When
        update_to_nrl_closed_episode(PARTICIPANT_ID, SORT_KEY, '2015-12-31')

        # Then
        _, kwargs = update_item_mock.call_args
        expected_update_value_expression = 'SET #status = :status, #closed_date = :closed_date REMOVE #live_record_status'  
        expected_update_name_expression = {'#status': 'status', '#closed_date': 'closed_date',
                                           '#live_record_status': 'live_record_status'}  
        expected_ex_attr_values = {':status': f'{EpisodeStatus.CLOSED}', ':closed_date': '2015-12-31'}
        expected_call = {
            'Key': {'participant_id': PARTICIPANT_ID, 'sort_key': SORT_KEY},
            'UpdateExpression': expected_update_value_expression,
            'ExpressionAttributeValues': expected_ex_attr_values,
            'ExpressionAttributeNames': expected_update_name_expression,
            'ReturnValues': 'NONE'
        }
        update_item_mock.assert_called_with(TableNames.PARTICIPANTS, expected_call)

    @patch('common.utils.participant_episode_utils.dynamodb_get_item')
    def test_get_episode_record_completes_sucxessfully(self, get_item_mock):
        # Given
        get_item_mock.return_value = EXAMPLE_PARTICIPANT

        # When
        expected_response = EXAMPLE_PARTICIPANT
        actual_response = get_episode_record(PARTICIPANT_ID, SORT_KEY)

        # Then
        get_item_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'participant_id': PARTICIPANT_ID,
            'sort_key': SORT_KEY
        })

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.dynamodb_query')
    def test_query_for_live_episode_uses_projection_expression_by_default(self, query_mock):
        # Given
        query_mock.return_value = [EXAMPLE_PARTICIPANT]
        # When
        expected_response = [EXAMPLE_PARTICIPANT]
        actual_response = query_for_live_episode(PARTICIPANT_ID)

        # Then

        expected_projection_expression = (
            '#live_record_status, #HMR101_form_data, #participant_id, #sort_key, #status, #user_id, #test_date, #test_due_date')  
        expected_expression_attribute_names = {
            '#live_record_status': 'live_record_status',
            '#HMR101_form_data': 'HMR101_form_data',
            '#participant_id': 'participant_id',
            '#sort_key': 'sort_key',
            '#status': 'status',
            '#user_id': 'user_id',
            '#test_date': 'test_date',
            '#test_due_date': 'test_due_date'
        }

        expected_key_condition = Key('participant_id').eq(PARTICIPANT_ID) & Key('sort_key').begins_with('EPISODE#')

        query_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'KeyConditionExpression': expected_key_condition,
            'FilterExpression': Attr('live_record_status').exists(),
            'ProjectionExpression': expected_projection_expression,
            'ExpressionAttributeNames': expected_expression_attribute_names
        })

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.dynamodb_query')
    def test_query_for_live_episode_does_not_use_projection_expression_when_requested(self, query_mock):
        # Given
        query_mock.return_value = [EXAMPLE_PARTICIPANT]

        # When
        expected_response = [EXAMPLE_PARTICIPANT]
        actual_response = query_for_live_episode(PARTICIPANT_ID, use_projection=False)

        # Then
        expected_key_condition = Key('participant_id').eq(PARTICIPANT_ID) & Key('sort_key').begins_with('EPISODE#')

        query_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'KeyConditionExpression': expected_key_condition,
            'FilterExpression': Attr('live_record_status').exists()
        })

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.segregated_query')
    def test_query_for_live_episode_uses_segregated_query_when_segregate_flag_is_set(self, query_mock):
        # Given
        query_mock.return_value = [EXAMPLE_PARTICIPANT]

        # When
        expected_response = [EXAMPLE_PARTICIPANT]
        actual_response = query_for_live_episode(PARTICIPANT_ID, use_projection=False, segregate=True)

        # Then
        expected_key_condition = Key('participant_id').eq(PARTICIPANT_ID) & Key('sort_key').begins_with('EPISODE#')

        query_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'KeyConditionExpression': expected_key_condition,
            'FilterExpression': Attr('live_record_status').exists()
        })

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.segregated_query')
    def test_query_for_participant_by_gp_id_and_episode_live_record_status(self, query_mock):
        # Given
        query_mock.return_value = [EXAMPLE_PARTICIPANT, EXAMPLE_PARTICIPANT]

        # When
        expected_response = [EXAMPLE_PARTICIPANT, EXAMPLE_PARTICIPANT]
        actual_response = query_for_participant_by_gp_id_and_episode_live_record_status(GP_ID, EpisodeStatus.PNL.value)

        # Then
        expected_key_condition = Key('live_record_status').eq(EpisodeStatus.PNL.value) & (
                                 Key('registered_gp_practice_code_and_test_due_date').begins_with(GP_ID))

        query_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'KeyConditionExpression': expected_key_condition,
            'IndexName': GP_ID_INDEX_NAME
        })

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.dynamodb_query')
    def test_query_by_state_and_test_due_date_with_no_optional_params(self, query_mock):
        invite_date = datetime.now(timezone.utc) + timedelta(64)
        invite_timestamp = invite_date.date().isoformat()

        dynamo_call = [call(TableNames.PARTICIPANTS, {
            'IndexName': 'episode-test-due-date',
            'KeyConditionExpression': And(
                Key('live_record_status').eq(EpisodeStatus.ACCEPTED.value),
                Key('test_due_date').lte(invite_timestamp)
            )
        }, return_all=True)]

        # Given
        query_mock.return_value = QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY

        # When
        expected_response = QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY
        actual_response = query_by_state_and_test_due_date(EpisodeStatus.ACCEPTED.value, invite_timestamp)

        # Then
        query_mock.assert_has_calls(dynamo_call)

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.dynamodb_query')
    def test_query_by_state_and_test_due_date_with_max_results(self, query_mock):
        invite_date = datetime.now(timezone.utc) + timedelta(64)
        invite_timestamp = invite_date.date().isoformat()

        dynamo_call = [call(TableNames.PARTICIPANTS, {
            'IndexName': 'episode-test-due-date',
            'KeyConditionExpression': And(
                Key('live_record_status').eq(EpisodeStatus.ACCEPTED.value),
                Key('test_due_date').lte(invite_timestamp)
            ),
            'Limit': 10
        }, return_all=True)]

        # Given
        query_mock.return_value = QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY

        # When
        expected_response = QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY
        actual_response = query_by_state_and_test_due_date(
            EpisodeStatus.ACCEPTED.value, invite_timestamp, max_results=10)

        # Then
        query_mock.assert_has_calls(dynamo_call)

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.dynamodb_query')
    def test_query_by_state_and_test_due_date_with_required_fields(self, dynamodb_mock):
        invite_date = datetime.now(timezone.utc) + timedelta(64)
        invite_timestamp = invite_date.date().isoformat()

        dynamo_call = [call(TableNames.PARTICIPANTS, {
            'IndexName': 'episode-test-due-date',
            'KeyConditionExpression': And(
                Key('live_record_status').eq(EpisodeStatus.ACCEPTED.value),
                Key('test_due_date').lte(invite_timestamp)
            ),
            'ProjectionExpression': '#Status, #test_due_date',
            'ExpressionAttributeNames': {'#Status': 'Status', '#test_due_date': 'test_due_date'}
        }, return_all=True)]

        # Given
        dynamodb_mock.return_value = QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY

        # When
        expected_response = QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY
        actual_response = query_by_state_and_test_due_date(EpisodeStatus.ACCEPTED.value, invite_timestamp,
                                                           required_fields=["Status", "test_due_date"])

        # Then
        dynamodb_mock.assert_has_calls(dynamo_call)

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.dynamodb_query')
    def test_query_by_state_and_test_due_date_with_last_evaluated_key(self, dynamodb_mock):
        invite_date = datetime.now(timezone.utc) + timedelta(64)
        invite_timestamp = invite_date.date().isoformat()

        dynamo_call = [call(TableNames.PARTICIPANTS, {
            'IndexName': 'episode-test-due-date',
            'KeyConditionExpression': And(
                Key('live_record_status').eq(EpisodeStatus.ACCEPTED.value),
                Key('test_due_date').lte(invite_timestamp)
            ),
            'ExclusiveStartKey': {'Status': 'Accepted',
                                  'test_due_date': '2020-10-10'}
        }, return_all=True)]

        # Given
        dynamodb_mock.return_value = QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY

        # When
        expected_response = QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY
        actual_response = query_by_state_and_test_due_date(EpisodeStatus.ACCEPTED.value, invite_timestamp,
                                                           last_evaluated_key={'Status': 'Accepted',
                                                                               'test_due_date': '2020-10-10'})

        # Then
        dynamodb_mock.assert_has_calls(dynamo_call)

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.dynamodb_query')
    def test_query_by_state_and_test_due_date_with_all_parameters(self, query_mock):
        invite_date = datetime.now(timezone.utc) + timedelta(64)
        invite_timestamp = invite_date.date().isoformat()

        dynamo_call = [call(TableNames.PARTICIPANTS, {
            'IndexName': 'episode-test-due-date',
            'KeyConditionExpression': And(
                Key('live_record_status').eq(EpisodeStatus.ACCEPTED.value),
                Key('test_due_date').lte(invite_timestamp)
            ),
            'ExclusiveStartKey': {'Status': 'Accepted',
                                  'test_due_date': '2020-10-10'},
            'ProjectionExpression': '#Status, #test_due_date',
            'ExpressionAttributeNames': {'#Status': 'Status', '#test_due_date': 'test_due_date'},
            'Limit': 5
        }, return_all=True)]

        # Given
        query_mock.return_value = QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY

        # When
        expected_response = QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY
        actual_response = query_by_state_and_test_due_date(EpisodeStatus.ACCEPTED.value, invite_timestamp,
                                                           required_fields=["Status", "test_due_date"],
                                                           max_results=5,
                                                           last_evaluated_key={'Status': 'Accepted',
                                                                               'test_due_date': '2020-10-10'})

        # Then
        query_mock.assert_has_calls(dynamo_call)

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.query_by_state_and_test_due_date')
    def test_episode_state_change_with_no_paging_1_status_and_empty_required_fields(self, query_function):
        invite_date = datetime.now(timezone.utc) + timedelta(64)
        invite_timestamp = invite_date.date().isoformat()

        query_function_call = [call('STATUS', invite_timestamp, required_fields=None)]

        # Given
        query_function.return_value = QUERY_RESPONSE_WITHOUT_LAST_EVALUATED_KEY

        # When
        expected_response = 2
        actual_response = episode_state_change(['STATUS'], invite_timestamp, dummy_status_change)

        # Then
        query_function.assert_has_calls(query_function_call)

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.query_by_state_and_test_due_date')
    def test_episode_state_change_with_2_statuses_and_empty_required_fields(self, query_function):
        invite_date = datetime.now(timezone.utc) + timedelta(64)
        invite_timestamp = invite_date.date().isoformat()

        query_function_call = [call('STATUS1', invite_timestamp, required_fields=None),
                               call('STATUS2', invite_timestamp, required_fields=None)]

        # Given
        query_function.return_value = QUERY_RESPONSE_WITHOUT_LAST_EVALUATED_KEY

        # When
        expected_response = 4
        actual_response = episode_state_change(['STATUS1', 'STATUS2'], invite_timestamp, dummy_status_change)

        # Then
        query_function.assert_has_calls(query_function_call)

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.query_by_state_and_test_due_date')
    def test_episode_state_change_with_2_statuses_and_required_fields(self, query_function):
        invite_date = datetime.now(timezone.utc) + timedelta(64)
        invite_timestamp = invite_date.date().isoformat()

        query_function_call = [call('STATUS1', invite_timestamp, required_fields=["FIELDA", "FIELDB"]),
                               call('STATUS2', invite_timestamp, required_fields=["FIELDA", "FIELDB"])]

        # Given
        query_function.return_value = QUERY_RESPONSE_WITHOUT_LAST_EVALUATED_KEY

        # When
        expected_response = 4
        actual_response = episode_state_change(['STATUS1', 'STATUS2'], invite_timestamp, dummy_status_change,
                                               required_fields=["FIELDA", "FIELDB"])

        # Then
        query_function.assert_has_calls(query_function_call)
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.query_by_state_and_test_due_date')
    def test_episode_state_change_with_2_statuses_with_zero_results(self, query_function):
        invite_date = datetime.now(timezone.utc) + timedelta(64)
        invite_timestamp = invite_date.date().isoformat()

        query_function_call = [call('STATUS1', invite_timestamp, required_fields=None)]

        # Given
        query_function.return_value = EMPTY_QUERY_RESPONSE

        # When
        expected_response = 0
        actual_response = episode_state_change(['STATUS1'], invite_timestamp, dummy_status_change)

        # Then
        query_function.assert_has_calls(query_function_call)

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.query_by_state_and_test_due_date')
    def test_episode_state_change_with_paging_1_status_and_empty_required_fields(self, query_function):
        invite_date = datetime.now(timezone.utc) + timedelta(64)
        invite_timestamp = invite_date.date().isoformat()

        query_function_call = [call('STATUS', invite_timestamp, required_fields=None),
                               call('STATUS', invite_timestamp, required_fields=None,
                                    last_evaluated_key=LAST_EVALUATED_KEY)]

        # Given
        query_function.side_effect = [QUERY_RESPONSE_WITH_LAST_EVALUATED_KEY,
                                      QUERY_RESPONSE_WITHOUT_LAST_EVALUATED_KEY]

        # When
        expected_response = 4
        actual_response = episode_state_change(['STATUS'], invite_timestamp, dummy_status_change)

        # Then
        query_function.assert_has_calls(query_function_call)

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.dynamodb_query')
    def test_query_for_non_responders(self, query_mock):
        non_responders_date = datetime.now(timezone.utc) + timedelta(64)
        non_responders_timestamp = non_responders_date.date().isoformat()

        # Given
        query_mock.return_value = [EXAMPLE_PARTICIPANT]

        # When
        expected_response = [EXAMPLE_PARTICIPANT]
        actual_response = query_for_non_responders(non_responders_timestamp)

        # Then
        query_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'IndexName': 'episode-test-due-date',
            'KeyConditionExpression': And(
                Key('live_record_status').eq(EpisodeStatus.REMINDED.value),
                Key('test_due_date').lte(non_responders_timestamp)
            )
        })

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_episode_utils.update_episode_with_condition_expression')
    @patch('common.utils.participant_episode_utils.query_for_live_episode')
    def test_update_live_episodes_status_to_logged(
            self,
            query_for_live_episode_mock,
            update_episode_db_mock):

        participant_id = '1'
        live_episodes = [{
            'sort_key': 'sort_key',
            'live_record_status': 'status'
        }]

        query_for_live_episode_mock.return_value = live_episodes

        update_live_episodes_status_to_logged(participant_id)

        expected_condition_expression = Key('sort_key').eq('sort_key')

        expected_key = {
            'participant_id': '1',
            'sort_key': 'sort_key'
        }

        expected_attributes_to_update = {
            'status': EpisodeStatus.LOGGED.value,
            'live_record_status': EpisodeStatus.LOGGED.value,
            'previous_status': 'status'
        }
        update_episode_db_mock.assert_called_once_with(
            expected_key, expected_attributes_to_update, expected_condition_expression
        )

    @patch('common.utils.participant_episode_utils.update_episode_with_condition_expression')
    @patch('common.utils.participant_episode_utils.query_for_live_episode')
    def test_update_live_episodes_status_from_logged_to_previous(
            self,
            query_for_live_episode_mock,
            update_episode_db_mock):

        participant_id = '1'
        live_episodes = [{
            'sort_key': 'sort_key',
            'previous_status': 'status'
        }]

        query_for_live_episode_mock.return_value = live_episodes

        update_live_episodes_status_from_logged_to_previous(participant_id)

        expected_condition_expression = Key('sort_key').eq('sort_key')

        expected_key = {
            'participant_id': '1',
            'sort_key': 'sort_key'
        }

        expected_attributes_to_update = {
            'status': 'status',
            'live_record_status': 'status'
        }

        expected_attributes_to_delete = ['previous_status']

        update_episode_db_mock.assert_called_once_with(
            expected_key, expected_attributes_to_update, expected_condition_expression, expected_attributes_to_delete
        )
