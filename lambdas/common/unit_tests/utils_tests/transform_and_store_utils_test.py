from unittest import TestCase
from unittest.mock import patch
from common import log
with patch('boto3.resource') as resource_mock, patch('boto3.client') as boto3_client_mock:
    import common.utils.transform_and_store_utils as utils


@patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'participants-table'})
class TransformAndStoreUtilsTest(TestCase):

    def test_remove_empty_values_removes_empties_leaves_False(self):
        input = {
            'is_ceased': False,
            'date': '01234',
            'first_name': '',
            'last_name': None,
            'middle_names': 'amelia joni',
            'dictionary': {
                'foo': 'bar',
                'foobar': {}
            },
            'data': {}
        }
        cleaned = utils.remove_empty_values(input)
        expected = {
            'is_ceased': False,
            'date': '01234',
            'middle_names': 'amelia joni',
            'dictionary': {
                'foo': 'bar'
            }
        }

        self.assertDictEqual(cleaned, expected)

    def test_missing_necessary_fields(self):
        necessary_fields = ['SOURCE_CIPHER', 'NHS_NUM']
        missing_unnecessary = {
            'TEST_SEQ': {'schema_error': 'Test sequence number is a required value'},
            'RES_CODE': {'schema_error': 'Test result code is a required value'},
            'ACT_CODE': {'schema_error': 'Action code is a required value'},
            'RPT_MNTH': {'schema_error': 'Repeat months is a required value'},
        }

        self.assertFalse(utils.missing_necessary_fields(missing_unnecessary, necessary_fields))

        missing_necessary = {
            'SOURCE_CIPHER': {'schema_error': 'Source cipher is a required value'}
        }

        self.assertTrue(utils.missing_necessary_fields(missing_necessary, necessary_fields))

        happy = {}

        self.assertFalse(utils.missing_necessary_fields(happy, necessary_fields))

    @patch('common.utils.transform_and_store_utils.generate_participant_id')
    def test_create_new_participant_record_creates_expected_participant_given_nhs_number_only(self, generate_id_mock):
        generate_id_mock.return_value = 'random123'
        nhs_number = '1234567890'
        expected_participant_record = {
            'nhs_number': '1234567890',
            'participant_id': 'random123',
            'sort_key': 'PARTICIPANT',
            'sanitised_nhs_number': '1234567890'
        }
        actual_participant_record = utils._create_new_participant_record(nhs_number)
        generate_id_mock.assert_called()
        self.assertDictEqual(expected_participant_record, actual_participant_record)

    @patch('common.utils.transform_and_store_utils.generate_participant_id')
    def test_create_new_participant_record_creates_expected_participant_given_nhs_number_and_additional_fields(
            self, generate_id_mock):
        generate_id_mock.return_value = 'random123'
        nhs_number = '1234567890'
        additional_fields = {'name': 'bob', 'age': '27'}
        expected_participant_record = {
            'nhs_number': '1234567890',
            'sanitised_nhs_number': '1234567890',
            'name': 'bob',
            'age': '27',
            'participant_id': 'random123',
            'sort_key': 'PARTICIPANT'
        }
        actual_participant_record = utils._create_new_participant_record(nhs_number, additional_fields)
        generate_id_mock.assert_called()
        self.assertDictEqual(expected_participant_record, actual_participant_record)

    @patch('common.utils.transform_and_store_utils.create_replace_record_in_participant_table')
    @patch('common.utils.transform_and_store_utils._create_new_participant_record')
    @patch('common.utils.transform_and_store_utils.query_participants_by_nhs_number_all_fields')
    def test_update_participant_creates_new_participant_if_no_participant_found_for_given_nhs_number(
            self, query_participants_mock, create_new_participant_record_mock,
            create_replace_record_in_participant_table_mock):

        nhs_number = '1234567890'
        query_participants_mock.return_value = []
        create_new_participant_record_mock.return_value = {'participant_id': 'participant12345'}

        expected_participant = {'participant_id': 'participant12345'}
        actual_participant = utils.update_participant_by_nhs_number(nhs_number)

        self.assertDictEqual(expected_participant, actual_participant)
        query_participants_mock.assert_called_with(nhs_number)
        create_new_participant_record_mock.assert_called_with(nhs_number, None)
        create_replace_record_in_participant_table_mock.assert_called_with(expected_participant)

    @patch('common.utils.transform_and_store_utils.create_replace_record_in_participant_table')
    @patch('common.utils.transform_and_store_utils._create_new_participant_record')
    @patch('common.utils.transform_and_store_utils.query_participants_by_nhs_number_all_fields')
    def test_update_participant_creates_new_participant_with_requested_fields_given_no_participant_found_for_nhs_number(
            self, query_participants_mock, create_new_participant_record_mock,
            create_replace_record_in_participant_table_mock):

        nhs_number = '1234567890'
        fields_to_add = {'name': 'bob', 'age': '23'}
        query_participants_mock.return_value = []
        create_new_participant_record_mock.return_value = {'participant_id': 'participant12345'}

        expected_participant = {'participant_id': 'participant12345'}
        actual_participant = utils.update_participant_by_nhs_number(nhs_number, fields_to_add_or_replace=fields_to_add)

        self.assertDictEqual(expected_participant, actual_participant)
        query_participants_mock.assert_called_with(nhs_number)
        create_new_participant_record_mock.assert_called_with(nhs_number, {'name': 'bob', 'age': '23'})
        create_replace_record_in_participant_table_mock.assert_called_with(expected_participant)

    @patch('common.utils.transform_and_store_utils.create_replace_record_in_participant_table')
    @patch('common.utils.transform_and_store_utils._create_new_participant_record')
    @patch('common.utils.transform_and_store_utils.query_participants_by_nhs_number_all_fields')
    def test_update_participant_does_not_create_new_participant_if_participant_found_for_given_nhs_number(
            self, query_participants_mock, create_new_participant_record_mock,
            create_replace_record_in_participant_table_mock):

        nhs_number = '1234567890'
        query_participants_mock.return_value = [{'nhs_number': '1234567890'}]

        expected_participant = {'nhs_number': '1234567890'}
        actual_participant = utils.update_participant_by_nhs_number(nhs_number)

        self.assertDictEqual(expected_participant, actual_participant)
        query_participants_mock.assert_called_with(nhs_number)
        create_new_participant_record_mock.assert_not_called()
        create_replace_record_in_participant_table_mock.assert_called_with(expected_participant)

    @patch('common.utils.transform_and_store_utils.create_replace_record_in_participant_table')
    @patch('common.utils.transform_and_store_utils._create_new_participant_record')
    @patch('common.utils.transform_and_store_utils.query_participants_by_nhs_number_all_fields')
    def test_update_participant_adds_new_fields_to_participant_record(
            self, query_participants_mock, create_new_participant_record_mock,
            create_replace_record_in_participant_table_mock):

        nhs_number = '1234567890'
        fields_to_add = {'name': 'bob', 'age': '23'}
        query_participants_mock.return_value = [{'nhs_number': '1234567890'}]

        expected_participant = {'age': '23', 'name': 'bob', 'nhs_number': '1234567890'}
        actual_participant = utils.update_participant_by_nhs_number(nhs_number, fields_to_add_or_replace=fields_to_add)

        self.assertDictEqual(expected_participant, actual_participant)
        query_participants_mock.assert_called_with(nhs_number)
        create_new_participant_record_mock.assert_not_called()
        create_replace_record_in_participant_table_mock.assert_called_with(expected_participant)

    @patch('common.utils.transform_and_store_utils.create_replace_record_in_participant_table')
    @patch('common.utils.transform_and_store_utils._create_new_participant_record')
    @patch('common.utils.transform_and_store_utils.query_participants_by_nhs_number_all_fields')
    def test_update_participant_updates_existing_fields_in_participant_record(
            self, query_participants_mock, create_new_participant_record_mock,
            create_replace_record_in_participant_table_mock):

        nhs_number = '1234567890'
        fields_to_replace = {'name': 'bob', 'age': '23'}
        query_participants_mock.return_value = [{'nhs_number': '1234567890', 'name': 'jess', 'age': '56'}]

        expected_participant = {'age': '23', 'name': 'bob', 'nhs_number': '1234567890'}
        actual_participant = utils.update_participant_by_nhs_number(nhs_number,
                                                                    fields_to_add_or_replace=fields_to_replace)

        self.assertDictEqual(expected_participant, actual_participant)
        query_participants_mock.assert_called_with(nhs_number)
        create_new_participant_record_mock.assert_not_called()
        create_replace_record_in_participant_table_mock.assert_called_with(expected_participant)

    @patch('common.utils.transform_and_store_utils.create_replace_record_in_participant_table')
    @patch('common.utils.transform_and_store_utils._create_new_participant_record')
    @patch('common.utils.transform_and_store_utils.query_participants_by_nhs_number_all_fields')
    def test_update_participant_removes_requested_fields_from_participant_record(
            self, query_participants_mock, create_new_participant_record_mock,
            create_replace_record_in_participant_table_mock):

        nhs_number = '1234567890'
        fields_to_remove = ['age', 'name']
        query_participants_mock.return_value = [{'nhs_number': '1234567890', 'name': 'jess', 'age': '56'}]

        expected_participant = {'nhs_number': '1234567890'}
        actual_participant = utils.update_participant_by_nhs_number(nhs_number, fields_to_remove=fields_to_remove)

        self.assertDictEqual(expected_participant, actual_participant)
        query_participants_mock.assert_called_with(nhs_number)
        create_new_participant_record_mock.assert_not_called()
        create_replace_record_in_participant_table_mock.assert_called_with(expected_participant)

    @patch('common.utils.transform_and_store_utils.create_replace_record_in_participant_table')
    @patch('common.utils.transform_and_store_utils._create_new_participant_record')
    @patch('common.utils.transform_and_store_utils.query_participants_by_nhs_number_all_fields')
    def test_update_participant_can_add_replace_and_remove_fields_from_participant_record(
            self, query_participants_mock, create_new_participant_record_mock,
            create_replace_record_in_participant_table_mock):

        nhs_number = '1234567890'
        fields_to_add_and_replace = {'name': 'bob', 'pet': 'cat'}
        fields_to_delete = ['age']
        query_participants_mock.return_value = [{'nhs_number': '1234567890', 'name': 'jess', 'age': '56'}]

        expected_participant = {'nhs_number': '1234567890', 'name': 'bob', 'pet': 'cat'}
        actual_participant = utils.update_participant_by_nhs_number(nhs_number,
                                                                    fields_to_add_or_replace=fields_to_add_and_replace,
                                                                    fields_to_remove=fields_to_delete)

        self.assertDictEqual(expected_participant, actual_participant)
        query_participants_mock.assert_called_with(nhs_number)
        create_new_participant_record_mock.assert_not_called()
        create_replace_record_in_participant_table_mock.assert_called_with(expected_participant)

    @patch('common.utils.transform_and_store_utils.create_replace_record_in_participant_table')
    @patch('common.utils.transform_and_store_utils.query_participants_by_nhs_number_all_fields')
    def test_add_record_returns_none_when_no_participants_found_for_given_nhs_number(self, query_mock, create_mock):
        migrated_data = {'nhs_number': 'the_nhs_number'}
        query_mock.return_value = []
        actual_response = utils.add_update_test_record(migrated_data)
        self.assertIsNone(actual_response)
        query_mock.assert_called_with('the_nhs_number')
        create_mock.assert_not_called()

    @patch('common.utils.transform_and_store_utils.create_replace_record_in_participant_table')
    @patch('common.utils.transform_and_store_utils.query_participants_by_nhs_number_all_fields')
    @patch('common.utils.transform_and_store_utils.get_participant_and_test_results')
    def test_add_record_returns_participant_given_participant_found_and_create_test_record_successful(
            self, get_participant_mock, query_mock, create_mock):
        # Arrange
        migrated_data = {'nhs_number': 'the_nhs_number', 'sort_key': 'RESULT'}
        participant = {'participant_id': 'the_participant_id', 'first_name': 'Joe', 'sort_key': 'PARTICIPANT'}
        query_mock.return_value = [participant]
        get_participant_mock.return_value = [participant]
        # Act
        actual_response = utils.add_update_test_record(migrated_data)

        # Assert
        self.assertDictEqual(participant, actual_response)
        query_mock.assert_called_with('the_nhs_number')
        create_mock.assert_called_with(migrated_data)

    @patch('common.utils.transform_and_store_utils.create_replace_record_in_participant_table')
    @patch('common.utils.transform_and_store_utils.query_participants_by_nhs_number_all_fields')
    @patch('common.utils.transform_and_store_utils.get_participant_and_test_results')
    @patch('common.utils.transform_and_store_utils.update_participant_record')
    def test_add_record_returns_participant_given_participant_found_with_result_and_test_record_not_created(
            self, update_mock, participant_and_test_mock, query_mock, create_mock):
        # Arrange
        migrated_data = {'nhs_number': 'the_nhs_number', 'sort_key': 'RESULT', 'test_date': '2018-01-11'}
        participant = {'participant_id': 'the_participant_id', 'first_name': 'Joe', 'sort_key': 'PARTICIPANT'}
        test_result = {'participant_id': 'the_participant_id', 'sort_key': 'RESULT', 'test_date': '2018-01-11'}
        query_mock.return_value = [participant]
        participant_and_test_mock.return_value = [participant, test_result]
        update_mock.return_value = None

        # Act
        actual_response = utils.add_update_test_record(migrated_data)

        # Assert
        self.assertDictEqual(participant, actual_response)
        query_mock.assert_called_with('the_nhs_number')
        create_mock.assert_not_called()

        key = {
            'participant_id': 'the_participant_id',
            'sort_key': 'RESULT'
        }
        update_mock.assert_called_with(key, migrated_data)

    @patch('common.utils.transform_and_store_utils.validate_message_only_contains_one_record')
    def test_get_message_body_from_event(self, validator_mock):
        records = [{'body': '{"internal_id":"123123123"}'}]
        event = {'Records': records}

        body = utils.get_message_body_from_event(event)

        validator_mock.assert_called_with(records)
        self.assertDictEqual(body, {'internal_id': '123123123'})

    def test_update_log_metadata_from_record_event(self):
        event = {'Records': [
            {'messageId': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
             'body': '{"internal_id":"123123123", "line_index": "123", "file_name": "aa", "file_part_name": "aa_p1"}'}
        ]}
        utils.update_log_metadata_from_record_event(event)

        self.assertEqual(log.LOG_RECORD_META_DATA.get('line_number'), '123')
        self.assertEqual(log.LOG_RECORD_META_DATA.get('file_name'), 'aa')
        self.assertEqual(log.LOG_RECORD_META_DATA.get('file_part_name'), 'aa_p1')

    @patch('common.utils.transform_and_store_utils.query_participant_records_by_source_hash')
    def test_record_is_duplicate_returns_false_if_no_matching_records(self, participant_records_mock):
        participant_records_mock.return_value = []
        test_record = {'source_hash': 'hash'}
        actual_response = utils.record_is_duplicate(test_record, 'comparison fields')
        self.assertEqual(False, actual_response)
        participant_records_mock.assert_called_with('hash')

    @patch('common.utils.transform_and_store_utils.query_participant_records_by_source_hash')
    def test_record_is_duplicate_returns_false_if_record_matching_hash_does_not_have_matching_fields(
            self, participant_records_mock):
        comparison_fields = ['field1', 'field2']
        participant_records_mock.return_value = [{
            'field1': 'something',
            'field2': 'something else'
        }]
        test_record = {'source_hash': 'hash', 'field1': 'different', 'field2': 'different'}
        actual_response = utils.record_is_duplicate(test_record, comparison_fields)
        self.assertEqual(False, actual_response)

    @patch('common.utils.transform_and_store_utils.query_participant_records_by_source_hash')
    def test_record_is_duplicate_returns_true_if_record_matching_hash_has_matching_fields(
            self, participant_records_mock):
        comparison_fields = ['field1', 'field2']
        test_record = {'source_hash': 'hash', 'field1': 'field1', 'field2': 'field2'}
        participant_records_mock.return_value = [test_record]
        actual_response = utils.record_is_duplicate(test_record, comparison_fields)
        self.assertEqual(True, actual_response)

    @patch('common.utils.transform_and_store_utils.query_participant_records_by_source_hash')
    def test_record_is_duplicate_returns_true_if_multiple_records_matching_hash_and_one_has_matching_fields(
            self, participant_records_mock):
        comparison_fields = ['field1', 'field2']
        test_record = {'source_hash': 'hash', 'field1': 'field1', 'field2': 'field2'}
        participant_records_mock.return_value = [test_record, {'field1': 'different', 'field2': 'field2'}]
        actual_response = utils.record_is_duplicate(test_record, comparison_fields)
        self.assertEqual(True, actual_response)

    @patch('common.utils.transform_and_store_utils.query_participant_records_by_source_hash')
    def test_record_is_duplicate_returns_true_if_key_is_empty_for_both_records(
            self, participant_records_mock):
        test_record = {'source_hash': 'hash', 'empty_field': ''}
        participant_records_mock.return_value = [{'other_key': 'value'}]
        actual_response = utils.record_is_duplicate(test_record, ['empty_field'])
        self.assertEqual(True, actual_response)
