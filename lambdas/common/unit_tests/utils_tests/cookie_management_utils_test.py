from unittest import TestCase
from mock import patch
from common.utils.cookie_management_utils import get_session_from_cookie, create_set_cookie_header, delete_cookie_header

SESSION_COOKIE_NAME = 'sp_session_cookie'


class TestSessionFromCookie(TestCase):

    def test_get_session_from_cookie_return_none_when_session_cookie_does_not_exist(self):
        expected_response = None
        cookie_header = 'key="123";'

        event_headers = {
            'Cookie': cookie_header
        }

        actual_response = get_session_from_cookie(event_headers)

        self.assertEqual(expected_response, actual_response)

    def test_get_session_from_cookie_return_none_when_cookie_header_is_none(self):
        expected_response = None
        cookie_header = None

        event_headers = {
            'Cookie': cookie_header
        }

        actual_response = get_session_from_cookie(event_headers)

        self.assertEqual(expected_response, actual_response)

    def test_get_session_from_cookie_return_session_token_with_cookie_header_lowercase(self):
        session_token = '123345572abc'
        expected_response = session_token
        cookie_header = f'{SESSION_COOKIE_NAME}={session_token}'

        event_headers = {
            'cookie': cookie_header
        }

        actual_response = get_session_from_cookie(event_headers)

        self.assertEqual(expected_response, actual_response)

    def test_get_session_from_cookie_return_session_token_with_cookie_header_uppercase(self):
        session_token = '123345572abc'
        expected_response = session_token
        cookie_header = f'{SESSION_COOKIE_NAME}={session_token}'

        event_headers = {
            'Cookie': cookie_header
        }

        actual_response = get_session_from_cookie(event_headers)

        self.assertEqual(expected_response, actual_response)

    def test_get_session_from_cookie_return_gives_priority_uppercase_over_lowercase(self):
        session_token = '123345572abc'
        expected_response = session_token
        cookie_header = f'{SESSION_COOKIE_NAME}={session_token}'

        event_headers = {
            'Cookie': cookie_header,
            'cookie': f'{SESSION_COOKIE_NAME}=something_wrong'
        }

        actual_response = get_session_from_cookie(event_headers)

        self.assertEqual(expected_response, actual_response)


class TestCreateSetCookieHeader(TestCase):

    @patch('time.time')
    def test_create_set_cookie_header(self, time_mock):
        now_epoch = 1580256000  # 'Wed, 29 Jan 2020 00:00:00 GMT'

        expected_expiry = 'Wed, 29 Jan 2020 08:00:00 GMT'
        time_mock.return_value = now_epoch

        session_token = '123456abcdef'
        expected = f'{SESSION_COOKIE_NAME}={session_token}; expires={expected_expiry}; Path=/; SameSite=Strict; Secure'

        actual_response = create_set_cookie_header(session_token)

        self.assertEqual(expected, actual_response)


class TestDeleteCookieHeader(TestCase):

    @patch('time.time')
    def test_delete_cookie_header(self, time_mock):

        now_epoch = 1580256600  # 'Wed, 29 Jan 2020 00:10:00 GMT'

        time_mock.return_value = now_epoch
        expected_expiry_http_format = 'Wed, 29 Jan 2020 00:00:00 GMT'

        expected_response = f'{SESSION_COOKIE_NAME}=""; expires={expected_expiry_http_format}; Path=/; Secure'

        actual_response = delete_cookie_header()

        self.assertEqual(expected_response, actual_response)
