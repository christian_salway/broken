from unittest.case import TestCase
from ddt import data, ddt, unpack
from mock import patch
from common.log_references import CommonLogReference

from common.utils.data_segregation.nhais_ciphers import (
    Cohorts, DMS_COHORT_CODES, ENGLISH_COHORT_CODES, IM_COHORT_CODES, can_workgroup_access_cipher, cohort_for_cipher
)


@ddt
class TestAddressUtils(TestCase):

    @unpack
    @data(
        (ENGLISH_COHORT_CODES, IM_COHORT_CODES),
        (ENGLISH_COHORT_CODES, DMS_COHORT_CODES),
        (IM_COHORT_CODES, DMS_COHORT_CODES),
    )
    def test_no_overlap_in_cohorts(self, cohort_1, cohort_2):
        intersection = cohort_1.intersection(cohort_2)
        self.assertEqual(intersection, set())

    @unpack
    @data(
        ('ENG', Cohorts.ENGLISH),
        ('DO', Cohorts.ENGLISH),
        ('IM', Cohorts.IOM),
        ('DMS', Cohorts.DMS),
        ('THISISNOTAVALIDCIPHER', Cohorts.ENGLISH),
    )
    def test_cohort_recognition(self, cipher, cohort):
        self.assertEqual(cohort_for_cipher(cipher), cohort)

    @data(
        'NI',
        'V',
    )
    def test_no_cohort_cipher_recognition(self, cipher):
        with self.assertRaises(Exception) as context:
            cohort_for_cipher(cipher)
        self.assertEqual(
            str(context.exception),
            f'The provided cipher of {cipher} is recognised but falls outside of the cohorts handled by CSMS'
        )

    @unpack
    @data(
        (Cohorts.ENGLISH, 'ENG', True),
        (Cohorts.ENGLISH, 'DO', True),
        (Cohorts.ENGLISH, 'NOTAREALCIPHER', True),
        (Cohorts.ENGLISH, 'IM', False),
        (Cohorts.IOM, 'IM', True),
        (Cohorts.IOM, 'ENG', False),
        (Cohorts.IOM, 'DO', False),
        (Cohorts.IOM, 'DMS', False),
        (Cohorts.IOM, 'NOTAREALCIPHER', False),
        (Cohorts.DMS, 'DMS', True),
        (Cohorts.DMS, 'ENG', False),
        (Cohorts.DMS, 'DO', False),
        (Cohorts.DMS, 'IM', False),
        (Cohorts.DMS, 'NOTAREALCIPHER', False),
    )
    def test_workgroup_cipher_access(self, workgroup, cipher, expected):
        self.assertEqual(can_workgroup_access_cipher(workgroup, cipher), expected)

    @patch('common.utils.data_segregation.nhais_ciphers.log')
    def test_no_cohort_logged(self, logger_mock):
        self.assertEqual(can_workgroup_access_cipher(Cohorts.ENGLISH, 'SCT'), False)
        logger_mock.assert_called_with({
            'log_reference': CommonLogReference.DATASEG0001, 'workgroup': Cohorts.ENGLISH, 'cipher': 'SCT'})
