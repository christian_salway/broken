from unittest.case import TestCase
from mock import patch, Mock
from ddt import ddt, data, unpack
from common.log_references import CommonLogReference as LogReference
from common.models.participant import ParticipantSortKey
from common.utils.data_segregation.nhais_ciphers import Cohorts
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.data_segregation.data_segregation_filters import (
    filter_items, filter_before_write, _filter_participant_records,
    _filter_result_records, _nhais_ciphers_for_participant_ids
)

# These fake participant records contain the necessary information for determining cohort
ENGLISH_PARTICIPANT = {
    'participant_id': 'test_eng_participant', 'sort_key': ParticipantSortKey.PARTICIPANT, 'nhais_cipher': 'ENG'
}
LEEDS_PARTICIPANT = {
    'participant_id': 'test_eng_participant', 'sort_key': ParticipantSortKey.PARTICIPANT, 'nhais_cipher': 'LDS'
}
IOM_PARTICIPANT = {
    'participant_id': 'test_iom_participant', 'sort_key': ParticipantSortKey.PARTICIPANT, 'nhais_cipher': 'IM'
}
DMS_PARTICIPANT = {
    'participant_id': 'test_dms_participant', 'sort_key': ParticipantSortKey.PARTICIPANT, 'nhais_cipher': 'DMS'
}

WELSH_PARTICIPANT = {
    'participant_id': 'test_cym_participant', 'sort_key': ParticipantSortKey.PARTICIPANT, 'nhais_cipher': 'CYM'
}

PARTICIPANTS = [
    ENGLISH_PARTICIPANT, IOM_PARTICIPANT, LEEDS_PARTICIPANT, DMS_PARTICIPANT, WELSH_PARTICIPANT
]

# Keys are used for delete, update and get_item operations and don't have enough information to determine cohort
# membership by themselves (no cipher field)
ENGLISH_KEY = {
    'participant_id': 'test_eng_participant', 'sort_key': ParticipantSortKey.EPISODE
}
LEEDS_KEY = {
    'participant_id': 'test_eng_participant', 'sort_key': ParticipantSortKey.EPISODE
}
IOM_KEY = {
    'participant_id': 'test_iom_participant', 'sort_key': ParticipantSortKey.EPISODE
}
WELSH_KEY = {
    'participant_id': 'test_cym_participant', 'sort_key': ParticipantSortKey.EPISODE
}
DMS_KEY = {
    'participant_id': 'test_dms_participant', 'sort_key': ParticipantSortKey.EPISODE
}

ENGLISH_PARTICIPANT_RESULT = {
    'participant_id': 'test_eng_participant', 'sort_key': ParticipantSortKey.RESULT
}
IOM_PARTICIPANT_RESULT = {
    'participant_id': 'test_iom_participant', 'sort_key': ParticipantSortKey.RESULT
}
WELSH_PARTICIPANT_RESULT = {
    'participant_id': 'test_cym_participant', 'sort_key': ParticipantSortKey.RESULT
}
DMS_PARTICIPANT_RESULT = {
    'participant_id': 'test_dms_participant', 'sort_key': ParticipantSortKey.RESULT
}

ENGLISH_RESULT = {'result_data': 'some_eng_data', 'sanitised_nhais_cipher': 'ENG'}
LEEDS_RESULT = {'result_data': 'some_some_leeds_data', 'sanitised_nhais_cipher': 'LDS'}
IOM_RESULT = {'result_data': 'some_iom_data', 'sanitised_nhais_cipher': 'IM'}
WELSH_RESULT = {'result_data': 'some_iom_data', 'sanitised_nhais_cipher': 'CYM'}
DMS_RESULT = {'result_data': 'some_dms_some_dms_data', 'sanitised_nhais_cipher': 'DMS'}

RESULTS = [ENGLISH_RESULT, LEEDS_RESULT, IOM_RESULT, DMS_RESULT, WELSH_RESULT]


@ddt
class TestDataSegregationFilters(TestCase):

    @unpack
    @data(
        (TableNames.PARTICIPANTS, 'participants'),
        (TableNames.RESULTS, 'results'),
        (TableNames.REFERENCE, None)
    )
    @patch('common.utils.data_segregation.data_segregation_filters._filter_result_records')
    @patch('common.utils.data_segregation.data_segregation_filters._filter_participant_records')
    def test_filter_items(self, table, code_path, filter_participants_mock, filter_results_mock):
        # Arrange
        items = [{"some_key": "some_value"}]

        # Act
        actual = filter_items(table, items)

        # Assert
        if not code_path:
            filter_participants_mock.assert_not_called()
            filter_results_mock.assert_not_called()
            self.assertEqual(actual, items)
        elif code_path == 'participants':
            filter_participants_mock.assert_called_once_with(items)
            filter_results_mock.assert_not_called()
            self.assertEqual(actual, filter_participants_mock(items))
        elif code_path == 'results':
            filter_participants_mock.assert_not_called()
            filter_results_mock.assert_called_once_with(items)
            self.assertEqual(actual, filter_results_mock(items))

    @unpack
    @data(
        (TableNames.PARTICIPANTS, 'participants'),
        (TableNames.RESULTS, 'results'),
        (TableNames.REFERENCE, None)
    )
    @patch('common.utils.data_segregation.data_segregation_filters._filter_result_records')
    @patch('common.utils.data_segregation.data_segregation_filters._filter_participant_records')
    def test_filter_before_write(self, table, code_path, filter_participants_mock, filter_results_mock):
        # Arrange
        items = [{"some_key": "some_value"}]
        filter_participants_mock.return_value = items
        filter_results_mock.return_value = items

        # Act
        if not code_path:
            with self.assertRaises(Exception) as context:
                filter_before_write(table, items)
                filter_participants_mock.assert_not_called()
                filter_results_mock.assert_not_called()
                self.assertEqual(
                    str(context.exception),
                    str(ValueError('Attempting to write items which are not available to this user'))
                )
        else:
            filter_before_write(table, items)

        # Assert
        if code_path == 'participants':
            filter_participants_mock.assert_called_once_with(items)
            filter_results_mock.assert_not_called()
        elif code_path == 'results':
            filter_participants_mock.assert_not_called()
            filter_results_mock.assert_called_once_with(items)

    @unpack
    @data(
        ([Cohorts.ENGLISH], [ENGLISH_PARTICIPANT, LEEDS_PARTICIPANT]),
        ([Cohorts.IOM], [IOM_PARTICIPANT]),
        ([Cohorts.WELSH], [WELSH_PARTICIPANT]),
        ([Cohorts.DMS], [DMS_PARTICIPANT]),
        ([Cohorts.ENGLISH, Cohorts.DMS], [ENGLISH_PARTICIPANT, LEEDS_PARTICIPANT, DMS_PARTICIPANT]),
        ([Cohorts.ENGLISH, Cohorts.DMS, Cohorts.IOM, Cohorts.WELSH], PARTICIPANTS),
    )
    @patch('common.utils.data_segregation.data_segregation_filters.log')
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups')
    def test_filter_participant_records_participant_records(
            self, cohorts, expected, get_workgroups_mock, log_mock):
        # Arrange
        get_workgroups_mock.return_value = cohorts

        # Act
        actual = _filter_participant_records(PARTICIPANTS)

        # Assert
        if len(expected) < 5:
            log_mock.assert_called_once_with({'log_reference': LogReference.DATASEG0002,
                                              'removed_items_count': 5 - len(expected)})
        else:
            log_mock.assert_not_called()

        self.assertEqual(actual, expected)

    @patch('common.utils.data_segregation.data_segregation_filters.log')
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups')
    @patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids')
    def test_filter_participant_records_other_records(
            self, get_nhais_ciphers_mock, get_workgroups_mock, log_mock):
        # Arrange
        items = [
            ENGLISH_PARTICIPANT,
            ENGLISH_PARTICIPANT_RESULT,
            IOM_PARTICIPANT_RESULT,
        ]
        get_workgroups_mock.return_value = [Cohorts.ENGLISH]
        get_nhais_ciphers_mock.return_value = {'test_iom_participant': 'IM'}

        # Act
        actual = _filter_participant_records(items)

        # Assert
        log_mock.assert_called_once_with({'log_reference': LogReference.DATASEG0002,
                                          'removed_items_count': 1})
        self.assertEqual(actual, [ENGLISH_PARTICIPANT, ENGLISH_PARTICIPANT_RESULT])

    @unpack
    @data(
        ([Cohorts.ENGLISH], [ENGLISH_RESULT, LEEDS_RESULT]),
        ([Cohorts.IOM], [IOM_RESULT]),
        ([Cohorts.DMS], [DMS_RESULT]),
        ([Cohorts.WELSH], [WELSH_RESULT]),
        ([Cohorts.ENGLISH, Cohorts.DMS], [ENGLISH_RESULT, LEEDS_RESULT, DMS_RESULT]),
        ([Cohorts.ENGLISH, Cohorts.DMS, Cohorts.IOM, Cohorts.WELSH], RESULTS),
    )
    @patch('common.utils.data_segregation.data_segregation_filters.log')
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups')
    def test_filter_result_records(
            self, cohorts, expected, get_workgroups_mock, log_mock):
        # Arrange
        get_workgroups_mock.return_value = cohorts

        # Act
        actual = _filter_result_records(RESULTS)

        # Assert
        if len(expected) < 5:
            log_mock.assert_called_once_with({'log_reference': LogReference.DATASEG0003,
                                              'removed_items_count': 5 - len(expected)})
        else:
            log_mock.assert_not_called()
        self.assertEqual(actual, expected)

    @patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_resource')
    @patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_table_name')
    def test_nhais_ciphers_for_participant_ids(self, get_table_name_mock, get_resource_mock):
        # Arrange
        get_table_name_mock.return_value = 'test-participants'
        resource_mock = Mock()
        resource_mock.batch_get_item.return_value = {
            'Responses': {'test-participants': [ENGLISH_PARTICIPANT]}
        }
        get_resource_mock.return_value = resource_mock
        # Act
        actual = _nhais_ciphers_for_participant_ids({'test_eng_participant'})
        # Assert
        self.assertEqual(actual, {'test_eng_participant': 'ENG'})
