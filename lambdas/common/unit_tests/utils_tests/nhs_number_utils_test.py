from unittest import TestCase
from ddt import data, ddt, unpack

from common.utils.nhs_number_utils import sanitise_nhs_number


@ddt
class TestParticipantUtils(TestCase):

    @unpack
    @data(
        ('0123456789ABCDEuvwxyz', '0123456789ABCDEUVWXYZ'),
        ('0 12  345   6789\tABCDEuvwxyz', '0123456789ABCDEUVWXYZ'),
        (r"\\,./;'#[]`1234567890ABCDEuvwxyz-=¬!\"£$%^&*()_+\{\}:@~|<>?¦", '/1234567890ABCDEUVWXYZ?'),
    )
    def test_nhs_number_sanitised_correctly(self, raw_nhs_number, expected_sanitised_nhs_number):
        actual = sanitise_nhs_number(raw_nhs_number)

        self.assertEqual(actual, expected_sanitised_nhs_number)
