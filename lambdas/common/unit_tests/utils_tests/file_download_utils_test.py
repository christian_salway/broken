from datetime import datetime, timezone
from unittest import TestCase
from unittest.mock import call, patch, Mock

from common.log_references import CommonLogReference
from common.utils.file_download_utils import ProcessingException

module = 'common.utils.file_download_utils'

test_selected_role = {
    "organisation_code": "A60",
    "organisation_name": "YORKSHIRE AND THE HUMBER",
    "role_id": "2001",
    "role_name": "CSAS Team Member",
    "workgroups": [
      "cervicalscreening"
    ],
    'access_groups': {
                'csas': True
    }
}

FILE_NAME = 'file-name'
BUCKET_NAME = 'bucket-name'


class TestFileDownloadUtils(TestCase):

    @classmethod
    def setUpClass(cls):
        import common.utils.file_download_utils as file_download_utils
        cls.file_download_utils = file_download_utils

    @patch(f'{module}.log')
    def test_verify_and_parse_session_with_filename_no_session(self, log_mock):
        test_session = {}

        with self.assertRaises(ProcessingException) as context:
            self.file_download_utils.verify_and_parse_session(test_session)
        self.assertTrue(CommonLogReference.DOWNLOADFILEEX0001.message in context.exception.message)
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILEEX0001})
        ])

    @patch(f'{module}.log')
    def test_verify_and_parse_session_success(self, log_mock):
        test_session = {
            'user_data': 'test',
            'selected_role': test_selected_role,
            'access_groups': {
                'csas': True
            }
        }

        actual_session_user_data, actual_access_groups = \
            self.file_download_utils.verify_and_parse_session(test_session)

        self.assertEqual(actual_session_user_data, test_session['user_data'])
        self.assertEqual(actual_access_groups, test_session['access_groups'])

        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILE0001})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event', Mock(return_value=({'query_parameters': {}}, [])))
    def test_get_file_name_with_wrong_type(self, log_mock):
        test_event = {'path': 'api/wrong/file-path'}

        with self.assertRaises(ProcessingException) as context:
            self.file_download_utils.get_file_name(test_event)

        self.assertTrue(CommonLogReference.DOWNLOADFILEEX0006.message in context.exception.message)
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILEEX0006})
        ])

    @patch(f'{module}.get_api_request_info_from_event', Mock(
        return_value=({'query_parameters': {'file_name': FILE_NAME}}, 'test_method', 'test_resource')
    ))
    @patch(f'{module}.log')
    def test_get_file_name_success(self, log_mock):
        test_event = {'path': 'api/correct/file-path'}

        file_name = self.file_download_utils.get_file_name(test_event)

        self.assertEqual(file_name, FILE_NAME)

        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILE0004, 'file_name': 'file-name'})
        ])

    @patch(f'{module}.get_object_from_bucket', Mock(return_value=None))
    @patch(f'{module}.log')
    def test_get_file_body_no_file(self, log_mock):
        with self.assertRaises(ProcessingException) as context:
            self.file_download_utils.get_file_body(BUCKET_NAME, FILE_NAME)

        self.assertTrue('Object not found with name file-name' in context.exception.message)
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILEEX0003})
        ])

    @patch(f'{module}.get_object_from_bucket')
    @patch(f'{module}.log')
    def test_get_file_body_success(self, log_mock, get_object_mock):
        file_mock = Mock()
        file_mock.read.return_value = b'test_file_body'
        get_object_mock.return_value = {'Body': file_mock}

        expected_result = b'test_file_body'.decode('utf-8')

        actual_result = self.file_download_utils.get_file_body(BUCKET_NAME, FILE_NAME)

        self.assertEqual(expected_result, actual_result)
        get_object_mock.assert_called_with(BUCKET_NAME, FILE_NAME)
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILE0002})
        ])

    @patch(f'{module}.update_object_metadata')
    @patch(f'{module}.log')
    def test_update_metadata_success(self, log_mock, metadata_mock):
        test_user_data = {
            'first_name': 'Test',
            'last_name': 'User'
        }

        test_file_name = FILE_NAME
        test_bucket_name = BUCKET_NAME

        self.file_download_utils.update_metadata(test_user_data, test_file_name, test_bucket_name)

        metadata_mock.assert_called_with(
            test_bucket_name,
            test_file_name,
            {
                'downloaded_by': 'Test User',
                'downloaded_date': datetime.now(timezone.utc).date().isoformat()
            }
        )
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.DOWNLOADFILE0003})
        ])
