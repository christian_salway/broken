import json
import os
import unittest
from datetime import datetime, timezone
from ddt import data, ddt, unpack
from boto3.dynamodb.conditions import Attr, Key
from mock import patch, MagicMock, Mock

from common.utils.dynamodb_access.table_names import TableNames

DIRECTORY_NAME = os.path.dirname(__file__)

mock_environ = {
    'ORGANISATION_DIRECTORY_SERVICE_URL': 'ods_url',
    'ORGANISATION_DIRECTORY_SERVICE_CONNECTION_TIMEOUT': '7',
    'ACTIVE_ORGANISATION_REFRESH_SECONDS': '100',
    'INACTIVE_ORGANISATION_REFRESH_SECONDS': '10',
    'ORGANISATION_ROLE_EXTENSION_URL': 'role_url'
}

with patch('boto3.resource') as resource_mock, patch('boto3.client') as boto3_client_mock:
    with patch('os.environ', mock_environ):
        from common.utils.organisation_directory_utils import (
            get_organisation_information_by_organisation_code,
            get_organisation_information_from_directory_service,
            get_phone_number,
            convert_organisation_response_to_organisation_data,
            convert_address,
            get_organisation_type,
            create_new_organisation_record,
            get_organisation_gp_practice_status,
            get_organisation_operational_periods,
            organisation_is_dms_valid
        )


@ddt
@patch('common.utils.organisation_directory_utils.log', Mock())
class TestOrganisationDirectoryUtils(unittest.TestCase):
    FIXED_NOW_TIME = datetime(2020, 3, 3, 9, 45, 23, 123456, tzinfo=timezone.utc)

    @patch('common.utils.organisation_directory_utils.datetime')
    @patch('common.utils.organisation_directory_utils.time')
    @patch('common.utils.organisation_directory_utils.dynamodb_query')
    def test_get_organisation_information_by_organisation_code_returns_cached_data_if_present(
            self, query_mock,
            time_mock, datetime_mock):
        organisation_code = 'A85609'
        now_in_seconds = 1543296451
        with open(os.path.join(DIRECTORY_NAME,
                               'test_data/expected/ods_response_gp_role_start_end_date.json')) as expected_file:
            response_body = json.load(expected_file)
        expected_data = {'active': True,
                         'address': {'address_line_1': '108 RAWLING ROAD',
                                     'address_line_2': 'BENSHAM',
                                     'address_line_3': 'GATESHEAD',
                                     'address_line_4': 'TYNE AND WEAR',
                                     'address_line_5': 'ENGLAND',
                                     'postcode': 'NE8 4QR'},
                         'gp_practice_status': True,
                         'last_updated': '2021-02-05T08:15:48.123456',
                         'name': '108 RAWLING ROAD(RAWLING ROAD PRACTICE)',
                         'operational_periods': [{'end': '2021-06-30', 'start': '1974-04-01'}],
                         'organisation_code': 'A85609',
                         'phone_number': '0191 4203255',
                         'type': 'UNKNOWN'}
        time_mock.return_value = now_in_seconds
        datetime_mock.now.return_value = datetime(2021, 2, 5, 8, 15, 48, 123456)
        query_mock.return_value = [{'organisation_code': organisation_code, 'data': response_body}]

        actual_data = get_organisation_information_by_organisation_code(organisation_code)

        query_mock.assert_called_with(TableNames.ORGANISATIONS, {
            'KeyConditionExpression': Key('organisation_code').eq(organisation_code),
            'FilterExpression': Attr('expires').gt(now_in_seconds)
        })

        self.assertEqual(expected_data, actual_data)

    @patch('common.utils.organisation_directory_utils.datetime')
    @patch('common.utils.organisation_directory_utils.time')
    @patch('common.utils.organisation_directory_utils.dynamodb_query')
    @patch('common.utils.organisation_directory_utils.get_organisation_information_from_directory_service')
    @patch('common.utils.organisation_directory_utils.create_new_organisation_record')
    def test_get_organisation_information_by_organisation_code_looks_up_and_caches_data_if_cached_data_not_present(
            self, save_mock, lookup_mock, query_mock, time_mock, datetime_mock):
        organisation_code = 'A85609'
        now_in_seconds = 1543296451
        with open(os.path.join(DIRECTORY_NAME,
                               'test_data/fixtures/ods_response_gp_role_start_end_date.json')) as expected_file:
            response_body = json.load(expected_file)
        expected_data = {'active': True,
                         'address': {'address_line_1': '108 RAWLING ROAD',
                                     'address_line_2': 'BENSHAM',
                                     'address_line_3': 'GATESHEAD',
                                     'address_line_4': 'TYNE AND WEAR',
                                     'address_line_5': 'ENGLAND',
                                     'postcode': 'NE8 4QR'},
                         'gp_practice_status': True,
                         'last_updated': '2021-02-05T08:15:48.123456',
                         'name': '108 RAWLING ROAD(RAWLING ROAD PRACTICE)',
                         'operational_periods': [{'end': '2021-06-30', 'start': '1974-04-01'}],
                         'organisation_code': 'A85609',
                         'phone_number': '0191 4203255',
                         'type': 'UNKNOWN'}
        time_mock.return_value = now_in_seconds
        datetime_mock.now.return_value = datetime(2021, 2, 5, 8, 15, 48, 123456)
        query_mock.return_value = []
        lookup_mock.return_value = response_body

        actual_data = get_organisation_information_by_organisation_code(organisation_code)

        query_mock.assert_called_with(TableNames.ORGANISATIONS, {
            'KeyConditionExpression': Key('organisation_code').eq(organisation_code),
            'FilterExpression': Attr('expires').gt(now_in_seconds)
        })

        lookup_mock.assert_called_with(organisation_code)
        save_mock.assert_called_with(organisation_code, response_body, True)

        self.assertEqual(expected_data, actual_data)

    @patch('requests.get')
    @patch('common.utils.organisation_directory_utils.datetime')
    @patch('common.utils.organisation_directory_utils.time')
    @patch('common.utils.organisation_directory_utils.dynamodb_query')
    def test_get_organisation_information_by_organisation_code_returns_error_object_if_get_request_fails(
            self, query_mock, time_mock, datetime_mock, get_request_mock):
        organisation_code = 'A85609'
        now_in_seconds = 1543296451
        expected_data = {'organisation_code': 'A85609',
                         'message': 'Request error'}
        time_mock.return_value = now_in_seconds
        datetime_mock.now.return_value = datetime(2021, 2, 5, 8, 15, 48, 123456)
        query_mock.return_value = []
        get_request_mock.side_effect = Exception('Something went wrong')

        actual_data = get_organisation_information_by_organisation_code(organisation_code)

        query_mock.assert_called_with(TableNames.ORGANISATIONS, dict(
            KeyConditionExpression=Key('organisation_code').eq(organisation_code),
            FilterExpression=Attr('expires').gt(now_in_seconds)
        ))

        self.assertEqual(expected_data, actual_data)

    @patch('common.utils.organisation_directory_utils.datetime')
    @patch('requests.get')
    def test_get_organisation_lookup_saves_and_returns_organisation_data_given_ok_response(
            self, get_request_mock, time_mock):
        with open(os.path.join(DIRECTORY_NAME,
                               'test_data/expected/ods_response_gp_role_start_end_date.json')) as expected_file:
            expected_response_body = json.load(expected_file)
        time_mock.now.return_value = self.FIXED_NOW_TIME
        organisation_code = 'A85609'
        expected_data = expected_response_body
        response_object = MagicMock()
        response_object.status_code = 200
        with open(os.path.join(DIRECTORY_NAME,
                               'test_data/fixtures/ods_response_gp_role_start_end_date.json')) as fixture_file:
            response_object.json.return_value = json.load(fixture_file)

        get_request_mock.return_value = response_object

        actual_data = get_organisation_information_from_directory_service(organisation_code)

        get_request_mock.assert_called_with(url=f'ods_url/{organisation_code}', timeout=7)

        self.assertEqual(expected_data, actual_data)

    @patch('common.utils.organisation_directory_utils.datetime')
    @patch('requests.get')
    def test_get_organisation_lookup_saves_and_returns_organisation_data_given_not_found_response(
            self, get_request_mock, time_mock):
        time_mock.now.return_value = self.FIXED_NOW_TIME
        organisation_code = 'AB123'
        expected_returned_data = {'organisation_code': organisation_code,
                                  'message': f'No organisation found with code {organisation_code}'}
        response_object = MagicMock()
        response_object.status_code = 404
        response_object.json.return_value = {
            "resourceType": "OperationOutcome",
            "id": "c96115ae-0e92-4309-994b-5eaef1c0ef8a",
            "meta": {
                "profile": "https://fhir.nhs.uk/STU3/StructureDefinition/Spine-OperationOutcome-1"
            },
            "issue": [
                {
                    "severity": "error",
                    "code": "not-found",
                    "details": {
                        "coding": {
                            "system": "https://fhir.nhs.uk/STU3/CodeSystem/Spine-ErrorOrWarningCode-1",
                            "code": "NO_RECORD_FOUND",
                            "display": "No record found"
                        }
                    },
                    "diagnostics": "No record found for supplied ODS code"
                }
            ]
        }
        get_request_mock.return_value = response_object

        actual_returned_data = get_organisation_information_from_directory_service(organisation_code)

        get_request_mock.assert_called_with(url=f'ods_url/{organisation_code}', timeout=7)

        self.assertEqual(expected_returned_data, actual_returned_data)

    @patch('requests.get')
    def test_get_organisation_lookup_returns_organisation_data_given_error_response(self, get_request_mock):
        organisation_code = 'AB123'
        expected_returned_data = {'organisation_code': organisation_code,
                                  'message': 'Error retrieving organisation details'}
        response_object = MagicMock()
        response_object.status_code = 500
        get_request_mock.return_value = response_object

        actual_returned_data = get_organisation_information_from_directory_service(organisation_code)

        get_request_mock.assert_called_with(url=f'ods_url/{organisation_code}', timeout=7)

        self.assertEqual(expected_returned_data, actual_returned_data)

    @patch('requests.get')
    def test_get_organisation_lookup_returns_organisation_data_given_get_error(self, get_request_mock):
        organisation_code = 'AB123'
        expected_returned_data = {'organisation_code': organisation_code,
                                  'message': 'Request error'}
        get_request_mock.side_effect = Exception('Something went wrong')

        actual_returned_data = get_organisation_information_from_directory_service(organisation_code)

        get_request_mock.assert_called_with(url=f'ods_url/{organisation_code}', timeout=7)

        self.assertEqual(expected_returned_data, actual_returned_data)

    @patch('common.utils.organisation_directory_utils.get_phone_number')
    @patch('common.utils.organisation_directory_utils.get_organisation_type')
    @patch('common.utils.organisation_directory_utils.convert_address')
    def test_response_converter_correctly_converts_response(
            self, address_converter_mock, get_org_type_mock, get_phone_number_mock):
        ods_response_body = {
            'address': {'line_1': 'road', 'postcode': 'AB1 2CD'},
            'id': 'AB123',
            'active': True,
            "telecom": [
                {
                    'system': 'phone',
                    'value': '01189 574614'
                }
            ],
            'name': 'St. James'
        }
        current_time = '2020-05-07T14:58:27.000+00:00'

        address_converter_mock.return_value = {'converted_line_1': 'converted road'}
        get_org_type_mock.return_value = 'A HOSPITAL'
        get_phone_number_mock.return_value = 'phone number'

        expected_data = {
            'organisation_code': 'AB123',
            'active': True,
            'address': {'converted_line_1': 'converted road'},
            'name': 'St. James',
            'type': 'A HOSPITAL',
            'phone_number': 'phone number',
            'gp_practice_status': False,
            'last_updated': '2020-05-07T14:58:27.000+00:00',
            'operational_periods': [],
        }

        actual_data = convert_organisation_response_to_organisation_data(ods_response_body, current_time)

        address_converter_mock.assert_called_with({'line_1': 'road', 'postcode': 'AB1 2CD'})
        get_org_type_mock.assert_called_with(ods_response_body)

        self.assertEqual(expected_data, actual_data)

    @patch('common.utils.organisation_directory_utils.remove_empty_values')
    def test_convert_address_removes_nonexistent_fields(self, remove_empty_values_mock):
        ods_address = {}
        expected_address = {}
        remove_empty_values_mock.return_value = expected_address
        actual_address = convert_address(ods_address)
        remove_empty_values_mock.assert_called_with({'address_line_1': None, 'address_line_2': None,
                                                     'address_line_3': None, 'address_line_4': None,
                                                     'address_line_5': None, 'postcode': None})
        self.assertEqual(expected_address, actual_address)

    @patch('common.utils.organisation_directory_utils.remove_empty_values')
    def test_convert_address_converts_address_without_address_line_2(self, remove_empty_values_mock):
        ods_address = {'line': ['line 1'], 'city': 'Leeds', 'district': 'West Yorkshire',
                       'country': 'UK', 'postalCode': 'AB1 2CD'}
        expected_address = {'address_line_1': 'line_1', 'address_line_3': 'Leeds', 'address_line_4': 'West Yorkshire',
                            'address_line_5': 'UK', 'postcode': 'AB1 2CD'}
        remove_empty_values_mock.return_value = expected_address
        actual_address = convert_address(ods_address)
        remove_empty_values_mock.assert_called_with({'address_line_1': 'line 1', 'address_line_2': None,
                                                     'address_line_3': 'Leeds', 'address_line_4': 'West Yorkshire',
                                                     'address_line_5': 'UK', 'postcode': 'AB1 2CD'})
        self.assertEqual(expected_address, actual_address)

    @patch('common.utils.organisation_directory_utils.remove_empty_values')
    def test_convert_address_converts_address_with_address_line_2(self, remove_empty_values_mock):
        ods_address = {'line': ['line 1', 'line 2'], 'city': 'Leeds', 'district': 'West Yorkshire',
                       'country': 'UK', 'postalCode': 'AB1 2CD'}
        expected_address = {'address_line_1': 'line 1', 'address_line_2': 'line 2', 'address_line_3': 'Leeds',
                            'address_line_4': 'West Yorkshire', 'address_line_5': 'UK', 'postcode': 'AB1 2CD'}
        remove_empty_values_mock.return_value = expected_address
        actual_address = convert_address(ods_address)
        remove_empty_values_mock.assert_called_with(expected_address)
        self.assertEqual(expected_address, actual_address)

    def test_get_organisation_type_returns_default_organisation_if_extension_list_is_empty(self):
        ods_response_body = {'extension': []}
        expected_organisation_type = 'UNKNOWN'
        actual_organisation_type = get_organisation_type(ods_response_body)
        self.assertEqual(expected_organisation_type, actual_organisation_type)

    def test_get_organisation_type_returns_default_organisation_if_extension_list_has_no_role_extension(self):
        ods_response_body = {'extension': [{'url': 'wrong_url'}, {'url': 'another_wrong_url'}]}
        expected_organisation_type = 'UNKNOWN'
        actual_organisation_type = get_organisation_type(ods_response_body)
        self.assertEqual(expected_organisation_type, actual_organisation_type)

    def test_get_organisation_type_returns_default_organisation_if_role_extension_has_no_primary_role(self):
        ods_response_body = {'extension': [{'url': 'role_url', 'extension': []}]}
        expected_organisation_type = 'UNKNOWN'
        actual_organisation_type = get_organisation_type(ods_response_body)
        self.assertEqual(expected_organisation_type, actual_organisation_type)

    def test_get_organisation_type_returns_organisation_type_if_role_extension_has_primary_role(self):
        ods_response_body = {'extension': [{'url': 'role_url',
                                            'extension': [{"url": "role", "valueCoding": {"display": "PHARMACY"}},
                                                          {"url": "primaryRole", "valueBoolean": True}]}]}
        expected_organisation_type = 'PHARMACY'
        actual_organisation_type = get_organisation_type(ods_response_body)
        self.assertEqual(expected_organisation_type, actual_organisation_type)

    @patch('common.utils.organisation_directory_utils.dynamodb_put_item')
    @patch('common.utils.organisation_directory_utils.time')
    def test_create_new_organisation_record_saves_organisation_data_in_cache_table_given_organisation_is_active(
            self, time_mock, put_mock):
        organisation_code = 'AB123'
        organisation_data = {'some_key': 'some_value'}
        is_active = True
        time_mock.return_value = 1543296451
        expected_organisation_record = {'organisation_code': organisation_code, 'data': organisation_data,
                                        'expires': 1543296551}
        create_new_organisation_record(organisation_code, organisation_data, is_active)
        put_mock.assert_called_with(TableNames.ORGANISATIONS, expected_organisation_record)

    @patch('common.utils.organisation_directory_utils.dynamodb_put_item')
    @patch('common.utils.organisation_directory_utils.time')
    def test_create_new_organisation_record_saves_organisation_data_in_cache_table_given_organisation_is_inactive(
            self, time_mock, put_mock):
        organisation_code = 'AB123'
        organisation_data = {'some_key': 'some_value'}
        is_active = False
        time_mock.return_value = 1543296451
        expected_organisation_record = {'organisation_code': organisation_code, 'data': organisation_data,
                                        'expires': 1543296461}
        create_new_organisation_record(organisation_code, organisation_data, is_active)
        put_mock.assert_called_with(TableNames.ORGANISATIONS, expected_organisation_record)

    def test_get_phone_number(self):
        body = {"telecom": [{'system': 'phone', 'value': '01189 574614'}]}
        result = get_phone_number(body)
        expected_result = '01189 574614'

        self.assertEqual(expected_result, result)

    def test_no_phone_number(self):
        body = {"telecom": []}
        result = get_phone_number(body)
        expected_result = ''

        self.assertEqual(expected_result, result)

    def test_get_organisation_gp_practice_status_true(self):
        with open(os.path.join(DIRECTORY_NAME, '../../../../stubs/ods/organisations/B86044.json')) as api_response:
            api_response_mock = json.load(api_response)
        is_gp_practice = get_organisation_gp_practice_status(api_response_mock)

        self.assertEqual(is_gp_practice, True)

    def test_get_organisation_gp_practice_status_false(self):
        with open(os.path.join(DIRECTORY_NAME, '../../../../stubs/ods/organisations/FK994.json')) as api_response:
            api_response_mock = json.load(api_response)
        is_gp_practice = get_organisation_gp_practice_status(api_response_mock)

        self.assertEqual(is_gp_practice, False)

    def test_get_organisation_operational_periods_empty(self):
        api_response_mock = {}
        operational_periods = get_organisation_operational_periods(api_response_mock)

        self.assertListEqual(operational_periods, [])

    def test_get_organisation_operational_periods_start_only(self):
        with open(os.path.join(DIRECTORY_NAME, '../../../../stubs/ods/organisations/B86044.json')) as api_response:
            api_response_mock = json.load(api_response)
        operational_periods = get_organisation_operational_periods(api_response_mock)

        self.assertListEqual(operational_periods, [{'start': '1974-04-01'}])

    def test_get_organisation_operational_periods_start_and_end(self):
        with open(os.path.join(DIRECTORY_NAME, '../../../../stubs/ods/organisations/A81003.json')) as api_response:
            api_response_mock = json.load(api_response)
        operational_periods = get_organisation_operational_periods(api_response_mock)

        self.assertListEqual(operational_periods, [{'end': '2017-11-01', 'start': '1974-04-01'}])

    @unpack
    @data(
        ({'address': {'address_line_5': 'SCOTLAND'}}, False),
        ({'address': {'address_line_5': 'WALES'}}, False),
        ({'address': {'address_line_5': 'NORTHERN IRELAND'}}, False),
        ({'address': {'address_line_5': 'ENGLAND'}}, True),
        ({'address': {'address_line_5': 'CYPRUS'}}, True),
        ({'address': {}}, True),
        (None, True)
    )
    @patch('common.utils.organisation_directory_utils.get_organisation_information_by_organisation_code')
    def test_organisation_is_dms_valid(
        self,
        organisation,
        expected_response,
        mock_get_org
    ):
        mock_get_org.return_value = organisation

        actual_response = organisation_is_dms_valid('ABC123')

        self.assertEqual(actual_response, expected_response)
        mock_get_org.assert_called_with('ABC123')
