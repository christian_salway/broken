import unittest
from mock import patch, Mock
from datetime import datetime, timezone
from boto3.dynamodb.conditions import Key, Attr
from common.models.notification import NotificationType
from common.models.participant import ParticipantSortKey
from common.utils.dynamodb_access.table_names import TableNames

PARTICIPANT_ID = 12345678
RESULT_ID = 87654321
EXAMPLE_NOTIFICATION = {
    "participant_id":           "unique-uuid",
    "sort_key":                 "NOTIFICATION#2001-01-01#1#2020-08-25T13:48:08.000Z",
    "nhs_number":               "9234567890",
    "notification_date":        "2001-01-01",
    "notification_sequence":    1,
    "created":                  "2020-08-25T13:48:08.000Z",
    "type":                     "First Non-Responder",
    "migrated_8_data": {
        "received": "2020-02-14T13:26:25.432",
        "value":    "LS|9234567890|20010101|1|RP|N1|||"
    }
}

example_date = datetime(2020, 10, 18, 13, 48, 8, 123456, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

with patch('datetime.datetime', datetime_mock):
    import common.utils.notification_utils as notification_utils

mock_env_vars = {
    'DYNAMODB_PARTICIPANTS': 'participants_table'
}


@patch('os.environ', mock_env_vars)
class TestParticipantUtils(unittest.TestCase):

    @patch('common.utils.notification_utils.dynamodb_query')
    def test_query_latest_resendable_invite_reminder(self, query_mock):
        # Given
        query_mock.return_value = EXAMPLE_NOTIFICATION

        expected_response = EXAMPLE_NOTIFICATION
        expected_conditions = (
            Key('participant_id').eq(PARTICIPANT_ID) &
            Key('sort_key').gt('NOTIFICATION#2019-10-18#')
        )

        expected_filter_expression = Attr('type').eq(NotificationType.INVITATION.value) & (
            Attr('original_send_date').not_exists() | Attr('original_send_date').gt('2019-10-18')
        )

        # When
        actual_response = notification_utils.query_latest_resendable_invite_reminder(PARTICIPANT_ID)

        # Then
        query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=expected_conditions,
            FilterExpression=expected_filter_expression,
            ScanIndexForward=False
        ))

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.notification_utils.dynamodb_query')
    def test_get_notifications_from_result(self, query_mock):
        # Given
        query_mock.return_value = EXAMPLE_NOTIFICATION

        expected_response = EXAMPLE_NOTIFICATION
        expected_conditions = (
            Key('participant_id').eq(PARTICIPANT_ID) &
            Key('sort_key').begins_with('NOTIFICATION')
        )

        expected_filter_expression = Attr('type').eq(NotificationType.RESULT.value) & (
            Attr('record_id').exists() & Attr('record_id').eq(RESULT_ID)
        )

        # When
        actual_response = notification_utils.get_notifications_from_result(PARTICIPANT_ID, RESULT_ID)

        # Then
        query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=expected_conditions,
            FilterExpression=expected_filter_expression,
            ScanIndexForward=False
        ))

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_utils.dynamodb_query')
    def test_query_print_file_notifications(self, query_mock):

        def dummy_function(notifications, status):
            self.assertEqual(notifications, EXAMPLE_NOTIFICATION)

        # Given
        query_mock.return_value = {
            'Items': EXAMPLE_NOTIFICATION
        }

        expected_conditions = (
            Key('file_name').eq('file_name')
        )

        expected_filter_expression = Attr('sort_key').begins_with(ParticipantSortKey.NOTIFICATION.value)

        # When
        notification_utils.query_print_file_notifications(
            'file_name',
            dummy_function,
            {
                'status': 'status'
            }
        )

        # Then
        query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
                IndexName='file-name',
                KeyConditionExpression=expected_conditions,
                FilterExpression=expected_filter_expression
            ),
            return_all=True
        )

    @patch('common.utils.participant_utils.dynamodb_query')
    def test_query_print_file_notifications_and_filter(self, query_mock):

        test_notification_status = 'old_status'

        def dummy_function(notifications, status):
            self.assertEqual(notifications, EXAMPLE_NOTIFICATION)

        # Given
        query_mock.return_value = {
            'Items': EXAMPLE_NOTIFICATION
        }

        expected_conditions = (
            Key('file_name').eq('file_name')
        )

        expected_filter_expression = Attr('sort_key').begins_with(ParticipantSortKey.NOTIFICATION.value) & (
            Attr('status').eq(test_notification_status))

        # When
        notification_utils.query_print_file_notifications_and_filter(
            'file_name',
            test_notification_status,
            dummy_function,
            {
                'status': 'status'
            }
        )

        # Then
        query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
                IndexName='file-name',
                KeyConditionExpression=expected_conditions,
                FilterExpression=expected_filter_expression
            ),
            return_all=True
        )
