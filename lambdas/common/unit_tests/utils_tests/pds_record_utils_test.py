from unittest import TestCase
from common.utils.pds_record_utils import (
    get_pds_record_fields_for_signature
)


class TestPDSRecordUtils(TestCase):

    def test_get_pds_fields_for_signature(self):
        # Given
        pds_record = {
            'nhs_number': '1234567890',
            'first_name': 'Firsty',
            'last_name': 'Lasty',
            'title': 'Sir',
            'date_of_birth': '1-1-01',
            'address': {
                'address_line_1': '1 STREETY STREET',
                'address_line_2': 'NEIGHBOURHOOD',
                'address_line_3': 'TOWNSVILLE',
                'address_line_4': 'CITYHAM',
                'address_line_5': 'COUNTYSHIRE',
                'postcode': 'AB12 3DE'
            },
            'pds_timestamp': '1',
            'some_superfluous_field': 'abc123'
        }
        # When
        actual_record = get_pds_record_fields_for_signature(pds_record)
        # Then
        expected_record = {
            'nhs_number': '1234567890',
            'first_name': 'Firsty',
            'last_name': 'Lasty',
            'title': 'Sir',
            'date_of_birth': '1-1-01',
            'address': {
                'address_line_1': '1 STREETY STREET',
                'address_line_2': 'NEIGHBOURHOOD',
                'address_line_3': 'TOWNSVILLE',
                'address_line_4': 'CITYHAM',
                'address_line_5': 'COUNTYSHIRE',
                'postcode': 'AB12 3DE'
            },
            'pds_timestamp': '1'
        }
        self.assertEqual(expected_record, actual_record)
