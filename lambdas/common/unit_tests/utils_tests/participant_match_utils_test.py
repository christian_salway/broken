import unittest
import os
import json
from mock import patch
from ddt import ddt, data, unpack
from common.unit_tests.utils_tests.test_data.nhais_strategy import (
    nhs_number_single_participant,
    nhs_number_single_low_score_participant,
    nhs_number_high_score_participant_with_no_matching_address,
    nhs_number_multiple_participants,
    dob_high_score_participant,
    dob_high_score_participants,
    dob_medium_score_participants,
    no_matching_participants,
    nhs_number_single_participant_with_episode_addresses,
    nhs_number_single_participant_with_episode_addresses_with_matching_postcode,
    nhs_number_missing_participants,
    nhs_number_dob_last_name_missing_participants
)

with patch('boto3.resource') as resource_mock, patch('boto3.client') as boto3_client_mock:
    from common.utils.participant_match_utils import attempt_to_match


@ddt
@patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'participants-table'})
class TestParticipantMatchUtils(unittest.TestCase):

    @patch('common.utils.participant_match_utils._match_nhais_scoring')
    def test_attempt_to_match(
            self,
            match_nhais_scoring_mock):
        # Given
        dummy_participant = {'participant_id': 'abc123'}
        match_nhais_scoring_mock.return_value = (dummy_participant, None)
        expected_response = {'participant': dummy_participant}

        # When
        actual_response = attempt_to_match({})

        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_match_utils._match_nhais_scoring')
    def test_attempt_match_result_manual(
        self,
        match_nhais_scoring_mock
    ):

        # Arrange

        test_participants = [{'participant_id': 'test'}]
        match_nhais_scoring_mock.return_value = (None, test_participants)
        expected_response = {
            'manual_match_participants': test_participants
        }

        # Act

        actual_response = attempt_to_match({})

        # Assert

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_match_utils._match_nhais_scoring')
    def test_attempt_match_multiple_participants(
        self,
        nhais_mock
    ):
        # Arrange
        sample_participants = [{'Participant': 'A'}, {'Participant': 'B'}]

        nhais_mock.return_value = (None, sample_participants)
        expected_response = {
            'manual_match_participants': sample_participants
        }

        # Act

        actual_response = attempt_to_match({})

        # Assert
        self.assertEqual(expected_response, actual_response)

    @unpack
    @data(nhs_number_single_participant,
          nhs_number_single_low_score_participant,
          nhs_number_high_score_participant_with_no_matching_address,
          nhs_number_multiple_participants,
          dob_high_score_participant,
          dob_high_score_participants,
          dob_medium_score_participants,
          no_matching_participants,
          nhs_number_single_participant_with_episode_addresses,
          nhs_number_single_participant_with_episode_addresses_with_matching_postcode,
          nhs_number_missing_participants,
          nhs_number_dob_last_name_missing_participants)
    @patch('common.utils.participant_match_utils.query_for_live_episode')
    @patch('common.utils.participant_match_utils._query_dob_initial_lastname')
    @patch('common.utils.participant_match_utils.query_participants_by_nhs_number')
    @patch('common.utils.participant_match_utils.log')
    def test_nhais_strategy(
        self,
        test_data_filename,
        result_data_filename,
        expected_logging,
        log_mock,
        nhs_number_query_mock,
        dob_initial_lastname_query_mock,
        live_episode_mock,
    ):
        # Arrange

        with open(os.path.join(
            os.path.dirname(__file__),
            'test_data',
            'fixtures',
            result_data_filename
        )) as test_result_file, open(os.path.join(
            os.path.dirname(__file__),
            'test_data',
            'fixtures',
            test_data_filename
        )) as test_data_file:

            test_data = json.load(test_data_file)
            test_result = json.load(test_result_file)

            nhs_number_query_mock.return_value = test_data.get(
                'nhs_number_query',
                []
            )

            dob_initial_lastname_query_mock.return_value = test_data.get(
                'dob_initial_lastname_query',
                []
            )

            live_episode_mock.return_value = test_data.get(
                'live_episodes',
                []
            )

            # Act

            actual_response = attempt_to_match(test_result)

            # Assert

            self.assertEqual(actual_response, test_data['expected_response'])
            log_mock.assert_has_calls(expected_logging)
