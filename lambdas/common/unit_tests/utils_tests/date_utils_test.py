import common.utils.date_utils as environment
from unittest.case import TestCase
from mock import patch, Mock
from ddt import ddt, data, unpack
from datetime import datetime


@patch.object(environment, 'datetime', Mock(wraps=datetime))
@ddt
class TestDateUtils(TestCase):

    @unpack
    @data(('19900101', True), ('199001', True), ('1990', True), ('', False))
    def test_is_valid_date_format(self, date_string, expected_is_valid_date_format):
        is_valid_date_format = environment.is_valid_date_format(date_string)
        self.assertEqual(expected_is_valid_date_format, is_valid_date_format)

    @unpack
    @data(('19900101', '19900101'), ('199001', '19900101'), ('1990', '19900101'))
    def test_pad_date_string(self, date_string, expected_date_string):
        padded_date_string = environment.pad_date_string(date_string)
        self.assertEqual(expected_date_string, padded_date_string)
