import unittest
from mock import patch
from ddt import ddt, data, unpack
from datetime import date
from common.models.result import SELF_SAMPLE_SENDER_CODE
from common.models.letters import SupressionReason

from common.utils.audit_utils import AuditActions, AuditUsers
from common.log_references import CommonLogReference
from common.utils.result_letter_utils import (
    check_for_result_letter_suppression,
    check_for_result_letter_suppress_reason,
    add_result_letter_to_processing_queue,
)

TEST_INTERNAL_ID = 'ID001'
mock_env_vars = {
    'NOTIFICATION_QUEUE_URL': 'notification_queue_url',
    'RESULT_LETTER_CUT_OFF_MONTHS': '3'
}


@patch('os.environ', mock_env_vars)
@ddt
class TestResultLetterUtils(unittest.TestCase):

    @unpack
    @data(
        (
            {'result_id': '0', 'test_date': date.today().strftime('%Y-%m-%d')},
            None
        ),
        (
            {'participant_id': '0', 'test_date': '2020-10-28'},
            SupressionReason.OLD_TEST
        )
    )
    def test_suppressed_if_result_is_at_least_three_months_old(self,
                                                               result_record,
                                                               expected_response):

        suppress_reason = check_for_result_letter_suppress_reason(result_record)

        self.assertEqual(suppress_reason, expected_response)

    @unpack
    @data(
        (
            {
                'participant_id': '0',
                'test_date': date.today().strftime('%Y-%m-%d'),
                'sender_code': 'test'
            },
            None
        ),
        (
            {
                'participant_id': '0',
                'test_date': date.today().strftime('%Y-%m-%d'),
                'sender_source_type': '7',
                'sender_code': 'test'
            },
            SupressionReason.SOURCE_TYPE_7
        )

    )
    def test_suppressed_if_result_sender_code_is_seven(self,
                                                       participant_result_record,
                                                       expected_response):

        suppress_reason = check_for_result_letter_suppress_reason(participant_result_record)

        self.assertEqual(suppress_reason, expected_response)

    @unpack
    @data(
        (
            {},
            {'participant_id': '1', 'result_id': '0',
             'decoded': {'result_code': '0', 'infection_code': 'Q', 'action_code': 'R'}}
        ),
        (
            {'participant_id': '1'},
            {'participant_id': '1', 'result_id': '0',
             'decoded': {'result_code': '0', 'infection_code': 'Q', 'action_code': 'R'}},
        ),
        (
            {'participant_id': '1'},
            {'participant_id': '1', 'result_id': '0',
             'decoded': {'result_code': 'A', 'infection_code': 'B', 'action_code': 'C'}},
        )
    )
    @patch('common.utils.result_letter_utils.send_new_message')
    @patch('common.utils.result_letter_utils.get_participant_by_participant_id')
    @patch('common.utils.result_letter_utils.log')
    def test_result_letter_message_is_not_added_to_queue_if_fields_missing_or_code_not_found(
            self,
            participant_record,
            result_record,
            log_mock,
            get_participant_by_participant_id_mock,
            send_new_message_mock):
        # When
        get_participant_by_participant_id_mock.return_value = participant_record
        add_result_letter_to_processing_queue(result_record, False)

        # Then
        send_new_message_mock.assert_not_called()

    @unpack
    @data(
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '1', 'letter_address': 'letter_address', 'sort_key': 'RESULT#whenever',
             'result_code': '0', 'infection_code': 'Q', 'action_code': 'R'},
            {'participant_id': '0', 'sort_key': 'RESULT#whenever', 'type': 'Result letter', 'template': 'P0QR',
             'internal_id': TEST_INTERNAL_ID, 'address': 'letter_address'}
        )
    )
    @patch('common.utils.result_letter_utils.os.environ', mock_env_vars)
    @patch('common.utils.result_letter_utils.get_internal_id')
    @patch('common.utils.result_letter_utils.send_new_message')
    @patch('common.utils.result_letter_utils.get_participant_by_participant_id')
    @patch('common.utils.result_letter_utils.log')
    def test_standard_result_letter_message_is_added_to_queue_if_not_self_sampled_and_result_code_unknown(
            self,
            participant_record,
            result_record,
            expected_message,
            log_mock,
            get_participant_by_participant_id_mock,
            send_new_message_mock,
            get_internal_id_mock):
        # Given
        get_internal_id_mock.return_value = TEST_INTERNAL_ID
        get_participant_by_participant_id_mock.return_value = participant_record

        # When
        add_result_letter_to_processing_queue(result_record, False)

        # Then
        send_new_message_mock.assert_called_with(mock_env_vars['NOTIFICATION_QUEUE_URL'], expected_message)

    @unpack
    @data(
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '1',
             'decoded': {'result_code': '0', 'infection_code': 'Q', 'action_code': 'R',
                         'sender_code': SELF_SAMPLE_SENDER_CODE, 'letter_address': 'letter_address'}}
        )
    )
    @patch('common.utils.result_letter_utils.os.environ', mock_env_vars)
    @patch('common.utils.result_letter_utils.get_internal_id')
    @patch('common.utils.result_letter_utils.send_new_message')
    @patch('common.utils.result_letter_utils.get_participant_by_participant_id')
    @patch('common.utils.result_letter_utils.log')
    def test_standard_result_letter_message_is_not_added_to_queue_if_self_sampled_and_result_code_unknown(
            self,
            participant_record,
            result_record,
            log_mock,
            get_participant_by_participant_id_mock,
            send_new_message_mock,
            get_internal_id_mock):
        # Given
        get_participant_by_participant_id_mock.return_value = participant_record
        get_internal_id_mock.return_value = TEST_INTERNAL_ID

        # When
        add_result_letter_to_processing_queue(result_record, False)

        # Then
        send_new_message_mock.assert_not_called()

    @unpack
    @data(
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '0',
             'decoded': {'result_code': 'X', 'infection_code': '0', 'action_code': 'C',
                         'sender_code': SELF_SAMPLE_SENDER_CODE, 'letter_address': 'letter_address',
                         'self_sample': True}},
        ),
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '0',
             'decoded': {'result_code': 'X', 'infection_code': '9', 'action_code': 'R'}},
        ),
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '0',
             'decoded': {'result_code': 'X', 'infection_code': 'U', 'action_code': 'H'}},
        )
    )
    @patch('common.utils.result_letter_utils.os.environ', mock_env_vars)
    @patch('common.utils.result_letter_utils.send_new_message')
    @patch('common.utils.result_letter_utils.get_participant_by_participant_id')
    @patch('common.utils.result_letter_utils.log')
    def test_special_cases_X0C_X9R_XUH_result_letters_messages_not_added_to_queue_when_not_relevant(
            self,
            participant_record,
            result_record,
            log_mock,
            get_participant_by_participant_id_mock,
            send_new_message_mock):
        # Given
        get_participant_by_participant_id_mock.return_value = participant_record

        # When
        add_result_letter_to_processing_queue(result_record, False)

        # Then
        send_new_message_mock.assert_not_called()

    @unpack
    @data(
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '1', 'letter_address': 'letter_address', 'sort_key': 'RESULT#whenever',
             'result_code': 'X', 'infection_code': '0', 'action_code': 'A', 'self_sample': False},
            {'sort_key': 'RESULT#whenever', 'participant_id': '0', 'type': 'Result letter', 'template': 'PX0A',
             'internal_id': TEST_INTERNAL_ID, 'address': 'letter_address'}
        ),
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '1', 'letter_address': 'letter_address', 'sort_key': 'RESULT#whenever',
             'result_code': 'X', 'infection_code': '0', 'action_code': 'A', 'sender_code': '12345'},
            {'sort_key': 'RESULT#whenever', 'participant_id': '0', 'type': 'Result letter', 'template': 'PX0A',
             'internal_id': TEST_INTERNAL_ID, 'address': 'letter_address'}
        ),
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '1', 'letter_address': 'letter_address', 'sort_key': 'RESULT#whenever',
             'result_code': 'X', 'infection_code': '0', 'action_code': 'A', 'sender_code': SELF_SAMPLE_SENDER_CODE,
             'self_sample': True},
            {'sort_key': 'RESULT#whenever', 'participant_id': '0', 'type': 'Result letter', 'template': 'K-0A',
             'internal_id': TEST_INTERNAL_ID, 'address': 'letter_address'}
        ),
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '1', 'letter_address': 'letter_address', 'sort_key': 'RESULT#whenever',
             'result_code': 'X', 'infection_code': '9', 'action_code': 'R', 'sender_code': SELF_SAMPLE_SENDER_CODE,
             'self_sample': True},
            {'sort_key': 'RESULT#whenever', 'participant_id': '0', 'type': 'Result letter', 'template': 'K-9R',
             'internal_id': TEST_INTERNAL_ID, 'address': 'letter_address'}
        )
    )
    @patch('common.utils.result_letter_utils.os.environ', mock_env_vars)
    @patch('common.utils.result_letter_utils.get_latest_result_sorted_by_latest')
    @patch('common.utils.result_letter_utils.get_internal_id')
    @patch('common.utils.result_letter_utils.send_new_message')
    @patch('common.utils.result_letter_utils.get_participant_by_participant_id')
    @patch('common.utils.result_letter_utils.log')
    def test_special_cases_X0A_X9R_XUH_result_letters_messages_are_added_to_queue_when_relevant(
            self,
            participant_record,
            result_record,
            expected_message,
            log_mock,
            get_participant_by_participant_id_mock,
            send_new_message_mock,
            get_internal_id_mock,
            get_latest_result_sorted_by_latest_mock):
        # Given
        get_participant_by_participant_id_mock.return_value = participant_record
        get_internal_id_mock.return_value = TEST_INTERNAL_ID

        # When
        add_result_letter_to_processing_queue(result_record, False)

        # Then
        send_new_message_mock.assert_called_with(mock_env_vars['NOTIFICATION_QUEUE_URL'], expected_message)

    @unpack
    @data(
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '1', 'letter_address': 'letter_address', 'sort_key': 'RESULT#whenever',
             'result_code': 'X', 'infection_code': 'U', 'action_code': 'H', 'self_sample': True,
             'sender_code': SELF_SAMPLE_SENDER_CODE},
            [{'result_id': '1', 'result_code': 'X', 'infection_code': 'U', 'action_code': 'H'}],
            {'sort_key': 'RESULT#whenever', 'participant_id': '0', 'type': 'Result letter', 'template': 'K1UH',
             'internal_id': TEST_INTERNAL_ID, 'address': 'letter_address'}
        ),
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '1', 'letter_address': 'letter_address', 'sort_key': 'RESULT#whenever',
             'result_code': 'X', 'infection_code': 'U', 'action_code': 'H', 'self_sample': True,
             'sender_code': SELF_SAMPLE_SENDER_CODE},
            [{'result_id': '1', 'result_code': 'X', 'infection_code': 'U', 'action_code': 'H'},
             {'result_id': '2', 'result_code': 'X', 'infection_code': 'U', 'action_code': 'H'}],
            {'sort_key': 'RESULT#whenever', 'participant_id': '0', 'type': 'Result letter', 'template': 'K2UH',
             'internal_id': TEST_INTERNAL_ID, 'address': 'letter_address'}
        ),
        (
            {'participant_id': '0'},
            {'participant_id': '0', 'result_id': '1', 'letter_address': 'letter_address', 'sort_key': 'RESULT#whenever',
             'result_code': 'X', 'infection_code': 'U', 'action_code': 'H', 'self_sample': True,
             'sender_code': SELF_SAMPLE_SENDER_CODE},
            [{'result_id': '1', 'result_code': 'X', 'infection_code': 'U', 'action_code': 'H'},
             {'result_id': '2', 'result_code': 'X', 'infection_code': '0', 'action_code': 'A'},
             {'result_id': '3', 'result_code': 'X', 'infection_code': 'U', 'action_code': 'H'}],
            {'sort_key': 'RESULT#whenever', 'participant_id': '0', 'type': 'Result letter', 'template': 'K1UH',
             'internal_id': TEST_INTERNAL_ID, 'address': 'letter_address'}
        )
    )
    @patch('common.utils.result_letter_utils.os.environ', mock_env_vars)
    @patch('common.utils.result_letter_utils.get_latest_result_sorted_by_latest')
    @patch('common.utils.result_letter_utils.get_internal_id')
    @patch('common.utils.result_letter_utils.send_new_message')
    @patch('common.utils.result_letter_utils.get_participant_by_participant_id')
    @patch('common.utils.result_letter_utils.log')
    def test_result_letter_message_is_added_to_queue_for_special_case_XUH_when_relevant(
            self,
            participant_record,
            result_record,
            latest_results,
            expected_message,
            log_mock,
            get_participant_by_participant_id_mock,
            send_new_message_mock,
            get_internal_id_mock,
            get_latest_result_sorted_by_latest_mock):
        # Given
        get_participant_by_participant_id_mock.return_value = participant_record
        get_internal_id_mock.return_value = TEST_INTERNAL_ID
        get_latest_result_sorted_by_latest_mock.return_value = latest_results

        # When
        add_result_letter_to_processing_queue(result_record, False)

        # Then
        send_new_message_mock.assert_called_with(mock_env_vars['NOTIFICATION_QUEUE_URL'], expected_message)

    @unpack
    @data(
        ({'result_code': 'X', 'infection_code': '0', 'action_code': 'A',
          'self_sample': True, 'sender_code': SELF_SAMPLE_SENDER_CODE}, 'K-0C'),
        ({'result_code': 'X', 'infection_code': '0', 'action_code': 'A',
          'sender_code': '12345'}, 'PX0C')
    )
    @patch('common.utils.result_letter_utils.os.environ', mock_env_vars)
    @patch('common.utils.result_letter_utils.get_internal_id')
    @patch('common.utils.result_letter_utils.send_new_message')
    @patch('common.utils.result_letter_utils.get_participant_by_participant_id')
    @patch('common.utils.result_letter_utils.log')
    def test_correct_template_for_cease_and_result_letter_selected_when_participant_was_autoceased(
            self,
            result_record,
            expected_letter_template_code,
            log_mock,
            get_participant_by_participant_id_mock,
            send_new_message_mock,
            get_internal_id_mock):
        # Given
        get_participant_by_participant_id_mock.return_value = {'participant_id': '0'}
        get_internal_id_mock.return_value = TEST_INTERNAL_ID

        result_record_to_test = {
            **result_record,
            'participant_id': '0',
            'result_id': '1',
            'letter_address': 'letter_address',
            'sort_key': 'RESULT#whenever',
        }

        # When
        add_result_letter_to_processing_queue(result_record_to_test, True)

        # Then
        expected_message = {
            'sort_key': 'RESULT#whenever',
            'participant_id': '0',
            'type': 'Result letter',
            'template': expected_letter_template_code,
            'internal_id': TEST_INTERNAL_ID,
            'address': 'letter_address'
        }
        send_new_message_mock.assert_called_with(mock_env_vars['NOTIFICATION_QUEUE_URL'], expected_message)

    @patch('common.utils.result_letter_utils.audit')
    @patch('common.utils.result_letter_utils.log')
    @patch('common.utils.result_letter_utils.check_for_result_letter_suppress_reason')
    def test_check_for_result_letter_suppression_logs_result_suppression_if_letter_suppressed(
            self, check_if_suppress_mock, log_mock, audit_mock):
        check_if_suppress_mock.return_value = 'Suppressed because reasons'
        result = {'result_id': 'the_result_id', 'participant_id': 'the_participant_id', 'test_date': '2020-01-14'}
        suppression_reason = 'Suppressed because reasons'
        check_if_suppress_mock.return_value = suppression_reason

        actual_response = check_for_result_letter_suppression(result)

        self.assertEqual(actual_response, suppression_reason)

        log_mock.assert_called_once_with(
            {'log_reference': CommonLogReference.MATCHRLET0004,
             'participant id': 'the_participant_id',
             'result_id': 'the_result_id',
             'suppression_reason': suppression_reason}
        )
        audit_mock.assert_called_once_with(
            action=AuditActions.RESULT_LETTER_SUPPRESSED,
            user=AuditUsers.SYSTEM,
            participant_ids=['the_participant_id'],
            additional_information={'test_date': '2020-01-14', 'suppression_reason': 'Suppressed because reasons'})

    @patch('common.utils.result_letter_utils.audit')
    @patch('common.utils.result_letter_utils.log')
    @patch('common.utils.result_letter_utils.check_for_result_letter_suppress_reason')
    def test_check_for_result_letter_suppression_returns_false_if_no_suppression_reason(
            self, check_if_suppress_mock, log_mock, audit_mock):
        result = {'result_id': 'the_result_id', 'participant_id': 'the_participant_id', 'test_date': '2020-01-14'}
        check_if_suppress_mock.return_value = None

        actual_response = check_for_result_letter_suppression(result)

        self.assertEqual(actual_response, None)

        log_mock.assert_not_called()
        audit_mock.assert_not_called()
