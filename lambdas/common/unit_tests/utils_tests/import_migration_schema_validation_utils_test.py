from unittest.case import TestCase
from common.utils.import_migration_schema_validation_utils import (
    ImportMigrationSchemaValidation as schema)

SCHEMA_ERROR_KEY = 'schema_error'


class TestDateSchemaValidation(TestCase):

    def test_validate_next_test_due_date_schema_is_successful(self):
        # Act
        response = schema().validate_date('20200325')

        # Assert
        self.assertEqual(response, None)

    def test_validate_optional_date_schema_no_data_is_successful(self):
        # Act
        response = schema().validate_date('', is_required=False)

        # Assert
        self.assertEqual(response, None)

    def test_validate_next_test_due_date_schema_returns_is_required_error(self):
        # Arrange
        expected_response = {SCHEMA_ERROR_KEY: 'Date is a required value'}

        # Act
        actual_response = schema().validate_date('', True)

        # Assert
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_next_test_due_date_schema_returns_custom_date_format_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Date was in an invalid format. Expected YYYYMMDD'}

        # Act
        actual_response = schema().validate_date('bad-date')

        # Assert
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_next_test_due_date_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Date length must be exactly 8 characters'}

        # Act
        actual_response = schema().validate_date('202000325')

        # Assert
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_previous_next_test_due_date_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Date length must be exactly 8 characters'}

        # Act
        actual_response = schema().validate_previous_test_due_date('202000325')

        # Assert
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_previous_next_test_due_date_schema_accepts_NONE_as_valid_value(self):
        # Act
        response = schema().validate_previous_test_due_date('NONE')

        # Assert
        self.assertEqual(response, None)


class TestRecallTypeSchemaValidation(TestCase):
    def test_validate_recall_type_schema_is_successful(self):
        # Act
        response = schema().validate_recall_type('R')

        # Assert
        self.assertEqual(response, None)

    def validate_recall_type_schema_returns_required_value_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Recall Type must be 1 character long'}

        # Act
        actual_response = schema().validate_recall_type('')

        # Assert
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_recall_type_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Recall Type must be 1 character long'}

        # Act
        actual_response = schema().validate_recall_type('PP')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestRecallStatSchemaValidation(TestCase):

    def test_validate_recall_stat_schema_is_successful(self):
        # Act
        response = schema().validate_recall_stat('1')

        # Assert
        self.assertEqual(response, None)

    def test_validate_recall_stat_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Recall Status must be 1 character long'}

        # Act
        actual_response = schema().validate_recall_stat('PP')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestPosDoubtSchemaValidation(TestCase):

    def test_validate_pos_doubt_schema_is_successful(self):
        # Act
        response = schema().validate_pos_doubt('N')

        # Assert
        self.assertEqual(response, None)

    def test_validate_pos_doubt_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Pos Doubt must be 1 character long'}

        # Act
        actual_response = schema().validate_pos_doubt('PP')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestFNRCountSchemaValidation(TestCase):

    def test_validate_fnr_count_schema_is_successful_for_blank_values(self):
        # Act
        response = schema().validate_fnr_count('')

        # Assert
        self.assertEqual(response, None)

    def test_validate_fnr_count_schema_is_successful_for_small_values(self):
        # Act
        response = schema().validate_fnr_count('0')

        # Assert
        self.assertEqual(response, None)

    def test_validate_fnr_count_schema_is_successful(self):
        # Act
        response = schema().validate_fnr_count('00')

        # Assert
        self.assertEqual(response, None)

    def test_validate_fnr_count_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'FNR Count must be between 0 and 2 characters (inclusive)'}

        # Act
        actual_response = schema().validate_fnr_count('010')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestNotesSchemaValidation(TestCase):

    def test_validate_notes_schema_is_successful(self):
        # Act
        response = schema().validate_notes('This is a test note', 1)

        # Assert
        self.assertEqual(response, None)

    def test_validate_notes_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Notes3 must be between 1 and 60 characters (inclusive)'}

        # Act
        actual_response = schema().validate_notes(
            'This is a piece of text that has a length greater than 60, so the expected error should occur', 3)

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestLDNSchemaValidation(TestCase):

    def test_validate_ldn_schema_is_successful(self):
        # Act
        response = schema().validate_ldn('N9')

        # Assert
        self.assertEqual(response, None)

    def test_validate_ldn_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {SCHEMA_ERROR_KEY: 'LDN must be 2 characters long'}

        # Act
        actual_response = schema().validate_ldn('L')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestGenderSchemaValidation(TestCase):

    def test_validate_gender_schema_is_successful(self):
        # Act
        response = schema().validate_gender('M')

        # Assert
        self.assertEqual(response, None)

    def test_validate_gender_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Gender must be 1 character long'}

        # Act
        actual_response = schema().validate_gender('LL')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestGPCodeSchemaValidation(TestCase):

    def test_validate_gp_code_schema_is_successful(self):
        # Act
        response = schema().validate_gp_code('P12345')

        # Assert
        self.assertEqual(response, None)

    def test_validate_gp_code_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'GP Code must be between 1 and 6 characters (inclusive)'}

        # Act
        actual_response = schema().validate_gp_code('AB123456')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestGPPracticeCodeSchemaValidation(TestCase):
    def test_validate_gp_practice_code_schema_is_successful(self):
        # Act
        response = schema().validate_gp_practice_code('P12345')

        # Assert
        self.assertEqual(response, None)

    def test_validate_gp_practice_code_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'GP Practice Code must be 6 characters long'}

        # Act
        actual_response = schema().validate_gp_practice_code('AB123')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestReasonForRemovalSchemaValidation(TestCase):
    def test_validate_reason_for_removal_schema_for_removal_schema_is_successful(self):
        # Act
        response = schema().validate_reason_for_removal('O')

        # Assert
        self.assertEqual(response, None)

    def test_validate_reason_for_removal_schema_for_removal_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Reason for removal must be between 1 and 3 characters (inclusive)'}

        # Act
        actual_response = schema().validate_reason_for_removal('AB123')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestDestinationSchemaValidation(TestCase):
    def test_validate_destination_schema_is_successful(self):
        # Act
        response = schema().validate_destination('ABC')

        # Assert
        self.assertEqual(response, None)

    def test_validate_destination_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Destination must be between 1 and 3 characters (inclusive)'}

        # Act
        actual_response = schema().validate_destination('AB123')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestTitleSchemaValidation(TestCase):
    def test_validate_title_schema_is_successful(self):
        # Act
        response = schema().validate_title('Mrs')

        # Assert
        self.assertEqual(response, None)

    def test_validate_title_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Title must be between 2 and 4 characters (inclusive)'}

        # Act
        actual_response = schema().validate_title('Messrs')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestSurnameSchemaValidation(TestCase):
    def test_validate_surname_schema_is_successful(self):
        # Act
        response = schema().validate_surname('Smith')

        # Assert
        self.assertEqual(response, None)

    def test_validate_surname_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Surname must be between 1 and 20 characters (inclusive)'}

        # Act
        actual_response = schema().validate_surname('Jonesington Smith the 5th Esquire')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestFirstForenameSchemaValidation(TestCase):
    def test_validate_first_forename_schema_is_successful(self):
        # Act
        response = schema().validate_first_forename('John')

        # Assert
        self.assertEqual(response, None)

    def test_validate_first_forename_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'First forename must be between 1 and 22 characters (inclusive)'}

        # Act
        actual_response = schema().validate_first_forename('Firsty Frankie Firsterson')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestOtherForenamesSchemaValidation(TestCase):
    def test_validate_other_forenames_schema_is_successful(self):
        # Act
        response = schema().validate_other_forenames('John')

        # Assert
        self.assertEqual(response, None)

    def test_validate_other_forenames_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Other forename must be between 1 and 22 characters (inclusive)'}

        # Act
        actual_response = schema().validate_other_forenames(
            'Otherie Fornamerie Fornamerson')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestAddressSchemaValidation(TestCase):
    def test_validate_address_schema_is_successful(self):
        # Act
        response = schema().validate_address('5 Streety Street', 'Address 1')

        # Assert
        self.assertEqual(response, None)

    def test_validate_address_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Locality must be between 1 and 30 characters (inclusive)'}

        # Act
        actual_response = schema().validate_address(
            'Reallylonglocalityreallylonglocality', 'Locality')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestCountySchemaValidation(TestCase):
    def test_validate_county_schema_is_successful(self):
        # Act
        response = schema().validate_county('Lancashire')

        # Assert
        self.assertEqual(response, None)

    def test_validate_county_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'County must be between 1 and 20 characters (inclusive)'}

        # Act
        actual_response = schema().validate_county('Reallylongcountyreallylongshire')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestPostcodeSchemaValidation(TestCase):
    def test_validate_postcode_schema_is_successful(self):
        # Act
        response = schema().validate_postcode('OL13 1RY')

        # Assert
        self.assertEqual(response, None)

    def test_validate_postcode_schema_returns_custom_invalid_length_error(self):
        # Arrange
        expected_response = {
            SCHEMA_ERROR_KEY: 'Postcode must be between 1 and 8 characters (inclusive)'}

        # Act
        actual_response = schema().validate_postcode('OL23 RETY')

        # Assert
        self.assertDictEqual(expected_response, actual_response)


class TestSourceCipherSchemaValidation(TestCase):

    def test_validate_source_cipher_schema_is_successful(self):
        response = schema().validate_source_cipher('ABC')
        self.assertEqual(response, None)

    def test_validate_source_cipher_schema_returns_is_required_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Source cipher is a required value'}
        actual_response = schema().validate_source_cipher('')
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_source_cipher_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Source cipher must be between 2 and 3 characters (inclusive)'}
        actual_response = schema().validate_source_cipher('A')
        self.assertDictEqual(expected_response, actual_response)


class TestNhsNumberSchemaValidation(TestCase):

    def test_validate_nhs_number_schema_is_successful(self):
        response = schema().validate_nhs_number('9876543210')
        self.assertEqual(response, None)

    def test_validate_nhs_number_schema_returns_is_required(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'NHS number is a required value'}
        actual_response = schema().validate_nhs_number('')
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_nhs_number_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'NHS number must be between 1 and 14 characters (inclusive)'}
        actual_response = schema().validate_nhs_number(
            'This is far too long of an NHS number')
        self.assertDictEqual(expected_response, actual_response)


class TestSequenceNumberSchemaValidation(TestCase):
    def test_validate_sequence_number_schema_is_successful(self):
        seq_number = 'A'
        response = schema().validate_sequence_number(seq_number)
        self.assertEqual(response, None)

    def test_validate_sequence_number_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Sequence number must be 1 character long'}
        actual_response = schema().validate_sequence_number('ABC')
        self.assertDictEqual(expected_response, actual_response)


class TestTestSequenceNumberSchemaValidation(TestCase):
    def test_validate_test_sequence_number_schema_is_successful(self):
        test_seq_number = '12'
        response = schema().validate_test_sequence_number(test_seq_number)
        self.assertEqual(response, None)

    def test_validate_test_sequence_number_schema_returns_custom_invalid_length_error_too_short(self):
        test_seq_number = '1'
        response = schema().validate_test_sequence_number(test_seq_number)
        self.assertEqual(response, None)

    def test_validate_test_sequence_number_schema_returns_custom_invalid_length_error_too_long(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Test sequence number must be between 1 and 2 characters (inclusive)'
        }
        actual_response = schema().validate_test_sequence_number('123')

        self.assertDictEqual(expected_response, actual_response)


class TestResultCodeSchemaValidation(TestCase):
    def test_validate_test_result_code_schema_is_successful(self):
        test_result_code = 'A'
        response = schema().validate_test_result_code(test_result_code)
        self.assertEqual(response, None)

    def test_validate_test_result_code_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Test result code must be 1 character long'}
        actual_response = schema().validate_test_result_code('AA')
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_test_result_code_schema_returns_required_value_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Test result code is a required value'}
        actual_response = schema().validate_test_result_code('')
        self.assertDictEqual(expected_response, actual_response)


class TestActionCodeSchemaValidation(TestCase):
    def test_validate_action_code_schema_is_successful(self):
        action_code = 'A'
        response = schema().validate_action_code(action_code)
        self.assertEqual(response, None)

    def test_validate_action_code_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Action code must be 1 character long'}
        actual_response = schema().validate_action_code('AA')
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_action_code_schema_returns_required_value_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Action code is a required value'}
        actual_response = schema().validate_action_code('')
        self.assertDictEqual(expected_response, actual_response)


class TestInfectionCodeSchemaValidation(TestCase):
    def test_validate_infection_code_schema_is_successful(self):
        infection_code = 'A'
        response = schema().validate_infection_code(infection_code)
        self.assertEqual(response, None)

    def test_validate_infection_code_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Infection code must be 1 character long'}
        actual_response = schema().validate_infection_code('AA')
        self.assertDictEqual(expected_response, actual_response)


class TestSourceCodeSchemaValidation(TestCase):
    def test_validate_source_code_schema_is_successful(self):
        infection_code = 'A'
        response = schema().validate_source_code(infection_code)
        self.assertEqual(response, None)

    def test_validate_source_code_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Source code must be 1 character long'}
        actual_response = schema().validate_source_code('AA')
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_source_code_schema_returns_required_value_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Source code is a required value'}
        actual_response = schema().validate_source_code('')
        self.assertDictEqual(expected_response, actual_response)


class TestLabNationalSchemaValidation(TestCase):
    def test_validate_lab_national_schema_is_successful(self):
        response = schema().validate_lab_national_code('AAAAA')
        self.assertEqual(response, None)

    def test_validate_lab_national_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Lab national code must be 5 characters long'}
        actual_response = schema().validate_lab_national_code('AA')
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_lab_local_schema_returns_required_value_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Lab national code is a required value'}
        actual_response = schema().validate_lab_national_code('')
        self.assertDictEqual(expected_response, actual_response)


class TestLabLocalSchemaValidation(TestCase):
    def test_validate_lab_local_schema_is_successful(self):
        lab_locale = 'A'
        response = schema().validate_lab_local(lab_locale)
        self.assertEqual(response, None)

    def test_validate_lab_local_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Lab local must be 1 character long'}
        actual_response = schema().validate_lab_local('AA')
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_lab_local_schema_returns_required_value_error(self):
        expected_response = {SCHEMA_ERROR_KEY: 'Lab local is a required value'}
        actual_response = schema().validate_lab_local('')
        self.assertDictEqual(expected_response, actual_response)


class TestRepeatMonthsSchemaValidation(TestCase):
    def test_validate_repeat_months_schema_is_successful(self):
        response = schema().validate_repeat_months('10')
        self.assertEqual(response, None)

    def test_validate_repeat_months_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Repeat months must be between 1 and 2 characters (inclusive)'}
        actual_response = schema().validate_repeat_months('101')
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_repeat_month_schema_returns_required_value_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Repeat months is a required value'}
        actual_response = schema().validate_repeat_months('')
        self.assertDictEqual(expected_response, actual_response)


class TestReasonSchemaValidation(TestCase):
    def test_validate_reason_schema_is_successful(self):
        for reason in ['1', '2', '3', '4', '5', '10', '11', '6', '7', '8', '9', '77', '99', '9C']:
            response = schema().validate_reason(reason)
            self.assertEqual(response, None)

    def test_validate_reason_schema_cipher_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Reason must be between 1 and 2 characters (inclusive)'}
        actual_response = schema().validate_reason('ABCD')
        self.assertDictEqual(expected_response, actual_response)


class TestUserIDSchemaValidation(TestCase):
    def test_validate_user_id_schema_is_successful(self):
        user_id = 'AA'
        response = schema().validate_user_id(user_id)
        self.assertEqual(response, None)

    def test_validate_user_id_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'User ID must be between 2 and 9 characters (inclusive)'}
        actual_response = schema().validate_user_id('A')
        self.assertDictEqual(expected_response, actual_response)


class TestReasonFlagSchemaValidation(TestCase):
    def test_validate_reason_flag_schema_is_successful(self):
        reason = 'A'
        response = schema().validate_reason_flag(reason)
        self.assertEqual(response, None)

    def test_validate_reason_flag_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Reason flag must be 1 character long'}
        actual_response = schema().validate_reason_flag('ABC')
        self.assertDictEqual(expected_response, actual_response)


class TestCeaseCodeSchemaValidation(TestCase):
    def test_validate_cease_code_schema_is_successful(self):
        for cease_code in ['1', 'C', 'D', 'F', 'N', 'Y', 'G', 'H']:
            response = schema().validate_cease_code(cease_code)
            self.assertEqual(response, None)

    def test_validate_cease_code_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Cease code must be 1 character long'}
        actual_response = schema().validate_cease_code('ABC')
        self.assertDictEqual(expected_response, actual_response)


class TestCeaseCipherSchemaValidation(TestCase):
    def test_validate_cease_cipher_schema_is_successful(self):
        cease_cipher = 'AA'
        response = schema().validate_cease_cipher(cease_cipher)
        self.assertEqual(response, None)

    def test_validate_cease_cipher_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Cease cipher must be between 2 and 3 characters (inclusive)'}
        actual_response = schema().validate_cease_cipher('A')
        self.assertDictEqual(expected_response, actual_response)


class TestTestCipherSchemaValidation(TestCase):
    def test_validate_test_cipher_schema_is_successful(self):
        test_cipher = 'AA'
        response = schema().validate_test_cipher(test_cipher)
        self.assertEqual(response, None)

    def test_validate_test_cipher_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Test cipher must be between 2 and 3 characters (inclusive)'}
        actual_response = schema().validate_test_cipher('A')
        self.assertDictEqual(expected_response, actual_response)


class TestCommSchemaValidation(TestCase):
    def test_validate_comm_schema_is_successful(self):
        comm = 'This is a notes section under the 80 character limit.'
        response = schema().validate_comm(comm)
        self.assertEqual(response, None)

    def test_validate_comm_schema_returns_custom_invalid_length_error(self):
        comm = "This is far far far too long of a comments line. It should really throw an error."
        expected_response = {
            SCHEMA_ERROR_KEY: 'Comm must be between 1 and 80 characters (inclusive)'}
        actual_response = schema().validate_comm(comm)
        self.assertDictEqual(expected_response, actual_response)

    def test_validate_amend_flag_schema_is_successful(self):
        amend_flag = 'AMEND'
        response = schema().validate_amend_flag(amend_flag)
        self.assertEqual(response, None)

    def test_validate_amend_flag_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Amend flag must be 5 characters long'}
        actual_response = schema().validate_amend_flag('ABC')
        self.assertDictEqual(expected_response, actual_response)


class TestValidateNationalSenderCodeSchemaValidation(TestCase):
    def test_validate_national_sender_code_schema_is_successful(self):
        response = schema().validate_national_sender_code('ABC123')
        self.assertEqual(response, None)

    def test_validate_national_sender_code_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'National Sender code must be 6 characters long'}
        actual_response = schema().validate_national_sender_code('ABC')
        self.assertDictEqual(expected_response, actual_response)


class TestValidateSenderCodeSchemaValidation(TestCase):
    def test_validate_sender_code_schema_is_successful(self):
        response = schema().validate_sender_code('ABC123')
        self.assertEqual(response, None)

    def test_validate_sender_code_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Sender code must be between 1 and 6 characters (inclusive)'}
        actual_response = schema().validate_sender_code('ABCDEF1')
        self.assertDictEqual(expected_response, actual_response)


class TestValidateSenderExistsSchemaValidation(TestCase):
    def test_validate_sender_exists_schema_is_successful_valid_value(self):
        response = schema().validate_sender_exist('Y')
        self.assertEqual(None, response)

    def test_validate_sender_exists_schema_is_successful_empty_value(self):
        response = schema().validate_sender_exist('')
        self.assertEqual(None, response)

    def test_validate_sender_exists_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Sender exist must be 1 character long'}
        actual_response = schema().validate_sender_exist('ABCDEF1')
        self.assertDictEqual(expected_response, actual_response)


class TestValidateSlideNumberSchemaValidation(TestCase):
    def test_validate_slide_number_schema_is_successful(self):
        response = schema().validate_slide_number('1234ABCD')
        self.assertEqual(response, None)

    def test_validate_slide_number_schema_returns_custom_invalid_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Slide number must be 8 characters long'}
        actual_response = schema().validate_slide_number('ABCDEF1')
        self.assertDictEqual(expected_response, actual_response)


class TestValidateHPVSchemaValidation(TestCase):
    def test_validate_hpv_schema_is_successful(self):
        response = schema().validate_hpv('Y')
        self.assertEqual(response, None)

    def test_validate_hpv_schema_returns_custom_invalid_length_error(self):
        expected_response = {SCHEMA_ERROR_KEY: 'HPV must be 1 character long'}
        actual_response = schema().validate_hpv('ABCDEF1')
        self.assertDictEqual(expected_response, actual_response)


class TestInvitationDateSchemaValidation(TestCase):
    def test_validatevalidate_invitation_date_is_successful(self):
        response_happy = schema().validate_invite_date('202011')
        self.assertEqual(response_happy, None)

    def test_validatevalidate_invitation_date_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Date length must be exactly 6 characters'}
        response = schema().validate_invite_date('20201')
        self.assertEqual(expected_response, response)

    def test_validatevalidate_invitation_date_format_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'Invite date was in an invalid format. Expected YYYYMM'}
        response = schema().validate_invite_date('2C2A1W')
        self.assertEqual(expected_response, response)


class TestValidatevalidatePathIdchemaValidation(TestCase):
    def test_validatevalidate_path_id_is_successful(self):
        response_happy = schema().validate_path_id('30')
        self.assertEqual(response_happy, None)

    def test_validatevalidate_path_id_null_error(self):
        expected_response = {SCHEMA_ERROR_KEY: 'PATH_ID value was empty, PATH_ID must be between 1 and 2 characters' +
                             ' (inclusive), PATH_ID is expected to be an integer'}
        response = schema().validate_path_id('')
        self.assertEqual(expected_response, response)

    def test_validatevalidate_path_id_length_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'PATH_ID must be between 1 and 2 characters (inclusive)'}
        response = schema().validate_path_id('302')
        self.assertEqual(expected_response, response)

    def test_validatevalidate_path_id_is_not_int_error(self):
        expected_response = {
            SCHEMA_ERROR_KEY: 'PATH_ID is expected to be an integer'}
        response = schema().validate_path_id('AB')
        self.assertEqual(expected_response, response)
