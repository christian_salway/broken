import json
import os
import base64
from ddt import ddt, data, unpack
from mock import call, patch, MagicMock
from requests.exceptions import HTTPError, RequestException, ConnectionError
from unittest import TestCase
from common.utils.pds_fhir_utils import (
    _map_pds_fhir_gp,
    _map_pds_fhir_patient_name,
    get_pds_fhir_access_token,
    get_pds_fhir_initial_access_tokens,
    map_pds_fhir_patient,
    pds_fhir_access_token_valid,
    perform_pds_fhir_redirect_to_search,
    refresh_pds_fhir_access_token,
    search_pds_fhir_by_demographics,
    search_pds_fhir_by_nhs_number,
    can_workgroup_access_participant
)
from common.log_references import CommonLogReference

MODULE_NAME = 'common.utils.pds_fhir_utils'
FROZEN_TIME = 1625488388
EXAMPLE_UUID = 'df6fb4bd-2819-4c32-a23e-cb5edc10a04c'


class MockRequestsResponse:
    json_obj: str = None
    status_code: int = 200
    error: RequestException = None
    text: str = 'TEST123'

    def __init__(
        self,
        json_obj,
        status_code=200,
        error=None
    ):
        self.json_obj = json_obj
        self.status_code = status_code
        self.error = error

    def json(self):
        return self.json_obj

    def raise_for_status(self):
        if self.error:
            raise self.error


@ddt
@patch(f'{MODULE_NAME}.log')
@patch(f'{MODULE_NAME}.requests')
@patch(f'{MODULE_NAME}.uuid4', MagicMock(return_value=EXAMPLE_UUID))
@patch('os.environ', {
    'PDS_FHIR_BASE_URL': 'https://fhir.example.org/api',
    'PDS_FHIR_AUTH_URL': 'https://fhir.example.org/auth',
    'PDS_FHIR_CLIENT_SECRET_ID': 'SECRETID',
    'PDS_FHIR_CALLBACK_URL': 'https://example.org/callback',
    'DNS_NAME': 'environment.example.org',
})
class PdsFhirUtilsTest(TestCase):

    @unpack
    @data(
        (
            {},
            False
        ),
        (
            {
                'pds_access_token': 'token123'
            },
            False
        ),
        (
            {
                'pds_access_token': 'token123',
                'pds_reauthenticate_timestamp': 1625488389
            },
            True
        ),
        (
            {
                'pds_access_token': 'token123',
                'pds_reauthenticate_timestamp': 1625488387
            },
            False
        )
    )
    @patch(f'{MODULE_NAME}.time')
    def test_pds_fhir_access_token_valid(
        self,
        session,
        expected_valid,
        time_mock,
        log_mock,
        requests_mock
    ):
        time_mock.return_value = FROZEN_TIME
        actual_valid = pds_fhir_access_token_valid(session)
        self.assertEqual(actual_valid, expected_valid)

    @patch(f'{MODULE_NAME}.time')
    @patch(f'{MODULE_NAME}.update_session')
    @patch(f'{MODULE_NAME}.get_secret')
    def test_refresh_pds_fhir_access_token(
        self,
        get_secret_mock,
        update_session_mock,
        time_mock,
        requests_mock,
        log_mock
    ):
        session = {
            'token': 'SESSION123',
            'pds_refresh_token': 'REFRESH123'
        }

        requests_mock.post.return_value = MockRequestsResponse(
            {
                'access_token': 'ACCESS123',
                'expires_in': 100,
                'refresh_token': 'REFRESH123',
                'refresh_token_expires_in': 200
            }
        )

        get_secret_mock.return_value = '{"client_id": "CLIENT123", "client_secret": "CLIENTSECRET123"}'
        time_mock.return_value = FROZEN_TIME
        expected_access_token = 'ACCESS123'

        actual_access_token = refresh_pds_fhir_access_token(session)

        self.assertEqual(expected_access_token, actual_access_token)

        update_session_mock.assert_called_with(
            'SESSION123',
            {
                'pds_access_token': 'ACCESS123',
                'pds_access_token_expiry': 1625488488,
                'pds_refresh_token': 'REFRESH123',
                'pds_reauthenticate_timestamp': 1625488588
            },
            delete_attributes=['hashed_session_token']
        )

        requests_mock.post.assert_called_with(
            'https://fhir.example.org/auth/token',
            headers={
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            parameters={
                'client_id': 'CLIENT123',
                'client_secret': 'CLIENTSECRET123',
                'grant_type': 'refresh_token',
                'refresh_token': 'REFRESH123'
            }
        )

    @unpack
    @data(
        (
            HTTPError(),
            {
                'log_reference': CommonLogReference.PDSFHIR0004,
                'status_code': 403,
                'error': {
                    'error': 'access_denied'
                }
            }
        ),
        (
            ConnectionError('Never heard of that server'),
            {
                'log_reference': CommonLogReference.PDSFHIR0004,
                'error': 'Never heard of that server'
            }
        ),
    )
    @patch(f'{MODULE_NAME}.time')
    @patch(f'{MODULE_NAME}.update_session')
    @patch(f'{MODULE_NAME}.get_secret')
    def test_refresh_pds_fhir_access_token_error(
        self,
        exception_to_throw,
        expected_log_call,
        get_secret_mock,
        update_session_mock,
        time_mock,
        requests_mock,
        log_mock
    ):
        session = {
            'token': 'SESSION123',
            'pds_refresh_token': 'REFRESH123'
        }

        requests_mock.post.return_value = MockRequestsResponse(
            {
                'error': 'access_denied'
            },
            403,
            exception_to_throw
        )

        get_secret_mock.return_value = '{"client_id": "CLIENT123", "client_secret": "CLIENTSECRET123"}'
        time_mock.return_value = FROZEN_TIME

        with self.assertRaises(Exception) as context:
            refresh_pds_fhir_access_token(session)

        self.assertEqual(type(context.exception).__name__, 'PdsTokenException')

        update_session_mock.asset_not_called()
        requests_mock.post.assert_called_with(
            'https://fhir.example.org/auth/token',
            headers={
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            parameters={
                'client_id': 'CLIENT123',
                'client_secret': 'CLIENTSECRET123',
                'grant_type': 'refresh_token',
                'refresh_token': 'REFRESH123'
            }
        )
        log_mock.assert_called_with(expected_log_call)

    def test_refresh_pds_fhir_access_token_missing_refresh_token(
        self,
        requests_mock,
        log_mock
    ):
        expected_log_call = {'log_reference': CommonLogReference.PDSFHIR0002}

        with self.assertRaises(Exception) as context:
            refresh_pds_fhir_access_token({})

        self.assertEqual(type(context.exception).__name__, 'PdsTokenException')
        log_mock.assert_called_with(expected_log_call)

    @patch(f'{MODULE_NAME}.pds_fhir_access_token_valid')
    def test_get_pds_fhir_access_token(
        self,
        mock_pds_fhir_access_token_valid,
        requests_mock,
        log_mock
    ):
        mock_pds_fhir_access_token_valid.return_value = True
        actual = get_pds_fhir_access_token({'pds_access_token': 'TOKEN123'})
        self.assertEqual(actual, 'TOKEN123')

    @patch(f'{MODULE_NAME}.pds_fhir_access_token_valid')
    @patch(f'{MODULE_NAME}.refresh_pds_fhir_access_token')
    def test_get_pds_fhir_access_token_invalid(
        self,
        mock_refresh_pds_fhir_access_token,
        mock_pds_fhir_access_token_valid,
        requests_mock,
        log_mock
    ):
        mock_pds_fhir_access_token_valid.return_value = False
        mock_refresh_pds_fhir_access_token.return_value = 'NEWTOKEN123'

        actual = get_pds_fhir_access_token({})

        self.assertEqual(actual, 'NEWTOKEN123')
        log_mock.assert_called_with({'log_reference': CommonLogReference.PDSFHIR0001})

    @patch(f'{MODULE_NAME}.boto3')
    @patch(f'{MODULE_NAME}.time')
    def test_search_pds_fhir_by_nhs_number(
        self,
        time_mock,
        boto3_mock,
        requests_mock,
        log_mock
    ):
        session = {
            'pds_access_token': 'TOKEN123',
            'selected_role': {
                'role_id': 'ROLE123'
            }
        }

        boto3_mock.client('kms').encrypt.return_value = {
            'CiphertextBlob': base64.b64encode('ENCRYPTED_NHS_NUMBER'.encode('utf-8')),
            'KeyId': 'some_key',
            'EncryptionAlgorithm': 'SYMMETRIC_DEFAULT'
        }

        time_mock.return_value = 1631179645

        example_patient = {
            'resourceType': 'Patient',
            'id': '9000000009',
            'meta': {
                'security': []
            },
            'gender': 'unknown',
            'name': [
                {
                    'use': 'usual',
                    'given': ['Tessa'],
                    'family': 'Person',
                    'prefix': ['Ms']
                }
            ],
            'address': [],
            'generalPractitioner': []
        }

        expected_patient = [
            {
                'participant_id': f'pds-{EXAMPLE_UUID}',
                'nhs_number': '9000000009',
                'gender': 0,
                'is_pds': True,
                'first_name': 'Tessa',
                'title': 'Ms',
                'last_name': 'Person',
                'pds_signature': 'UlU1RFVsbFFWRVZFWDA1SVUxOU9WVTFDUlZJPQ==',
                'pds_timestamp': '1631179645'
            }
        ]

        requests_mock.get.return_value = MockRequestsResponse(example_patient, 200)

        actual = search_pds_fhir_by_nhs_number('9000000009', session)

        self.assertEqual(actual, expected_patient)

        requests_mock.get.assert_called_with(
            'https://fhir.example.org/api/Patient/9000000009',
            headers={
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer TOKEN123',
                'X-Request-ID': EXAMPLE_UUID
            }
        )

        log_mock.assert_has_calls([
            call({
                'log_reference': CommonLogReference.PDSFHIR0008,
                'X-Request-ID': EXAMPLE_UUID
            }),
            call({'log_reference': CommonLogReference.PDSFHIR0013}),
            call({'log_reference': CommonLogReference.PDSFHIR0011}),
            call({'log_reference': CommonLogReference.PDSFHIR0012})
        ])

    @patch(f'{MODULE_NAME}.boto3')
    @patch(f'{MODULE_NAME}.time')
    def test_search_pds_fhir_by_demographics(
        self,
        time_mock,
        boto3_mock,
        requests_mock,
        log_mock,
    ):
        session = {
            'pds_access_token': 'TOKEN123',
            'selected_role': {
                'role_id': 'ROLE123'
            }
        }

        boto3_mock.client('kms').encrypt.return_value = {
            'CiphertextBlob': base64.b64encode('ENCRYPTED_NHS_NUMBER'.encode('utf-8')),
            'KeyId': 'some_key',
            'EncryptionAlgorithm': 'SYMMETRIC_DEFAULT'
        }

        time_mock.return_value = 1631179645

        example_patient = {
            "fullUrl": "",
            "search": {
                "score": 0.8343
            },
            "resource": {
                'resourceType': 'Patient',
                'id': '9000000009',
                'meta': {
                    'security': []
                },
                'gender': 'unknown',
                'name': [
                    {
                        'use': 'usual',
                        'given': ['Tessa'],
                        'family': 'Person',
                        'prefix': ['Ms']
                    }
                ],
                'address': [],
                'generalPractitioner': []
            }
        }

        expected_patient = [
            {
                'participant_id': f'pds-{EXAMPLE_UUID}',
                'nhs_number': '9000000009',
                'gender': 0,
                'is_pds': True,
                'first_name': 'Tessa',
                'title': 'Ms',
                'last_name': 'Person',
                'pds_signature': 'UlU1RFVsbFFWRVZFWDA1SVUxOU9WVTFDUlZJPQ==',
                'pds_timestamp': '1631179645'
            }
        ]

        requests_mock.get.return_value = MockRequestsResponse({'total': 1, 'entry': [example_patient]}, 200)

        demographics = {
            'first_name': 'J', 'last_name': 'Brown', 'date_of_birth': '1990-01-01',
            'address': {'postcode': 'DE22 2DD'}
        }
        actual = search_pds_fhir_by_demographics(demographics, session)

        self.assertEqual(actual, expected_patient)

        requests_mock.get.assert_called_with(
            'https://fhir.example.org/api/Patient',
            headers={
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer TOKEN123',
                'X-Request-ID': EXAMPLE_UUID
            },
            params=[
                ('_exact-match', 'false'), ('given', 'J*'), ('family', 'Brown*'), ('birthdate', 'eq1990-01-01'),
                ('address-postcode', 'DE22 2DD')
            ]
        )

        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.PDSFHIR0013}),
            call({'log_reference': CommonLogReference.PDSFHIR0011}),
            call({'log_reference': CommonLogReference.PDSFHIR0012})
        ])

    @patch(f'{MODULE_NAME}.uuid4')
    def test_search_pds_fhir_by_nhs_number_not_found(
        self,
        uuid4_mock,
        requests_mock,
        log_mock
    ):
        session = {
            'pds_access_token': 'TOKEN123',
            'selected_role': {
                'role_id': 'ROLE123'
            }
        }

        example_error = {
            'error': 'INVALIDATED_RESOURCE'
        }

        example_uuid = 'df6fb4bd-2819-4c32-a23e-cb5edc10a04c'

        requests_mock.get.return_value = MockRequestsResponse(example_error, 404, HTTPError())
        uuid4_mock.return_value = example_uuid

        actual = search_pds_fhir_by_nhs_number('9000000009', session)

        self.assertEqual(actual, [])

        requests_mock.get.assert_called_with(
            'https://fhir.example.org/api/Patient/9000000009',
            headers={
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer TOKEN123',
                'X-Request-ID': example_uuid
            }
        )

        log_mock.assert_called_with({
            'log_reference': CommonLogReference.PDSFHIR0007,
            'status_code': 404,
            'error': {
                'error': 'INVALIDATED_RESOURCE'
            },
            'X-Request-ID': example_uuid
        })

    @patch(f'{MODULE_NAME}.uuid4')
    def test_search_pds_fhir_by_nhs_number_error(
        self,
        uuid4_mock,
        requests_mock,
        log_mock
    ):
        session = {
            'pds_access_token': 'TOKEN123',
            'selected_role': {
                'role_id': 'ROLE123'
            }
        }

        example_uuid = 'df6fb4bd-2819-4c32-a23e-cb5edc10a04c'

        requests_mock.get.return_value = MockRequestsResponse({}, 404, ConnectionError('Server gone AWOL'))
        uuid4_mock.return_value = example_uuid

        actual = search_pds_fhir_by_nhs_number('9000000009', session)

        self.assertEqual(actual, [])

        requests_mock.get.assert_called_with(
            'https://fhir.example.org/api/Patient/9000000009',
            headers={
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer TOKEN123',
                'X-Request-ID': example_uuid
            }
        )

        log_mock.assert_called_with({
            'log_reference': CommonLogReference.PDSFHIR0007,
            'error': 'Server gone AWOL',
            'X-Request-ID': example_uuid
        })

    @unpack
    @data(
        (
            'pds_fhir_patient_details.json',
            'pds_fhir_patient_details.json',
            'Y12345',
            []
        ),
        (
            'pds_fhir_patient_restricted.json',
            'pds_fhir_patient_restricted.json',
            None,
            [
                call({
                    'log_reference': CommonLogReference.PDSFHIR0010,
                    'security_level': 'restricted'
                })
            ]
        ),
        (
            'pds_fhir_patient_details_partial.json',
            'pds_fhir_patient_details_partial.json',
            None,
            [
                call({'log_reference': CommonLogReference.PDSFHIR0013}),
                call({'log_reference': CommonLogReference.PDSFHIR0011}),
                call({'log_reference': CommonLogReference.PDSFHIR0012})
            ]
        ),
        (
            'pds_fhir_patient_empty.json',
            'pds_fhir_patient_empty.json',
            None,
            [
                call({'log_reference': CommonLogReference.PDSFHIR0009})
            ]
        ),
        (
            'pds_fhir_no_postcode.json',
            'pds_fhir_no_postcode.json',
            'D82021',
            []
        ),
    )
    @patch(f'{MODULE_NAME}.get_organisation_information_by_organisation_code')
    @patch(f'{MODULE_NAME}.boto3')
    @patch(f'{MODULE_NAME}.time')
    def test_map_pds_fhir_patient(
        self,
        input_file_name,
        output_file_name,
        expected_org_code_call,
        log_calls_made,
        time_mock,
        boto3_mock,
        org_info_mock,
        requests_mock,
        log_mock
    ):

        self.maxDiff = None

        boto3_mock.client('kms').encrypt.return_value = {
            'CiphertextBlob': base64.b64encode('ENCRYPTED_NHS_NUMBER'.encode('utf-8')),
            'KeyId': 'some_key',
            'EncryptionAlgorithm': 'SYMMETRIC_DEFAULT'
        }

        time_mock.return_value = 1631179645

        org_info_mock.return_value = {
            'address': {
                'address_line_1': 'BROADGATE LANE',
                'address_line_2': 'HORSFORTH',
                'address_line_3': 'LEEDS',
                'address_line_4': 'WEST YORKSHIRE',
                'address_line_5': 'ENGLAND',
                'postcode': 'LS18 4SE'
            },
            'name': 'LLOYDSPHARMACY',
        }

        input_file_path = os.path.join(
            os.path.dirname(__file__),
            'test_data',
            'fixtures',
            input_file_name
        )

        output_file_path = os.path.join(
            os.path.dirname(__file__),
            'test_data',
            'expected',
            output_file_name
        )

        with open(input_file_path) as input_file, open(output_file_path) as output_file:
            test_data = json.load(input_file)
            expected_mapped = json.load(output_file)

            actual_mapped = map_pds_fhir_patient(test_data)

            self.assertDictEqual(expected_mapped, actual_mapped)

            if expected_org_code_call:
                org_info_mock.assert_called_with(expected_org_code_call)
            else:
                org_info_mock.assert_not_called()

            if log_calls_made:
                log_mock.assert_has_calls(log_calls_made)
            else:
                log_mock.assert_not_called()

    def test_map_pds_fhir_patient_name(
        self,
        request_mock,
        log_mock
    ):
        example_patient = {
            'name': [
                {
                    'use': 'nickname',
                    'given': [
                        'Big',
                        'T',
                        'P'
                    ],
                    'prefix': [
                        'DJ'
                    ],
                    'family': 'Person'
                },
                {
                    'use': 'usual',
                    'given': [
                        'Tessa',
                        'Penny',
                        'Jennifer'
                    ],
                    'prefix': [
                        'Ms'
                    ],
                    'family': 'Person'
                }
            ]
        }

        expected_name = {
            'first_name': 'Tessa',
            'middle_names': 'Penny Jennifer',
            'last_name': 'Person',
            'title': 'Ms'
        }

        actual_name = _map_pds_fhir_patient_name(example_patient)
        self.assertDictEqual(actual_name, expected_name)

    @patch(f'{MODULE_NAME}.get_organisation_information_by_organisation_code')
    def test_map_pds_fhir_gp_failure(
        self,
        org_info_mock,
        request_mock,
        log_mock
    ):
        org_info_mock.return_value = {
            'message': 'Failed to find organisation'
        }

        participant = {
            'generalPractitioner': [
                {
                    'identifier': {
                        'value': 'ABC123'
                    }
                }
            ]
        }

        actual = _map_pds_fhir_gp(participant)

        self.assertEqual(actual, {})
        org_info_mock.assert_called_with('ABC123')
        log_mock.assert_called_with({
            'log_reference': CommonLogReference.PDSFHIR0013,
            'practice_code': 'ABC123'
        })

    @patch(f'{MODULE_NAME}.get_secret')
    def test_get_pds_fhir_initial_access_tokens(
        self,
        get_secret_mock,
        request_mock,
        log_mock
    ):
        get_secret_mock.return_value = '{"client_id": "CLIENTID", "client_secret": "CLIENTSECRET"}'
        request_mock.post.return_value = MockRequestsResponse({'status': 'ok'})

        expected_response = {'status': 'ok'}

        actual_response = get_pds_fhir_initial_access_tokens('AUTH123')
        self.assertDictEqual(actual_response, expected_response)
        get_secret_mock.assert_called_with('SECRETID', secret_name_is_key=False)
        log_mock.assert_called_with({'log_reference': CommonLogReference.PDSFHIR0014})

    @unpack
    @data(
        (
            HTTPError(),
            {
                'log_reference': CommonLogReference.PDSFHIR0015,
                'status_code': 403,
                'error': {
                    'error': 'access_denied'
                }
            }
        ),
        (
            ConnectionError('Never heard of that server'),
            {
                'log_reference': CommonLogReference.PDSFHIR0015,
                'error': 'Never heard of that server'
            }
        ),
    )
    @patch(f'{MODULE_NAME}.get_secret')
    def test_get_pds_fhir_initial_access_tokens_with_error(
        self,
        exception_to_throw,
        expected_log_call,
        get_secret_mock,
        request_mock,
        log_mock
    ):
        get_secret_mock.return_value = '{"client_id": "CLIENTID", "client_secret": "CLIENTSECRET"}'
        request_mock.post.return_value = MockRequestsResponse(
            {'error': 'access_denied'},
            403,
            exception_to_throw
        )

        with self.assertRaises(Exception) as context:
            get_pds_fhir_initial_access_tokens('AUTH123')

        self.assertEqual(type(context.exception).__name__, 'PdsTokenException')

        get_secret_mock.assert_called_with('SECRETID', secret_name_is_key=False)
        log_mock.assert_called_with(expected_log_call)

    @patch(f'{MODULE_NAME}.create_set_cookie_header')
    def test_perform_pds_fhir_redirect_to_search(
        self,
        create_set_cookie_header_mock,
        request_mock,
        log_mock
    ):
        create_set_cookie_header_mock.return_value = 'COOKIES'
        expected_response = {
            'statusCode': 302,
            'headers': {
                'Location': 'https://environment.example.org/#/results?search_type=nhs_number&nhs_number=1234567890',
                'Set-Cookie': 'COOKIES'
            }
        }

        actual_response = perform_pds_fhir_redirect_to_search(
            {
                'search_type': 'nhs_number',
                'nhs_number': '1234567890'
            },
            'TOKEN123',
            True
        )

        self.assertDictEqual(actual_response, expected_response)
        create_set_cookie_header_mock.assert_called_with('TOKEN123', use_strict=False)

    @unpack
    @data(
        (['ENG'], {'practice_address': {'address_line_5': 'England'}}, [], True),
        (['SCT'], {'practice_address': {'address_line_5': 'Scotland'}}, [], True),
        (['IM'], {'practice_address': {'address_line_5': 'Isle of Man'}}, [], True),
        (['WGA'], {'practice_address': {'address_line_5': 'Wales'}}, [], True),
        (['ENG'], {'practice_address': {'address_line_5': 'Wales'}}, [], False),
        (
            ['ENG'],
            {'practice_address': {'address_line_5': 'Other'}},
            [call({'log_reference': CommonLogReference.PDSFHIR0017, 'registered_gp_country': 'Other'})],
            True
        ),
        (
            ['ZZ'],
            {'practice_address': {'address_line_5': 'England'}},
            [call({'log_reference': CommonLogReference.PDSFHIR0018})],
            False
        ),
    )
    @patch(f'{MODULE_NAME}.get_current_workgroups')
    def test_can_workgroup_access_participant(
        self, workgroups, gp_details, expected_log_calls, expected_result, get_workgroup_mock, request_mock, log_mock
    ):
        get_workgroup_mock.return_value = workgroups
        actual_result = can_workgroup_access_participant(gp_details)
        self.assertEqual(actual_result, expected_result)
        if expected_log_calls:
            log_mock.assert_has_calls(expected_log_calls)
        else:
            log_mock.assert_not_called()
