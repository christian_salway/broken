from mock import call
from common.log_references import CommonLogReference

nhs_number_single_participant = (
    'nhs_number_single_participant.json',
    'test_result_for_match.json',
    [call({
        'log_reference': CommonLogReference.PARTICIPANT0002,
        'score': 5
    })]
)

nhs_number_single_low_score_participant = (
    'nhs_number_single_low_score_participant.json',
    'test_result_for_match.json',
    [
        call({'log_reference': CommonLogReference.PARTICIPANT0006}),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0009,
                'participant_id': 'single_participant',
                'nhs_number': 1.0,
                'date_of_birth': 0.9,
                'postcode': 0.8,
                'first_name': 1.0,
                'last_name': 1.0
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0010,
                'participant_id': 'single_participant',
                'sensitive_data': True,
                'date_of_birth': [
                    'INSERT: Result:  --> Participant: 1',
                    'DELETE: Result: 1 --> Participant: '
                ],
                'postcode_diff': [
                    'REPLACE: Result: 6 --> Participant: 5'
                ]
            }
        )
    ]
)

nhs_number_high_score_participant_with_no_matching_address = (
    'nhs_number_high_score_participant_with_no_matching_address.json',
    'test_result_for_match.json',
    [call({'log_reference': CommonLogReference.PARTICIPANT0003})]
)

nhs_number_multiple_participants = (
    'nhs_number_multiple_participants.json',
    'test_result_for_match.json',
    [
        call({'log_reference': CommonLogReference.PARTICIPANT0004}),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0009,
                'participant_id': 'expected_1',
                'nhs_number': 1.0,
                'date_of_birth': 1.0,
                'postcode': 1.0,
                'first_name': 1.0,
                'last_name': 1.0
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0009,
                'participant_id': 'expected_2',
                'nhs_number': 1.0,
                'date_of_birth': 1.0,
                'postcode': 1.0,
                'first_name': 0.0,
                'last_name': 1.0
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0010,
                'participant_id': 'expected_2',
                'sensitive_data': True,
                'first_name': ['REPLACE: Result: JOHN --> Participant: BARRY']
            }
        )
    ]
)

dob_high_score_participant = (
    'dob_high_score_participant.json',
    'test_result_for_match.json',
    [
        call({'log_reference': CommonLogReference.PARTICIPANT0008}),
        call({
            'log_reference': CommonLogReference.PARTICIPANT0002,
            'score': 4
        })
    ]
)

dob_high_score_participants = (
    'dob_high_score_participants.json',
    'test_result_for_match.json',
    [
        call({'log_reference': CommonLogReference.PARTICIPANT0008}),
        call({'log_reference': CommonLogReference.PARTICIPANT0005}),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0009,
                'participant_id': 'expected_1',
                'nhs_number': 0.9,
                'date_of_birth': 1.0,
                'postcode': 1.0,
                'first_name': 1.0,
                'last_name': 1.0
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0010,
                'participant_id': 'expected_1',
                'sensitive_data': True,
                'nhs_number': ['REPLACE: Result: 1 --> Participant: 2']
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0009,
                'participant_id': 'expected_2',
                'nhs_number': 0.9,
                'date_of_birth': 1.0,
                'postcode': 1.0,
                'first_name': 1.0,
                'last_name': 1.0
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0010,
                'participant_id': 'expected_2',
                'sensitive_data': True,
                'nhs_number': ['REPLACE: Result: 1 --> Participant: 2']
            }
        )
    ]
)

dob_medium_score_participants = (
    'dob_medium_score_participants.json',
    'test_result_for_match.json',
    [
        call({'log_reference': CommonLogReference.PARTICIPANT0008}),
        call({'log_reference': CommonLogReference.PARTICIPANT0006}),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0009,
                'participant_id': 'expected_1',
                'nhs_number': 0.9,
                'date_of_birth': 1.0,
                'postcode': 1.0,
                'first_name': 0.0,
                'last_name': 1.0
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0010,
                'participant_id': 'expected_1',
                'sensitive_data': True,
                'nhs_number': ['REPLACE: Result: 1 --> Participant: 0'],
                'first_name': ['REPLACE: Result: JOHN --> Participant: BARRY']
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0009,
                'participant_id': 'expected_2',
                'nhs_number': 0.8,
                'date_of_birth': 1.0,
                'postcode': 1.0,
                'first_name': 0.0,
                'last_name': 1.0
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0010,
                'participant_id': 'expected_2',
                'sensitive_data': True,
                'nhs_number': ['REPLACE: Result: 11 --> Participant: 23'],
                'first_name': ['REPLACE: Result: JOHN --> Participant: BARRY']
            }
        )
    ]
)

no_matching_participants = (
    'no_matching_participants.json',
    'test_result_for_match.json',
    [
        call({'log_reference': CommonLogReference.PARTICIPANT0008}),
        call({'log_reference': CommonLogReference.PARTICIPANT0007})
    ]
)

nhs_number_single_participant_with_episode_addresses = (
    'nhs_number_single_participant_with_episode_addresses.json',
    'test_result_for_match.json',
    [
        call({'log_reference': CommonLogReference.PARTICIPANT0006}),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0009,
                'participant_id': 'single_participant',
                'nhs_number': 1.0,
                'date_of_birth': 0.9,
                'postcode': 0.8,
                'first_name': 1.0,
                'last_name': 1.0
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0010,
                'participant_id': 'single_participant',
                'sensitive_data': True,
                'date_of_birth': [
                    'INSERT: Result:  --> Participant: 1',
                    'DELETE: Result: 1 --> Participant: '
                ],
                'postcode_diff': [
                    'REPLACE: Result: 6 --> Participant: 5'
                ]
            }
        )
    ]
)

nhs_number_single_participant_with_episode_addresses_with_matching_postcode = (
    'nhs_number_single_participant_with_episode_addresses_with_matching_postcode.json',
    'test_result_for_episode_match.json',
    [
        call({'log_reference': CommonLogReference.PARTICIPANT0006}),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0009,
                'participant_id': 'single_participant',
                'nhs_number': 0.9,
                'date_of_birth': 0.9,
                'postcode': 1.0,
                'first_name': 1.0,
                'last_name': 1.0
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0010,
                'participant_id': 'single_participant',
                'sensitive_data': True,
                'nhs_number': [
                    'REPLACE: Result: 1 --> Participant: 2'
                ],
                'date_of_birth': [
                    'INSERT: Result:  --> Participant: 1',
                    'DELETE: Result: 1 --> Participant: '
                ]
            }
        )
    ]
)

nhs_number_missing_participants = (
    'nhs_number_missing_for_participant.json',
    'test_result_for_missing_nhs_number.json',
    [
        call({'log_reference': CommonLogReference.PARTICIPANT0008}),
        call({'log_reference': CommonLogReference.PARTICIPANT0006}),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0009,
                'participant_id': 'expected_1',
                'nhs_number': 0.0,
                'date_of_birth': 1.0,
                'postcode': 1.0,
                'first_name': 0.0,
                'last_name': 1.0
            }
        ),
        call(
            {
                'log_reference': CommonLogReference.PARTICIPANT0010,
                'participant_id': 'expected_1',
                'sensitive_data': True,
                'nhs_number': ['INSERT: Result:  --> Participant: 9876543210'],
                'first_name': ['REPLACE: Result: JOHN --> Participant: BARRY']
            }
        )
    ]
)

nhs_number_dob_last_name_missing_participants = (
    'nhs_number_dob_last_name_missing_for_participant.json',
    'test_result_for_missing_nhs_number_dob_last_name.json',
    [
        call({'log_reference': CommonLogReference.PARTICIPANT0007})
    ]
)
