NHS_NUMBER = 'NHS_NUMBER'
SURNAME = 'SURNAME'
FORENAME_1 = 'FORENAME'
SENDER_CODE = 'DIMLGH'

# Example lab results which would be extracted from EDIFACT Data
TYPE_3_LAB_RESULT_1 = (
    "PAD+NHS_NUMBER:801+++SURNAME++:FORENAME++++800:DDDDDDDD:911'"
    "NHS+7:838+1:814:202'"
    "NHS+DIMLGH:839'"
    "DTM+832:20191021:102'"
    "RAR+2:843+S:864+834:00:912'"
    "PCD+840:19035247'"
    "NAD+HP++AAAAAAAAA A AAAA AAAAA AAAA"
)

INVALID_TYPE_3_LAB_RESULT = (
    "PAD+NHS_NUMBER:801+++SURNAME++:FORENAME++++800:DDDDDDDD:911"
    "+++:+++:+++'"
)

TYPE_3_LAB_RESULT_2 = (
    "PAD+NHS_NUMBER:801+++SURNAME++:FORENAME++++800:DDDDDDDD:911'"
    "NHS+7:838+1:814:202'"
    "NHS+DIMLGH:839'"
    "DTM+832:20191021:102'"
    "RAR+2:843+S:864+834:00:912'"
    "PCD+840:19035247'"
    "NAD+HP++AAAAAAAAA A AAAA AAAAA AAAAAA"
)

TYPE_3_LAB_RESULT_3 = (
    "PAD+NHS_NUMBER:801+++SSSSSS++:FORENAMEFFFF++++800:DDDDDDDD:911'"
    "NHS+7:838+1:814:202'"
    "NHS+DIMLGH:839'"
    "DTM+832:20191021:102'"
    "RAR+2:843+S:864+834:00:912'"
    "PCD+840:19035248'"
    "NAD+HP++AA AAA AAA"
)

TYPE_3_LAB_RESULT_4 = (
    "PAD+NHS_NUMBER:801+++SURNAME??++:FORE?'NAME++++800:DDDDDDDD:911'"
    "NHS+7:838+1:814:202'"
    "NHS+DIMLGH:839'"
    "DTM+832:20191021:102'"
    "RAR+2:843+S:864+834:00:912'"
    "PCD+840:19035247'"
    "NAD+HP++AAAAAAAAA A AAAA AAAAA AAAA"
)


# EDIFACT metadata and lab info
TYPE_3_EDIFACT_HEADER_RAW = (
    "UNB+UNOA:2+WHI3+WIG1+191025:1144+00002155++CYTFH++1'UNH+1+CYTFH:0:2:FH'BGM++003+"
    "243:201910251144:306+62'RFF+CSF:131'DTM+829:20191021:102'DTM+830:20191021:102'DT"
    "M+831:20191025:102'NHS+62010:837"
)

INVALID_EDIFACT_HEADER = (
    "UNB+UNOA:2+WHI3+WIG1+191025:1144+00002155++CATFD++1'UNH+1+CATFD:0:2:FH'BGM++003+"
    "243:201910251144:306+62'RFF+CSF:131'DTM+829:20191021:102'DTM+830:20191021:102'DT"
    "M+831:20191025:102'NHS+62010:837"
)

TYPE_3_EDIFACT_FOOTER_RAW = (
    "UNT+22+1'UNZ+1+00002155'"
)

EDIFACT_UNA_HEADER_RAW = "UNA:+.? '"

TYPE_9_LAB_RESULT_1 = (
    "S01+1'"
    "NAD+SRC+000001:ZZZ'"
    "NAD+SND+710632:ZZZ'"
    "S02+2'"
    "PNA+PAT+NNNNNNNNNN:OPI+++SU:SSSSSSSS+FS:FFFFFFF'"
    "DTM+329:DDDDDDDD:951'"
    "NAD+PAT++TH'"
    "E AAAA AAAAAA AAAAAAA PPPP P:RN'"
    "S03+3'"
    "TST+RCD+X:ZZZ+SNO:19092991+A:ZZZ'"
    "DTM+119:2'"
    "0191021:102'"
    "HEA+INM+0:ZZZ'"
    "HEA+ETP+Y:ZZZ'"
    "QTY+961:36'"
)

TYPE_9_LAB_RESULT_2 = (
    "S01+1'"
    "NAD+SRC+000001:ZZZ'"
    "NAD+'"
    "SND+714522:ZZZ'"
    "S02+2'"
    "PNA+PAT+NNNNNNNNNN:OPI+++SU:SSSSSS+FS:FFFFF'"
    "DTM+329:DDDDD'"
    "DDD:951'"
    "NAD+PAT++AAAA 2, AAAAAAAA AAAA AA:AAAAA AAAAAA AAAAA LOOE PPPP P'"
    "S03+3'"
    "TST+RCD+X:ZZZ+SNO:19093000+A:ZZZ'"
    "DTM+119:20191022:102'"
    "HEA+INM+0:ZZZ'"
    "HEA+ETP'"
    "+Y:ZZZ'"
    "QTY+961:36'"
)

TYPE_9_LAB_RESULT_3 = (
    "S01+1'"
    "NAD+SRC+000001:ZZZ'"
    "NAD+SND+714522:ZZZ'"
    "S02+2'"
    "PNA+PAT+NNNNNNNNNN:OPI+++SU:SSSSSSSSSS+FS:FFFFFF FFFF'"
    "DTM+329:22121994:951'"
    "NAD+PAT++AA A'"
    "AAA AAAAAAAAA AAAA:AAA PPPP PPP'"
    "S03+3'"
    "TST+RCD+X:ZZZ+SNO:19093054+A:ZZZ'"
    "DTM+119:20191018:102'"
    "HEA+INM+0:ZZZ'"
    "HEA+ETP+Y:ZZZ'"
    "QTY+961:36'"
)

TYPE_9_LAB_RESULT_4 = (
    "S01+1'"
    "NAD+SRC+000001:ZZZ'"
    "NAD+SND+710522:ZZZ'"
    "S02+2'"
    "PNA+PAT+NNNNNNNNNN:OPI+++SU:SSSSSSS+FS:FFFFFF'"
    "DTM+329:DDDDDDDD:951'"
    "NAD+PAT++AAAA AAAAAAA AAAAAAAAA PPPP:PPP'"
    "S03+3'"
    "TST+RCD+2:ZZZ+SN'"
    "O:19093282+R:ZZZ'"
    "DTM+119:20191021:102'"
    "HEA+INM+9:ZZZ'"
    "HEA+ETP+Y:ZZZ'"
    "QTY+961:12'"
)

TYPE_9_LAB_RESULT_5 = (
    "S01+1'"
    "NAD+SRC+000001:ZZZ'"
    "NAD+SND+711296:ZZZ'"
    "S02+2'"
    "PNA+PAT+NNNNNNNNNN:OPI+++SU:SSSSS+FS:FFFFFFF'"
    "DTM+329:16101987:951'"
    "NAD+PAT++AA AAAA AAAA AAAAAAAA PPPP PPP'"
    "S03+3'"
    "TST+RCD+8:ZZZ+SNO:19091776+S:ZZZ'"
    "DTM+119:20191017:102'"
    "HEA+INM+9:ZZZ'"
    "HEA+ETP+Y:ZZZ'"
)

TYPE_9_HEADER_RAW = (
    "UNB+UNOA:2+NBTYPE_3+CR01+191029:1113+00000114++FHSPRV'UNH+1+FHSPRV:0:2:FH:FHS003'BGM"
    "+++9'NAD+FHS+5QP:954'NAD+PTH+1:963'DTM+137:201910291113:203'DTM+194:191017:101'D"
    "TM+206:191022:101'RFF+TN:01197"
)

TYPE_9_FOOTER_RAW = "UNT+73+1'UNZ+1+00000114"

INVALID_TYPE_3_WITH_INVALID_RESULT = (
    f"{INVALID_EDIFACT_HEADER}'"
    f"{INVALID_TYPE_3_LAB_RESULT}"
)

# Full examples of raw EDIFACT messages containing lab results
TYPE_3_WITH_1_RESULT_MESSAGE = (
    f"{TYPE_3_EDIFACT_HEADER_RAW}'"
    f"{TYPE_3_LAB_RESULT_3}'"
    f"{TYPE_3_EDIFACT_FOOTER_RAW}"
)

TYPE_3_WITH_2_RESULTS_MESSAGE = (
    f"{EDIFACT_UNA_HEADER_RAW}"
    f"{TYPE_3_EDIFACT_HEADER_RAW}'"
    f"{TYPE_3_LAB_RESULT_1}'"
    f"{TYPE_3_LAB_RESULT_2}'"
    f"{TYPE_3_EDIFACT_FOOTER_RAW}"
)

TYPE_3_WITH_3_RESULTS_MESSAGE = (
    f"{TYPE_3_EDIFACT_HEADER_RAW}'"
    f"{TYPE_3_LAB_RESULT_2}'"
    f"{TYPE_3_LAB_RESULT_1}'"
    f"{TYPE_3_LAB_RESULT_3}'"
    f"{TYPE_3_EDIFACT_FOOTER_RAW}"
)

TYPE_3_WITH_1_RESULT_CONTAINING_SPECIAL_CHARS_MESSAGE = (
    f"{EDIFACT_UNA_HEADER_RAW}"
    f"{TYPE_3_EDIFACT_HEADER_RAW}'"
    f"{TYPE_3_LAB_RESULT_4}'"
    f"{TYPE_3_EDIFACT_FOOTER_RAW}"
)

# Decomposed EDIFACT message split into individual test results
TYPE_3_WITH_1_RESULT_SPLIT = [TYPE_3_EDIFACT_HEADER_RAW, TYPE_3_LAB_RESULT_3]
TYPE_3_WITH_2_RESULTS_SPLIT = [EDIFACT_UNA_HEADER_RAW + TYPE_3_EDIFACT_HEADER_RAW,
                               TYPE_3_LAB_RESULT_1,
                               TYPE_3_LAB_RESULT_2]
TYPE_3_WITH_3_RESULTS_SPLIT = [TYPE_3_EDIFACT_HEADER_RAW, TYPE_3_LAB_RESULT_2, TYPE_3_LAB_RESULT_1, TYPE_3_LAB_RESULT_3]

# Representation of EDIFACT message split into hierarchy of segments, elements and components
TYPE_3_WITH_2_RESULTS_MESSAGE_DECODED = [
    [['UNA', ''], '. '],
    ['UNB', ['UNOA', '2'], 'WHI3', 'WIG1', ['191025', '1144'], '00002155', '', 'CYTFH', '', '1'],
    ['UNH', '1', ['CYTFH', '0', '2', 'FH']],
    ['BGM', '', '003', ['243', '201910251144', '306'], '62'],
    ['RFF', ['CSF', '131']],
    ['DTM', ['829', '20191021', '102']],
    ['DTM', ['830', '20191021', '102']],
    ['DTM', ['831', '20191025', '102']],
    ['NHS', ['62010', '837']],
    ['PAD', ['NHS_NUMBER', '801'], '', '', 'SURNAME', '', ['', 'FORENAME'], '', '', '', ['800', 'DDDDDDDD', '911']],
    ['NHS', ['7', '838'], ['1', '814', '202']],
    ['NHS', ['DIMLGH', '839']],
    ['DTM', ['832', '20191021', '102']],
    ['RAR', ['2', '843'], ['S', '864'], ['834', '00', '912']],
    ['PCD', ['840', '19035247']],
    ['NAD', 'HP', '', 'AAAAAAAAA A AAAA AAAAA AAAA'],
    ['PAD', ['NHS_NUMBER', '801'], '', '', 'SURNAME', '', ['', 'FORENAME'], '', '', '', ['800', 'DDDDDDDD', '911']],
    ['NHS', ['7', '838'], ['1', '814', '202']],
    ['NHS', ['DIMLGH', '839']],
    ['DTM', ['832', '20191021', '102']],
    ['RAR', ['2', '843'], ['S', '864'], ['834', '00', '912']],
    ['PCD', ['840', '19035247']],
    ['NAD', 'HP', '', 'AAAAAAAAA A AAAA AAAAA AAAAAA'],
    ['UNT', '22', '1'],
    ['UNZ', '1', '00002155'],
]

TYPE_3_WITH_1_RESULT_CONTAINING_SPECIAL_CHARS_MESSAGE_DECODED = [
    [['UNA', ''], '. '],
    ['UNB', ['UNOA', '2'], 'WHI3', 'WIG1', ['191025', '1144'], '00002155', '', 'CYTFH', '', '1'],
    ['UNH', '1', ['CYTFH', '0', '2', 'FH']],
    ['BGM', '', '003', ['243', '201910251144', '306'], '62'],
    ['RFF', ['CSF', '131']],
    ['DTM', ['829', '20191021', '102']],
    ['DTM', ['830', '20191021', '102']],
    ['DTM', ['831', '20191025', '102']],
    ['NHS', ['62010', '837']],
    ['PAD', ['NHS_NUMBER', '801'], '', '', 'SURNAME?', '', ['', 'FORE\'NAME'], '', '', '', ['800', 'DDDDDDDD', '911']],
    ['NHS', ['7', '838'], ['1', '814', '202']],
    ['NHS', ['DIMLGH', '839']],
    ['DTM', ['832', '20191021', '102']],
    ['RAR', ['2', '843'], ['S', '864'], ['834', '00', '912']],
    ['PCD', ['840', '19035247']],
    ['NAD', 'HP', '', 'AAAAAAAAA A AAAA AAAAA AAAA'],
    ['UNT', '22', '1'],
    ['UNZ', '1', '00002155'],
]

INDIVIDUAL_TYPE_3_RESULT = [
    ['UNB', ['UNOA', '2'], 'WHI3', 'WIG1', ['191025', '1144'], '00002155', '', '', 'CYTFH', '', '', '1'],
    ['PAD', [NHS_NUMBER, '801'], '', '', SURNAME, '', ['', FORENAME_1], '', '', '', ['800', '12121982', '911']],
    ['NHS', ['7', '838'], ['1', '814', '202']],
    ['NHS', [SENDER_CODE, '839']],
    ['DTM', ['832', '20191021', '102']],
    ['RAR', ['2', '843'], ['S', '864'], ['834', '00', '912']],
    ['PCD', ['840', '19035247'], 'Y'],
    ['NAD', 'HP', '', 'AAAAAAAAA A AAAA AAAAA AAAA'],
]

DECODED_RESULT_TYPE_3 = {
    'action_code': 'S',
    'action': 'Suspended',
    'address': 'AAAAAAAAA A AAAA AAAAA AAAA',
    'date_of_birth_format': '%d%m%Y',
    'date_of_birth': '12121982',
    'first_name': FORENAME_1,
    'hpv_primary': True,
    'last_name': SURNAME,
    'nhs_number': NHS_NUMBER,
    'previous_last_name': '',
    'raw_nhais_cipher': 'WIG1',
    'recall_months': '00',
    'result_code': '2',
    'result': 'Negative',
    'self_sample': False,
    'sender_code': SENDER_CODE,
    'sending_lab': 'WHI3',
    'slide_number': '19035247',
    'sender_source_type': '7',
    'source_code': 'H',
    'test_date_format': '%Y%m%d',
    'test_date': '20191021',
    'title': ''
}

TYPE_9_WITH_1_RESULT_MESSAGE = (
    f"{TYPE_9_HEADER_RAW}'"
    f"{TYPE_9_LAB_RESULT_4}'"
    f"{TYPE_9_FOOTER_RAW}"
)

TYPE_9_WITH_2_RESULTS_MESSAGE = (
    f"{TYPE_9_HEADER_RAW}'"
    f"{TYPE_9_LAB_RESULT_3}'"
    f"{TYPE_9_LAB_RESULT_1}'"
    f"{TYPE_9_FOOTER_RAW}"
)

TYPE_9_WITH_3_RESULTS_MESSAGE = (
    f"{TYPE_9_HEADER_RAW}'"
    f"{TYPE_9_LAB_RESULT_5}'"
    f"{TYPE_9_LAB_RESULT_2}'"
    f"{TYPE_9_LAB_RESULT_4}'"
    f"{TYPE_9_FOOTER_RAW}"
)

# Decomposed EDIFACT message split into individual test results
TYPE_9_WITH_1_RESULT_SPLIT = [TYPE_9_HEADER_RAW, TYPE_9_LAB_RESULT_4]
TYPE_9_WITH_2_RESULTS_SPLIT = [TYPE_9_HEADER_RAW, TYPE_9_LAB_RESULT_3, TYPE_9_LAB_RESULT_1]
TYPE_9_WITH_3_RESULTS_SPLIT = [TYPE_9_HEADER_RAW, TYPE_9_LAB_RESULT_5, TYPE_9_LAB_RESULT_2, TYPE_9_LAB_RESULT_4]


DECODED_RESULT_TYPE_9 = {
    'action_code': 'A',
    'action': 'Routine',
    'address': 'AA AAAA AAAAAAAAA AAAAAAA PPPP PPP',
    'date_of_birth_format': '%d%m%Y',
    'date_of_birth': '22121994',
    'first_name': 'FFFFFF FFFF',
    'hpv_primary': True,
    'infection_code': '0',
    'infection_result': 'HPV negative',
    'last_name': 'SSSSSSSSSS',
    'nhs_number': 'NNNNNNNNNN',
    'raw_nhais_cipher': 'CR01',
    'recall_months': '36',
    'result_code': 'X',
    'result': 'No Cytology test undertaken',
    'self_sample': False,
    'sender_code': '714522',
    'sending_lab': 'NBT3',
    'slide_number': '19093054',
    'sender_source_type': '000001',
    'source_code': 'G',
    'test_date_format': '%Y%m%d',
    'test_date': '20191018'
}

INDIVIDUAL_TYPE_9_RESULT = [
    ['UNB', ['UNOA', '2'], 'NBT3', 'CR01', ['191029', '1113'], '00000114', '', '', 'FHSPRV'],
    ['S01', '1'],
    ['NAD', 'SRC', ['000001', 'ZZZ']],
    ['NAD', 'SND', ['714522', 'ZZZ']],
    ['S02', '2'],
    ['PNA', 'PAT', ['NNNNNNNNNN', 'OPI'], '', '', ['SU', 'SSSSSSSSSS'], ['FS', 'FFFFFF FFFF']],
    ['DTM', ['329', '22121994', '951']],
    ['NAD', 'PAT', '', ['AA AAAA AAAAAAAAA AAAA', 'AAA PPPP PPP']],
    ['S03', '3'],
    ['TST', 'RCD', ['X', 'ZZZ'], ['SNO', '19093054'], ['A', 'ZZZ']],
    ['DTM', ['119', '20191018', '102']],
    ['HEA', 'INM', ['0', 'ZZZ']],
    ['HEA', 'ETP', ['Y', 'ZZZ']],
    ['QTY', ['961', '36']],
]

TYPE_3_EDIFACT_INTERCHANGE_HEADER = "UNB+UNOA:2+WHI3+WIG1+191025:1144+00002155++CYTFH++1'"

TYPE_3_EDIFACT_MESSAGE_HEADER = ("UNH+1+CYTFH:0:2:FH'"
                                 "BGM++003+243:201910251144:306+62'"
                                 "RFF+CSF:131'"
                                 "DTM+829:20191021:102'"
                                 "DTM+830:20191021:102'"
                                 "DTM+831:20191025:102'"
                                 "NHS+62010:837"
                                 )

TYPE_3_EDIFACT_MESSAGE_TRAILER = "UNT+22+1'"

TYPE_3_EDIFACT_INTERCHANGE_TRAILER = "UNZ+1+00002155'"
