from unittest.case import TestCase
from mock import patch, Mock, MagicMock
from common.utils.s3_multipart_upload_utils import S3MultiPartUpload

BUCKET = 'Bucket'


class TestS3MultiPartUploadUtils(TestCase):

    @patch('common.utils.s3_multipart_upload_utils.client', Mock())
    def test_create_multipart_upload_sets_upload_id(self):
        s3_multipart_uploader = S3MultiPartUpload(bucket=BUCKET, key='Key')
        s3_multipart_uploader.s3_client.create_multipart_upload.return_value = {
            'UploadId': '1'
        }
        s3_multipart_uploader.create_multipart_upload()
        self.assertEqual(s3_multipart_uploader.upload_id, '1')

    def test_upload_multipart_object_s3_object_key_value_error(self):
        s3_multipart_uploader = S3MultiPartUpload(bucket=BUCKET)

        with self.assertRaises(ValueError) as context:
            s3_multipart_uploader.upload_multipart_object('test', 1)

        self.assertTrue('S3 object key is not set' in context.exception.args[0])

    def test_upload_multipart_object_upload_id_value_error(self):
        s3_multipart_uploader = S3MultiPartUpload(bucket=BUCKET, key='Key')

        with self.assertRaises(ValueError) as context:
            s3_multipart_uploader.upload_multipart_object('test', 1)

        self.assertTrue('The transaction upload ID has not been set' in context.exception.args[0])

    @patch('common.utils.s3_multipart_upload_utils.client', Mock())
    def test_upload_multipart_object_success_only_buffer(self):
        s3_multipart_uploader = S3MultiPartUpload(bucket=BUCKET)
        s3_multipart_uploader.upload_id = '1'
        s3_multipart_uploader.s3_client.upload_part.return_value = 'upload_test_response'

        s3_multipart_uploader.object_key = 'TEST_KEY'
        object_body = Mock()
        object_body.read.return_value = 'TEST_BODY'.encode('utf-8')
        s3_multipart_uploader.upload_multipart_object(object_body, 1)
        s3_multipart_uploader.s3_client.upload_part.assert_not_called()

        self.assertEqual(s3_multipart_uploader.part_number, 0)

    @patch('common.utils.s3_multipart_upload_utils.client', Mock())
    def test_complete_multipart_upload_part_mismatch(self):
        s3_multipart_uploader = S3MultiPartUpload(bucket=BUCKET)
        s3_multipart_uploader.part_number = 4
        s3_multipart_uploader.s3_client.list_parts.return_value = {'Parts': ['part_1', 'part_2']}

        with self.assertRaises(ValueError) as context:
            s3_multipart_uploader.complete_multipart_upload()

        self.assertTrue('Mismatch in part numbers, 2 found in s3, but 4 have been sent' in context.exception.args[0])

    @patch('common.utils.s3_multipart_upload_utils.client', MagicMock())
    def test_complete_multipart_upload(self):
        s3_multipart_uploader = S3MultiPartUpload(bucket=BUCKET)
        s3_multipart_uploader.parts = []
        s3_multipart_uploader.upload_id = '1'
        s3_multipart_uploader.s3_client.complete_multipart_upload.return_value = 'completed_upload_response'

        complete_multipart_upload_response = s3_multipart_uploader.complete_multipart_upload()

        s3_multipart_uploader.s3_client.complete_multipart_upload.called_with(
            Bucket=BUCKET, Key='', MultipartUpload=[], UploadId='1'
        )

        self.assertEqual(complete_multipart_upload_response, 'completed_upload_response')

    @patch('common.utils.s3_multipart_upload_utils.client', Mock())
    def test_abort_multipart_upload(self):
        s3_multipart_uploader = S3MultiPartUpload(bucket=BUCKET)
        s3_multipart_uploader.part_number = 10
        s3_multipart_uploader.upload_id = 'TEST'
        s3_multipart_uploader.s3_client.abort_multipart_upload.return_value = 'abort_upload_response'

        complete_abort_upload_response = s3_multipart_uploader.abort_multipart_upload()

        s3_multipart_uploader.s3_client.abort_multipart_upload.called_with(
            Bucket=BUCKET, Key='', UploadId='TEST'
        )

        self.assertEqual(complete_abort_upload_response, 'abort_upload_response')

    @patch('common.utils.s3_multipart_upload_utils.client')
    def test_create_s3_client(self, client_mock):
        s3_multipart_uploader = S3MultiPartUpload(bucket=BUCKET)
        self.assertEqual(s3_multipart_uploader.s3_client, client_mock.return_value)
