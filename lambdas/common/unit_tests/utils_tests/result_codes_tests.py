from unittest import TestCase
from ddt import ddt, data, unpack

from common.utils.result_codes import get_concatenated_result_code_combination


@ddt
class TestResultCodes(TestCase):

    @unpack
    @data(({'result_code': 'Res', 'infection_code': 'Inf', 'action_code': 'Act'}, 'ResInfAct'),
          ({'result_code': 'Res', 'action_code': 'Act'}, 'ResAct'),
          ({'result_code': 'Res', 'infection_code': None, 'action_code': 'Act'}, 'ResAct'),
          ({'irrelevant_field': 'some_value'}, ''))
    def test_result_codes_are_concatenated_correctly(self, test_result, expected_result):
        actual_result = get_concatenated_result_code_combination(test_result)
        self.assertEqual(expected_result, actual_result)
