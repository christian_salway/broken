from dateutil.parser import parse

from common.log_references import CommonLogReference
import unittest
from datetime import date, datetime, timezone
from mock import patch, call
import json
import os
from boto3.dynamodb.conditions import Key
from ddt import ddt, data, unpack
import common.utils.participant_utils as participant_utils
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.audit_utils import AuditActions, AuditUsers


PARTICIPANT_ID = 12345678
EXAMPLE_RECORD = {'name': 'Struan'}
NHS_NUMBER = "23456789"
EXAMPLE_PARTICIPANT = {'name': 'Paul'}
EXAMPLE_RESULT = {'Item': {'test_date': 'Some_Time'}}
EXAMPLE_RESPONSE = {'success': 'true'}
EXAMPLE_UUID = '98f1f2c2-fd40-4e75-9d36-d171479db19d'
DIRECTORY_NAME = os.path.dirname(__file__)


@ddt
@patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'participants-table'})
class TestParticipantUtils(unittest.TestCase):

    maxDiff = None
    example_date = datetime(2020, 1, 1, 0, 0, 0, 123456, tzinfo=timezone.utc)

    @patch('common.utils.participant_utils.dynamodb_get_item')
    def test_get_participant_by_participant_id_calls_through(self, get_item_mock):

        # Given
        get_item_mock.return_value = EXAMPLE_PARTICIPANT

        # When
        expected_response = EXAMPLE_PARTICIPANT
        actual_response = participant_utils.get_participant_by_participant_id(PARTICIPANT_ID)

        # Then
        get_item_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'participant_id': PARTICIPANT_ID,
            'sort_key': participant_utils.ParticipantSortKey.PARTICIPANT
        })

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_utils.segregated_query')
    def test_query_participants_by_nhs_number_calls_through(self, query_mock):

        # Given
        query_mock.return_value = EXAMPLE_PARTICIPANT

        # When
        expected_response = EXAMPLE_PARTICIPANT
        actual_response = participant_utils.query_participants_by_nhs_number(NHS_NUMBER)

        # Then
        query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            IndexName='sanitised-nhs-number-sort-key',
            KeyConditionExpression=Key('sanitised_nhs_number').eq(
                NHS_NUMBER) & Key('sort_key').eq(participant_utils.ParticipantSortKey.PARTICIPANT),
            ProjectionExpression=', '.join([
                'nhs_number',
                'participant_id',
                'sort_key',
                'title',
                'first_name',
                'middle_names',
                'last_name',
                'patient_name',
                'gender',
                'address',
                'date_of_birth',
                'is_fp69',
                'is_ceased',
                'event_text',
                'registered_gp_practice_code',
                'active',
                'nhais_cipher',
                'is_paused'
            ])
        ))

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_utils.segregated_query')
    def test_additional_fields_as_list_in_query_by_nhs_number(self, query_mock):
        participant_utils.query_participants_by_nhs_number(NHS_NUMBER, ['blood_type', 'sanitised_postcode'])

        query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            IndexName='sanitised-nhs-number-sort-key',
            KeyConditionExpression=Key('sanitised_nhs_number').eq(
                NHS_NUMBER) & Key('sort_key').eq(participant_utils.ParticipantSortKey.PARTICIPANT),
            ProjectionExpression=', '.join([
                'nhs_number',
                'participant_id',
                'sort_key',
                'title',
                'first_name',
                'middle_names',
                'last_name',
                'patient_name',
                'gender',
                'address',
                'date_of_birth',
                'is_fp69',
                'is_ceased',
                'event_text',
                'registered_gp_practice_code',
                'active',
                'nhais_cipher',
                'is_paused',
                'blood_type',
                'sanitised_postcode',
            ])
        ))

    @patch('common.utils.participant_utils.segregated_query')
    def test_additional_fields_as_string_in_query_by_nhs_number(self, query_mock):
        participant_utils.query_participants_by_nhs_number(NHS_NUMBER, 'blood_type')

        query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            IndexName='sanitised-nhs-number-sort-key',
            KeyConditionExpression=Key('sanitised_nhs_number').eq(
                NHS_NUMBER) & Key('sort_key').eq(participant_utils.ParticipantSortKey.PARTICIPANT),
            ProjectionExpression=', '.join([
                'nhs_number',
                'participant_id',
                'sort_key',
                'title',
                'first_name',
                'middle_names',
                'last_name',
                'patient_name',
                'gender',
                'address',
                'date_of_birth',
                'is_fp69',
                'is_ceased',
                'event_text',
                'registered_gp_practice_code',
                'active',
                'nhais_cipher',
                'is_paused',
                'blood_type',
            ])
        ))

    @patch('common.utils.participant_utils.dynamodb_query')
    def test_query_participant_records_by_source_hash_calls_dynamo_with_correct_parameters(
            self, query_mock):
        query_mock.return_value = 'records'
        test_source_hash = 'hashed'
        actual_value = participant_utils.query_participant_records_by_source_hash(test_source_hash)

        self.assertEqual('records', actual_value)
        query_mock.assert_called_once_with(TableNames.PARTICIPANTS, dict(
            IndexName='source-hash',
            KeyConditionExpression=Key('source_hash').eq(test_source_hash)
        ))

    @patch('common.utils.participant_utils.dynamodb_query')
    def test_query_participant_records_by_source_hash_returns_none_if_no_records(self, query_mock):
        query_mock.return_value = {}
        test_source_hash = 'hashed'
        actual_value = participant_utils.query_participant_records_by_source_hash(test_source_hash)
        self.assertEqual({}, actual_value)

    @patch('common.utils.participant_utils.dynamodb_get_item')
    def test_get_record_from_participant_table(self, get_item_mock):
        get_item_mock.return_value = EXAMPLE_RESULT

        expected_response = EXAMPLE_RESULT
        actual_response = participant_utils.get_record_from_participant_table(
            'PARTICIPANT', 'SORT_KEY', segregate=False)

        get_item_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'participant_id': 'PARTICIPANT',
            'sort_key': 'SORT_KEY'
        })
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_utils.segregated_dynamodb_get_item')
    def test_segregated_get_record_from_participant_table(self, get_item_mock):
        get_item_mock.return_value = EXAMPLE_RESULT

        expected_response = EXAMPLE_RESULT
        actual_response = participant_utils.get_record_from_participant_table(
            'PARTICIPANT', 'SORT_KEY', segregate=True)

        get_item_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'participant_id': 'PARTICIPANT',
            'sort_key': 'SORT_KEY',
        })
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_utils.dynamodb_put_item')
    def test_create_replace_record_calls_through(self, put_item_mock):

        # Given
        put_item_mock.return_value = EXAMPLE_RESPONSE

        # When
        expected_response = EXAMPLE_RESPONSE
        actual_response = participant_utils.create_replace_record_in_participant_table(EXAMPLE_RECORD, segregate=False)

        # Then
        put_item_mock.assert_called_with(TableNames.PARTICIPANTS, EXAMPLE_RECORD)
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_utils.segregated_put_item')
    def test_segregated_create_replace_record_calls_through(self, put_item_mock):

        # Given
        put_item_mock.return_value = EXAMPLE_RESPONSE

        # When
        expected_response = EXAMPLE_RESPONSE
        actual_response = participant_utils.create_replace_record_in_participant_table(EXAMPLE_RECORD, segregate=True)

        # Then
        put_item_mock.assert_called_with(TableNames.PARTICIPANTS, EXAMPLE_RECORD)
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_utils.dynamodb_update_item')
    def test_update_record_calls_through(self, dynamodb_update_mock):
        # When
        update_attributes = {
            'name': 'Paul'
        }
        participant_utils.update_participant_record(EXAMPLE_PARTICIPANT, update_attributes)

        dynamodb_update_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'Key': {'name': 'Paul'},
            'UpdateExpression': 'SET #name = :name',
            'ExpressionAttributeNames': {'#name': 'name'},
            'ExpressionAttributeValues': {':name': 'Paul'},
            'ReturnValues': 'NONE'
        })

    @patch('common.utils.participant_utils.dynamodb_update_item')
    def test_update_record_calls_through_no_expression_values(self, dynamodb_update_mock):
        # When
        update_attributes = {}
        participant_utils.update_participant_record(EXAMPLE_PARTICIPANT, update_attributes, delete_attributes=['name'])

        dynamodb_update_mock.assert_called_with(TableNames.PARTICIPANTS, {
            'Key': {'name': 'Paul'},
            'UpdateExpression': 'REMOVE #name',
            'ExpressionAttributeNames': {'#name': 'name'},
            'ReturnValues': 'NONE'
        })

    update_record = {
        'record_keys': {
            'participant_id': 'PARTICIPANT2',
            'sort_key': 'SORT_KEY2',
        },
        'update_values': {
            'status': 'R',
            'next_test_due_date': '2022-01-01',
        },
        'delete_values': ['crm_number']
    }
    transact_mock_items = [
        {
            'Put': {
                'TableName': 'participants-table',
                'Item': {'key': {'S': 'value_to_edit'}},
            }
        },
        {
            'Delete': {
                'TableName': 'participants-table',
                'Key': {
                    'participant_id': {'S': 'PARTICIPANT'},
                    'sort_key': {'S': 'SORT_KEY'}}
            }
        },
        {
            'Update': {
                'TableName': 'participants-table',
                'Key': {
                    'participant_id': {'S': 'PARTICIPANT2'},
                    'sort_key': {'S': 'SORT_KEY2'}
                },
                'UpdateExpression': 'SET #status = :status, #next_test_due_date = :next_test_due_date REMOVE ' +
                '#crm_number',
                'ExpressionAttributeValues': {
                    ':status': {'S': 'R'},
                    ':next_test_due_date': {'S': '2022-01-01'}
                },
                'ExpressionAttributeNames': {
                    '#status': 'status',
                    '#next_test_due_date': 'next_test_due_date',
                    '#crm_number': 'crm_number'
                }
            }
        }
    ]

    @patch('common.utils.participant_utils.dynamodb_transact_write_items')
    def test_transaction_write_participant_record(self, transact_items_mock):
        put_record = {'key': 'value_to_edit'}
        key_to_delete = {
            'participant_id': 'PARTICIPANT',
            'sort_key': 'SORT_KEY',
        }

        participant_utils.transaction_write_records(
            [put_record], [key_to_delete], [self.update_record], segregate=False)

        transact_items_mock.assert_called_with(self.transact_mock_items)

    @patch('common.utils.participant_utils.segregated_dynamodb_transact_write_items')
    def test_segregated_transaction_write_participant_record(self, transact_items_mock):
        put_record = {'key': 'value_to_edit'}
        key_to_delete = {
            'participant_id': 'PARTICIPANT',
            'sort_key': 'SORT_KEY',
        }

        participant_utils.transaction_write_records(
            [put_record], [key_to_delete], [self.update_record], segregate=True)

        transact_items_mock.assert_called_with(self.transact_mock_items)

    @patch('common.utils.participant_utils.uuid')
    def test_generate_participant_id_returns_uuid(self, mock_uuid):

        # Given
        mock_uuid.uuid4.return_value = EXAMPLE_UUID

        # When
        expected_response = EXAMPLE_UUID
        actual_response = participant_utils.generate_participant_id()

        # Then
        self.assertEqual(expected_response, actual_response)

    def test_sanitise_string_removes_nonalpha_characters(self):

        # Given
        strings_to_sanitise = ['smith', '351726583**', 'O\'neill', 'Mc Pherson', 'Bo-Jangles']
        expected_strings = ['SMITH', '351726583', 'ONEILL', 'MCPHERSON', 'BOJANGLES']

        # When
        results = [participant_utils.sanitise_string(string) for string in strings_to_sanitise]

        # Then
        self.assertEqual(expected_strings, results)

    def test_get_first_letter(self):

        # Given
        strings = ['', None, 'Hat']
        expected_initials = ['', '', 'H']

        # When
        results = [participant_utils.get_first_letter(string) for string in strings]

        # Then
        self.assertEqual(expected_initials, results)

    def test_set_search_indexes(self):

        # Given
        demographics = {
            'first_name': 'first name',
            'last_name': 'last name',
            'address': {'postcode': 'PO5 5ST'},
            'date_of_birth': '1990-01-01'}

        expected_result = {
            'address': {'postcode': 'PO5 5ST'},
            'date_of_birth': '1990-01-01',
            'date_of_birth_and_initial': '1990-01-01L',
            'date_of_birth_and_initial_and_postcode': '1990-01-01LPO55ST',
            'first_name': 'first name',
            'last_name': 'last name',
            'sanitised_first_name': 'FIRSTNAME',
            'sanitised_last_name': 'LASTNAME',
            'sanitised_postcode': 'PO55ST'}

        # When
        participant_utils.set_search_indexes(demographics)

        # Then
        self.assertDictEqual(expected_result, demographics)

    def test_set_search_indexes_only_added_if_exist(self):

        # Given
        demographics = {
            'first_name': '',
            'last_name': '',
            'address': {'postcode': ''},
            'date_of_birth': ''}

        expected_result = {
            'address': {'postcode': ''},
            'date_of_birth': '',
            'first_name': '',
            'last_name': ''}

        # When
        participant_utils.set_search_indexes(demographics)

        # Then
        self.assertDictEqual(expected_result, demographics)

    def test_participant_response_correctly_built_given_participant_entity_only_with_all_fields(self):

        participant_entity = json.load(open(
            os.path.join(DIRECTORY_NAME, 'test_data/fixtures/participant_entity_with_all_fields.json')))

        expected_response = json.load(open(
            os.path.join(DIRECTORY_NAME, 'test_data/expected/participant_with_all_participant_derived_fields.json')))

        actual_response = participant_utils.convert_dynamodb_entity_to_participant_response(
            participant_entity, test_results=None, registered_gp_practice_details=None)

        self.assertDictEqual(expected_response, actual_response)

    def test_participant_response_correctly_built_given_participant_entity_only_with_only_mandatory_fields(self):

        participant_entity = {
            'participant_id': 'a random id',
            'nhs_number': 'a random number'
        }

        expected_response = json.load(
            open(os.path.join(DIRECTORY_NAME, 'test_data/expected/participant_with_mandatory_fields.json')))

        actual_response = participant_utils.convert_dynamodb_entity_to_participant_response(
            participant_entity, test_results=None, registered_gp_practice_details=None)

        self.assertDictEqual(expected_response, actual_response)

    def test_participant_contains_test_results_if_test_results_supplied(self):
        participant_entity = {
            'participant_id': 'a random id',
            'nhs_number': 'a random number'
        }

        test_results = [
            {'sort_key': 'RESULT#2019-03-04', 'participant_id': '5', 'slide_number': 'test 1'},
            {'sort_key': 'RESULT#2019-03-05', 'participant_id': '5', 'slide_number': 'test 2', 'nhais_cipher': 'ABC'}
        ]

        expected_response = json.load(
            open(os.path.join(DIRECTORY_NAME, 'test_data/expected/participant_with_test_results.json')))

        actual_response = participant_utils.convert_dynamodb_entity_to_participant_response(
            participant_entity, test_results=test_results, registered_gp_practice_details=None)

        self.assertDictEqual(expected_response, actual_response)

    def test_participant_contains_registered_gp_practice_details_if_registered_gp_practice_details_supplied(self):
        participant_entity = {
            'participant_id': 'a random id',
            'nhs_number': 'a random number'
        }

        gp_practice_details = {'name': 'Test Hospital', 'code': 'AB123'}

        expected_response = json.load(
            open(os.path.join(DIRECTORY_NAME, 'test_data/expected/participant_with_gp_practice_details.json')))

        actual_response = participant_utils.convert_dynamodb_entity_to_participant_response(
            participant_entity, test_results=None, registered_gp_practice_details=gp_practice_details)

        self.assertDictEqual(expected_response, actual_response)

    def test_participant_entity_correctly_built_given_particpant_and_test_results_and_gp_practice_details(self):
        participant_entity = json.load(open(
            os.path.join(DIRECTORY_NAME, 'test_data/fixtures/participant_entity_with_all_fields_ceased.json')))

        test_results = [
            {
                'id': 'test 1',
                'sort_key': 'RESULT#2019-03-04',
                'participant_id': '5',
                'slide_number': '1'
            },
            {
                'id': 'test 2',
                'sort_key': 'RESULT#2019-03-05',
                'participant_id': '5',
                'slide_number': '2',
                'nhais_cipher': 'ABC'
            }
        ]

        gp_practice_details = {'name': 'Test Hospital', 'code': 'AB123'}

        expected_response = json.load(
            open(os.path.join(DIRECTORY_NAME, 'test_data/expected/participant_with_all_fields.json')))

        actual_response = participant_utils.convert_dynamodb_entity_to_participant_response(
            participant_entity, test_results=test_results, registered_gp_practice_details=gp_practice_details)

        self.assertDictEqual(expected_response, actual_response)

    @patch('common.utils.participant_utils.query_participants_by_nhs_number')
    def test_get_participant_id_returns_participant_id_given_participant_found(self, query_participants_mock):
        nhs_number = '1234567890'
        participant_id = 'participant123'
        query_participants_mock.return_value = [{'participant_id': participant_id}]

        actual_participant_id = participant_utils.get_participant_id(nhs_number)

        self.assertEqual(participant_id, actual_participant_id)

    @patch('common.utils.participant_utils.query_participants_by_nhs_number')
    def test_get_participant_id_raises_exception_if_no_participant_found(self, query_participants_mock):
        nhs_number = '1234567890'
        query_participants_mock.return_value = []

        participant_id = participant_utils.get_participant_id(nhs_number)

        self.assertEqual(None, participant_id)

    @patch('common.utils.participant_utils.log')
    @patch('common.utils.participant_utils.query_participants_by_nhs_number')
    def test_get_participant_id_logs_error_if_multiple_particpants_found(self, query_participants_mock, log_mock):
        nhs_number = '1234567890'
        query_participants_mock.return_value = [{'participant_id': '1'}, {'participant_id': '2'}]

        participant_id = participant_utils.get_participant_id(nhs_number)

        log_mock.assert_called_with({'log_reference': CommonLogReference.PARTICIPANT0001})
        self.assertEqual(None, participant_id)

    def test_convert_test_results_can_convert_test_results_with_fields_missing(self):
        results_to_convert = [{'result_id': '1', 'sort_key': 'the sort key'}]

        expected_response = json.load(
            open(os.path.join(DIRECTORY_NAME, 'test_data/expected/test_results_missing_fields.json')))

        actual_response = participant_utils.convert_test_results_entity_to_response(results_to_convert)

        self.assertListEqual(expected_response, actual_response)

    def test_convert_test_results_can_convert_test_results_with_all_fields(self):
        results_to_convert = json.load(
            open(os.path.join(DIRECTORY_NAME, 'test_data/fixtures/test_results_all_fields.json')))

        expected_response = json.load(
            open(os.path.join(DIRECTORY_NAME, 'test_data/expected/test_results_all_fields.json')))

        actual_response = participant_utils.convert_test_results_entity_to_response(results_to_convert)

        self.assertListEqual(expected_response, actual_response)

    def test_previous_name_record_is_created_with_correct_information(self):
        existing_record = json.load(
            open(os.path.join(DIRECTORY_NAME, 'test_data/expected/new_participant_record.json')))

        current_date = date.today().isoformat()

        previous_name_item = {
            'participant_id': '12345',
            'sort_key': 'PREVNAME#' + current_date,
            'date_to': current_date,
            'title': 'MISS',
            'first_name': 'TEST',
            'middle_names': 'WITH MIDDLE NAMES',
            'last_name': 'USER'
        }
        return_value = participant_utils.create_previous_name_record_from_existing_record(
            existing_record)

        self.assertEqual(return_value, previous_name_item)

    @patch('common.utils.participant_utils.create_replace_record_in_participant_table')
    @patch('common.utils.participant_utils.create_previous_name_record_from_existing_record')
    def test_create_previous_name_record_builds_and_saves_record(self, builder_mock, save_mock):
        existing_record = {'some_key': 'some_value'}
        name_record = {'sort_key': 'ADDRESS'}
        builder_mock.return_value = name_record

        participant_utils.create_previous_name_record(existing_record)

        builder_mock.assert_called_with(existing_record)
        save_mock.assert_called_with(name_record)

    @patch('common.utils.participant_utils.update_participant_record')
    @patch('common.utils.participant_utils.audit')
    def test_pause_participant_for_lab_result(
            self,
            audit_mock,
            update_participant_record_mock):

        # Given
        result_id = '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e0'
        participant_id = '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1'

        update_participant_record_mock.return_value = None

        expected_key = {
            'participant_id': participant_id,
            'sort_key': 'PARTICIPANT'
        }

        # When
        participant_utils.pause_participant_for_lab_or_manually_added_result(participant_id, result_id, True)

        # Then
        key_args = update_participant_record_mock.call_args[0][0]
        attributes_args = update_participant_record_mock.call_args[0][1]
        is_paused_received_time = parse(attributes_args['is_paused_received_time'])

        self.assertEqual(expected_key, key_args)
        self.assertEqual(True, attributes_args['is_paused'])
        self.assertEqual(result_id, attributes_args['is_paused_result_id'])
        self.assertEqual(datetime.now(timezone.utc).day, is_paused_received_time.day)
        audit_mock.assert_has_calls(
            [call(
                action=AuditActions.PAUSE_PARTICIPANT,
                user=AuditUsers.SYSTEM,
                participant_ids=['52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1'],
                additional_information={
                    'participant_id': '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1',
                    'is_paused': True,
                    'is_paused_received_time': attributes_args['is_paused_received_time'],
                    'is_paused_result_id': '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e0'
                }
            )]
        )

    @patch('common.utils.participant_utils.update_participant_record')
    @patch('common.utils.participant_utils.audit')
    def test_pause_participant_for_manually_added_result(
            self,
            audit_mock,
            update_participant_record_mock):

        # Given
        crm_number = 'ABC-MISC-CRM-NO'
        comment = 'A comment'
        participant_id = '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1'

        update_participant_record_mock.return_value = None

        expected_key = {
            'participant_id': participant_id,
            'sort_key': 'PARTICIPANT'
        }

        # When
        participant_utils.pause_participant_for_lab_or_manually_added_result(
            participant_id, crm_number, False, 'A comment')

        # Then
        key_args = update_participant_record_mock.call_args[0][0]
        attributes_args = update_participant_record_mock.call_args[0][1]
        is_paused_received_time = parse(attributes_args['is_paused_received_time'])

        self.assertEqual(expected_key, key_args)
        self.assertEqual(True, attributes_args['is_paused'])
        self.assertEqual(crm_number, attributes_args['is_paused_info'].get('crm_number'))
        self.assertEqual(comment, attributes_args['is_paused_info'].get('comment'))
        self.assertEqual(datetime.now(timezone.utc).day, is_paused_received_time.day)
        audit_mock.assert_has_calls(
            [call(
                action=AuditActions.PAUSE_PARTICIPANT,
                user=AuditUsers.SYSTEM,
                participant_ids=['52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1'],
                additional_information={
                    'participant_id': '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1',
                    'is_paused': True,
                    'is_paused_received_time': attributes_args['is_paused_received_time'],
                    'is_paused_info': {'crm_number': 'ABC-MISC-CRM-NO', 'comment': 'A comment'}
                }
            )]
        )

    @patch('common.utils.participant_utils.update_participant_record')
    def test_unpause_participant(
            self,
            update_participant_record_mock):
        # Given
        participant_id = '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1'

        update_participant_record_mock.return_value = None

        # When
        participant_utils.unpause_participant(participant_id)

        # Then
        expected_key = {
            'participant_id': participant_id,
            'sort_key': 'PARTICIPANT'
        }

        key_args = update_participant_record_mock.call_args[0][0]
        delete_attributes_arg = update_participant_record_mock.call_args[0][2]

        self.assertEqual(expected_key, key_args)
        self.assertEqual(["is_paused",
                          "is_paused_result_id",
                          "is_paused_crm_number",
                          "is_paused_received_time"],
                         delete_attributes_arg)

    @unpack
    @data(
        (
            {'nhs_number': '123456'},
            'participant_id_1',
            '123456',
            False
        ),
        (
            None,
            'participant_id_2',
            'participant not found',
            False
        ),
        (
            {'participant_id': 'participant_with_no_nhs_number'},
            'participant_with_no_nhs_number',
            'not present',
            False
        ),
        (
            {'nhs_number': '123456'},
            'participant_id_3',
            '123456',
            True
        ),
    )
    @patch('common.utils.participant_utils.get_participant_by_participant_id')
    def test_get_nhs_number_by_participant_id(
        self, participant, participant_id, expected_nhs_number, segregate, get_participant_mock,
    ):
        # Arrange
        get_participant_mock.return_value = participant

        # Act
        actual_nhs_number = participant_utils.get_nhs_number_by_participant_id(participant_id, segregate=segregate)

        # Assert
        get_participant_mock.assert_called_once_with(participant_id, segregate=segregate)
        self.assertEqual(actual_nhs_number, expected_nhs_number)
