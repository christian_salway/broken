import unittest
from boto3.dynamodb.conditions import Attr, Or, Key
from mock import patch
from common.utils import organisation_supplementary_utils
from common.utils.dynamodb_access.table_names import TableNames


class TestOrganisationSupplementaryUtils(unittest.TestCase):

    @patch('common.utils.organisation_supplementary_utils.dynamodb_scan')
    def test_get_organisation_information_returns_pnl_nrl_data_if_present(self, scan_mock):
        scan_mock.return_value = ['test']
        organisation_supplementary_utils.query_for_pnl_nrl_email_records()

        filter_expression = Or(Attr('type_value').begins_with('email_PNL'), Attr('type_value').begins_with('email_NRL'))

        scan_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, {
            'FilterExpression': filter_expression
        })

    @patch('common.utils.organisation_supplementary_utils.dynamodb_query')
    def test_get_organisation_information_returns_not_registered_data_if_present(self, query_mock):
        organisation_supplementary_utils.query_for_not_registered_records()
        query_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, {
            'IndexName': 'type_value',
            'KeyConditionExpression': Key('type_value').eq('NOT_REGISTERED')
        })

    @patch('common.utils.organisation_supplementary_utils.dynamodb_query')
    def test_query_for_print_record(self, query_mock):
        query_mock.return_value = ['test']
        organisation_supplementary_utils._query_for_print_record('gp_code', 'some_key')
        expected_key_expression = (Key('organisation_code').eq('gp_code') & Key('type_value').begins_with('some_key'))
        query_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, {
            'KeyConditionExpression': expected_key_expression,
            'ScanIndexForward': False
        })

    @patch('common.utils.organisation_supplementary_utils.dynamodb_query')
    def test_get_print_pnl_record(self, query_mock):
        query_mock.return_value = ['test']
        organisation_supplementary_utils.get_print_pnl_record('gp_code')
        expected_key_expression = (Key('organisation_code').eq('gp_code') & Key('type_value').begins_with('PRINT_PNL'))
        query_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, {
            'KeyConditionExpression': expected_key_expression,
            'ScanIndexForward': False
        })

    @patch('common.utils.organisation_supplementary_utils.dynamodb_query')
    def test_get_print_nrl_record(self, query_mock):
        query_mock.return_value = ['test']
        organisation_supplementary_utils.get_print_nrl_record('gp_code')
        expected_key_expression = (Key('organisation_code').eq('gp_code') & Key('type_value').begins_with('PRINT_NRL'))
        query_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, {
            'KeyConditionExpression': expected_key_expression,
            'ScanIndexForward': False
        })

    @patch('common.utils.organisation_supplementary_utils.dynamodb_query')
    def test_get_print_review_record(self, query_mock):
        query_mock.return_value = ['test']
        organisation_supplementary_utils.get_print_review_record('gp_code')
        expected_key_expression = (
            Key('organisation_code').eq('gp_code') & Key('type_value').begins_with('PRINT_REVIEW')
        )
        query_mock.assert_called_with(TableNames.ORGANISATION_SUPPLEMENTAL, {
            'KeyConditionExpression': expected_key_expression,
            'ScanIndexForward': False
        })
