from unittest import TestCase

from boto3.dynamodb.conditions import Key
from mock import patch, Mock, call

from common.log_references import CommonLogReference
from common.utils.dynamodb_access.table_names import TableNames

with patch('boto3.resource') as resource_mock:
    from common.utils.session_utils import create_session, get_session, update_session, delete_session,\
        get_session_from_lambda_event, _has_session_expired, invalidate_any_duplicate_sessions


class SessionsUtilsTests(TestCase):

    @patch('common.utils.session_utils.get_salted_and_hashed_value')
    @patch('common.utils.session_utils.time')
    @patch('common.utils.session_utils.uuid')
    @patch('common.utils.session_utils.dynamodb_put_item')
    def test_create_session_adds_session_to_dynamodb(
            self, dynamodb_put_mock, uuid_mock, time_mock, salt_and_hash_mock):
        uuid_mock.uuid4.return_value = "uuid-v4"
        time_mock.return_value = 1543000000
        salt_and_hash_mock.return_value = 'uuid-v4-hashed-and-salted'
        expected_session = {
            'session_token': 'uuid-v4',
            'hashed_session_token': 'uuid-v4-hashed-and-salted',
            'expires': 1543000600,
            'nonce': 'uuid-v4',
            'session_id': 'uuid-v4',
            'reauthenticate_timestamp': 1543028800,
            'is_current_session': True
        }
        create_session()
        args, _ = dynamodb_put_mock.call_args
        self.assertDictEqual(expected_session, args[1])

    @patch('common.utils.session_utils.log', Mock())
    @patch('common.utils.session_utils.Attr')
    @patch('common.utils.session_utils.Key')
    @patch('common.utils.session_utils.dynamodb_query')
    @patch('common.utils.session_utils.time')
    def test_get_session_calls_dynamodb_with_correct_parameters_and_returns_session(
            self, time_mock, dynamodb_query_mock, key_mock, attr_mock):
        time_mock.return_value = 1543000000
        session_data = {
            'session_token': '123456',
            'reauthenticate_timestamp': 1543000001,
            'expires': 1543000001
        }
        dynamodb_query_mock.return_value = [
            session_data
        ]
        key_condition_expression_mock = Mock()
        filter_expression_mock = Mock()
        key_mock.return_value = key_condition_expression_mock
        attr_mock.return_value = filter_expression_mock
        actual_session = get_session('123456')

        key_condition_expression_mock.eq.assert_called_with('123456')
        filter_expression_mock.gt.assert_called_with(1543000000)
        dynamodb_query_mock.assert_called_with(TableNames.SESSIONS, dict(
            KeyConditionExpression=key_condition_expression_mock.eq.return_value,
            FilterExpression=filter_expression_mock.gt.return_value
        ))
        self.assertDictEqual(session_data, actual_session)

    @patch('common.utils.session_utils.log')
    @patch('common.utils.session_utils.time')
    @patch('common.utils.session_utils.dynamodb_update_item')
    def test_update_session_calls_dynamodb_with_correct_parameters_and_returns_response_attributes(
            self, dynamodb_update_mock, time_mock, log_mock):
        time_mock.return_value = 1543000000

        update_attributes = {
            'session_type': 'simples',
            'user_data': 'things and such'
        }

        expected_attributes_names = {
            '#expires': 'expires',
            '#session_type': 'session_type',
            '#user_data': 'user_data'
        }

        expected_attributes_values = {
            ':session_type': 'simples',
            ':user_data': 'things and such',
            ':expires': 1543000600
        }

        dynamodb_update_mock.return_value = {'Attributes': {'some_key': 'some_value'}}

        response_attributes = update_session('123456', update_attributes)

        args, _ = dynamodb_update_mock.call_args

        self.assertDictEqual({'some_key': 'some_value'}, response_attributes)
        self.assertEqual(TableNames.SESSIONS, args[0])
        self.assertDictEqual({'session_token': '123456'}, args[1]['Key'])
        self.assertDictEqual(expected_attributes_names, args[1]['ExpressionAttributeNames'])
        self.assertDictEqual(expected_attributes_values, args[1]['ExpressionAttributeValues'])
        self.assertEqual('ALL_NEW', args[1]['ReturnValues'])

        actual_log_1, _ = log_mock.call_args_list[0]
        actual_log_2, _ = log_mock.call_args_list[1]

        self.assertDictEqual({'log_reference': CommonLogReference.SESSION0004}, actual_log_1[0])
        self.assertDictEqual({'log_reference': CommonLogReference.SESSION0005}, actual_log_2[0])

    @patch('common.utils.session_utils.log')
    @patch('common.utils.session_utils.time')
    @patch('common.utils.session_utils.dynamodb_update_item')
    def test_update_session_calls_dynamodb_with_correct_parameters_and_returns_response_attributes_if_expiry_supplied(
            self, dynamodb_update_mock, time_mock, log_mock):
        time_mock.return_value = 1543000000
        expiry_update_seconds = 10

        update_attributes = {
            'session_type': 'simples',
            'user_data': 'things and such'
        }

        expected_attributes_names = {
            '#expires': 'expires',
            '#session_type': 'session_type',
            '#user_data': 'user_data'
        }

        expected_attributes_values = {
            ':session_type': 'simples',
            ':user_data': 'things and such',
            ':expires': 1543000010
        }

        dynamodb_update_mock.return_value = {'Attributes': {'some_key': 'some_value'}}

        response_attributes = update_session('123456', update_attributes, expiry_update_seconds)

        args, _ = dynamodb_update_mock.call_args

        self.assertDictEqual({'some_key': 'some_value'}, response_attributes)
        self.assertEqual(TableNames.SESSIONS, args[0])
        self.assertDictEqual({'session_token': '123456'}, args[1]['Key'])
        self.assertDictEqual(expected_attributes_names, args[1]['ExpressionAttributeNames'])
        self.assertDictEqual(expected_attributes_values, args[1]['ExpressionAttributeValues'])
        self.assertEqual('ALL_NEW', args[1]['ReturnValues'])

        actual_log_1, _ = log_mock.call_args_list[0]
        actual_log_2, _ = log_mock.call_args_list[1]

        self.assertDictEqual({'log_reference': CommonLogReference.SESSION0004}, actual_log_1[0])
        self.assertDictEqual({'log_reference': CommonLogReference.SESSION0005}, actual_log_2[0])

    @patch('common.utils.session_utils.log')
    @patch('common.utils.session_utils.time')
    @patch('common.utils.session_utils.dynamodb_update_item')
    def test_update_session_extends_session_given_no_attributes_supplied(self,
                                                                         dynamodb_update_mock,
                                                                         time_mock,
                                                                         log_mock):
        time_mock.return_value = 1543000000

        dynamodb_update_mock.return_value = {'Attributes': {'some_key': 'some_value'}}

        response_attributes = update_session('123456')

        expected_attributes_names = {
            '#expires': 'expires'
        }

        expected_attributes_values = {
            ':expires': 1543000600
        }

        args, _ = dynamodb_update_mock.call_args

        self.assertEqual(TableNames.SESSIONS, args[0])
        self.assertDictEqual({'some_key': 'some_value'}, response_attributes)
        self.assertDictEqual({'session_token': '123456'}, args[1]['Key'])
        self.assertDictEqual(expected_attributes_names, args[1]['ExpressionAttributeNames'])
        self.assertDictEqual(expected_attributes_values, args[1]['ExpressionAttributeValues'])
        self.assertEqual('ALL_NEW', args[1]['ReturnValues'])

        actual_log_1, _ = log_mock.call_args_list[0]
        actual_log_2, _ = log_mock.call_args_list[1]

        self.assertDictEqual({'log_reference': CommonLogReference.SESSION0004}, actual_log_1[0])
        self.assertDictEqual({'log_reference': CommonLogReference.SESSION0005}, actual_log_2[0])

    @patch('common.utils.session_utils.log', Mock())
    @patch('common.utils.session_utils.dynamodb_delete_item')
    def test_delete_session_calls_dynamodb_with_correct_parameters(self, dynamodb_delete_mock):
        delete_session('123456')
        args, _ = dynamodb_delete_mock.call_args
        self.assertEqual(TableNames.SESSIONS, args[0])
        self.assertEqual({'session_token': '123456'}, args[1])

    def test_get_session_from_lambda_event(self):
        event = {'requestContext': {'authorizer': {'session': '{"session_id": "the_session", "expires": 123456}'}}}
        expected_session = {'session_id': 'the_session', 'expires': 123456}
        actual_session = get_session_from_lambda_event(event)
        self.assertDictEqual(expected_session, actual_session)

    @patch('common.utils.session_utils.log', Mock())
    @patch('common.utils.session_utils.time')
    def test_has_session_expired_returns_false_when_session_has_not_expired(self, time_mock):
        time_mock.return_value = 1543000001
        session_data = {
            'session_token': '123456',
            'reauthenticate_timestamp': 1543000001,
            'expires': 1543000001
        }

        self.assertFalse(_has_session_expired(session_data))

    @patch('common.utils.session_utils.log', Mock())
    @patch('common.utils.session_utils.time')
    def test_has_session_expired_returns_true_when_session_has_expired(self, time_mock):
        time_mock.return_value = 1543000001

        session_test_data = [
            {
                'session_token': '123456',
                'reauthenticate_timestamp': 1543000001,
                'expires': 1543000000
            },
            {
                'session_token': '123456',
                'reauthenticate_timestamp': 1543000000,
                'expires': 1543000001
            }
        ]

        for session_data in session_test_data:
            with self.subTest(session_data=session_data):
                self.assertTrue(_has_session_expired(session_data))

    @patch('common.utils.session_utils.log')
    @patch('common.utils.session_utils.dynamodb_query')
    @patch('common.utils.session_utils.dynamodb_update_item')
    def test_invalidate_any_duplicate_sessions_performs_no_updates_if_no_duplicates_found(
        self, dynamodb_update_mock, dynamodb_query_mock, log_mock
    ):
        nhsid_useruid = 'the_user_id'
        dynamodb_query_mock.return_value = []

        invalidate_any_duplicate_sessions('', nhsid_useruid)

        expected_query_call = call(TableNames.SESSIONS, dict(
            IndexName='nhsid_useruid',
            KeyConditionExpression=Key('nhsid_useruid').eq(nhsid_useruid)
        ))
        self.assertEqual(expected_query_call, dynamodb_query_mock.call_args)

        dynamodb_update_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': CommonLogReference.SESSION0007}),
                              call({'log_reference': CommonLogReference.SESSION0008})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('common.utils.session_utils.time')
    @patch('common.utils.session_utils.log')
    @patch('common.utils.session_utils.dynamodb_query')
    @patch('common.utils.session_utils.dynamodb_update_item')
    def test_invalidate_any_duplicate_sessions_invalidates_any_duplicate_sessions_found(self,
                                                                                        dynamodb_update_mock,
                                                                                        dynamodb_query_mock,
                                                                                        log_mock,
                                                                                        time_mock):

        nhsid_useruid = 'the_user_id'
        dynamodb_query_mock.return_value = [{'session_token': 'token_1'}, {'session_token': 'token_2'}]
        time_mock.return_value = 1543000001

        invalidate_any_duplicate_sessions('token_1', nhsid_useruid)

        expected_query_call = call(TableNames.SESSIONS, dict(
            IndexName='nhsid_useruid',
            KeyConditionExpression=Key('nhsid_useruid').eq(nhsid_useruid)
        ))
        self.assertEqual(expected_query_call, dynamodb_query_mock.call_args)

        expected_update_calls = [
            call(TableNames.SESSIONS, {
                "Key": {'session_token': 'token_2'},
                "UpdateExpression": 'SET #is_current_session = :is_current_session',
                "ExpressionAttributeValues": {':is_current_session': False},
                "ExpressionAttributeNames": {'#is_current_session': 'is_current_session'},
                "ReturnValues": 'ALL_NEW'
            })
        ]

        dynamodb_update_mock.assert_has_calls(expected_update_calls)

        expected_log_calls = [call({'log_reference': CommonLogReference.SESSION0007}),
                              call({'log_reference': CommonLogReference.SESSION0009, 'number_of_duplicates': 1}),
                              call({'log_reference': CommonLogReference.SESSION0004}),
                              call({'log_reference': CommonLogReference.SESSION0005}),
                              call({'log_reference': CommonLogReference.SESSION0010})]

        log_mock.assert_has_calls(expected_log_calls)
