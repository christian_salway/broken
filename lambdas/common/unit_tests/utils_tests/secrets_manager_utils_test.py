from unittest import TestCase
from mock import patch, Mock
import json
from common.utils.secrets_manager_utils import get_secret, get_secret_by_key, get_secrets_dictionary
from common.exceptions import (
    DecryptionFailureException, InternalServiceErrorException, InvalidParameterException,
    InvalidRequestException, ResourceNotFoundException)
from botocore.exceptions import ClientError


@patch('common.utils.secrets_manager_utils.create_secrets_manager_client')
class SecretsUtilsTests(TestCase):

    def setUp(self):
        self.client = Mock()

    @patch('common.utils.secrets_manager_utils.base64.b64decode')
    def test_get_secret_from_binary(self, decode_mock, client_mock):
        client_mock.return_value = self.client
        self.client.get_secret_value.return_value = {'SecretBinary': '01101010'}
        decode_mock.return_value = 'decoded'

        decoded_secret = get_secret('spujb', region_name="eu-west-1")

        self.assertEqual(decoded_secret, 'decoded')
        decode_mock.assert_called_with('01101010')
        client_mock.assert_called_with("eu-west-1")

    def test_get_secret_error_handling(self, client_mock):
        errors = [
            ('DecryptionFailureException', DecryptionFailureException),
            ('InternalServiceErrorException', InternalServiceErrorException),
            ('InvalidParameterException', InvalidParameterException),
            ('InvalidRequestException', InvalidRequestException),
            ('ResourceNotFoundException', ResourceNotFoundException)
        ]
        client_mock.return_value = self.client

        for code, exception in errors:
            self.client.get_secret_value.side_effect = ClientError({
                'Error': {'Code': code}
            }, '')
            with self.subTest():
                with self.assertRaises(exception):
                    get_secret('spujb', region_name="eu-west-1")

    def test_get_secret_from_string(self, client_mock):
        client_mock.return_value = self.client
        self.client.get_secret_value.return_value = {
            'SecretString': json.dumps({'weeyah': 'seventeen', 'spujb': 'warg'})
        }

        secret = get_secret('spujb', region_name="eu-west-1")

        self.assertEqual(secret, 'warg')
        client_mock.assert_called_with("eu-west-1")

    def test_get_secret_from_string_name_is_not_key(self, client_mock):
        client_mock.return_value = self.client
        secret_return_value = json.dumps({'weeyah': 'seventeen', 'spujb': 'warg'})
        self.client.get_secret_value.return_value = {'SecretString': secret_return_value}

        secret = get_secret('spujb', secret_name_is_key=False)

        self.assertEqual(secret, secret_return_value)
        client_mock.assert_called_with("eu-west-2")

    # get_secret_by_key

    @patch('common.utils.secrets_manager_utils.base64.b64decode')
    def test_get_secret_by_key_from_binary(self, decode_mock, client_mock):
        client_mock.return_value = self.client
        self.client.get_secret_value.return_value = {'SecretBinary': '01101010'}
        decode_mock.return_value = 'decoded'

        decoded_secret = get_secret_by_key('spujb', 'weeyah')

        self.assertEqual(decoded_secret, 'decoded')
        decode_mock.assert_called_with('01101010')
        client_mock.assert_called_with("eu-west-2")

    def test_get_secret_by_key_error_handling(self, client_mock):
        errors = [
            ('DecryptionFailureException', DecryptionFailureException),
            ('InternalServiceErrorException', InternalServiceErrorException),
            ('InvalidParameterException', InvalidParameterException),
            ('InvalidRequestException', InvalidRequestException),
            ('ResourceNotFoundException', ResourceNotFoundException)
        ]
        client_mock.return_value = self.client

        for code, exception in errors:
            self.client.get_secret_value.side_effect = ClientError({
                'Error': {'Code': code}
            }, '')
            with self.subTest():
                with self.assertRaises(exception):
                    get_secret_by_key('spujb', 'weeyah', region_name="eu-west-1")

    def test_get_secret_by_key_from_string(self, client_mock):
        client_mock.return_value = self.client
        self.client.get_secret_value.return_value = {
            'SecretString': json.dumps({'weeyah': 'seventeen', 'asdf': 'warg'})
        }

        secret = get_secret_by_key('spujb', 'weeyah', region_name="eu-west-1")

        self.assertEqual(secret, 'seventeen')
        client_mock.assert_called_with("eu-west-1")

    def test_get_secrets_dictionary(self, client_mock):
        client_mock.return_value = self.client
        secrets = {'weeyah': 'seventeen', 'asdf': 'warg'}
        self.client.get_secret_value.return_value = {
            'SecretString': json.dumps({'weeyah': 'seventeen', 'asdf': 'warg'})
        }

        actual_secrets = get_secrets_dictionary('spujb')

        self.assertEqual(secrets, actual_secrets)
        client_mock.assert_called_with("eu-west-2")
