from unittest import TestCase
from mock import patch

from common.utils.dynamodb_access.table_names import TableNames

with patch('boto3.resource') as resource_mock:
    from common.utils.reference_utils import (
        create_replace_reference_record)

EXAMPLE_RECORD = {'ref_type': 'G'}
EXAMPLE_RESPONSE = {'success': 'true'}


@patch('os.environ', {'DYNAMODB_REFERENCE': 'reference_table'})
class TestReferenceUtils(TestCase):
    @patch('common.utils.reference_utils.dynamodb_put_item')
    def test_create_replace_record_calls_through(self, dynamodb_put_mock):

        # Given
        dynamodb_put_mock.return_value = EXAMPLE_RESPONSE

        # When
        expected_response = EXAMPLE_RESPONSE
        actual_response = create_replace_reference_record(EXAMPLE_RECORD)

        # Then
        dynamodb_put_mock.assert_called_with(TableNames.REFERENCE, EXAMPLE_RECORD)
        self.assertEqual(expected_response, actual_response)
