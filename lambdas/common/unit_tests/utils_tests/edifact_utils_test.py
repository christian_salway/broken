import unittest

from ddt import ddt, data, unpack

from common.utils.edifact_exceptions import (
    InvalidEdifactHeader
)

from common.utils.edifact_utils import (
    tokenize_edifact,
    split_raw_edifact_into_raw_lab_results,
    decode_lab_result,
    _escaped_split,
    _remove_escapes,
    _decode_nhais_cipher_and_sending_lab,
    _decode_patient_details,
    _decode_date_time_period,
    _decode_health_body,
    _decode_result_recall_details,
    _decode_patient_cytology_details,
    _decode_name_and_address,
    _decode_person_name,
    _decode_test_result,
    _decode_health_information,
    _decode_quantity_row,
    validate_edifact_postcode
)
from common.unit_tests.utils_tests.test_data.edifact_test_data import (
    TYPE_3_WITH_1_RESULT_MESSAGE,
    TYPE_3_WITH_2_RESULTS_MESSAGE,
    TYPE_3_WITH_3_RESULTS_MESSAGE,
    TYPE_3_WITH_1_RESULT_CONTAINING_SPECIAL_CHARS_MESSAGE,
    TYPE_3_WITH_1_RESULT_SPLIT,
    TYPE_3_WITH_2_RESULTS_SPLIT,
    TYPE_3_WITH_3_RESULTS_SPLIT,
    TYPE_9_WITH_1_RESULT_MESSAGE,
    TYPE_9_WITH_2_RESULTS_MESSAGE,
    TYPE_9_WITH_3_RESULTS_MESSAGE,
    TYPE_9_WITH_1_RESULT_SPLIT,
    TYPE_9_WITH_2_RESULTS_SPLIT,
    TYPE_9_WITH_3_RESULTS_SPLIT,
    TYPE_3_WITH_2_RESULTS_MESSAGE_DECODED,
    TYPE_3_WITH_1_RESULT_CONTAINING_SPECIAL_CHARS_MESSAGE_DECODED,
    INDIVIDUAL_TYPE_3_RESULT,
    INDIVIDUAL_TYPE_9_RESULT,
    DECODED_RESULT_TYPE_3,
    DECODED_RESULT_TYPE_9,
    INVALID_TYPE_3_WITH_INVALID_RESULT
)

from common.models.result import SELF_SAMPLE_SENDER_CODE


@ddt
class TestEdifactUtils(unittest.TestCase):

    def test_decode_type_3_edifact_lab_result_containing_2_results(self):

        # When
        elements = tokenize_edifact(TYPE_3_WITH_2_RESULTS_MESSAGE)

        # Then
        self.assertListEqual(TYPE_3_WITH_2_RESULTS_MESSAGE_DECODED, elements)

    def test_decode_type_3_edifact_lab_result_containing_1_result_with_special_chars(self):

        # When
        elements = tokenize_edifact(TYPE_3_WITH_1_RESULT_CONTAINING_SPECIAL_CHARS_MESSAGE)

        # Then
        self.assertListEqual(TYPE_3_WITH_1_RESULT_CONTAINING_SPECIAL_CHARS_MESSAGE_DECODED, elements)

    def test_extract_type_3_lab_results_with_edifact_containing_1_results(self):

        # When
        result_type, result_grouping = split_raw_edifact_into_raw_lab_results(TYPE_3_WITH_1_RESULT_MESSAGE)

        # Then
        self.assertEqual(3, result_type)
        self.assertListEqual(TYPE_3_WITH_1_RESULT_SPLIT, result_grouping)

    def test_extract_type_3_lab_results_with_edifact_containing_2_results(self):

        # When
        result_type, result_grouping = split_raw_edifact_into_raw_lab_results(TYPE_3_WITH_2_RESULTS_MESSAGE)

        # Then
        self.assertEqual(3, result_type)
        self.assertListEqual(TYPE_3_WITH_2_RESULTS_SPLIT, result_grouping)

    def test_extract_type_3_lab_results_with_edifact_containing_3_results(self):

        # When
        result_type, result_grouping = split_raw_edifact_into_raw_lab_results(TYPE_3_WITH_3_RESULTS_MESSAGE)

        # Then
        self.assertEqual(3, result_type)
        self.assertListEqual(TYPE_3_WITH_3_RESULTS_SPLIT, result_grouping)

    def test_decode_type_3_full_lab_result(self):
        actual_result = decode_lab_result(INDIVIDUAL_TYPE_3_RESULT)

        self.assertDictEqual(DECODED_RESULT_TYPE_3, actual_result)

    def test_extract_type_9_lab_results_with_edifact_containing_1_results(self):

        # When
        result_type, result_grouping = split_raw_edifact_into_raw_lab_results(TYPE_9_WITH_1_RESULT_MESSAGE)

        # Then
        self.assertEqual(9, result_type)
        self.assertListEqual(TYPE_9_WITH_1_RESULT_SPLIT, result_grouping)

    def test_extract_type_9_lab_results_with_edifact_containing_2_results(self):

        # When
        result_type, result_grouping = split_raw_edifact_into_raw_lab_results(TYPE_9_WITH_2_RESULTS_MESSAGE)

        # Then
        self.assertEqual(9, result_type)
        self.assertListEqual(TYPE_9_WITH_2_RESULTS_SPLIT, result_grouping)

    def test_extract_type_9_lab_results_with_edifact_containing_3_results(self):

        # When
        result_type, result_grouping = split_raw_edifact_into_raw_lab_results(TYPE_9_WITH_3_RESULTS_MESSAGE)

        # Then
        self.assertEqual(9, result_type)
        self.assertListEqual(TYPE_9_WITH_3_RESULTS_SPLIT, result_grouping)

    def test_decode_type_9_full_lab_result(self):
        actual_result = decode_lab_result(INDIVIDUAL_TYPE_9_RESULT)

        self.assertDictEqual(DECODED_RESULT_TYPE_9, actual_result)

    def test_decode_nhais_cipher_and_sending_lab(self):
        test_data = ['UNB', ['UNOA', '2'], 'WHI3', 'WIG1', ['191025', '1144'], '00002155', '', '', 'CYTFH', '', '', '1'] 
        decoded_row = _decode_nhais_cipher_and_sending_lab(test_data)

        expected_result = {
            'raw_nhais_cipher': 'WIG1',
            'sending_lab': 'WHI3'
        }
        self.assertDictEqual(expected_result, decoded_row)

    def test_decode_patient_details(self):
        test_data = ['PAD', ['12345', '801'], '', '', 'surname', '', ['', 'forename'], '', '', '', ['800', '12121982', '911']] 
        decoded_row = _decode_patient_details(test_data)

        expected_result = {
            'nhs_number': '12345',
            'last_name': 'surname',
            'previous_last_name': '',
            'first_name': 'forename',
            'title': '',
            'date_of_birth': '12121982',
            'date_of_birth_format': '%d%m%Y'
        }
        self.assertDictEqual(expected_result, decoded_row)

    def test_interpret_nhs_row_source_of_smear(self):
        test_data = ['NHS', ['1', '838']]
        decoded_row = _decode_health_body(test_data)

        expected_result = {
            'sender_source_type': '1',
            'source_code': 'G'
        }
        self.assertDictEqual(expected_result, decoded_row)

    def test_interpret_nhs_row_sender_code(self):
        test_data = ['NHS', ['SENDER_CODE', '839']]
        decoded_row = _decode_health_body(test_data)

        expected_result = {
            'sender_code': 'SENDER_CODE'
        }
        self.assertDictEqual(expected_result, decoded_row)

    @unpack
    @data((['DTM', ['832', '20191021', '102']], {'test_date': '20191021', 'test_date_format': '%Y%m%d'}),
          (['DTM', ['119', '21102019', '951']], {'test_date': '21102019', 'test_date_format': '%d%m%Y'}),
          (['DTM', ['329', '21102019', '951']], {'date_of_birth': '21102019', 'date_of_birth_format': '%d%m%Y'}))
    def test_decode_date_time_period(self, datetime_row, expected_decoded_row):
        actual_decoded_row = _decode_date_time_period(datetime_row)

        self.assertDictEqual(expected_decoded_row, actual_decoded_row)

    @unpack
    @data((['RAR', ['2', '843'], ['S', '864'], ['834', '36', '912']], {'result_code': '2', 'result': 'Negative',
          'action_code': 'S', 'action': 'Suspended', 'recall_months': '36'}),
          (['RAR', ['Z', '843'], ['Y', '864'], ['834', '36', '912']], {'result_code': 'Z', 'result': 'Unknown',
           'action_code': 'Y', 'action': 'Unknown', 'recall_months': '36'}))
    def test_decode_result_recall_details(self, result_recall_row, expected_decoded_row):
        actual_decoded_row = _decode_result_recall_details(result_recall_row)

        self.assertDictEqual(expected_decoded_row, actual_decoded_row)

    @unpack
    @data((['PCD', ['840', '19035247']], {'slide_number': '19035247'}),
          (['PCD', ['1', '842']], {'infection_code': '1', 'infection_result': 'Trichomonas'}),
          (['PCD', ['Z', '842']], {'infection_code': 'Z', 'infection_result': 'Unknown'}),
          (['PCD', ['0', '842'], '', '', '', 'S'],
           {'infection_code': '0', 'infection_result': 'HPV negative',
            'sender_code': SELF_SAMPLE_SENDER_CODE, 'source_code': 'H',
            'sender_source_type': '6', 'self_sample': True, 'hpv_primary': True}),
          (['PCD', 'Y'], {'hpv_primary': True}),)
    def test_decode_patient_cytology_details(self, cytology_row, expected_decoded_row):
        actual_decoded_row = _decode_patient_cytology_details(cytology_row)

        self.assertDictEqual(expected_decoded_row, actual_decoded_row)

    def test_name_address(self):
        test_data = ['NAD', 'HP', '', 'edith wharton, 12 winchester way, cambridge, CB332L']
        decoded_row = _decode_name_and_address(test_data)

        expected_result = {
            'address': 'edith wharton, 12 winchester way, cambridge, CB332L'
        }
        self.assertDictEqual(expected_result, decoded_row)

    def test_name_address_lab_code(self):
        test_data = ['NAD', 'PTH', ['14', '963']]
        decoded_row = _decode_name_and_address(test_data)

        expected_result = {
            'pathology_lab': '14'
        }
        self.assertDictEqual(expected_result, decoded_row)

    def test_name_address_gp(self):
        test_data = ['NAD', 'GP', ['test_gp', 'ZZZ']]
        decoded_row = _decode_name_and_address(test_data)

        expected_result = {
            'GP': 'test_gp'
        }
        self.assertDictEqual(expected_result, decoded_row)

    def test_patient_name_row(self):
        test_data = ['PNA', 'PAT', ['NHS_NUMBER', 'OPI'], '', '', ['SU', 'DeBonesby'], ['FS', 'Frederick']]
        decoded_row = _decode_person_name(test_data)

        expected_result = {
            'nhs_number': 'NHS_NUMBER',
            'last_name': 'DeBonesby',
            'first_name': 'Frederick'
        }
        self.assertDictEqual(expected_result, decoded_row)

    def test_patient_name_row_former_name(self):
        test_data = ['PNA', 'PER', ['NHS_NUMBER', 'OPI'], '', '', ['SU', 'DeBonesby'], ['FS', 'Frederick']]
        decoded_row = _decode_person_name(test_data)

        expected_result = {
            'previous_nhs_number': 'NHS_NUMBER',
            'previous_last_name': 'DeBonesby',
            'previous_first_name': 'Frederick'
        }
        self.assertDictEqual(expected_result, decoded_row)

    @unpack
    @data((['TST', 'RCD', ['6', 'ZZZ'], ['SNO', 'SLIDE_NUMBER'], ['R', 'ZZZ']],
           {'slide_number': 'SLIDE_NUMBER', 'result_code': '6', 'result': 'Glandular neoplasia of endocervical type',
            'action_code': 'R', 'action': 'Repeat advised'}),
          (['TST', 'RCD', ['Z', 'ZZZ'], ['SNO', 'SLIDE_NUMBER'], ['Z', 'ZZZ']],
           {'slide_number': 'SLIDE_NUMBER', 'result_code': 'Z', 'result': 'Unknown', 'action_code': 'Z',
            'action': 'Unknown'}))
    def test_test_result_row(self, test_result_row, expected_decoded_row):
        decoded_row = _decode_test_result(test_result_row)

        self.assertDictEqual(expected_decoded_row, decoded_row)

    @unpack
    @data((['HEA', 'INM', ['9', 'ZZZ']], {'infection_code': '9', 'infection_result': 'HPV positive'}),
          (['HEA', 'INM', ['Z', 'ZZZ']], {'infection_code': 'Z', 'infection_result': 'Unknown'}),
          (['HEA', 'ETP', ['Y', 'ZZZ']], {'hpv_primary': True}),
          (['HEA', 'ABC', ['9', 'ZZZ']], {}))
    def test_health_information_row(self, health_info_row, expected_decoded_row):
        actual_decoded_row = _decode_health_information(health_info_row)

        self.assertDictEqual(expected_decoded_row, actual_decoded_row)

    def test_quantity_row(self):
        test_data = ['QTY', ['961', '12']]
        decoded_row = _decode_quantity_row(test_data)

        expected_result = {
            'recall_months': '12'
        }
        self.assertDictEqual(expected_result, decoded_row)

    def test_edifact_utils_throws_invalid_edifact_exception_when_header_is_invalid(self):
        with self.assertRaises(InvalidEdifactHeader):
            split_raw_edifact_into_raw_lab_results(INVALID_TYPE_3_WITH_INVALID_RESULT)

    @unpack
    @data(({'address': '67 JENNERT RUE LEIGHTON BUZZARD LU6 5BT'},
           {'address': '67 JENNERT RUE LEIGHTON BUZZARD LU6 5BT', 'sanitised_postcode': 'LU65BT'},
           []),
          ({'address': '67 JENNERT RUE LEIGHTON BUZZARD'},
           {'address': '67 JENNERT RUE LEIGHTON BUZZARD'},
           ['Postcode is missing or invalid']))
    def test_edifact_utils_validates_edifact_postcode_with_postcode_in_addres(self, initial_result,
                                                                              expected_result, expected_errors):
        decoded_result = initial_result
        validation_errors = []
        validate_edifact_postcode(decoded_result, validation_errors)
        self.assertDictEqual(decoded_result, expected_result)
        self.assertListEqual(validation_errors, expected_errors)

    @unpack
    @data(
        ('', '', '', ['']), ("'''", '', '', ["'''"]),
        ("?'", "'", '', ['?', '']), ("'?'", "'", '', ['', '?', '']),
        ("", "'", '?', ['']),
        ("'", "'", '?', ['', '']),
        ("''", "'", '?', ['', '', '']),
        ("?", "'", '?', ['?']),
        ("?'", "'", '?', ["?'"]),
        ("??", "'", '?', ['??']),
        ("??'", "'", '?', ['??', '']),
        ("'?", "'", '?', ['', '?']),
        ("'?'", "'", '?', ['', "?'"]),
        ("'??'", "'", '?', ['', '??', '']),
        ("Where?'s the delimiter?", "'", '', ['Where?', 's the delimiter?']),
        ("Where?'s the delimiter?", "'", '?', ["Where?'s the delimiter?"]),
        ("Where??'s the delimiter?", "'", '?', ['Where??', 's the delimiter?']))
    def test_edifact_utils_escaped_split(self, item, delimiter, escape_char, expected_result):
        split_result = _escaped_split(item, delimiter, escape_char)
        self.assertListEqual(split_result, expected_result)

    @unpack
    @data(
        ("Where?'s Regent?'s Park??", '', "Where?'s Regent?'s Park??"),
        ("Where?'s Regent?'s Park??", '?', "Where's Regent's Park?"),
    )
    def test_edifact_utils_remove_escapes(self, item, escape_char, expected_result):
        split_result = _remove_escapes(item, escape_char)
        self.assertEqual(split_result, expected_result)
