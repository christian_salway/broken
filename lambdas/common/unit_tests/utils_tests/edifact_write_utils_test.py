from datetime import datetime
from ddt import ddt, data, unpack
from unittest import TestCase
from mock import patch
from common.unit_tests.utils_tests.test_data import edifact_test_data


@ddt
class TestEdifactWriteUtils(TestCase):

    @classmethod
    def setUpClass(self):
        import common.utils.edifact_write_utils as _edifact_write_utils
        global edifact_write_utils
        self.edifact_write_utils = _edifact_write_utils

    @patch('common.utils.edifact_write_utils.datetime')
    @patch('common.utils.edifact_write_utils.generate_interchange_number')
    @patch('common.utils.edifact_write_utils.generate_message_number')
    def test_receipt_report_generated_when_results_split_successfully(self,
                                                                      generate_message_number_mock,
                                                                      generate_interchange_number_mock,
                                                                      datetime_mock):
        raw_edifact = edifact_test_data.TYPE_3_WITH_1_RESULT_MESSAGE

        datetime_mock.now.return_value = datetime(2021, 2, 5, 8, 15, 48, 123456)
        datetime_mock.strftime.side_effect = datetime.strftime
        datetime_mock.strptime.side_effect = datetime.strptime

        generate_message_number_mock.return_value = '00000001'
        generate_interchange_number_mock.return_value = '00000001'

        expected_receipt_report = ("UNB+UNOA:2+WIG1+WHI3+20210205:0815+00000001++RECEP+++FHSA/PATH LABTRANSFER'"
                                   "UNH+00000001+RECEP:0:2:FH'"
                                   "BGM++600+243:202102050815:306+64'"
                                   "NHS+LNW:819:GB2+WHI'"
                                   "DTM+815:202102051630:306'"
                                   "RFF+MIS:1 CP'"
                                   "RFF+RIS:00002155 OK:1'"
                                   "UNT+7+00000001'"
                                   "UNZ+1+00000001'")

        receipt_report = self.edifact_write_utils.generate_receipt_report(
            raw_edifact, '2021-02-05 16:30:37+00:00', True)

        self.assertEqual(expected_receipt_report, receipt_report)

    @patch('common.utils.edifact_write_utils.datetime')
    @patch('common.utils.edifact_write_utils.generate_interchange_number')
    @patch('common.utils.edifact_write_utils.generate_message_number')
    def test_receipt_report_generated_when_results_split_fails(self,
                                                               generate_message_number_mock,
                                                               generate_interchange_number_mock,
                                                               datetime_mock):
        raw_edifact = edifact_test_data.TYPE_3_WITH_1_RESULT_MESSAGE

        datetime_mock.now.return_value = datetime(2021, 2, 5, 8, 15, 48, 123456)
        datetime_mock.strftime.side_effect = datetime.strftime
        datetime_mock.strptime.side_effect = datetime.strptime

        generate_message_number_mock.return_value = '00000001'
        generate_interchange_number_mock.return_value = '00000001'

        expected_receipt_report = ("UNB+UNOA:2+WIG1+WHI3+20210205:0815+00000001++RECEP+++FHSA/PATH LABTRANSFER'"
                                   "UNH+00000001+RECEP:0:2:FH'"
                                   "BGM++600+243:202102050815:306+64'"
                                   "NHS+LNW:819:GB2+WHI'"
                                   "DTM+815:202102051630:306'"
                                   "RFF+RIS:00002155 NA'"
                                   "UNT+6+00000001'"
                                   "UNZ+1+00000001'")

        receipt_report = self.edifact_write_utils.generate_receipt_report(
            raw_edifact, '2021-02-05 16:30:37+00:00', False)

        self.assertEqual(expected_receipt_report, receipt_report)

    @patch('common.utils.edifact_write_utils.datetime')
    @patch('common.utils.edifact_write_utils.generate_interchange_number')
    @patch('common.utils.edifact_write_utils.generate_message_number')
    def test_receipt_report_generated_given_multiple_messages_in_interchange(self,
                                                                             generate_message_number_mock,
                                                                             generate_interchange_number_mock,
                                                                             datetime_mock):
        raw_edifact = (
            f"{edifact_test_data.TYPE_3_EDIFACT_INTERCHANGE_HEADER}"
            f"{edifact_test_data.TYPE_3_EDIFACT_MESSAGE_HEADER}"
            f"{edifact_test_data.TYPE_3_LAB_RESULT_1}'"
            f"{edifact_test_data.TYPE_3_EDIFACT_MESSAGE_TRAILER}"
            f"{edifact_test_data.TYPE_3_EDIFACT_MESSAGE_HEADER}"
            f"{edifact_test_data.TYPE_3_LAB_RESULT_2}'"
            f"{edifact_test_data.TYPE_3_EDIFACT_MESSAGE_TRAILER}"
            f"{edifact_test_data.TYPE_3_EDIFACT_INTERCHANGE_TRAILER}"
        )

        datetime_mock.now.return_value = datetime(2021, 2, 5, 8, 15, 48, 123456)
        datetime_mock.strftime.side_effect = datetime.strftime
        datetime_mock.strptime.side_effect = datetime.strptime

        generate_message_number_mock.return_value = '00000001'
        generate_interchange_number_mock.return_value = '00000001'

        expected_receipt_report = ("UNB+UNOA:2+WIG1+WHI3+20210205:0815+00000001++RECEP+++FHSA/PATH LABTRANSFER'"
                                   "UNH+00000001+RECEP:0:2:FH'"
                                   "BGM++600+243:202102050815:306+64'"
                                   "NHS+LNW:819:GB2+WHI'"
                                   "DTM+815:202102051630:306'"
                                   "RFF+RIS:00002155 NA'"
                                   "UNT+6+00000001'"
                                   "UNZ+1+00000001'")

        receipt_report = self.edifact_write_utils.generate_receipt_report(
            raw_edifact, '2021-02-05 16:30:37+00:00', True)

        self.assertEqual(expected_receipt_report, receipt_report)

    @patch('common.utils.edifact_write_utils.datetime')
    @patch('common.utils.edifact_write_utils.generate_interchange_number')
    @patch('common.utils.edifact_write_utils.generate_message_number')
    def test_receipt_report_generated_given_no_messages_in_interchange(self,
                                                                       generate_message_number_mock,
                                                                       generate_interchange_number_mock,
                                                                       datetime_mock):
        raw_edifact = (
            f"{edifact_test_data.TYPE_3_EDIFACT_INTERCHANGE_HEADER}"
            f"{edifact_test_data.TYPE_3_EDIFACT_INTERCHANGE_TRAILER}"
        )

        datetime_mock.now.return_value = datetime(2021, 2, 5, 8, 15, 48, 123456)
        datetime_mock.strftime.side_effect = datetime.strftime
        datetime_mock.strptime.side_effect = datetime.strptime

        generate_message_number_mock.return_value = '00000001'
        generate_interchange_number_mock.return_value = '00000001'

        expected_receipt_report = ("UNB+UNOA:2+WIG1+WHI3+20210205:0815+00000001++RECEP+++FHSA/PATH LABTRANSFER'"
                                   "UNH+00000001+RECEP:0:2:FH'"
                                   "BGM++600+243:202102050815:306+64'"
                                   "NHS+LNW:819:GB2+WHI'"
                                   "DTM+815:202102051630:306'"
                                   "RFF+RIS:00002155 NA'"
                                   "UNT+6+00000001'"
                                   "UNZ+1+00000001'")

        receipt_report = self.edifact_write_utils.generate_receipt_report(
            raw_edifact, '2021-02-05 16:30:37+00:00', True)

        self.assertEqual(expected_receipt_report, receipt_report)

    @unpack
    @data(
        ('WHI3', 'WHI'),
        ('LD01', 'LD')
        )
    def test_recipient_id_parsed_correctly(self, link_code, expected_id):
        actual = self.edifact_write_utils.get_recipient_id(link_code)

        self.assertEqual(actual, expected_id)
