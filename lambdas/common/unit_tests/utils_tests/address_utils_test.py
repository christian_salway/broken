from unittest.case import TestCase

from ddt import data, ddt, unpack

from common.utils.address_utils import destructure_postcode_from_address_list, parse_address_from_decoded_string


@ddt
class TestAddressUtils(TestCase):
    def setUp(self):
        self.maxDiff = None

    @unpack
    @data(
        (
            '67 JENNERT RUE LEIGHTON BUZZARD LU6 5BT',
            {
                'address_line_1': '67 JENNERT RUE LEIGHTON BUZZARD',
                'address_line_2': '',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': 'LU65BT'
            },
        ),
        (
            'STUDIO 103 THE BUSINESS CENTRE 61 WELLFIELD ROAD ROATH CARDIFF CF24 3DG',
            {
                'address_line_1': 'STUDIO 103 THE BUSINESS CENTRE 61',
                'address_line_2': 'WELLFIELD ROAD ROATH CARDIFF',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': 'CF243DG'
            },
        ),
        (
            'Isle of Man Government Office of Human Resources Learning Education and Development The Lodge Education '
            'and Training Centre Braddan Road Strang Douglas ISLE OF MAN IM4 4QN',
            {
                'address_line_1': 'Isle of Man Government Office of',
                'address_line_2': 'Human Resources Learning Education',
                'address_line_3': 'and Development The Lodge Education',
                'address_line_4': 'and Training Centre Braddan Road',
                'address_line_5': 'Strang Douglas ISLE OF MAN',
                'postcode': 'IM44QN'
            },
        ),
        (
            'STUDIO 103,THE BUSINESS CENTRE, 61 WELLFIELD ROAD ROATH CARDIFF CF24 3DG',
            {
                'address_line_1': 'STUDIO 103 THE BUSINESS CENTRE 61',
                'address_line_2': 'WELLFIELD ROAD ROATH CARDIFF',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': 'CF243DG'
            },
        ),
        (
            'Isle of Man Government Office of Human Resources Learning Education and Development The Lodge Education '
            'and Training Centre Braddan Road Strang Douglas ISLE OF MAN Extra Characters removed correctly IM4 4QN',
            {
                'address_line_1': 'Isle of Man Government Office of',
                'address_line_2': 'Human Resources Learning Education',
                'address_line_3': 'and Development The Lodge Education',
                'address_line_4': 'and Training Centre Braddan Road',
                'address_line_5': 'Strang Douglas ISLE OF MAN Extra',
                'postcode': 'IM44QN'
            },
        ),
        (
            '',
            None
        )
    )
    def test_parse_address_from_decoded_string(self, address_string,
                                               expected_address_object):
        address_object = parse_address_from_decoded_string(address_string)
        if expected_address_object is not None:
            self.assertDictEqual(address_object, expected_address_object)
        else:
            self.assertEqual(address_object, expected_address_object)

    @unpack
    @data(
        (
            ['Training', 'Centre', 'Braddan', 'Road', 'Strang', 'Douglas', 'ISLE', 'OF', 'MAN', 'IM4', '4QN'],
            {
                'postcode': 'IM44QN',
                'address_list': ['Training', 'Centre', 'Braddan', 'Road', 'Strang', 'Douglas', 'ISLE', 'OF', 'MAN']
            }
        ),
        (
            ['THE BUSINESS CENTRE', '61 WELLFIELD ROAD', 'ROATH CARDIFF', 'CF24 3DG'],
            {
                'postcode': 'CF243DG',
                'address_list': ['THE BUSINESS CENTRE', '61 WELLFIELD ROAD', 'ROATH CARDIFF']
            }
        ),
        (
            ['THE BUSINESS CENTRE', '61 WELLFIELD ROAD', 'ROATH CARDIFF'],
            {
                'postcode': None,
                'address_list': ['THE BUSINESS CENTRE', '61 WELLFIELD ROAD', 'ROATH CARDIFF']
            }
        ),
        (
            [''],
            {
                'postcode': None,
                'address_list': ['']
            }
        )
    )
    def test_destructure_postcode_from_address_list(self, address_list, expected):
        postcode, address_list = destructure_postcode_from_address_list(address_list)
        self.assertEqual(postcode, expected['postcode'])
        self.assertEqual(address_list, expected['address_list'])
