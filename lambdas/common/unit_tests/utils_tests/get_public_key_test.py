import os
import json
from unittest.mock import call
from jwt.api_jwt import encode
from common.utils.auth.log_references import LogReference
from common.test_mocks.mock_response import MockResponse
from common.test_mocks.test_case import PatchTestCase


class GetPublicKeyTests(PatchTestCase):

    def setUp(self):
        self.module = 'common.utils.auth.get_public_key'

        with open(os.path.join(os.path.dirname(__file__), 'test_data/public_jwks.json')) as file:
            jwks = file.read()

        self.jwks_json = json.loads(jwks)

        with open(os.path.join(os.path.dirname(__file__), 'test_data/private_key.pem')) as file:
            self.private_key = file.read()

        with open(os.path.join(os.path.dirname(__file__), 'test_data/public_key.pem')) as file:
            self.public_key = file.read()

        self.single_jwk_json = self.jwks_json.get('keys')[1]
        self.nhsid_config = {
            'jwks_uri': 'jwks_uri',
            'id_token_signing_alg_values_supported': ['RS256']
        }
        self.audience = 'audience'
        self.jwks_uri = 'jwks_uri'
        self.key = 'key'
        self.token = encode({}, self.private_key, headers={'kid': '123'}, algorithm='RS256')
        self.config = {
            'NHSID_AUD_ID': self.audience}

        self.create_patchers(self.module, [
            'log',
            'create_replace',
            'dynamodb_get_item',
            'dynamodb_put_item',
            'requests'
        ])

        from common.utils.auth.get_public_key import get_public_key
        self.get_public_key = get_public_key

        self.dynamodb_get_item_patcher.return_value = {'key': json.dumps(self.single_jwk_json)}
        self.requests_patcher.get.return_value = MockResponse(self.jwks_json)

    def act(self):
        return self.get_public_key(self.nhsid_config, self.config, self.token)

    def mock_empty_cache(self):
        self.dynamodb_get_item_patcher.return_value = None

    def test_returns_public_jwk_from_cache(self):
        result = self.act()
        # Assert
        self.assertEqual(self.single_jwk_json, result)
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.VETOK0008, 'hash_key': '123_RS256'})])

    def test_returns_none_if_algorithm_not_supported(self):
        # Arrange
        self.nhsid_config = {
            'jwks_uri': 'jwks_uri',
            'id_token_signing_alg_values_supported': ['something_invalid']
        }
        # Act
        result = self.act()
        # Assert
        self.assertIsNone(result)
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.VETOK0001}),
            call({'log_reference': LogReference.VETOK0005, 'nhsid_algorithms': ['something_invalid']})])

    def test_fetches_key_from_nhsid_if_not_in_cache(self):
        # Arrange
        self.mock_empty_cache()
        # Act
        result = self.act()
        # Assert
        self.assertIsNone(result)
        self.requests_patcher.assert_has_calls([call.get('jwks_uri')])
        self.create_replace_patcher.assert_called()
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.VETOK0001}),
            call({'log_reference': LogReference.VETOK0008, 'hash_key': '123_RS256'}),
            call({'log_reference': LogReference.VETOK0006})])

    def test_returns_none_if_kid_missing_from_token_header(self):
        # Arrange
        self.mock_empty_cache()
        self.token = encode({}, self.private_key, algorithm='RS256')
        # Act
        result = self.act()
        # Assert
        self.assertIsNone(result)
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.VETOK0001}),
            call({'log_reference': LogReference.VETOK0007, 'headers': {'typ': 'JWT', 'alg': 'RS256'}})])
