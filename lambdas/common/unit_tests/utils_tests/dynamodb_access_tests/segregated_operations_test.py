from unittest.case import TestCase
from mock import call, patch, Mock
from ddt import ddt, data
from boto3.dynamodb.types import TypeSerializer

from common.utils.dynamodb_access.segregated_operations import (
    segregated_dynamodb_transact_write_items, segregated_query, segregated_put_item,
    segregated_dynamodb_paginated_query, segregated_dynamodb_get_item, segregated_dynamodb_scan,
    segregated_dynamodb_delete_item, segregated_dynamodb_update_item, segregated_dynamodb_batch_write_records,
    segregated_dynamodb_batch_delete_records
)
from common.utils.dynamodb_access.table_names import TableNames

serializer = TypeSerializer()


@ddt
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table')
class TestDynamoDBBoto3(TestCase):

    @patch('common.utils.dynamodb_access.segregated_operations.filter_items')
    def test_dynamodb_query(
        self, filter_items_mock, get_table_mock
    ):
        # Arrange
        items = [{'item': '1'}, {'item': '2'}]
        table_mock = Mock()
        get_table_mock.return_value = table_mock
        table_mock.query.return_value = {'Items': items}
        filter_items_mock.return_value = items
        # Act
        actual = segregated_query(TableNames.PARTICIPANTS, {'test_query_kwarg': 'test_query_value'})

        # Assert
        get_table_mock.called_once_with(TableNames.PARTICIPANTS)
        table_mock.query.assert_called_once_with(test_query_kwarg='test_query_value')
        filter_items_mock.called_once_with(TableNames.PARTICIPANTS, items)
        self.assertEqual(actual, items)

    @patch('common.utils.dynamodb_access.segregated_operations.filter_before_write')
    def test_dynamodb_put_item_for_participants(
        self, filter_before_write_mock, get_table_mock
    ):
        # Arrange
        item = {'participant_id': 'a_participant_id', 'data': 'data'}
        table_mock = Mock()
        get_table_mock.return_value = table_mock
        table_mock.put_item.return_value = {'key': 'value'}

        # Act
        actual = segregated_put_item(TableNames.PARTICIPANTS, item)

        # Assert
        get_table_mock.called_once_with(TableNames.PARTICIPANTS)
        table_mock.put_item.assert_called_once_with(Item=item)
        filter_before_write_mock.assert_called_once_with(
            TableNames.PARTICIPANTS,
            [{'participant_id': 'a_participant_id', 'data': 'data', }]
        )
        self.assertEqual(actual, {'key': 'value'})

    @patch('common.utils.dynamodb_access.segregated_operations.filter_items')
    def test_dynamodb_paginated_query_for_participants(
        self, filter_items_mock, get_table_mock
    ):
        # Arrange
        first_items = [{'item': '1'}, {'item': '2'}]
        second_items = [{'item': '3'}, {'item': '4'}]
        all_items = first_items + second_items
        table_mock = Mock()
        get_table_mock.return_value = table_mock
        table_mock.query.side_effect = [{
            'Items': first_items,
            'LastEvaluatedKey': 'test-key'
        }, {
            'Items': second_items
        }]
        filter_items_mock.return_value = all_items

        # Act
        actual = segregated_dynamodb_paginated_query(TableNames.PARTICIPANTS, {'test_query_kwarg': 'test_query_value'})

        # Assert
        get_table_mock.called_once_with(TableNames.PARTICIPANTS)
        table_mock.query.assert_has_calls([call(**{
            'test_query_kwarg': 'test_query_value',
        }), call(**{
            'test_query_kwarg': 'test_query_value',
            'ExclusiveStartKey': 'test-key'
        })])
        filter_items_mock.assert_called_once_with(TableNames.PARTICIPANTS, all_items)
        self.assertEqual(actual, all_items)

    @data({'item': '1'}, None)
    @patch('common.utils.dynamodb_access.segregated_operations.filter_items')
    def test_dynamodb_get_item_for_participants(
        self, item, filter_items_mock, get_table_mock
    ):
        # Arrange
        table_mock = Mock()
        get_table_mock.return_value = table_mock
        table_mock.get_item.return_value = {'Item': item}
        filter_items_mock.return_value = [item]

        # Act
        actual = segregated_dynamodb_get_item(TableNames.PARTICIPANTS, {'test_query_kwarg': 'test_query_value'})

        # Assert
        get_table_mock.called_once_with(TableNames.PARTICIPANTS)
        table_mock.get_item.assert_called_once_with(Key={'test_query_kwarg': 'test_query_value'})
        if item:
            filter_items_mock.assert_called_once_with(TableNames.PARTICIPANTS, [item])
        else:
            filter_items_mock.assert_not_called()
        self.assertEqual(actual, item)

    @patch('common.utils.dynamodb_access.segregated_operations.filter_items')
    def test_dynamodb_scan_for_participants(
        self, filter_items_mock, get_table_mock
    ):
        # Arrange
        items = [{'item': '1'}, {'item': '2'}]
        table_mock = Mock()
        get_table_mock.return_value = table_mock
        table_mock.scan.return_value = {'Items': items}
        filter_items_mock.return_value = items

        # Act
        actual = segregated_dynamodb_scan(TableNames.PARTICIPANTS, {'test_query_kwarg': 'test_query_value'})

        # Assert
        get_table_mock.called_once_with(TableNames.PARTICIPANTS)
        table_mock.scan.assert_called_once_with(test_query_kwarg='test_query_value')
        filter_items_mock.assert_called_once_with(TableNames.PARTICIPANTS, items)
        self.assertEqual(actual, items)

    @patch('common.utils.dynamodb_access.segregated_operations.filter_before_write')
    def test_dynamodb_delete_item_for_participants(
        self, filter_before_write_mock, get_table_mock
    ):
        # Arrange
        key = {'participant_id': 'a_participant_id'}
        table_mock = Mock()
        get_table_mock.return_value = table_mock
        table_mock.delete_item

        # Act
        segregated_dynamodb_delete_item(TableNames.PARTICIPANTS, key)

        # Assert
        get_table_mock.called_once_with(TableNames.PARTICIPANTS)
        table_mock.delete_item.assert_called_once_with(Key=key)
        filter_before_write_mock.assert_called_once_with(TableNames.PARTICIPANTS, [key])

    @patch('common.utils.dynamodb_access.segregated_operations.filter_before_write')
    def test_dynamodb_update_item_for_participants(
        self, filter_before_write_mock, get_table_mock
    ):
        # Arrange
        key = {'participant_id': 'a_participant_id'}
        table_mock = Mock()
        get_table_mock.return_value = table_mock
        table_mock.update_item.return_value = {'key': 'value'}
        update_dict = {
            'Key': key,
            'UpdateExpression': 'a = 1'
        }

        # Act
        actual = segregated_dynamodb_update_item(TableNames.PARTICIPANTS, update_dict)

        # Assert
        get_table_mock.called_once_with(TableNames.PARTICIPANTS)
        table_mock.update_item.assert_called_once_with(Key=key, UpdateExpression='a = 1')
        filter_before_write_mock.assert_called_once_with(TableNames.PARTICIPANTS, [key])
        self.assertEqual(actual, {'key': 'value'})

    @patch('common.utils.dynamodb_access.segregated_operations.filter_before_write')
    def test_dynamodb_batch_write_item(
        self, filter_before_write_mock, get_table_mock
    ):
        # Arrange
        item = {'participant_id': 'a_participant_id', 'data': 'data'}
        batch_writer_mock = Mock()
        table_mock = Mock()
        context_manager_mock = Mock()
        context_manager_mock.__enter__ = Mock(return_value=batch_writer_mock)
        context_manager_mock.__exit__ = Mock()
        table_mock.batch_writer.return_value = context_manager_mock
        get_table_mock.return_value = table_mock

        # Act
        segregated_dynamodb_batch_write_records(TableNames.PARTICIPANTS, [item])

        # Assert
        get_table_mock.called_once_with(TableNames.PARTICIPANTS)
        table_mock.batch_writer.assert_called_once()
        filter_before_write_mock.assert_called_once_with(TableNames.PARTICIPANTS, [item])
        batch_writer_mock.put_item.assert_called_once_with(Item=item)

    @patch('common.utils.dynamodb_access.segregated_operations.filter_before_write')
    def test_dynamodb_batch_delete_item(
        self, filter_before_write_mock, get_table_mock
    ):
        # Arrange
        key = {'participant_id': 'a_participant_id'}
        table_mock = Mock()
        get_table_mock.return_value = table_mock
        batch_writer_mock = Mock()
        table_mock = Mock()
        context_manager_mock = Mock()
        context_manager_mock.__enter__ = Mock(return_value=batch_writer_mock)
        context_manager_mock.__exit__ = Mock()
        table_mock.batch_writer.return_value = context_manager_mock
        get_table_mock.return_value = table_mock

        # Act
        segregated_dynamodb_batch_delete_records(TableNames.PARTICIPANTS, [key])

        # Assert
        get_table_mock.called_once_with(TableNames.PARTICIPANTS)
        filter_before_write_mock.assert_called_once_with(TableNames.PARTICIPANTS, [key])
        batch_writer_mock.delete_item.assert_called_once_with(Key=key)

    @patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'test-participants', 'DYNAMODB_RESULTS': 'test-results'})
    @patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_client')
    @patch('common.utils.dynamodb_access.segregated_operations.filter_before_write')
    def test_dynamodb_transact_write_items(
        self, filter_before_write_mock, get_client_mock, get_table_mock
    ):
        # Arrange
        item = {'participant_id': 'a_participant_id'}
        table_mock = Mock()
        get_table_mock.return_value = table_mock

        client_mock = Mock()
        get_client_mock.return_value = client_mock

        transaction = [
            {
                "Put": {
                    "TableName": 'test-participants',
                    "Item": {k: serializer.serialize(v) for k, v in item.items()}
                }
            },
            {
                "Update": {
                    "TableName": 'test-participants',
                    "Key": {k: serializer.serialize(v) for k, v in item.items()}
                }
            },
            {
                "Delete": {
                    "TableName": 'test-participants',
                    "Key": {k: serializer.serialize(v) for k, v in item.items()}
                }
            },
            {
                "Delete": {
                    "TableName": 'test-results',
                    "Key": {k: serializer.serialize(v) for k, v in item.items()}
                }
            }
        ]

        # Act
        segregated_dynamodb_transact_write_items(transaction)

        # Assert
        get_table_mock.called_once_with(TableNames.PARTICIPANTS)
        client_mock.transact_write_items.assert_called_once_with(TransactItems=transaction)
        filter_before_write_mock.assert_has_calls([
            call(TableNames.PARTICIPANTS, [item, item, item]),
            call(TableNames.RESULTS, [item])
        ])
        self.assertEqual(filter_before_write_mock.call_count, 2)
