from unittest.case import TestCase
from mock import patch, Mock

from common.utils.dynamodb_access.dynamodb_boto3 import get_dynamodb_table
from common.utils.dynamodb_access.table_names import TableNames


@patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'test-table'})
class TestDynamoDBBoto3(TestCase):

    @patch('common.utils.dynamodb_access.dynamodb_boto3.get_dynamodb_resource')
    def test_get_dynamodb_table_caches_table_references(self, get_resource_mock):
        resource_mock = Mock()
        resource_mock.Table.return_value = 'test_table_reference'
        get_resource_mock.return_value = resource_mock

        table = get_dynamodb_table(TableNames.PARTICIPANTS)

        self.assertEqual(table, 'test_table_reference')
        get_resource_mock.assert_called_once_with()
        resource_mock.Table.assert_called_once_with('test-table')

        get_resource_mock.reset_mock()

        cached_table = get_dynamodb_table(TableNames.PARTICIPANTS)

        self.assertEqual(cached_table, table)
        get_resource_mock.assert_not_called()
        resource_mock.Table.assert_not_called()
