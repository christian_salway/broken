from unittest.case import TestCase
from mock import call, patch, Mock

from common.utils.dynamodb_access import operations


class TestDynamoDBClientOperations(TestCase):
    @patch('common.utils.dynamodb_access.operations.get_dynamodb_client')
    def test_list_tables(self, get_dynamodb_client_mock):
        # Arrange
        list_tables_mock = Mock()
        list_tables_mock.list_tables.side_effect = [
            {
                'TableNames': [
                    'test-table-1',
                    'test-table-2'
                ],
                'LastEvaluatedTableName': 'test-table-2'
            },
            {
                'TableNames': [
                    'test-table-3',
                    'another-env-test-table-4'
                ],
            }
        ]

        get_dynamodb_client_mock.return_value = list_tables_mock
        # Act
        tables = operations.list_dynamodb_tables()
        # Assert
        self.assertEqual(tables, ['test-table-1', 'test-table-2', 'test-table-3', 'another-env-test-table-4'])
        list_tables_mock.list_tables.assert_has_calls([call(), call(ExclusiveStartTableName='test-table-2')])
