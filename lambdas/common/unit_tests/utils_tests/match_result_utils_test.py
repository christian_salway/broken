import unittest
import os
import json
from copy import deepcopy
from datetime import datetime, timezone
from dateutil.relativedelta import relativedelta
from mock import patch, Mock, call
from ddt import ddt, data, unpack

from common.models.result import ResultRejectionReason, ResultWorkflowState
from common.models.participant import ParticipantStatus
from common.utils.dynamodb_access.table_names import TableNames
from common.log_references import CommonLogReference


decoded_record = {
    'nhs_number': '',
    'slide_number': '19035247',
    'first_name': '',
    'last_name': '',
    'previous_last_name': '',
    'address': 'AAAAAAAAA A AAAA AAAAA AAAA',
    'source_code': '7',
    'sender_code': '',
    'infection_code': 'U',
    'date_of_birth': '12121982',
    'date_of_birth_format': '%d%m%Y',
    'test_date': '20191021',
    'test_date_format': '%Y%m%d',
    'recall_months': '00',
    'action_code': 'R',
    'action': 'Routine',
    'result_code': 'X',
    'result': 'Negative',
    'title': ''
}

query_results = {'Items': [{}]}


@ddt
@patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS', 'DYNAMODB_RESULTS': 'results-table'})
class TestMatchResultUtils(unittest.TestCase):

    @patch('boto3.resource', Mock())
    def setUp(self):
        global AuditUsers, AuditActions
        from common.utils.audit_utils import AuditUsers, AuditActions

        global match_result_utils_module
        from common.utils import match_result_utils as match_result_utils_module

    @patch('common.utils.match_result_utils.query_duplicate_results')
    @patch('common.utils.match_result_utils.query_results_by_participant_id')
    @patch('common.utils.match_result_utils.get_participant_and_test_results')
    def test_attempt_to_validate_matched_result_no_previous_results(self,
                                                                    get_participant_and_test_results_mock,
                                                                    query_results_by_participant_id_mock,
                                                                    query_duplicate_results_mock):
        # Given
        query_results_by_participant_id_mock.return_value = []
        query_duplicate_results_mock.return_value = []
        get_participant_and_test_results_mock.return_value = []
        participant_id = 'test'
        record = {
            'decoded': decoded_record,
            'result_id': '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1',
            'sending_lab': 'LAB1'
        }

        expected_response = None

        # When
        actual_response = match_result_utils_module.validate_matched_result(record,
                                                                            participant_id)
        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils.query_results_by_participant_id')
    def test_get_latest_result_sorted_by_latest_no_history(
            self, query_results_by_participant_id_mock):
        # Given
        query_results_by_participant_id_mock.return_value = []
        participant_id = 'test'
        expected_response = None

        # When
        actual_response = match_result_utils_module.get_latest_result_sorted_by_latest(participant_id)

        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils.query_results_by_participant_id')
    def test_get_latest_result_sorted_by_latest_sorts_properly(
            self, query_results_by_participant_id_mock):
        # Given
        query_results_by_participant_id_mock.return_value = [
            {'test_date': '2020-01-01'},
            {'test_date': '2020-01-02'}
        ]

        participant_id = 'test'
        expected_response = [{'test_date': '2020-01-02'}, {'test_date': '2020-01-01'}]

        # When
        actual_response = match_result_utils_module.get_latest_result_sorted_by_latest(participant_id)

        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils._participant_ceased_with_reason_death_before_test_date')
    @patch('common.utils.match_result_utils._invalid_return_to_routine_recall')
    @patch('common.utils.match_result_utils._second_consecutive_inadequate_colposcopy_result')
    @patch('common.utils.match_result_utils.query_results_by_participant_id')
    @patch('common.utils.match_result_utils.get_participant_and_test_results')
    def test_all_validation_methods_are_not_run_when_validation_error_found(
            self,
            get_participant_and_test_results_mock,
            query_results_by_participant_id_mock,
            second_inad_colp_res_mock,
            invalid_return_to_routine_mock,
            participant_ceased_with_reason_death_before_test_date_mock):
        # Given
        query_results_by_participant_id_mock.return_value = [{'decoded': {}}]
        second_inad_colp_res_mock.return_value = "validation_error"
        get_participant_and_test_results_mock.return_value = []

        participant_id = 'test'
        decoded_record = {}
        # expected_response = [{}]

        # When
        match_result_utils_module.validate_matched_result({'decoded': decoded_record}, participant_id)

        second_inad_colp_res_mock.assert_called()
        invalid_return_to_routine_mock.assert_not_called()
        participant_ceased_with_reason_death_before_test_date_mock.assert_not_called()
        # Then
        # self.assertEqual(expected_response, actual_response)

    @unpack
    @data(
        (
            {'result_code': '6', 'infection_code': '2', 'action_code': 'R'},
            [{'result_code': '6', 'infection_code': '2', 'action_code': '2'}],
            None
        ),
        (
            {'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'},
            [{'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'}],
            '2nd (inad) Colp Res'
        ),
        (
            {'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'},
            [{'result_code': '1', 'infection_code': '9', 'action_code': 'R'}],
            '2nd (inad) Colp Res'
        ),
        (
            {'result_code': '1', 'infection_code': '9', 'action_code': 'R'},
            [{'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'}],
            '2nd (inad) Colp Res'
        ),
        (
            {'result_code': '1', 'infection_code': '9', 'action_code': 'R'},
            [{'result_code': '1', 'infection_code': '9', 'action_code': 'R'}],
            '2nd (inad) Colp Res'
        ),
        (
            {'result_code': '6', 'infection_code': '2', 'action_code': 'R'},
            [{'result_code': '1', 'infection_code': '9', 'action_code': 'R'}],
            None
        ),
        (
            {'result_code': '1', 'infection_code': '9', 'action_code': 'R'},
            [{'result_code': '6', 'infection_code': '2', 'action_code': '1'}],
            None
        ),
        (
            {'result_code': '6', 'infection_code': '2', 'action_code': 'R'},
            [{'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'}],
            None
        ),
        (
            {'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'},
            [{'result_code': '6', 'infection_code': '2', 'action_code': '1'}],
            None
        )
    )
    def test_second_consecutive_inadequate_colposcopy_result(
            self, decoded_record, latest_result, expected_response):
        # Given

        # When
        actual_response = match_result_utils_module._second_consecutive_inadequate_colposcopy_result(
            decoded_record, latest_result)

        # Then
        self.assertEqual(expected_response, actual_response)

    @unpack
    @data(
        # X0A code after 3 consecutive 29R codes - OK
        (
            {'result_code': 'X', 'infection_code': '0', 'action_code': 'A'},
            [
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            ],
            None
        ),
        # 29R code after 2 consecutive 29R codes - REJECTION
        (
            {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            ],
            ResultRejectionReason.THIRD_POSITIVE_HPV_RESULT
        ),
        # 09R code after 2 consecutive 09R codes - REJECTION
        (
            {'result_code': '0', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': '0', 'infection_code': '9', 'action_code': 'R'},
                {'result_code': '0', 'infection_code': '9', 'action_code': 'R'},
            ],
            ResultRejectionReason.THIRD_POSITIVE_HPV_RESULT
        ),
        # 29R code after 2 non-consecutive 29R codes - OK
        (
            {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
                {'result_code': 'X', 'infection_code': '0', 'action_code': 'A'},
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            ],
            None
        ),
        # 29R code after 2 consecutive 09R codes - REJECTION
        (
            {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': '0', 'infection_code': '9', 'action_code': 'R'},
                {'result_code': '0', 'infection_code': '9', 'action_code': 'R'},
            ],
            ResultRejectionReason.THIRD_POSITIVE_HPV_RESULT
        ),
        # X0A code after 2 consecutive 29R codes with inadequate results between them - OK
        (
            {'result_code': 'X', 'infection_code': '0', 'action_code': 'A'},
            [
                {'result_code': '1', 'infection_code': '9', 'action_code': 'R'},  # inad.
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
                {'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'},  # inad.
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            ],
            None
        ),
        # 29R code after 2 consecutive 29R codes with inadequate results between them - REJECTION
        (
            {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'},  # inad.
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
                {'result_code': '1', 'infection_code': '9', 'action_code': 'R'},  # inad.
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            ],
            ResultRejectionReason.THIRD_POSITIVE_HPV_RESULT
        ),
        # Mixture: 09R code after a 29R and 09R with inadequate results between them - REJECTION
        (
            {'result_code': '0', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'},  # inad.
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
                {'result_code': '1', 'infection_code': '9', 'action_code': 'R'},  # inad.
                {'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'},  # inad.
                {'result_code': '0', 'infection_code': '9', 'action_code': 'R'},
            ],
            ResultRejectionReason.THIRD_POSITIVE_HPV_RESULT
        ),
        # 29R code after 2 results but only one 29R positive HPV result - OK
        (
            {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': 'X', 'infection_code': 'U', 'action_code': 'R'},  # inad.
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            ],
            None
        ),
        # 09R code after 2 results but only one 09R positive HPV result - OK
        (
            {'result_code': '0', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': '1', 'infection_code': '9', 'action_code': 'R'},  # inad.
                {'result_code': '0', 'infection_code': '9', 'action_code': 'R'},
            ],
            None
        ),
        # 29R code but only one previous result - OK
        (
            {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': '2', 'infection_code': '9', 'action_code': 'R'},
            ],
            None
        ),
        # 09R code but no previous results - OK
        (
            {'result_code': '0', 'infection_code': '9', 'action_code': 'R'},
            None,
            None
        ),
        # No result to validate - OK
        (
            None,
            None,
            None
        ),
    )
    def test_third_positive_HPV_result(
            self, decoded_record, latest_result, expected_response):

        actual_response = match_result_utils_module._third_positive_HPV_result(
            decoded_record, latest_result)

        self.assertEqual(expected_response, actual_response)

    @unpack
    @data(
        (
            {'result_code': '1', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': '6', 'infection_code': '2', 'action_code': '1'},
                {'result_code': '6', 'infection_code': '2', 'action_code': '1'}
            ],
            None
        ),
        (
            {'result_code': '1', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': '6', 'infection_code': '2', 'action_code': 'S'},
                {'result_code': '6', 'infection_code': '2', 'action_code': 'R'}
            ],
            None
        ),
        (
            {'result_code': '1', 'infection_code': '9', 'action_code': 'R'},
            [
                {'result_code': '6', 'infection_code': '2', 'action_code': 'S'},
                {'result_code': '6', 'infection_code': '2', 'action_code': 'S'}
            ],
            None
        ),
        (
            {'result_code': '1', 'infection_code': '9', 'action_code': 'S'},
            [
                {'result_code': '6', 'infection_code': '2', 'action_code': 'S'},
                {'result_code': '6', 'infection_code': '2', 'action_code': 'S'}
            ],
            'Invalid return to routine recall'
        ),

    )
    def test_invalid_return_to_routine_recall(self, decoded_record, latest_two_results, expected_response):
        # Given

        # When
        actual_response = match_result_utils_module._invalid_return_to_routine_recall(
            decoded_record, latest_two_results)

        # Then
        self.assertEqual(expected_response, actual_response)

    @unpack
    @data(([{
        'participant_id': 'test',
        'nhs_number': '1',
        'sort_key': 'PARTICIPANT',
        'is_ceased': False
    }], False),
        ([{
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'PARTICIPANT',
            'is_ceased': True
        }, {
            'participant_id': 'test',
            'sort_key': 'CEASE#2020-01-01',
            'reason': 'DUE_TO_AGE',
            'date_from': '2020-01-01T12:15:08.488670+00:00',
        }], True),
        ([{
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'PARTICIPANT',
            'is_ceased': True
        }, {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2020-01-01',
            'reason': 'NOT AGE RELATED',
            'date_from': '2020-01-01T12:15:08.488670+00:00',
        }], False),
        ([{
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'PARTICIPANT',
            'is_ceased': True
        },
            {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2020-11-11',
            'reason': 'DUE_TO_AGE',
            'date_from': '2020-11-11T12:15:08.488670+00:00',
        },
            {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2020-01-01',
            'reason': 'NOT AGE RELATED',
            'date_from': '2020-01-01T12:15:08.488670+00:00',
        }], True),
        ([{
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'PARTICIPANT',
            'is_ceased': True
        },
            {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2021-02-01',
            'reason': 'MENTAL_CAPACITY_ACT',
            'date_from': '2020-11-11T09:24:31.488670+00:00',
        },
            {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2020-01-01',
            'reason': 'NOT AGE RELATED',
            'date_from': '2020-01-01T12:15:08.488670+00:00',
        }], True),
        ([{
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'PARTICIPANT',
            'is_ceased': True
        },
            {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2021-03-01',
            'reason': 'MENTAL_CAPACITY_ACT',
            'date_from': '2021-03-01T09:24:31.488670+00:00',
        },
            {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2020-01-01',
            'reason': 'NOT AGE RELATED',
            'date_from': '2020-01-01T12:15:08.488670+00:00',
        }], False),
        ([{
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'PARTICIPANT',
            'is_ceased': True
        },
            {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2020-11-11',
            'reason': 'PATIENT_INFORMED_CHOICE',
            'date_from': '2020-11-11T09:24:31.488670+00:00',
        },
            {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2020-01-01',
            'reason': 'NOT AGE RELATED',
            'date_from': '2020-01-01T12:15:08.488670+00:00',
        }], True),
        ([{
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'PARTICIPANT',
            'is_ceased': True
        },
            {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2020-11-11',
            'reason': 'PATIENT_INFORMED_CHOICE',
            'date_from': '2021-03-01T09:24:31.488670+00:00',
        },
            {
            'participant_id': 'test',
            'nhs_number': '1',
            'sort_key': 'CEASE#2020-01-01',
            'reason': 'NOT AGE RELATED',
            'date_from': '2020-01-01T12:15:08.488670+00:00',
        }], False))
    @patch('common.utils.match_result_utils.update_participant_record', Mock())
    @patch('common.utils.match_result_utils.create_replace_record_in_participant_table', Mock())
    @patch('common.utils.match_result_utils.audit')
    @patch('common.utils.match_result_utils.calculate_next_test_due_date_and_status_for_ceased_participant')
    @patch('common.utils.match_result_utils.get_participant_and_test_results')
    def test_reinstate_participant(
            self, participant_and_test_results, expected_response,
            get_participant_and_test_results_mock,
            calculate_next_test_due_date_and_status_for_ceased_participant_mock,
            audit_mock):
        # Arrange
        get_participant_and_test_results_mock.return_value = participant_and_test_results
        calculate_next_test_due_date_and_status_for_ceased_participant_mock.return_value = \
            {'status': 'test', 'next_test_due_date': '2023-01-02', 'explanation': 'test123'}
        # Act
        actual_response = match_result_utils_module.reinstate_participant('test', '2021-02-01')
        # Assert
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils.audit')
    def test_generate_reinstate_participant_record_creates_correct_record(
            self, audit_mock):
        # Arrange
        participant_id = 'test_participant_id'
        today_date = datetime.now(timezone.utc).date().isoformat()

        reinstate_record = {
            'participant_id': participant_id,
            'sort_key': f'REINSTATE#{today_date}',
            'date_from': today_date,
            'user_id': AuditUsers.MATCH_RESULT.value
        }
        # Act
        actual_response = match_result_utils_module.generate_reinstate_participant_record(participant_id)

        # Assert
        self.assertEqual(reinstate_record, actual_response)

    @patch('common.utils.match_result_utils.datetime')
    @patch('common.utils.match_result_utils._check_if_participant_should_be_reinstated')
    def test_generate_reinstate_participant_updates_creates_expected_transactions(
            self, check_should_be_reinstated_mock, datetime_mock):
        # Arrange
        datetime_mock.now.return_value = datetime(2021, 4, 15, 12, 15, 8, 132000, tzinfo=timezone.utc)
        participant_id = '1234'
        test_date = '2021-06-06'
        expected_update = {
            "Update": {
                "TableName": "PARTICIPANTS",
                "Key": {
                    "participant_id": {"S": "1234"},
                    "sort_key": {"S": "PARTICIPANT"}
                },
                "UpdateExpression": "SET #is_ceased = :is_ceased, #next_test_due_date = :next_test_due_date, " +
                                    "#status = :status",
                "ExpressionAttributeValues": {
                    ":is_ceased": {"BOOL": False},
                    ":next_test_due_date": {"S": "2021-06-06"},
                    ":status": {"S": "some status"}
                },
                "ExpressionAttributeNames": {
                    "#is_ceased": "is_ceased",
                    "#next_test_due_date": "next_test_due_date",
                    "#status": "status"
                }
            }
        }
        expected_put = {
            "Put": {
                "TableName": "PARTICIPANTS",
                "Item": {
                    "participant_id": {"S": "1234"},
                    "sort_key": {"S": "REINSTATE#2021-04-15"},
                    "date_from": {"S": "2021-04-15"},
                    "user_id": {"S": "MATCH_RESULT"}
                }
            }
        }
        check_should_be_reinstated_mock.return_value = True, participant_id, {
            'status': 'some status',
            'next_test_due_date': '2021-06-06'
        }

        # Act
        participant_update, reinstate_put, next_test_due_date = \
            match_result_utils_module.generate_reinstate_participant_transactions(
                participant_id, TableNames.PARTICIPANTS, test_date)

        # Assert
        self.assertEqual(expected_update, participant_update)
        self.assertEqual(expected_put, reinstate_put)
        self.assertEqual('2021-06-06', next_test_due_date)

    @patch('common.utils.match_result_utils.calculate_ntdd')
    def test_generate_ntdd_transaction_creates_expected_transactions(self, calculate_mock):
        # Arrange
        decoded_result = {
            'test_date': '2021-09-09',
            'recall_months': '2'
        }

        matched_participant = {
            'participant_id': '456'
        }

        expected_update_transaction = {
            'Update': {
                'TableName': 'PARTICIPANTS',
                'Key': {
                    'participant_id': {'S': '456'},
                    'sort_key': {'S': 'PARTICIPANT'}
                },
                'UpdateExpression': 'SET #next_test_due_date = :next_test_due_date, #status = :status',
                'ExpressionAttributeValues': {
                    ':next_test_due_date': {'S': '2021-11-09'},
                    ':status': {'S': 'OPEN'}
                },
                'ExpressionAttributeNames': {
                    '#next_test_due_date': 'next_test_due_date',
                    '#status': 'status'
                }
            }
        }
        expected_next_test_due_date = '2021-11-09'
        calculate_mock.return_value = datetime.strptime(expected_next_test_due_date, '%Y-%m-%d').date()

        # Act
        actual_update_transaction, actual_next_test_due_date = match_result_utils_module.generate_ntdd_transaction(
            decoded_result,
            matched_participant,
            'OPEN',
            TableNames.PARTICIPANTS
        )

        # Assert
        self.assertEqual(expected_update_transaction, actual_update_transaction)
        self.assertEqual(expected_next_test_due_date, str(actual_next_test_due_date))

    @unpack
    @data(
        (
            {'date_of_death': '2020-12-03', 'is_ceased': True, 'status': 'Ceased'},
            {'test_date': '2020-12-02'},
            None
        ),
        (
            {'date_of_death': '2020-12-03', 'is_ceased': True, 'status': 'Ceased'},
            {'test_date': '2020-12-05'},
            ResultRejectionReason.REMOVED_DEATH
        ),
        (
            {'date_of_death': '2020-12-01', 'reason_for_removal_code': 'DEA', 'status': 'Removed'},
            {'test_date': '2020-12-02'},
            None
        ),
        (
            {'date_of_death': '2020-12-03', 'is_ceased': True, 'status': 'Ceased'},
            {'test_date': '2020-12-02'},
            None
        ),
        (
            None,
            {'test_date': '2020-12-02'},
            None
        ),
        (
            None,
            {'test_date': '2020-12-02'},
            None
        ),
        (
            {'date_of_death': '2020-12-01', 'is_ceased': True, 'status': 'Ceased'},
            {'test_date': None},
            None
        ),
        (
            {'date_of_death': None, 'is_ceased': True, 'status': 'Ceased'},
            {'test_date': '2020-12-02'},
            None
        )
    )
    def test_participant_ceased_with_reason_death_before_test_date(
            self, participant_record, decoded_record, expected_response):
        # Given
        # No need to mock as we are passing the participant record data above.

        # When
        actual_response = match_result_utils_module._participant_ceased_with_reason_death_before_test_date(
            decoded_record, participant_record)

        # Then
        self.assertEqual(expected_response, actual_response)

    @unpack
    @data(
        # Test date before the cease date
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      True
            },
            [{
                'participant_id': 'test',
                'sort_key':       'CEASE#2020-01-01',
                'reason':         'NO_CERVIX',
                'date_from':      '2020-01-02T12:15:08.132',
            }],
            None
        ),
        # Test date on the cease date
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      True
            },
            [{
                'participant_id': 'test',
                'sort_key':       'CEASE#2020-01-01',
                'reason':         'NO_CERVIX',
                'date_from':      '2020-01-01T12:15:08.132',
            }],
            None
        ),
        # Test date after the cease date
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      True
            },
            [{
                'participant_id': 'test',
                'sort_key':       'CEASE#2020-01-01',
                'reason':         'NO_CERVIX',
                'date_from':      '2019-12-31T12:15:08.132',
            }],
            ResultRejectionReason.CEASED_NO_CERVIX
        ),
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      None
            },
            [{
                'participant_id': 'test',
                'sort_key':       'CEASE#2020-01-01',
                'reason':         'NO_CERVIX',
                'date_from':      '2020-01-02T12:15:08.132',
            }],
            None
        ),
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      True
            },
            [{
                'participant_id': 'test',
                'sort_key':       'CEASE#2020-01-01',
                'reason':         None,
                'date_from':      '2020-01-02T12:15:08.132',
            }],
            None
        ),
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      True
            },
            None,
            None
        ),
        (
            None,
            None,
            None
        )
    )
    def test_participant_ceased_with_reason_no_cervix(self,
                                                      participant_record,
                                                      ceased_records,
                                                      expected_response):
        # Given
        decoded_record = {
            'test_date': '2020-01-01'
        }

        # When
        actual_response = match_result_utils_module._participant_ceased_with_reason_no_cervix(
            decoded_record, participant_record, ceased_records)

        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils.log', Mock())
    @patch('common.utils.match_result_utils.update_participant_record')
    def test_calculate_and_update_ntdd(self, update_participant_record_mock):
        current_path = os.path.dirname(os.path.realpath(__file__))
        file_name = 'example_record_1.json'
        full_file_path = os.path.join(current_path, 'test_data', file_name)
        with open(full_file_path) as f:
            decoded_record = json.loads(f.read())['decoded']

        matched_participant = {
            'participant_id': '37f4524d-e59f-41d3-9071-97eec87955c7'
        }
        key = {
            'participant_id': matched_participant['participant_id'],
            'sort_key': 'PARTICIPANT'
        }

        for recall_months, next_test_due_date in [('00', '2022-10-21'), ('06', '2022-10-21'), ('12', '2022-10-21')]:
            decoded_record['recall_months'] = recall_months

            attributes_to_update = {
                'next_test_due_date': next_test_due_date,
                'status': ParticipantStatus.ROUTINE
            }

            match_result_utils_module.calculate_and_update_ntdd(
                decoded_record, matched_participant, ParticipantStatus.ROUTINE)

            update_participant_record_mock.assert_called_with(key, attributes_to_update)

    @patch('common.utils.match_result_utils.update_participant_record')
    def test_calculate_and_update_ntdd_does_not_update_if_unchanged_status(self, update_participant_record_mock):
        decoded_record = get_result_record_from_file('example_record_1.json')['decoded']

        matched_participant = {
            'participant_id': '37f4524d-e59f-41d3-9071-97eec87955c7'
        }

        match_result_utils_module.calculate_and_update_ntdd(decoded_record, matched_participant, 'Unchanged')

        update_participant_record_mock.assert_not_called()

    @patch('common.utils.match_result_utils.update_participant_record')
    def test_calculate_and_update_ntdd_does_not_update_if_no_status(self, update_participant_record_mock):
        decoded_record = get_result_record_from_file('example_record_1.json')['decoded']

        matched_participant = {
            'participant_id': '37f4524d-e59f-41d3-9071-97eec87955c7'
        }

        match_result_utils_module.calculate_and_update_ntdd(decoded_record, matched_participant, None)

        update_participant_record_mock.assert_not_called()

    @patch('common.utils.match_result_utils.query_duplicate_results')
    def test_no_duplicate_found_on_identify_duplicate_slide_numbers(self,
                                                                    query_duplicate_results_mock):
        # Given
        new_record = get_result_record_from_file('example_record_1.json')
        expected_response = deepcopy(new_record)

        query_duplicate_results_mock.return_value = []

        # When
        actual_response = match_result_utils_module.identify_duplicate_slide_numbers(new_record)
        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils.update_result_record')
    @patch('common.utils.match_result_utils.query_duplicate_results')
    def test_duplicate_slide_number_found_on_identify_duplicate_slide_numbers(
            self,
            query_duplicate_results_mock, update_result_mock):
        # Given
        new_record = get_result_record_from_file('example_record_1.json')
        duplicate_items = [get_result_record_from_file('result_record_duplicate_slide_number.json')]
        query_duplicate_results_mock.return_value = duplicate_items

        expected_response = deepcopy(new_record)
        expected_response['duplicates'] = [{
            'result_id': '540407ef-2578-4cf2-82c8-0ab735330265', 'received_time': '2020-01030T09:52:12.345'}]

        # When
        actual_response = match_result_utils_module.identify_duplicate_slide_numbers(new_record)
        # Then
        self.assertEqual(expected_response, actual_response)
        update_result_mock.assert_called_once_with(
            {'result_id': '540407ef-2578-4cf2-82c8-0ab735330267', 'received_time': '2020-01030T09:52:12.345'},
            {
                'workflow_state': ResultWorkflowState.REJECTED,
                'rejected_reason': 'Slide ref duplicate',
                'duplicates': [
                    {'result_id': '540407ef-2578-4cf2-82c8-0ab735330265', 'received_time': '2020-01030T09:52:12.345'}
                ]
            }
        )

    @patch('common.utils.match_result_utils.update_result_record')
    @patch('common.utils.match_result_utils.query_duplicate_results')
    def test_duplicate_result_id_not_added_to_duplicate_list_when_already_in_duplicate_list(
            self,
            query_duplicate_results_mock, update_result_mock):
        # Given
        new_record = get_result_record_from_file('example_record_1.json')
        new_record['duplicates'] = [{'result_id': '540407ef-2578-4cf2-82c8-0ab735330265'}]
        expected_response = deepcopy(new_record)
        duplicate_items = [get_result_record_from_file('result_record_duplicate_slide_number.json')]
        query_duplicate_results_mock.return_value = duplicate_items

        # When
        actual_response = match_result_utils_module.identify_duplicate_slide_numbers(new_record)
        # Then
        self.assertEqual(expected_response, actual_response)
        update_result_mock.assert_called_once_with(
            {'result_id': '540407ef-2578-4cf2-82c8-0ab735330267', 'received_time': '2020-01030T09:52:12.345'},
            {
                'workflow_state': ResultWorkflowState.REJECTED,
                'rejected_reason': 'Slide ref duplicate',
                'duplicates': [{'result_id': '540407ef-2578-4cf2-82c8-0ab735330265'}]
            }
        )

    @patch('common.utils.match_result_utils.update_result_record')
    @patch('common.utils.match_result_utils.query_duplicate_results')
    def test_duplicate_result_id_added_to_duplicate_list_when_not_in_duplicate_list(
            self,
            query_duplicate_results_mock, update_result_mock):
        self.maxDiff = None
        # Given
        new_record = get_result_record_from_file('example_record_1.json')
        new_record['duplicates'] = [{'result_id': 'other_id'}]
        duplicate_items = [get_result_record_from_file('result_record_duplicate_slide_number.json')]
        query_duplicate_results_mock.return_value = duplicate_items

        expected_response = deepcopy(new_record)
        expected_response['duplicates'] = [{'result_id': 'other_id'}, {
            'result_id': '540407ef-2578-4cf2-82c8-0ab735330265', 'received_time': '2020-01030T09:52:12.345'}]

        # When
        actual_response = match_result_utils_module.identify_duplicate_slide_numbers(new_record)

        # Then
        self.assertEqual(expected_response, actual_response)
        update_result_mock.assert_called_once_with(
            {'result_id': '540407ef-2578-4cf2-82c8-0ab735330267', 'received_time': '2020-01030T09:52:12.345'},
            {
                'workflow_state': ResultWorkflowState.REJECTED,
                'rejected_reason': 'Slide ref duplicate',
                'duplicates': [
                    {'result_id': 'other_id'},
                    {'result_id': '540407ef-2578-4cf2-82c8-0ab735330265', 'received_time': '2020-01030T09:52:12.345'}
                ]
            }
        )

    @patch('common.utils.match_result_utils.query_duplicate_results')
    @patch('common.utils.match_result_utils.reject_result')
    def test_exact_duplicate_found_on_identify_duplicate_slide_numbers(
            self,
            reject_result_mock,
            query_duplicate_results_mock):
        # Given
        new_record = get_result_record_from_file('example_record_1.json')
        duplicate_items = [new_record]
        query_duplicate_results_mock.return_value = duplicate_items

        duplicates = [{
            'result_id': '540407ef-2578-4cf2-82c8-0ab735330267', 'received_time': '2020-01030T09:52:12.345'
        }
        ]
        expected_response = deepcopy(new_record)
        expected_response['workflow_state'] = ResultWorkflowState.REJECTED
        expected_response['rejected_reason'] = 'Slide ref duplicate'
        expected_response['duplicates'] = duplicates

        # When
        actual_response = match_result_utils_module.identify_duplicate_slide_numbers(new_record)

        # Then
        reject_result_mock.assert_called_with(expected_response, 'Slide ref duplicate', {'duplicates': duplicates})
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils.query_duplicate_results')
    @patch('common.utils.match_result_utils.reject_result')
    def test_multiple_duplicates_found_on_identify_duplicate_slide_numbers(
            self,
            reject_result_mock,
            query_duplicate_results_mock):
        # Given
        new_record_1 = get_result_record_from_file('example_record_1.json')
        new_record_2 = get_result_record_from_file('example_record_2.json')
        duplicate_items = [new_record_1, new_record_2]
        query_duplicate_results_mock.return_value = duplicate_items

        duplicates = [{
            'result_id': '540407ef-2578-4cf2-82c8-0ab735330267', 'received_time': '2020-01030T09:52:12.345'
        },
            {
                'result_id': '540407ef-2578-4cf2-82c8-0ab735330265', 'received_time': '2020-01030T09:52:12.345'
        }
        ]
        expected_response = deepcopy(new_record_1)
        expected_response['workflow_state'] = ResultWorkflowState.REJECTED
        expected_response['rejected_reason'] = 'Slide ref duplicate'
        expected_response['duplicates'] = duplicates

        # When
        actual_response = match_result_utils_module.identify_duplicate_slide_numbers(new_record_1)

        # Then
        reject_result_mock.assert_called_with(expected_response, 'Slide ref duplicate', {'duplicates': duplicates})
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils.log', Mock())
    def test_create_participant_result_record(self):
        # Given
        decoded_record = {
            'test_date': '2020-12-01',
            'date_of_birth': '1980-01-01',
            'slide_number': '49491234',
            'action': 'Routine',
            'action_code': 'A',
            'result': 'No Cytology test undertaken',
            'result_code': 'X',
            'infection_result': 'HPV negative',
            'infection_code': '0',
            'sender_source_type': '2',
            'source_code': 'H',
            'sender_code': '1445',
            'self_sample': True,
            'hpv_primary': True,
            'recall_months': '06',
            'status': 'ROUTINE',
        }

        matched_participant = {
            'participant_id': '1',
            'nhs_number': '123 456 7890',
            'letter_address': 'letter_address',
            'date_of_birth': '1980-01-01',
        }

        created = '2021-03-05'

        result_id = 'the_result_id'

        expected_response = {
            'participant_id': '1',
            'sort_key': 'RESULT#2020-12-01#2021-03-05',
            'result_id': 'the_result_id',
            'nhs_number': '123 456 7890',
            'sanitised_nhs_number': '1234567890',
            'slide_number': '49491234',
            'created': '2021-03-05',
            'test_date': '2020-12-01',
            'result_date': '2021-03-05',
            'next_test_due_date': '2023-12-01',
            'action': 'Routine',
            'action_code': 'A',
            'result': 'No Cytology test undertaken',
            'result_code': 'X',
            'infection_result': 'HPV negative',
            'infection_code': '0',
            'source_code': 'H',
            'sender_source_type': '2',
            'sender_code': '1445',
            'self_sample': True,
            'status': 'ROUTINE',
            'letter_address': 'letter_address',
            'hpv_primary': True
        }

        # When
        actual_response = match_result_utils_module.create_participant_result_record(
            decoded_record, result_id, matched_participant, created)

        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils.log', Mock())
    def test_create_participant_result_record_repeat_recommended(self):
        # Given
        decoded_record = {
            'test_date': '2020-12-01',
            'date_of_birth': '1980-01-01',
            'slide_number': '49491234',
            'action': 'Repeat requested',
            'action_code': 'R',
            'result': 'No Cytology test undertaken',
            'result_code': 'X',
            'infection_result': 'HPV negative',
            'infection_code': '0',
            'sender_source_type': '2',
            'source_code': 'H',
            'sender_code': '1445',
            'self_sample': True,
            'hpv_primary': True,
            'recall_months': '06',
            'status': 'ROUTINE',
        }

        matched_participant = {
            'participant_id': '1',
            'nhs_number': '123 456 7890',
            'letter_address': 'letter_address',
            'date_of_birth': '1980-01-01',
        }

        created = '2021-03-05'

        result_id = 'the_result_id'

        expected_response = {
            'participant_id': '1',
            'sort_key': 'RESULT#2020-12-01#2021-03-05',
            'result_id': 'the_result_id',
            'nhs_number': '123 456 7890',
            'sanitised_nhs_number': '1234567890',
            'slide_number': '49491234',
            'created': '2021-03-05',
            'test_date': '2020-12-01',
            'result_date': '2021-03-05',
            'next_test_due_date': '2021-06-01',
            'action': 'Repeat requested',
            'action_code': 'R',
            'result': 'No Cytology test undertaken',
            'result_code': 'X',
            'infection_result': 'HPV negative',
            'infection_code': '0',
            'source_code': 'H',
            'sender_source_type': '2',
            'sender_code': '1445',
            'self_sample': True,
            'status': 'ROUTINE',
            'letter_address': 'letter_address',
            'hpv_primary': True,
            'recall_months': '06'
        }

        # When
        actual_response = match_result_utils_module.create_participant_result_record(
            decoded_record, result_id, matched_participant, created)

        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils.update_result_record')
    def test_reject_result_no_add_data(
            self,
            update_result_record_mock):
        # Given
        update_result_record_mock.return_value = None

        key = {
            'result_id': '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1',
            'received_time': '2021-03-22 10:12:23+00:00'
        }

        reason = 'this reason'

        attributes_to_update = {
            'workflow_state': ResultWorkflowState.REJECTED,
            'rejected_reason': 'this reason',
        }

        # When
        match_result_utils_module.reject_result(key, reason)

        # Then
        update_result_record_mock.assert_called_once_with(key, attributes_to_update)

    @patch('common.utils.match_result_utils.update_result_record')
    def test_reject_result_with_add_data(
            self,
            update_result_record_mock):
        # Given
        update_result_record_mock.return_value = None

        key = {
            'result_id': '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1',
            'received_time': '2021-03-22 10:12:23+00:00'
        }

        reason = 'this reason'

        additional_data = {
            'duplicates': ['540407ef-2578-4cf2-82c8-0ab735330267']
        }

        attributes_to_update = {
            'workflow_state': ResultWorkflowState.REJECTED,
            'rejected_reason': 'this reason',
            'duplicates': ['540407ef-2578-4cf2-82c8-0ab735330267']
        }

        # When
        match_result_utils_module.reject_result(key, reason, additional_data)

        # Then
        update_result_record_mock.assert_called_once_with(key, attributes_to_update)

    @unpack
    @data(
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      False
            },
            [],
            '2021-01-01',
            None
        ),
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      True
            },
            [{
                'participant_id': 'test',
                'sort_key':       'CEASE#2020-01-01',
                'reason':         'NO_CERVIX',
                'date_from':      '2020-03-04',
            }],
            '2021-01-01',
            None
        ),
        (
            {},
            [],
            '2021-01-01',
            None
        ),
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      True
            },
            [{
                'participant_id': 'test',
                'sort_key':       'CEASE#2020-03-04',
                'reason':         'RADIOTHERAPY',
                'date_from':      '2020-03-04',
            }],
            '2021-01-01',
            'Ceased - RADIOTHERAPY'
        ),
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      True
            },
            [{
                'participant_id': 'test',
                'sort_key':       'CEASE#2020-03-04',
                'reason':         'RADIOTHERAPY',
                'date_from':      '2020-03-04',
            }],
            '',
            None
        ),
        # Lab test date on cease date
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      True
            },
            [{
                'participant_id': 'test',
                'sort_key':       'CEASE#2020-03-04',
                'reason':         'RADIOTHERAPY',
                'date_from':      '2020-03-04',
            }],
            '2020-03-04',
            None
        ),
        # Lab test date before cease date
        (
            {
                'participant_id': 'test',
                'sort_key':       'PARTICIPANT',
                'is_ceased':      True
            },
            [{
                'participant_id': 'test',
                'sort_key':       'CEASE#2020-03-04',
                'reason':         'RADIOTHERAPY',
                'date_from':      '2020-03-04',
            }],
            '2020-01-01',
            None
        ),
    )
    def test_participant_ceased_with_radiotherapy(
            self, participant_record, ceased_records, test_date, expected_response):
        # Given
        decoded_record = {'test_date': test_date}  # not used

        # When
        actual_response = match_result_utils_module._radiotherapy_check(
            decoded_record, participant_record, ceased_records)

        # Then
        self.assertEqual(expected_response, actual_response)

    @unpack
    @data(
        (
            {
                'active': False,
                'reason_for_removal_code': 'CGA',
                'reason_for_removal_effective_from_date': '2021-07-19'
            },
            {'test_date': '2021-07-20'},
            'INACTIVE_CGA'
        ),
        (
            {
                'active': False,
                'reason_for_removal_code': 'CGA',
                'reason_for_removal_effective_from_date': '2021-07-21'
            },
            {'test_date': '2021-07-20'},
            None
        ),
        (
            {
                'active': False,
                'reason_for_removal_code': 'DEA',
                'reason_for_removal_effective_from_date': '2021-07-20'
            },
            {'test_date': '2021-07-20'},
            None
        ),
        (
            {
                'active': False,
                'reason_for_removal_code': 'EMB',
                'reason_for_removal_effective_from_date': '2021-07-21'
            },
            {'test_date': '2021-07-20'},
            None
        ),
        (
            {
                'active': False,
                'reason_for_removal_code': 'CGA',
                'reason_for_removal_effective_from_date': None
            },
            {'test_date': '2020-07-20'},
            None
        )
    )
    def test_inactive_participant_check(
            self, participant_record, decoded_record, expected_response):
        # Given
        # No need to mock as we are passing the participant_record and decoded_record

        # When
        actual_response = match_result_utils_module._inactive_participant_check(decoded_record, participant_record)

        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.match_result_utils.calculate_next_test_due_date_for_result')
    def test_calculate_ntdd_from_result_data(self, calculate_mock):
        # Given
        decoded_record = {
            'test_date': '2021-07-20',
            'date_of_birth': '1980-01-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'A'
        }
        calculate_mock.return_value = {
            'default': {'next_test_due_date': datetime.strptime('2024-07-20', '%Y-%m-%d').date()}
        }
        expected_next_test_due_date = datetime.strptime('2024-07-20', '%Y-%m-%d').date()
        # When
        actual_next_test_due_date = match_result_utils_module._calculate_ntdd_from_result_data(decoded_record)
        # Then
        calculate_mock.assert_called_with(
            datetime.strptime(decoded_record['test_date'], '%Y-%m-%d').date(),
            (decoded_record['result_code'], decoded_record['infection_code'], decoded_record['action_code']),
            datetime.strptime(decoded_record['date_of_birth'], '%Y-%m-%d').date(),
            True,
            False)
        self.assertEqual(expected_next_test_due_date, actual_next_test_due_date)

    def test_calculate_ntdd_from_recall_months(self):
        # Given
        recall_months = 36
        decoded_record = {
            'test_date': '2021-07-20',
            'action_code': 'R',
            'recall_months': f'{recall_months}'
        }
        test_date = datetime.strptime('2021-07-20', '%Y-%m-%d').date()
        expected_next_test_due_date = test_date + relativedelta(months=recall_months)
        # When
        actual_next_test_due_date = match_result_utils_module._calculate_ntdd_from_recall_months(decoded_record)
        # Then
        self.assertEqual(expected_next_test_due_date, actual_next_test_due_date)

    @unpack
    @data(
        (
            {
                'test_date': '2021-07-20',
                'action_code': 'R'
            },
            12
        ),
        (
            {
                'test_date': '2021-07-20',
                'action_code': 'S'
            },
            12
        )
    )
    def test_calculate_ntdd_from_fail_safe(self, decoded_record, recall_months):
        # Given
        test_date = datetime.strptime(decoded_record['test_date'], '%Y-%m-%d').date()
        expected_next_test_due_date = test_date + relativedelta(months=recall_months)
        # When
        actual_next_test_due_date = match_result_utils_module._calculate_ntdd_from_fail_safe(decoded_record)
        # Then
        self.assertEqual(expected_next_test_due_date, actual_next_test_due_date)

    @patch('common.utils.match_result_utils.log', Mock())
    @patch('common.utils.match_result_utils._calculate_ntdd_from_recall_months')
    @patch('common.utils.match_result_utils._calculate_ntdd_from_result_data')
    @patch('common.utils.match_result_utils._calculate_ntdd_from_fail_safe')
    def test_calculate_ntdd_uses_recall_months_as_first_preference(self,
                                                                   fail_safe_mock,
                                                                   result_data_mock,
                                                                   recall_months_mock):
        # Given
        decoded_record = {}
        expected_next_test_due_date = datetime.strptime('2020-01-01', '%Y-%m-%d').date()
        recall_months_mock.return_value = expected_next_test_due_date
        # When
        actual_next_test_due_date = match_result_utils_module.calculate_ntdd(decoded_record)
        # Then
        self.assertEqual(expected_next_test_due_date, actual_next_test_due_date)
        recall_months_mock.assert_called_with(decoded_record)
        result_data_mock.assert_not_called()
        fail_safe_mock.assert_not_called()

    @patch('common.utils.match_result_utils.log', Mock())
    @patch('common.utils.match_result_utils._calculate_ntdd_from_recall_months')
    @patch('common.utils.match_result_utils._calculate_ntdd_from_result_data')
    @patch('common.utils.match_result_utils._calculate_ntdd_from_fail_safe')
    def test_calculate_ntdd_uses_recall_months_as_second_preference(self,
                                                                    fail_safe_mock,
                                                                    result_data_mock,
                                                                    recall_months_mock):
        # Given
        decoded_record = {}
        expected_next_test_due_date = datetime.strptime('2020-01-01', '%Y-%m-%d').date()
        recall_months_mock.return_value = None
        result_data_mock.return_value = expected_next_test_due_date
        # When
        actual_next_test_due_date = match_result_utils_module.calculate_ntdd(decoded_record)
        # Then
        self.assertEqual(expected_next_test_due_date, actual_next_test_due_date)
        recall_months_mock.assert_called_with(decoded_record)
        result_data_mock.assert_called_with(decoded_record)
        fail_safe_mock.assert_not_called()

    @patch('common.utils.match_result_utils.log', Mock())
    @patch('common.utils.match_result_utils._calculate_ntdd_from_recall_months')
    @patch('common.utils.match_result_utils._calculate_ntdd_from_result_data')
    @patch('common.utils.match_result_utils._calculate_ntdd_from_fail_safe')
    def test_calculate_ntdd_uses_fail_safe_as_third_preference(self,
                                                               fail_safe_mock,
                                                               result_data_mock,
                                                               recall_months_mock):
        # Given
        decoded_record = {}
        expected_next_test_due_date = datetime.strptime('2020-01-01', '%Y-%m-%d').date()
        recall_months_mock.return_value = None
        result_data_mock.return_value = None
        fail_safe_mock.return_value = expected_next_test_due_date
        # When
        actual_next_test_due_date = match_result_utils_module.calculate_ntdd(decoded_record)
        # Then
        self.assertEqual(expected_next_test_due_date, actual_next_test_due_date)
        recall_months_mock.assert_called_with(decoded_record)
        result_data_mock.assert_called_with(decoded_record)
        fail_safe_mock.assert_called_with(decoded_record)

    @patch('common.utils.match_result_utils.log', Mock())
    @patch('common.utils.match_result_utils._calculate_ntdd_from_recall_months')
    @patch('common.utils.match_result_utils._calculate_ntdd_from_result_data')
    @patch('common.utils.match_result_utils._calculate_ntdd_from_fail_safe')
    def test_calculate_ntdd_raises_exception_in_defeat(self,
                                                       fail_safe_mock,
                                                       result_data_mock,
                                                       recall_months_mock):
        # Given
        decoded_record = {'date_of_birth': '1980-01-01'}
        recall_months_mock.return_value = None
        result_data_mock.return_value = None
        fail_safe_mock.return_value = None
        # When
        with self.assertRaises(Exception) as context:
            match_result_utils_module.calculate_ntdd(decoded_record)
        # Then
        recall_months_mock.assert_called_with(decoded_record)
        result_data_mock.assert_called_with(decoded_record)
        fail_safe_mock.assert_called_with(decoded_record)
        self.assertEqual('Unable to calculate NTDD', context.exception.args[0])

    @patch('common.utils.match_result_utils.log', Mock())
    @patch('common.utils.match_result_utils.update_result_record')
    @patch('common.utils.match_result_utils.update_episodes_and_pause_participant_for_lab_result')
    def test_participant_is_paused_when_result_is_rejected(
            self,
            pause_participant_mock,
            update_result_record_mock):
        # Given
        update_result_record_mock.return_value = None
        pause_participant_mock.return_value = None
        participant_id = 'test'
        result_record = {
            'result_id': '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1',
            'received_time': '2021-03-22 10:12:23+00:00',
            'matched_to_participant': 'test'
        }
        key = {
            'result_id': '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1',
            'received_time': '2021-03-22 10:12:23+00:00'
        }

        reason = 'this reason'

        attributes_to_update = {
            'workflow_state': ResultWorkflowState.REJECTED,
            'rejected_reason': 'this reason',
        }

        # When
        match_result_utils_module.reject_result_and_pause_participant(reason, participant_id, result_record)

        # Then
        update_result_record_mock.assert_called_once_with(key, attributes_to_update)
        pause_participant_mock.assert_called_once_with('test', '52bb6ddb-bdc0-4e6f-98b3-81449d85f7e1')

    @patch('common.utils.match_result_utils.log')
    @patch('common.utils.match_result_utils.update_live_episodes_status_to_logged')
    @patch('common.utils.match_result_utils.pause_participant_for_lab_or_manually_added_result')
    def test_update_episodes_and_pause_participant_for_lab_result_has_expected_calls(
            self,
            pause_participant_mock,
            update_episodes_mock,
            log_mock):
        participant_id = '1'
        result_id = '2'

        match_result_utils_module.update_episodes_and_pause_participant_for_lab_result(
            participant_id, result_id
        )

        expected_log_call = [call({'log_reference': CommonLogReference.PARTICIPANT0012, 'result_id': '2'})]

        log_mock.assert_has_calls(expected_log_call)
        update_episodes_mock.assert_called_once_with('1')
        pause_participant_mock.assert_called_once_with('1', '2', True)

    @patch('common.utils.match_result_utils.log')
    @patch('common.utils.match_result_utils.update_live_episodes_status_to_logged')
    @patch('common.utils.match_result_utils.pause_participant_for_lab_or_manually_added_result')
    def test_update_episodes_and_pause_participant_for_manually_added_result_has_expected_calls(
            self,
            pause_participant_mock,
            update_episodes_mock,
            log_mock):
        participant_id = '1'
        crm_number = '2'

        match_result_utils_module.update_episodes_and_pause_participant_for_manually_added_result(
            participant_id, crm_number, ''
        )

        expected_log_call = [call({'log_reference': CommonLogReference.PARTICIPANT0012, 'crm_number': '2'})]

        log_mock.assert_has_calls(expected_log_call)
        update_episodes_mock.assert_called_once_with('1')
        pause_participant_mock.assert_called_once_with('1', '2', False, '')

    @patch('common.utils.match_result_utils.audit')
    @patch('common.utils.match_result_utils.log')
    @patch('common.utils.match_result_utils.update_live_episodes_status_from_logged_to_previous')
    @patch('common.utils.match_result_utils.unpause_participant')
    def test_update_episodes_and_unpause_participant_has_expected_calls(
            self,
            unpause_participant_mock,
            update_episodes_mock,
            log_mock,
            audit_mock):
        participant_id = '1'
        crm_number = 'CAS-12345-ABCDE'
        comment = 'A comment'

        match_result_utils_module.update_episodes_and_unpause_participant(
            participant_id, crm_number, comment
        )

        expected_log_call = [call({'log_reference': CommonLogReference.PARTICIPANT0013})]

        log_mock.assert_has_calls(expected_log_call)
        update_episodes_mock.assert_called_once_with('1')
        unpause_participant_mock.assert_called_once_with('1')

        audit_mock.assert_has_calls(
            [call(
                action=AuditActions.UNPAUSE_PARTICIPANT,
                user=AuditUsers.SYSTEM,
                participant_ids=['1'],
                additional_information={
                    'crm_number': 'CAS-12345-ABCDE',
                    'comment': 'A comment'
                }
            )]
        )


def get_result_record_from_file(file_name):
    if not file_name:
        return None

    current_path = os.path.dirname(os.path.realpath(__file__))
    full_file_path = os.path.join(current_path, 'test_data', file_name)
    with open(full_file_path) as f:
        record = json.loads(f.read())
    return record
