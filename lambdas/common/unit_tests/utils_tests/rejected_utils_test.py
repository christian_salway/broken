
from ddt import ddt, data, unpack
from unittest.case import TestCase
from mock import patch
import common.utils.rejected_utils as rejected_utils
from boto3.dynamodb.conditions import Key

from common.utils.dynamodb_access.table_names import TableNames


module = 'common.utils.rejected_utils'


@ddt
class TestRejectedUtils(TestCase):

    @patch(f'{module}.dynamodb_put_item')
    def test_create_replace_record(self, put_item_mock):
        put_item_mock.return_value = {'Attributes': {}, 'ConsumedCapacity': {}}
        example_record = {
            'uuid': '48360fb1-9de8-43a7-be9a-9ce71354d6df', 'nhs_number': '9565512593', 'rejection_type': 'PDS',
            'file_name': 'CSS_Update_20200528.dat', 'created': '2021-07-02T10:12:28.516191+00:00',
            'metadata': {'failed_rules': ['CSMS-1'], 'hard_fail': True},
        }
        actual_response = rejected_utils.create_or_replace_rejected_record(example_record)

        expected_response = {'Attributes': {}, 'ConsumedCapacity': {}}

        put_item_mock.assert_called_with(TableNames.REJECTED, example_record)
        self.assertEqual(expected_response, actual_response)

    @patch(f'{module}.dynamodb_update_item')
    @patch(f'{module}.build_update_query')
    def test_update_rejected_record(self, build_query_mock, update_item_mock):

        build_query_mock.return_value = (
            'SET #rejected_status = :rejected_status, #date_written_to_csv = :date_written_to_csv',
            {'#rejected_status': 'rejected_status', '#date_written_to_csv': 'date_written_to_csv'},
            {':rejected_status': 'WRITTEN_TO_CSV', ':written_timestamp': '2021-07-05T16:19:20.198498+00:00'}
        )
        update_item_mock.return_value = {'ResponseMetadata': {}}
        key = {'uuid': '48360fb1-9de8-43a7-be9a-9ce71354d6df', 'rejection_type': 'PDS#2021-07-10'}
        attributes_to_update = {
            'rejected_status': 'WRITTEN_TO_CSV',
            'date_written_to_csv': '2021-07-05T16:19:20.198498+00:00'
        }

        actual_response = rejected_utils.update_rejected_record(key, attributes_to_update)

        expected_response = {'ResponseMetadata': {}}

        update_item_mock.assert_called_with(
            TableNames.REJECTED,
            dict(
                Key={'uuid': '48360fb1-9de8-43a7-be9a-9ce71354d6df', 'rejection_type': 'PDS#2021-07-10'},
                UpdateExpression='SET #rejected_status = :rejected_status, #date_written_to_csv = :date_written_to_csv',
                ExpressionAttributeValues={':rejected_status': 'WRITTEN_TO_CSV',
                                           ':written_timestamp': '2021-07-05T16:19:20.198498+00:00'},
                ExpressionAttributeNames={'#rejected_status': 'rejected_status',
                                          '#date_written_to_csv': 'date_written_to_csv'}
            )
        )
        build_query_mock.assert_called_with(attributes_to_update)
        self.assertEqual(expected_response, actual_response)

    @patch(f'{module}.dynamodb_query')
    def test_query_rejected_records_by_status(self, query_mock):
        query_mock.return_value = [
            {
                'uuid': '48360fb1-9de8-43a7-be9a-9ce71354d6df', 'nhs_number': '9565512593', 'rejection_type': 'PDS',
                'file_name': 'CSS_Update_20200528.dat', 'created': '2021-07-02T10:12:28.516191+00:00',
                'metadata': {'failed_rules': ['CSMS-1'], 'hard_fail': True},
            },
            {
                'uuid': '1d163e6d-c5fe-429c-8ef8-ef4b1b991186', 'nhs_number': '9999999999', 'rejection_type': 'PDS',
                'file_name': 'CSS_Update_20200528.dat', 'created': '2021-07-02T10:13:28.516191+00:00',
                'metadata': {'failed_rules': ['CSMS-1'], 'hard_fail': True},
            }
        ]

        actual_response = rejected_utils.query_rejected_records_by_status('UNPROCESSED', 'PDS')

        expected_response = [
            {
                'uuid': '48360fb1-9de8-43a7-be9a-9ce71354d6df', 'nhs_number': '9565512593', 'rejection_type': 'PDS',
                'file_name': 'CSS_Update_20200528.dat', 'created': '2021-07-02T10:12:28.516191+00:00',
                'metadata': {'failed_rules': ['CSMS-1'], 'hard_fail': True},
            },
            {
                'uuid': '1d163e6d-c5fe-429c-8ef8-ef4b1b991186', 'nhs_number': '9999999999', 'rejection_type': 'PDS',
                'file_name': 'CSS_Update_20200528.dat', 'created': '2021-07-02T10:13:28.516191+00:00',
                'metadata': {'failed_rules': ['CSMS-1'], 'hard_fail': True},
            }
        ]

        query_mock.assert_called_with(
            TableNames.REJECTED,
            dict(
                IndexName='rejected-status',
                KeyConditionExpression=Key('rejected_status').eq('UNPROCESSED')
                & Key('rejection_type').begins_with('PDS#')
            )
        )
        self.assertEqual(actual_response, expected_response)

    @unpack
    @data(
        (
            {'rejection_type': 'PDS#2021-05-07', 'metadata': {'failed_rules': ['CSMS-1']}},
            'Failed PDS validation rules: CSMS-1'
        ),
        (
            {'rejection_type': 'PDS#2021-05-07', 'metadata': {'failed_rules': ['CSMS-1', 'CSMS-2']}},
            'Failed PDS validation rules: CSMS-1, CSMS-2'
        )
    )
    def test_get_rejection_reason(self, rejected_record, expected_reason):
        output = rejected_utils.get_rejection_reason(rejected_record)
        self.assertEqual(output, expected_reason)
