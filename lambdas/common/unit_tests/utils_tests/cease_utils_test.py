from mock import patch, call
from unittest import TestCase
import datetime
from ddt import ddt, data, unpack

from common.log_references import CommonLogReference
from common.models.participant import ParticipantStatus, EpisodeStatus, CeaseReason

mock_env_vars = {
    'NOTIFICATION_QUEUE_URL': 'TEST'
}

path = 'common.utils.cease_utils'

with patch('boto3.resource'):
    import common.utils.cease_utils as cease_utils


@ddt
@patch('os.environ', mock_env_vars)
class TestCeaseUtils(TestCase):
    mock_user_id = 'NHS12345'
    mock_event_body = {'reason': 'NO_CERVIX', 'crm_number': 'CAS-12345-ABCDE'}

    def setUp(self):
        self.log_patcher = patch('common.log.logger').start()

    def tearDown(self):
        self.log_patcher.stop()

    @unpack
    @data(
        ({'is_ceased': True}, '', [], False),
        ({'active': False}, '', [], False),
        ({'active': True, 'date_of_birth': '1955-03-24'}, datetime.date(2020, 3, 24), [], False),
        ({'active': True, 'date_of_birth': '1955-03-24', 'participant_id': 'id'}, datetime.date(2021, 3, 24),
         [{'sort_key': 'NOT_A_RESULT'}], True),
        ({'active': True, 'date_of_birth': '1955-03-24', 'participant_id': 'id'}, datetime.date(2021, 3, 24),
         [{'sort_key': 'RESULT#1', 'action_code': 'A', 'test_date': '2020-09-01'},
          {'sort_key': 'RESULT#1', 'action_code': 'B', 'test_date': '2020-09-02'}], False),
        ({'active': True, 'date_of_birth': '1955-03-24', 'participant_id': 'id'}, datetime.date(2021, 3, 24),
         [{'sort_key': 'RESULT#1', 'result_code': '0', 'infection_code': None, 'action_code': 'A',
           'test_date': '2020-09-04'},
          {'sort_key': 'RESULT#1', 'result_code': '0', 'infection_code': None, 'action_code': 'S',
           'test_date': '2020-09-03'},
          {'sort_key': 'RESULT#1', 'result_code': '2', 'infection_code': None, 'action_code': 'A',
           'test_date': '2020-09-02'},
          {'sort_key': 'RESULT#1', 'result_code': '2', 'infection_code': None, 'action_code': 'R',
           'test_date': '2020-09-01'}], True),
        ({'active': True, 'date_of_birth': '1955-03-24', 'participant_id': 'id'}, datetime.date(2021, 3, 24),
         [{'sort_key': 'RESULT#1', 'result_code': 'B', 'infection_code': '0', 'action_code': 'A',
           'test_date': '2020-09-04'},
          {'sort_key': 'RESULT#1', 'result_code': 'X', 'infection_code': '0', 'action_code': 'S',
           'test_date': '2020-09-03'},
          {'sort_key': 'RESULT#1', 'result_code': '2', 'infection_code': '9', 'action_code': 'R',
           'test_date': '2020-09-02'},
          {'sort_key': 'RESULT#1', 'result_code': 'Z', 'infection_code': 'Y', 'action_code': 'X',
           'test_date': '2020-09-01'}], True),
        ({'active': True, 'date_of_birth': '1955-03-24', 'participant_id': 'id'}, datetime.date(2021, 3, 24),
         [{'sort_key': 'RESULT#1', 'result_code': '1', 'infection_code': '0', 'action_code': 'H',
           'test_date': '2020-09-04'},
          {'sort_key': 'RESULT#1', 'result_code': 'X', 'infection_code': '0', 'action_code': 'S',
           'test_date': '2020-09-03'},
          {'sort_key': 'RESULT#1', 'result_code': '2', 'infection_code': '9', 'action_code': 'R',
           'test_date': '2020-09-02'},
          {'sort_key': 'RESULT#1', 'result_code': 'Z', 'infection_code': 'Y', 'action_code': 'X',
           'test_date': '2020-09-01'}], False),
        ({'active': True, 'date_of_birth': '1955-03-24', 'participant_id': 'id'}, datetime.date(2021, 3, 24),
         [{'sort_key': 'RESULT#1', 'result_code': 'B', 'infection_code': '0', 'action_code': 'A',
           'test_date': '2020-09-04'},
          {'sort_key': 'RESULT#1', 'result_code': 'X', 'infection_code': '0', 'action_code': 'S',
           'test_date': '2020-09-03'},
          {'sort_key': 'RESULT#1', 'result_code': 'Z', 'infection_code': 'Z', 'action_code': 'Z',
           'test_date': '2020-09-02'},
          {'sort_key': 'RESULT#1', 'result_code': 'E', 'infection_code': 'U', 'action_code': 'S',
           'test_date': '2020-09-01'}], False)
    )
    @patch(f'{path}.get_participant_and_test_results')
    def test_participant_eligible_for_auto_cease_decision_tree(
            self, participant, next_test_due_date, participant_table_records, expected_can_auto_cease, get_mock):
        get_mock.return_value = participant_table_records
        actual_can_auto_cease = cease_utils.participant_eligible_for_auto_cease(participant, next_test_due_date)
        self.assertEqual(expected_can_auto_cease, actual_can_auto_cease)

    @data(True, False)
    @patch(f'{path}.get_participant_and_test_results')
    def test_participant_eligible_for_auto_cease_segregation_param_passed_correctly(
            self, segregate, get_mock):
        participant = {'active': True, 'date_of_birth': '1955-03-24', 'participant_id': 'id'}
        next_test_due_date = datetime.date(2021, 3, 24)
        participant_table_record = [{'sort_key': 'NOT_A_RESULT'}]

        get_mock.return_value = participant_table_record
        cease_utils.participant_eligible_for_auto_cease(participant, next_test_due_date, segregate=segregate)

        get_mock.assert_called_once_with('id', segregate=segregate)

    @unpack
    @data(({}, False), ({'result_code': '2'}, False), ({'action_code': 'A'}, False),
          ({'result_code': '3', 'action_code': 'R'}, False),
          ({'result_code': '1'}, True), ({'result_code': '1', 'action_code': 'A'}, True),
          ({'action_code': 'H'}, True), ({'result_code': '9', 'action_code': 'H'}, True),
          ({'result_code': '1', 'action_code': 'H'}, True))
    def test_is_inadequate_result(self, result_record, expected_is_inadequate_result):
        actual_is_inadequate_result = cease_utils.is_inadequate_result(result_record)
        self.assertEqual(expected_is_inadequate_result, actual_is_inadequate_result)

    def test_cease_participant_and_send_notification_messages_raises_exception_given_invalid_cease_reason(self):
        self.assertRaises(
            Exception,
            cease_utils.cease_participant_and_send_notification_messages,
            {'participant_id': '123456', 'sort_key': 'episode sort key'},
            self.mock_user_id,
            {'reason': 'BAD REASON', 'crm_number': 'CAS-12345-ABCDE'}
        )

    @patch('common.utils.sqs_utils.get_sqs_client')
    @patch(f'{path}.datetime')
    @patch(f'{path}.create_replace_record_in_participant_table')
    @patch(f'{path}.update_participant_record')
    @patch(f'{path}.update_participant_episode_record')
    @patch(f'{path}.query_for_live_episode')
    def test_cease_participant_and_send_notification_messages_happy_path_given_existing_live_episode_in_valid_state(
            self, get_episode_mock, update_episode_mock,
            update_participant_mock, create_replace_mock,
            datetime_mock, sqs_client):
        datetime_mock.now.return_value = datetime.datetime(2020, 1, 1, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        get_episode_mock.return_value = [
            {'live_record_status': EpisodeStatus.PNL.value, 'sort_key': 'episode sort key'}]  

        cease_utils.cease_participant_and_send_notification_messages(
            {'participant_id': '123456', 'sort_key': 'episode sort key', 'nhais_cipher': 'ENG'},
            self.mock_user_id,
            self.mock_event_body
        )

        update_episode_mock.assert_called_with(
            record_key={'participant_id': '123456', 'sort_key': 'episode sort key'},
            delete_attributes=['live_record_status'],
            update_attributes={'status': EpisodeStatus.CEASED.value, 'ceased_date': '2020-01-01',
                               'user_id': 'NHS12345'},
            segregate=False
        )

        update_participant_mock.assert_called_with(
            record_key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'},
            update_attributes={'is_ceased': True, 'status': ParticipantStatus.CEASED},
            delete_attributes=['next_test_due_date'],
            segregate=False
        )

        create_replace_mock.assert_called_with({
            'participant_id': '123456',
            'sort_key': 'CEASE#2020-01-01',
            'reason': 'NO_CERVIX',
            'user_id': 'NHS12345',
            'date_from': '2020-01-01T12:15:08.132000+00:00',
            'crm_number': 'CAS-12345-ABCDE'
        },
            segregate=False)

    @patch('common.utils.sqs_utils.get_sqs_client')
    @patch(f'{path}.datetime')
    @patch(f'{path}.create_replace_record_in_participant_table')
    @patch(f'{path}.update_participant_record')
    @patch(f'{path}.update_participant_episode_record')
    @patch(f'{path}.query_for_live_episode')
    def test_cease_participant_and_send_notification_messages_happy_path_with_data_segregation(
            self, get_episode_mock, update_episode_mock,
            update_participant_mock, create_replace_mock,
            datetime_mock, sqs_client):
        datetime_mock.now.return_value = datetime.datetime(2020, 1, 1, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        get_episode_mock.return_value = [
            {'live_record_status': EpisodeStatus.PNL.value, 'sort_key': 'episode sort key'}]  

        cease_utils.cease_participant_and_send_notification_messages(
            {'participant_id': '123456', 'sort_key': 'episode sort key', 'nhais_cipher': 'ENG'},
            self.mock_user_id,
            self.mock_event_body,
            segregate=True
        )

        update_episode_mock.assert_called_with(
            record_key={'participant_id': '123456', 'sort_key': 'episode sort key'},
            delete_attributes=['live_record_status'],
            update_attributes={'status': EpisodeStatus.CEASED.value, 'ceased_date': '2020-01-01',
                               'user_id': 'NHS12345'},
            segregate=True
        )

        update_participant_mock.assert_called_with(
            record_key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'},
            update_attributes={'is_ceased': True, 'status': ParticipantStatus.CEASED},
            delete_attributes=['next_test_due_date'],
            segregate=True
        )

        create_replace_mock.assert_called_with({
                'participant_id': '123456',
                'sort_key': 'CEASE#2020-01-01',
                'reason': 'NO_CERVIX',
                'user_id': 'NHS12345',
                'date_from': '2020-01-01T12:15:08.132000+00:00',
                'crm_number': 'CAS-12345-ABCDE'
            },
            segregate=True
        )

    @patch('common.utils.sqs_utils.get_sqs_client')
    @patch(f'{path}.datetime')
    @patch(f'{path}.create_replace_record_in_participant_table')
    @patch(f'{path}.update_participant_record')
    @patch(f'{path}.update_participant_episode_record')
    @patch(f'{path}.query_for_live_episode')
    def test_cease_participant_and_send_notification_messages_happy_path_given_no_existing_live_episode(
            self, get_episode_mock, update_episode_mock, update_participant_mock,
            create_replace_mock, datetime_mock, sqs_client):
        datetime_mock.now.return_value = datetime.datetime(2020, 1, 1, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        get_episode_mock.return_value = []

        cease_utils.cease_participant_and_send_notification_messages(
            {'participant_id': '123456', 'nhais_cipher': 'ENG'}, self.mock_user_id, self.mock_event_body
        )

        update_episode_mock.assert_not_called()

        update_participant_mock.assert_called_with(
            record_key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'},
            update_attributes={'is_ceased': True, 'status': ParticipantStatus.CEASED},
            delete_attributes=['next_test_due_date'],
            segregate=False
        )

        create_replace_mock.assert_called_with({
                'participant_id': '123456',
                'sort_key': 'CEASE#2020-01-01',
                'reason': 'NO_CERVIX',
                'user_id': 'NHS12345',
                'date_from': '2020-01-01T12:15:08.132000+00:00',
                'crm_number': 'CAS-12345-ABCDE'
            },
            segregate=False
        )

    @patch(f'{path}.update_participant_episode_record')
    @patch(f'{path}.query_for_live_episode')
    def test_handle_existing_episodes_handles_no_episode_scenario(self, query_mock, update_mock):
        query_mock.return_value = []
        cease_utils.handle_existing_episodes('12345678', self.mock_user_id, segregate=False)
        update_mock.assert_not_called()

    @patch(f'{path}.datetime')
    @patch(f'{path}.update_participant_episode_record')
    @patch(f'{path}.query_for_live_episode')
    def test_handle_existing_episodes_accepts_any_status(self, query_mock, update_mock, datetime_mock):
        datetime_mock.now.return_value = datetime.datetime(2020, 1, 1, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)
        query_mock.return_value = [{
            'live_record_status': 'any status is accepted',
            'sort_key': 'EPISODE'
        }]
        cease_utils.handle_existing_episodes('12345678', self.mock_user_id, segregate=False)
        update_mock.assert_called_with(
            record_key={"participant_id": "12345678", 'sort_key': 'EPISODE'},
            update_attributes={
                'status': EpisodeStatus.CEASED.value,
                'ceased_date': '2020-01-01',
                'user_id': 'NHS12345'
            },
            delete_attributes=['live_record_status'],
            segregate=False
        )

    @patch(f'{path}.datetime')
    @patch(f'{path}.update_participant_episode_record')
    @patch(f'{path}.query_for_live_episode')
    def test_handle_existing_episodes_updates_record_correctly(self, query_mock, update_mock, datetime_mock):
        datetime_mock.now.return_value = datetime.datetime(2020, 1, 1, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)
        query_mock.return_value = [{'live_record_status': EpisodeStatus.PNL.value, 'sort_key': 'EPISODE'}]
        cease_utils.handle_existing_episodes('12345678', self.mock_user_id, segregate=False)
        update_mock.assert_called_with(
            record_key={"participant_id": "12345678", 'sort_key': 'EPISODE'},
            update_attributes={
                'status': EpisodeStatus.CEASED.value,
                'ceased_date': '2020-01-01',
                'user_id': 'NHS12345'
            },
            delete_attributes=['live_record_status'],
            segregate=False
        )

    @patch(f'{path}.log')
    @patch(f'{path}.datetime')
    @patch(f'{path}.update_participant_episode_record')
    @patch(f'{path}.query_for_live_episode')
    def test_handle_existing_episodes_updates_records_correctly_if_more_than_one_live_episode(
            self, query_mock, update_mock, datetime_mock, log_mock):
        datetime_mock.now.return_value = datetime.datetime(2020, 1, 1, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)
        query_mock.return_value = [{'live_record_status': EpisodeStatus.PNL.value, 'sort_key': 'EPISODE1'},
                                   {'live_record_status': EpisodeStatus.WALKIN.value, 'sort_key': 'EPISODE2'}]
        cease_utils.handle_existing_episodes('12345678', self.mock_user_id, segregate=False)

        expected_update_calls = [
            call(record_key={'participant_id': '12345678', 'sort_key': 'EPISODE1'},
                 update_attributes={'status': EpisodeStatus.CEASED.value, 'ceased_date': '2020-01-01',
                                    'user_id': 'NHS12345'},  
                 delete_attributes=['live_record_status'],
                 segregate=False),
            call(record_key={'participant_id': '12345678', 'sort_key': 'EPISODE2'},
                 update_attributes={'status': EpisodeStatus.CEASED.value, 'ceased_date': '2020-01-01',
                                    'user_id': 'NHS12345'},  
                 delete_attributes=['live_record_status'],
                 segregate=False),
        ]

        update_mock.assert_has_calls(expected_update_calls)

        expected_log_calls = [
            call({'log_reference': CommonLogReference.CEASE0005, 'participant_id': '12345678',
                  'previous_episode_status': EpisodeStatus.PNL.value}),
            call({'log_reference': CommonLogReference.CEASE0005, 'participant_id': '12345678',
                  'previous_episode_status': EpisodeStatus.WALKIN.value})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{path}.datetime')
    @patch(f'{path}.create_replace_record_in_participant_table')
    def test_create_cease_record_creates_record(self, create_mock, datetime_mock):
        datetime_mock.now.return_value = datetime.datetime(2020, 1, 1, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)
        cease_utils.create_cease_record(
            self.mock_event_body, self.mock_user_id, '123456', segregate=False
        )
        create_mock.assert_called_with({
                'participant_id': '123456',
                'sort_key': 'CEASE#2020-01-01',
                'reason': 'NO_CERVIX',
                'user_id': 'NHS12345',
                'date_from': '2020-01-01T12:15:08.132000+00:00',
                'crm_number': 'CAS-12345-ABCDE'
            },
            segregate=False
        )

    @patch(f'{path}.datetime')
    @patch(f'{path}.create_replace_record_in_participant_table')
    def test_create_cease_record_creates_record_with_comment(self, create_mock, datetime_mock):
        datetime_mock.now.return_value = datetime.datetime(2020, 1, 1, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)
        event_body = {
            'reason': 'NO_CERVIX',
            'comments': 'comments expanding on reason'
        }
        cease_utils.create_cease_record(
            event_body, self.mock_user_id, '123456', segregate=False
        )
        create_mock.assert_called_with({
                'participant_id': '123456',
                'sort_key': 'CEASE#2020-01-01',
                'reason': 'NO_CERVIX',
                'user_id': 'NHS12345',
                'date_from': '2020-01-01T12:15:08.132000+00:00',
                'comments': 'comments expanding on reason',
            },
            segregate=False
        )

    @patch(f'{path}.datetime')
    @patch(f'{path}.create_replace_record_in_participant_table')
    def test_create_cease_record_creates_record_with_crm_number(self, create_mock, datetime_mock):
        datetime_mock.now.return_value = datetime.datetime(2020, 1, 1, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)
        event_body = {
            'reason': 'NO_CERVIX',
            'crm_number': '928375928374',
        }
        cease_utils.create_cease_record(
            event_body, self.mock_user_id, '123456', segregate=False
        )
        create_mock.assert_called_with({
                'participant_id': '123456',
                'sort_key': 'CEASE#2020-01-01',
                'reason': 'NO_CERVIX',
                'user_id': 'NHS12345',
                'date_from': '2020-01-01T12:15:08.132000+00:00',
                'crm_number': '928375928374',
            },
            segregate=False
        )

    @patch(f'{path}.datetime')
    @patch(f'{path}.create_replace_record_in_participant_table')
    def test_create_cease_record_creates_record_with_crm_number_and_comment(self, create_mock, datetime_mock):
        datetime_mock.now.return_value = datetime.datetime(2020, 1, 1, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)
        event_body = {
            'reason': 'NO_CERVIX',
            'crm_number': '32532',
            'comments': 'commenting about this'
        }
        cease_utils.create_cease_record(
            event_body, self.mock_user_id, '123456', segregate=False
        )
        create_mock.assert_called_with({
                'participant_id': '123456',
                'sort_key': 'CEASE#2020-01-01',
                'reason': 'NO_CERVIX',
                'user_id': 'NHS12345',
                'date_from': '2020-01-01T12:15:08.132000+00:00',
                'crm_number': '32532',
                'comments': 'commenting about this'
            },
            segregate=False
        )

    @unpack
    @data(
        (CeaseReason.NO_CERVIX, 'CXNC'),
        (CeaseReason.DUE_TO_AGE, 'CXAN'),
        (CeaseReason.PATIENT_INFORMED_CHOICE, 'CIC'),
        (CeaseReason.MENTAL_CAPACITY_ACT, 'CMC'),
        (CeaseReason.AUTOCEASE_DUE_TO_AGE, 'CUA')
    )
    @patch(f'{path}.log')
    @patch(f'{path}.send_new_message')
    def test_queue_cease_letter(self, reason, code, send_new_mesage_mock, log_mock):
        cease_utils.queue_cease_letter('participant_id', reason)

        send_new_mesage_mock.assert_called_with(
            mock_env_vars['NOTIFICATION_QUEUE_URL'],
            {'participant_id': 'participant_id', 'type': 'Cease letter', 'template': code}
        )

        log_mock.assert_called_with(
            {
                'log_reference': CommonLogReference.CEASE0003,
                'participant_id': 'participant_id',
            }
        )
