from unittest import TestCase
from mock import patch, Mock
from decimal import Decimal

from common.utils.dynamodb_access.table_names import TableNames


EXAMPLE_RESPONSE = {'Attributes': {'sequence_value': Decimal('2')}}


@patch('os.environ', {'DYNAMODB_SEQUENCES': 'sequences_table'})
class TestSequenceUtils(TestCase):

    @classmethod
    @patch('boto3.resource')
    @patch('boto3.client', Mock())
    def setUpClass(cls, boto3_resource):
        from common.utils.sequence_utils import (
            generate_sequence_number as _generate_sequence_number,
        )
        global generate_sequence_number
        generate_sequence_number = _generate_sequence_number

    @patch('common.utils.sequence_utils.dynamodb_update_item')
    def test_generate_sequence_number(self, dynamodb_update_mock):

        # Given
        dynamodb_update_mock.return_value = EXAMPLE_RESPONSE

        # When
        expected_response = '00000002'
        actual_response = generate_sequence_number('test_sequence')

        # Then
        dynamodb_update_mock.assert_called_with(TableNames.SEQUENCES, dict(
            Key={'sequence_name': 'test_sequence'},
            UpdateExpression='ADD #sequence_value :increment',
            ExpressionAttributeValues={':increment': 1},
            ExpressionAttributeNames={'#sequence_value': 'sequence_value'},
            ReturnValues='UPDATED_NEW'
        ))
        self.assertEqual(expected_response, actual_response)
