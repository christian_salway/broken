from unittest import TestCase
from unittest.mock import Mock, patch

from common.utils.roles_utils import (
    get_role_name_from_role_hierarchy,
    get_role_code_from_role_hierarchy,
    get_organisation_name_from_organisation_code,
    get_filtered_roles_list_from_user_data
)


class TestGetRoles(TestCase):

    def test_can_get_role_name_when_role_name_contains_one_levels_of_role(self):
        nhs_identity_role_name = "\"Privacy Officer\""

        actual_role_name = get_role_name_from_role_hierarchy(nhs_identity_role_name)

        self.assertEqual("Privacy Officer", actual_role_name)

    def test_can_get_role_name_when_role_name_contains_multiple_levels_of_role(self):
        nhs_identity_role_name = "\"Admin & Clerical\":\"Management - A & C\":\"Registration Authority Manager\""

        actual_role_name = get_role_name_from_role_hierarchy(nhs_identity_role_name)

        self.assertEqual("Registration Authority Manager", actual_role_name)

    def test_can_get_role_code_when_role_code_contains_one_level(self):
        nhs_identity_role_code = 'R0000'

        actual_role_code = get_role_code_from_role_hierarchy(nhs_identity_role_code)

        self.assertEqual(nhs_identity_role_code, actual_role_code)

    def test_can_get_role_code_when_role_code_contains_multiple_levels(self):
        nhs_identity_role_code = "R0000:R0001:R0002"

        actual_role_code = get_role_code_from_role_hierarchy(nhs_identity_role_code)

        self.assertEqual('R0002', actual_role_code)

    def test_organisation_name_returned_given_organisation_with_organisation_code_exists(self):
        organisation_code = 'C60'

        nhsid_org_roles = [
            {'org_name': "York", 'org_code': 'A01'},
            {'org_name': "Manchester", 'org_code': 'C23'},
            {'org_name': "Leeds", 'org_code': 'C60'}
        ]

        organisation_name = get_organisation_name_from_organisation_code(organisation_code, nhsid_org_roles)

        self.assertEqual(organisation_name, 'Leeds')

    @patch('common.utils.roles_utils.get_role_name_from_role_hierarchy', Mock(return_value='my role'))
    @patch('common.utils.roles_utils.get_role_code_from_role_hierarchy', Mock(return_value='R0000'))
    @patch('common.utils.roles_utils.get_organisation_name_from_organisation_code', Mock(
        return_value='my organisation'))
    @patch('common.utils.roles_utils.allowed_roles', {'R0000': 'my role'})
    def test_can_get_roles_list_given_user_has_one_allowed_role_for_one_organisation(self):
        user_orgs = [{'org_code': 'A01', 'org_name': 'an organisation'}]
        nrbac_roles = [{'org_code': 'A01', 'role_code': 'role code', 'role_name': 'a role', 'activity_codes': []}]

        expected_roles = [{'role_id': 'R0000', 'role_name': 'my role', 'activity_codes': [],
                           'organisation_code': 'A01', 'organisation_name': 'my organisation', 'workgroups': []}]

        actual_roles = get_filtered_roles_list_from_user_data(user_orgs, nrbac_roles)

        self.assertListEqual(expected_roles, actual_roles)

    def test_returns_roles_list_in_alphabetical_order_given_user_has_multiple_allowed_roles_for_multiple_organisations(
            self):

        user_orgs = [
            {'org_code': 'A01', 'org_name': 'an organisation'},
            {'org_code': 'B01', 'org_name': 'another organisation'},
            {'org_code': 'C01', 'org_name': 'one more organisation'}
        ]
        nrbac_roles = [
            {'org_code': 'A01', 'role_code': 'R8000', 'role_name': 'the role', 'activity_codes': ['AB1']},
            {'org_code': 'B01', 'role_code': 'R8005', 'role_name': 'a role', 'activity_codes': []},
            {'org_code': 'A01', 'role_code': 'R8008', 'role_name': 'another role', 'activity_codes': ['CD2', 'EF3']}
        ]

        expected_roles = [
            {'role_id': 'R8005', 'role_name': 'a role', 'activity_codes': [], 'organisation_code': 'B01', 'organisation_name': 'another organisation', 'workgroups': []}, 
            {'role_id': 'R8008', 'role_name': 'another role', 'activity_codes': ['CD2', 'EF3'], 'organisation_code': 'A01', 'organisation_name': 'an organisation', 'workgroups': []}, 
            {'role_id': 'R8000', 'role_name': 'the role', 'activity_codes': ['AB1'], 'organisation_code': 'A01', 'organisation_name': 'an organisation', 'workgroups': []} 
        ]

        actual_roles = get_filtered_roles_list_from_user_data(user_orgs, nrbac_roles)
        self.maxDiff = None
        self.assertListEqual(expected_roles, actual_roles)

    @patch('common.utils.roles_utils.get_role_name_from_role_hierarchy', Mock(return_value='my role'))
    @patch('common.utils.roles_utils.get_role_code_from_role_hierarchy', Mock(return_value='R0000'))
    @patch('common.utils.roles_utils.get_organisation_name_from_organisation_code', Mock(
        return_value='my organisation'))
    @patch('common.utils.roles_utils.allowed_roles', {'not a role': 'test'})
    def test_returns_empty_roles_list_if_no_roles_allowed(self):
        user_orgs = [{'org_code': 'A01', 'org_name': 'an organisation'}]
        nrbac_roles = [{'org_code': 'A01', 'role_code': 'role code', 'role_name': 'a role'}]

        expected_roles = []

        actual_roles = get_filtered_roles_list_from_user_data(user_orgs, nrbac_roles)

        self.assertListEqual(expected_roles, actual_roles)

    def test_returns_only_allowed_roles_when_multiple_in_user_data(self):

        user_orgs = [{'org_code': 'A01', 'org_name': 'the org'}]
        nrbac_roles = [
            {'org_code': 'A01', 'role_code': 'R8010', 'role_name': 'a role 1', 'activity_codes': [], 'workgroups': ['group1', 'group2']}, 
            {'org_code': 'A01', 'role_code': 'R8010', 'role_name': 'a role 2', 'activity_codes': ['AB1', 'CD2']},
            {'org_code': 'A01', 'role_code': 'R8004', 'role_name': 'not allowed role', 'activity_codes': []},
        ]

        expected_roles = [
            {'role_id': 'R8010', 'role_name': 'a role 1', 'activity_codes': [], 'organisation_code': 'A01', 'organisation_name': 'the org', 'workgroups': ['group1', 'group2']}, 
            {'role_id': 'R8010', 'role_name': 'a role 2', 'activity_codes': ['AB1', 'CD2'], 'organisation_code': 'A01', 'organisation_name': 'the org', 'workgroups': []} 
        ]

        actual_roles = get_filtered_roles_list_from_user_data(user_orgs, nrbac_roles)

        self.assertListEqual(expected_roles, actual_roles)

    def test_can_return_role_without_workgroups_or_activity_codes(self):
        user_orgs = [{'org_code': 'A01', 'org_name': 'the org'}]
        nrbac_roles = [
            {'org_code': 'A01', 'role_code': 'R8010', 'role_name': 'a role'},
        ]

        expected_roles = [
            {'role_id': 'R8010', 'role_name': 'a role', 'activity_codes': [], 'organisation_code': 'A01', 'organisation_name': 'the org', 'workgroups': []} 
        ]

        actual_roles = get_filtered_roles_list_from_user_data(user_orgs, nrbac_roles)

        self.assertListEqual(expected_roles, actual_roles)
