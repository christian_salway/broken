from unittest import TestCase
from mock import patch, Mock
from jinja2 import Template


class EmailUtilsTests(TestCase):

    @classmethod
    def setUpClass(cls):
        global email_utils
        import common.utils.email_utils as email_utils

    @classmethod
    def setUp(self):
        email_utils.ENV = Mock()
        email_utils.BUCKET = 'TEST'

    @patch('common.utils.email_utils.get_file_contents_from_bucket')
    def test_should_load_templates_from_s3_bucket(self, s3_get_object_mock):

        # Given
        object_key = 'templates.html'
        get_contents_response = 'templates_html'
        s3_get_object_mock.return_value = get_contents_response
        # When
        expected_response = get_contents_response
        actual_response = email_utils.load_templates_from_s3(object_key)
        # Then
        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.email_utils.get_file_contents_from_bucket')
    def test_should_build_templates_correctly(self, s3_get_object_mock):

        jinja_template = '''<html><head><title>{{ data['title'] }}</title></head>\
<body><h1>{{data['content']}}</h1></body></html>'''

        get_template_response = Template(jinja_template)
        email_utils.ENV.get_template.return_value = get_template_response

        built_template = '<html><head><title>My Title</title></head><body><h1>Hello World!</h1></body></html>'
        s3_get_object_mock.return_value = jinja_template

        email_type = 'prior_notification_email'
        input_vars = {
            'title': 'My Title',
            'content': 'Hello World!'
        }
        actual_response = email_utils.build_message_body(email_type, input_vars)
        self.assertEqual(built_template, actual_response)
