import unittest
import decimal
import json
from mock import patch, Mock, MagicMock, call

from common.utils.dynamodb_access.table_names import TableNames

with patch('boto3.resource') as resource_mock:
    from common.utils.dynamodb_helper_utils import (
        build_projection_expression,
        build_update_query,
        paginate,
        DecimalEncoder,
        build_update_record,
        build_put_record,
        build_delete_record)


DYNAMODB_TABLE_MOCK = Mock()
PARTICIPANT_ID = 12345678
NHS_NUMBER = 23456789
EXAMPLE_RECORD = {'name': 'Struan'}
EXAMPLE_PARTICIPANT = {'name': 'Paul'}
EXAMPLE_RESPONSE = {'success': 'true'}
EXAMPLE_UUID = '98f1f2c2-fd40-4e75-9d36-d171479db19d'


@patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'participants-table'})
class TestDynamoDBHelperUtils(unittest.TestCase):

    def test_build_projection_expression_returns_correct_expression(self):
        projection_expression = ["value", "status", "some-other-value"]

        built_projection_attributes = build_projection_expression(projection_expression)

        expected_projection_expression = '#value, #status, #some-other-value'
        expected_expression_attribute_names = {
            '#value': 'value',
            '#status': 'status',
            '#some-other-value': 'some-other-value',
        }

        actual_projection_expression = built_projection_attributes['projection_expression']
        actual_expression_attribute_names = built_projection_attributes['expression_attribute_names']

        self.assertEqual(expected_projection_expression, actual_projection_expression)
        self.assertDictEqual(expected_expression_attribute_names, actual_expression_attribute_names)

    def test_build_update_query_builds_set_clause(self):
        expression, names, values = build_update_query(
            update_attributes={'key1': 'value1', 'key2': 'value2'}
        )
        self.assertEqual(expression, 'SET #key1 = :key1, #key2 = :key2')
        self.assertEqual(names, {'#key1': 'key1', '#key2': 'key2'})
        self.assertEqual(values, {':key1': 'value1', ':key2': 'value2'})

    def test_build_update_query_builds_delete_clause(self):
        expression, names, values = build_update_query(
            delete_attributes=['key1', 'key2']
        )
        self.assertEqual(expression, 'REMOVE #key1, #key2')
        self.assertEqual(names, {'#key1': 'key1', '#key2': 'key2'})
        self.assertEqual(values, {})

    def test_build_update_query_combines_clauses(self):
        expression, names, values = build_update_query(
            update_attributes={'key1': 'value1'},
            delete_attributes=['key2']
        )
        self.assertEqual(expression, 'SET #key1 = :key1 REMOVE #key2')
        self.assertEqual(names, {'#key1': 'key1', '#key2': 'key2'})
        self.assertEqual(values, {':key1': 'value1'})

    def test_decimal_encoder_return_current_value(self):
        session_data = {
            'session_token': decimal.Decimal(1543000001.21)
        }

        expected_json = '{"session_token": 1543000001.21}'
        formated_json = json.dumps(session_data, cls=DecimalEncoder)
        self.assertEqual(formated_json, expected_json)

    def test_paginate_calls_wrapped_function(self):
        @paginate
        def wrapped_call(options):
            if 'ExclusiveStartKey' in options:
                return {'Items': [{'some': 'key'}]}
            else:
                return {
                    'Items': [{'some': 'key'}],
                    'LastEvaluatedKey': {'some': 'key'}
                }

        mocked_query = MagicMock(side_effect=wrapped_call)
        mocked_query({})

        mocked_query.assert_has_calls([
            call({'ExclusiveStartKey': {'some': 'key'}})
        ])

    def test_build_update_record_function(self):
        update_record = {
            'record_keys': {
                'participant_id': 'PARTICIPANT2',
                'sort_key': 'SORT_KEY2',
            },
            'update_values': {
                'status': 'R',
                'next_test_due_date': '2022-01-01',
            },
            'delete_values': ['crm_number']
        }
        expected_result = {
            'Update': {
                'TableName': 'participants-table',
                'Key': {
                    'participant_id': 'PARTICIPANT2',
                    'sort_key': 'SORT_KEY2'
                },
                'UpdateExpression': 'SET #status = :status, ' +
                                    '#next_test_due_date = :next_test_due_date REMOVE #crm_number',
                'ExpressionAttributeValues': {
                    ':status': 'R', ':next_test_due_date': '2022-01-01'
                },
                'ExpressionAttributeNames': {
                    '#status': 'status', '#next_test_due_date': 'next_test_due_date', '#crm_number': 'crm_number'
                }
            }
        }
        actual_result = build_update_record(update_record, TableNames.PARTICIPANTS)
        self.assertEqual(expected_result, actual_result)

    def test_build_put_record_function(self):
        item = {
            'id': '1234'
        }
        expected_result = {
            'Put': {
                'TableName': 'participants-table',
                'Item': {
                    'id': '1234'
                },
            }
        }
        actual_result = build_put_record(item, TableNames.PARTICIPANTS)
        self.assertEqual(expected_result, actual_result)

    def test_build_delete_record_function(self):
        item = {
            'id': '1234'
        }
        expected_result = {
            'Delete': {
                'TableName': 'participants-table',
                'Key': {
                    'id': '1234'
                },
            }
        }
        actual_result = build_delete_record(item, TableNames.PARTICIPANTS)
        self.assertEqual(expected_result, actual_result)
