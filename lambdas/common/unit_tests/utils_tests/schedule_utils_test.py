import unittest
from datetime import datetime, timezone
from mock import patch
from ddt import ddt, data, unpack
import common.utils.schedule_utils as schedule_utils


@ddt
class TestScheduleUtils(unittest.TestCase):

    @unpack
    @data(('something', None), (None, 'something'), (None, None))
    def test_check_schedule_returns_false_with_missing_parameters(self, schedule_expression, local_time_zone_str):
        expected_response = False
        actual_response = schedule_utils.check_schedule(schedule_expression, local_time_zone_str)
        self.assertEqual(actual_response, expected_response)

    @unpack
    @data((2020, 1, 1,  9, 0, 0, 'Europe/London', '* 10 * * ? *', False),
          (2020, 1, 1, 10, 0, 0, 'Europe/London', '* 10 * * ? *', True),
          (2020, 5, 1,  9, 0, 0, 'Europe/London', '* 10 * * ? *', True),
          (2020, 5, 1, 10, 0, 0, 'Europe/London', '* 10 * * ? *', False),
          (2020, 5, 1,  9, 0, 0, '', '* 10 * * ? *', False))
    @patch('common.utils.schedule_utils.datetime')
    def test_check_schedule_handles_daylight_saving(self,
                                                    year, month, day, hour, minute, second, local_time_zone_str,
                                                    schedule_expression, expected_response,
                                                    datetime_mock):

        simulated_time = datetime(year=year, month=month, day=day, hour=hour, minute=minute, second=second,
                                  tzinfo=timezone.utc)
        datetime_mock.now.return_value = simulated_time
        actual_response = schedule_utils.check_schedule(schedule_expression, local_time_zone_str)
        self.assertEqual(actual_response, expected_response)
