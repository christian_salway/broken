from unittest import TestCase
from mock import patch

with patch('boto3.resource') as resource_mock:
    from common.utils.api_utils import get_api_request_info_from_event


@patch('common.utils.api_utils.get_session_from_lambda_event')
class TestAPIUtils(TestCase):
    def test_get_api_info_retrieves_full_info(self, session_mock):
        session_mock.return_value = 'session'
        event = {
            'body': '[]',
            'pathParameters': {
                'type': 'hello',
                'blah': '12'
            },
            'queryStringParameters': {
                'type': 'afterwards',
                'seventeen': 'optics'
            },
            'headers': {
                'haha': 'yes',
                'nearly': 'there'
            },
            'actual': 53,
            'httpMethod': 'GET',
            'resource': 'api/x/y/hello/no'
        }
        actual_info, actual_method, actual_resource = get_api_request_info_from_event(event)
        expected_info = {
            'body': [],
            'path_parameters': {
                'type': 'hello',
                'blah': '12'
            },
            'query_parameters': {
                'type': 'afterwards',
                'seventeen': 'optics'
            },
            'session': 'session'
        }
        expected_method = 'GET'
        expected_resource = 'api/x/y/hello/no'
        self.assertEqual(expected_method, actual_method)
        self.assertEqual(expected_resource, actual_resource)
        self.assertDictEqual(expected_info, actual_info)

    def test_get_api_info_retrieves_partial_info(self, session_mock):
        session_mock.return_value = 'session'
        event = {
            'body': '[]',
            'pathParameters': None,
            'queryStringParameters': {
                'type': 'afterwards',
                'seventeen': 'optics'
            },
            'headers': {
                'haha': 'yes',
                'nearly': 'there'
            },
            'actual': 53,
            'httpMethod': 'POST',
            'resource': 'api/x/y/hello/no'
        }
        actual_info, actual_method, actual_resource = get_api_request_info_from_event(event)
        expected_info = {
            'body': [],
            'path_parameters': {},
            'query_parameters': {
                'type': 'afterwards',
                'seventeen': 'optics'
            },
            'session': 'session'
        }
        expected_method = 'POST'
        expected_resource = 'api/x/y/hello/no'
        self.assertEqual(expected_method, actual_method)
        self.assertEqual(expected_resource, actual_resource)
        self.assertDictEqual(expected_info, actual_info)
