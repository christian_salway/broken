from mock import patch
from unittest import TestCase
from mock import Mock
from datetime import datetime, timezone
from boto3.dynamodb.conditions import Key

from common.models.participant import GPNotificationStatus
from common.utils.dynamodb_access.table_names import TableNames

GP_ID_INDEX_NAME = 'live-record-status'

EXAMPLE_PARTICIPANT = {'name': 'Paul'}


class TestEpisodeUtils(TestCase):

    example_date = datetime(2021, 1, 1, 0, 0, 0, 123456, tzinfo=timezone.utc)
    example_date_as_string = '2021-01-01T00:00:00.123456+00:00'
    datetime_mock = Mock(wraps=datetime)
    datetime_mock.now.return_value = example_date

    @patch('datetime.datetime', datetime_mock)
    @patch('boto3.client')
    @patch('boto3.resource')
    def setUp(self, boto3_resource, boto3_client):

        # Setup database mock
        organisation_supplementary_table_mock = Mock()
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = organisation_supplementary_table_mock
        boto3_resource.return_value = boto3_table_mock
        self.organisation_supplementary_table_mock = organisation_supplementary_table_mock

        # Setup lambda module
        import common.utils.participant_gp_notification_utils as _participant_gp_notification_utils
        self.gp_notification_utils = _participant_gp_notification_utils

    @patch('common.utils.participant_gp_notification_utils.segregated_query')
    def test_query_for_participant_by_gp_id_and_episode_live_record_status(self, query_mock):

        # Given
        query_mock.return_value = [EXAMPLE_PARTICIPANT, EXAMPLE_PARTICIPANT]

        # When
        expected_response = [EXAMPLE_PARTICIPANT, EXAMPLE_PARTICIPANT]
        actual_response = \
            self.gp_notification_utils.query_for_participant_by_gp_id_and_gp_notification_live_record_status(
                'A60', GPNotificationStatus.NEW_NOTIFICATION
            )

        # Then

        query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=Key('live_record_status').eq('NEW_NOTIFICATION') &
            Key('registered_gp_practice_code_and_test_due_date').begins_with('A60'),
            IndexName=GP_ID_INDEX_NAME
        ))

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.participant_gp_notification_utils.dynamodb_query')
    def test_query_for_gp_notifications(self, query_mock):

        def callback_function(response):
            pass

        # Given
        query_mock.return_value = {'Items': [EXAMPLE_PARTICIPANT, EXAMPLE_PARTICIPANT]}

        # When
        self.gp_notification_utils.query_for_gp_notifications(self.example_date_as_string, callback_function)

        # Then

        query_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            IndexName='live-record-status-only',
            KeyConditionExpression=Key('live_record_status').eq('NEW_NOTIFICATION'),
            FilterExpression=Key('expires').lte(self.example_date_as_string)),
            return_all=True
        )

    @patch('common.utils.participant_gp_notification_utils.dynamodb_update_item')
    def test_archive_gp_notification(self, update_item_mock):

        # Given
        record = {
            'participant_id': 'GP_NOTIFICATION#gp#creation'
        }

        # When
        self.gp_notification_utils.archive_gp_notification(record, self.example_date_as_string)

        # Then

        update_item_mock.assert_called_with(TableNames.PARTICIPANTS, dict(
            Key={'participant_id': 'GP_NOTIFICATION#gp#creation'},
            UpdateExpression='SET #status = :status, #archived_date = :archived_date REMOVE #live_record_status',
            ExpressionAttributeValues={
                ':status': 'ARCHIVED_NOTIFICATION',
                ':archived_date': '2021-01-01T00:00:00.123456+00:00'
            },
            ExpressionAttributeNames={
                '#status': 'status',
                '#archived_date': 'archived_date',
                '#live_record_status': 'live_record_status'
            },
            ReturnValues='NONE'
        ))
