from unittest.case import TestCase
from mock import patch, Mock, call
from common.log_references import CommonLogReference

mock_env_vars_no_ssl = {
    'MESH_HOST': 'the_host',
    'MESH_ENVIRONMENT': 'the_env',
    'MESH_VERIFY_SSL': 'FALSE'
}

with patch('os.environ', mock_env_vars_no_ssl):
    import common.utils.mesh.mesh_client_utils as mesh_client_utils_module


class TestMeshClientUtils(TestCase):

    module = 'common.utils.mesh.mesh_client_utils'

    @patch(f'{module}.log')
    @patch(f'{module}.MeshClient')
    @patch(f'{module}.get_mesh_certificates_by_mailbox')
    @patch(f'{module}.get_secret_by_key')
    def test_get_mesh_client(self, get_secret_mock, get_certs_mock, mesh_client_mock, log_mock):
        get_secret_mock.return_value = 'the_shared_key'
        get_certs_mock.return_value = 'the_client_cert_file_name', 'the_client_key_file_name', 'the_ca_cert_file_name'
        mesh_client_instance = Mock()
        mesh_client_mock.return_value = mesh_client_instance

        client = mesh_client_utils_module.get_mesh_client('the_mailbox_id', 'the_password', 'the_temp_dir')

        get_secret_mock.assert_called_once_with('mesh_shared_key', 'the_env')
        get_certs_mock.assert_called_once_with('the_mailbox_id', 'the_temp_dir')
        mesh_client_mock.assert_called_with(url='the_host',
                                            mailbox='the_mailbox_id',
                                            password='the_password',
                                            shared_key=b'the_shared_key',
                                            cert=('the_client_cert_file_name', 'the_client_key_file_name'),
                                            verify='the_ca_cert_file_name')
        client.handshake.assert_called_once()
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.MESHCLIENT0010, 'mesh_environment': 'the_env'}),
            call({'log_reference': CommonLogReference.MESHCLIENT0011}),
            call({'log_reference': CommonLogReference.MESHCLIENT0020}),
            call({'log_reference': CommonLogReference.MESHCLIENT0021}),
            call({'log_reference': CommonLogReference.MESHCLIENT0022})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.tempfile')
    @patch(f'{module}.get_secrets_dictionary')
    def test_get_mesh_certificates_by_mailbox_given_no_ssl_and_known_mailbox_id(
            self, get_secrets_mock, tempfile_mock, log_mock):
        mesh_client_certs = {'ABC123': 'ABC123_client_cert', 'the_env': 'the_env_client_cert'}
        mesh_client_private_keys = {'ABC123': 'ABC123_private_key', 'the_env': 'the_env_private_key'}
        get_secrets_mock.side_effect = [mesh_client_certs, mesh_client_private_keys]

        mesh_client_cert_file = Mock()
        mesh_client_cert_file.name = 'the_client_cert_filename'
        mesh_client_private_key_file = Mock()
        mesh_client_private_key_file.name = 'the_private_key_filename'
        tempfile_mock.NamedTemporaryFile.side_effect = [mesh_client_cert_file, mesh_client_private_key_file]

        mesh_client_cert_file_name, mesh_client_private_key_file_name, mesh_ca_cert_name = \
            mesh_client_utils_module.get_mesh_certificates_by_mailbox('ABC123', 'the_temp_dir_name')

        self.assertEqual('the_client_cert_filename', mesh_client_cert_file_name)
        self.assertEqual('the_private_key_filename', mesh_client_private_key_file_name)
        self.assertIsNone(mesh_ca_cert_name)

        self.assertEqual(tempfile_mock.NamedTemporaryFile.call_count, 2)
        tempfile_mock.NamedTemporaryFile.assert_has_calls([call(dir='the_temp_dir_name', delete=False),
                                                           call(dir='the_temp_dir_name', delete=False)])
        mesh_client_cert_file.write.assert_called_once_with(b'ABC123_client_cert')
        mesh_client_private_key_file.write.assert_called_once_with(b'ABC123_private_key')

        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.MESHCLIENT0012}),
            call({'log_reference': CommonLogReference.MESHCLIENT0013}),
            call({'log_reference': CommonLogReference.MESHCLIENT0014}),
            call({'log_reference': CommonLogReference.MESHCLIENT0015, 'use_mailbox_cert': True,
                  'use_mailbox_key': True}),
            call({'log_reference': CommonLogReference.MESHCLIENT0016}),
            call({'log_reference': CommonLogReference.MESHCLIENT0017})
        ])

    @patch(f'{module}.MESH_VERIFY_SSL', True)
    @patch(f'{module}.log')
    @patch(f'{module}.tempfile')
    @patch(f'{module}.get_secrets_dictionary')
    def test_get_mesh_certificates_by_mailbox_given_ssl_verification_required_and_known_mailbox_id(
            self, get_secrets_mock, tempfile_mock, log_mock):
        mesh_client_certs = {'ABC123': 'ABC123_client_cert', 'the_env': 'the_env_client_cert'}
        mesh_client_private_keys = {'ABC123': 'ABC123_private_key', 'the_env': 'the_env_private_key'}
        mesh_ca_certs = {'the_env': 'the_env_ca_cert'}
        get_secrets_mock.side_effect = [mesh_client_certs, mesh_client_private_keys, mesh_ca_certs]

        mesh_client_cert_file = Mock()
        mesh_client_cert_file.name = 'the_client_cert_filename'
        mesh_client_private_key_file = Mock()
        mesh_client_private_key_file.name = 'the_private_key_filename'
        mesh_ca_cert_file = Mock()
        mesh_ca_cert_file.name = 'the_ca_cert_filename'
        tempfile_mock.NamedTemporaryFile.side_effect = [mesh_client_cert_file, mesh_client_private_key_file,
                                                        mesh_ca_cert_file]

        mesh_client_cert_file_name, mesh_client_private_key_file_name, mesh_ca_cert_name = \
            mesh_client_utils_module.get_mesh_certificates_by_mailbox('ABC123', 'the_temp_dir_name')

        self.assertEqual('the_client_cert_filename', mesh_client_cert_file_name)
        self.assertEqual('the_private_key_filename', mesh_client_private_key_file_name)
        self.assertEqual('the_ca_cert_filename', mesh_ca_cert_name)

        self.assertEqual(tempfile_mock.NamedTemporaryFile.call_count, 3)
        tempfile_mock.NamedTemporaryFile.assert_has_calls([call(dir='the_temp_dir_name', delete=False),
                                                           call(dir='the_temp_dir_name', delete=False),
                                                           call(dir='the_temp_dir_name', delete=False)])
        mesh_client_cert_file.write.assert_called_once_with(b'ABC123_client_cert')
        mesh_client_private_key_file.write.assert_called_once_with(b'ABC123_private_key')
        mesh_ca_cert_file.write.assert_called_once_with(b'the_env_ca_cert')

        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.MESHCLIENT0012}),
            call({'log_reference': CommonLogReference.MESHCLIENT0013}),
            call({'log_reference': CommonLogReference.MESHCLIENT0014}),
            call({'log_reference': CommonLogReference.MESHCLIENT0015, 'use_mailbox_cert': True,
                  'use_mailbox_key': True}),
            call({'log_reference': CommonLogReference.MESHCLIENT0016}),
            call({'log_reference': CommonLogReference.MESHCLIENT0017}),
            call({'log_reference': CommonLogReference.MESHCLIENT0018}),
            call({'log_reference': CommonLogReference.MESHCLIENT0019})
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.tempfile')
    @patch(f'{module}.get_secrets_dictionary')
    def test_get_mesh_certificates_by_mailbox_given_no_ssl_and_unknown_mailbox_id(
            self, get_secrets_mock, tempfile_mock, log_mock):
        mesh_client_certs = {'ABC123': 'ABC123_client_cert', 'the_env': 'the_env_client_cert'}
        mesh_client_private_keys = {'ABC123': 'ABC123_private_key', 'the_env': 'the_env_private_key'}
        get_secrets_mock.side_effect = [mesh_client_certs, mesh_client_private_keys]

        mesh_client_cert_file = Mock()
        mesh_client_cert_file.name = 'the_client_cert_filename'
        mesh_client_private_key_file = Mock()
        mesh_client_private_key_file.name = 'the_private_key_filename'
        tempfile_mock.NamedTemporaryFile.side_effect = [mesh_client_cert_file, mesh_client_private_key_file]

        mesh_client_cert_file_name, mesh_client_private_key_file_name, mesh_ca_cert_name = \
            mesh_client_utils_module.get_mesh_certificates_by_mailbox('feature_test', 'the_temp_dir_name')

        self.assertEqual('the_client_cert_filename', mesh_client_cert_file_name)
        self.assertEqual('the_private_key_filename', mesh_client_private_key_file_name)
        self.assertIsNone(mesh_ca_cert_name)

        self.assertEqual(tempfile_mock.NamedTemporaryFile.call_count, 2)
        tempfile_mock.NamedTemporaryFile.assert_has_calls([call(dir='the_temp_dir_name', delete=False),
                                                           call(dir='the_temp_dir_name', delete=False)])
        mesh_client_cert_file.write.assert_called_once_with(b'the_env_client_cert')
        mesh_client_private_key_file.write.assert_called_once_with(b'the_env_private_key')

        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.MESHCLIENT0012}),
            call({'log_reference': CommonLogReference.MESHCLIENT0013}),
            call({'log_reference': CommonLogReference.MESHCLIENT0014}),
            call({'log_reference': CommonLogReference.MESHCLIENT0015, 'use_mailbox_cert': False,
                  'use_mailbox_key': False}),
            call({'log_reference': CommonLogReference.MESHCLIENT0016}),
            call({'log_reference': CommonLogReference.MESHCLIENT0017})
        ])

    @patch(f'{module}.log')
    def test_get_message_contents_by_message_id(self, log_mock):
        message_mock = Mock()
        message_mock.mex_header.side_effect = ['the_file_name', 'the_sender']
        message_mock.read.return_value = 'the_message_contents'
        mesh_client_mock = Mock()
        mesh_client_mock.retrieve_message.return_value = message_mock

        file_name, from_mailbox_id, file_contents = \
            mesh_client_utils_module.get_message_contents_by_message_id('123456', mesh_client_mock)
        self.assertEqual('the_file_name', file_name)
        self.assertEqual('the_sender', from_mailbox_id)
        self.assertEqual('the_message_contents', file_contents)

        mesh_client_mock.retrieve_message.assert_called_with('123456')
        self.assertEqual(message_mock.mex_header.call_count, 2)
        message_mock.mex_header.assert_has_calls([call('filename'), call('from')])
        message_mock.read.assert_called_once()
        message_mock.close.assert_called_once()

        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.MESHCLIENT0023}),
            call({'log_reference': CommonLogReference.MESHCLIENT0024}),
            call({'log_reference': CommonLogReference.MESHCLIENT0025, 'file_name': 'the_file_name',
                  'from_mailbox_id': 'the_sender'})
        ])
