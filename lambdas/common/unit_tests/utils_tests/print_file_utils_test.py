import unittest
from mock import patch
from mock import Mock
from common.models.letters import PrintFileStatus
from common.utils.dynamodb_access.table_names import TableNames


EXAMPLE_FILE_NAME = "CSAS_EU-WEST-2_CP_2106030622.dat"
EXAMPLE_PRINT_FILE = {
    "file_name": EXAMPLE_FILE_NAME,
    "sort_key":  "CIC",
    "type":      "invite-reminder",
    "status":    "PENDING",
    "row_count": "15000",
    "created":   "2020-01-23T13:48:08.000+00:00",
    "lock":      False
}


@patch('os.environ', {'DYNAMODB_PRINT_FILES': 'print_files_table'})
class TestPrintFilesUtils(unittest.TestCase):

    @patch('boto3.client')
    @patch('boto3.resource')
    def setUp(self, boto3_resource, boto3_client):

        # Setup database mock
        print_file_table_mock = Mock()
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = print_file_table_mock
        boto3_resource.return_value = boto3_table_mock
        self.print_file_table_mock = print_file_table_mock

        # Setup lambda module
        import common.utils.print_file_utils as _print_file_utils
        self.print_file_utils = _print_file_utils

    @patch('common.utils.print_file_utils.dynamodb_get_item')
    def test_get_print_file_by_file_name_calls_through(self, get_item_mock):

        # Given
        get_item_mock.return_value = EXAMPLE_PRINT_FILE

        # When
        expected_response = EXAMPLE_PRINT_FILE
        actual_response = self.print_file_utils.get_print_file_by_file_name(EXAMPLE_FILE_NAME, 'sort_key')

        # Then
        get_item_mock.assert_called_with(TableNames.PRINT_FILES, {
            'file_name': EXAMPLE_FILE_NAME,
            'sort_key': 'sort_key'
        })

        self.assertEqual(expected_response, actual_response)

    @patch('common.utils.print_file_utils.dynamodb_update_item')
    def test_update_print_file_by_filename(self, update_mock):
        # Given
        filename = 'test_file.dat'
        sort_key = 'CIC'
        status = PrintFileStatus.WITH_PRINTER

        # When
        self.print_file_utils.update_print_file_by_file_name(filename, sort_key, status)

        # Then
        update_mock.assert_called_with(TableNames.PRINT_FILES, dict(
            Key={'file_name': filename, 'sort_key': sort_key},
            UpdateExpression='SET #status = :status',
            ExpressionAttributeValues={
                ':status': status
            },
            ExpressionAttributeNames={
                '#status': 'status'
            },
            ReturnValues='NONE'
        ))

    @patch('common.utils.print_file_utils.dynamodb_get_item')
    def test_print_file_throws_exception_when_lock_is_aquired(self, get_item_mock):
        test_key = 'print_file_key'
        test_sort_key = 'sort_key'
        test_print_file_status = PrintFileStatus.PENDING
        EXAMPLE_PRINT_FILE['lock'] = True
        get_item_mock.return_value = EXAMPLE_PRINT_FILE

        @self.print_file_utils.lock_printfile
        def dummy_function(key, sort_key, print_file_status):
            self.assertEqual(key, test_key)
            self.assertEqual(sort_key, test_sort_key)
            self.assertEqual(print_file_status, test_print_file_status)

        with self.assertRaises(Exception) as ex:
            dummy_function(test_key, test_sort_key, test_print_file_status)

        self.assertEqual('Print file print_file_key is already locked', ex.exception.args[0])

    @patch('common.utils.print_file_utils.dynamodb_get_item')
    @patch('common.utils.print_file_utils.dynamodb_update_item')
    def test_print_file_aquires_lock(self, update_mock, get_item_mock):
        test_key = 'print_file_key'
        test_sort_key = 'CIC'
        test_print_file_status = PrintFileStatus.PENDING
        get_item_mock.return_value = EXAMPLE_PRINT_FILE

        @self.print_file_utils.lock_printfile
        def dummy_function(key, sort_key, print_file_status):
            self.assertEqual(key, test_key)
            self.assertEqual(sort_key, test_sort_key)
            self.assertEqual(print_file_status, test_print_file_status)

        dummy_function(test_key, test_sort_key, test_print_file_status)

        update_mock.assert_called_with(TableNames.PRINT_FILES, dict(
            Key={'file_name': test_key, 'sort_key': test_sort_key},
            UpdateExpression='SET #lock = :lock',
            ExpressionAttributeValues={
                ':lock': True
            },
            ExpressionAttributeNames={
                '#lock': 'lock'
            },
            ReturnValues='NONE'
        ))

    @patch('common.utils.print_file_utils.dynamodb_get_item')
    @patch('common.utils.print_file_utils.dynamodb_update_item')
    def test_print_file_unlock(self, update_mock, get_item_mock):
        test_key = 'print_file_key'
        test_sort_key = 'CIC'
        get_item_mock.return_value = EXAMPLE_PRINT_FILE

        @self.print_file_utils.unlock_printfile
        def dummy_function(key, sort_key):
            self.assertEqual(key, test_key)
            self.assertEqual(sort_key, test_sort_key)

        dummy_function(test_key, test_sort_key)

        update_mock.assert_called_with(TableNames.PRINT_FILES, dict(
            Key={'file_name': test_key, 'sort_key': test_sort_key},
            UpdateExpression='SET #lock = :lock',
            ExpressionAttributeValues={
                ':lock': False
            },
            ExpressionAttributeNames={
                '#lock': 'lock'
            },
            ReturnValues='NONE'
        ))
