from unittest import TestCase
from mock import call, patch
from common.utils.lambda_wrappers import lambda_entry_point, auth_lambda, api_lambda, auth_exception_wrapper
from common.test_mocks.mock_events import sqs_mock_event
from common.log_references import CommonLogReference

module = 'common.utils.lambda_wrappers'


class TestUtils(TestCase):

    def setUp(self):
        self.log_patcher = patch(f'{module}.log').start()
        self.initialize_logger_patcher = patch(f'{module}.initialize_logger').start()
        self.update_logger_metadata_fields_patcher = patch(
            f'{module}.update_logger_metadata_fields').start()
        self.set_internal_id_patcher = patch(f'{module}.set_internal_id').start()
        self.log_patcher = patch(f'{module}.log').start()
        self.create_internal_id_patcher = patch('common.utils.create_internal_id').start()
        self.is_deep_ping_patcher = patch(f'{module}.is_deep_ping').start()
        self.handle_deep_ping_patcher = patch(f'{module}.handle_deep_ping').start()

        self.create_internal_id_patcher.return_value = 'internal_id'
        self.is_deep_ping_patcher.return_value = False
        self.handle_deep_ping_patcher.return_value = 'response'

    def test_lambda_entry_point_decorator_calls_initialize_logger(self):

        @lambda_entry_point
        def entry_point(event, context):
            pass

        entry_point(sqs_mock_event({}), {})

        self.initialize_logger_patcher.assert_called()
        self.handle_deep_ping_patcher.assert_not_called()

    def test_lambda_entry_point_passes_the_event_and_context_down_to_the_lambda(self):
        @lambda_entry_point
        def entry_point(event, context):
            self.assertEqual(sqs_mock_event({}), event)
            self.assertEqual('context', context)

        entry_point(sqs_mock_event({}), 'context')

    def test_lambda_entry_point_passes_the_return_value_back(self):
        @lambda_entry_point
        def entry_point(event, context):
            return 'something'

        actual = entry_point(sqs_mock_event({}), 'context')

        self.assertEqual(actual, 'something')

    def test_lambda_entry_point_propagates_the_raised_exception(self):
        @lambda_entry_point
        def entry_point(event, context):
            raise Exception

        with self.assertRaises(Exception):
            entry_point(sqs_mock_event({}), 'context')

    def test_raises_exception_if_not_exactly_one_record(self):
        event_with_2_records = {'Records': [
            {'messageId': '98f1f2c2-fd40-4e75-9d36-d171479db19d', 'body': '{"internal_id":"123123123"}'},
            {'messageId': '98f1f2c2-fd40-4e75-9d36-d171479db19d', 'body': '{"internal_id":"123123123"}'}
        ]}

        @lambda_entry_point
        def entry_point(event, context):
            self.fail('Expected exception to be thrown reaching this point')

        with self.assertRaises(Exception) as context:
            entry_point(event_with_2_records, 'context')

        self.assertEqual(str(context.exception), 'Expected exception to be thrown reaching this point')

    def test_lambda_entry_point_logs_the_starting_of_the_lambda(self):
        @lambda_entry_point
        def entry_point(event, context):
            pass

        entry_point(sqs_mock_event({'internal_id': 'internal_id'}), {})

        actual_args, _ = self.log_patcher.call_args_list[0]
        self.assertEqual(actual_args[0], CommonLogReference.LAMBDA0000)

    def test_lambda_entry_point_logs_the_successful_ending_of_the_lambda(self):
        @lambda_entry_point
        def entry_point(event, context):
            pass

        entry_point(sqs_mock_event({'internal_id': 'internal_id'}), {})

        self.log_patcher.assert_has_calls([
            call(CommonLogReference.LAMBDA0000),
            call(CommonLogReference.LAMBDA0008),
            call(CommonLogReference.LAMBDA0001)
        ])
        self.assertEqual(3, self.log_patcher.call_count)

    def test_lambda_entry_point_logs_the_failed_ending_of_the_lambda(self):
        @lambda_entry_point
        def entry_point(event, context):
            raise Exception('test exception')

        try:
            entry_point(sqs_mock_event({'internal_id': 'internal_id'}), {})
        except Exception:
            pass
        self.log_patcher.assert_has_calls([
            call(CommonLogReference.LAMBDA0000),
            call(CommonLogReference.LAMBDA0008),
            call(CommonLogReference.EXCEPTION000),
            call(CommonLogReference.LAMBDA0002)
        ])
        self.assertEqual(4, self.log_patcher.call_count)

    def test_lambda_entry_point_calls_handle_deep_ping_if_event_is_deep_ping(self):
        # Arrange
        self.is_deep_ping_patcher.return_value = True
        expected_response = 'response'

        @lambda_entry_point
        def entry_point(event, context):
            pass

        # Act
        response = entry_point(sqs_mock_event({'internal_id': 'internal_id'}), {})
        # Assert
        self.assertEqual(expected_response, response)
        self.handle_deep_ping_patcher.assert_called_once()

    @patch('common.utils.lambda_wrappers.delete_cookie_header')
    def test_auth_exception_wrapper_returns_302_redirect_when_exception_occurs(self, del_cookie_mock):
        expected_cookie_header = 'sp_session_cookie=""; expires=Wed, 29 Jan 2020 00:00:00 GMT; Secure'
        del_cookie_mock.return_value = expected_cookie_header
        exception_message = 'test exception'
        test_auth_event = {
            'headers': {
                'Cookie': 'sp_session_cookie="whatever"'
            }
        }

        @auth_exception_wrapper
        @lambda_entry_point
        def auth_entry_point(event, context):
            raise Exception(exception_message)

        try:
            response = auth_entry_point(test_auth_event, {})
        except Exception:
            pass

        self.assertEqual(response, {
            'statusCode': 302,
            'headers': {
                'location': 'None?reason=unauthorised',
                'Cookie': expected_cookie_header
            }
        })

    @patch('common.utils.lambda_wrappers.get_session')
    @patch('common.utils.lambda_wrappers.get_session_from_cookie')
    def test_auth_lambda_passes_session_to_lambda(self, cookie_mock, session_mock):
        expected_session = {
            'session_id': 'session_id123456',
            'session_token': 'session_token'
        }

        event = {
            'headers': {
                'Cookie': 'sp_session_cookie="whatever"'
            }
        }

        session_mock.return_value = expected_session
        cookie_mock.return_value = 'session_token'

        @lambda_entry_point
        @auth_lambda
        def some_lambda(event, context, session):
            return session

        response = some_lambda(event, {})

        self.assertDictEqual(response, expected_session)
        cookie_mock.assert_called_with({'Cookie': 'sp_session_cookie="whatever"'})
        session_mock.assert_called_with('session_token')
        self.update_logger_metadata_fields_patcher.assert_called_with({'session_id': 'session_id123456'})

    @patch('common.utils.lambda_wrappers.get_session')
    @patch('common.utils.lambda_wrappers.get_session_from_cookie')
    def test_auth_lambda_passes_empty_session_to_lambda_if_no_session_found_for_given_session_token_in_cookie_header(
            self, cookie_mock, session_mock):

        event = {
            'headers': {
                'Cookie': 'sp_session_cookie="whatever"'
            }
        }

        cookie_mock.return_value = 'session_token'
        session_mock.return_value = None

        @lambda_entry_point
        @auth_lambda
        def some_lambda(event, context, session):
            return session

        response = some_lambda(event, {})

        self.assertDictEqual({}, response)
        cookie_mock.assert_called_with({'Cookie': 'sp_session_cookie="whatever"'})
        session_mock.assert_called_with('session_token')
        self.update_logger_metadata_fields_patcher.assert_called_with({'session_id': 'None'})

    @patch('common.utils.lambda_wrappers.get_session')
    @patch('common.utils.lambda_wrappers.get_session_from_cookie')
    def test_auth_lambda_passes_empty_session_to_lambda_given_no_session_cookie_header(
            self, cookie_mock, session_mock):

        event = {
            'headers': {
                'Accept': 'text/html'
            }
        }

        cookie_mock.return_value = None

        @lambda_entry_point
        @auth_lambda
        def some_lambda(event, context, session):
            return session

        response = some_lambda(event, {})

        self.assertDictEqual({}, response)
        cookie_mock.assert_called_with({'Accept': 'text/html'})
        session_mock.assert_not_called()
        self.update_logger_metadata_fields_patcher.assert_called_with({'session_id': 'None'})

    def test_api_lambda_sets_logger_metadata(self):
        expected_session_id = 'session_id123456'
        test_api_event = {
            'requestContext': {
                'authorizer': {
                    'principalId': expected_session_id
                }
            },
            'headers': {
                'request_id': 'requestId'
            }
        }

        @lambda_entry_point
        @api_lambda
        def some_lambda(event, context):
            pass

        some_lambda(test_api_event, {})

        self.update_logger_metadata_fields_patcher.assert_called_with({
            'session_id': expected_session_id,
            'request_id': 'requestId'}
        )
