from unittest.case import TestCase
from mock import patch, call, Mock
from datetime import datetime, timezone
from common.services.reinstate.log_references import LogReference


MOCK_ENV_VARS = {
    "POSITIVE_FAILSAFE_REPEAT_MONTHS": '11',
    "NEGATIVE_FAILSAFE_REPEAT_MONTHS": '10',
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE',
}
PARTICIPANT_ID = 'reinstate_participant'
USER_ID = 'user_id'


@patch('common.services.reinstate.reinstate_service.log')
@patch('common.services.reinstate.reinstate_service.datetime')
@patch('os.environ', MOCK_ENV_VARS)
class TestReinstateService(TestCase):

    FIXED_DATE_TIME = datetime(2021, 9, 28, 1, 1, 1, tzinfo=timezone.utc)

    @patch('boto3.client')
    @patch('boto3.resource')
    def setUp(self, boto3_resource, boto3_client):
        self.context = Mock()
        self.context.function_name = ''

        # Setup database mock
        table_mock = Mock()
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = table_mock
        boto3_resource.return_value = boto3_table_mock
        self.table_mock = boto3_resource

        import common.services.reinstate.reinstate_service as _reinstate_service
        self.reinstate_service_module = _reinstate_service

    @patch('common.services.reinstate.reinstate_service.transaction_write_records')
    def test_can_participant_be_reinstated_for_ceased_participant(
            self, transaction_writer_mock, datetime_mock, log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        self.reinstate_service_module.reinstate_participant(
            PARTICIPANT_ID, 'nhs_number', 'status', datetime(2021, 12, 1).date().isoformat(), USER_ID
        )

        expected_put_record = {
            'participant_id': PARTICIPANT_ID,
            'sort_key': 'REINSTATE#2021-09-28T01:01:01+00:00',
            'date_from': '2021-09-28T01:01:01+00:00',
            'user_id': USER_ID
        }
        expected_update_record = {
            'record_keys': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'PARTICIPANT'
            },
            'delete_values': [],
            'update_values': {
                'is_ceased': False,
                'next_test_due_date': '2021-12-01',
                'status': 'status'
            }
        }
        transaction_writer_mock.assert_has_calls([
            call(put_records=[expected_put_record], update_records=[expected_update_record])
        ])
        log_patcher.assert_has_calls([
            call({'log_reference': LogReference.REINSTATESER0009}),
            call({'log_reference': LogReference.REINSTATESER0010}),
            call({'log_reference': LogReference.REINSTATESER0011}),
        ])
