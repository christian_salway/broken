from unittest import TestCase
from mock import patch
from ddt import ddt, data, unpack
from common.models.participant import ParticipantStatus
import common.services.reinstate.reinstate_status as reinstate_status


@ddt
class TestNextTestReinstateStatus(TestCase):

    module = 'common.services.reinstate.reinstate_status'

    # Helper functions tests

    @unpack
    #       test_results                result_codes    expected_index    expected_status
    @data(([{'id': '1'}, {'id': '2'}],  ['XUH', 'X0A'], 1,                ParticipantStatus.ROUTINE),
          ([{'id': '1'}],               ['X0R'],        0,                ParticipantStatus.REPEAT_ADVISED),
          ([{'id': '1'}, {'id': '2'}],  ['19R', 'X0A'], 0,                ParticipantStatus.INADEQUATE),
          ([{'id': '1'}],               ['39S'],        0,                ParticipantStatus.SUSPENDED),
          ([], [], None, None),
          (None, [], None, None))
    @patch(f'{module}.get_concatenated_result_code_combination')
    def test_calculate_reinstate_status_from_latest_test_result_returns_status(
            self, test_results, result_code_combos, expected_index, expected_status, get_result_combo_mock):
        get_result_combo_mock.side_effect = result_code_combos
        actual_status, actual_index = reinstate_status.calculate_status_from_latest_test_result_for_manual_reinstate(test_results)  
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_index, actual_index)

    @unpack
    @data(([{'id': '1'}], ['XUH']),
          ([{'id': '1'}, {'id': '2'}], ['XUH', 'XUH']),
          ([{'id': '1'}], ['LOL']))
    @patch(f'{module}.get_concatenated_result_code_combination')
    def test_calculate_reinstate_status_from_latest_test_result_raises_exception(
            self, test_results, result_code_combos, get_result_combo_mock):
        get_result_combo_mock.side_effect = result_code_combos
        expected_exception_message = 'Participant status cannot be calculated from previous test results.'

        with self.assertRaises(Exception) as context:
            reinstate_status.calculate_status_from_latest_test_result_for_manual_reinstate(test_results)

        self.assertEqual(expected_exception_message, context.exception.args[0])

    @unpack
    #       test_results                result_codes     expected_status
    @data(([{'id': '1'}, {'id': '2'}],  ['XUH', 'X0A'],  ParticipantStatus.ROUTINE),
          ([{'id': '1'}, {'id': '2'}],  ['XOA', 'X0R'],  ParticipantStatus.REPEAT_ADVISED),
          ([{'id': '1'}, {'id': '2'}],  ['19R', 'X0A'],  ParticipantStatus.INADEQUATE),
          ([{'id': '1'}],               ['39S'],         ParticipantStatus.SUSPENDED),
          ([{'id': '1'}, {'id': '2'}],  ['X0A', '7US'],  ParticipantStatus.REPEAT_ADVISED),
          ([], [], None),
          (None, [], None))
    @patch(f'{module}.get_concatenated_result_code_combination')
    def test_calculate_reinstate_status_from_last_3_test_results_returns_status(
            self, test_results, result_code_combos, expected_status, get_result_combo_mock):
        get_result_combo_mock.side_effect = result_code_combos
        actual_status = reinstate_status.calculate_status_from_last_3_test_results_for_manual_reinstate(test_results)
        self.assertEqual(expected_status, actual_status)

    @unpack
    @data(([{'id': '1'}], ['XUH']),
          ([{'id': '1'}, {'id': '2'}], ['XUH', 'XUH']),
          ([{'id': '1'}], ['LOL']))
    @patch(f'{module}.get_concatenated_result_code_combination')
    def test_calculate_reinstate_status_from_last_3_test_results_raises_exception(
            self, test_results, result_code_combos, get_result_combo_mock):
        get_result_combo_mock.side_effect = result_code_combos
        expected_exception_message = 'Participant status cannot be calculated from previous test results.'

        with self.assertRaises(Exception) as context:
            reinstate_status.calculate_status_from_last_3_test_results_for_manual_reinstate(test_results)

        self.assertEqual(expected_exception_message, context.exception.args[0])
