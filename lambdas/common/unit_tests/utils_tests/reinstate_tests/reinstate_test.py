from unittest.case import TestCase
from mock import patch, call, Mock
from ddt import ddt, data, unpack
from datetime import datetime, timezone
from common.models.participant import CeaseReason, ParticipantStatus
from common.services.reinstate import reinstate_eligibility
from common.services.reinstate.reinstate import (
    reinstate_participant_by_participant_id,
    remove_no_recall_change_result_combinations
)
from common.services.reinstate.log_references import LogReference
from common.services.reinstate.reinstate_reasons import Explanation
from common.services.reinstate.reinstate_result import ReinstateResult
from common.services.reinstate.reinstate_response import ReinstateResponse


MOCK_ENV_VARS = {
    "POSITIVE_FAILSAFE_REPEAT_MONTHS": '11',
    "NEGATIVE_FAILSAFE_REPEAT_MONTHS": '10',
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE',
}
PARTICIPANT_ID = 'reinstate_participant'
USER_ID = 'user_id'


def participant_schema(override={}):
    return {
        'participant_id': PARTICIPANT_ID,
        'sort_key': 'PARTICIPANT',
        'nhs_number': '111111111',
        'date_of_birth': '1991-09-24',
        'active': True,
        'is_ceased': True,
        **override
    }


def cease_schema(overrride={}):
    return {
        'participant_id': 'reinstate_participant',
        'nhs_number': '1',
        'sort_key': 'CEASE#2020-02-01',
        'reason': CeaseReason.DUE_TO_AGE,
        'date_from': '2020-01-01T12:15:08.488670+00:00',
        **overrride
    }


def result_schema(overrride={}):
    return {
        'participant_id': 'reinstate_participant',
        'sort_key': 'RESULT#1',
        'result_code': 'X',
        'infection_code': '0',
        'action_code': 'R',
        'test_date': '2020-01-01',
        **overrride
    }


@ddt
@patch('common.services.reinstate.reinstate_eligibility.log')
class TestReinstateEligibility(TestCase):

    def test_can_participant_be_reinstated_for_ceased_participant(self, log_patcher):
        test_participant = participant_schema()

        response = reinstate_eligibility.can_participant_be_reinstated(test_participant)
        self.assertEqual(response, True)
        log_patcher.assert_has_calls([
            call({'log_reference': LogReference.REINSTATESER0003}),
            call({'log_reference': LogReference.REINSTATESER0006}),
        ])

    def test_can_participant_be_reinstated_for_out_of_programme_participant(self, log_patcher):
        test_participant = participant_schema(override={
            'active': False
        })

        response = reinstate_eligibility.can_participant_be_reinstated(test_participant)
        self.assertEqual(response, False)
        log_patcher.assert_has_calls([
            call({'log_reference': LogReference.REINSTATESER0003}),
            call({'log_reference': LogReference.REINSTATESER0004}),
        ])

    def test_can_participant_be_reinstated_for_routine_participant(self, log_patcher):
        test_participant = participant_schema(override={
            'is_ceased': False
        })

        response = reinstate_eligibility.can_participant_be_reinstated(test_participant)
        self.assertEqual(response, False)
        log_patcher.assert_has_calls([
            call({'log_reference': LogReference.REINSTATESER0003}),
            call({'log_reference': LogReference.REINSTATESER0005}),
        ])

    @data(
        'NO_CERVIX',
        'DUE_TO_AGE',
        'RADIOTHERAPY',
        'PATIENT_INFORMED_CHOICE',
        'MENTAL_CAPACITY_ACT',
        'AUTOCEASE_DUE_TO_AGE',
    )
    def test_returns_true_for_valid_cease_reasons(self, cease_reason, log_patcher):
        response = reinstate_eligibility.is_valid_cease_reason(cease_reason)
        self.assertEqual(response, True)

    def test_returns_false_for_invalid_cease_reasons(self, log_patcher):
        response = reinstate_eligibility.is_valid_cease_reason('OTHER')
        self.assertEqual(response, False)

    def test_throws_exception_for_invalid_cease_reason_enum(self, log_patcher):
        response = reinstate_eligibility.is_valid_cease_reason('EX')
        self.assertEqual(response, False)
        log_patcher.assert_has_calls([
            call({'log_reference': LogReference.REINSTATESER0013, 'cease_reason': 'EX'})
        ])


@ddt
@patch('common.log.logger')
@patch('common.utils.next_test_due_date_calculators.next_test_due_date_calculator.datetime')
@patch('common.services.reinstate.reinstate.reinstate_participant')
@patch('os.environ', MOCK_ENV_VARS)
class TestCeaseDueToAgeReinstateCalculator(TestCase):

    FIXED_DATE_TIME = datetime(2021, 9, 24, 1, 1, 1, tzinfo=timezone.utc)

    def test_unchanged_recall_types_removed_from_list(self,
                                                      reinstate_service_mock,
                                                      datetime_mock,
                                                      log_patcher):
        test_results = [
            {
                **result_schema(overrride={
                    'result_code': 'X',
                    'infection_code': 'U',
                    'action_code': 'H',
                })
            },
            {
                **result_schema()
            },
            {
                **result_schema(overrride={
                    'result_code': 'X',
                    'infection_code': 'U',
                    'action_code': 'R',
                })
            }
        ]
        filtered_test_results = remove_no_recall_change_result_combinations(test_results)
        expected_test_results = [
            {
                **result_schema()
            },
            {
                **result_schema(overrride={
                    'result_code': 'X',
                    'infection_code': 'U',
                    'action_code': 'R',
                })
            }
        ]
        self.assertEqual(filtered_test_results, expected_test_results)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    def test_reinstate_rejected_when_participant_not_found(self,
                                                           participant_results_mock,
                                                           reinstate_service_mock,
                                                           datetime_mock,
                                                           log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = []
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.NO_PARTICIPANT.value,
            status=None,
            next_test_due_date=None,
            explanation=[],
            error=False
        )
        self.assertEqual(response, expected_response)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    @data(
        CeaseReason.AUTOCEASE_DUE_TO_AGE,
        CeaseReason.DUE_TO_AGE,
    )
    def test_reinstate_rejected_when_participant_out_of_programme(self,
                                                                  cease_reason,
                                                                  participant_results_mock,
                                                                  reinstate_service_mock,
                                                                  datetime_mock, log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = [
            {
                **participant_schema(override={'active': False})
            },
            {
                **cease_schema(overrride={'reason': cease_reason})
            },
            {
                **result_schema()
            }
        ]
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.CANNOT_BE_REINSTATED.value,
            status=None,
            next_test_due_date=None,
            explanation=[],
            error=False
        )
        self.assertEqual(response, expected_response)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    def test_reinstate_rejected_when_participant_has_no_cease_record(self,
                                                                     participant_results_mock,
                                                                     reinstate_service_mock,
                                                                     datetime_mock, log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = [
            {
                **participant_schema()
            },
            {
                **result_schema()
            }
        ]
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.NOT_CEASED.value,
            status=None,
            next_test_due_date=None,
            explanation=[],
            error=False
        )
        self.assertEqual(response, expected_response)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    @data(
        CeaseReason.MERGED,
        CeaseReason.OTHER,
    )
    def test_reinstate_rejected_when_participant_invalid_cease_reason(self,
                                                                      cease_reason,
                                                                      participant_results_mock,
                                                                      reinstate_service_mock,
                                                                      datetime_mock, log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = [
            {
                **participant_schema()
            },
            {
                **cease_schema(overrride={'reason': cease_reason})
            },
            {
                **result_schema()
            }
        ]
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.INVALID_CEASE_REASON.value,
            status=None,
            next_test_due_date=None,
            explanation=[],
            error=False
        )
        self.assertEqual(response, expected_response)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    @unpack
    @data(
        (CeaseReason.AUTOCEASE_DUE_TO_AGE, datetime(2021, 12, 4)),
        (CeaseReason.DUE_TO_AGE, datetime(2021, 12, 4)),
    )
    def test_reinstate_returns_called_when_for_ceased_participant(self,
                                                                  cease_reason,
                                                                  expected_ntdd,
                                                                  participant_results_mock,
                                                                  reinstate_service_mock,
                                                                  datetime_mock,
                                                                  log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = [
            {
                **participant_schema()
            },
            {
                **cease_schema(overrride={'reason': cease_reason})
            },
        ]
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED.value,
            status=ParticipantStatus.CALLED.value,
            next_test_due_date=expected_ntdd.date(),
            explanation=[Explanation.CEASED_DUE_TO_AGE.value, Explanation.NO_PREVIOUS_TEST.value],
            error=False
        )
        self.assertEqual(response, expected_response)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    @unpack
    @data(
        (CeaseReason.AUTOCEASE_DUE_TO_AGE, datetime(2021, 12, 4)),
        (CeaseReason.DUE_TO_AGE, datetime(2021, 12, 4)),
    )
    def test_reinstate_returns_routine_when_for_ceased_participant(self,
                                                                   cease_reason,
                                                                   expected_ntdd,
                                                                   participant_results_mock,
                                                                   reinstate_service_mock,
                                                                   datetime_mock,
                                                                   log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = [
            {
                **participant_schema()
            },
            {
                **cease_schema(overrride={'reason': cease_reason})
            },
            {
                **result_schema()
            }
        ]
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED.value,
            status=ParticipantStatus.ROUTINE.value,
            next_test_due_date=expected_ntdd.date(),
            explanation=[Explanation.CEASED_DUE_TO_AGE.value, Explanation.STATUS_NOT_ROUTINE.value],
            error=False
        )
        self.assertEqual(response, expected_response)


@ddt
@patch('common.log.logger')
@patch('common.utils.next_test_due_date_calculators.next_test_due_date_calculator.datetime')
@patch('common.services.reinstate.reinstate.reinstate_participant')
@patch('os.environ', MOCK_ENV_VARS)
class TestReinstateCalculatorForOtherValidCeaseReasons(TestCase):

    FIXED_DATE_TIME = datetime(2021, 9, 24, 1, 1, 1, tzinfo=timezone.utc)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    @unpack
    @data(
        ('1997-09-24', CeaseReason.NO_CERVIX, datetime(2022, 5, 7)),
        ('1990-01-24', CeaseReason.NO_CERVIX, datetime(2021, 12, 4)),
        ('1997-09-24', CeaseReason.RADIOTHERAPY, datetime(2022, 5, 7)),
        ('1990-01-24', CeaseReason.RADIOTHERAPY, datetime(2021, 12, 4)),
        ('1997-09-24', CeaseReason.PATIENT_INFORMED_CHOICE, datetime(2022, 5, 7)),
        ('1990-01-24', CeaseReason.PATIENT_INFORMED_CHOICE, datetime(2021, 12, 4)),
        ('1997-09-24', CeaseReason.MENTAL_CAPACITY_ACT, datetime(2022, 5, 7)),
        ('1990-01-24', CeaseReason.MENTAL_CAPACITY_ACT, datetime(2021, 12, 4)),
    )
    def test_participant_reinstated_for_no_previous_tests(self,
                                                          age,
                                                          cease_reason,
                                                          expected_ntdd,
                                                          participant_results_mock,
                                                          reinstate_service_mock,
                                                          datetime_mock,
                                                          log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = [
            {
                **participant_schema(override={'date_of_birth': age})
            },
            {
                **cease_schema(overrride={'reason': cease_reason})
            },
        ]
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED.value,
            status=ParticipantStatus.CALLED,
            next_test_due_date=expected_ntdd.date(),
            explanation=[Explanation.NO_PREVIOUS_TEST.value],
            error=False
        )
        self.assertEqual(response, expected_response)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    @unpack
    @data(
        ('1975-09-24', datetime(2024, 1, 1), Explanation.UNDER_50.value),
        ('1965-01-24', datetime(2026, 1, 1), Explanation.BETWEEN_50_AND_59.value),
        # ('1955-09-24', datetime(2022, 5, 7), Explanation.OVER_59.value),
    )
    def test_reinstate_participant_returns_routine_recall(self,
                                                          age,
                                                          expected_ntdd,
                                                          step_explanation,
                                                          participant_results_mock,
                                                          reinstate_service_mock,
                                                          datetime_mock,
                                                          log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = [
            {
                **participant_schema(override={
                    'date_of_birth': age
                })
            },
            {
                **cease_schema(overrride={'reason': CeaseReason.NO_CERVIX})
            },
            {
                **result_schema(overrride={
                    'result_code': 'X',
                    'infection_code': '0',
                    'action_code': 'A',
                    'test_date': '2021-01-01'
                })
            }
        ]
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED.value,
            status=ParticipantStatus.ROUTINE.value,
            next_test_due_date=expected_ntdd.date(),
            explanation=[Explanation.ROUTINE_STATUS.value, step_explanation],
            error=False
        )
        self.assertEqual(response, expected_response)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    def test_reinstate_participant_returns_repeat_advised_recall(self,
                                                                 participant_results_mock,
                                                                 reinstate_service_mock,
                                                                 datetime_mock,
                                                                 log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = [
            {
                **participant_schema()
            },
            {
                **cease_schema(overrride={'reason': CeaseReason.NO_CERVIX})
            },
            {
                **result_schema(overrride={
                    'result_code': 'X',
                    'infection_code': '0',
                    'action_code': 'R',
                    'test_date': '2021-01-01'
                })
            }
        ]
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED.value,
            status=ParticipantStatus.REPEAT_ADVISED.value,
            next_test_due_date=datetime(2024, 1, 1).date(),
            explanation=[Explanation.REPEAT_ADVISED_STATUS.value],
            error=False
        )
        self.assertEqual(response, expected_response)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    def test_reinstate_participant_returns_inadequate(self,
                                                      participant_results_mock,
                                                      reinstate_service_mock,
                                                      datetime_mock,
                                                      log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = [
            {
                **participant_schema()
            },
            {
                **cease_schema(overrride={'reason': CeaseReason.NO_CERVIX})
            },
            {
                **result_schema(overrride={
                    'result_code': 'X',
                    'infection_code': 'U',
                    'action_code': 'R',
                    'test_date': '2021-01-01'
                })
            }
        ]
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED.value,
            status=ParticipantStatus.INADEQUATE.value,
            next_test_due_date=datetime(2021, 12, 4).date(),
            explanation=[Explanation.INADEQUATE_STATUS.value],
            error=False
        )
        self.assertEqual(response, expected_response)

    @patch('common.services.reinstate.reinstate.get_participant_and_test_results')
    def test_reinstate_participant_returns_suspended_recall(self,
                                                            participant_results_mock,
                                                            reinstate_service_mock,
                                                            datetime_mock,
                                                            log_patcher):
        datetime_mock.now = Mock(return_value=self.FIXED_DATE_TIME)
        participant_results_mock.return_value = [
            {
                **participant_schema()
            },
            {
                **cease_schema(overrride={'reason': CeaseReason.NO_CERVIX})
            },
            {
                **result_schema(overrride={
                    'result_code': 'X',
                    'infection_code': '0',
                    'action_code': 'S',
                    'test_date': '2021-01-01'
                })
            }
        ]
        response = reinstate_participant_by_participant_id(PARTICIPANT_ID, USER_ID)
        expected_response = ReinstateResponse(
            result=ReinstateResult.REINSTATED.value,
            status=ParticipantStatus.SUSPENDED.value,
            next_test_due_date=datetime(2021, 12, 4).date(),
            explanation=[Explanation.SUSPENDED_STATUS.value],
            error=False
        )
        self.assertEqual(response, expected_response)
