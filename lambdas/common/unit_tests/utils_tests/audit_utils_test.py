from datetime import datetime, timezone
from unittest import TestCase
from mock import patch, Mock


@patch('common.utils.audit_utils.get_internal_id', Mock(return_value='1'))
@patch('os.environ', {'AUDIT_QUEUE_URL': 'audit_queue_url'})
class TestAudit(TestCase):

    @classmethod
    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    @patch('common.log.log', Mock())
    def setUpClass(cls):
        global audit_utils
        import common.utils.audit_utils as audit_utils
        cls.date_time_patcher = patch.object(audit_utils, 'datetime', Mock(wraps=datetime))
        cls.date_time_patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.date_time_patcher.stop()

    FIXED_NOW_TIME = datetime(2020, 5, 29, 12, 14, 27, 34, tzinfo=timezone.utc)

    def setUp(self):
        audit_utils._SQS_CLIENT = Mock()
        self.maxDiff = None

    def test_validator_returns_false_given_no_user_and_no_session(self):
        is_valid = audit_utils.has_valid_session_user_data(None, None)
        self.assertFalse(is_valid)

    def test_validator_returns_false_given_no_user_and_an_empty_session_dictionary(self):
        is_valid = audit_utils.has_valid_session_user_data(session={}, user=None)
        self.assertFalse(is_valid)

    def test_validator_returns_false_given_no_user_and_session_has_no_user_data(self):
        is_valid = audit_utils.has_valid_session_user_data(session={'a_key': 'a_value'}, user=None)
        self.assertFalse(is_valid)

    def test_validator_returns_false_given_no_user_and_session_has_no_nhsid_useruid(self):
        is_valid = audit_utils.has_valid_session_user_data(session={'user_data': {'a_key': 'a_value'}}, user=None)
        self.assertFalse(is_valid)

    def test_validator_returns_false_given_no_user_and_session_has_user_data_but_no_nhsid_useruid(self):
        is_valid = audit_utils.has_valid_session_user_data(session={'user_data': {'a_key': 'a_value'}}, user=None)
        self.assertFalse(is_valid)

    def test_validator_returns_false_given_user_and_session_both_supplied(self):
        is_valid = audit_utils.has_valid_session_user_data(session='a_session', user='a_user')
        self.assertFalse(is_valid)

    def test_validator_returns_false_given_user_is_not_a_valid_user_type(self):
        is_valid = audit_utils.has_valid_session_user_data(session=None, user='invalid')
        self.assertFalse(is_valid)

    def test_audit_raises_exception_given_invalid_action(self):
        expected_exception_message = 'Requested action "invalid_action" is not a valid audit action.'

        with self.assertRaises(Exception) as context:
            audit_utils.audit('invalid_action')

        self.assertEqual(expected_exception_message, context.exception.args[0])

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_raises_exception_given_session_and_user_not_valid(self, validator_mock):
        validator_mock.return_value = False
        expected_exception_message = 'Session user data is not valid. Cannot audit the action.'

        with self.assertRaises(Exception) as context:
            audit_utils.audit(action=audit_utils.AuditActions.LOGOUT, user='invalid_user')

        self.assertEqual(expected_exception_message, context.exception.args[0])
        validator_mock.assert_called_with(None, 'invalid_user')

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_an_automated_process_audit_event_without_participants(
            self, validator_mock):
        audit_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        validator_mock.return_value = True

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "STORE_CSO1C", "timestamp": "2020-05-29T12:14:27.000034+00:00", "internal_id": "1", "nhsid_useruid": "MIGRATION", "session_id": "NONE"}'  
        }

        audit_utils.audit(action=audit_utils.AuditActions.STORE_CSO1C, user=audit_utils.AuditUsers.MIGRATION)

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_an_automated_process_audit_event_with_timestamp(
            self, validator_mock):
        validator_mock.return_value = True

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "STORE_CSO1C", "timestamp": "2019-04-16T17:21:03.000034+00:00", "internal_id": "1", "nhsid_useruid": "MIGRATION", "session_id": "NONE"}'  
        }

        audit_utils.audit(action=audit_utils.AuditActions.STORE_CSO1C,
                          user=audit_utils.AuditUsers.MIGRATION, timestamp='2019-04-16T17:21:03.000034+00:00')

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_an_automated_process_audit_event_with_participants(
            self, validator_mock):
        audit_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        validator_mock.return_value = True

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "STORE_CSO1C", "timestamp": "2020-05-29T12:14:27.000034+00:00", "internal_id": "1", "nhsid_useruid": "MIGRATION_123", "session_id": "NONE", "participant_ids": ["123"], "nhs_numbers": ["456"]}'  
        }

        audit_utils.audit(action=audit_utils.AuditActions.STORE_CSO1C,
                          user=audit_utils.AuditUsers.MIGRATION, participant_ids=['123'], nhs_numbers=['456'])

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_an_automated_process_audit_event_with_participants_no_nhs_numbers(
            self, validator_mock):
        audit_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        validator_mock.return_value = True

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "STORE_CSO1C", "timestamp": "2020-05-29T12:14:27.000034+00:00", "internal_id": "1", "nhsid_useruid": "MIGRATION_123", "session_id": "NONE", "participant_ids": ["123"]}'  
        }

        audit_utils.audit(action=audit_utils.AuditActions.STORE_CSO1C,
                          user=audit_utils.AuditUsers.MIGRATION, participant_ids=['123'])

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_a_user_audit_event_given_session_has_no_selected_role(
            self, validator_mock):
        audit_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        validator_mock.return_value = True
        session = {'session_id': 'the_session',
                   'user_data': {'nhsid_useruid': 'the_user', 'first_name': 'Tom', 'last_name': 'Smith'}}

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "LOGIN", "timestamp": "2020-05-29T12:14:27.000034+00:00", "internal_id": "1", "nhsid_useruid": "the_user", "session_id": "the_session", "first_name": "Tom", "last_name": "Smith"}'  
        }

        audit_utils.audit(action=audit_utils.AuditActions.LOGIN, session=session)

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_a_user_audit_event_given_session_selected_role(
            self, validator_mock):
        audit_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        validator_mock.return_value = True
        session = {
            'session_id': 'the_session',
            'user_data': {
                'nhsid_useruid': 'the_user',
                'first_name': 'Tom',
                'last_name': 'Smith'
            },
            'selected_role': {
                'role_id': 'the_role',
                'organisation_code': 'the_user_org_code'
            }
        }

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "LOGIN", "timestamp": "2020-05-29T12:14:27.000034+00:00", "internal_id": "1", "nhsid_useruid": "the_user", "session_id": "the_session", "first_name": "Tom", "last_name": "Smith", "role_id": "the_role", "user_organisation_code": "the_user_org_code"}'  
        }

        audit_utils.audit(action=audit_utils.AuditActions.LOGIN, session=session)

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_a_user_audit_event_given_a_timestamp(
            self, validator_mock):
        validator_mock.return_value = True
        session = {'session_id': 'the_session',
                   'user_data': {'nhsid_useruid': 'the_user', 'first_name': 'Tom', 'last_name': 'Smith'}}

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "LOGIN", "timestamp": "2019-04-16T17:21:03.000034+00:00", "internal_id": "1", "nhsid_useruid": "the_user", "session_id": "the_session", "first_name": "Tom", "last_name": "Smith"}'  
        }

        audit_utils.audit(action=audit_utils.AuditActions.LOGIN, session=session,
                          timestamp='2019-04-16T17:21:03.000034+00:00')

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_a_user_audit_event_given_participant_ids(
            self, validator_mock):
        audit_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        validator_mock.return_value = True
        session = {'session_id': 'the_session',
                   'user_data': {'nhsid_useruid': 'the_user', 'first_name': 'Tom', 'last_name': 'Smith'}}

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "LOGIN", "timestamp": "2020-05-29T12:14:27.000034+00:00", "internal_id": "1", "nhsid_useruid": "the_user", "session_id": "the_session", "first_name": "Tom", "last_name": "Smith", "participant_ids": ["123"]}'  
        }

        audit_utils.audit(action=audit_utils.AuditActions.LOGIN, session=session, participant_ids=['123'])

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_a_user_audit_event_given_participant_ids_and_nhs_numbers(
            self, validator_mock):
        audit_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        validator_mock.return_value = True
        session = {'session_id': 'the_session',
                   'user_data': {'nhsid_useruid': 'the_user', 'first_name': 'Tom', 'last_name': 'Smith'}}

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "LOGIN", "timestamp": "2020-05-29T12:14:27.000034+00:00", "internal_id": "1", "nhsid_useruid": "the_user", "session_id": "the_session", "first_name": "Tom", "last_name": "Smith", "participant_ids": ["123"], "nhs_numbers": ["456"]}'  
        }

        audit_utils.audit(action=audit_utils.AuditActions.LOGIN, session=session, participant_ids=['123'],
                          nhs_numbers=['456'])

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_a_user_audit_event_given_additional_information(
            self, validator_mock):
        audit_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        validator_mock.return_value = True
        session = {
            'session_id': '12345678',
            'user_data': {
                'nhsid_useruid': 'NHS12345',
                'first_name': 'Tom',
                'last_name': 'Smith'
            }
        }

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "CEASE_EPISODE", "timestamp": "2020-05-29T12:14:27.000034+00:00", "internal_id": "1", "nhsid_useruid": "NHS12345", "session_id": "12345678", "first_name": "Tom", "last_name": "Smith", "participant_ids": ["123"], "nhs_numbers": ["456"], "additional_information": {"reason": "Cease reason from lambda", "crm_number": "123456789", "comments": "I was asked to cease this participant"}}'  
        }

        audit_utils.audit(
            action=audit_utils.AuditActions.CEASE_EPISODE,
            session=session, participant_ids=['123'], nhs_numbers=['456'],
            additional_information={
                "reason": "Cease reason from lambda",
                "crm_number": "123456789",
                "comments": "I was asked to cease this participant"
            }
        )

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)

    @patch('common.utils.audit_utils.has_valid_session_user_data')
    def test_audit_sends_correct_audit_message_for_a_user_audit_event_given_organisation_code_and_email_address(
            self, validator_mock):
        audit_utils.datetime.now.return_value = self.FIXED_NOW_TIME
        validator_mock.return_value = True
        session = {'session_id': 'the_session',
                   'user_data': {'nhsid_useruid': 'the_user', 'first_name': 'Tom', 'last_name': 'Smith'}}

        expected_sqs_call = {
            'QueueUrl': 'audit_queue_url',
            'MessageBody': '{"action": "LOGIN", "timestamp": "2020-05-29T12:14:27.000034+00:00", "internal_id": "1", "nhsid_useruid": "the_user", "session_id": "the_session", "first_name": "Tom", "last_name": "Smith", "organisation_code": "abcde", "email_address": "test1@nhs.net"}'  
        }

        audit_utils.audit(
            action=audit_utils.AuditActions.LOGIN,
            session=session,
            organisation_code="abcde",
            email_address="test1@nhs.net"
        )

        actual_sqs_call = audit_utils._SQS_CLIENT.send_message.call_args_list[0][1]
        self.assertDictEqual(expected_sqs_call, actual_sqs_call)
