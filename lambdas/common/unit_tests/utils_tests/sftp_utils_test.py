from unittest import TestCase
from mock import patch, Mock, call

from common.log_references import CommonLogReference


class TestSendPrintFile(TestCase):

    module = 'common.utils.sftp_utils'

    @patch('boto3.resource')
    def setUp(self, *args):
        import common.utils.sftp_utils as _module
        self.module = _module

    @patch(f'{module}.io')
    @patch(f'{module}.paramiko')
    @patch(f'{module}.log')
    def test_create_sftp_client(self, log_mock, paramiko_mock, io_mock):
        io_mock.StringIO.return_value = 'test_keyfile'
        paramiko_mock.RSAKey.from_private_key.return_value = 'RSA_key'
        transport_mock = Mock()
        transport_mock.connect.return_value = 'successful_connection'
        paramiko_mock.Transport.return_value = transport_mock
        paramiko_mock.SFTPClient.from_transport.return_value = 'client'

        client = self.module.create_sftp_client('private_key', 'host', 'user_id')

        io_mock.StringIO.assert_called_with('private_key')
        paramiko_mock.RSAKey.from_private_key.assert_called_with(io_mock.StringIO.return_value)
        paramiko_mock.Transport.assert_called_with(('host', 22))
        transport_mock.connect.assert_called_with(
            username='user_id',
            pkey=paramiko_mock.RSAKey.from_private_key.return_value
        )

        self.assertEqual(client, 'client')
        log_mock.assert_has_calls([
            call({'log_reference': CommonLogReference.SFTP0001}),
            call({'log_reference': CommonLogReference.SFTP0002, 'host': 'cic'}),
            call({'log_reference': CommonLogReference.SFTP0003}),
            call({'log_reference': CommonLogReference.SFTP0004}),
        ])

    @patch(f'{module}.get_secret')
    def test_get_secrets(self, secret_mock):
        secret_mock.return_value = '{"private-key": "test_private_key", "host": "test_host", "user_id": "test_user_id"}'
        actual_private_key, actual_host, actual_user_id = self.module.get_sftp_secrets('blah')
        self.assertEqual(
            ['test_private_key', 'test_host', 'test_user_id'],
            [actual_private_key, actual_host, actual_user_id]
        )

    @patch(f'{module}.log')
    def test_exception_handler(self, log_mock):

        @self.module.exception_handler('test_log')
        def mock_decorated_function():
            raise Exception('test_exception')

        with self.assertRaises(Exception):
            mock_decorated_function()

        log_mock.assert_called_with({'log_reference': 'test_log', 'add_exception_info': True})
