import os
from unittest import TestCase
from common.utils.postcode_utils import (
    is_completed_postcode, is_valid_postcode, sanitise_postcode)


class PostcodeUtilsTests(TestCase):

    def test_is_complete_postcode_with_sanitised_postcode_returns_ok(self):
        current_path = os.path.dirname(os.path.realpath(__file__))
        full_file_path = os.path.join(current_path, 'test_data', 'postcodes.txt')
        postcodes = (open(full_file_path).read()).split(',')

        for postcode in postcodes:
            sanitised_postcode = sanitise_postcode(postcode)
            is_postcode_complete = is_completed_postcode(sanitised_postcode)
            self.assertTrue(is_postcode_complete)

    def test_is_complete_postcode_with_non_sanitised_postcode_returns_not_ok(self):
        current_path = os.path.dirname(os.path.realpath(__file__))
        full_file_path = os.path.join(current_path, 'test_data', 'postcodes.txt')
        postcodes = (open(full_file_path).read()).split(',')

        for postcode in postcodes:
            is_postcode_complete = is_completed_postcode(postcode)
            self.assertFalse(is_postcode_complete)

    def test_is_complete_postcode_with_incorrect_postcode_returns_not_ok(self):
        for postcode in ['not-ok-postcode', 'AAA111X']:
            is_postcode_complete = is_completed_postcode(postcode)
            self.assertFalse(is_postcode_complete)

    def test_is_valid_postcode_with_sanitised_postcode_returns_ok(self):
        current_path = os.path.dirname(os.path.realpath(__file__))
        full_file_path = os.path.join(current_path, 'test_data', 'postcodes.txt')
        postcodes = (open(full_file_path).read()).split(',')

        for postcode in postcodes:
            sanitised_postcode = sanitise_postcode(postcode)
            is_postcode_valid = is_valid_postcode(sanitised_postcode)
            self.assertTrue(is_postcode_valid)

    def test_is_valid_postcode_with_non_sanitised_postcode_returns_not_ok(self):
        current_path = os.path.dirname(os.path.realpath(__file__))
        full_file_path = os.path.join(current_path, 'test_data', 'postcodes.txt')
        postcodes = (open(full_file_path).read()).split(',')

        for postcode in postcodes:
            is_postcode_valid = is_valid_postcode(postcode)
            self.assertFalse(is_postcode_valid)

    def test_is_valid_postcode_with_incorrect_postcode_but_used_in_participants_data_returns_ok(self):
        invalid_but_valid_postcode = '1D737AH'
        is_postcode_valid = is_valid_postcode(invalid_but_valid_postcode)
        self.assertTrue(is_postcode_valid)

    def test_is_valid_postcode_with_incorrect_postcode_returns_not_ok(self):
        for postcode in ['not-ok-postcode', 'AAA111X']:
            is_postcode_complete = is_completed_postcode(postcode)
            self.assertFalse(is_postcode_complete)

    def test_sanitise_postcode_returns_sanitised_postcode(self):
        postcodes = [
            {
                'postcode': 'aaa aaa',
                'sanitised_postcode': 'AAAAAA'
            },
            {
                'postcode': ' d123 33 3',
                'sanitised_postcode': 'D123333'
            }
        ]

        for postcode in postcodes:
            sanitised_postcode = sanitise_postcode(postcode['postcode'])
            self.assertEqual(sanitised_postcode, postcode['sanitised_postcode'])
