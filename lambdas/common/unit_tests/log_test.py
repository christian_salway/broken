import json
from unittest import TestCase
from mock import patch, Mock
from common import log
from common.log import initialize_logger
from common.log_references import CommonLogReference


class TestLogUpdateLoggerMetadataFields(TestCase):

    def test_update_logger_metadata_fields_adds_new_key_values_to_the_metadata_dict(self):
        log.LOG_RECORD_META_DATA = {}

        kv_pairs_to_add = {'key': 'value'}
        log.update_logger_metadata_fields(kv_pairs_to_add)

        self.assertLessEqual(kv_pairs_to_add.items(), log.LOG_RECORD_META_DATA.items())

    def test_update_logger_metadata_fields_updates_existing_key_in_the_metadata_dict(self):
        log.LOG_RECORD_META_DATA = {'key': 'something'}

        kv_pairs_to_add = {'key': 'other thing'}
        log.update_logger_metadata_fields(kv_pairs_to_add)

        self.assertEqual(log.LOG_RECORD_META_DATA.get('key'), 'other thing')


class TestLog(TestCase):
    @patch('common.log.logger')
    def test_log_raises_exception_when_called_with_unexpected_argument(self, _):

        with self.assertRaises(Exception):
            log.log(object)

    @patch('common.log.logger')
    def test_log_set_exception_info_to_true_when_passed_exception_log_reference_in_dictionary(self, logger_mock):
        log.log({'log_reference': CommonLogReference.EXCEPTION000})

        args, kwargs = logger_mock.log.call_args
        self.assertLessEqual({'exc_info': True}.items(), kwargs.items())

    @patch('common.log.logger')
    def test_log_set_exception_info_to_false_when_used_the_exception_log_reference_and_add_exception_info_is_false(
            self, logger_mock):
        log.log({'log_reference': CommonLogReference.EXCEPTION000, 'add_exception_info': False})

        args, kwargs = logger_mock.log.call_args
        self.assertLessEqual({'exc_info': False}.items(), kwargs.items())

    @patch('common.log.logger')
    def test_log_set_exception_info_to_true_when_add_exception_info_flag_is_true(self, logger_mock):
        log.log({'log_reference': CommonLogReference.LAMBDA0000, 'add_exception_info': True})

        args, kwargs = logger_mock.log.call_args
        self.assertLessEqual({'exc_info': True}.items(), kwargs.items())

    @patch('common.log.logger')
    def test_log_set_exception_info_to_false_when_add_exception_info_flag_is_false(self, logger_mock):
        log.log({'log_reference': CommonLogReference.LAMBDA0000, 'add_exception_info': False})

        args, kwargs = logger_mock.log.call_args
        self.assertLessEqual({'exc_info': False}.items(), kwargs.items())

    @patch('common.log.logger')
    def test_log_set_exception_info_to_false_when_not_passed_add_exception_info_flag_and_no_exception_log_reference(
            self, logger_mock):
        log.log({'log_reference': CommonLogReference.LAMBDA0000, 'extra_field': 'something'})

        args, kwargs = logger_mock.log.call_args
        self.assertLessEqual({'exc_info': False}.items(), kwargs.items())

    @patch('common.log.logger')
    def test_log_set_exception_info_to_true_when_passed_exception_log_reference(self, logger_mock):
        log.log(CommonLogReference.EXCEPTION000)

        args, kwargs = logger_mock.log.call_args
        self.assertLessEqual({'exc_info': True}.items(), kwargs.items())

    @patch('common.log.logger')
    def test_log_retrieve_the_log_level_and_log_message_from_the_logs_map_when_receives_a_string(self, logger_mock):
        log_reference = list(CommonLogReference)[0]

        log.log(log_reference)

        expected_message = {
            'log_reference': log_reference.name,
            'message': log_reference.message
        }
        args, kwargs = logger_mock.log.call_args
        self.assertTupleEqual((log_reference.level, json.dumps(expected_message)), args)

    @patch('common.log.logger')
    def test_log_append_all_key_value_pairs_to_the_log_message_correctly_when_passing_a_dictionary(self, logger_mock):
        log_reference = list(CommonLogReference)[0]

        log_record = {
            'log_reference': log_reference,
            'key1': 'value1',
            'key2': 'value2',
        }

        log.log(log_record)

        expected_message = {
            'log_reference': log_reference.name,
            'message': log_reference.message,
            'key1': 'value1',
            'key2': 'value2',
        }
        actual_call_args, _ = logger_mock.log.call_args
        actual_call_args_log_level = actual_call_args[0]
        actual_call_args_log_message = actual_call_args[1]
        self.assertEqual(log_reference.level, actual_call_args_log_level)
        self.assertDictEqual(expected_message, json.loads(actual_call_args_log_message))

    @patch('common.log.logger')
    def test_log_with_sensitive_data(self, logger_mock):
        log_reference = CommonLogReference.PARTICIPANT0009
        log_record = {
            'log_reference': log_reference,
            'sensitive_data': True,
        }

        log.log(log_record)

        expected_message = {
            'log_reference': log_reference.name,
            'message': log_reference.message,
            'sensitive_data': True
        }

        actual_call_args, _ = logger_mock.log.call_args
        actual_call_args_log_level = actual_call_args[0]
        actual_call_args_log_message = actual_call_args[1]

        self.assertEqual(log_reference.level, actual_call_args_log_level)
        self.assertDictEqual(expected_message, json.loads(actual_call_args_log_message))


class TestSerializeMessage(TestCase):
    def test_serialize_message_when_passed_a_string(self):
        formatter = log.KeyValueFormatter()

        actual = formatter._serialize_message('some string')

        self.assertEqual(actual, 'message="some string"')

    def test_serialize_message_WhenPassedAJson(self):
        formatter = log.KeyValueFormatter()
        message = {'key1': 'value1', 'key2': 'value2'}

        log.DELIMITER = ' | '
        actual = formatter._serialize_message(json.dumps(message))

        self.assertEqual(actual.strip(), 'key1="value1" | key2="value2"')


class TestInitializeLogger(TestCase):

    @patch('common.log.logging', Mock())
    @patch('common.log.os')
    def test_initialize_logger_clears_the_global_meta_data_dict_and_sets_new_values(self, os_mock):
        log.LOG_RECORD_META_DATA = {'environment_name': 'old_environment_name',
                                    'function_name': 'old_function_name',
                                    'aws_request_id': 'old_aws_request_id'}

        lambda_context = Mock(spec=[])
        lambda_context.function_name = 'new_function_name'
        lambda_context.function_version = None
        os_mock.environ = {'ENVIRONMENT_NAME': 'new_environment_name', 'AWS_ACCOUNT_ID': 'aws_account_id'}

        expected_meta_data = {'environment_name': 'new_environment_name',
                              'aws_account_id': 'aws_account_id',
                              'function_name': 'new_function_name',
                              'aws_request_id': None}

        initialize_logger(lambda_context)

        self.assertDictEqual(expected_meta_data, log.LOG_RECORD_META_DATA)
