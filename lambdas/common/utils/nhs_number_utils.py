import re


def sanitise_nhs_number(nhs_number):
    sanitised_nhs_number = re.sub(r'[^a-zA-Z0-9\/\?]', "", nhs_number).upper()
    return sanitised_nhs_number
