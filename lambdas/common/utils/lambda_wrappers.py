import json
import os
from functools import wraps
from common.log_references import CommonLogReference
from common.log import initialize_logger, log, update_logger_metadata_fields
from common.utils.cookie_management_utils import delete_cookie_header, get_session_from_cookie
from common.utils.data_segregation.nhais_ciphers import Cohorts
from common.utils.data_segregation.workgroup_store import set_current_workgroups
from common.utils.session_utils import get_session
from common.utils.deep_ping import handle_deep_ping, is_deep_ping
from common.utils import set_internal_id


AUTH_ERROR_URL = os.environ.get('AUTH_ERROR_URL')


def is_xray_enabled() -> bool:
    xray_env_var = os.getenv('ENABLE_XRAY', 'FALSE')
    if isinstance(xray_env_var, str):
        return xray_env_var == 'TRUE'

    return False


def annotate_segment_with_environment_name():
    from aws_xray_sdk.core import xray_recorder
    xray_recorder.begin_subsegment('xray_initialisation')
    environment_name = os.getenv('ENVIRONMENT_NAME', '')
    segment = xray_recorder.current_subsegment()
    segment.put_annotation('environment_name', environment_name)
    xray_recorder.end_subsegment()


if is_xray_enabled():
    from aws_xray_sdk.core import patch_all
    patch_all()


# Lambda wrappers
def lambda_entry_point(lambda_handler):
    @wraps(lambda_handler)
    def wrapper(event={}, context={}):
        initialize_logger(lambda_context=context)
        set_internal_id(event)
        log(CommonLogReference.LAMBDA0000)

        if is_xray_enabled():
            annotate_segment_with_environment_name()

        if is_deep_ping(event):
            log(CommonLogReference.LAMBDA0006)
            return handle_deep_ping()

        try:
            if 'requestContext' in event.keys():
                session = json.loads(event.get('requestContext', {}).get('authorizer', {}).get('session', '{}'))
                workgroups = session.get('selected_role', {}).get('workgroups', [])
                log({'log_reference': CommonLogReference.LAMBDA0007, 'assigned_workgroups': workgroups})
            else:
                workgroups = Cohorts.list()
                log(CommonLogReference.LAMBDA0008)

            set_current_workgroups([Cohorts(workgroup) for workgroup in workgroups if workgroup in Cohorts.list()])
            return_value = lambda_handler(event, context)
            log(CommonLogReference.LAMBDA0001)
            return return_value
        except Exception as ex:
            log(CommonLogReference.EXCEPTION000)
            log(CommonLogReference.LAMBDA0002)
            raise ex
        finally:
            set_current_workgroups([])

    return wrapper
    """
    Initializes logger, logs success/failure of the lambda, and logs uncaught exceptions.
    This decorator should be used on every lambda.
    """


def auth_exception_wrapper(lambda_handler):
    @wraps(lambda_handler)
    def wrapper(*args, **kwargs):
        try:
            return_value = lambda_handler(*args, **kwargs)
            return return_value
        except Exception as ex:
            print(str(ex))
            return {
                'statusCode': 302,
                'headers': {
                    'location': f'{AUTH_ERROR_URL}?reason=unauthorised',
                    'Cookie': delete_cookie_header()
                }
            }
    return wrapper
    """
    Catches unhandled exceptions and redirects user to error page with appropriate message.
    This decorator should be used for all auth flow lambdas.
    This decorator must be the first one applied to ensure exceptions bubble up correctly.
    """


def auth_lambda(lambda_handler):
    @wraps(lambda_handler)
    def wrapper(*args, **kwargs):
        event_headers = args[0]['headers']
        session_token = event_headers and get_session_from_cookie(event_headers)
        session = session_token and get_session(session_token) or {}
        kwargs['session'] = session
        session_id = session.get('session_id', 'None') if session else 'None'
        update_logger_metadata_fields({
            'session_id': session_id
        })
        return_value = lambda_handler(*args, **kwargs)
        return return_value
    return wrapper
    """
    Adds session_id to logger metadata and passes session to wrapped lambda.
    This decorator should be used for all auth flow lambdas.
    This decorator must be placed _after_ @lambda_entry_point, which initializes the logger.
    """


def api_lambda(lambda_handler):
    @wraps(lambda_handler)
    def wrapper(*args, **kwargs):
        session_id = args[0]['requestContext']['authorizer']['principalId']
        request_id = args[0]['headers'].get('request_id')
        update_logger_metadata_fields({
            'request_id': request_id,
            'session_id': session_id
        })
        return_value = lambda_handler(*args, **kwargs)
        return return_value
    return wrapper
    """
    Adds session_id / request_id to logger metadata.
    This decorator should be used for all lambdas hit via API authorizer.
    This decorator must be placed _after_ @lambda_entry_point, which initializes the logger.
    """
