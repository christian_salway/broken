from common.utils.participant_utils import (
    query_participants_by_demographics,
    set_search_indexes,
    get_first_letter,
    query_participants_by_nhs_number,
    sanitise_string
)
from common.log import log
from common.log_references import CommonLogReference
from boto3.dynamodb.conditions import Key
from common.utils.participant_episode_utils import query_for_live_episode
from collections import defaultdict
from difflib import SequenceMatcher

SCORE_FULL_MATCH = 5
SCORE_AUTO_MATCH = 4
SCORE_MANUAL_MATCH = 3


def _get_matching_episode_address(formatted_record, participant):
    episode_addresses = _get_episode_addresses(participant)
    for episode_address in episode_addresses:
        if formatted_record["sanitised_postcode"] == sanitise_string(episode_address["postcode"]):
            return episode_address
    return None


def _get_episode_addresses(participant):
    episode_addresses = []
    live_episodes = query_for_live_episode(participant['participant_id'], False)

    for live_episode in live_episodes:
        for form_entry in live_episode.get('HMR101_form_data', []):
            if 'episode_address' in form_entry:
                episode_addresses.append(form_entry['episode_address'])

    return episode_addresses


def _score_match(formatted_record, participant):
    score = 0

    formatted_record_nhs_number = sanitise_string(formatted_record.get("nhs_number"))
    participant_nhs_number = sanitise_string(participant.get("nhs_number"))
    if formatted_record_nhs_number and participant_nhs_number:
        score += formatted_record_nhs_number == participant_nhs_number

    dob = formatted_record.get("date_of_birth", None)
    if dob:
        score += dob == participant["date_of_birth"]

    first_name = formatted_record.get("first_name", None)
    if first_name:
        score += sanitise_string(first_name) == sanitise_string(participant["first_name"])

    last_name = formatted_record.get("last_name", None)
    if last_name:
        score += sanitise_string(last_name) == sanitise_string(participant["last_name"])

    matching_episode_address = _get_matching_episode_address(formatted_record, participant)
    if matching_episode_address:
        participant['episode_address_match'] = True
        participant["letter_address"] = matching_episode_address
        score += 1
    elif formatted_record["sanitised_postcode"] == sanitise_string(participant["address"]["postcode"]):
        participant["letter_address"] = participant["address"]
        score += 1
    else:
        participant["letter_address"] = None

    return score


def _query_dob_initial_lastname(date_of_birth, initial, last_name):
    index = 'date-of-birth-initial-last-name'
    key_condition_expression = (
        Key('date_of_birth_and_initial').eq(f'{date_of_birth}{initial}') &
        Key('sanitised_last_name').eq(last_name)
    )

    return query_participants_by_demographics(index, key_condition_expression)


def _match_nhais_scoring(formatted_record):

    date_of_birth = formatted_record.get('date_of_birth', '')
    sanitised_last_name = formatted_record.get('sanitised_last_name', '')
    initial = get_first_letter(sanitised_last_name)
    nhs_number = formatted_record.get('nhs_number', None)

    potential_matches = []
    if nhs_number:
        sanitised_nhs_number = sanitise_string(nhs_number)
        potential_matches = query_participants_by_nhs_number(sanitised_nhs_number)
        if len(potential_matches) > 1:
            log({'log_reference': CommonLogReference.PARTICIPANT0004})
            log_match_ratios(formatted_record, potential_matches)
            return (None, map_participants(potential_matches))

    if (len(potential_matches) == 0) and date_of_birth and sanitised_last_name:
        log({'log_reference': CommonLogReference.PARTICIPANT0008})
        potential_matches = _query_dob_initial_lastname(date_of_birth, initial, sanitised_last_name)

    score_map = defaultdict(list)
    for potential_match in potential_matches:
        potential_match_score = _score_match(formatted_record, potential_match)
        score_map[potential_match_score].append(potential_match)

    if SCORE_FULL_MATCH in score_map:
        matched_participant = score_map[SCORE_FULL_MATCH][0]
        log({
            'log_reference': CommonLogReference.PARTICIPANT0002,
            'score': SCORE_FULL_MATCH
        })
        return (matched_participant, None)

    if SCORE_AUTO_MATCH in score_map:
        auto_match_participants = score_map[SCORE_AUTO_MATCH]
        if len(auto_match_participants) == 1:
            if auto_match_participants[0]['letter_address']:
                log({
                    'log_reference': CommonLogReference.PARTICIPANT0002,
                    'score': SCORE_AUTO_MATCH
                })
                return (auto_match_participants[0], None)
            log({'log_reference': CommonLogReference.PARTICIPANT0003})
            return (None, map_participants(auto_match_participants))
        log({'log_reference': CommonLogReference.PARTICIPANT0005})
        log_match_ratios(formatted_record, auto_match_participants)
        return (None, map_participants(auto_match_participants))

    if SCORE_MANUAL_MATCH in score_map:
        log({'log_reference': CommonLogReference.PARTICIPANT0006})
        log_match_ratios(formatted_record, score_map[SCORE_MANUAL_MATCH])
        return (None, map_participants(score_map[SCORE_MANUAL_MATCH]))

    log({'log_reference': CommonLogReference.PARTICIPANT0007})
    return (None, None)


def map_participant(participant):

    mapped_participant = {
        'participant_id': participant['participant_id'],
    }

    episode_addresses = _get_episode_addresses(participant)
    if episode_addresses:
        mapped_participant['episode_addresses'] = episode_addresses

    return mapped_participant


def map_participants(participants):
    return list(map(map_participant, participants))


def _get_match_ratio_and_diff(result_field, participant_field):
    matcher = SequenceMatcher(None, result_field, participant_field)
    ratio = matcher.ratio()

    if ratio < 1:
        differences = []
        for group in matcher.get_grouped_opcodes():
            for tag, result1, result2, participant1, participant2 in group:
                if tag != 'equal':
                    differences.append(
                        (
                            f'{tag.upper()}: Result: {result_field[result1:result2]}'
                            ' --> '
                            f'Participant: {participant_field[participant1:participant2]}'
                        )
                    )
        return (round(ratio, 2), differences)

    return (ratio, None)


def log_match_ratio(formatted_record, participant):

    nhs_number_ratio, nhs_number_diff = _get_match_ratio_and_diff(
        formatted_record.get('nhs_number', ''),
        participant.get('nhs_number', '')
    )

    dob_ratio, dob_diff = _get_match_ratio_and_diff(
        formatted_record.get('date_of_birth', ''),
        participant['date_of_birth']
    )

    if 'episode_address_match' in participant:
        postcode_ratio, postcode_diff = _get_match_ratio_and_diff(
            formatted_record.get("sanitised_postcode", ""),
            sanitise_string(participant["letter_address"]["postcode"])
        )
    else:
        postcode_ratio, postcode_diff = _get_match_ratio_and_diff(
            formatted_record.get("sanitised_postcode", ""),
            sanitise_string(participant["address"]["postcode"])
        )

    first_name_ratio, first_name_diff = _get_match_ratio_and_diff(
        formatted_record.get('sanitised_first_name', ''),
        sanitise_string(participant['first_name'])
    )

    last_name_ratio, last_name_diff = _get_match_ratio_and_diff(
        formatted_record.get('sanitised_last_name', ''),
        sanitise_string(participant['last_name'])
    )

    log({
        'log_reference': CommonLogReference.PARTICIPANT0009,
        'nhs_number': nhs_number_ratio,
        'date_of_birth': dob_ratio,
        'postcode': postcode_ratio,
        'first_name': first_name_ratio,
        'last_name': last_name_ratio,
        'participant_id': participant['participant_id']
    })

    diffs = {}

    if nhs_number_diff:
        diffs['nhs_number'] = nhs_number_diff
    if dob_diff:
        diffs['date_of_birth'] = dob_diff
    if postcode_diff:
        diffs['postcode_diff'] = postcode_diff
    if first_name_diff:
        diffs['first_name'] = first_name_diff
    if last_name_diff:
        diffs['last_name'] = last_name_diff

    if diffs:
        log({
            'log_reference': CommonLogReference.PARTICIPANT0010,
            'participant_id': participant['participant_id'],
            'sensitive_data': True,
            **diffs
        })


def log_match_ratios(formatted_record, particpants):
    for participant in particpants:
        log_match_ratio(formatted_record, participant)


def attempt_to_match(record):
    set_search_indexes(record)

    auto_matched_participant, manual_match_participants = _match_nhais_scoring(record)
    if auto_matched_participant:
        return {
            'participant': auto_matched_participant
        }
    if manual_match_participants:
        return {
            'manual_match_participants': manual_match_participants
        }
