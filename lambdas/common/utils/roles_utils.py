from enum import Enum


class AccessGroups(str, Enum):
    DMS = 'csas_dms'
    IOM = 'csas_iom'
    CIC = 'csas'


allowed_roles = {
    'R8000': 'Clinical Practitioner',
    'R8001': 'Nurse',
    'R8008': 'Support',
    'R8005': 'Lab',
    'R8010': 'Clerical',
}


def get_filtered_roles_list_from_user_data(user_orgs, nrbac_roles) -> list:
    roles = []
    for nrbac_role in nrbac_roles:
        nrbac_role_code = nrbac_role['role_code']
        role_code = get_role_code_from_role_hierarchy(nrbac_role_code)
        if role_code not in allowed_roles.keys():
            continue

        role = {'role_id': role_code}

        nrbac_role_name = nrbac_role['role_name']
        role['role_name'] = get_role_name_from_role_hierarchy(nrbac_role_name)
        role['activity_codes'] = nrbac_role.get('activity_codes', [])

        organisation_code = nrbac_role['org_code']
        role['organisation_code'] = organisation_code
        role['organisation_name'] = get_organisation_name_from_organisation_code(organisation_code, user_orgs)

        role['workgroups'] = nrbac_role.get('workgroups', [])

        roles.append(role)

    roles = sorted(roles, key=lambda i: i['role_name'])

    return roles


def get_role_name_from_role_hierarchy(nhs_identity_role_name):
    role_list = nhs_identity_role_name.replace('"', '').split(':')
    return role_list[-1]


def get_role_code_from_role_hierarchy(nhs_identity_role_code):
    code_list = nhs_identity_role_code.split(':')
    return code_list[-1]


def get_organisation_name_from_organisation_code(organisation_code, user_orgs):
    organisation = next(org for org in user_orgs if org.get('org_code') == organisation_code)
    return organisation['org_name']


def determine_access_group_from_session(session):
    access_groups = session['access_groups']

    if access_groups[AccessGroups.DMS]:
        return 'DMS'
    if access_groups[AccessGroups.IOM]:
        return 'IOM'
    if access_groups[AccessGroups.CIC]:
        return 'CIC'
    return None
