from common.utils.secrets_manager_utils import get_secret
import json
import io
import paramiko
from common.log import log
from common.log_references import CommonLogReference as LogReference


def exception_handler(failure_log):
    def real_decorator(function):
        def wrapper(*args):
            try:
                result = function(*args)
            except Exception as e:
                log({'log_reference': failure_log, 'add_exception_info': True})
                raise e
            return result
        return wrapper
    return real_decorator


@exception_handler(LogReference.SFTPEX0001)
def get_sftp_secrets(sftp_secret):
    secret_dict = json.loads(get_secret(sftp_secret, False), strict=False)
    return secret_dict['private-key'], secret_dict['host'], secret_dict['user_id']


@exception_handler(LogReference.SFTPEX0002)
def create_sftp_client(private_key, host, user_id):
    keyfile = io.StringIO(private_key)
    log({'log_reference': LogReference.SFTP0001})
    RSA_key = paramiko.RSAKey.from_private_key(keyfile)
    log({'log_reference': LogReference.SFTP0002, 'host': 'cic'})
    transport = paramiko.Transport((host, 22))
    log({'log_reference': LogReference.SFTP0003})
    transport.connect(username=user_id, pkey=RSA_key)
    log({'log_reference': LogReference.SFTP0004})
    return paramiko.SFTPClient.from_transport(transport)
