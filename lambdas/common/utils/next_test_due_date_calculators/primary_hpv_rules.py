from dataclasses import dataclass, field
import os
from typing import Callable, List, Tuple
from common.models.participant import ParticipantStatus
from common.log import log
from common.log_references import CommonLogReference


@dataclass(frozen=True)
class Rule:
    default_interval: Callable = field(default=None)
    allowable_intervals: List = field(default=None)
    allowable_range: Tuple = field(default=None)
    status: ParticipantStatus = field(default=None)
    ignore_for_calculations: bool = field(default=False)


def simple_interval(interval, *args):
    def return_simple_interval(*args):
        return interval
    return return_simple_interval


def age_dependant_interval(cut_off, interval_lt, interval_gte, *args):
    def return_age_dependant_interval(age):
        if age < cut_off:
            return interval_lt
        return interval_gte
    return return_age_dependant_interval


def get_positive_failsafe_repeat_months():
    def return_failsafe(*args):
        repeat_months = _parse_int_from_environment('POSITIVE_FAILSAFE_REPEAT_MONTHS')
        if not repeat_months:
            log({'log_reference': CommonLogReference.CONFIG0001, 'repeat_months': repeat_months})
            raise ValueError(CommonLogReference.CONFIG0001.message)
        return repeat_months
    return return_failsafe


def get_negative_failsafe_repeat_months():
    def return_failsafe(*args):
        repeat_months = _parse_int_from_environment('NEGATIVE_FAILSAFE_REPEAT_MONTHS')
        if not repeat_months:
            log({'log_reference': CommonLogReference.CONFIG0002, 'repeat_months': repeat_months})
            raise ValueError(CommonLogReference.CONFIG0002.message)
        return repeat_months
    return return_failsafe


def _parse_int_from_environment(environment_variable_name):
    variable = os.environ.get(environment_variable_name)
    return int(variable) if variable and variable.isdigit() else None


# DIGEST_CHECK_BEGIN
rules = {
    ('X', '0', 'A'): Rule(default_interval=age_dependant_interval(50, 36, 60), status=ParticipantStatus.ROUTINE),
    ('X', '0', 'R'): Rule(
        default_interval=simple_interval(36), allowable_intervals=[12, 6], status=ParticipantStatus.REPEAT_ADVISED
    ),
    ('X', '0', 'S'): Rule(
        default_interval=get_negative_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('X', 'U', 'R'): Rule(default_interval=simple_interval(3), status=ParticipantStatus.REPEAT_ADVISED),
    ('X', 'U', 'S'): Rule(
        default_interval=get_negative_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('X', '9', 'S'): Rule(
        default_interval=get_negative_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('1', '9', 'R'): Rule(default_interval=simple_interval(3), status=ParticipantStatus.INADEQUATE),
    ('1', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.INADEQUATE
    ),
    ('3', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('4', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('5', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('6', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('7', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('8', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('9', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('0', '9', 'R'): Rule(
        default_interval=simple_interval(12), allowable_intervals=[36], status=ParticipantStatus.REPEAT_ADVISED
    ),
    ('2', '9', 'R'): Rule(
        default_interval=simple_interval(12), allowable_intervals=[36], status=ParticipantStatus.REPEAT_ADVISED
    ),
    ('0', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('2', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    # Q infection codes are legacy but should be available for HPV Primary type results
    ('0', 'Q', 'R'): Rule(default_interval=simple_interval(12), status=ParticipantStatus.REPEAT_ADVISED),
    ('0', 'Q', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.ROUTINE
    ),
    ('1', 'Q', 'R'): Rule(
        default_interval=simple_interval(3), status=ParticipantStatus.REPEAT_ADVISED
    ),
    ('1', 'Q', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.INADEQUATE
    ),
    ('2', 'Q', 'R'): Rule(default_interval=simple_interval(12), status=ParticipantStatus.REPEAT_ADVISED),
    ('2', 'Q', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.ROUTINE
    ),
    ('3', 'Q', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('4', 'Q', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('5', 'Q', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('6', 'Q', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('7', 'Q', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('8', 'Q', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('9', 'Q', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
}
# DIGEST_CHECK_END


def get_primary_hpv_rules_digest():
    return 'ab9764cd7b85cad75cd3b67b7e87b0b7'  # only change this when changing rules above
