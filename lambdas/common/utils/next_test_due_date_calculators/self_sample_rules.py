from common.models.participant import ParticipantStatus
from common.utils.next_test_due_date_calculators.primary_hpv_rules import simple_interval, age_dependant_interval, Rule

# DIGEST_CHECK_BEGIN
rules = {
    ('X', '0', 'A'): Rule(default_interval=age_dependant_interval(50, 36, 60), status=ParticipantStatus.ROUTINE),
    ('X', '9', 'R'): Rule(default_interval=simple_interval(3), status=ParticipantStatus.REPEAT_ADVISED),
    # XUH result code must be recorded but it is ignored for the purposes of calculating a NTDD or status change
    ('X', 'U', 'H'): Rule(ignore_for_calculations=True),
}
# DIGEST_CHECK_END


def get_self_sample_rules_digest():
    return 'a2b42eb78a5ec75dc35b542d5b3b51ed'  # only change this when changing rules above
