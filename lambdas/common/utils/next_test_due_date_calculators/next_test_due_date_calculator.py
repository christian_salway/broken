from datetime import datetime, timedelta, timezone, date
from dateutil.relativedelta import relativedelta
from nose.tools import nottest
from common.models.participant import ParticipantStatus, CeaseReason
from common.utils.result_codes import (
    RECALL_TYPES,
    get_concatenated_result_code_combination
)
from common.utils.next_test_due_date_calculators.primary_hpv_rules import rules as hpv_primary_rules
from common.utils.next_test_due_date_calculators.self_sample_rules import rules as self_sample_rules
from common.utils.next_test_due_date_calculators.cytology_and_hpv_rules import rules as cytology_and_hpv_rules
from common.utils.next_test_due_date_calculators.cytology_rules import rules as cytology_rules
from common.services.reinstate.reinstate_reasons import Explanation


ALL_CEASE_REASONS = [
    CeaseReason.NO_CERVIX,
    CeaseReason.PATIENT_INFORMED_CHOICE,
    CeaseReason.RADIOTHERAPY,
    CeaseReason.MENTAL_CAPACITY_ACT
]

RULE_SETS = {
    (True, False): hpv_primary_rules,
    (True, True): self_sample_rules,
    (False, False): {
        **cytology_and_hpv_rules,
        **cytology_rules,
    },
}


# Participant who has never been screened logic
@nottest
def calculate_next_test_due_date_for_new_participant(date_of_birth: date) -> date:
    if participant_is_less_than_equal_to_24_years_and_22_weeks(date_of_birth):
        twenty_fifth_birthday = date_of_birth + relativedelta(years=25)
        return (twenty_fifth_birthday - relativedelta(weeks=20))
    else:
        return default_next_test_due_date()


def participant_is_less_than_equal_to_24_years_and_22_weeks(date_of_birth: date):
    today = datetime.now(timezone.utc).date()
    age_offset = today - relativedelta(years=24, weeks=22)
    return date_of_birth >= age_offset


NO_RULES_MESSAGE = 'No profile for the provided combination of hpv_primary and self_sample'
NO_MATCH_MESSAGE = 'No matching profile for results found when calculating next test due date'
IGNORE_FOR_CALCULATIONS_MESSAGE = 'This result code should be ignored for calculating next test due date'
INVALID_DATE_MESSAGE = 'You must provide a valid test date'


def get_rule(hpv_primary, self_sample, result):
    rules = RULE_SETS.get((hpv_primary, self_sample))
    if not rules:
        raise ValueError(NO_RULES_MESSAGE)
    return rules.get(result)


# Add/edit result NTDD calculator. Returns an object with possible next_test_due_dates and participant status
@nottest
def calculate_next_test_due_date_for_result(test_date, result, date_of_birth, hpv_primary=True, self_sample=False):
    if not test_date:
        raise ValueError(INVALID_DATE_MESSAGE)
    rule = get_rule(hpv_primary, self_sample, result)
    if not rule:
        raise ValueError(NO_MATCH_MESSAGE)
    elif rule.ignore_for_calculations:
        raise ValueError(IGNORE_FOR_CALCULATIONS_MESSAGE)

    def info_for_interval(interval):
        return {
            'interval': interval,
            'next_test_due_date': calculate_next_test_due_date(test_date, interval)
        }

    default = None
    if rule.default_interval:
        age = calculate_age_in_years_at_last_test(test_date, date_of_birth)
        default_interval = rule.default_interval(age)
        default = info_for_interval(default_interval)

    options = None
    if rule.allowable_intervals:
        options = list(map(info_for_interval, rule.allowable_intervals))

    range_intervals = None

    if rule.allowable_range:
        min_interval = rule.allowable_range[0]
        max_interval = rule.allowable_range[1]
        range_intervals = [{
            'min_interval': min_interval,
            'max_interval': max_interval,
        }]
        for interval in range(min_interval, max_interval + 1):
            range_intervals.append(info_for_interval(interval))

    return {
        'default': default,
        'options': options,
        'range': range_intervals,
        'status': rule.status
    }


# Reinstate logic
# Test results must be ordered newest first
@nottest
def calculate_next_test_due_date_and_status_for_ceased_participant(participant, test_results, cease_reason):
    date_of_birth = datetime.strptime(participant['date_of_birth'], '%Y-%m-%d').date()

    last_test_result = None
    last_test_date = None
    for test_result in test_results:
        last_test_code = (
            (test_result['result_code'], test_result.get('infection_code', None), test_result['action_code'])
        )
        last_test_rule = get_rule(
            test_result.get('hpv_primary', True),
            test_result.get('self_sample', False),
            last_test_code
        )
        if not last_test_rule:
            raise ValueError(NO_MATCH_MESSAGE)
        if not last_test_rule.ignore_for_calculations:
            last_test_result = test_result
            last_test_date = datetime.strptime(last_test_result['test_date'], '%Y-%m-%d').date()
            break

    age_at_last_test = calculate_age_in_years_at_last_test(last_test_date, date_of_birth)

    status = calculate_status_from_test_results(test_results)

    default_date = default_next_test_due_date()
    explanation = []

    if cease_reason == CeaseReason.DUE_TO_AGE:
        explanation.append(Explanation.CEASED_DUE_TO_AGE.value)
        if not last_test_result:
            explanation.append(Explanation.NO_PREVIOUS_TEST.value)
            return build_response(ParticipantStatus.ROUTINE, default_date, explanation)
        elif status != ParticipantStatus.ROUTINE.value:
            explanation.append(Explanation.STATUS_NOT_ROUTINE.value)
            return build_response(ParticipantStatus.ROUTINE, default_date, explanation)
        elif age_at_last_test < 60:
            explanation.append(Explanation.UNDER_60.value)
            return build_response(ParticipantStatus.ROUTINE, default_date, explanation)
        else:
            explanation.append(Explanation.OVER_59.value)
            return reject(explanation)

    if cease_reason not in ALL_CEASE_REASONS:
        raise Exception('Participant ceased for unrecognised reason.')

    if not last_test_result or not last_test_date:
        explanation.append(Explanation.NO_PREVIOUS_TEST.value)
        next_test_due_date = calculate_next_test_due_date_for_new_participant(date_of_birth)
        return build_response(ParticipantStatus.CALLED, next_test_due_date, explanation)
    if status == ParticipantStatus.SUSPENDED.value:
        explanation.append(Explanation.SUSPENDED_STATUS.value)
        next_test_due_date = calculate_next_test_due_date(last_test_date, 12)
        return build_response(ParticipantStatus.SUSPENDED, next_test_due_date, explanation)
    if status == ParticipantStatus.INADEQUATE.value:
        explanation.append(Explanation.INADEQUATE_STATUS.value)
        next_test_due_date = calculate_next_test_due_date(last_test_date, 3)
        return build_response(ParticipantStatus.INADEQUATE, next_test_due_date, explanation)
    if status == ParticipantStatus.REPEAT_ADVISED.value:
        recall_months = int(last_test_result['recall_months'])
        next_test_due_date = calculate_next_test_due_date(last_test_date, recall_months)
        explanation.append(Explanation.REPEAT_ADVISED_STATUS.value)
        return build_response(ParticipantStatus.REPEAT_ADVISED, next_test_due_date, explanation)
    if status == ParticipantStatus.ROUTINE.value:
        explanation.append(Explanation.ROUTINE_STATUS.value)
        if age_at_last_test < 50:
            next_test_due_date = calculate_next_test_due_date(last_test_date, 36)
            explanation.append(Explanation.UNDER_50.value)
            return build_response(ParticipantStatus.ROUTINE, next_test_due_date, explanation)
        elif age_at_last_test in range(50, 60):
            next_test_due_date = calculate_next_test_due_date(last_test_date, 60)
            explanation.append(Explanation.BETWEEN_50_AND_59.value)
            return build_response(ParticipantStatus.ROUTINE, next_test_due_date, explanation)
        else:
            explanation.append(Explanation.OVER_59.value)
            return reject(explanation)

    raise Exception(f'Unimplemented branch: status={status} age={age_at_last_test} cease_reason={cease_reason}')


def reject(explanation):
    return {'status': None, 'explanation': explanation}


def build_response(status: ParticipantStatus, next_test_due_date: date, explanation):
    return {'status': status.value, 'next_test_due_date': next_test_due_date.isoformat(), 'explanation': explanation}


@nottest
def calculate_age_in_years_at_last_test(last_test_date, date_of_birth):
    if not last_test_date:
        return None
    return relativedelta(last_test_date, date_of_birth).years


@nottest
def calculate_status_from_test_results(test_results):
    if not test_results:
        return None

    statuses = []
    for test_result in test_results:
        concatenated_result_and_action_codes = get_concatenated_result_code_combination(test_result)  # eg X0A
        status = 'Unknown'
        for recall_type, valid_combinations in RECALL_TYPES.items():
            if concatenated_result_and_action_codes in valid_combinations:
                status = recall_type
                break
        statuses.append(status)

    status = next((status for status in statuses if status != 'Unchanged'), 'Unknown')

    if status == 'Unknown':
        raise Exception('Participant status cannot be calculated from previous test results.')

    return status


@nottest
def default_next_test_due_date() -> date:
    return (datetime.now(timezone.utc) + timedelta(weeks=10, days=1)).date()


@nottest
def calculate_next_test_due_date(last_test_date: date, recall_months: int) -> date:
    potential_next_test_due_date = last_test_date + relativedelta(months=recall_months)
    default_date = default_next_test_due_date()
    return potential_next_test_due_date if potential_next_test_due_date > default_date else default_date


def calculate_participants_years_of_age(date_of_birth):
    today = datetime.now(timezone.utc).date()
    return today.year - date_of_birth.year - ((today.month, today.day) < (date_of_birth.month, date_of_birth.day))


# Closing episode
@nottest
def calculate_next_test_due_date_for_closed_episode(participant, episode):
    date_of_birth = datetime.strptime(participant['date_of_birth'], '%Y-%m-%d')
    age = calculate_participants_years_of_age(date_of_birth)

    test_due_date = datetime.strptime(episode['test_due_date'], '%Y-%m-%d')

    if participant['status'].upper() in [ParticipantStatus.CALLED, ParticipantStatus.ROUTINE]:
        if age < 50:
            proposed_test_due_date = test_due_date + relativedelta(months=36)
        elif age >= 50:
            proposed_test_due_date = test_due_date + relativedelta(months=60)
        else:
            return None
    elif participant['status'].upper() in [
        ParticipantStatus.INADEQUATE,
        ParticipantStatus.REPEAT_ADVISED,
        ParticipantStatus.SUSPENDED
    ]:
        proposed_test_due_date = test_due_date + relativedelta(months=12)
    else:
        return None

    return proposed_test_due_date.date()
