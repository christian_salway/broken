from datetime import datetime
from common.models.participant import CeaseReason, ParticipantStatus
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator import (
    default_next_test_due_date, calculate_status_from_test_results,
    calculate_participants_years_of_age, calculate_next_test_due_date_for_new_participant,
    calculate_next_test_due_date, calculate_next_test_due_date_for_result, calculate_age_in_years_at_last_test)
from common.services.reinstate.reinstate_response import ReinstateResponse
from common.services.reinstate.reinstate_result import ReinstateResult
from common.services.reinstate.reinstate_reasons import Explanation
from common.services.reinstate.reinstate_status import (
    calculate_status_from_last_3_test_results_for_manual_reinstate,
    calculate_status_from_latest_test_result_for_manual_reinstate
)


VALID_CEASE_REASONS = [
    CeaseReason.DUE_TO_AGE,
    CeaseReason.AUTOCEASE_DUE_TO_AGE,
    CeaseReason.NO_CERVIX,
    CeaseReason.PATIENT_INFORMED_CHOICE,
    CeaseReason.RADIOTHERAPY,
    CeaseReason.MENTAL_CAPACITY_ACT
]

# Automatic


def calculate(participant: dict, test_results: list, cease_reason: str) -> ReinstateResponse:
    reinstate_stratergy = reinstate_strategies(cease_reason, test_results)
    if not reinstate_stratergy:
        return ReinstateResponse(result=ReinstateResult.UNKNOWN_STRATEGY, error=True)

    try:
        return reinstate_stratergy(participant, test_results)
    except Exception:
        return ReinstateResponse(result=ReinstateResult.ERROR, error=True)


def reinstate_strategies(cease_reason: str, test_results: list):
    root_strategies = {
        CeaseReason.DUE_TO_AGE: due_to_age_stratergy,
        CeaseReason.AUTOCEASE_DUE_TO_AGE: due_to_age_stratergy
    }
    if cease_reason in root_strategies:
        return root_strategies[cease_reason]

    if cease_reason in VALID_CEASE_REASONS:
        recent_test_result_status = calculate_status_from_test_results(test_results)
        if not recent_test_result_status:
            return no_previous_test_strategy

        secondary_route_strategies = {
            ParticipantStatus.ROUTINE: routine_normal_stratergy,
            ParticipantStatus.REPEAT_ADVISED: repeat_advised_stratergy,
            ParticipantStatus.INADEQUATE: inadequate_stratergy,
            ParticipantStatus.SUSPENDED: suspended_stratergy
        }

        return secondary_route_strategies.get(recent_test_result_status)

    return None


def due_to_age_stratergy(participant: dict, test_results: list) -> ReinstateResponse:
    recent_test_result_status = calculate_status_from_test_results(test_results)
    date_of_birth = datetime.strptime(participant['date_of_birth'], '%Y-%m-%d').date()
    years_of_age = calculate_participants_years_of_age(date_of_birth)
    explanations = [Explanation.CEASED_DUE_TO_AGE.value]

    if not recent_test_result_status:
        explanations.append(Explanation.NO_PREVIOUS_TEST.value)

        return ReinstateResponse(
            status=ParticipantStatus.CALLED,
            next_test_due_date=default_next_test_due_date(),
            explanation=explanations
        )
    elif recent_test_result_status != ParticipantStatus.ROUTINE:
        explanations.append(Explanation.STATUS_NOT_ROUTINE.value)
        return ReinstateResponse(
            status=ParticipantStatus.ROUTINE,
            next_test_due_date=default_next_test_due_date(),
            explanation=explanations
        )
    elif years_of_age > 60:
        explanations.append(Explanation.OVER_59.value)
        return ReinstateResponse(
            status=ParticipantStatus.ROUTINE,
            next_test_due_date=default_next_test_due_date(),
            explanation=explanations
        )

    explanations.append(Explanation.OVER_59.value)
    return ReinstateResponse(
        explanation=explanations
    )


def no_previous_test_strategy(participant: dict, test_results: list) -> ReinstateResponse:
    date_of_birth = datetime.strptime(participant['date_of_birth'], '%Y-%m-%d').date()
    explanations = [Explanation.NO_PREVIOUS_TEST.value]
    next_test_due_date = calculate_next_test_due_date_for_new_participant(date_of_birth)
    return ReinstateResponse(
        status=ParticipantStatus.CALLED,
        next_test_due_date=next_test_due_date,
        explanation=explanations
    )


def routine_normal_stratergy(participant: dict, test_results: list) -> ReinstateResponse:
    explanations = [Explanation.ROUTINE_STATUS.value]
    last_test_date = datetime.strptime(test_results[0]['test_date'], '%Y-%m-%d').date()
    date_of_birth = datetime.strptime(participant['date_of_birth'], '%Y-%m-%d').date()
    age_at_last_test = calculate_age_in_years_at_last_test(last_test_date, date_of_birth)
    if age_at_last_test < 50:
        explanations.append(Explanation.UNDER_50.value)
        next_test_due_date = calculate_next_test_due_date(last_test_date, 36)

        return ReinstateResponse(
            status=ParticipantStatus.ROUTINE,
            next_test_due_date=next_test_due_date,
            explanation=explanations
        )
    elif age_at_last_test in range(50, 59):
        explanations.append(Explanation.BETWEEN_50_AND_59.value)
        next_test_due_date = calculate_next_test_due_date(last_test_date, 60)

        return ReinstateResponse(
            status=ParticipantStatus.ROUTINE,
            next_test_due_date=next_test_due_date,
            explanation=explanations
        )
    else:
        explanations.append(Explanation.OVER_59.value)

    return ReinstateResponse(
        explanation=explanations
    )


def repeat_advised_stratergy(participant: dict, test_results: list) -> ReinstateResponse:
    explanations = [Explanation.REPEAT_ADVISED_STATUS.value]
    result = test_results[0]
    test_date = datetime.fromisoformat(result['test_date']).date()
    next_test_due_dates = calculate_next_test_due_date_for_result(
        test_date,
        (result['result_code'], result['infection_code'], result['action_code']),
        datetime.fromisoformat(participant['date_of_birth']).date(),
        result.get('hpv_primary', True),
        result.get('self_sample', False),
    )
    calculated_ntdd = next_test_due_dates.get('default', {}).get('next_test_due_date')

    return ReinstateResponse(
        status=ParticipantStatus.REPEAT_ADVISED,
        next_test_due_date=calculated_ntdd,
        explanation=explanations
    )


def inadequate_stratergy(participant: dict, test_results: list) -> ReinstateResponse:
    explanations = [Explanation.INADEQUATE_STATUS.value]
    result = test_results[0]
    test_date = datetime.fromisoformat(result['test_date']).date()
    next_test_due_dates = calculate_next_test_due_date_for_result(
        test_date,
        (result['result_code'], result['infection_code'], result['action_code']),
        datetime.fromisoformat(participant['date_of_birth']).date(),
        result.get('hpv_primary', True),
        result.get('self_sample', False),
    )
    calculated_ntdd = next_test_due_dates.get('default', {}).get('next_test_due_date')

    return ReinstateResponse(
        status=ParticipantStatus.INADEQUATE,
        next_test_due_date=calculated_ntdd,
        explanation=explanations
    )


def suspended_stratergy(participant: dict, test_results: list) -> ReinstateResponse:
    explanations = [Explanation.SUSPENDED_STATUS.value]
    result = test_results[0]
    test_date = datetime.fromisoformat(result['test_date']).date()
    next_test_due_dates = calculate_next_test_due_date_for_result(
        test_date,
        (result['result_code'], result['infection_code'], result['action_code']),
        datetime.fromisoformat(participant['date_of_birth']).date(),
        result.get('hpv_primary', True),
        result.get('self_sample', False),
    )
    calculated_ntdd = next_test_due_dates.get('default', {}).get('next_test_due_date')

    return ReinstateResponse(
        status=ParticipantStatus.SUSPENDED,
        next_test_due_date=calculated_ntdd,
        explanation=explanations
    )

# Manual


def calculate_for_manual_reinstate(participant: dict, test_results: list, cease_reason: str) -> ReinstateResponse:
    try:
        return use_manual_reinstate_strategy(participant, test_results, cease_reason)
    except Exception:
        return ReinstateResponse(result=ReinstateResult.ERROR, error=True)


def use_manual_reinstate_strategy(participant: dict, test_results: list, cease_reason: str) -> ReinstateResponse:
    date_of_birth = datetime.strptime(participant['date_of_birth'], '%Y-%m-%d').date()
    years_of_age = calculate_participants_years_of_age(date_of_birth)

    if cease_reason in [CeaseReason.DUE_TO_AGE, CeaseReason.AUTOCEASE_DUE_TO_AGE] or years_of_age > 59:
        status = calculate_status_from_last_3_test_results_for_manual_reinstate(test_results)
        return manual_due_to_age_strategy(status, years_of_age)

    if cease_reason in VALID_CEASE_REASONS:
        status, result_index = calculate_status_from_latest_test_result_for_manual_reinstate(test_results)
        if not status:
            return manual_no_previous_test_strategy(date_of_birth)
        elif status == ParticipantStatus.ROUTINE:
            return manual_routine_normal_strategy(date_of_birth, test_results[result_index])
        elif status == ParticipantStatus.REPEAT_ADVISED:
            return manual_repeat_advised_strategy(test_results[result_index])
        else:
            secondary_route_strategies = {
                ParticipantStatus.INADEQUATE: manual_inadequate_strategy,
                ParticipantStatus.SUSPENDED: manual_suspended_strategy
            }
            strategy = secondary_route_strategies.get(status)
            return strategy(participant, test_results[result_index])

    return ReinstateResponse(result=ReinstateResult.UNKNOWN_STRATEGY, error=True)


def manual_due_to_age_strategy(recent_test_result_status, years_of_age) -> ReinstateResponse:
    explanations = [Explanation.CEASED_DUE_TO_AGE.value]

    if not recent_test_result_status:
        explanations.append(Explanation.NO_PREVIOUS_TEST.value)
        return ReinstateResponse(
            status=ParticipantStatus.CALLED,
            next_test_due_date=default_next_test_due_date(),
            explanation=explanations
        )

    elif recent_test_result_status != ParticipantStatus.ROUTINE:
        explanations.append(Explanation.STATUS_NOT_ROUTINE.value)
        return ReinstateResponse(
            status=ParticipantStatus.ROUTINE,
            next_test_due_date=default_next_test_due_date(),
            explanation=explanations
        )

    else:
        explanations.append(Explanation.OVER_59.value)
        return ReinstateResponse(
            explanation=explanations
        )


def manual_no_previous_test_strategy(date_of_birth) -> ReinstateResponse:
    explanations = [Explanation.NO_PREVIOUS_TEST.value]
    next_test_due_date = calculate_next_test_due_date_for_new_participant(date_of_birth)
    return ReinstateResponse(
        status=ParticipantStatus.CALLED,
        next_test_due_date=next_test_due_date,
        explanation=explanations
    )


def manual_routine_normal_strategy(date_of_birth, result) -> ReinstateResponse:
    explanations = [Explanation.ROUTINE_STATUS.value]
    last_test_date = datetime.strptime(result['test_date'], '%Y-%m-%d').date()
    age_at_last_test = calculate_age_in_years_at_last_test(last_test_date, date_of_birth)

    if age_at_last_test < 50:
        explanations.append(Explanation.UNDER_50.value)
        next_test_due_date = calculate_next_test_due_date(last_test_date, 36)
        return ReinstateResponse(
            status=ParticipantStatus.ROUTINE,
            next_test_due_date=next_test_due_date,
            explanation=explanations
        )

    elif age_at_last_test in range(50, 59):
        explanations.append(Explanation.BETWEEN_50_AND_59.value)
        next_test_due_date = calculate_next_test_due_date(last_test_date, 60)
        return ReinstateResponse(
            status=ParticipantStatus.ROUTINE,
            next_test_due_date=next_test_due_date,
            explanation=explanations
        )

    else:
        explanations.append(Explanation.OVER_59.value)
        return ReinstateResponse(
            explanation=explanations
        )


def manual_repeat_advised_strategy(result: dict) -> ReinstateResponse:
    explanations = [Explanation.REPEAT_ADVISED_STATUS.value]
    test_date = datetime.fromisoformat(result['test_date']).date()
    recall_months = int(result['recall_months'])
    calculated_ntdd = calculate_next_test_due_date(test_date, recall_months)

    return ReinstateResponse(
        status=ParticipantStatus.REPEAT_ADVISED,
        next_test_due_date=calculated_ntdd,
        explanation=explanations
    )


def manual_inadequate_strategy(participant: dict, result: dict) -> ReinstateResponse:
    explanations = [Explanation.INADEQUATE_STATUS.value]
    test_date = datetime.fromisoformat(result['test_date']).date()
    next_test_due_dates = calculate_next_test_due_date_for_result(
        test_date,
        (result['result_code'], result['infection_code'], result['action_code']),
        datetime.fromisoformat(participant['date_of_birth']).date(),
        result.get('hpv_primary', True),
        result.get('self_sample', False),
    )
    calculated_ntdd = next_test_due_dates.get('default', {}).get('next_test_due_date')

    return ReinstateResponse(
        status=ParticipantStatus.INADEQUATE,
        next_test_due_date=calculated_ntdd,
        explanation=explanations
    )


def manual_suspended_strategy(participant: dict, result: dict) -> ReinstateResponse:
    explanations = [Explanation.SUSPENDED_STATUS.value]
    test_date = datetime.fromisoformat(result['test_date']).date()
    next_test_due_dates = calculate_next_test_due_date_for_result(
        test_date,
        (result['result_code'], result['infection_code'], result['action_code']),
        datetime.fromisoformat(participant['date_of_birth']).date(),
        result.get('hpv_primary', True),
        result.get('self_sample', False),
    )
    calculated_ntdd = next_test_due_dates.get('default', {}).get('next_test_due_date')

    return ReinstateResponse(
        status=ParticipantStatus.SUSPENDED,
        next_test_due_date=calculated_ntdd,
        explanation=explanations
    )
