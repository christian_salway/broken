from common.models.participant import ParticipantStatus
from common.utils.next_test_due_date_calculators.primary_hpv_rules import \
    get_positive_failsafe_repeat_months, get_negative_failsafe_repeat_months,\
    simple_interval, age_dependant_interval, Rule

# DIGEST_CHECK_BEGIN
rules = {
    ('B', '0', 'A'): Rule(default_interval=age_dependant_interval(50, 36, 60), status=ParticipantStatus.ROUTINE),
    ('B', '0', 'R'): Rule(allowable_range=(1, 36), status=ParticipantStatus.REPEAT_ADVISED),
    ('B', '0', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('B', '9', 'R'): Rule(allowable_range=(1, 12), status=ParticipantStatus.REPEAT_ADVISED),
    ('B', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('B', 'U', 'R'): Rule(allowable_range=(1, 6), status=ParticipantStatus.REPEAT_ADVISED),
    ('B', 'U', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('E', '0', 'A'): Rule(default_interval=age_dependant_interval(50, 36, 60), status=ParticipantStatus.ROUTINE),
    ('E', '0', 'R'): Rule(allowable_range=(1, 36), status=ParticipantStatus.REPEAT_ADVISED),
    ('E', '0', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('E', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('E', 'U', 'R'): Rule(allowable_range=(1, 6), status=ParticipantStatus.REPEAT_ADVISED),
    ('E', 'U', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('G', '0', 'A'): Rule(default_interval=age_dependant_interval(50, 36, 60), status=ParticipantStatus.ROUTINE),
    ('G', '0', 'R'): Rule(allowable_range=(1, 36), status=ParticipantStatus.REPEAT_ADVISED),
    ('G', '0', 'S'): Rule(
        default_interval=get_negative_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('G', '9', 'R'): Rule(default_interval=simple_interval(12), status=ParticipantStatus.REPEAT_ADVISED),
    ('G', '9', 'S'): Rule(
        default_interval=get_negative_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('G', 'U', 'R'): Rule(allowable_range=(1, 6), status=ParticipantStatus.REPEAT_ADVISED),
    ('G', 'U', 'S'): Rule(
        default_interval=get_negative_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('M', '0', 'A'): Rule(default_interval=age_dependant_interval(50, 36, 60), status=ParticipantStatus.ROUTINE),
    ('M', '0', 'R'): Rule(allowable_range=(1, 36), status=ParticipantStatus.REPEAT_ADVISED),
    ('M', '0', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('M', '9', 'R'): Rule(allowable_range=(1, 12), status=ParticipantStatus.REPEAT_ADVISED),
    ('M', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('M', 'U', 'R'): Rule(allowable_range=(1, 6), status=ParticipantStatus.REPEAT_ADVISED),
    ('M', 'U', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('N', '0', 'A'): Rule(default_interval=age_dependant_interval(50, 36, 60), status=ParticipantStatus.ROUTINE),
    ('N', '0', 'R'): Rule(allowable_range=(1, 36), status=ParticipantStatus.REPEAT_ADVISED),
    ('N', '0', 'S'): Rule(
        default_interval=get_negative_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('N', '9', 'R'): Rule(default_interval=simple_interval(12), status=ParticipantStatus.REPEAT_ADVISED),
    ('N', '9', 'S'): Rule(
        default_interval=get_negative_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('N', 'U', 'R'): Rule(allowable_range=(1, 6), status=ParticipantStatus.REPEAT_ADVISED),
    ('N', 'U', 'S'): Rule(
        default_interval=get_negative_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('4', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('4', '0', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('4', 'U', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('5', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('5', '0', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('5', 'U', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('6', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('6', '0', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('6', 'U', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('7', '9', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('7', '0', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    ('7', 'U', 'S'): Rule(
        default_interval=get_positive_failsafe_repeat_months(), status=ParticipantStatus.SUSPENDED
    ),
    # E9R code has never been allowed in the system and should be considered invalid
}
# DIGEST_CHECK_END


def get_cytology_and_hpv_rules_digest():
    return 'b7461ff4ebe088d5cd0e1a208e1f572f'  # only change this when changing rules above
