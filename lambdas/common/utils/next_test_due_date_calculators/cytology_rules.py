from common.models.participant import ParticipantStatus
from common.utils.next_test_due_date_calculators.primary_hpv_rules import \
    get_positive_failsafe_repeat_months, get_negative_failsafe_repeat_months,\
    age_dependant_interval, Rule

# DIGEST_CHECK_BEGIN
rules = {
    ("0", None, "A"): Rule(default_interval=age_dependant_interval(50, 36, 60), status=ParticipantStatus.ROUTINE),
    ("0", None, "H"): Rule(ignore_for_calculations=True),
    ("0", None, "R"): Rule(allowable_range=(1, 36), status=ParticipantStatus.REPEAT_ADVISED),
    ("0", None, "S"): Rule(
        default_interval=get_negative_failsafe_repeat_months(),
        allowable_range=(1, 12),
        status=ParticipantStatus.SUSPENDED
    ),
    ("1", None, "H"): Rule(ignore_for_calculations=True),
    ("1", None, "R"): Rule(allowable_range=(1, 6), status=ParticipantStatus.INADEQUATE),
    ("1", None, "S"): Rule(
        default_interval=get_positive_failsafe_repeat_months(),
        allowable_range=(1, 12),
        status=ParticipantStatus.INADEQUATE
    ),
    ("2", None, "A"): Rule(default_interval=age_dependant_interval(50, 36, 60), status=ParticipantStatus.ROUTINE),
    ("2", None, "H"): Rule(ignore_for_calculations=True),
    ("2", None, "R"): Rule(allowable_range=(1, 36), status=ParticipantStatus.REPEAT_ADVISED),
    ("2", None, "S"): Rule(
        default_interval=get_negative_failsafe_repeat_months(),
        allowable_range=(1, 12),
        status=ParticipantStatus.SUSPENDED
    ),
    ("3", None, "R"): Rule(allowable_range=(1, 6), status=ParticipantStatus.REPEAT_ADVISED),
    ("3", None, "S"): Rule(
        default_interval=get_positive_failsafe_repeat_months(),
        allowable_range=(1, 12),
        status=ParticipantStatus.SUSPENDED
    ),
    ("4", None, "S"): Rule(
        default_interval=get_positive_failsafe_repeat_months(),
        allowable_range=(1, 12),
        status=ParticipantStatus.SUSPENDED
    ),
    ("5", None, "S"): Rule(
        default_interval=get_positive_failsafe_repeat_months(),
        allowable_range=(1, 12),
        status=ParticipantStatus.SUSPENDED
    ),
    ("6", None, "S"): Rule(
        default_interval=get_positive_failsafe_repeat_months(),
        allowable_range=(1, 12),
        status=ParticipantStatus.SUSPENDED
    ),
    ("7", None, "S"): Rule(
        default_interval=get_positive_failsafe_repeat_months(),
        allowable_range=(1, 12),
        status=ParticipantStatus.SUSPENDED
    ),
    ("8", None, "R"): Rule(allowable_range=(1, 12), status=ParticipantStatus.REPEAT_ADVISED),
    ("8", None, "S"): Rule(
        default_interval=get_positive_failsafe_repeat_months(),
        allowable_range=(1, 12),
        status=ParticipantStatus.SUSPENDED
    ),
    ("9", None, "R"): Rule(allowable_range=(1, 12), status=ParticipantStatus.REPEAT_ADVISED),
    ("9", None, "S"): Rule(
        default_interval=get_positive_failsafe_repeat_months(),
        allowable_range=(1, 12),
        status=ParticipantStatus.SUSPENDED
    ),
}
# DIGEST_CHECK_END


def get_cytology_rules_digest():
    return '852860433190c50cb2278b4abee50418'  # only change this when changing rules above
