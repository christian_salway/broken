import os
import logging
import json
import boto3
import requests
from datetime import datetime, timezone
from common.log_references import BaseLogReference
from common.log import log
from common.utils import json_return_message
from common.utils.dynamodb_access.deep_ping import check_dynamodb_table

DEEP_PING_PREFIX = 'deep_ping'
DEEP_PING_MESSAGE = 'deep_ping'
DEEP_PING_MESSAGE_BODY = {'message': DEEP_PING_MESSAGE}


class LogReference(BaseLogReference):
    DEEPPING0001 = (logging.INFO, 'Deep ping disabled due to environment variable "DISABLE_DEEP_PING"')
    DEEPPING0002 = (logging.INFO, 'Fetched dependencies from environment')
    DEEPPING0003 = (logging.INFO, 'Connecting to dynamodb')
    DEEPPING0004 = (logging.INFO, 'Successfully connected to dynamodb')
    DEEPPING0005 = (logging.ERROR, 'Failed to connect to dynamodb')
    DEEPPING0006 = (logging.INFO, 'Sending deep ping message to sqs')
    DEEPPING0007 = (logging.INFO, 'Successfully sent message to sqs')
    DEEPPING0008 = (logging.ERROR, 'Failed to send message to sqs')
    DEEPPING0009 = (logging.INFO, 'Connecting to S3')
    DEEPPING0010 = (logging.INFO, 'Successfully connected to S3')
    DEEPPING0011 = (logging.ERROR, 'Failed to connect to s3')
    DEEPPING0012 = (logging.INFO, 'Connecting to http url')
    DEEPPING0013 = (logging.INFO, 'Successfully connected to http url')
    DEEPPING0014 = (logging.ERROR, 'Failed to connect to http url')


def is_api_deep_ping(event):
    request_id = event.get('headers', {}).get('request_id', '')
    return request_id.lower().startswith(DEEP_PING_PREFIX)


def is_sqs_deep_ping(event):
    records = event.get('Records')
    if not records:
        return False
    body = records[0].get('body')
    if not body:
        return False
    return json.loads(body) == DEEP_PING_MESSAGE_BODY


def is_direct_invocation(event):
    return event.get('message') == DEEP_PING_MESSAGE


def is_deep_ping(event):
    env = get_environment_variables()
    if env['DISABLE_DEEP_PING']:
        log({'log_reference': LogReference.DEEPPING0001})
        return False
    return is_api_deep_ping(event) or is_sqs_deep_ping(event) or is_direct_invocation(event)


def get_deep_ping_env_var_with_prefix(prefix):
    return [os.getenv(x) for x in os.environ if x.startswith(f'{prefix}_DEEP_PING')]


def get_deep_ping_dependencies_from_environment():
    return {
        'sqs': get_deep_ping_env_var_with_prefix('SQS'),
        'dynamo': get_deep_ping_env_var_with_prefix('DYNAMO'),
        's3': get_deep_ping_env_var_with_prefix('S3'),
        'http': get_deep_ping_env_var_with_prefix('HTTP'),
    }


def get_environment_variables():
    return {
        'DISABLE_DEEP_PING': os.getenv('DISABLE_DEEP_PING', 'FALSE') == 'TRUE',
        'ENVIRONMENT_NAME': os.getenv('ENVIRONMENT_NAME'),
    }


def handle_deep_ping():
    dependencies = get_deep_ping_dependencies_from_environment()
    log({'log_reference': LogReference.DEEPPING0002, 'dependencies': dependencies})

    sqs = boto3.resource('sqs')
    s3 = boto3.client('s3')

    failure_count = 0

    for table in dependencies['dynamo']:
        if not connect_to_dynamodb(table):
            failure_count += 1
    for queue in dependencies['sqs']:
        if not send_test_message_to_sqs(sqs, queue):
            failure_count += 1
    for bucket in dependencies['s3']:
        if not test_s3_bucket(s3, bucket):
            failure_count += 1
    for url in dependencies['http']:
        if not test_http_url(url):
            failure_count += 1

    if failure_count > 0:
        return json_return_message(400, 'ERROR')

    return json_return_message(200, 'OK')


def connect_to_dynamodb(table_name):
    send_total()
    try:
        log({'log_reference': LogReference.DEEPPING0003, 'table': table_name})
        check_dynamodb_table(table_name)
        log({'log_reference': LogReference.DEEPPING0004, 'table': table_name})
        send_success()
        return True
    except Exception:
        send_failure()
        log({'log_reference': LogReference.DEEPPING0005, 'table': table_name, 'add_exception_info': True})
        return False


def send_test_message_to_sqs(sqs, queue_url):
    queue_name = queue_url.split('/')[-1]
    send_total()
    try:
        log({'log_reference': LogReference.DEEPPING0006, 'queue': queue_name})
        queue = sqs.Queue(queue_url)
        queue.send_message(MessageBody=json.dumps(DEEP_PING_MESSAGE_BODY))
        log({'log_reference': LogReference.DEEPPING0007, 'queue': queue_name})
        send_success()
        return True
    except Exception:
        send_failure()
        log({'log_reference': LogReference.DEEPPING0008, 'queue': queue_name, 'add_exception_info': True})
        return False


def test_s3_bucket(s3, bucket_name):
    send_total()
    try:
        log({'log_reference': LogReference.DEEPPING0009, 'bucket': bucket_name})
        s3.list_objects_v2(Bucket=bucket_name)
        log({'log_reference': LogReference.DEEPPING0010, 'bucket': bucket_name})
        send_success()
        return True
    except Exception:
        send_failure()
        log({'log_reference': LogReference.DEEPPING0011, 'bucket': bucket_name, 'add_exception_info': True})
        return False


def test_http_url(url):
    send_total()
    try:
        log({'log_reference': LogReference.DEEPPING0012, 'url': url})
        response = requests.get(url, timeout=5)
        response.raise_for_status()
        send_success()
        log({'log_reference': LogReference.DEEPPING0013, 'url': url})
        return True
    except Exception:
        send_failure()
        log({'log_reference': LogReference.DEEPPING0014, 'url': url, 'add_exception_info': True})
        return False


def send_success():
    send_metric('SuccessCount')


def send_failure():
    send_metric('FailureCount')


def send_total():
    send_metric('TotalCount')


def send_metric(metric_name):
    cloudwatch = boto3.client('cloudwatch')
    env = get_environment_variables()
    environment = env['ENVIRONMENT_NAME']
    cloudwatch.put_metric_data(
        Namespace=f'{environment}/deep-ping',
        MetricData=[
            {
                'MetricName': metric_name,
                'Timestamp': datetime.now(timezone.utc),
                'Value': 1,
                'Unit': 'Count',
                'StorageResolution': 1
            },
        ]
    )
