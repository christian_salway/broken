from re import (split, compile)

from common.utils.address_utils import destructure_postcode_from_address_list
from common.utils.edifact_dictionaries import (
    CYTOLOGY_RESULTS,
    ACTIONS,
    INFECTION_CODES
)
from common.utils.edifact_exceptions import (
    InvalidEdifactHeader
)
from common.utils.postcode_utils import (
    is_completed_postcode
)

from common.models.result import SELF_SAMPLE_SENDER_CODE


DEFAULT_DELIMITERS = {
    'delimiter_top': "'",
    'delimiter_middle': '+',
    'delimiter_bottom': ':'
}

TYPE_3_CODE = ['CYTFH', '0', '2', 'FH']
TYPE_9_CODE = ['FHSPRV', '0', '2', 'FH', 'FHS003']

UNKNOWN_CODE_VALUE = 'Unknown'
TYPE_3_DATE_OF_BIRTH_DATE_FORMAT = '%d%m%Y'


def tokenize_edifact(raw_edifact):
    """
    Takes a raw EDIFACT string and converts it into a multi-dimensional array,
    split by segments (delimited by '), elements (delimited by +) and components (delimited by :)
    according to the EDIFACT specification.
    """
    delimiters = _get_delimiters(raw_edifact)
    escape_character = delimiters.get('escape_character')
    segments = [x for x in _escaped_split(raw_edifact, delimiters['delimiter_top'], escape_character) if x]
    elements = [_escaped_split(segment, delimiters['delimiter_middle'], escape_character) for segment in segments]
    return [_split_out_element(element, delimiters['delimiter_bottom'], escape_character) for element in elements]


def get_una_header(raw_result):
    return raw_result[:9] if raw_result.startswith('UNA') and len(raw_result) >= 9 else ''


def _get_delimiters(raw_result):
    header = get_una_header(raw_result)
    if header:
        delimiters = {}
        delimiters['delimiter_bottom'] = header[3]
        delimiters['delimiter_middle'] = header[4]
        delimiters['escape_character'] = header[6]
        delimiters['delimiter_top'] = header[8]
        return delimiters
    else:
        return DEFAULT_DELIMITERS


def _split_out_element(element, delimiter, escape_character):
    return [[_remove_escapes(x, escape_character) for x in _escaped_split(component, delimiter, escape_character)]
            if delimiter in component
            else _remove_escapes(component, escape_character)
            for component in element]


def _escaped_split(raw_input, delimiter, escape_char):
    # emulates behaviour of native string split, but with optional escapes
    results = []
    escapes = segment_start = 0
    for idx in range(len(raw_input)):
        if (raw_input[idx] == delimiter):
            if escapes in [0, 2]:
                results.append(raw_input[segment_start:idx])
                segment_start = idx + 1
            escapes = 0
        elif (raw_input[idx] == escape_char):
            escapes = escapes + 1 if escapes < 2 else 1
        else:
            escapes = 0
    results.append(raw_input[segment_start:] if segment_start < len(raw_input) else '')
    return results


def _regex_escape(character):
    regex_chars = "'.,+*?^$()[]|\\{}"
    return '\\' if regex_chars.find(character) >= 0 else ''


def _remove_escapes(item, escape_character):
    if (escape_character):
        escape_exp = f"{_regex_escape(escape_character)}{escape_character}"
        regex1 = f"([^{escape_exp}]){escape_exp}([^{escape_exp}])"
        regex2 = f"([{escape_exp}])([{escape_exp}])"
        return compile(regex2).sub(r"\1", compile(regex1).sub(r"\1\2", item))
    return item


def split_raw_edifact_into_raw_lab_results(full_message):
    """
    Takes a raw EDIFACT string, and returns a subset of the EDIFACT split into individual
    lab results, still in the raw EDIFACT format.
    """
    message_type = determine_message_type(full_message)
    if message_type == 3:
        return message_type, split_by_head_generic(full_message, 'PAD')
    elif message_type == 9:
        return message_type, split_by_head_generic(full_message, 'S01')
    else:
        raise InvalidEdifactHeader


def split_by_head_generic(full_message, head):
    delimiters = _get_delimiters(full_message)
    delimiter_top = delimiters['delimiter_top']
    split_base = split(f"{delimiter_top}{head}|{delimiter_top}UNT", full_message)
    header = split_base.pop(0)
    split_base.pop(-1)
    results = [header] + [head+result for result in split_base]
    return results


def determine_message_type(raw_message):
    delimiters = _get_delimiters(raw_message)
    delimiter_top = delimiters['delimiter_top']
    heading = raw_message.split(f'{delimiter_top}BGM', 1)[0]
    UNH_ROW = [row for row in tokenize_edifact(heading) if row[0] == 'UNH'][0]
    message_type = UNH_ROW[2]
    if message_type == TYPE_9_CODE:
        return 9
    elif message_type == TYPE_3_CODE:
        return 3
    else:
        return -1


def decode_lab_result(lab_result):
    """
    Takes a decoded EDIFACT segment representing a lab result and converts it to a
    dictionary representing the lab result's contents
    """
    row_identifiers = {
        'UNB': _decode_nhais_cipher_and_sending_lab,
        'PAD': _decode_patient_details,
        'NHS': _decode_health_body,
        'DTM': _decode_date_time_period,
        'RAR': _decode_result_recall_details,
        'PCD': _decode_patient_cytology_details,
        'NAD': _decode_name_and_address,

        'S01': _no_action,
        'S02': _no_action,
        'PNA': _decode_person_name,
        'S03': _no_action,
        'TST': _decode_test_result,
        'HEA': _decode_health_information,
        'QTY': _decode_quantity_row,
    }

    decoded_lab_result = {
        'self_sample': False,
        'hpv_primary': False
    }

    for row in lab_result:
        segment_name = row[0]
        if isinstance(segment_name, str):

            interpret = row_identifiers.get(row[0], _no_action)
            new_info = interpret(row)
            decoded_lab_result.update(new_info)

    return decoded_lab_result


def _no_action(row):
    return {}


def _decode_nhais_cipher_and_sending_lab(UNB_row):
    fields = {}
    fields['raw_nhais_cipher'] = UNB_row[3]
    fields['sending_lab'] = UNB_row[2]
    return fields


def _decode_patient_details(PAD_row):
    fields = {}
    fields['nhs_number'] = PAD_row[1][0]
    fields['last_name'] = PAD_row[4]
    fields['previous_last_name'] = PAD_row[5]
    if(PAD_row[6]):
        fields['first_name'] = ' '.join(filter(None, PAD_row[6]))
    if(PAD_row[7]):
        fields['previous_first_name'] = ' '.join(filter(None, PAD_row[7]))
    fields['title'] = PAD_row[8]
    if(PAD_row[9]):
        fields['sex'] = PAD_row[9][0]
        fields['ethnic_origin'] = PAD_row[9][1]
        fields['height'] = PAD_row[9][2]
        fields['weight'] = PAD_row[9][3]
    fields['date_of_birth'] = PAD_row[10][1]
    fields['date_of_birth_format'] = TYPE_3_DATE_OF_BIRTH_DATE_FORMAT
    return fields


def _decode_date_time_period(DTM_row):
    data = DTM_row[1]

    date_types = {
        '119': 'test_date',
        '832': 'test_date',
        '329': 'date_of_birth',
    }

    date_formats = {
        '951': '%d%m%Y',
        '102': '%Y%m%d',
    }

    date_key_name = date_types.get(data[0])
    date_value = data[1]
    date_format = date_formats.get(data[2])

    return {
        date_key_name: date_value,
        f'{date_key_name}_format': date_format
    }


def _decode_health_body(NHS_row):
    code = NHS_row[1][0]
    identifier = NHS_row[1][1]
    # source_code should be renamed to sender_source_type e.g. 1,2 to avoid confusion
    # we can then map this to a source_code/source_character e.g. G,H
    # see "Lab entered results" / "Manually entered results" in the functional design
    identifiers = {
        '837': 'cytology_lab_code',
        '838': 'sender_source_type',
        '839': 'sender_code'
    }
    update = {
        identifiers[identifier]: code
    }

    if identifier == '838':
        source_code = 'H'
        if code == '1' or code == '000001':
            source_code = 'G'
        update['source_code'] = source_code
    return update


def _decode_result_recall_details(RAR_row):
    test_result = RAR_row[1]
    recall_status = RAR_row[2]
    time_period = RAR_row[3]

    update = {
        'result_code': test_result[0],
        'result': CYTOLOGY_RESULTS.get(test_result[0], UNKNOWN_CODE_VALUE),
        'action_code': recall_status[0],
        'action': ACTIONS.get(recall_status[0], UNKNOWN_CODE_VALUE),
        'recall_months': time_period[1],
    }
    return update


def _decode_patient_cytology_details(PCD_row):
    update = {}

    for field in [x for x in PCD_row if isinstance(x, list)]:
        if(field[0] == '840'):
            update['slide_number'] = field[1]
        if(field[1] == '842'):
            update['infection_code'] = field[0]
            update['infection_result'] = INFECTION_CODES.get(field[0], UNKNOWN_CODE_VALUE)
    if PCD_row[-1] == 'S':
        update['sender_code'] = SELF_SAMPLE_SENDER_CODE
        update['source_code'] = 'H'
        update['sender_source_type'] = '6'
        update['self_sample'] = True
        update['hpv_primary'] = True
    if PCD_row[-1] == 'Y':
        update['hpv_primary'] = True

    return update


def _decode_name_and_address(NAD_row):
    update_types = {
        'HP': decode_variable_name_address,
        'PAT': decode_variable_name_address,
        'SRC': lambda row: {'sender_source_type': row[2][0]},
        'SND': lambda row: {'sender_code': row[2][0]},
        'PTH': lambda row: {'pathology_lab': row[2][0]},
        'FHS': lambda row: {'HA(FHSA)': row[2][0]},
        'GP': lambda row: {'GP': row[2][0]}
    }

    decode_NAD_subtype = update_types[NAD_row[1]]

    decoded_row = decode_NAD_subtype(NAD_row)

    if NAD_row[1] == 'SRC':
        source_code = 'H'
        if decoded_row['sender_source_type'] == '1' or decoded_row['sender_source_type'] == '000001':
            source_code = 'G'
        decoded_row['source_code'] = source_code

    return decoded_row


def decode_variable_name_address(row):
    component = row[3]
    if isinstance(component, list):
        return {'address': ''.join(component)}
    else:
        return {'address': component}


def _decode_person_name(PNA_row):
    update = {}
    if PNA_row[1] == 'PAT':
        first_name = 'first_name'
        last_name = 'last_name'
        nhs_number = 'nhs_number'
    if PNA_row[1] == 'PER':
        first_name = 'previous_first_name'
        last_name = 'previous_last_name'
        nhs_number = 'previous_nhs_number'

    for field in [x for x in PNA_row if isinstance(x, list)]:
        if field[1] == 'OPI':
            update[nhs_number] = field[0]
        if field[0] == 'SU':
            update[last_name] = field[1]
        if field[0] == 'FS':
            update[first_name] = field[1]

    return update


def _decode_test_result(TST_row):
    test_result = TST_row[2][0]
    slide_number = TST_row[3][1]
    action_code = TST_row[4][0]
    update = {
        'slide_number': slide_number,
        'action_code': action_code,
        'action': ACTIONS.get(action_code, UNKNOWN_CODE_VALUE),
        'result_code': test_result,
        'result': CYTOLOGY_RESULTS.get(test_result, UNKNOWN_CODE_VALUE)
    }
    return update


def _decode_health_information(HEA_row):
    if HEA_row[1] == 'INM':
        return {
            'infection_code': HEA_row[2][0],
            'infection_result': INFECTION_CODES.get(HEA_row[2][0], UNKNOWN_CODE_VALUE)
        }
    elif HEA_row[1] == 'ETP':
        return {
            'hpv_primary': HEA_row[2][0] == 'Y'
        }
    else:
        return {}


def _decode_quantity_row(QTY_row):
    update = {}
    for field in [x for x in QTY_row if isinstance(x, list)]:
        if field[0] == '961':
            update['recall_months'] = field[1]
    return update


def validate_edifact_postcode(decoded_result, validation_errors):
    address_list = decoded_result['address'].split(' ')

    sanitised_postcode, _ = destructure_postcode_from_address_list(address_list)

    if sanitised_postcode and is_completed_postcode(sanitised_postcode):
        decoded_result['sanitised_postcode'] = sanitised_postcode
    else:
        validation_errors.append('Postcode is missing or invalid')
