from boto3.dynamodb.conditions import Key

from common.utils.dynamodb_helper_utils import build_update_query
from common.utils.dynamodb_access.operations import dynamodb_put_item, dynamodb_query, dynamodb_update_item
from common.utils.dynamodb_access.table_names import TableNames


def create_or_replace_rejected_record(record):
    return dynamodb_put_item(TableNames.REJECTED, record)


def update_rejected_record(key, attributes_to_update):
    update_expression, expression_names, expression_values = build_update_query(attributes_to_update)
    return dynamodb_update_item(
        TableNames.REJECTED,
        dict(
            Key=key,
            UpdateExpression=update_expression,
            ExpressionAttributeValues=expression_values,
            ExpressionAttributeNames=expression_names
        )
    )


def query_rejected_records_by_status(status, rejected_type):
    condition_expression = Key('rejected_status').eq(status) & Key('rejection_type').begins_with(f'{rejected_type}#')
    return dynamodb_query(
        TableNames.REJECTED,
        dict(
            IndexName='rejected-status',
            KeyConditionExpression=condition_expression
        )
    )


def get_rejection_reason(rejected_record):
    if rejected_record.get('rejection_type', '').startswith('PDS'):
        failed_rules = rejected_record.get('metadata', {}).get('failed_rules', [])
        if failed_rules:
            return 'Failed PDS validation rules: ' + ', '.join(failed_rules)
        else:
            return None
