import decimal
import json
from functools import wraps
from boto3.dynamodb.types import TypeSerializer
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.dynamodb_access.table_names import get_dynamodb_table_name


def build_update_query(update_attributes={}, delete_attributes=[]):
    record_keys = []
    expression_names = {}
    expression_values = {}
    delete_keys = []
    for key, value in update_attributes.items():
        record_keys.append(f'#{key} = :{key}')
        expression_values[f':{key}'] = value
        expression_names[f'#{key}'] = key
    for key in delete_attributes:
        delete_keys.append(f'#{key}')
        expression_names[f'#{key}'] = key

    clauses = []
    if update_attributes:
        clauses.append('SET ' + ', '.join(record_keys))
    if delete_attributes:
        clauses.append('REMOVE ' + ', '.join(delete_keys))
    update_expression = ' '.join(clauses)

    return update_expression, expression_names, expression_values


def build_projection_expression(projection_expression):
    expression_keys = []
    expression_values = {}
    for value in projection_expression:
        expression_keys.append(f'#{value}')
        expression_values[f'#{value}'] = value

    return {
        'projection_expression': ', '.join(expression_keys),
        'expression_attribute_names': expression_values
    }


def build_filter_expression(filter_expression_list):
    filter_expression = filter_expression_list[0]

    for index in range(1, len(filter_expression_list)):
        filter_expression = filter_expression & filter_expression_list[index]

    return filter_expression


def paginate(query_operation):
    @wraps(query_operation)
    def wrapper(*args, **kwargs):
        done = False
        start_key = None
        while not done:
            if start_key:
                args[0]['ExclusiveStartKey'] = start_key
            response = query_operation(*args, **kwargs)
            start_key = response.get('LastEvaluatedKey', None)
            done = start_key is None
    return wrapper


def serialize_item(item):
    serializer = TypeSerializer()
    return {k: serializer.serialize(v) for k, v in item.items()}


def serialize_update(update):
    return {
        'record_keys': serialize_item(update['record_keys']),
        'update_values': serialize_item(update['update_values']),
        'delete_values': update.get('delete_values', []),
    }


def build_update_expression(record):
    set_fields = 'SET ' + ', '.join(
        [f'#{key} = :{key}' for key, value in record['update_values'].items()]
    )
    remove_fields = ((' REMOVE ' + ', '.join([f'#{key}' for key in record['delete_values']]))
                     if record.get('delete_values') else '')
    return f'{set_fields}{remove_fields}'


def build_update_record(update_item, table_enum: TableNames):
    """
    Create an Update for a transaction to use with Boto3 `transact_write_items`
    """
    return {
        'Update': {
            'TableName': get_dynamodb_table_name(table_enum),
            'Key': update_item['record_keys'],
            'UpdateExpression': build_update_expression(update_item),
            'ExpressionAttributeValues': {
                f':{key}': value for key, value in update_item['update_values'].items()
            },
            'ExpressionAttributeNames': {
                f'#{key}': key for key in
                [*update_item['update_values'].keys(), *update_item['delete_values']]
            },
        }
    }


def build_put_record(item, table_enum: TableNames):
    return {
        'Put': {
            'TableName': get_dynamodb_table_name(table_enum),
            'Item': item,
        }
    }


def build_delete_record(item, table_enum: TableNames):
    return {
        'Delete': {
            'TableName': get_dynamodb_table_name(table_enum),
            'Key': item,
        }
    }


class DecimalEncoder(json.JSONEncoder):
    """
    The Boto SDK uses the Decimal class to hold Amazon DynamoDB number values.
    The default method converts top level decimals in DynamoDB items to floats or ints.
    This allows a dictionary containing decimals to be encoded as json.
    """

    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)
