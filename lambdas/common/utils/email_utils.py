import os
from jinja2 import Environment, select_autoescape, FunctionLoader
from common.utils.s3_utils import get_file_contents_from_bucket

EMAIL_TEMPLATES = {
    'prior_notification_email': 'prior_notification_email_template.html',
    'non_responder_email': 'non_responder_email_template.html'
}
BUCKET = os.environ.get('EMAIL_TEMPLATES_BUCKET')


def load_templates_from_s3(template):
    return get_file_contents_from_bucket(BUCKET, template)


def build_message_body(email_type, input_vars):
    JINJA_ENV = Environment(
        loader=FunctionLoader(load_templates_from_s3),
        autoescape=select_autoescape(['html', 'xml'])
    )

    j2_template = JINJA_ENV.get_template(EMAIL_TEMPLATES[email_type])
    return j2_template.render(data=input_vars)
