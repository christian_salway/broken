import json
from common.utils.session_utils import get_session_from_lambda_event


def get_api_request_info_from_event(event):
    body = event.get('body') if event.get('body') else "{}"
    path_parameters = event.get('pathParameters') if event.get('pathParameters') else {}
    query_parameters = event.get('queryStringParameters') if event.get('queryStringParameters') else {}

    return (
        {
            'path_parameters': path_parameters,
            'query_parameters': query_parameters,
            'body': json.loads(body),
            'session': get_session_from_lambda_event(event)
        },
        event.get('httpMethod'),
        event.get('resource')
    )


def format_query_string(params):
    return "&".join([f"{k}={v}" for k, v in params.items()])
