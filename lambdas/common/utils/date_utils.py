from datetime import datetime


def is_valid_date_format(date):
    date_formats = [
        '%Y%m%d',
        '%Y%m',
        '%Y'
    ]
    for date_format in date_formats:
        try:
            datetime.strptime(date, date_format)
        except ValueError:
            continue
        else:
            return True
    return False


def pad_date_string(date):
    pad_amount = 8 - len(date)
    return date + ''.join('01' for _ in range(0, pad_amount, 2))
