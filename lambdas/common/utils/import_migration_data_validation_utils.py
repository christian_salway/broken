import re
from datetime import datetime, timezone
from common.utils import exit_if_null
from common.utils.data_segregation.nhais_ciphers import VALID_MIGRATION_CIPHER_CODES


NOW_DATE = datetime.now(timezone.utc)
VALID_RECALL_TYPES = ['N', 'R', 'I', 'L', 'S', 'C']
VALID_RECALL_STATUSES = ['1', 'G', 'N', 'P', '2', 'F', 'H', 'L', 'S', 'C', 'Z', '-']
VALID_POS_DOUBT_VALUES = ['Y', 'N']
VALID_LDN_VALUES = ['L1', 'L2', 'L3', 'L4', 'D1', 'D2']
VALID_REASON_VALUES = ['1', '2', '3', '4', '5', '10', '11', '6', '7', '8', '9', '77', '99', '9C']
VALID_CEASE_CODES = ['C', 'D', 'F', 'N', 'Y', 'G', 'H']
VALID_TEST_RESULT_CODES = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'B', 'E', 'M', 'N', 'G', 'X']
VALID_ACTION_CODES = ['R', 'A', 'S', 'H']
VALID_INFECTION_CODES = ['0', '9', 'U', 'Q', '1', '2', '3', '4', '5', '6', '7']
VALID_SOURCE_CODES = ['G', 'H', 'X', 'N']
VALID_GENDERS = ['M', 'F', 'I']
VALID_TITLES = ['MRS', 'MISS', 'MS', 'DR', 'PROF', 'REV', 'LADY', 'DAME', 'MR', 'SIR', 'CAPT', 'COL', 'CMDR', 'LORD']
VALID_HPV_VALUES = ['Y', 'N']

DATA_ERROR_KEY = 'data_error'


class ImportMigrationDataValidation():
    """
    The validation rules below are for CSO1C subject records
    NHAIS data migration import validation
    """
    @exit_if_null
    def validate_fnr_count(self, fnr_count):
        if not 0 <= int(fnr_count) <= 20:
            return {DATA_ERROR_KEY: 'FNR Count must be between 0 and 20 (inclusive)'}

    @exit_if_null
    def validate_text(self, text):
        validation_pattern = r'^[\w\s!"£$%^&*()_\-=+#~\'@;:\/?.>,<\\\|`¬\[\]]*$'
        if not re.match(validation_pattern, text):
            return {DATA_ERROR_KEY: 'Note text can only contains letters, numbers and punctuation'}

    @exit_if_null
    def validate_recall_type(self, recall_type):
        if recall_type not in VALID_RECALL_TYPES:
            return {DATA_ERROR_KEY: f'Recall Type not one of the expected values: {VALID_RECALL_TYPES}'}

    @exit_if_null
    def validate_recall_stat(self, recall_stat):
        if recall_stat not in VALID_RECALL_STATUSES:
            return {DATA_ERROR_KEY: f'Recall Status not one of the expected values: {VALID_RECALL_STATUSES}'}

    @exit_if_null
    def validate_pos_doubt(self, pos_doubt):
        if pos_doubt not in VALID_POS_DOUBT_VALUES:
            return {DATA_ERROR_KEY: f'Pos Doubt not one of the expected values: {VALID_POS_DOUBT_VALUES}'}

    @exit_if_null
    def validate_pnl_fnr(self, pnl_fnr):
        if pnl_fnr and pnl_fnr != 'UA':
            return self._validate_date(pnl_fnr, 0)

    """
    The validation rules below are for CSO1R subject records
    NHAIS data migration import validation
    """

    @exit_if_null
    def validate_dob(self, dob):
        return self._validate_date(dob, 0)

    @exit_if_null
    def validate_gender(self, gender):
        if gender not in VALID_GENDERS:
            return {DATA_ERROR_KEY: f'Gender not one of the expected values: {VALID_GENDERS}'}

    @exit_if_null
    def validate_oapd(self, oapd):
        return self._validate_date(oapd, 0, 1948)

    @exit_if_null
    def validate_ldn(self, ldn):
        if ldn not in VALID_LDN_VALUES:
            return {DATA_ERROR_KEY: f'LDN not one of the expected values: {VALID_LDN_VALUES}'}

    @exit_if_null
    def validate_gp_or_practice_code(self, gp_or_practice_code):
        validation_pattern = r'(^[^_\W]*)$'
        if not re.match(validation_pattern, gp_or_practice_code):
            return {DATA_ERROR_KEY: 'GP code and GP practice code can only contains letters and numbers'}

    @exit_if_null
    def validate_remdate(self, remdate):
        return self._validate_date(remdate, 0, 1988)

    @exit_if_null
    def validate_destination(self, destination):
        validation_pattern = r'^[A-Z]+$'
        if not re.match(validation_pattern, destination):
            return {DATA_ERROR_KEY: 'Destination must contain uppercase letters only'}

    @exit_if_null
    def validate_title(self, title):
        if title.upper() not in VALID_TITLES:
            return {DATA_ERROR_KEY: f'Title not one of the expected values: {VALID_TITLES}'}

    """
    The validation rules below are for 2 screening test
    NHAIS data migration import validation
    """

    @exit_if_null
    def validate_test_result_code(self, test_result_code):
        if test_result_code not in VALID_TEST_RESULT_CODES:
            return {DATA_ERROR_KEY: f'Test result code not one of the expected values: {VALID_TEST_RESULT_CODES}'}

    @exit_if_null
    def validate_infection_code(self, infection_code):
        if infection_code not in VALID_INFECTION_CODES:
            return {DATA_ERROR_KEY: f'Infection code not one of the expected values: {VALID_INFECTION_CODES}'}

    @exit_if_null
    def validate_repeat_months(self, repeat_months):
        if repeat_months == '--':
            return None
        validation_pattern = '^[0-9]*$'
        if not re.match(validation_pattern, repeat_months):
            return {DATA_ERROR_KEY: 'Repeat months can only contain numbers or be --'}
        if not self._in_bounds(1, 60, int(repeat_months)):
            return {DATA_ERROR_KEY: 'Repeat months must be between 1 and 60 months'}

    @exit_if_null
    def validate_lab_national_code(self, lab_code):
        validation_pattern = '^[0-9]*$'
        if not re.match(validation_pattern, lab_code):
            return {DATA_ERROR_KEY: 'Lab national code can only contain numbers through 0 - 9'}

    def validate_sender_exists(self, sender_exists):
        if sender_exists not in ['Y', '']:
            return {DATA_ERROR_KEY: 'Sender exists must be empty or Y'}

    @exit_if_null
    def validate_national_sender_code(self, national_sender_code):
        validation_pattern = '^[0-9]*$'
        if not re.match(validation_pattern, national_sender_code):
            return {DATA_ERROR_KEY: 'National Sender code can only contain numbers through 0 - 9'}

    @exit_if_null
    def validate_date_with_days_after_1970(self, date):
        clean_date = date.replace('-', '')
        if clean_date:
            return self._validate_date(date, 0, min_year=1970, date_format='%Y%m%d')

    @exit_if_null
    def validate_date_without_days_after_1970(self, date):
        clean_date = date.replace('-', '')
        if clean_date:
            return self._validate_date(date, 0, min_year=1970, date_format='%Y%m')

    @exit_if_null
    def validate_slide_number(self, slide_number):
        validation_pattern = '^[0-9]*$'
        if not re.match(validation_pattern, slide_number):
            return {DATA_ERROR_KEY: 'Slide number can only contain numbers through 0 - 9'}

    @exit_if_null
    def validate_action_code(self, action_code):
        if action_code not in VALID_ACTION_CODES:
            return {DATA_ERROR_KEY: f'Action code not one of the expected values: {VALID_ACTION_CODES}'}

    @exit_if_null
    def validate_source_code(self, source_code):
        if source_code not in VALID_SOURCE_CODES:
            return {DATA_ERROR_KEY: f'Source code not one of the expected values: {VALID_SOURCE_CODES}'}

    @exit_if_null
    def validate_hpv_value(self, hpv_value):
        if hpv_value not in VALID_HPV_VALUES:
            return {DATA_ERROR_KEY: f'HPV not one of the expected values: {VALID_HPV_VALUES}'}

    """
    The validation rules below are for 3 cease and defer
    NHAIS data migration import validation
    """
    @exit_if_null
    def validate_amend_flag(self, amend_flag):
        if amend_flag != 'AMEND':
            return {DATA_ERROR_KEY: 'Amend flag value must be set to AMEND'}

    @exit_if_null
    def validate_sequence_number(self, seq_number):
        validation_pattern = '^[1-9]*$'
        if not re.match(validation_pattern, seq_number):
            return {DATA_ERROR_KEY: 'Sequence number can only contain numbers through 1 - 9'}

    @exit_if_null
    def validate_reason(self, reason):
        if reason.replace('*', '') not in VALID_REASON_VALUES:
            return {DATA_ERROR_KEY: f'Reason not one of the expected values: {VALID_REASON_VALUES}'}

    @exit_if_null
    def validate_next_test_due_date(self, next_test_due_date):
        return self._validate_date(next_test_due_date, 5, 1988)

    @exit_if_null
    def validate_cease_code(self, cease_code):
        if cease_code not in VALID_CEASE_CODES:
            return {DATA_ERROR_KEY: f'Cease code not one of the expected values: {VALID_CEASE_CODES}'}

    @exit_if_null
    def validate_source_cipher(self, cipher_code):
        if cipher_code not in VALID_MIGRATION_CIPHER_CODES:
            return {DATA_ERROR_KEY: 'Cipher code not one of the expected values'}

    def validate_amend_date_data(self, amend_date):
        return self._validate_date(amend_date, 0, 1970)

    """
    The validation rules below are for common functions
    shared throughout all validation rules
    """

    def _in_bounds(self, min_bound, max_bound, data_length):
        if not min_bound <= data_length <= max_bound:
            return False
        return True

    def _validate_date(self, date_input, years_to_add, min_year=1880, date_format='%Y%m%d'):
        try:
            converted_date = datetime.strptime(date_input, date_format).date()
            max_date = self._get_max_date(years_to_add)
            min_date = self._get_min_date(min_year)
            if not (converted_date >= min_date and converted_date <= max_date):
                return {DATA_ERROR_KEY: f'Date must be between {min_date} and {max_date}'}
        except ValueError:
            return {DATA_ERROR_KEY: f'Invalid date format. Expected format to be {date_format}'}

    def _get_min_date(self, year=1900):
        return datetime(year, 1, 1).date()

    def _get_max_date(self, years_to_add=0):
        try:
            return datetime(NOW_DATE.year + years_to_add, NOW_DATE.month, NOW_DATE.day).date()
        except Exception as ex:
            if 'day is out of range for month' in str(ex):
                return datetime(NOW_DATE.year + years_to_add, NOW_DATE.month + 1, 1).date()

    def validate_optional_date(self, date):
        if date:
            return self._validate_date(date, 0)
