from boto3.dynamodb.conditions import Key
from datetime import datetime, timezone
from dateutil.relativedelta import relativedelta
from functools import wraps

from common.utils.dynamodb_access.operations import dynamodb_query, dynamodb_update_item
from common.log import log
from common.log_references import CommonLogReference
from common.utils.dynamodb_helper_utils import build_update_query
from common.models.result import ResultWorkflowState, ResultSecondaryStatus
from common.utils.dynamodb_access.table_names import TableNames


PROJECTION_EXPRESSION = ', '.join([
    'user_id',
    'lock_timestamp',
    'received_time',
    'workflow_state',
    'workflow_history',
    'role_id',
    'user_name',
    'secondary_status'
])
LOCK_TIMEOUT_MINUTES = 10


class LockException(Exception):
    pass


def do_acquire_lock(function):

    @wraps(function)
    def decorator(*args):

        if not args:
            return function(args)

        event_data = args[0]
        lock_state, current_user, result_id = _check_event_data_for_lock_state(event_data)

        if not _is_lock_valid_for_user(lock_state, current_user):
            log({
                'log_reference': CommonLogReference.RESULTLOCK0006,
                'result_id': result_id,
                'user_id': current_user['id'],
                'lock_user': lock_state['user_id'],
                'lock_timestamp': lock_state['lock_timestamp']
            })
            raise LockException()

        _lock_result(result_id, lock_state['received_time'], current_user)
        return function(*args)

    return decorator


def release_lock(original_function=None, reset_workflow_state=False):

    def _release_lock_decorator(function):

        @wraps(function)
        def decorator(*args):
            if not args:
                return function(args)

            event_data = args[0]
            result = function(*args)
            do_release_lock(event_data, reset_workflow_state)

            return result

        return decorator

    if original_function:
        return _release_lock_decorator(original_function)

    return _release_lock_decorator


def do_release_lock(event_data, reset_workflow_history=True):
    lock_state, current_user, result_id = _check_event_data_for_lock_state(event_data)

    if not _is_lock_valid_for_user(lock_state, current_user):
        log({
            'log_reference': CommonLogReference.RESULTLOCK0012,
            'result_id': result_id,
            'user_id': current_user['id'],
            'lock_user': lock_state['user_id'],
            'lock_timestamp': lock_state['lock_timestamp']
        })
        raise LockException()

    _release_lock(result_id, lock_state, reset_workflow_history)


def _get_secondary_status(lock_state):
    workflow_state = lock_state['workflow_state']
    workflow_history = lock_state.get('workflow_history') or []
    secondary_status = lock_state.get('secondary_status')

    if workflow_state == ResultWorkflowState.PROCESS:
        for entry in reversed(workflow_history):
            if entry.get('action', '') == 'check':
                return ResultSecondaryStatus.CHECKER_REJECTED
        return ResultSecondaryStatus.NOT_STARTED

    if workflow_state == ResultWorkflowState.CHECK:
        return ResultSecondaryStatus.NOT_CHECKED

    if workflow_state == ResultWorkflowState.REJECTED:
        if secondary_status == ResultSecondaryStatus.ACTIONED:
            return ResultSecondaryStatus.ACTIONED

        for entry in reversed(workflow_history):
            if rejectionActioned(entry):
                return ResultSecondaryStatus.ACTIONED

        return ResultSecondaryStatus.NOT_ACTIONED


def rejectionActioned(entry):
    step = entry.get('step', '')
    if step != 'rejected':
        return False

    action = entry.get('action', '')
    return (action in ['reject', 'delete']
            or (action == 'save'
                and (entry.get('comment') is not None
                     or entry.get('crm_number') is not None)))


def _release_lock(result_id, lock_state, reset_workflow_history):
    log({'log_reference': CommonLogReference.RESULTLOCK0011})

    try:

        key_condition = {'result_id': result_id, 'received_time': lock_state['received_time']}
        delete_attributes = ['user_id', 'lock_timestamp', 'role_id']

        if reset_workflow_history:
            secondary_status = _get_secondary_status(lock_state)
            # If a rejected result is ACTIONED, we don't want to remove the user_name
            if secondary_status != ResultSecondaryStatus.ACTIONED:
                delete_attributes.append('user_name')

            update_attributes = {'secondary_status': secondary_status}

            update_expression, expression_names, expression_values = build_update_query(
                update_attributes,
                delete_attributes=delete_attributes
            )
            dynamodb_update_item(TableNames.RESULTS, dict(
                Key=key_condition,
                UpdateExpression=update_expression,
                ExpressionAttributeValues=expression_values,
                ExpressionAttributeNames=expression_names,
                ReturnValues='NONE'
            ))

        else:
            update_attributes = {}

            update_expression, expression_names, _ = build_update_query(
                update_attributes,
                delete_attributes=delete_attributes
            )
            dynamodb_update_item(TableNames.RESULTS, dict(
                Key=key_condition,
                UpdateExpression=update_expression,
                ExpressionAttributeNames=expression_names,
                ReturnValues='NONE'
            ))

    except Exception as e:
        log({'log_reference': CommonLogReference.RESULTLOCK0013})
        raise e

    log({'log_reference': CommonLogReference.RESULTLOCK0014})


def _check_event_data_for_lock_state(event_data):

    session = event_data.get('session')
    if not session or 'user_data' not in session:
        log({'log_reference': CommonLogReference.RESULTLOCK0001})
        raise LockException()

    current_user_data = session['user_data']
    current_user_role = session['selected_role']['role_id']
    current_user_id = current_user_data['nhsid_useruid']
    current_user_name = f"{current_user_data['first_name']} {current_user_data['last_name']}"
    if not current_user_id:
        log({'log_reference': CommonLogReference.RESULTLOCK0001})

    user = {
        'name': current_user_name,
        'id': current_user_id,
        'role': current_user_role
    }

    result_id = event_data['body'].get('result_id') or event_data['path_parameters'].get('result_id')
    return _get_current_result_lock_state(result_id), user, result_id


def _lock_result(result_id, received_time, user):
    log({'log_reference': CommonLogReference.RESULTLOCK0007})

    try:

        key_condition = {'result_id': result_id, 'received_time': received_time}
        update_attributes = {
            'user_id': user['id'],
            'lock_timestamp': datetime.now(timezone.utc).isoformat(),
            'role_id': user['role'],
            'user_name': user['name']
        }

        update_expression, expression_names, expression_values = build_update_query(update_attributes)
        dynamodb_update_item(TableNames.RESULTS, dict(
            Key=key_condition,
            UpdateExpression=update_expression,
            ExpressionAttributeValues=expression_values,
            ExpressionAttributeNames=expression_names,
            ReturnValues='NONE'
        ))

    except Exception as e:
        log({'log_reference': CommonLogReference.RESULTLOCK0008})
        raise e

    log({'log_reference': CommonLogReference.RESULTLOCK0010})


def _get_current_result_lock_state(result_id):

    if not result_id:
        log({'log_reference': CommonLogReference.RESULTLOCK0002})
        raise LockException()

    key_condition = Key('result_id').eq(result_id)
    results = dynamodb_query(TableNames.RESULTS, dict(
        KeyConditionExpression=key_condition,
        ProjectionExpression=PROJECTION_EXPRESSION,
    ))

    if not results:
        log({
            'log_reference': CommonLogReference.RESULTLOCK0004,
            'result_id': result_id
        })
        raise LockException()

    log({'log_reference': CommonLogReference.RESULTLOCK0003, 'result_id': result_id})
    first_result = results[0]
    return {
        'user_id': first_result.get('user_id'),
        'lock_timestamp': first_result.get('lock_timestamp'),
        'received_time': first_result.get('received_time'),
        'workflow_state': first_result.get('workflow_state'),
        'workflow_history': first_result.get('workflow_history')
    }


def _is_lock_valid_for_user(lock_state, user):

    current_lock_user = lock_state.get('user_id')
    if not current_lock_user or current_lock_user == user['id']:
        return True

    current_lock_timestamp = lock_state.get('lock_timestamp')
    if not current_lock_timestamp:
        log({'log_reference': CommonLogReference.RESULTLOCK0005})
        raise LockException()

    lock_end = (
        datetime.now(timezone.utc).fromisoformat(current_lock_timestamp)
        + relativedelta(minutes=LOCK_TIMEOUT_MINUTES)
    )
    return datetime.now(timezone.utc) > lock_end
