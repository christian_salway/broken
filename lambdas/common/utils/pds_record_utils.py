PDS_RECORD_KEYS_FOR_SIGNATURE = [
    'nhs_number',
    'first_name',
    'last_name',
    'title',
    'date_of_birth',
    'address',
    'pds_timestamp'
]


def get_pds_record_fields_for_signature(pds_record):
    return {key: pds_record[key] for key in PDS_RECORD_KEYS_FOR_SIGNATURE if key in pds_record}
