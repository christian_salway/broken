from datetime import datetime, timezone

from common.log import log
from common.log_references import CommonLogReference
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.s3_utils import get_object_from_bucket, update_object_metadata


def verify_and_parse_session(session):

    if not session or not session.get('user_data') or not session.get('access_groups'):
        log({'log_reference': CommonLogReference.DOWNLOADFILEEX0001})
        raise ProcessingException(400, CommonLogReference.DOWNLOADFILEEX0001.message)

    session_user_data = session['user_data']
    session_selected_access_groups = session['access_groups']

    log({'log_reference': CommonLogReference.DOWNLOADFILE0001})
    return session_user_data, session_selected_access_groups


def get_file_name(event):
    event_data, *_ = get_api_request_info_from_event(event)
    query_parameters = event_data.get('query_parameters', {})
    file_name = query_parameters.get('file_name')
    if not file_name:
        log({'log_reference': CommonLogReference.DOWNLOADFILEEX0006})
        raise ProcessingException(400, CommonLogReference.DOWNLOADFILEEX0006.message)

    log({'log_reference': CommonLogReference.DOWNLOADFILE0004, 'file_name': file_name})
    return file_name


def get_file_body(bucket_name, file_key_name):
    file_object = get_object_from_bucket(bucket_name, file_key_name)

    if not file_object:
        log({'log_reference': CommonLogReference.DOWNLOADFILEEX0003})
        raise ProcessingException(400, f'Object not found with name {file_key_name}')

    file_body = file_object['Body'].read().decode('utf-8')

    log({'log_reference': CommonLogReference.DOWNLOADFILE0002})
    return file_body


def update_metadata(session_user_data, file_key_name, bucket_name):
    new_metadata = {
        'downloaded_by': f"{session_user_data.get('first_name')} {session_user_data.get('last_name')}",
        'downloaded_date': datetime.now(timezone.utc).date().isoformat()
    }

    update_object_metadata(bucket_name, file_key_name, new_metadata)
    log({'log_reference': CommonLogReference.DOWNLOADFILE0003})


class ProcessingException(Exception):
    def __init__(self, response_code, message):
        self.response_code = response_code
        self.message = message
        super().__init__(self.message)
