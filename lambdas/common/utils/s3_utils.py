from boto3 import client

# Rather than accessing this global directly use the get_s3_client() function which aids in testability
_S3_CLIENT = None


def get_s3_client():
    global _S3_CLIENT
    if not _S3_CLIENT:
        _S3_CLIENT = client('s3', endpoint_url='https://s3.eu-west-2.amazonaws.com')
    return _S3_CLIENT


def get_object_from_bucket(bucket, object_key):
    return get_s3_client().get_object(Bucket=bucket, Key=object_key)


def get_object_meta_data(bucket, object_key):
    object_head_data = get_s3_client().head_object(Bucket=bucket, Key=object_key)
    return object_head_data['Metadata']


def put_object(bucket, object_body, object_key, meta_data={}):
    return get_s3_client().put_object(Bucket=bucket, Key=object_key,
                                      Metadata=meta_data, Body=object_body)


def move_object_to_bucket_and_transition_to_infrequent_access_class(
        from_bucket, target_bucket, object_key, destination=None):
    get_s3_client().copy(
        CopySource={'Bucket': from_bucket, 'Key': object_key},
        Bucket=target_bucket,
        Key=destination if destination else object_key,
        ExtraArgs={
            'StorageClass': 'STANDARD_IA'
        }
    )
    return delete_object_from_bucket(from_bucket, object_key)


def move_object_to_bucket(from_bucket, target_bucket, object_key):
    get_s3_client().copy(
        CopySource={'Bucket': from_bucket, 'Key': object_key},
        Bucket=target_bucket,
        Key=object_key,
    )
    return delete_object_from_bucket(from_bucket, object_key)


def read_bucket(bucket):
    return get_s3_client().list_objects_v2(Bucket=bucket)


def read_bucket_with_subfolders(bucket, subfolder):
    return get_s3_client().list_objects_v2(Bucket=bucket, Prefix=subfolder, Delimiter='/')


def delete_object_from_bucket(bucket, object_key):
    return get_s3_client().delete_object(Bucket=bucket, Key=object_key)


def get_file_contents_from_bucket(bucket, file_name):
    file_object = get_object_from_bucket(bucket, file_name)
    return {
        'body': file_object.get('Body').read().decode('utf-8'),
        'meta_data': file_object.get('Metadata', [])
    }


def update_object_metadata(bucket, file_name, metadata_to_add):
    object_head_data = get_s3_client().head_object(Bucket=bucket, Key=file_name)
    object_metadata = object_head_data["Metadata"]
    new_metadata = {**object_metadata, **metadata_to_add}
    get_s3_client().copy_object(
        Bucket=bucket,
        Key=file_name,
        CopySource=f'{bucket}/{file_name}',
        Metadata=new_metadata,
        MetadataDirective='REPLACE'
    )
