from datetime import date
from typing import Tuple, Union
from dateutil.parser import isoparse
from nose.tools.nontrivial import nottest
from common.utils.organisation_directory_utils import get_organisation_information_by_organisation_code
from common.utils.reference_utils import get_reference_by_cipher_and_key
from common.log import log
from common.log_references import CommonLogReference as LogReference

"""
Source types meaning:
    000001 / 1 = GP
    000002 / 2 = NHS community clinic
    000003 / 3 = GUM clinic
    000004 / 4 = NHS hospital
    000005 / 5 = Private
    000006 / 6 = Other
    000007 / 7 = NHS colposcopy
"""

ORG_CACHE_SOURCE_TYPE = ['000001', '1']

RESOURCE_SOURCE_TYPE = [
    '000002', '000003', '000004', '000005', '000006', '000007',
    '2', '3', '4', '5', '6', '7'
]

SENDER_NOT_FOUND = 'Sender not found'


def get_sender_details_and_validate(NHAIS_cipher, sender_code, sender_source_type, test_date=''):
    sender, is_ods_lookup = get_sender_details(NHAIS_cipher, sender_code, sender_source_type)
    result = validate_sender(sender, is_ods_lookup, NHAIS_cipher, sender_code, sender_source_type, test_date)
    return result


def get_sender_details(NHAIS_cipher, sender_code, sender_source_type):
    sender = None
    is_ods_lookup = False
    if sender_source_type in ORG_CACHE_SOURCE_TYPE:
        log({'log_reference': LogReference.SENDER0001})
        is_ods_lookup = True
        if sender_code:
            sender = get_organisation_information_by_organisation_code(sender_code)
    elif sender_source_type in RESOURCE_SOURCE_TYPE:
        log({'log_reference': LogReference.SENDER0002})
        key = f'{NHAIS_cipher}#Sender#{sender_code}'
        sender = get_reference_by_cipher_and_key(NHAIS_cipher, key)
    else:
        log({'log_reference': LogReference.SENDER0003, 'sender_source_type': sender_source_type})
        sender = None

    return (sender, is_ods_lookup)


def validate_sender(sender, is_ods_lookup, NHAIS_cipher, sender_code, sender_source_type, test_date):
    message = None
    valid = False

    if not sender:
        message = SENDER_NOT_FOUND

    if sender:
        valid, message = _is_sender_valid(sender, is_ods_lookup, sender_source_type, test_date)

    if valid and sender:
        return sender

    failure_response = {
        'message': 'Could not retrieve sender details with given parameters',
        'sender_details': {'NHAIS_cipher': NHAIS_cipher, 'sender_code': sender_code,
                           'sender_source_type': sender_source_type}
    }
    if message:
        failure_response.update({'failure_message': message})

    log({'log_reference': LogReference.SENDER0004, 'NHAIS_cipher': NHAIS_cipher,
        'sender_code': sender_code, 'sender_source_type': sender_source_type})

    return failure_response


def _is_sender_valid(sender, is_ods_lookup, source_type, test_date):
    message = sender.get('message')

    log({'log_reference': LogReference.VALSEND0001})

    # When doing an organisation lookup using ODS, if a message exists, it means the lookup failed
    # If no message exists at this point, we have sender details that can be validated below
    if message:
        return (False, SENDER_NOT_FOUND)

    # For ODS lookups, perform the specific checks for sender type 1
    if is_ods_lookup:
        valid, message = _validated_sender_type_1(sender, test_date)
        log({'log_reference': LogReference.VALSEND0005})
        return (valid, message)

    is_active = sender.get('active')
    inactive_from = sender.get('inactive_from')
    # If the active value is False and there's no inactive_from date, the sender is invalid
    if is_active is False and not inactive_from:
        log({'log_reference': LogReference.VALSEND0002})
        return (False, message)

    # If the active value is False and the inactive_from date was before the test_date, the sender is invalid
    if is_active is False and inactive_from and test_date > inactive_from:
        log({'log_reference': LogReference.VALSEND0003})
        message = 'Sender inactive at test date'
        return (False, message)

    reference_subtype = sender.get('reference_subtype')
    # The source code/type from decoded should match the reference subtype in order to be valid
    if reference_subtype and reference_subtype != source_type:
        log({'log_reference': LogReference.VALSEND0004})
        message = 'Sender source type invalid'
        return (False, message)

    log({'log_reference': LogReference.VALSEND0005})
    return (True, message)


def _validated_sender_type_1(sender_details, test_date) -> Tuple[bool, Union[str, None]]:
    """
    Perform all necessary validation for Sender of Source Type 1
    """
    SENDER_IS_NOT_GP_ERROR = "Sender is not GP practice"
    SENDER_INACTIVE_ERROR = "Sender inactive at test date"

    gp_practice = _sender_is_gp_practice(sender_details)
    if not gp_practice:
        return (False, SENDER_IS_NOT_GP_ERROR)

    was_closed_on_test_date = not _sender_operational_at_date_of_test(sender_details, test_date)

    if was_closed_on_test_date:
        return (False, SENDER_INACTIVE_ERROR)

    return (True, None)


def _sender_is_gp_practice(sender_details):
    """
    Organisation is a GP practice if it has a Role of 76, when the source_type is 1
    """
    return sender_details.get('gp_practice_status', False)


@nottest
def _sender_operational_at_date_of_test(sender_details, test_date):
    """
    Check the organisation was operational on the date the test was taken.
    """
    operational_periods = sender_details.get('operational_periods', [])

    for operational_period in operational_periods:
        operational_start_date = operational_period.get('start', None)

        if not operational_start_date:
            continue

        operational_end_date = operational_period.get('end', date.today().isoformat())
        iso_start_date = None
        iso_end_date = None
        iso_test_date = None

        try:
            iso_start_date = isoparse(operational_start_date).date()
            iso_end_date = isoparse(operational_end_date).date()
            iso_test_date = isoparse(test_date).date()
        except Exception as e:
            # Log that we failed to parse the date to iso and skip this period
            log({'log_reference': LogReference.VALSEND0006, 'exception': str(e)})
            continue

        # This is the only test we need, if iso parsing passses, and end date defaults to today
        if iso_start_date <= iso_test_date <= iso_end_date:
            return True

    return False
