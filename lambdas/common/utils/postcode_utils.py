import re

is_valid_postcode_matcher = re.compile('^[A-Z0-9]{5,7}$')
is_completed_postcode_matcher = re.compile(
    '^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$')


def is_valid_postcode(postcode) -> bool:
    return postcode and bool(is_valid_postcode_matcher.match(postcode))


def is_completed_postcode(postcode) -> bool:
    return bool(is_completed_postcode_matcher.match(postcode))


def sanitise_postcode(postcode):
    return postcode.replace(' ', '').upper()
