from common.utils.dynamodb_access.operations import (
    dynamodb_get_item, dynamodb_put_item, dynamodb_query, dynamodb_update_item
)
from common.utils.dynamodb_access.operations import dynamodb_batch_get_items
from common.utils.dynamodb_helper_utils import (
    build_update_query, build_filter_expression)
from boto3.dynamodb.conditions import Key, Attr, And

from common.utils.dynamodb_access.table_names import TableNames, get_dynamodb_table_name


def get_result(result_id, received_time):
    return dynamodb_get_item(TableNames.RESULTS, {
        'result_id': result_id,
        'received_time': received_time
    })


def get_results_by_result_keys(result_keys, projection_expression):
    table_name = get_dynamodb_table_name(TableNames.RESULTS)
    batch_keys = {
        table_name: {
            'Keys': [
                {
                    'result_id': result_key['result_id'],
                    'received_time': result_key['received_time']
                } for result_key in result_keys
            ]
        }
    }

    if projection_expression:
        batch_keys[table_name]['ProjectionExpression'] = projection_expression

    response = dynamodb_batch_get_items(batch_keys)
    return response['Responses'][table_name]


def query_results_by_participant_id(participant_id):
    condition_expression = Key('matched_to_participant').eq(participant_id)
    return dynamodb_query(TableNames.RESULTS, dict(
        IndexName='matched-to-participant',
        KeyConditionExpression=condition_expression,
    ))


def query_duplicate_results(result_id, sending_lab, slide_number):
    condition_expression = And(Key('sending_lab').eq(sending_lab), Key('slide_number').eq(slide_number))
    filter_expression = [
        Attr('result_id').ne(result_id)
    ]

    return dynamodb_query(TableNames.RESULTS, dict(
        IndexName='sending-lab-slide-number',
        KeyConditionExpression=condition_expression,
        FilterExpression=build_filter_expression(filter_expression)
    ))


def create_replace_record(record):
    return dynamodb_put_item(TableNames.RESULTS, record)


def update_result_record(key, update_attributes: dict):
    update_expression, expression_names, expression_values = build_update_query(update_attributes, [])

    return dynamodb_update_item(TableNames.RESULTS, dict(
        Key=key,
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ExpressionAttributeNames=expression_names,
        ReturnValues='NONE'
    ))
