import re
import uuid
from datetime import date, datetime, timezone
from typing import Any, Dict, List

from boto3.dynamodb.conditions import Key, Attr

from common.log import log
from common.log_references import CommonLogReference
from common.models.participant import ParticipantSortKey
from common.utils import _remove_empty_items_from_dictionary
from common.utils.dynamodb_access.operations import dynamodb_batch_get_items
from common.utils.dynamodb_access.operations import (
    dynamodb_get_item, dynamodb_put_item, dynamodb_query, dynamodb_update_item, dynamodb_transact_write_items
)
from common.utils.dynamodb_access.segregated_operations import (
    segregated_dynamodb_get_item, segregated_dynamodb_transact_write_items,
    segregated_dynamodb_update_item, segregated_put_item, segregated_query
    )
from common.utils.dynamodb_access.table_names import TableNames, get_dynamodb_table_name
from common.utils.dynamodb_helper_utils import (
    build_filter_expression, build_update_query, paginate,
    build_update_record, build_put_record, build_delete_record,
    serialize_item, serialize_update
)
from common.utils.nhs_number_utils import sanitise_nhs_number
from common.utils.postcode_utils import sanitise_postcode
from common.utils.audit_utils import audit, AuditActions, AuditUsers

PROJECTION_EXPRESSION = ', '.join([
    'nhs_number',
    'participant_id',
    'sort_key',
    'title',
    'first_name',
    'middle_names',
    'last_name',
    'patient_name',
    'gender',
    'address',
    'date_of_birth',
    'is_fp69',
    'is_ceased',
    'event_text',
    'registered_gp_practice_code',
    'active',
    'nhais_cipher',
    'is_paused'
])

PARTICIPANT_AND_RESULTS_PROJECTION = ', '.join([
    'nhs_number',
    'sort_key',
    'participant_id',
    'title',
    'first_name',
    'middle_names',
    'last_name',
    'gender',
    'address',
    'date_of_birth',
    'date_of_death',
    'is_ceased',
    'is_invalid_patient',
    'next_test_due_date',
    'registered_gp_practice_code',
    'infection_result',
    'infection_code',
    '#result',
    '#action',
    'action_code',
    'test_date',
    'recall_months',
    'result_code',
    'sender_code',
    'source_code',
    'sender_source_type',
    'slide_number',
    'sending_lab',
    '#status',
    'suppression_reason',
    'invited_date',
    'reason',
    'date_from',
    'is_fp69',
    'event_text',
    'health_authority',
    'crm',
    'active',
    'reason_for_removal_code',
    'reason_for_removal_effective_from_date',
    'hpv_primary',
    'self_sample',
    'practice_code',
    'nhais_cipher',
    'is_paused',
    'is_paused_received_time'
])
SANITISED_STRING_DISALLOWED_CHARACTERS = re.compile('[^a-zA-Z0-9]')


# The segregate bool eventually wants to be removed as segregation will be the default and only behaviour
def get_participant_by_participant_id(participant_id, segregate=False):
    get_item_function = segregated_dynamodb_get_item if segregate else dynamodb_get_item
    return get_item_function(TableNames.PARTICIPANTS, {
        'participant_id': participant_id,
        'sort_key': ParticipantSortKey.PARTICIPANT.value
    })


def get_participants_by_participant_ids(participant_ids, projection_expression=None):
    table_name = get_dynamodb_table_name(TableNames.PARTICIPANTS)
    batch_keys = {
        table_name: {
            'Keys': [
                {
                    'participant_id': participant_id,
                    'sort_key': 'PARTICIPANT'
                } for participant_id in participant_ids
            ]
        }
    }

    if projection_expression:
        batch_keys[table_name]['ProjectionExpression'] = projection_expression

    response = dynamodb_batch_get_items(batch_keys)

    return response['Responses'][table_name]


# Also returns CEASE records
def get_participant_and_test_results(participant_id, segregate=False):
    condition_expression = Key('participant_id').eq(participant_id)
    order_by_ascending = False
    query_function = segregated_query if segregate else dynamodb_query
    return query_function(TableNames.PARTICIPANTS, dict(
        KeyConditionExpression=condition_expression,
        ProjectionExpression=PARTICIPANT_AND_RESULTS_PROJECTION,
        ExpressionAttributeNames={'#result': 'result', '#action': 'action', '#status': 'status'},
        ScanIndexForward=order_by_ascending
    ))


def query_participants_by_nhs_number(nhs_number: str, additional_fields=None):
    index_name = 'sanitised-nhs-number-sort-key'
    sanitised_nhs_number = sanitise_nhs_number(nhs_number)
    condition_expression = Key('sanitised_nhs_number').eq(sanitised_nhs_number) \
        & Key('sort_key').eq(ParticipantSortKey.PARTICIPANT.value)
    projection_expression = PROJECTION_EXPRESSION
    if additional_fields:
        if isinstance(additional_fields, str):
            additional_fields = [additional_fields]
        projection_expression += ', ' + ', '.join(additional_fields)

    return query_via_secondary_index(index_name, condition_expression, None, projection_expression)


def query_participants_by_nhs_number_all_fields(nhs_number: str):
    index_name = 'sanitised-nhs-number-sort-key'
    sanitised_nhs_number = sanitise_nhs_number(nhs_number)
    condition_expression = Key('sanitised_nhs_number').eq(sanitised_nhs_number) \
        & Key('sort_key').eq(ParticipantSortKey.PARTICIPANT.value)
    return query_via_secondary_index(index_name, condition_expression, None, None)


def query_participants_by_demographics(index_name, condition_expression, filter_expression=None):
    return query_via_secondary_index(index_name, condition_expression, filter_expression, PROJECTION_EXPRESSION)


def query_participants_by_next_test_due_date(date, callback_function):
    condition_expression = Key('next_test_due_date').eq(date)
    query_arguments = {
        'IndexName': 'next-test-due-date',
        'KeyConditionExpression': condition_expression,
        'FilterExpression': build_filter_expression([Attr('active').eq(True)]),
        'ProjectionExpression': PROJECTION_EXPRESSION
    }
    paginated_query(query_arguments, callback_function)


@paginate
def paginated_query(query_arguments, callback_function, callback_function_args={}):
    response = dynamodb_query(TableNames.PARTICIPANTS, query_arguments, return_all=True)
    callback_function(response.get('Items'), **callback_function_args)
    return response


def query_participant_records_by_source_hash(source_hash):
    return dynamodb_query(TableNames.PARTICIPANTS, dict(
        IndexName='source-hash',
        KeyConditionExpression=Key('source_hash').eq(source_hash)
    ))


def get_record_from_participant_table(participant_id, sort_key, segregate=False):
    get_item_func = segregated_dynamodb_get_item if segregate else dynamodb_get_item
    return get_item_func(TableNames.PARTICIPANTS,  {
        'participant_id': participant_id,
        'sort_key': sort_key
    })


def create_replace_record_in_participant_table(
        record, condition_expression=None, condition_expression_value=None, segregate=False
        ):
    set_search_indexes(record)
    keyword_args = {}
    if condition_expression and condition_expression_value:
        keyword_args = dict(
            ConditionExpression=condition_expression,
            ExpressionAttributeValues=condition_expression_value)

    put_item_func = segregated_put_item if segregate else dynamodb_put_item

    return put_item_func(
        TableNames.PARTICIPANTS,
        dict(record),
        **keyword_args
    )


# The segregate bool eventually wants to be removed as segregation will be the default and only behaviour
def update_participant_record(record_key, update_attributes: dict, delete_attributes=None,
                              return_values: str = 'NONE', segregate=False):
    update_item_func = segregated_dynamodb_update_item if segregate else dynamodb_update_item

    if not delete_attributes:
        delete_attributes = []
    set_search_indexes(update_attributes)
    update_expression, expression_names, expression_values = build_update_query(update_attributes,
                                                                                delete_attributes=delete_attributes)

    if expression_values:
        return update_item_func(TableNames.PARTICIPANTS, dict(
            Key=record_key,
            UpdateExpression=update_expression,
            ExpressionAttributeValues=expression_values,
            ExpressionAttributeNames=expression_names,
            ReturnValues=return_values
        ))
    else:
        return update_item_func(TableNames.PARTICIPANTS, dict(
            Key=record_key,
            UpdateExpression=update_expression,
            ExpressionAttributeNames=expression_names,
            ReturnValues=return_values
        ))


def transaction_write_records(put_records=[], keys_to_delete=[], update_records=[], segregate=False):
    serialized_put_records = [serialize_item(put_record) for put_record in put_records]
    serialized_keys_to_delete = [serialize_item(key_to_delete) for key_to_delete in keys_to_delete]
    serialized_update_records = [serialize_update(update_record) for update_record in update_records]

    transact_items = [
        *[build_put_record(item, TableNames.PARTICIPANTS) for item in serialized_put_records],
        *[build_delete_record(
            item, TableNames.PARTICIPANTS) for item in serialized_keys_to_delete],
        *[build_update_record(record, TableNames.PARTICIPANTS)
          for record in serialized_update_records],
    ]

    transact_write_items_func = segregated_dynamodb_transact_write_items if segregate else dynamodb_transact_write_items

    return transact_write_items_func(transact_items)


def generate_participant_id():
    return str(uuid.uuid4())


def sanitise_string(string: str) -> str:
    return SANITISED_STRING_DISALLOWED_CHARACTERS.sub('', string).upper()


def get_first_letter(string: str) -> str:
    return string[0] if string else ''


def set_search_indexes(record):
    postcode = record.get('address', {}).get('postcode', '')
    first_name = record.get('first_name', '')
    last_name = record.get('last_name', '')
    dob = record.get('date_of_birth', '')

    sanitised_postcode = sanitise_postcode(postcode)
    sanitised_first_name = sanitise_string(first_name)
    sanitised_last_name = sanitise_string(last_name)
    initial = get_first_letter(sanitised_last_name)

    if sanitised_postcode:
        record['sanitised_postcode'] = sanitised_postcode

    if sanitised_first_name:
        record['sanitised_first_name'] = sanitised_first_name

    if sanitised_last_name:
        record['sanitised_last_name'] = sanitised_last_name

    if dob and initial and sanitised_postcode:
        record['date_of_birth_and_initial_and_postcode'] = dob + initial + sanitised_postcode

    if dob and initial:
        record['date_of_birth_and_initial'] = dob + initial


def convert_dynamodb_entity_to_participant_response(participant: Dict[Any, Any], test_results,
                                                    registered_gp_practice_details) -> Dict[Any, Any]:
    address = participant.get('address')
    converted_address = convert_address_entity_to_address_response(address) if address else None
    converted_test_results = convert_test_results_entity_to_response(test_results) if test_results else []

    return {
        'nhs_number': participant['nhs_number'],
        'date_of_birth': participant.get('date_of_birth'),
        'gender': participant.get('gender'),
        'title': participant.get('title'),
        'first_name': participant.get('first_name'),
        'middle_names': participant.get('middle_names'),
        'last_name': participant.get('last_name'),
        'next_test_due_date': participant.get('next_test_due_date'),
        'date_of_death': participant.get('date_of_death'),
        'is_invalid_patient': participant.get('is_invalid_patient'),
        'status': participant.get('status'),
        'is_fp69': participant.get('is_fp69'),
        'event_text': participant.get('event_text'),

        # Test Results
        'test_results': converted_test_results,

        # GP Practice Information
        'registered_gp_practice': registered_gp_practice_details,

        # Key Demographic Information
        'address': converted_address,
        'participant_id': participant['participant_id'],

        # Active Status
        'active': participant.get('active', True),

        # Additional Information
        'reason_for_removal_code': participant.get('reason_for_removal_code'),
        'reason_for_removal_effective_from_date': participant.get('reason_for_removal_effective_from_date'),
        'is_ceased': participant.get('is_ceased'),
        'serial_change_number': participant.get('serial_change_number'),
        'superseded_nhs_number': participant.get('superseded_nhs_number'),
        'address_effective_from_date': participant.get('address_effective_from_date'),
        'invited_date': participant.get('invited_date'),
        'is_paused': participant.get('is_paused'),
        'is_paused_received_time': participant.get('is_paused_received_time')
    }


def convert_address_entity_to_address_response(address: Dict[Any, Any]) -> Dict[Any, Any]:
    return {
        'address_line_1': address.get('address_line_1'),
        'address_line_2': address.get('address_line_2'),
        'address_line_3': address.get('address_line_3'),
        'address_line_4': address.get('address_line_4'),
        'address_line_5': address.get('address_line_5'),
        'postcode': address.get('postcode')
    }


def convert_test_results_entity_to_response(test_results: List) -> List:
    converted_test_results = []
    for test_result in test_results:
        converted_test_result = {
            'sort_key': test_result['sort_key'],
            'title': test_result.get('title'),
            'first_name': test_result.get('first_name'),
            'last_name': test_result.get('last_name'),
            'date_of_birth': test_result.get('date_of_birth'),
            'gender': test_result.get('gender'),
            'health_authority': test_result.get('health_authority'),
            'sending_lab': test_result.get('sending_lab'),
            'source_code': test_result.get('source_code'),
            'sender_code': test_result.get('sender_code'),
            'sender_source_type': test_result.get('sender_source_type'),
            'slide_number': test_result.get('slide_number'),
            'test_date': test_result.get('test_date'),
            'infection_result': test_result.get('infection_result'),
            'infection_code': test_result.get('infection_code'),
            'result': test_result.get('result'),
            'result_code': test_result.get('result_code'),
            'action': test_result.get('action'),
            'action_code': test_result.get('action_code'),
            'hpv_primary': test_result.get('hpv_primary'),
            'self_sample': test_result.get('self_sample'),
            'recall_months': test_result.get('recall_months'),
            'crm': test_result.get('crm', []),
            'notification_status': test_result.get('notification_status'),
            'nhais_cipher': test_result.get('nhais_cipher')
        }
        converted_test_results.append(converted_test_result)

    return converted_test_results


def get_participant_id(nhs_number):
    participants = query_participants_by_nhs_number(nhs_number)
    if participants:
        if len(participants) > 1:
            log({'log_reference': CommonLogReference.PARTICIPANT0001})
            return None

        participant = participants[0]
        return participant['participant_id']
    else:
        return None


def query_via_secondary_index(index_name, condition_expression, filter_expression, projection_expression):
    if filter_expression:
        return segregated_query(TableNames.PARTICIPANTS, dict(
            IndexName=index_name,
            KeyConditionExpression=condition_expression,
            FilterExpression=build_filter_expression(filter_expression),
            ProjectionExpression=projection_expression
        ))

    if projection_expression:
        return segregated_query(TableNames.PARTICIPANTS, dict(
            IndexName=index_name,
            KeyConditionExpression=condition_expression,
            ProjectionExpression=projection_expression
        ))

    return segregated_query(TableNames.PARTICIPANTS, dict(
        IndexName=index_name,
        KeyConditionExpression=condition_expression
    ))


def query_results_by_participant_id(participant_id):
    condition_expression = Key('participant_id').eq(participant_id) & Key('sort_key').begins_with('RESULT#')
    return dynamodb_query(
        TableNames.PARTICIPANTS, dict(KeyConditionExpression=condition_expression))


def get_nhs_number_by_participant_id(participant_id, segregate=False):
    participant = get_participant_by_participant_id(participant_id, segregate=segregate)
    return participant.get('nhs_number', 'not present') if participant else 'participant not found'


def create_previous_name_record(existing_record):
    previous_name_record = create_previous_name_record_from_existing_record(existing_record)
    create_replace_record_in_participant_table(previous_name_record)


def create_previous_name_record_from_existing_record(existing_record):
    current_date = date.today()
    previous_name_record = {
        'participant_id': existing_record['participant_id'],
        'sort_key': "PREVNAME#" + current_date.isoformat(),
        'date_to': current_date.isoformat(),
        'title': existing_record.get('title', ''),
        'first_name': existing_record.get('first_name', ''),
        'middle_names': existing_record.get('middle_names', ''),
        'last_name': existing_record.get('last_name', '')
    }

    return _remove_empty_items_from_dictionary(previous_name_record)


def nhs_checksum_valid(nhs_number):
    try:
        CHECKSUM_WEIGHT = range(10, 1, -1)
        nhs_number = nhs_number.replace(" ", "")
        nhs_number_digits = map(int, nhs_number[:9])
        checksum = sum(w * d for w, d in zip(CHECKSUM_WEIGHT, nhs_number_digits)) * -1 % 11
        last_digit = int(nhs_number[-1:])
        return checksum == last_digit
    except Exception:
        return False


def unpause_participant(participant_id: str):
    attributes_to_delete = [
        "is_paused",
        "is_paused_result_id",
        "is_paused_crm_number",
        "is_paused_received_time"
    ]

    key = {
        "participant_id": participant_id,
        "sort_key": "PARTICIPANT"
    }

    update_participant_record(key, {}, attributes_to_delete)


def pause_participant_for_lab_or_manually_added_result(
        participant_id: str, result_reference: str, is_lab_result: bool, comment: str = None):
    attributes_to_update = {
        "is_paused": True,
        "is_paused_received_time": datetime.now(timezone.utc).isoformat()
    }

    if is_lab_result:
        attributes_to_update["is_paused_result_id"] = result_reference
    else:
        attributes_to_update["is_paused_info"] = {
            'crm_number': result_reference,
            'comment': comment if comment else ''
        }

    key = {
        "participant_id": participant_id,
        "sort_key": "PARTICIPANT"
    }

    update_participant_record(key, attributes_to_update)

    additional_information = {
        "participant_id": participant_id
    }
    additional_information.update(attributes_to_update)
    audit(
        action=AuditActions.PAUSE_PARTICIPANT,
        user=AuditUsers.SYSTEM,
        participant_ids=[participant_id],
        additional_information=additional_information
    )
