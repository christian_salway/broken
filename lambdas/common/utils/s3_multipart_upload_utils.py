import sys
from boto3 import client

BUFFER_SIZE = 5e6  # Minimum part size is 5MB, reflected in this buffer size
S3_ENDPOINT = 'https://s3.eu-west-2.amazonaws.com'


class S3MultiPartUpload():

    def __init__(self, bucket, key='', meta_data={}):
        self.bucket = bucket
        self.object_key = key
        self.meta_data = meta_data
        self.part_number = 0
        self.upload_id = None
        self.buffer = bytearray()
        self.record_count = 0
        self._create_s3_client()

    def reset(self):
        self.part_number = 0
        self.upload_id = None
        self.buffer = bytearray()
        self.record_count = 0

    def create_multipart_upload(self):
        if self.object_key == '':
            raise ValueError('S3 object key is not set')

        response = self.s3_client.create_multipart_upload(
            Bucket=self.bucket, Key=self.object_key
        )
        self.upload_id = response['UploadId']

    def upload_multipart_object(self, object_body, length):
        if self.object_key == '':
            raise ValueError('S3 object key is not set')

        if self.upload_id is None:
            raise ValueError('The transaction upload ID has not been set')

        self.record_count += length

        self.buffer += bytearray(object_body.read())

        if sys.getsizeof(self.buffer) >= BUFFER_SIZE:
            self.part_number += 1
            try:
                self.s3_client.upload_part(
                    Bucket=self.bucket,
                    Key=self.object_key,
                    Body=self.buffer,
                    PartNumber=self.part_number,
                    UploadId=self.upload_id
                )
            except Exception as ex:
                return ex
            else:
                self.buffer.clear()

    def complete_multipart_upload(self):

        #  Send final buffer in case of not meeting 5mb threshold
        if len(self.buffer):
            self.part_number += 1
            self.s3_client.upload_part(
                    Bucket=self.bucket, Key=self.object_key, Body=self.buffer,
                    PartNumber=self.part_number, UploadId=self.upload_id)

        parts = self.s3_client.list_parts(Bucket=self.bucket, Key=self.object_key, UploadId=self.upload_id)['Parts']

        if len(parts) != (self.part_number):
            raise ValueError(
                f'Mismatch in part numbers, {len(parts)} found in s3, but {self.part_number} have been sent'
            )

        filtered_part_list = [{k: v for (k, v) in part.items() if k in ['PartNumber', 'ETag']} for part in parts]

        try:
            response = self.s3_client.complete_multipart_upload(
                Bucket=self.bucket,
                Key=self.object_key,
                MultipartUpload={'Parts': filtered_part_list},
                UploadId=self.upload_id
            )
        except Exception as ex:
            return ex
        else:
            self.reset()

        return response

    def abort_multipart_upload(self):

        response = self.s3_client.abort_multipart_upload(
            Bucket=self.bucket,
            Key=self.object_key,
            UploadId=self.upload_id
        )

        self.reset()
        return response

    def _create_s3_client(self):
        self.s3_client = client('s3', endpoint_url='https://s3.eu-west-2.amazonaws.com')
