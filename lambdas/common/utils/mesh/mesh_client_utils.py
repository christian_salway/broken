import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import os
from mesh_client import MeshClient
from distutils import util
from common.utils.secrets_manager_utils import get_secrets_dictionary, get_secret_by_key
import tempfile
from common.log import log
from common.log_references import CommonLogReference as LogReference

MESH_HOST = os.environ.get('MESH_HOST')
MESH_ENVIRONMENT = os.environ.get('MESH_ENVIRONMENT')
MESH_VERIFY_SSL = bool(util.strtobool(os.environ.get('MESH_VERIFY_SSL', "TRUE")))

if MESH_VERIFY_SSL is False:
    requests.urllib3.disable_warnings(InsecureRequestWarning)


def get_mesh_client(mailbox_id, password, temp_dir_name):
    log({'log_reference': LogReference.MESHCLIENT0010, 'mesh_environment': MESH_ENVIRONMENT})
    mesh_shared_key = get_secret_by_key('mesh_shared_key', MESH_ENVIRONMENT).encode('utf-8')
    log({'log_reference': LogReference.MESHCLIENT0011})
    mesh_client_cert_file_name, mesh_client_private_key_file_name, mesh_ca_cert_name = \
        get_mesh_certificates_by_mailbox(mailbox_id, temp_dir_name)
    log({'log_reference': LogReference.MESHCLIENT0020})
    mesh_client = MeshClient(
        url=MESH_HOST,
        mailbox=mailbox_id,
        password=password,
        shared_key=mesh_shared_key,
        cert=(mesh_client_cert_file_name, mesh_client_private_key_file_name),
        verify=mesh_ca_cert_name
    )
    log({'log_reference': LogReference.MESHCLIENT0021})
    mesh_client.handshake()
    log({'log_reference': LogReference.MESHCLIENT0022})
    return mesh_client


def get_mesh_certificates_by_mailbox(mailbox_id, temp_dir_name):
    log({'log_reference': LogReference.MESHCLIENT0012})
    mesh_client_certs = get_secrets_dictionary('mesh_client_certificate')
    log({'log_reference': LogReference.MESHCLIENT0013})
    mesh_client_private_keys = get_secrets_dictionary('mesh_client_private_key')
    log({'log_reference': LogReference.MESHCLIENT0014})

    # try to find certificate and private key specific to the mailbox, if not found default to env specific cert/key
    use_mailbox_cert = True if mesh_client_certs.get(mailbox_id) else False
    mesh_client_cert = mesh_client_certs[mailbox_id] if use_mailbox_cert else mesh_client_certs.get(MESH_ENVIRONMENT)
    use_mailbox_key = True if mesh_client_private_keys.get(mailbox_id) else False
    mesh_client_private_key = mesh_client_private_keys[mailbox_id] if use_mailbox_key \
        else mesh_client_private_keys.get(MESH_ENVIRONMENT)
    log({'log_reference': LogReference.MESHCLIENT0015, 'use_mailbox_cert': use_mailbox_cert,
         'use_mailbox_key': use_mailbox_key})

    # store as temporary files for the mesh client
    log({'log_reference': LogReference.MESHCLIENT0016})
    mesh_client_cert_file = tempfile.NamedTemporaryFile(dir=temp_dir_name, delete=False)
    mesh_client_cert_file.write(mesh_client_cert.encode('utf-8'))
    mesh_client_private_key_file = tempfile.NamedTemporaryFile(dir=temp_dir_name, delete=False)
    mesh_client_private_key_file.write(mesh_client_private_key.encode('utf-8'))
    log({'log_reference': LogReference.MESHCLIENT0017})

    mesh_ca_cert_name = None
    if MESH_VERIFY_SSL:
        log({'log_reference': LogReference.MESHCLIENT0018})
        mesh_ca_certs = get_secrets_dictionary('mesh_ca_certificate')
        mesh_ca_cert = mesh_ca_certs[MESH_ENVIRONMENT]
        mesh_ca_cert_file = tempfile.NamedTemporaryFile(dir=temp_dir_name, delete=False)
        mesh_ca_cert_file.write(mesh_ca_cert.encode('utf-8'))
        mesh_ca_cert_name = mesh_ca_cert_file.name
        log({'log_reference': LogReference.MESHCLIENT0019})

    return mesh_client_cert_file.name, mesh_client_private_key_file.name, mesh_ca_cert_name


def get_message_contents_by_message_id(message_id, mesh_client):
    log({'log_reference': LogReference.MESHCLIENT0023})
    message = mesh_client.retrieve_message(message_id)
    log({'log_reference': LogReference.MESHCLIENT0024})
    file_name = message.mex_header('filename')
    from_mailbox_id = message.mex_header('from')
    file_contents = message.read()
    log({'log_reference': LogReference.MESHCLIENT0025, 'file_name': file_name, 'from_mailbox_id': from_mailbox_id})
    message.close()
    return file_name, from_mailbox_id,  file_contents
