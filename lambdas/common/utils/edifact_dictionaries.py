
CYTOLOGY_RESULTS = {
    '': 'No result',
    'X': 'No Cytology test undertaken',
    '1': 'Inadequate specimen',
    '2': 'Negative',
    '3': 'Low-grade dyskaryosis',
    '4': 'High-grade dyskaryosis (severe)',
    '5': 'Invasive squamous carcinoma',
    '6': 'Glandular neoplasia of endocervical type',
    '7': 'High-grade dyskaryosis (moderate)',
    '8': 'Borderline change in squamous cells',
    '9': 'Borderline change in endocervical cells',
    '0': 'Glandular neoplasia (non-cervical)',
    'N': 'Negative + HPV Triage (HPV tested)',
    'B': 'Borderline change in squamous cells',
    'E': 'Borderline change in endocervic cells',
    'G': 'Glandular neoplasia (non-cervical) (HPV Tested)',
    'M': 'Low-grade dyskariosis'
}

ACTIONS = {
    'A': 'Routine',
    'R': 'Repeat advised',
    'S': 'Suspended',
    'H': 'Record result only',
}

INFECTION_CODES = {
    '0': 'HPV negative',
    '9': 'HPV positive',
    'U': 'HPV inadequate',
    'Q': 'No HPV undertaken',
    '1': 'Trichomonas',
    '2': 'Candida',
    '3': 'Wart virus',
    '4': 'Herpes',
    '5': 'Actinomyces',
    '6': 'Other',
    '7': 'Multiple infection codes'
}
