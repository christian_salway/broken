from common.utils.dynamodb_access.operations import dynamodb_get_item, dynamodb_put_item
from common.utils.dynamodb_access.table_names import TableNames


PROJECTION_EXPRESSION = ', '.join([
    '#key',
    'record_id',
    'source_cipher',
    'reference_type',
    'reference_subtype',
    'reference_id',
    '#name',
    'dha_code',
    'national_code',
    'address',
    'telephone_number',
    'gp_practice_code',
    'active',
    'inactive_from',
    'cipher_responsible',
    'email_address_1',
    'email_address_2',
    'smartcard_id',
])


def create_replace_reference_record(record, condition_expression=None, condition_expression_value=None):

    if condition_expression:
        return dynamodb_put_item(TableNames.REFERENCE, dict(
            Item=record,
            ConditionExpression=condition_expression,
            ExpressionAttributeValues=condition_expression_value,
        ))
    else:
        return dynamodb_put_item(TableNames.REFERENCE, record)


def get_reference_by_cipher_and_key(source_cipher, key):
    expression_attribute_names = {'#key': 'key', '#name': 'name'}
    return dynamodb_get_item(
        TableNames.REFERENCE,
        {'source_cipher': source_cipher, 'key': key},
        dict(
            ProjectionExpression=PROJECTION_EXPRESSION,
            ExpressionAttributeNames=expression_attribute_names
        )
    )
