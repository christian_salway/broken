from boto3.dynamodb.conditions import Key, Attr
from common.utils.dynamodb_helper_utils import build_update_query

from datetime import datetime, timezone
from dateutil.relativedelta import relativedelta

from common.models.letters import NotificationStatus
from common.models.participant import ParticipantSortKey
from common.models.notification import NotificationType
from common.utils.dynamodb_access.operations import dynamodb_query, dynamodb_update_item
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.participant_utils import paginated_query


def query_latest_resendable_invite_reminder(participant_id: str):
    """
    Return an Array of Notifications for Invite Reminders in descending sort key order, which were not originally sent
    more than 12 months ago.

    """
    iso_date_months_ago = (datetime.now(timezone.utc) - relativedelta(months=12)).date().isoformat()

    condition_expression = (
        Key('participant_id').eq(participant_id) &
        Key('sort_key').gt(f'{ParticipantSortKey.NOTIFICATION.value}#{iso_date_months_ago}#')
    )

    return dynamodb_query(TableNames.PARTICIPANTS, {
        'KeyConditionExpression': condition_expression,
        'FilterExpression': Attr('type').eq(NotificationType.INVITATION.value) & (
            Attr('original_send_date').not_exists() | Attr('original_send_date').gt(iso_date_months_ago)
            # The original_send_date is set on records that have been resent, valid if null or within last 12 months.
        ),
        'ScanIndexForward': False
    })


def query_latest_resendable_result_letter(participant_id: str):
    """
    Return an Array of Notifications for Result letters in descending sort key order, which have a test date within the
    last 3 months.
    """
    iso_date_months_ago = (datetime.now(timezone.utc) - relativedelta(months=3)).date().isoformat()

    condition_expression = (
        Key('participant_id').eq(participant_id) &
        Key('sort_key').gt(f'{ParticipantSortKey.NOTIFICATION.value}#{iso_date_months_ago}#')
    )

    # The record_id contains the sort key of the result which includes the test_date.
    # The test-date must be in the last three months.
    record_id = f'{ParticipantSortKey.RESULT.value}#{iso_date_months_ago}'

    return dynamodb_query(TableNames.PARTICIPANTS, {
        'KeyConditionExpression': condition_expression,
        'FilterExpression': Attr('type').eq(NotificationType.RESULT.value) & (
            Attr('record_id').exists() & Attr('record_id').gt(record_id)
        ),
        'ScanIndexForward': False
    })


def get_notifications_from_result(participant_id: str, sort_key: str):
    condition_expression = (
        Key('participant_id').eq(participant_id) &
        Key('sort_key').begins_with(ParticipantSortKey.NOTIFICATION)
    )

    filter_expression = Attr('type').eq(NotificationType.RESULT.value) & (
        Attr('record_id').exists() & Attr('record_id').eq(sort_key)
    )

    return dynamodb_query(TableNames.PARTICIPANTS, {
        'KeyConditionExpression': condition_expression,
        'FilterExpression': filter_expression,
        # The record_id contains the sort key of the result which includes the test_date.
        'ScanIndexForward': False
    })


def query_print_file_notifications(file_name, callback_function, callback_args):
    condition_expression = Key('file_name').eq(file_name)
    query_arguments = {
        'IndexName': 'file-name',
        'KeyConditionExpression': condition_expression,
        'FilterExpression': Attr('sort_key').begins_with(ParticipantSortKey.NOTIFICATION)
    }
    paginated_query(query_arguments, callback_function, callback_args)


def query_print_file_notifications_and_filter(file_name, notification_status, callback_function, callback_args):
    condition_expression = Key('file_name').eq(file_name)
    query_arguments = {
        'IndexName': 'file-name',
        'KeyConditionExpression': condition_expression,
        'FilterExpression': Attr('sort_key').begins_with(ParticipantSortKey.NOTIFICATION) & (
            Attr('status').eq(notification_status)
        ),
    }
    paginated_query(query_arguments, callback_function, callback_args)


def update_notification_status(record, notification_status):
    key = {
        'participant_id': record['participant_id'],
        'sort_key': record['sort_key']
    }

    delete_attributes = []
    if notification_status is NotificationStatus.IN_PRINT_FILE:
        update_attributes = {
            'status': notification_status,
            'live_record_status': notification_status
        }
    else:
        update_attributes = {
            'status': notification_status,
        }
        delete_attributes = ['live_record_status']

    update_expression, expression_names, expression_values = build_update_query(
        update_attributes,
        delete_attributes
    )

    return dynamodb_update_item(
        TableNames.PARTICIPANTS,
        dict(
            Key=key,
            UpdateExpression=update_expression,
            ExpressionAttributeValues=expression_values,
            ExpressionAttributeNames=expression_names
        ))
