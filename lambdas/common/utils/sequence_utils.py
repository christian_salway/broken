from common.utils.dynamodb_access.operations import dynamodb_update_item
from common.utils.dynamodb_access.table_names import TableNames


def generate_interchange_number():
    return generate_sequence_number('interchange_number')


def generate_message_number():
    return generate_sequence_number('message_number')


def generate_sequence_number(sequence_name):
    """
    If a record with the given sequence_name doesn't exist, a new record will be created
    with the given sequence_name and the returned value will be 1 on the first call
    """
    response = dynamodb_update_item(TableNames.SEQUENCES, dict(
        Key={'sequence_name': sequence_name},
        UpdateExpression='ADD #sequence_value :increment',
        ExpressionAttributeValues={':increment': 1},
        ExpressionAttributeNames={'#sequence_value': 'sequence_value'},
        ReturnValues='UPDATED_NEW'
    ))

    sequence_value = response['Attributes']['sequence_value']

    return str(sequence_value).zfill(8)  # pad with leading zeroes up to 8 digits
