from datetime import datetime

SCHEMA_ERROR_KEY = 'schema_error'


class ImportMigrationSchemaValidation():
    """
    The validation rules below are for CSO1C subject records
    NHAIS data migration import validation
    """

    def validate_recall_type(self, recall_type):
        if recall_type:
            return self._validate_length(recall_type, 'Recall Type', 1, 1)
        else:
            return {SCHEMA_ERROR_KEY: 'Recall Type is a required value'}

    def validate_recall_stat(self, recall_stat):
        if recall_stat:
            return self._validate_length(recall_stat, 'Recall Status', 1, 1)

    def validate_fnr_count(self, fnr_count):
        if fnr_count:
            return self._validate_length(fnr_count, 'FNR Count', 0, 2)

    def validate_pos_doubt(self, pos_doubt):
        if pos_doubt:
            return self._validate_length(pos_doubt, 'Pos Doubt', 1, 1)

    def validate_notes(self, notes, note_number):
        if notes:
            return self._validate_length(notes, f'Notes{note_number}', 1, 60)

    def validate_national_sender_code(self, national_sender_code):
        if national_sender_code:
            return self._validate_length(national_sender_code, 'National Sender code', 6, 6)

    def validate_reason(self, reason):
        if reason:
            return self._validate_length(reason.replace('*', ''), 'Reason', 1, 2)

    def validate_path_id(self, path_id):

        schema_errors = []

        if not path_id:
            schema_errors.append('PATH_ID value was empty')

        length_error = self._validate_length(path_id, 'PATH_ID', 1, 2)
        if length_error:
            schema_errors.append(length_error[SCHEMA_ERROR_KEY])

        if not path_id.isdigit():
            schema_errors.append('PATH_ID is expected to be an integer')

        if schema_errors:
            return {SCHEMA_ERROR_KEY: ', '.join(schema_errors)}

    def validate_pnl_fnr(self, pnl_fnr):
        if pnl_fnr and pnl_fnr != 'UA':
            return self.validate_date(pnl_fnr)

    """
    The validation rules below are for CSO1R subject records
    NHAIS data migration import validation
    """

    def validate_ldn(self, ldn):
        if not ldn:
            return {SCHEMA_ERROR_KEY: 'LDN is a required value'}
        return self._validate_length(ldn, 'LDN', 2, 2)

    def validate_gender(self, gender):
        if gender:
            return self._validate_length(gender, 'Gender', 1, 1)

    def validate_gp_code(self, gp_code):
        if gp_code:
            return self._validate_length(gp_code, 'GP Code', 1, 6)

    def validate_gp_practice_code(self, gp_practice_code):
        if gp_practice_code:
            return self._validate_length(gp_practice_code, 'GP Practice Code', 6, 6)

    def validate_reason_for_removal(self, reason_for_removal):
        if reason_for_removal:
            return self._validate_length(reason_for_removal, 'Reason for removal', 1, 3)

    def validate_destination(self, destination):
        if destination:
            return self._validate_length(destination, 'Destination', 1, 3)

    def validate_title(self, title):
        if title:
            return self._validate_length(title, 'Title', 2, 4)

    def validate_surname(self, surname):
        if surname:
            return self._validate_length(surname, 'Surname', 1, 20)

    def validate_first_forename(self, first_forename):
        if first_forename:
            return self._validate_length(first_forename, 'First forename', 1, 22)

    def validate_other_forenames(self, other_forenames):
        if other_forenames:
            return self._validate_length(other_forenames, 'Other forename', 1, 22)

    # Can be used for Address 1, Address 2 and Locality
    def validate_address(self, address, address_type):
        if address:
            return self._validate_length(address, address_type, 1, 30)

    def validate_county(self, county):
        if county:
            return self._validate_length(county, 'County', 1, 20)

    def validate_postcode(self, postcode):
        if postcode:
            return self._validate_length(postcode, 'Postcode', 1, 8)

    """
    The validation rules below are for 2 screening test
    NHAIS data migration import validation
    """

    def validate_test_cipher(self, test_cipher):
        if test_cipher:
            return self._validate_length(test_cipher, 'Test cipher', 2, 3)

    def validate_test_result_code(self, test_result_code):
        if not test_result_code:
            return {SCHEMA_ERROR_KEY: 'Test result code is a required value'}
        return self._validate_length(test_result_code, 'Test result code', 1, 1)

    def validate_action_code(self, action_code):
        if not action_code:
            return {SCHEMA_ERROR_KEY: 'Action code is a required value'}
        return self._validate_length(action_code, 'Action code', 1, 1)

    def validate_infection_code(self, infection_code):
        if infection_code:
            return self._validate_length(infection_code, 'Infection code', 1, 1)

    def validate_source_code(self, source_code):
        if not source_code:
            return {SCHEMA_ERROR_KEY: 'Source code is a required value'}
        return self._validate_length(source_code, 'Source code', 1, 1)

    def validate_repeat_months(self, repeat):
        if not repeat:
            return {SCHEMA_ERROR_KEY: 'Repeat months is a required value'}
        return self._validate_length(repeat, 'Repeat months', 1, 2)

    def validate_lab_national_code(self, lab_national_code):
        if not lab_national_code:
            return {SCHEMA_ERROR_KEY: 'Lab national code is a required value'}
        return self._validate_length(lab_national_code, 'Lab national code', 5, 5)

    def validate_lab_local(self, lab_local):
        if not lab_local:
            return {SCHEMA_ERROR_KEY: 'Lab local is a required value'}
        return self._validate_length(lab_local, 'Lab local', 1, 1)

    def validate_sender_exist(self, sender_exist):
        if sender_exist:
            return self._validate_length(sender_exist, 'Sender exist', 1, 1)

    def validate_sender_code(self, sender_code):
        if sender_code:
            return self._validate_length(sender_code, 'Sender code', 1, 6)

    def validate_slide_number(self, slide_number):
        if slide_number:
            return self._validate_length(slide_number, 'Slide number', 8, 8)

    def validate_hpv(self, hpv):
        if hpv:
            return self._validate_length(hpv, 'HPV', 1, 1)

    def validate_invite_date(self, invite_date, is_required=False):

        if is_required and not invite_date:
            return {SCHEMA_ERROR_KEY: 'Date is a required value'}

        if invite_date:
            try:
                if not self._in_bounds(6, 6, len(invite_date)):
                    return {SCHEMA_ERROR_KEY: 'Date length must be exactly 6 characters'}
                else:
                    datetime.strptime(invite_date, '%Y%m').date()
            except ValueError:
                return {SCHEMA_ERROR_KEY: 'Invite date was in an invalid format. Expected YYYYMM'}

    def validate_comm(self, comm):
        if comm:
            return self._validate_length(comm, 'Comm', 1, 80)

    def validate_test_sequence_number(self, test_seq_number):
        if not test_seq_number:
            return {SCHEMA_ERROR_KEY: 'Test sequence number is a required value'}
        return self._validate_length(test_seq_number, 'Test sequence number', 1, 2)

    def validate_samp_meth(self, samp_meth):
        if samp_meth:
            if samp_meth != "S" and samp_meth != "s":
                return {SCHEMA_ERROR_KEY: 'Sample Method must be S or null'}

    """
    The validation rules below are for 3 cease and defer
    NHAIS data migration import validation
    """

    def validate_source_cipher(self, cipher):
        if not cipher:
            return {SCHEMA_ERROR_KEY: 'Source cipher is a required value'}
        return self._validate_length(cipher, 'Source cipher', 2, 3)

    def validate_nhs_number(self, nhs_number):
        if not nhs_number:
            return {SCHEMA_ERROR_KEY: 'NHS number is a required value'}
        return self._validate_length(nhs_number, 'NHS number', 1, 14)

    def validate_sequence_number(self, seq_number):
        if seq_number:
            return self._validate_length(seq_number, 'Sequence number', 1, 1)

    def validate_user_id(self, user_id):
        if user_id:
            return self._validate_length(user_id, 'User ID', 2, 9)

    def validate_reason_flag(self, reason_flag):
        if reason_flag:
            return self._validate_length(reason_flag, 'Reason flag', 1, 1)

    def validate_cease_code(self, cease_code):
        if cease_code:
            return self._validate_length(cease_code, 'Cease code', 1, 1)

    def validate_cease_cipher(self, cease_cipher):
        if cease_cipher:
            return self._validate_length(cease_cipher, 'Cease cipher', 2, 3)

    def validate_amend_flag(self, amend_flag):
        if amend_flag:
            return self._validate_length(amend_flag, 'Amend flag', 5, 5)
    """
    The validation rules below are for common functions
    shared throughout all validation rules
    """

    def validate_date(self, date, is_required=False):
        if not date:
            if is_required:
                return {SCHEMA_ERROR_KEY: 'Date is a required value'}
        else:
            try:
                if not self._in_bounds(8, 8, len(date)):
                    return {SCHEMA_ERROR_KEY: 'Date length must be exactly 8 characters'}
                else:
                    datetime.strptime(date, '%Y%m%d').date()
            except ValueError:
                return {SCHEMA_ERROR_KEY: 'Date was in an invalid format. Expected YYYYMMDD'}

    def validate_previous_test_due_date(self, date, is_required=False):
        if is_required and not date:
            return {SCHEMA_ERROR_KEY: 'Date is a required value'}
        try:
            if date == 'NONE':
                return None
            elif not self._in_bounds(8, 8, len(date)):
                return {SCHEMA_ERROR_KEY: 'Date length must be exactly 8 characters'}
            else:
                datetime.strptime(date, '%Y%m%d').date()
        except ValueError:
            return {SCHEMA_ERROR_KEY: 'Date was in an invalid format. Expected YYYYMMDD'}

    def _in_bounds(self, min_bound, max_bound, data_length):
        if not min_bound <= data_length <= max_bound:
            return False
        return True

    def _validate_length(self, field_value, field_description, min_bound, max_bound):
        if not self._in_bounds(min_bound, max_bound, len(str(field_value))):
            format_string = f'{field_description} must be between {min_bound} and {max_bound} characters (inclusive)'
            plural = 's' if min_bound > 1 else ''
            if min_bound == max_bound:
                format_string = f'{field_description} must be {min_bound} character{plural} long'

            return {SCHEMA_ERROR_KEY: format_string}
