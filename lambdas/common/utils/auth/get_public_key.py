import requests
import datetime
import json
from jwt.api_jwt import get_unverified_header

from common.utils.dynamodb_access.table_names import TableNames
from common.utils.dynamodb_access.operations import dynamodb_get_item, dynamodb_put_item
from common.log import log
from common.utils.auth.log_references import LogReference


def get_public_key(nhsid_config, config, token):
    try:
        log({'log_reference': LogReference.VETOK0001})
        jwks_uri = nhsid_config['jwks_uri']
        nhsid_supported_algorithms = nhsid_config['id_token_signing_alg_values_supported']
        supported_algorithms = get_allowed_algorithms(nhsid_supported_algorithms)
        if not supported_algorithms:
            log({'log_reference': LogReference.VETOK0005, 'nhsid_algorithms': nhsid_supported_algorithms})
            return None

        token_hash_key = get_hash_key_from_id_token(token)
        if not token_hash_key:
            return None

        public_jwk = get_public_key_from_cache(token_hash_key)
        if not public_jwk:
            log({'log_reference': LogReference.VETOK0006})
            public_key_info_json = requests.get(jwks_uri).json()
            return add_update_public_keys_cache(public_key_info_json.get('keys'), token_hash_key)

        return public_jwk
    except Exception as e:
        log({'log_reference': LogReference.VETOK0002, 'error': str(e)})


def get_public_key_from_cache(hash_key):
    log({'log_reference': LogReference.VETOK0008, 'hash_key': hash_key})
    item = dynamodb_get_item(TableNames.PUBLIC_KEY_CACHE, {'key_id_and_algorithm': hash_key})

    if not item:
        return None

    return json.loads(item.get('key'))


def add_update_public_keys_cache(keys, id_token_hash_key):
    expires = datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=91)
    expires_seconds = int(expires.timestamp())
    return_key = None
    for key in keys:
        hash_key = f'{key["kid"]}_{key["alg"]}'
        if id_token_hash_key == hash_key:
            return_key = key

        public_keys_item = {
            'key_id_and_algorithm': hash_key,
            'key': json.dumps(key),
            'expires': expires_seconds,
        }
        create_replace(TableNames.PUBLIC_KEY_CACHE, public_keys_item)

    return return_key


def get_hash_key_from_id_token(id_token):
    headers = get_unverified_header(id_token)
    kid = headers.get('kid')
    alg = headers.get('alg')
    if not kid or not alg or alg == 'none':
        log({'log_reference': LogReference.VETOK0007, 'headers': headers})
        return None

    return f'{kid}_{alg}'


def get_allowed_algorithms(nhsid_supported_algorithms):
    # To add support for algorithms other than RSA,
    # the JWK will need to be converted with the correspondng algorithm in verify_id_token
    screening_supported_algorithms = ['RS256']
    return [alg for alg in nhsid_supported_algorithms if alg in screening_supported_algorithms]


def create_replace(table_name, public_keys_item):
    dynamodb_put_item(table_name, public_keys_item)
