import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    VETOK0001 = (logging.INFO, 'Retrieved public key.')
    VETOK0002 = (logging.ERROR, 'Public keys could not be retrieved.')
    VETOK0005 = (logging.ERROR, 'RS256 is not supported by NHSID.')
    VETOK0006 = (logging.ERROR, 'Public key not found in cache. Retrieving from nhs identity.')
    VETOK0007 = (logging.ERROR, 'kid or alg missing from ID token header')
    VETOK0008 = (logging.INFO, 'Fetching public key from cache.')
