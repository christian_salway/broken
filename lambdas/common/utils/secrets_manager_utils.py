from boto3.session import Session
import base64
import json
from botocore.exceptions import ClientError
from common.exceptions import (
    DecryptionFailureException, InternalServiceErrorException, InvalidParameterException,
    InvalidRequestException, ResourceNotFoundException)


def get_secret(secret_name, secret_name_is_key=True, region_name="eu-west-2"):
    try:
        secret_value_response = get_secret_value_response(secret_name, region_name)
    except ClientError as e:
        handle_exceptions(e)
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in secret_value_response:
            secret = secret_value_response['SecretString']
            if secret_name_is_key:
                secret = json.loads(secret)[secret_name]
            return secret
        else:
            return get_decoded_binary_secret(secret_value_response)


def get_secret_by_key(secret_name, secret_key, region_name="eu-west-2"):
    try:
        secret_value_response = get_secret_value_response(secret_name, region_name)
    except ClientError as e:
        handle_exceptions(e)
    else:
        if 'SecretString' in secret_value_response:
            return json.loads(secret_value_response['SecretString'])[secret_key]
        else:
            return get_decoded_binary_secret(secret_value_response)


def get_secrets_dictionary(secret_name, region_name="eu-west-2"):
    secret_value_response = get_secret_value_response(secret_name, region_name)
    return json.loads(secret_value_response['SecretString'])


def get_secret_value_response(secret_name, region_name):
    client = create_secrets_manager_client(region_name)
    return client.get_secret_value(
        SecretId=secret_name
    )


def create_secrets_manager_client(region_name):
    # Create a Secrets Manager client
    session = Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name,
        endpoint_url='https://secretsmanager.eu-west-2.amazonaws.com'
    )
    return client


def get_decoded_binary_secret(secret_value_response):
    decoded_binary_secret = base64.b64decode(secret_value_response['SecretBinary'])
    return decoded_binary_secret


def handle_exceptions(e):
    if e.response['Error']['Code'] == 'DecryptionFailureException':
        raise DecryptionFailureException()
    elif e.response['Error']['Code'] == 'InternalServiceErrorException':
        raise InternalServiceErrorException()
    elif e.response['Error']['Code'] == 'InvalidParameterException':
        raise InvalidParameterException()
    elif e.response['Error']['Code'] == 'InvalidRequestException':
        raise InvalidRequestException()
    elif e.response['Error']['Code'] == 'ResourceNotFoundException':
        raise ResourceNotFoundException()
    else:
        raise e
