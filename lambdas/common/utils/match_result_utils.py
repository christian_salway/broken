from typing import Union, Dict, List
from datetime import date, datetime, timezone

from dateutil.parser import parse
from dateutil.relativedelta import relativedelta

from common.log import log
from common.log_references import CommonLogReference
from common.models.participant import CeaseReason
from common.utils.audit_utils import AuditActions, AuditUsers, audit
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator import (
    calculate_next_test_due_date_and_status_for_ceased_participant,
    calculate_next_test_due_date_for_result
)
from common.utils.dynamodb_helper_utils import (
    build_update_record, build_put_record, serialize_item, serialize_update
)
from common.utils.participant_utils import (
    create_replace_record_in_participant_table,
    get_participant_and_test_results,
    query_results_by_participant_id,
    update_participant_record,
    pause_participant_for_lab_or_manually_added_result,
    unpause_participant
)
from common.utils.nhs_number_utils import sanitise_nhs_number
from common.utils.result_utils import (
    get_result, query_duplicate_results,
    update_result_record
)
from common.utils.participant_episode_utils import (
    update_live_episodes_status_to_logged,
    update_live_episodes_status_from_logged_to_previous
)

from common.models.participant import ParticipantSortKey
from common.models.result import ResultRejectionReason, ResultWorkflowState
from common.utils.dynamodb_access.table_names import TableNames


INADEQUATE_RESULT_CODE_COMBOS = ('19R', 'XUR', '1QR', 'XUS')
POSITIVE_HPV_RESULT_COMBOS = ('09R', '29R')
UNCHANGED_RECALL_TYPE = 'Unchanged'
REASON_FOR_REMOVAL_CODES = ['CGA', 'DEA', 'EMB', 'LDN', 'NIT', 'ORR', 'RFI', 'SCT', 'TRA']
RECALL_ACTION_CODE = 'R'
SUSPEND_ACTION_CODE = 'S'
FAIL_SAFE_RECALL_MONTHS = 12
REJECT_REASONS_FOR_DELETION = [
    ResultRejectionReason.REMOVED_DEATH,
    ResultRejectionReason.CEASED_NO_CERVIX]


def get_result_record(result_id, received_date):
    return get_result(result_id, received_date)


def result_matched_already(result):
    return result.get('matched_time')


def validate_matched_result(record: dict, participant_id: str) -> Union[str, None]:
    """
    If a result should be rejected a string is returned
    If the result is valid None is returned
    """
    rejected_reason = None
    cease_records = None

    previous_results = get_latest_result_sorted_by_latest(participant_id)
    participant_record, participant_and_test_results = _get_participant_and_test_results(participant_id)
    if participant_and_test_results:
        cease_records = _extract_records(participant_and_test_results, ParticipantSortKey.CEASE)

    validation_methods = [
        {
            'method': _second_consecutive_inadequate_colposcopy_result,
            'parameters': (record, previous_results)
        },
        {
            'method': _invalid_return_to_routine_recall,
            'parameters': (record, previous_results)
        },
        {
            'method': _participant_ceased_with_reason_death_before_test_date,
            'parameters': (record, participant_record)
        },
        {
            'method': _participant_ceased_with_reason_no_cervix,
            'parameters': (record, participant_record, cease_records)
        },
        {
            'method': _third_positive_HPV_result,
            'parameters': (record, previous_results)
        },
        {
            'method': _radiotherapy_check,
            'parameters': (record, participant_record, cease_records)
        },
        {
            'method': _inactive_participant_check,
            'parameters': (record, participant_record)
        },
        {
            'method': _pre_match_validation_errors_check,
            'parameters': (record, {})
        }
    ]

    for validation_method in validation_methods:
        rejected_reason = validation_method['method'](*validation_method['parameters'])
        if rejected_reason:
            break

    return rejected_reason


def get_latest_result_sorted_by_latest(participant_id: str) -> List[Dict]:
    results = query_results_by_participant_id(participant_id)

    if not results:
        return None
    sorted_results = sorted(results, key=lambda r: r.get('test_date', ''), reverse=True)
    return sorted_results


def filter_out_inadequate_results(results):
    adequate_results = []
    for result in results:
        if _get_result_combination(result) in INADEQUATE_RESULT_CODE_COMBOS:
            continue
        adequate_results.append(result)

    return adequate_results


def _pre_match_validation_errors_check(current_result, _):
    """
    Check to see if there are any pre-match validation errors being carried
    already by this result. If so, the result must now be rejected
    """
    validation_errors = current_result.get('validation_errors', [])
    if len(validation_errors) > 0:
        return "Missing or invalid data"
    return None


def _third_positive_HPV_result(current_result, previous_results):
    if not current_result or not previous_results or len(previous_results) < 2:
        return None

    current_result_combo = _get_result_combination(current_result)
    if current_result_combo not in POSITIVE_HPV_RESULT_COMBOS:
        return None

    previous_adequate_results = filter_out_inadequate_results(previous_results)
    if len(previous_adequate_results) < 2:
        return None

    if _get_result_combination(previous_adequate_results[0]) in POSITIVE_HPV_RESULT_COMBOS\
            and _get_result_combination(previous_adequate_results[1]) in POSITIVE_HPV_RESULT_COMBOS:
        return ResultRejectionReason.THIRD_POSITIVE_HPV_RESULT

    return None


def _second_consecutive_inadequate_colposcopy_result(decoded_record, previous_results):
    if not previous_results:
        return None

    latest_result = previous_results[0]

    current_result_combination = _get_result_combination(decoded_record)
    latest_result_combination = _get_result_combination(latest_result)

    if (latest_result_combination == "XUR" or latest_result_combination == "19R") and \
       (current_result_combination == "XUR" or current_result_combination == "19R"):
        return ResultRejectionReason.SECOND_CONSECUTIVE_INADEQUATE_COLPOSCOPY
    else:
        return None


def _get_result_combination(result):
    result_code = result.get('result_code', '')
    infection_code = result.get('infection_code', '')
    action_code = result.get('action_code', '')

    return result_code + infection_code + action_code


def build_reject_result_data(record, reason, additional_data=None):
    key = {
        'result_id': record['result_id'],
        'received_time': record['received_time']
    }
    attributes_to_update = {
        'workflow_state': ResultWorkflowState.REJECTED,
        'rejected_reason': reason,
    }

    if additional_data:
        attributes_to_update.update(additional_data)

    return key, attributes_to_update


def reject_result(record, reason, additional_data=None):
    key, attributes_to_update = build_reject_result_data(
        record, reason, additional_data
    )

    update_result_record(key, attributes_to_update)


def reject_result_and_pause_participant(rejected_reason: str, participant_id: str, result_record: Dict) -> None:
    workflow_attributes = get_workflow_attributes(rejected_reason, participant_id)

    # Responsible only for building an update, no side effects
    key, update_attributes = build_reject_result_data(result_record, rejected_reason, workflow_attributes)

    log({'log_reference': CommonLogReference.PARTICIPANT0011})
    update_result_record(key, update_attributes)
    update_episodes_and_pause_participant_for_lab_result(
        result_record['matched_to_participant'], result_record['result_id']
    )


def update_episodes_and_pause_participant_for_lab_result(participant_id: str, result_id: str):
    log({'log_reference': CommonLogReference.PARTICIPANT0012, 'result_id': result_id})
    update_live_episodes_status_to_logged(participant_id)
    pause_participant_for_lab_or_manually_added_result(participant_id, result_id, True)


def update_episodes_and_pause_participant_for_manually_added_result(participant_id: str, crm_number: str, comment: str):
    log({'log_reference': CommonLogReference.PARTICIPANT0012, 'crm_number': crm_number})
    update_live_episodes_status_to_logged(participant_id)
    pause_participant_for_lab_or_manually_added_result(participant_id, crm_number, False, comment)


def update_episodes_and_unpause_participant(participant_id: str, crm_number: str, comment: str):
    log({'log_reference': CommonLogReference.PARTICIPANT0013})
    update_live_episodes_status_from_logged_to_previous(participant_id)
    unpause_participant(participant_id)

    audit(
        action=AuditActions.UNPAUSE_PARTICIPANT,
        user=AuditUsers.SYSTEM,
        participant_ids=[participant_id],
        additional_information={'crm_number': crm_number, 'comment': comment}
    )


def get_workflow_attributes(reason, participant_id):
    additional_attributes = {}
    if reason in REJECT_REASONS_FOR_DELETION:
        additional_attributes = {'workflow_history': [{'selected_participant_id': participant_id}]}
    return additional_attributes


def _invalid_return_to_routine_recall(decoded_record, previous_results):
    """
    CERSS-897

    When a Participant has the action_code 'S' - Suspend, for two consecutive
    previous tests, and the action_code for the current test is not 'R' - Routine
    then reject with specific reason.
    """
    if not previous_results or len(previous_results) < 2:
        return None

    action_code = decoded_record.get('action_code')
    latest_action_codes = previous_results[0].get('action_code', '') \
        + previous_results[1].get('action_code', '')

    if action_code != 'R' and latest_action_codes == 'SS':
        return ResultRejectionReason.INVALID_RETURN_TO_ROUTINE_RECALL
    else:
        return None


def _check_if_participant_should_be_reinstated(participant_id, test_date=None):
    do_not_reinstate = False, None, None

    participant_and_test_results = get_participant_and_test_results(participant_id)
    if not participant_and_test_results:
        return do_not_reinstate

    participant_records = _extract_records(participant_and_test_results, ParticipantSortKey.PARTICIPANT)
    if not participant_records:
        return do_not_reinstate

    participant_record = participant_records[0]

    cease_records = _extract_records(participant_and_test_results, ParticipantSortKey.CEASE)
    is_ceased = participant_record.get('is_ceased', False)
    if not cease_records or is_ceased is False:
        return do_not_reinstate

    test_results = _extract_records(participant_and_test_results, ParticipantSortKey.RESULT)

    cease_reason = cease_records[0].get('reason')
    cease_date = cease_records[0].get('date_from')
    reinstate = False
    if cease_reason in [CeaseReason.MENTAL_CAPACITY_ACT, CeaseReason.PATIENT_INFORMED_CHOICE] and\
            parse(cease_date).date() <= date.fromisoformat(test_date):
        reinstate = True
    elif reinstate is False and cease_reason == CeaseReason.DUE_TO_AGE:
        reinstate = True

    calculator_response = None
    if reinstate:
        calculator_response = calculate_next_test_due_date_and_status_for_ceased_participant(participant_record,
                                                                                             test_results,
                                                                                             cease_reason)
        if not (calculator_response or calculator_response['status']):
            return do_not_reinstate

    return reinstate, participant_record, calculator_response


def reinstate_participant(participant_id, test_date=None):
    reinstate, participant, calculator_response = _check_if_participant_should_be_reinstated(
        participant_id, test_date)
    if reinstate:
        key, update_attributes = generate_reinstate_update(participant_id, calculator_response)
        if not key:
            return

        update_participant_record(key, update_attributes=update_attributes)

        reinstate_participant_record = generate_reinstate_participant_record(participant_id)

        create_replace_record_in_participant_table(reinstate_participant_record)

        audit(
            action=AuditActions.REINSTATE_PARTICIPANT,
            user=AuditUsers.MATCH_RESULT,
            participant_ids=[participant_id],
            additional_information={'updating_next_test_due_date_to': update_attributes['next_test_due_date']}
        )

    return reinstate


def generate_reinstate_participant_transactions(participant_id, table_enum: TableNames, test_date=None):
    reinstate, participant, calculator_response = _check_if_participant_should_be_reinstated(
        participant_id, test_date)
    participant_update = None
    reinstate_put = None

    if reinstate:
        key, update_attributes = generate_reinstate_update(participant_id, calculator_response)

        update = {
            'record_keys': key,
            'update_values': update_attributes
        }
        participant_update = build_update_record(serialize_update(update), table_enum)

        reinstate_participant_record = generate_reinstate_participant_record(participant_id)
        reinstate_put = build_put_record(serialize_item(reinstate_participant_record), table_enum)

    reinstated_next_test_due_date = update_attributes['next_test_due_date'] if reinstate else None
    return participant_update, reinstate_put, reinstated_next_test_due_date


def create_participant_result_record(decoded_record, result_id, matched_participant, created):
    test_date = decoded_record['test_date']
    sort_key = f'RESULT#{test_date}#{created}'

    result_record = {
        'participant_id': matched_participant['participant_id'],
        'sort_key': sort_key,
        'result_id': result_id,
        'nhs_number': matched_participant['nhs_number'],
        'sanitised_nhs_number': sanitise_nhs_number(matched_participant['nhs_number']),
        'slide_number': decoded_record['slide_number'],
        'created': created,
        'test_date': test_date,
        'result_date': created,
        'next_test_due_date': calculate_ntdd(decoded_record).isoformat(),
        'action': decoded_record['action'],
        'action_code': decoded_record['action_code'],
        'result': decoded_record['result'],
        'result_code': decoded_record['result_code'],
        'infection_result': decoded_record['infection_result'],
        'infection_code': decoded_record['infection_code'],
        'source_code': decoded_record['source_code'],
        'sender_source_type': decoded_record['sender_source_type'],
        'sender_code': decoded_record['sender_code'],
        'self_sample': decoded_record['self_sample'],
        'status': decoded_record['status'],
        'letter_address': matched_participant['letter_address'],
        'hpv_primary': decoded_record['hpv_primary'],
    }
    if decoded_record.get('action_code', '') == "R" and decoded_record.get('recall_months', '').isnumeric():
        result_record['recall_months'] = decoded_record['recall_months']
    return result_record


def _extract_records(participant_records, sort_key):
    return [record for record in participant_records if record and record['sort_key'].startswith(sort_key)]


def generate_reinstate_update(participant_id, calculator_response):
    default_response = None, None
    if not calculator_response:
        return default_response

    status = calculator_response.get('status')
    next_test_due_date = calculator_response.get('next_test_due_date')

    if not next_test_due_date:
        return default_response

    key = {
        'participant_id': participant_id,
        'sort_key': 'PARTICIPANT'
    }
    update_attributes = {
        'is_ceased': False,
        'next_test_due_date': next_test_due_date,
        'status': status
    }
    return key, update_attributes


def generate_reinstate_participant_record(participant_id):
    today_date = datetime.now(timezone.utc).date().isoformat()
    reinstate_participant_record = {
        'participant_id': participant_id,
        'sort_key': f'REINSTATE#{today_date}',
        'date_from': today_date,
        'user_id': AuditUsers.MATCH_RESULT.value
    }
    return reinstate_participant_record


def _get_participant_and_test_results(participant_id):
    default_response = None, None
    if not participant_id:
        return default_response

    participant_record = None

    participant_and_test_results = get_participant_and_test_results(participant_id)
    if not participant_and_test_results:
        return default_response

    participant_records = _extract_records(participant_and_test_results, ParticipantSortKey.PARTICIPANT)
    if participant_records:
        participant_record = participant_records[0]

    return participant_record, participant_and_test_results


def _participant_ceased_with_reason_death_before_test_date(decoded_record, participant_record):
    test_date = decoded_record.get('test_date')

    if not participant_record or not test_date:
        return None

    date_of_death = participant_record.get('date_of_death')

    if not date_of_death:
        return None

    is_ceased = participant_record.get('is_ceased')

    is_test_date_before_date_of_death = date.fromisoformat(test_date) < date.fromisoformat(date_of_death)

    if is_ceased and not is_test_date_before_date_of_death:
        return ResultRejectionReason.REMOVED_DEATH

    return None


def _participant_ceased_with_reason_no_cervix(decoded_record, participant_record, ceased_records):
    if not participant_record or not ceased_records:
        return None

    is_ceased = participant_record.get('is_ceased')
    ceased_reason = ceased_records[0].get('reason')
    cease_date = ceased_records[0].get('date_from')

    if not is_ceased or not ceased_reason:
        return None

    if is_ceased and (ceased_reason == CeaseReason.NO_CERVIX)\
            and parse(cease_date).date() < date.fromisoformat(decoded_record.get('test_date')):
        return ResultRejectionReason.CEASED_NO_CERVIX

    return None


def _get_participant_record_from_records(participants_and_results):
    for record in participants_and_results:
        if record['sort_key'] == ParticipantSortKey.PARTICIPANT:
            return record
    return None


def _get_ceased_records_from_records(participants_and_results):
    ceased_results = _extract_records(participants_and_results, ParticipantSortKey.CEASE)
    return sorted(ceased_results, key=lambda r: r['date_from'], reverse=True)


def calculate_and_update_ntdd(decoded_result, matched_participant, recall_type):
    # Do not update participant for type 9 results (no recall type) or if type 3 result
    # has recall type of 'Unchanged'
    if not recall_type or recall_type == UNCHANGED_RECALL_TYPE:
        return

    key = {
        'participant_id': matched_participant['participant_id'],
        'sort_key': 'PARTICIPANT'
    }

    next_test_due_date = calculate_ntdd(decoded_result)

    attributes_to_update = {
        'next_test_due_date': next_test_due_date.isoformat(),
        'status': recall_type
    }

    update_participant_record(key, attributes_to_update)

    return next_test_due_date


def generate_ntdd_transaction(
        decoded_result, matched_participant, recall_type, table_enum: TableNames):
    # do not update participant for type 9 results (no recall type) or if type 3 result
    # has recall type of 'Unchanged'
    if not recall_type or recall_type == UNCHANGED_RECALL_TYPE:
        return

    key = {
        'participant_id': matched_participant['participant_id'],
        'sort_key': 'PARTICIPANT'
    }

    next_test_due_date = calculate_ntdd(decoded_result)

    attributes_to_update = {
        'next_test_due_date': next_test_due_date.isoformat(),
        'status': recall_type
    }

    update = {
        'record_keys': key,
        'update_values': attributes_to_update
    }

    serialized_update = serialize_update(update)
    participant_update = build_update_record(serialized_update, table_enum)

    return participant_update, next_test_due_date


def _calculate_ntdd_from_recall_months(decoded_result):
    if decoded_result['action_code'] != RECALL_ACTION_CODE:
        return None
    if decoded_result.get('recall_months'):
        test_date = datetime.strptime(decoded_result['test_date'], '%Y-%m-%d').date()
        recall_months = int(decoded_result['recall_months'])
        return test_date + relativedelta(months=recall_months)
    return None


def _calculate_ntdd_from_result_data(decoded_result):
    test_date = datetime.strptime(decoded_result['test_date'], '%Y-%m-%d').date()
    date_of_birth = datetime.strptime(decoded_result['date_of_birth'], '%Y-%m-%d').date()
    next_test_due_dates = calculate_next_test_due_date_for_result(
        test_date,
        (decoded_result['result_code'], decoded_result['infection_code'], decoded_result['action_code']),
        date_of_birth,
        decoded_result.get('hpv_primary', True),
        decoded_result.get('self_sample', False)
    )
    calculated_ntdd = next_test_due_dates.get('default', {}).get('next_test_due_date')
    return calculated_ntdd


def _calculate_ntdd_from_fail_safe(decoded_result):
    action_code = decoded_result['action_code']
    if (action_code == RECALL_ACTION_CODE or action_code == SUSPEND_ACTION_CODE):
        test_date = datetime.strptime(decoded_result['test_date'], '%Y-%m-%d').date()
        return test_date + relativedelta(months=FAIL_SAFE_RECALL_MONTHS)
    return None


def calculate_ntdd(decoded_result):
    next_test_due_date = _calculate_ntdd_from_recall_months(decoded_result)
    if next_test_due_date:
        log({'log_reference': CommonLogReference.NTDDCALCULATION0001})
        return next_test_due_date
    next_test_due_date = _calculate_ntdd_from_result_data(decoded_result)
    if next_test_due_date:
        log({'log_reference': CommonLogReference.NTDDCALCULATION0002})
        return next_test_due_date
    next_test_due_date = _calculate_ntdd_from_fail_safe(decoded_result)
    if next_test_due_date:
        log({'log_reference': CommonLogReference.NTDDCALCULATION0003})
        return next_test_due_date
    log({'log_reference': CommonLogReference.NTDDCALCULATION0004})
    raise Exception('Unable to calculate NTDD')


def identify_duplicate_slide_numbers(record):
    result_id = record['result_id']
    sending_lab = record['sending_lab']
    slide_number = record['decoded'].get('slide_number', None)
    if slide_number is None:
        return record

    results_with_duplicate_slide_number = query_duplicate_results(
        result_id, sending_lab, slide_number
    )

    if not results_with_duplicate_slide_number or len(results_with_duplicate_slide_number) == 0:
        return record

    rejected_reason = 'Slide ref duplicate'
    for result in results_with_duplicate_slide_number:
        if (_is_exact_duplicate(result['decoded'], record['decoded'])):
            record = _link_duplicate_result(record, result)

            record['workflow_state'] = ResultWorkflowState.REJECTED
            record['rejected_reason'] = rejected_reason
        else:
            record = _link_duplicate_result(record, result)

    reject_result(record, rejected_reason, {'duplicates': record['duplicates']})

    return record


def _is_exact_duplicate(old_result, new_result):
    return (_is_demographics_duplicate(old_result, new_result)
            and _is_test_result_duplicate(old_result, new_result))


def _is_demographics_duplicate(old_result, new_result):
    return (old_result['first_name'] == new_result['first_name']
            and old_result['last_name'] == new_result['last_name']
            and old_result['address'] == new_result['address']
            and old_result['sanitised_postcode'] == new_result['sanitised_postcode']
            and old_result['date_of_birth'] == new_result['date_of_birth']
            and old_result['nhs_number'] == new_result['nhs_number'])


def _is_test_result_duplicate(old_result, new_result):
    return (old_result.get('action_code') == new_result.get('action_code')
            and old_result.get('infection_code') == new_result.get('infection_code')
            and old_result.get('result_code') == new_result.get('result_code'))


def _link_duplicate_result(record, old_result):
    if record.get('duplicates'):
        duplicate_result_ids = [result['result_id'] for result in record['duplicates']]
        if old_result['result_id'] not in duplicate_result_ids:
            record['duplicates'].append({
                'result_id': old_result['result_id'],
                'received_time': old_result['received_time']
            })
    else:
        record['duplicates'] = [{
            'result_id': old_result['result_id'],
            'received_time': old_result['received_time']
        }]
    return record


def _radiotherapy_check(decoded_record, participant_record, ceased_records):
    """
    CERSS-1647

    If a participant is ceased due to radiotherapy, the result should be
    rejected if the test date is after the date of ceasing
    """
    test_date = decoded_record.get('test_date')

    if not test_date:
        return None

    if not participant_record or not ceased_records:
        return None

    is_ceased = participant_record.get('is_ceased')
    ceased_reason = ceased_records[0].get('reason')
    ceased_from = ceased_records[0].get('date_from')
    if not is_ceased or ceased_reason != CeaseReason.RADIOTHERAPY:
        return None

    is_ceased_date_before_test_date = parse(ceased_from).date() < date.fromisoformat(test_date)

    if is_ceased_date_before_test_date:
        return ResultRejectionReason.CEASED_DUE_TO_RADIOTHERAPY

    return None


def _inactive_participant_check(decoded_record, participant_record):
    """
    CERSS-3074
    If a participant is inactive (active=False) and the reason for removal effective from
    date is before the test date, then reject the result with a reason of "INACTIVE" followed
    by the reason for removal code, separated by a an underscore.
    """

    if not participant_record:
        return None

    active = participant_record.get('active')
    test_date = decoded_record.get('test_date')
    reason_for_removal_code = participant_record.get('reason_for_removal_code')
    reason_for_removal_effective_from_date = participant_record.get('reason_for_removal_effective_from_date')

    # If the participant is active or the value doesn't exist and
    # either of the dates is missing, the participant is valid
    if (active is True or active is None) or not test_date or not reason_for_removal_effective_from_date:
        return None

    is_removal_date_before_test_date = \
        date.fromisoformat(reason_for_removal_effective_from_date) < date.fromisoformat(test_date)

    if not active and reason_for_removal_code in REASON_FOR_REMOVAL_CODES and is_removal_date_before_test_date:
        return f'INACTIVE_{reason_for_removal_code}'

    return None
