
from boto3.dynamodb.conditions import Key
from common.models.participant import GPNotificationStatus
from common.utils.dynamodb_access.segregated_operations import segregated_query
from common.utils.dynamodb_helper_utils import build_update_query, paginate
from common.utils.dynamodb_access.operations import dynamodb_update_item, dynamodb_query
from common.utils.dynamodb_access.table_names import TableNames

GP_ID_INDEX_NAME = 'live-record-status'


def query_for_participant_by_gp_id_and_gp_notification_live_record_status(gp_id, live_record_status):
    episode_status = GPNotificationStatus(live_record_status)
    condition_expression = (Key('live_record_status').eq(episode_status.value) &
                            Key('registered_gp_practice_code_and_test_due_date').begins_with(gp_id))
    query_response = segregated_query(TableNames.PARTICIPANTS, dict(
        IndexName=GP_ID_INDEX_NAME,
        KeyConditionExpression=condition_expression
    ))
    return query_response


def query_for_gp_notifications(expiry_date, callback_function):
    query_arguments = {
            'IndexName': 'live-record-status-only',
            'KeyConditionExpression': Key('live_record_status').eq(GPNotificationStatus.NEW_NOTIFICATION.value),
            'FilterExpression': Key('expires').lte(expiry_date)
    }
    paginated_query(query_arguments, callback_function)


def archive_gp_notification(record_key, string_timestamp):
    update_attributes = {
        'status': GPNotificationStatus.ARCHIVED_NOTIFICATION.value,
        'archived_date': string_timestamp
    }
    delete_attributes = [
        'live_record_status'
    ]
    update_expression, expression_names, expression_values = build_update_query(update_attributes,
                                                                                delete_attributes=delete_attributes)
    dynamodb_update_item(TableNames.PARTICIPANTS, dict(
            Key=record_key,
            UpdateExpression=update_expression,
            ExpressionAttributeValues=expression_values,
            ExpressionAttributeNames=expression_names,
            ReturnValues='NONE'
    ))


@paginate
def paginated_query(query_arguments, callback_function):
    response = dynamodb_query(TableNames.PARTICIPANTS, query_arguments, return_all=True)
    callback_function(response.get('Items'))
    return response
