import json
import hashlib
from common.log import update_logger_metadata_fields
from common.utils import validate_message_only_contains_one_record
from common.utils.participant_utils import (
    query_participants_by_nhs_number_all_fields,
    query_participant_records_by_source_hash,
    create_replace_record_in_participant_table,
    generate_participant_id,
    get_participant_and_test_results,
    update_participant_record
)
from common.utils.nhs_number_utils import sanitise_nhs_number


def get_message_contents_from_event(event):

    update_log_metadata_from_record_event(event)

    message_records = event.get('Records', [{}])
    validate_message_only_contains_one_record(message_records)

    message_record = message_records[0]
    message_id = message_record.get('messageId', 'NotProvided')
    message_body = json.loads(str(message_record.get('body')))

    record_line = message_body.get('line', '')

    if record_line == '':
        raise Exception('Record line is blank')

    return {
        'message_id': message_id,
        'record_line': record_line
    }


def get_message_body_from_event(event):
    message_records = event.get('Records', [{}])
    validate_message_only_contains_one_record(message_records)

    message_record = message_records[0]
    return json.loads(str(message_record.get('body')))


def get_message_uuid_from_event(event):
    message_records = event.get('Records', [{}])
    validate_message_only_contains_one_record(message_records)
    return message_records[0]['messageId']


def add_update_test_record(migrated_record):
    participants = query_participants_by_nhs_number_all_fields(migrated_record.get('nhs_number'))
    if participants:
        participant = participants[0]
        participant_id = participant['participant_id']

        participant_and_test_results = get_participant_and_test_results(participant_id)
        test_result = _get_test_result_record(participant_and_test_results, migrated_record)

        if test_result:
            del migrated_record['sort_key']

            key = {
                'participant_id': participant_id,
                'sort_key': test_result['sort_key']
            }
            update_participant_record(key, migrated_record)
        else:
            migrated_record['participant_id'] = participant_id
            create_replace_record_in_participant_table(migrated_record)

        return participant
    else:
        return None


def _get_test_result_record(participant_and_test_results, migrated_record):
    for result in participant_and_test_results:
        if 'RESULT' in result['sort_key'] and result['test_date'] == migrated_record['test_date']:
            return result


def update_participant_by_nhs_number(nhs_number: str, fields_to_add_or_replace: {} = None, fields_to_remove: [] = None):
    participants = query_participants_by_nhs_number_all_fields(nhs_number)

    if participants:
        participant = participants[0]

        if fields_to_add_or_replace:
            participant.update(fields_to_add_or_replace)

        if fields_to_remove:
            for field in fields_to_remove:
                participant.pop(field, None)
    else:
        participant = _create_new_participant_record(nhs_number, fields_to_add_or_replace)

    create_replace_record_in_participant_table(participant)
    return participant


def _create_new_participant_record(nhs_number: str, additional_fields: {} = None):
    participant_record = {
        'participant_id': generate_participant_id(),
        'sort_key': 'PARTICIPANT',
        'nhs_number': nhs_number,
        'sanitised_nhs_number': sanitise_nhs_number(nhs_number)
    }

    if additional_fields:
        participant_record.update(additional_fields)

    return participant_record


def remove_empty_values(mapped_data: dict) -> dict:
    return {k: remove_empty_values_decider(v) for k, v in mapped_data.items() if v not in ['', None, {}]}


def remove_empty_values_decider(data):
    if isinstance(data, dict):
        return remove_empty_values(data)
    return data


def add_hyphens_to_date(date_string):
    if date_string:
        return'{}-{}-{}'.format(date_string[0:4], date_string[4:6], date_string[6:])


def missing_necessary_fields(errors, necessary_fields):
    return any(['is a required value' in errors.get(field, {}).get('schema_error', '')
                for field in necessary_fields])


def update_log_metadata_from_record_event(event):
    metadata = {}

    event_records = event.get('Records', [{}])
    event_record = event_records[0]
    event_body = json.loads(event_record.get('body'))

    if 'line_index' in event_body:
        metadata['line_number'] = str(event_body['line_index'])

    if 'file_name' in event_body:
        metadata['file_name'] = event_body['file_name']

    if 'file_part_name' in event_body:
        metadata['file_part_name'] = event_body['file_part_name']

    update_logger_metadata_fields(metadata)


def get_hashed_value(original_value):
    return hashlib.sha512((original_value).encode('utf-8')).hexdigest()


def record_is_duplicate(record, comparison_fields):
    existing_records = query_participant_records_by_source_hash(record['source_hash'])
    if not existing_records:
        return False
    is_duplicate = False
    for existing_record in existing_records:
        matching_fields = map(lambda field: is_field_equal(existing_record, record, field), comparison_fields)
        if all(matching_fields):
            is_duplicate = True
    return is_duplicate


def is_field_equal(existing_record, new_record, field):
    if existing_record.get(field):
        return new_record[field] == existing_record[field]
    return not new_record[field]
