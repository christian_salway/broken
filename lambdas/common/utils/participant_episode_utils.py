from boto3.dynamodb.conditions import Key, Attr, And
from nose.tools import nottest

from common.models.participant import EpisodeStatus
from common.utils.dynamodb_access.operations import (
    dynamodb_put_item, dynamodb_query, dynamodb_get_item, dynamodb_update_item
)
from common.utils.dynamodb_access.segregated_operations import segregated_query, segregated_dynamodb_update_item
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.dynamodb_helper_utils import (
    build_projection_expression, build_filter_expression, build_update_query)

EPISODE_SORT_KEY = 'EPISODE#'
DEFER_SORT_KEY = 'DEFER#'
PROJECTION_EXPRESSION = 'test_due_date'
GP_ID_INDEX_NAME = 'live-record-status'
STATUS_TEST_DUE_DATE_INDEX_NAME = 'episode-test-due-date'


def create_replace_episode_record(episode):
    return dynamodb_put_item(TableNames.PARTICIPANTS, episode)


def update_episode_to_completion_state(participant_id, sort_key, result_date, result_id, auto_cease,
                                       completion_state):
    key = {
        'participant_id': participant_id,
        'sort_key': sort_key
    }

    attributes_to_update = {
        'result_date': result_date,
        'result_id': result_id
    }

    attributes_to_delete = []

    if not auto_cease:
        attributes_to_delete = ['live_record_status']
        attributes_to_update['status'] = completion_state

    update_expression, expression_names, expression_values = build_update_query(
        attributes_to_update,
        attributes_to_delete
    )

    return dynamodb_update_item(TableNames.PARTICIPANTS, dict(
        Key=key,
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ExpressionAttributeNames=expression_names,
        ReturnValues='NONE'
    ))


def update_to_nrl_closed_episode(participant_id, sort_key, closure_date):
    key = {
        'participant_id': participant_id,
        'sort_key': sort_key
    }

    attributes_to_update = {
        'status': EpisodeStatus.CLOSED,
        'closed_date': closure_date,
    }

    update_expression, expression_names, expression_values = build_update_query(
        attributes_to_update, ['live_record_status']
    )

    return dynamodb_update_item(TableNames.PARTICIPANTS, dict(
        Key=key,
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ExpressionAttributeNames=expression_names,
        ReturnValues='NONE'
    ))


def update_episode_with_condition_expression(
    key, attributes_to_update, condition_expression, attributes_to_delete=[], segregate=False
):
    update_expression, expression_names, expression_values = build_update_query(
        attributes_to_update, attributes_to_delete)

    update_item_query = segregated_dynamodb_update_item if segregate else dynamodb_update_item
    return update_item_query(TableNames.PARTICIPANTS, dict(
        Key=key,
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ConditionExpression=condition_expression,
        ExpressionAttributeNames=expression_names,
        ReturnValues='NONE'
    ))


def get_episode_record(participant_id, sort_key):
    key = {
        'participant_id': participant_id,
        'sort_key': sort_key
    }
    return dynamodb_get_item(TableNames.PARTICIPANTS, key)


def query_for_live_episode(participant_id, use_projection=True, additional_filters=None, segregate=False):
    if additional_filters is None:
        additional_filters = []
    projection_expression = [
        'live_record_status',
        'HMR101_form_data',
        'participant_id',
        'sort_key',
        'status',
        'user_id',
        'test_date',
        'test_due_date'
    ]
    filter_expression = [
        Attr('live_record_status').exists()
    ]
    if len(additional_filters) > 0:
        filter_expression.extend(additional_filters)

    condition_expression = Key('participant_id').eq(participant_id) & Key('sort_key').begins_with(EPISODE_SORT_KEY)
    parsed_projection_expression = build_projection_expression(projection_expression)
    query_function = segregated_query if segregate else dynamodb_query
    if use_projection:
        query_response = query_function(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=condition_expression,
            FilterExpression=build_filter_expression(filter_expression),
            ProjectionExpression=parsed_projection_expression['projection_expression'],
            ExpressionAttributeNames=parsed_projection_expression['expression_attribute_names']
        ))
    else:
        query_response = query_function(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=condition_expression,
            FilterExpression=build_filter_expression(filter_expression)
        ))
    return query_response


def query_episodes_by_participant_id(participant_id):
    condition_expression = Key('participant_id').eq(participant_id) & Key('sort_key').begins_with(EPISODE_SORT_KEY)
    return dynamodb_query(TableNames.PARTICIPANTS, {
        'KeyConditionExpression': condition_expression
    })


def query_defer_record_by_participant_id_and_date(participant_id, date_of_deferral):
    condition_expression = Key('participant_id').eq(participant_id) & \
                           Key('sort_key').eq(DEFER_SORT_KEY + date_of_deferral)
    return dynamodb_query(TableNames.PARTICIPANTS, {
        'KeyConditionExpression': condition_expression
    })


def query_defer_records_by_participant_id(participant_id):
    condition_expression = Key('participant_id').eq(participant_id) & Key('sort_key').begins_with(DEFER_SORT_KEY)
    return dynamodb_query(TableNames.PARTICIPANTS, {
        'KeyConditionExpression': condition_expression
    })


def query_for_participant_by_gp_id_and_episode_live_record_status(gp_id, live_record_status):
    episode_status = EpisodeStatus(live_record_status)
    condition_expression = (Key('live_record_status').eq(episode_status.value) &
                            Key('registered_gp_practice_code_and_test_due_date').begins_with(gp_id))
    return segregated_query(TableNames.PARTICIPANTS, dict(
        IndexName=GP_ID_INDEX_NAME,
        KeyConditionExpression=condition_expression
    ))


@nottest
def query_by_state_and_test_due_date(state, test_due_date, last_evaluated_key=None, required_fields=None,
                                     max_results=None):
    query_params = {'IndexName': STATUS_TEST_DUE_DATE_INDEX_NAME,
                    'KeyConditionExpression': And(Key('live_record_status').eq(state),
                                                  (Key('test_due_date').lte(test_due_date)))}
    if last_evaluated_key:
        query_params['ExclusiveStartKey'] = last_evaluated_key
    if required_fields:
        parsed_projection_expression = build_projection_expression(required_fields)
        query_params['ProjectionExpression'] = parsed_projection_expression['projection_expression']
        query_params['ExpressionAttributeNames'] = parsed_projection_expression['expression_attribute_names']
    if max_results:
        query_params['Limit'] = max_results
    return dynamodb_query(TableNames.PARTICIPANTS, query_params, return_all=True)


def episode_state_change(statuses, timestamp, state_change_function, required_fields=None):
    count = 0
    for status in statuses:
        result = query_by_state_and_test_due_date(status, timestamp, required_fields=required_fields)
        while True:
            count += state_change_function(result['Items'])
            if 'LastEvaluatedKey' not in result:
                break
            result = query_by_state_and_test_due_date(status, timestamp, required_fields=required_fields,
                                                      last_evaluated_key=result['LastEvaluatedKey'])
    return count


def query_for_non_responders(non_responders_threshold):
    condition_expression = And(
        Key('live_record_status').eq(EpisodeStatus.REMINDED.value), Key('test_due_date').lte(non_responders_threshold)
    )
    return dynamodb_query(TableNames.PARTICIPANTS, {
        'IndexName': STATUS_TEST_DUE_DATE_INDEX_NAME,
        'KeyConditionExpression': condition_expression
    })


def update_participant_episode_record(record_key, update_attributes: dict, delete_attributes=None, segregate=False):
    if delete_attributes is None:
        delete_attributes = []

    update_expression, expression_names, expression_values = build_update_query(update_attributes, delete_attributes)
    query_function = segregated_dynamodb_update_item if segregate else dynamodb_update_item
    return query_function(TableNames.PARTICIPANTS, dict(
        Key=record_key,
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ExpressionAttributeNames=expression_names,
        ReturnValues='NONE'
    ))


def update_live_episodes_status_to_logged(participant_id):
    live_episodes = query_for_live_episode(participant_id)
    if live_episodes:
        for live_episode in live_episodes:
            condition_expression = Key('sort_key').eq(live_episode['sort_key'])

            key = {
                'participant_id': participant_id,
                'sort_key': live_episode['sort_key']
            }

            attributes_to_update = {
                    'status': EpisodeStatus.LOGGED.value,
                    'live_record_status': EpisodeStatus.LOGGED.value,
                    'previous_status': live_episode['live_record_status']
            }
            update_episode_with_condition_expression(key, attributes_to_update, condition_expression)


def update_live_episodes_status_from_logged_to_previous(participant_id):
    live_episodes = query_for_live_episode(participant_id, use_projection=False)
    if live_episodes:
        for live_episode in live_episodes:
            condition_expression = Key('sort_key').eq(live_episode['sort_key'])

            key = {
                'participant_id': participant_id,
                'sort_key': live_episode['sort_key']
            }

            attributes_to_update = {
                    'status': live_episode['previous_status'],
                    'live_record_status': live_episode['previous_status'],
            }

            attributes_to_delete = ['previous_status']

            update_episode_with_condition_expression(
                key, attributes_to_update, condition_expression, attributes_to_delete
            )
