from time import time
import base64
import uuid
import json
import hashlib
import secrets

from boto3.dynamodb.conditions import Key, Attr

from common.utils.dynamodb_access.operations import (
    dynamodb_batch_delete_records, dynamodb_delete_item, dynamodb_put_item, dynamodb_query, dynamodb_update_item)

from common.log import log
from common.log_references import CommonLogReference
from common.constants import MAXIMUM_COOKIE_AND_SESSION_LENGTH_SECONDS, COOKIE_AND_SESSION_EXPIRY_REFRESH_SECONDS
from common.utils.dynamodb_helper_utils import build_update_query
from common.utils.dynamodb_access.table_names import TableNames


def create_session():
    session_token = str(uuid.uuid4())
    hashed_session_token = get_salted_and_hashed_value(session_token)
    session_id = str(uuid.uuid4())
    token = str(uuid.uuid4())
    current_time_in_seconds = int(time())
    expires_in = current_time_in_seconds + COOKIE_AND_SESSION_EXPIRY_REFRESH_SECONDS
    reauthenticate_in = current_time_in_seconds + MAXIMUM_COOKIE_AND_SESSION_LENGTH_SECONDS

    dynamodb_put_item(TableNames.SESSIONS, {
        'session_token': session_token,
        'hashed_session_token': hashed_session_token,
        'session_id': session_id,
        'nonce': token,
        'expires': expires_in,
        'reauthenticate_timestamp': reauthenticate_in,
        'is_current_session': True
    })

    return session_token, session_id, token, hashed_session_token


def get_session(session_token):

    log({'log_reference': CommonLogReference.SESSION0001})
    now = int(time())

    response = dynamodb_query(TableNames.SESSIONS, dict(
        KeyConditionExpression=Key('session_token').eq(session_token),
        FilterExpression=Attr('expires').gt(now)
    ))

    session = response[0] if len(response) > 0 else None

    if not session or _has_session_expired(session):
        log({'log_reference': CommonLogReference.SESSION0003})
        return None

    log({'log_reference': CommonLogReference.SESSION0002})

    return session


def _has_session_expired(session):
    now = time()

    if session['reauthenticate_timestamp'] < now or session['expires'] < now:
        return True

    return False


def get_salted_and_hashed_value(original_value):
    salt = secrets.token_bytes(32)
    return hashlib.sha512((original_value).encode('utf-8') + salt).hexdigest()


def get_session_from_lambda_event(event: dict):
    """ only use for lambdas where the event comes from an api gateway request via the authorizer """
    return json.loads(event['requestContext']['authorizer']['session'])


def update_session(session_token, update_attributes=None,
                   expiry_extension_seconds=COOKIE_AND_SESSION_EXPIRY_REFRESH_SECONDS,
                   extend_session=True, delete_attributes=[]):

    if update_attributes is None:
        update_attributes = {}

    log({'log_reference': CommonLogReference.SESSION0004})

    if extend_session:
        now = int(time())
        new_session_expiry = now + expiry_extension_seconds
        update_attributes['expires'] = new_session_expiry

    key = {'session_token': session_token}

    update_expression, expression_names, expression_values = build_update_query(update_attributes, delete_attributes)

    response = dynamodb_update_item(TableNames.SESSIONS, dict(
        Key=key,
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ExpressionAttributeNames=expression_names,
        ReturnValues='ALL_NEW',
    ))

    log({'log_reference': CommonLogReference.SESSION0005})

    return response['Attributes']


def delete_session(session_token):
    dynamodb_delete_item(TableNames.SESSIONS, {'session_token': session_token})
    log({'log_reference': CommonLogReference.SESSION0006})


def delete_sessions(session_tokens):
    sessions = [{'session_token': token} for token in session_tokens]
    dynamodb_batch_delete_records(TableNames.SESSIONS, sessions)


def get_sessions_by_nhsid_session_id(nhsid_session_id):
    return dynamodb_query(TableNames.SESSIONS, dict(
        IndexName='nhsid_session_id',
        KeyConditionExpression=Key('nhsid_session_id').eq(nhsid_session_id),
        ProjectionExpression='nhsid_session_id,user_data,session_token',
    ))


def get_sessions_by_nhsid_subject_id(nhsid_subject_id):
    return dynamodb_query(TableNames.SESSIONS, {
        "IndexName": 'nhsid_subject_id',
        "KeyConditionExpression": Key('nhsid_subject_id').eq(nhsid_subject_id),
        "ProjectionExpression": 'nhsid_subject_id,user_data,session_token',
    })


def valid_session_with_selected_role(session):
    return bool(session and session.get('user_data') and session.get('selected_role'))


def invalidate_any_duplicate_sessions(current_session_token, nhsid_useruid):
    log({'log_reference': CommonLogReference.SESSION0007})

    sessions = dynamodb_query(TableNames.SESSIONS, dict(
        IndexName='nhsid_useruid',
        KeyConditionExpression=Key('nhsid_useruid').eq(nhsid_useruid)
    ))
    duplicate_sessions = [x['session_token'] for x in sessions if x['session_token'] != current_session_token]

    if not duplicate_sessions:
        log({'log_reference': CommonLogReference.SESSION0008})
    else:
        log({'log_reference': CommonLogReference.SESSION0009, 'number_of_duplicates': len(duplicate_sessions)})

        for duplicate_session in duplicate_sessions:
            update_session(session_token=duplicate_session,
                           update_attributes={'is_current_session': False},
                           extend_session=False)

        log({'log_reference': CommonLogReference.SESSION0010})


def encode_state(state: dict) -> str:
    state_string = json.dumps(state)
    encoded_bytes = state_string.encode('utf-8')
    return base64.urlsafe_b64encode(encoded_bytes).decode()
