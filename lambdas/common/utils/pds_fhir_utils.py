import json
import os
import requests
import base64
import boto3
from common.utils.api_utils import format_query_string
from common.utils.cookie_management_utils import create_set_cookie_header
from common.utils.organisation_directory_utils import get_organisation_information_by_organisation_code
from common.utils.secrets_manager_utils import get_secret
from common.utils.session_utils import update_session
from common.utils.pds_record_utils import get_pds_record_fields_for_signature
from common.utils.data_segregation.workgroup_store import get_current_workgroups
from common.utils.data_segregation.nhais_ciphers import (
    ENGLISH_COHORT_CODES, SCOTTISH_COHORT_CODES, IM_COHORT_CODES, WELSH_COHORT_CODES
)
from datetime import datetime

from common.log import log
from common.log_references import CommonLogReference
from requests.exceptions import HTTPError, RequestException
from time import time
from typing import Dict, List
from uuid import uuid4


PDS_FHIR_GENDER_MAP = {
    'male': 1,
    'female': 2,
    'other': 9,
    'unknown': 0
}

COHORT_COUNTRY_MAP = {
    "England": ENGLISH_COHORT_CODES,
    "Scotland": SCOTTISH_COHORT_CODES,
    "Isle of Man": IM_COHORT_CODES,
    "Wales": WELSH_COHORT_CODES
}


PDS_FHIR_ADDRESS_PREFERENCE = ['home']
PDS_FHIR_RESTRICTED_CODES = ['R', 'V', 'REDACTED']
GRANT_REFRESH_TOKEN = 'refresh_token'
REQUEST_HEADERS = {'Content-Type': 'application/x-www-form-urlencoded'}
PDS_SIGNING_KEY = os.environ.get('PDS_SIGNING_KEY')


class PdsTokenException(Exception):
    pass


def get_pds_fhir_base_url():
    return os.environ.get('PDS_FHIR_BASE_URL')


def get_pds_fhir_auth_url():
    return os.environ.get('PDS_FHIR_AUTH_URL')


def get_pds_fhir_client_secret_id():
    return os.environ.get('PDS_FHIR_CLIENT_SECRET_ID')


def get_pds_fhir_callback_url():
    return os.environ.get('PDS_FHIR_CALLBACK_URL')


def get_dns_name():
    return os.environ.get('DNS_NAME')


def pds_fhir_access_token_valid(session: Dict) -> bool:
    if not session or not session.get('pds_access_token'):
        return False

    now = time()
    return session.get('pds_reauthenticate_timestamp', 0) >= now


def refresh_pds_fhir_access_token(session: Dict) -> str:
    if not session or not session.get('pds_refresh_token'):
        log({
            'log_reference': CommonLogReference.PDSFHIR0002
        })
        raise PdsTokenException()

    refresh_token = session['pds_refresh_token']
    refresh_url = f'{get_pds_fhir_auth_url()}/token'

    parameters = {
        **_generate_grant_body(GRANT_REFRESH_TOKEN),
        'refresh_token': refresh_token
    }

    try:
        response = requests.post(refresh_url, headers=REQUEST_HEADERS, parameters=parameters)
        response.raise_for_status()
    except HTTPError:
        log({
            'log_reference': CommonLogReference.PDSFHIR0004,
            'status_code': response.status_code,
            'error': response.json()
        })
        raise PdsTokenException()
    except RequestException as error:
        log({
            'log_reference': CommonLogReference.PDSFHIR0004,
            'error': str(error)
        })
        raise PdsTokenException()

    response_json = response.json()
    now = time()
    update_session(
        session['token'],
        {
            'pds_access_token': response_json['access_token'],
            'pds_access_token_expiry': now + int(response_json['expires_in']),
            'pds_refresh_token': response_json['refresh_token'],
            'pds_reauthenticate_timestamp': now + int(response_json['refresh_token_expires_in'])
        },
        delete_attributes=['hashed_session_token']
    )

    return response_json['access_token']


def get_pds_fhir_access_token(session: Dict) -> str:
    if not pds_fhir_access_token_valid(session):
        log({
            'log_reference': CommonLogReference.PDSFHIR0001
        })
        return refresh_pds_fhir_access_token(session)

    return session['pds_access_token']


def _generate_grant_body(grant_type: str) -> Dict:
    client_secrets = json.loads(get_secret(get_pds_fhir_client_secret_id(), secret_name_is_key=False))
    return {
        'client_id': client_secrets['client_id'],
        'client_secret': client_secrets['client_secret'],
        'grant_type': grant_type
    }


def _generate_authenticated_headers(session: dict) -> Dict:
    return {
        **REQUEST_HEADERS,
        'Authorization': f'Bearer {session["pds_access_token"]}',
        'X-Request-ID': str(uuid4())
    }


def search_pds_fhir_by_nhs_number(nhs_number: str, session: dict) -> List[Dict]:
    search_url = f'{get_pds_fhir_base_url()}/Patient/{nhs_number}'
    headers = _generate_authenticated_headers(session)

    try:
        response = requests.get(search_url, headers=headers)
        response.raise_for_status()
    except HTTPError:
        log({
            'log_reference': CommonLogReference.PDSFHIR0007,
            'status_code': response.status_code,
            'error': response.json(),
            'X-Request-ID': headers['X-Request-ID']
        })
        return []
    except RequestException as error:
        log({
            'log_reference': CommonLogReference.PDSFHIR0007,
            'error': str(error),
            'X-Request-ID': headers['X-Request-ID']
        })
        return []

    log({
        'log_reference': CommonLogReference.PDSFHIR0008,
        'X-Request-ID': headers['X-Request-ID']
    })
    return map_pds_fhir_patients([response.json()])


def get_search_parameters_for_demographics(demographics, fuzzy_match=False, max_results=None):
    search_params = [('_exact-match', 'false')]

    if demographics.get('first_name'):
        search_params.append(('given', demographics["first_name"] + "*" if not fuzzy_match else ""))
    if demographics.get('last_name'):
        search_params.append(('family', demographics["last_name"] + "*" if not fuzzy_match else ""))
    if demographics.get('date_of_birth'):
        search_params.append(('birthdate', f'eq{demographics["date_of_birth"]}'))
    if demographics.get('address', {}).get('postcode'):
        search_params.append(('address-postcode', demographics["address"]["postcode"]))

    return search_params


def search_pds_fhir_by_demographics(demographics, session,  max_results=None):

    search_params = get_search_parameters_for_demographics(demographics, max_results=max_results)
    search_url = f'{get_pds_fhir_base_url()}/Patient'

    headers = _generate_authenticated_headers(session)
    try:
        response = requests.get(search_url, headers=headers, params=search_params)
        response.raise_for_status()
    except HTTPError:
        pass
    except RequestException:
        pass

    response = response.json()

    if response['total'] > 0:
        pds_participants = [participant['resource'] for participant in response['entry']]

        return map_pds_fhir_patients(pds_participants)
    else:
        log({'log_reference': CommonLogReference.PDSFHIR0016})
        return []


def _map_pds_fhir_patient_name(fhir_patient: Dict) -> Dict:
    constructed_name = {}

    usual_name = [name for name in fhir_patient['name'] if name['use'] == 'usual'][0]
    given_names = usual_name.get('given')
    if given_names:
        constructed_name['first_name'] = given_names.pop(0)

    if given_names:
        constructed_name['middle_names'] = ' '.join(given_names)

    if usual_name.get('prefix'):
        constructed_name['title'] = usual_name['prefix'][0]

    constructed_name['last_name'] = usual_name['family']
    return constructed_name


def _map_pds_fhir_gender(gender: str) -> int:
    if gender in PDS_FHIR_GENDER_MAP:
        return PDS_FHIR_GENDER_MAP[gender]
    return 0


def _map_pds_fhir_dob(dob_raw: str) -> Dict:
    try:
        datetime.strptime(dob_raw, '%Y-%m-%d')
        return {'date_of_birth': dob_raw}
    except ValueError:
        log({'log_reference': CommonLogReference.PDSFHIR0011})
        return {}


def _map_pds_fhir_address(fhir_patient: Dict) -> Dict:
    addresses = fhir_patient['address']
    for address_type in PDS_FHIR_ADDRESS_PREFERENCE:
        type_addresses = [address for address in addresses if address['use'] == address_type]
        if type_addresses:
            current_address = type_addresses[0]
            mapped_address = {}

            for number, line in enumerate(current_address['line']):
                mapped_address[f'address_line_{number + 1}'] = line

            mapped_address['postcode'] = current_address.get('postalCode', '')
            return {'address': mapped_address}

    log({'log_reference': CommonLogReference.PDSFHIR0012})
    return {}


def _map_pds_fhir_gp(fhir_patient: Dict) -> Dict:
    if not fhir_patient['generalPractitioner']:
        log({'log_reference': CommonLogReference.PDSFHIR0013})
        return {}

    practice_code = fhir_patient['generalPractitioner'][0]['identifier']['value']
    practice = get_organisation_information_by_organisation_code(practice_code)
    if 'name' not in practice:
        log({
            'log_reference': CommonLogReference.PDSFHIR0013,
            'practice_code': practice_code
        })
        return{}

    return {
        'practice_code': practice_code,
        'practice_name': practice['name'],
        'practice_address': practice['address']
    }


def map_pds_fhir_patient(fhir_patient: Dict) -> Dict:
    if not fhir_patient:
        log({'log_reference': CommonLogReference.PDSFHIR0009})
        return {}

    if (
        fhir_patient['meta']['security']
        and fhir_patient['meta']['security'][0]['code'] in PDS_FHIR_RESTRICTED_CODES
    ):
        log({
            'log_reference': CommonLogReference.PDSFHIR0010,
            'security_level': fhir_patient['meta']['security'][0]['display']
        })
        return {}

    gp_details = _map_pds_fhir_gp(fhir_patient)
    if not can_workgroup_access_participant:
        return {}

    record = {
        'participant_id': f'pds-{uuid4()}',
        'nhs_number': fhir_patient['id'],
        'gender': _map_pds_fhir_gender(fhir_patient['gender']),
        'is_pds': True,
        **_map_pds_fhir_patient_name(fhir_patient),
        **_map_pds_fhir_dob(fhir_patient.get('birthDate', '')),
        **_map_pds_fhir_address(fhir_patient),
        **gp_details
    }

    timestamp = str(int(time()))
    record['pds_timestamp'] = str(timestamp)
    record['pds_signature'] = get_signature(record, PDS_SIGNING_KEY)
    return record


def map_pds_fhir_patients(fhir_patients: List[Dict]):
    converted = [map_pds_fhir_patient(fhir_patient) for fhir_patient in fhir_patients]
    return [converted_patient for converted_patient in converted if converted_patient]


def get_pds_fhir_initial_access_tokens(auth_code: str) -> Dict:
    client_secrets = json.loads(get_secret(get_pds_fhir_client_secret_id(), secret_name_is_key=False))
    params = {
        'grant_type': 'authorization_code',
        'client_id': client_secrets['client_id'],
        'client_secret': client_secrets['client_secret'],
        'redirect_uri': get_pds_fhir_callback_url(),
        'code': auth_code
    }

    log({'log_reference': CommonLogReference.PDSFHIR0014})
    url = f'{get_pds_fhir_auth_url()}/token'

    try:
        response = requests.post(url, headers=REQUEST_HEADERS, data=params)
        response.raise_for_status()
    except HTTPError:
        log({
            'log_reference': CommonLogReference.PDSFHIR0015,
            'status_code': response.status_code,
            'error': response.json()
        })
        raise PdsTokenException()
    except RequestException as error:
        log({
            'log_reference': CommonLogReference.PDSFHIR0015,
            'error': str(error)
        })
        raise PdsTokenException()

    return response.json()


def perform_pds_fhir_redirect_to_search(
    search_params: Dict,
    session_token: str,
    set_cookie: bool = False
) -> Dict:
    redirect_url = f'https://{get_dns_name()}/#/results?{format_query_string(search_params)}'
    response = {
        'statusCode': 302,
        'headers': {
            'Location': redirect_url
        }
    }

    if set_cookie:
        response['headers']['Set-Cookie'] = create_set_cookie_header(session_token, use_strict=False)

    return response


def get_signature(pds_record, secret):
    plaintext = bytes(str(get_pds_record_fields_for_signature(pds_record)), 'utf-8')
    encryption_context = {'nhs_number': pds_record['nhs_number']}

    encryption = boto3.client('kms').encrypt(
        KeyId=secret,
        Plaintext=plaintext,
        EncryptionContext=encryption_context
    )

    return base64.b64encode(encryption['CiphertextBlob']).decode('utf-8')


def can_workgroup_access_participant(gp_details):
    registered_gp_country = gp_details.get('practice_address', {}).get('address_line_5')
    if not registered_gp_country:
        return True
    elif registered_gp_country not in COHORT_COUNTRY_MAP.keys():
        log({'log_reference': CommonLogReference.PDSFHIR0017, 'registered_gp_country': registered_gp_country})
        return True

    current_workgroups = get_current_workgroups()
    can_access_countries = []

    for country, country_workgroups in COHORT_COUNTRY_MAP.items():

        if set(current_workgroups) & country_workgroups:
            can_access_countries.append(country)

    if not can_access_countries:
        log({'log_reference': CommonLogReference.PDSFHIR0018})
        return False
    elif registered_gp_country in can_access_countries:
        return True
    else:
        return False
