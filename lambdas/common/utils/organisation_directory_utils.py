import os
from datetime import datetime, timezone
from time import time
from typing import Any, Dict

import requests

from boto3.dynamodb.conditions import Key, Attr

from common.utils.dynamodb_access.table_names import TableNames
from common.utils.dynamodb_access.operations import dynamodb_put_item, dynamodb_query
from common.utils.transform_and_store_utils import remove_empty_values
from common.log import log
from common.log_references import CommonLogReference

ACTIVE_ORGANISATION_REFRESH_SECONDS = os.environ.get('ACTIVE_ORGANISATION_REFRESH_SECONDS')
INACTIVE_ORGANISATION_REFRESH_SECONDS = os.environ.get('INACTIVE_ORGANISATION_REFRESH_SECONDS')
ORGANISATION_DIRECTORY_SERVICE_URL = os.environ.get('ORGANISATION_DIRECTORY_SERVICE_URL')
ORGANISATION_ROLE_EXTENSION_URL = os.environ.get('ORGANISATION_ROLE_EXTENSION_URL')
ORGANISATION_DIRECTORY_SERVICE_CONNECTION_TIMEOUT = os.environ.get('ORGANISATION_DIRECTORY_SERVICE_CONNECTION_TIMEOUT')
DEFAULT_ORGANISATION_TYPE = 'UNKNOWN'
PRIMARY_ROLE_OBJECT = {'url': 'primaryRole', 'valueBoolean': True}
HOME_COUNTRIES_EXTERNAL = ['SCOTLAND', 'WALES', 'NORTHERN IRELAND']


def get_organisation_information_by_organisation_code(organisation_code: str) -> Dict[Any, Any]:
    current_time = datetime.now(timezone.utc).isoformat()
    current_time_in_seconds = int(time())
    cached_response = dynamodb_query(TableNames.ORGANISATIONS, {
        'KeyConditionExpression': Key('organisation_code').eq(organisation_code),
        'FilterExpression': Attr('expires').gt(current_time_in_seconds)
    })
    cached_organisation = cached_response[0] if cached_response else None

    if cached_organisation:
        log({'log_reference': CommonLogReference.ODS0002, 'organisation_code': organisation_code})
        organisation_data = convert_organisation_response_to_organisation_data(
            cached_organisation['data'], current_time)
        return organisation_data
    else:
        log({'log_reference': CommonLogReference.ODS0003, 'organisation_code': organisation_code})
        organisation_information = get_organisation_information_from_directory_service(organisation_code)

        if 'message' in organisation_information:
            return organisation_information

        create_new_organisation_record(organisation_code, organisation_information, True)
        organisation_data = convert_organisation_response_to_organisation_data(
            organisation_information, current_time)
        return organisation_data


def get_organisation_information_from_directory_service(organisation_code: str) -> Dict[Any, Any]:
    url = f'{ORGANISATION_DIRECTORY_SERVICE_URL}/{organisation_code}'
    timeout = int(ORGANISATION_DIRECTORY_SERVICE_CONNECTION_TIMEOUT)
    log({'log_reference': CommonLogReference.ODS0004})
    try:
        response = requests.get(url=url, timeout=timeout)
    except Exception:
        log({'log_reference': CommonLogReference.ODS0009})
        return {'organisation_code': organisation_code,
                'message': 'Request error'}

    if response.status_code == 200:
        log({'log_reference': CommonLogReference.ODS0005})
        response_body = response.json()
        return response_body
    elif response.status_code == 404:
        log({'log_reference': CommonLogReference.ODS0007, 'organisation_code': organisation_code})
        return {'organisation_code': organisation_code,
                'message': f'No organisation found with code {organisation_code}'}
    else:
        log({'log_reference': CommonLogReference.ODS0008})
        return {'organisation_code': organisation_code,
                'message': 'Error retrieving organisation details'}


def convert_organisation_response_to_organisation_data(
        response_body: Dict[Any, Any], current_time: str) -> Dict[Any, Any]:
    address = convert_address(response_body['address'])
    organisation_type = get_organisation_type(response_body)
    phone_number = get_phone_number(response_body)
    gp_practice_status = get_organisation_gp_practice_status(response_body)
    operational_periods = get_organisation_operational_periods(response_body)

    return {
        'organisation_code': response_body.get('id', response_body.get('organisation_code', '')),
        'active': response_body['active'],
        'address': address,
        'name': response_body['name'],
        'type': organisation_type,
        'phone_number': phone_number,
        'last_updated': current_time,
        'gp_practice_status': gp_practice_status,
        'operational_periods': operational_periods
    }


def convert_address(ods_address: Dict[Any, Any]) -> Dict[Any, Any]:
    address_lines_1_and_2 = ods_address.get('line', [])
    address_lines_length = len(address_lines_1_and_2)
    address = {
        'address_line_1': address_lines_1_and_2[0] if address_lines_length > 0 else None,
        'address_line_2': address_lines_1_and_2[1] if address_lines_length > 1 else None,
        'address_line_3': ods_address.get('city'),
        'address_line_4': ods_address.get('district'),
        'address_line_5': ods_address.get('country'),
        'postcode': ods_address.get('postalCode')
    }
    cleaned_address = remove_empty_values(address)
    return cleaned_address


def get_phone_number(body: Dict[Any, Any]) -> str:
    telecoms = body.get('telecom', [])
    phone_numbers = [telecom.get('value') for telecom in telecoms if telecom.get('system') == "phone"]
    if not phone_numbers:
        return ''

    return phone_numbers[0]


def get_organisation_type(response_body: Dict[Any, Any]) -> Dict[Any, Any]:
    extensions = response_body.get('extension', [])
    for extension in extensions:
        if extension['url'] == ORGANISATION_ROLE_EXTENSION_URL:
            role_extensions = extension['extension']
            if PRIMARY_ROLE_OBJECT in role_extensions:
                return next(role_extension['valueCoding']['display'] for role_extension in role_extensions
                            if role_extension['url'] == 'role')
    return DEFAULT_ORGANISATION_TYPE


def create_new_organisation_record(organisation_code, organisation_data, is_active):
    expires_refresh_seconds = ACTIVE_ORGANISATION_REFRESH_SECONDS if is_active \
        else INACTIVE_ORGANISATION_REFRESH_SECONDS
    current_time_in_seconds = int(time())
    expires = current_time_in_seconds + int(expires_refresh_seconds)
    organisation_record = {
        'organisation_code': organisation_code,
        'data': organisation_data,
        'expires': expires
    }
    log({'log_reference': CommonLogReference.ODS0006, 'expires': expires})
    dynamodb_put_item(TableNames.ORGANISATIONS, organisation_record)


def get_organisation_gp_practice_status(response_body) -> bool:
    """
    Valid organisations have an extension entry with role code 76,
    identifying a GP Practice
    """
    root_extensions = response_body.get('extension', [])

    for entry_extension in root_extensions:
        local_extension = entry_extension.get('extension', [])
        for extension in local_extension:
            if extension.get('url', "") == "role" \
                    and str(extension.get('valueCoding', {}).get('code', "")) == "76":
                return True

    return False


def get_organisation_operational_periods(response_body):
    """
    The organisation has a url for active periods, example URL:
    https://directory.spineservices.nhs.uk/STU3/Organization/A81003
    """

    # Get root extensions array
    root_extensions = response_body.get('extension', [])
    operational_periods = []

    # Get entries array in root_extensions array where .url contains ActivePeriod
    for entry_extension in root_extensions:
        is_active_period = entry_extension.get('url', '').find('ActivePeriod')

        if is_active_period <= 0:
            continue

        for extension_item in entry_extension.get('valuePeriod', {}).get('extension', []):
            if extension_item.get('valueString') == 'Operational':
                operational_period = {}
                start_date = entry_extension.get('valuePeriod', {}).get('start', None)
                end_date = entry_extension.get('valuePeriod', {}).get('end', None)

                if start_date:
                    operational_period['start'] = start_date
                if end_date:
                    operational_period['end'] = end_date

                operational_periods.append(operational_period)

    return operational_periods


def organisation_is_dms_valid(organisation_code: str) -> bool:
    organisation = get_organisation_information_by_organisation_code(organisation_code)
    valid = True

    if organisation:
        valid = organisation.get('address', {}).get('address_line_5') not in HOME_COUNTRIES_EXTERNAL

    log(
        {
            'log_reference': CommonLogReference.ODS0010,
            'organisation_code': organisation_code,
            'dms_valid': str(valid)
        }
    )
    return valid
