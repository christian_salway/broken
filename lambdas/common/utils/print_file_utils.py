from common.utils.dynamodb_helper_utils import build_update_query
from common.utils.dynamodb_access.operations import (
    dynamodb_get_item, dynamodb_put_item, dynamodb_update_item
)
from common.utils.dynamodb_access.table_names import TableNames
from common.exceptions import PrintFileMissingException
from functools import wraps

CAPITA_DATE_FORMAT = '%y%m%d%H%M%S'


def get_print_file_by_file_name(file_name, sort_key):
    return dynamodb_get_item(TableNames.PRINT_FILES, {
        'file_name': file_name,
        'sort_key': sort_key
    })


def create_print_file_record(record):
    dynamodb_put_item(TableNames.PRINT_FILES, record)


def lock_printfile(function):
    @wraps(function)
    def decorator(*args):
        if not args:
            return function(args)

        file_name, sort_key, _ = args
        print_file_row = get_print_file_by_file_name(file_name, sort_key)
        if not print_file_row:
            raise PrintFileMissingException

        if is_print_file_locked(print_file_row):
            raise Exception(f'Print file {file_name} is already locked')

        set_print_file_lock(file_name, sort_key, True)
        return function(*args)
    return decorator


def unlock_printfile(function):
    @wraps(function)
    def decorator(*args):
        if not args:
            return function(args)

        file_name, sort_key = args
        set_print_file_lock(file_name, sort_key, False)
        return function(*args)
    return decorator


def is_print_file_locked(print_file_row):
    return print_file_row['lock']


def set_print_file_lock(file_name, sort_key, lock):
    key = {
        'file_name': file_name,
        'sort_key': sort_key
    }
    attributes_to_update = {
        'lock': lock
    }
    update_expression, expression_names, expression_values = build_update_query(attributes_to_update)

    return dynamodb_update_item(TableNames.PRINT_FILES, dict(
        Key=key,
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ExpressionAttributeNames=expression_names,
        ReturnValues='NONE'
    ))


def update_print_file_by_file_name(file_name, sort_key, status):
    key = {
        'file_name': file_name,
        'sort_key': sort_key
    }

    attributes_to_update = {
        'status': status
    }

    update_expression, expression_names, expression_values = build_update_query(attributes_to_update)

    return dynamodb_update_item(TableNames.PRINT_FILES, dict(
        Key=key,
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ExpressionAttributeNames=expression_names,
        ReturnValues='NONE'
    ))
