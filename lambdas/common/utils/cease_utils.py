from typing import Dict
from dateutil.relativedelta import relativedelta
from datetime import datetime, timezone, date
from common.utils.participant_utils import (get_participant_and_test_results, update_participant_record,
                                            create_replace_record_in_participant_table)
from common.utils.participant_episode_utils import query_for_live_episode, update_participant_episode_record
from common.utils.result_codes import get_concatenated_result_code_combination, NORMAL_RESULT_CODE_COMBINATIONS
from common.utils.sqs_utils import send_new_message
from common.models.participant import CeaseReason, EpisodeStatus, ParticipantStatus
from common.models.letters import NotificationType
from common.log import log, get_internal_id
from common.log_references import CommonLogReference as LogReference
import os


# Radiotherapy is not on this list as no letter is sent in that case
cease_reason_to_template_map = {
    CeaseReason.NO_CERVIX: 'CXNC',
    CeaseReason.DUE_TO_AGE: 'CXAN',
    CeaseReason.PATIENT_INFORMED_CHOICE: 'CIC',
    CeaseReason.MENTAL_CAPACITY_ACT: 'CMC',
    CeaseReason.AUTOCEASE_DUE_TO_AGE: 'CUA'
}


CEASE_REASONS_NO_NOTIFICATION = [CeaseReason.RADIOTHERAPY, CeaseReason.MERGED]


# https://nhsd-confluence.digital.nhs.uk/x/qK-sCg
def participant_eligible_for_auto_cease(participant: Dict, next_test_due_date: date, segregate=False):
    # Do not auto cease if already ceased
    if participant.get('is_ceased', False):
        log({'log_reference': LogReference.CEASE0007, 'eligible_for_auto_cease': False, 'reason': 'Already ceased'})
        return False

    # Do not auto cease if inactive
    if not participant.get('active', True):
        log({'log_reference': LogReference.CEASE0007, 'eligible_for_auto_cease': False, 'reason': 'Inactive'})
        return False

    # Do not auto cease if under 65 at next test due date
    date_of_birth = datetime.strptime(participant['date_of_birth'], '%Y-%m-%d').date()
    if relativedelta(next_test_due_date, date_of_birth).years <= 65:
        log({'log_reference': LogReference.CEASE0007, 'eligible_for_auto_cease': False, 'reason': 'Under 65'})
        return False

    # Check results history
    participant_id = participant['participant_id']
    participant_table_records = get_participant_and_test_results(participant_id, segregate=segregate)

    results = sorted([
        record for record in participant_table_records if record['sort_key'].startswith('RESULT#')
    ], key=lambda record: record.get('test_date'), reverse=True)

    if not results:
        log({'log_reference': LogReference.CEASE0007, 'eligible_for_auto_cease': True, 'reason': 'No test results'})
        return True

    if results[0].get('action_code') != 'A':
        log({'log_reference': LogReference.CEASE0007, 'eligible_for_auto_cease': False,
             'reason': 'Last test action code not A'})
        return False

    # remove inadequate or non-NHS tests
    filtered_results = [record for record in results if not is_inadequate_result(record)]

    most_recent_result_outcomes = [get_concatenated_result_code_combination(result) for result in filtered_results[:3]]
    can_auto_cease = set(most_recent_result_outcomes).issubset(NORMAL_RESULT_CODE_COMBINATIONS)
    log({'log_reference': LogReference.CEASE0007, 'eligible_for_auto_cease': can_auto_cease,
         'reason': f"Last 3 tests contain{' no' if can_auto_cease else ''} abnormal results"})
    return can_auto_cease


def is_inadequate_result(result_record):
    return result_record.get('result_code') == '1' or result_record.get('action_code') == 'H'


def cease_participant_and_send_notification_messages(participant,
                                                     user_id,
                                                     event_body,
                                                     send_letter=True,
                                                     segregate=False):
    participant_id = participant['participant_id']
    cease_reason = event_body['reason']
    if cease_reason not in cease_reason_to_template_map and cease_reason not in CEASE_REASONS_NO_NOTIFICATION:
        log({
            'log_reference': LogReference.CEASEEX0001,
            'participant_id': participant_id,
            'cease_reason': cease_reason
        })
        raise Exception(LogReference.CEASEEX0001.message)

    handle_existing_episodes(participant_id, user_id, segregate=segregate)

    update_participant_record(
        record_key={'participant_id': participant_id,
                    'sort_key': 'PARTICIPANT'},
        update_attributes={'is_ceased': True, 'status': ParticipantStatus.CEASED},
        delete_attributes=['next_test_due_date'],
        segregate=segregate
    )

    log({'log_reference': LogReference.CEASE0001, 'participant_id': participant_id})

    cease_record = create_cease_record(event_body, user_id, participant_id, segregate=segregate)

    if participant.get('registered_gp_practice_code'):
        notification_message = {
            'participant_id': participant_id,
            'internal_id': get_internal_id(),
            'registered_gp_practice_code': participant['registered_gp_practice_code'],
            'notification_origin': EpisodeStatus.CEASED.value
        }
        send_new_message(os.environ.get('GP_NOTIFICATION_QUEUE_URL'), notification_message)

        log({'log_reference': LogReference.CEASE0002, 'participant_id': participant_id})

    if cease_reason not in CEASE_REASONS_NO_NOTIFICATION and send_letter:
        queue_cease_letter(participant_id, cease_reason)

    return cease_record


def queue_cease_letter(participant_id, cease_reason):
    message = {
        'participant_id': participant_id,
        'type': NotificationType.CEASE,
        'template': cease_reason_to_template_map.get(cease_reason)
    }

    send_new_message(os.environ.get('NOTIFICATION_QUEUE_URL'), message)
    log({'log_reference': LogReference.CEASE0003, 'participant_id': participant_id})


def handle_existing_episodes(participant_id, user_id, segregate):
    now = datetime.now(timezone.utc)
    live_episodes = query_for_live_episode(participant_id, segregate=segregate)

    if not live_episodes:
        log({'log_reference': LogReference.CEASE0004, 'participant_id': participant_id})
        return

    for episode in live_episodes:

        key = {
            'participant_id': participant_id,
            'sort_key': episode['sort_key']
        }

        update_participant_episode_record(
            record_key=key,
            update_attributes={
                'status': EpisodeStatus.CEASED.value,
                'ceased_date': now.date().isoformat(),
                'user_id': user_id,
            },
            delete_attributes=['live_record_status'],
            segregate=segregate
        )

        log({'log_reference': LogReference.CEASE0005, 'participant_id': participant_id,
             'previous_episode_status': episode['live_record_status']})


def create_cease_record(event_body, user_id, participant_id, segregate):
    now = datetime.now(timezone.utc)
    cease_record = {
        'participant_id': participant_id,
        'sort_key': f'CEASE#{now.date().isoformat()}',
        'reason': event_body['reason'],
        'user_id': user_id,
        'date_from': now.isoformat(),
    }
    if event_body.get('comments'):
        cease_record['comments'] = event_body['comments']
    if event_body.get('crm_number'):
        cease_record['crm_number'] = event_body['crm_number']
    create_replace_record_in_participant_table(cease_record, segregate=segregate)
    log({'log_reference': LogReference.CEASE0006, 'participant_id': participant_id})
    return cease_record
