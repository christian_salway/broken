from datetime import datetime, timezone
from uuid import uuid4
from boto3.dynamodb.conditions import Attr, Or, Key

from common.utils.dynamodb_access.operations import (
    dynamodb_scan, dynamodb_put_item, dynamodb_query
)
from common.utils.dynamodb_access.table_names import TableNames

EMAIL_PREFIX = 'email_'
PNL_EMAIL_PREFIX = 'email_PNL'
NRL_EMAIL_PREFIX = 'email_NRL'
NOT_REGISTERED_PREFIX = 'NOT_REGISTERED'


def query_for_pnl_nrl_email_records():
    return dynamodb_scan(TableNames.ORGANISATION_SUPPLEMENTAL, {
        "FilterExpression": Or(
            Attr('type_value').begins_with(PNL_EMAIL_PREFIX), Attr('type_value').begins_with(NRL_EMAIL_PREFIX)
        )
    })


def query_for_not_registered_records():
    return dynamodb_query(TableNames.ORGANISATION_SUPPLEMENTAL, {
        'IndexName': 'type_value',
        'KeyConditionExpression': Key('type_value').eq(NOT_REGISTERED_PREFIX)
    })


def add_last_viewed_by_record(first_name, last_name, organisation_code, print_type):
    timestamp = datetime.now(timezone.utc).isoformat()
    record_to_add = {
        'organisation_code': organisation_code,
        'type_value': f'{print_type}#{timestamp}#{first_name}.{last_name}',
        'organisation_supplemental_id': str(uuid4())
    }

    dynamodb_put_item(TableNames.ORGANISATION_SUPPLEMENTAL, record_to_add)


def _query_for_print_record(organisation_code, type_value_key):
    condition_expression = (
        Key('organisation_code').eq(organisation_code) & Key('type_value').begins_with(type_value_key)
    )
    notifications = dynamodb_query(TableNames.ORGANISATION_SUPPLEMENTAL, {
        'KeyConditionExpression': condition_expression,
        'ScanIndexForward': False
    })

    return notifications[0] if notifications else None


def get_print_pnl_record(organisation_code):
    return _query_for_print_record(organisation_code, 'PRINT_PNL')


def get_print_nrl_record(organisation_code):
    return _query_for_print_record(organisation_code, 'PRINT_NRL')


def get_print_review_record(organisation_code):
    return _query_for_print_record(organisation_code, 'PRINT_REVIEW')
