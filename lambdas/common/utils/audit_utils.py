import json
import os
from boto3 import client
from botocore.client import Config
from enum import Enum
from datetime import datetime, timezone
from common.log import log, get_internal_id
from common.log_references import CommonLogReference

# Rather than accessing this global directly use the get_sqs_client() function which aids in testability
_SQS_CLIENT = None


def get_sqs_client():
    global _SQS_CLIENT
    if not _SQS_CLIENT:
        _SQS_CLIENT = client('sqs',
                             config=Config(retries={'max_attempts': 3}),
                             endpoint_url='https://sqs.eu-west-2.amazonaws.com')
    return _SQS_CLIENT


class AuditActions(Enum):
    GET_PNL_BY_GP_PRACTICE_CODE = 'GET_PNL_BY_GP_PRACTICE_CODE'
    GET_NRL_BY_GP_PRACTICE_CODE = 'GET_NRL_BY_GP_PRACTICE_CODE'
    GET_GP_NOTIFICATIONS_BY_GP_PRACTICE_CODE = 'GET_GP_NOTIFICATIONS_BY_GP_PRACTICE_CODE'
    LOGIN = 'LOGIN'
    PDS_LOGIN = 'PDS_LOGIN'
    LOGOUT = 'LOGOUT'
    PRINT_HMR101 = 'PRINT_HMR101'
    SEARCH_PARTICIPANTS = 'SEARCH_PARTICIPANTS'
    SELECT_ROLE = 'SELECT_ROLE'
    SESSION_TIMEOUT = 'SESSION_TIMEOUT'
    BACKCHANNEL_LOGOUT = 'BACKCHANNEL_LOGOUT'
    STORE_CSO1C = 'STORE_CSO1C'
    STORE_CSO1R = 'STORE_CSO1R'
    STORE_CSO2 = 'STORE_CSO2'
    STORE_CSO3 = 'STORE_CSO3'
    STORE_CSO5 = 'STORE_CSO5'
    STORE_CSO8 = 'STORE_CSO8'
    VIEW_PARTICIPANT = 'VIEW_PARTICIPANT'
    INSUFFICIENT_PERMISSIONS = 'INSUFFICIENT_PERMISSIONS'
    ACCEPT_EPISODE = 'ACCEPT_EPISODE'
    NOACTION_EPISODE = 'NOACTION_EPISODE'
    CEASE_EPISODE = 'CEASE_EPISODE'
    NRL_EPISODE = 'NRL_EPISODE'
    CREATE_EPISODE = 'CREATE_EPISODE'
    COHORT_ADD = 'COHORT_ADD'
    COHORT_AMEND = 'COHORT_AMEND'
    COHORT_REMOVE = 'COHORT_REMOVE'
    RESULT_MATCHED = 'RESULT_MATCHED'
    DEFER_PARTICIPANT = 'DEFER_PARTICIPANT'
    ADD_GP_NRL_EMAIL_ADDRESS = 'ADD_GP_NRL_EMAIL_ADDRESS'
    REMOVE_GP_NRL_EMAIL_ADDRESS = 'REMOVE_GP_NRL_EMAIL_ADDRESS'
    ADD_GP_PNL_EMAIL_ADDRESS = 'ADD_GP_PNL_EMAIL_ADDRESS'
    REMOVE_GP_PNL_EMAIL_ADDRESS = 'REMOVE_GP_PNL_EMAIL_ADDRESS'
    CLOSE_NON_RESPONDER_EPISODE = 'CLOSE_NON_RESPONDER_EPISODE'
    REMIND_GP_PRACTICES = 'REMIND_GP_PRACTICES'
    REINSTATE_PARTICIPANT = 'REINSTATE_PARTICIPANT'
    GENERATE_PRINT_FILE = 'GENERATE_PRINT_FILE'
    VIEW_PARTICIPANT_AUDIT_HISTORY = 'VIEW_PARTICIPANT_AUDIT_HISTORY'
    VIEW_USER_AUDIT_HISTORY = 'VIEW_USER_AUDIT_HISTORY'
    SEND_EMAIL = 'SEND_EMAIL'
    FP69_SET = 'FP69_SET'
    FP69_UNSET = 'FP69_UNSET'
    MANUAL_ADD_PARTICIPANT = 'MANUAL_ADD_PARTICIPANT'
    SUPERSEDING = 'SUPERSEDING'
    # Notifications
    REMINDER_CREATED = 'REMINDER_CREATED'
    INVITE_CREATED = 'INVITE_CREATED'
    NOTIFICATION_CREATED = 'NOTIFICATION_CREATED'
    NOTIFICATION_IN_PRINT_FILE = 'NOTIFICATION_IN_PRINT_FILE'
    LETTER_SENT = 'LETTER_SENT'
    LETTER_NOT_SENT = 'LETTER_NOT_SENT'
    SEND_PRINT_FILE_SUCCESS = 'SEND_PRINT_FILE_SUCCESS'
    SEND_PRINT_FILE_FAILURE = 'SEND_PRINT_FILE_FAILURE'
    CONFIRMATION_REPORT_DOWNLOADED = 'CONFIRMATION_REPORT_DOWNLOADED'
    # Result management
    MANUAL_ADD_RESULT = 'MANUAL_ADD_RESULT'
    MANUAL_EDIT_RESULT = 'MANUAL_EDIT_RESULT'
    MANUAL_DELETE_RESULT = 'MANUAL_DELETE_RESULT'
    GET_UNMATCHED_RESULT = 'GET_UNMATCHED_RESULT'
    UPDATE_UNMATCHED_RESULT = 'UPDATE_UNMATCHED_RESULT'
    MANUAL_MATCH_RESULT = 'MANUAL_MATCH_RESULT'
    EVENT_TEXT_ADDED = 'EVENT_TEXT_ADDED'
    EVENT_TEXT_REMOVED = 'EVENT_TEXT_REMOVED'
    RESULT_LETTER_CANCELLED = 'RESULT_LETTER_CANCELLED'
    RESULT_LETTER_SUPPRESSED = 'RESULT_LETTER_SUPPRESSED'
    # GP notifications
    CREATE_GP_NOTIFICATION = 'CREATE_GP_NOTIFICATION'
    REVIEW_GP_NOTIFICATION = 'REVIEW_GP_NOTIFICATION'
    ARCHIVED_GP_NOTIFICATION = 'ARCHIVED_GP_NOTIFICATION'
    DMS_PRINT_FILE_DOWNLOADED = 'DMS_PRINT_FILE_DOWNLOADED'
    IOM_PRINT_FILE_DOWNLOADED = 'IOM_PRINT_FILE_DOWNLOADED'
    DEACTIVATE_PARTICIPANT = 'DEACTIVATE_PARTICIPANT'
    ACTIVATE_PARTICIPANT = 'ACTIVATE_PARTICIPANT'
    RESEND_NOTIFICATION = 'RESEND_NOTIFICATION'
    AUTOMATED_RESEND_NOTIFICATION = 'AUTOMATED_RESEND_NOTIFICATION'
    # Confirmation Reports
    DOWNLOAD_CONFIRMATION_REPORT = 'DOWNLOAD_CONFIRMATION_REPORT'
    DELETE_CONFIRMATION_REPORT = 'DELETE_CONFIRMATION_REPORT'
    # Demographics
    DECEASED_PARTICIPANT_MARKED_AS_ALIVE = 'DECEASED_PARTICIPANT_MARKED_AS_ALIVE'
    # Pause Participant
    PAUSE_PARTICIPANT = 'PAUSE_PARTICIPANT'
    UNPAUSE_PARTICIPANT = 'UNPAUSE_PARTICIPANT'
    # Cross border export
    EXPORT_WALES_PARTICIPANT = 'EXPORT_WALES_PARTICIPANT'
    EXPORT_WALES_RESULTS = 'EXPORT_WALES_RESULTS'


class AuditUsers(Enum):
    SYSTEM = 'SYSTEM'
    MIGRATION = 'MIGRATION'
    DEMOGRAPHICS = 'LOAD_DEMOGRAPHICS'
    MATCH_RESULT = 'MATCH_RESULT'
    NHS_IDENTITY = 'NHS_IDENTITY'
    LETTERS = "LETTERS"
    CROSSBORDER = 'CROSSBORDER'


AUTOMATED_PROCESS_SESSION_ID = 'NONE'


def audit(
    action,
    session=None,
    user=None,
    timestamp=None,
    participant_ids=None,
    nhs_numbers=None,
    gp_practice_code=None,
    organisation_code=None,
    email_address=None,
    file_name=None,
    host_name=None,
    additional_information=None
):
    """
    Parameters
    ----------
    additional_information : Dictionary of information for rendering in the audit log.
        This is supporting information for the audit context that is purely presentational.
        {
            "result_code": "X0A",
            "test_date": "11-01-2020"
        }
    """

    if action not in AuditActions.__members__.values():
        raise Exception(f'Requested action "{action}" is not a valid audit action.')

    if not has_valid_session_user_data(session, user):
        raise Exception('Session user data is not valid. Cannot audit the action.')

    audit_record = {
        'action': action.value,
        'timestamp': timestamp or datetime.now(timezone.utc).isoformat(),
        'internal_id': get_internal_id()
    }

    if user:
        audit_record['nhsid_useruid'] = user.value
        if participant_ids and user is AuditUsers.MIGRATION:
            audit_record['nhsid_useruid'] += f'_{participant_ids[0]}'

        audit_record['session_id'] = AUTOMATED_PROCESS_SESSION_ID
    else:
        audit_record['nhsid_useruid'] = session.get('user_data').get('nhsid_useruid')
        audit_record['session_id'] = session.get('session_id')
        audit_record['first_name'] = session.get('user_data').get('first_name')
        audit_record['last_name'] = session.get('user_data').get('last_name')
        selected_role = session.get('selected_role', {})
        if selected_role:
            audit_record['role_id'] = selected_role['role_id']
            audit_record['user_organisation_code'] = selected_role['organisation_code']

    if participant_ids:
        audit_record['participant_ids'] = participant_ids

    if nhs_numbers:
        audit_record['nhs_numbers'] = nhs_numbers

    if gp_practice_code:
        audit_record['gp_practice_code'] = gp_practice_code

    if organisation_code and email_address:
        audit_record['organisation_code'] = organisation_code
        audit_record['email_address'] = email_address

    if host_name:
        audit_record['host_name'] = host_name

    if file_name:
        audit_record['file_name'] = file_name

    if additional_information:
        audit_record['additional_information'] = additional_information
    AUDIT_QUEUE_URL = os.environ.get('AUDIT_QUEUE_URL')
    get_sqs_client().send_message(QueueUrl=AUDIT_QUEUE_URL, MessageBody=json.dumps(audit_record))

    log({'log_reference': CommonLogReference.AUDIT0001})


def has_valid_session_user_data(session, user):
    if user:
        if user not in AuditUsers.__members__.values():
            return False
    elif not session or not session.get('user_data', {}).get('nhsid_useruid'):
        return False
    return True
