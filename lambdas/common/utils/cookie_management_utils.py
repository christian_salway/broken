from http.cookies import SimpleCookie
from common.constants import MAXIMUM_COOKIE_AND_SESSION_LENGTH_SECONDS, COOKIE_AND_SESSION_EXPIRY_REFRESH_SECONDS

SESSION_COOKIE_NAME = 'sp_session_cookie'


def get_session_from_cookie(event_headers):

    cookie_header = event_headers.get('Cookie') or event_headers.get('cookie')

    session_cookie = SimpleCookie(cookie_header)
    session_cookie_morsel = session_cookie.get(SESSION_COOKIE_NAME)

    if not session_cookie_morsel:
        return None

    return session_cookie_morsel.value


def create_set_cookie_header(value, expiry_in_seconds=MAXIMUM_COOKIE_AND_SESSION_LENGTH_SECONDS, use_strict=True):

    cookie = SimpleCookie()
    cookie[SESSION_COOKIE_NAME] = value
    cookie[SESSION_COOKIE_NAME]['expires'] = expiry_in_seconds
    cookie[SESSION_COOKIE_NAME]['secure'] = True
    cookie[SESSION_COOKIE_NAME]['path'] = "/"
    if use_strict:
        cookie[SESSION_COOKIE_NAME]['samesite'] = "Strict"

    return cookie[SESSION_COOKIE_NAME].OutputString()


def delete_cookie_header():

    cookie = SimpleCookie()
    cookie[SESSION_COOKIE_NAME] = ''
    cookie[SESSION_COOKIE_NAME]['expires'] = -COOKIE_AND_SESSION_EXPIRY_REFRESH_SECONDS
    cookie[SESSION_COOKIE_NAME]['secure'] = True
    cookie[SESSION_COOKIE_NAME]['path'] = "/"

    return cookie[SESSION_COOKIE_NAME].OutputString()
