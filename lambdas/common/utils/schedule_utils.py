from datetime import datetime, timezone
import pytz
from cronex import CronExpression


def check_schedule(schedule_expression, local_time_zone_str):
    if not schedule_expression or not local_time_zone_str:
        return False
    local_timezone = pytz.timezone(local_time_zone_str)
    utc_now = datetime.now(timezone.utc)
    local_now = utc_now.astimezone(local_timezone)
    now_tuple = (local_now.year, local_now.month, local_now.day, local_now.hour, local_now.minute)
    cron_expression = CronExpression(schedule_expression)
    return cron_expression.check_trigger(now_tuple)
