import os
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

from common.log import get_internal_id, log
from common.log_references import CommonLogReference
from common.models.letters import NotificationType, SupressionReason
from common.utils.match_result_utils import get_latest_result_sorted_by_latest
from common.utils.participant_utils import get_participant_by_participant_id
from common.utils.result_codes import get_concatenated_result_code_combination
from common.utils.sqs_utils import send_new_message
from common.utils.audit_utils import audit, AuditActions, AuditUsers


def check_for_result_letter_suppression(participant_result):
    suppression_reason = check_for_result_letter_suppress_reason(participant_result)
    if suppression_reason:
        log({'log_reference': CommonLogReference.MATCHRLET0004,
             'participant id': participant_result['participant_id'],
             'result_id': participant_result.get('result_id'),
             'suppression_reason': suppression_reason})
        audit(
            action=AuditActions.RESULT_LETTER_SUPPRESSED,
            user=AuditUsers.SYSTEM,
            participant_ids=[participant_result['participant_id']],
            additional_information={
                'test_date': participant_result['test_date'],
                'suppression_reason': suppression_reason})
        return suppression_reason
    else:
        return None


def check_for_result_letter_suppress_reason(participant_result):
    reason = None
    sender_source_type = participant_result.get('sender_source_type')
    test_date = datetime.strptime(participant_result['test_date'], '%Y-%m-%d').date()
    if sender_source_type == '7':
        reason = SupressionReason.SOURCE_TYPE_7
    elif result_is_older_than_cut_off(test_date):
        reason = SupressionReason.OLD_TEST
    return reason


def result_is_older_than_cut_off(test_date: date):
    cut_off_months = os.environ.get('RESULT_LETTER_CUT_OFF_MONTHS')
    cut_off_months_int = int(cut_off_months) if cut_off_months and cut_off_months.isdigit() else None
    if not cut_off_months_int:
        log({'log_reference': CommonLogReference.CONFIG0003, 'cut_off_months': cut_off_months})
        raise ValueError(CommonLogReference.CONFIG0003.message)
    three_months_ago = date.today() - relativedelta(months=+cut_off_months_int)
    return test_date and test_date < three_months_ago


STANDARD_RESULT_CODE_COMBINATIONS = [
    #         result|infection|action
    'X0R',  # X      0         R
    'X0S',  # X      0         S
    'XUR',  # X      U         R
    'XUS',  # X      U         S
    'X9S',  # X      9         S
    '09R',  # 0      9         R
    '0QR',  # 0      Q         R
    '0QS',  # 0      Q         S
    '09S',  # 0      9         S
    '19R',  # 1      9         R
    '19S',  # 1      9         S
    '1QR',  # 1      Q         R
    '1QS',  # 1      Q         S
    '2QR',  # 2      Q         R
    '3QS',  # 3      Q         S
    '4QS',  # 4      Q         S
    '5QS',  # 5      Q         S
    '6QS',  # 6      Q         S
    '7QS',  # 7      Q         S
    '8QS',  # 8      Q         S
    '9QS',  # 9      Q         S
    '39S',  # 3      9         S
    '49S',  # 4      9         S
    '59S',  # 5      9         S
    '69S',  # 6      9         S
    '79S',  # 7      9         S
    '89S',  # 8      9         S
    '99S',  # 9      9         S
    '29R',  # 2      9         R
    '2QS',  # 2      Q         S
    '29S'   # 2      9         S
]


def add_result_letter_to_processing_queue(participant_result, was_autoceased):
    participant = get_participant_by_participant_id(participant_result['participant_id'])
    if not participant:
        return

    participant_id = participant.get('participant_id')
    result_id = participant_result['result_id']
    result_code_combo = get_concatenated_result_code_combination(participant_result)

    log({'log_reference': CommonLogReference.MATCHRLET0001, 'participant id': participant_id,
         'result code': result_code_combo, 'result_id': result_id})
    if not participant_id or not result_code_combo or not result_id:
        return
    if was_autoceased:
        _handle_cease_with_result(participant, participant_result, result_code_combo)
    elif result_code_combo in STANDARD_RESULT_CODE_COMBINATIONS:
        _handle_standard_result(participant, participant_result, result_code_combo)
    elif result_code_combo in SPECIAL_RESULT_CODE_COMBINATIONS:
        SPECIAL_RESULT_CODE_COMBINATIONS[result_code_combo](participant, participant_result)


def _handle_standard_result(participant, participant_result, result_code_combo):
    if not _is_self_sampled_result(participant_result):
        letter_template_code = f'P{result_code_combo}'
        _send_letter_with_template_code(participant, participant_result, letter_template_code)


def _handle_cease_with_result(participant, participant_result, result_code_combo):
    if result_code_combo == 'X0A':
        letter_template_code = 'K-0C' if _is_self_sampled_result(participant_result) else 'PX0C'

    _send_letter_with_template_code(participant, participant_result, letter_template_code)


def _is_self_sampled_result(participant_result):
    return participant_result.get('self_sample', False)


def _handle_special_case_X0A(participant, participant_result):
    if _is_self_sampled_result(participant_result):
        _send_letter_with_template_code(participant, participant_result, 'K-0A')
    else:
        _send_letter_with_template_code(participant, participant_result, 'PX0A')


def _handle_special_case_X9R(participant, participant_result):
    if _is_self_sampled_result(participant_result):
        _send_letter_with_template_code(participant, participant_result, 'K-9R')


def _handle_special_case_XUH(participant, participant_result):
    if _is_self_sampled_result(participant_result):
        results = get_latest_result_sorted_by_latest(participant['participant_id'])
        if results:
            letter_template_code = 'K2UH' if len(results) > 1 \
                and get_concatenated_result_code_combination(results[1]) == 'XUH' else 'K1UH'
            _send_letter_with_template_code(participant, participant_result, letter_template_code)


SPECIAL_RESULT_CODE_COMBINATIONS = {
    #                                   result|infection|action
    'X0A': _handle_special_case_X0A,  # X      0         A
    'X9R': _handle_special_case_X9R,  # X      9         R
    'XUH': _handle_special_case_XUH   # X      U         H
}


def _send_letter_with_template_code(participant, participant_result, letter_template_code):
    message = {
        'participant_id': participant['participant_id'],
        'sort_key': participant_result['sort_key'],
        'type': NotificationType.RESULT.value,
        'template': letter_template_code,
        'internal_id': get_internal_id(),
        'address': participant_result.get('letter_address')
    }
    log({'log_reference': CommonLogReference.MATCHRLET0002, 'letter code': letter_template_code})
    _send_notification_message(message)


def _send_notification_message(message):
    notification_url = os.environ['NOTIFICATION_QUEUE_URL']
    log({'log_reference': CommonLogReference.MATCHRLET0003, 'url': notification_url})
    send_new_message(notification_url, message)
