from datetime import datetime, timezone
from enum import Enum
from common.utils.edifact_utils import tokenize_edifact
from common.utils.sequence_utils import generate_interchange_number, generate_message_number


def generate_receipt_report(raw_edifact, received_time, is_success):
    tokenized_edifact = tokenize_edifact(raw_edifact)

    interchange_details = _get_interchange_details(tokenized_edifact)
    # sender is now original recipient (i.e. us)
    # recipient is original sender of incoming interchange
    sender = interchange_details['original_recipient']
    recipient = interchange_details['original_sender']
    recipient_code = get_recipient_id(recipient)

    current_datetime = datetime.now(timezone.utc)
    formatted_date = current_datetime.strftime('%Y%m%d')
    formatted_time = current_datetime.strftime('%H%M')
    formatted_datetime = f'{formatted_date+formatted_time}'

    interchange_reference = generate_interchange_number()
    outgoing_message_reference = generate_message_number()

    sender_party_id = 'LNW:819:GB2'  # This may need to change once CS/Interim solution/ICSCRS is assigned an identifier
    formatted_received_time = datetime.strptime(received_time, '%Y-%m-%d %H:%M:%S%z').strftime('%Y%m%d%H%M')

    receipt_report_segments = [
        f'UNB+UNOA:2+{sender}+{recipient}+{formatted_date}:{formatted_time}+{interchange_reference}++RECEP+++FHSA/PATH LABTRANSFER',  
        f'UNH+{outgoing_message_reference}+RECEP:0:2:FH',
        f'BGM++600+243:{formatted_datetime}:306+64',
        f'NHS+{sender_party_id}+{recipient_code}',
        f'DTM+815:{formatted_received_time}:306'
    ]

    interchange_status = EdifactInterchangeStatus.NO_VALID_DATA.value
    incoming_interchange_reference = interchange_details['reference']
    interchange_status_segment = f'RFF+RIS:{incoming_interchange_reference}'

    if is_success:
        message_receipt = _generate_message_receipt_segment(tokenized_edifact)

        if message_receipt:
            receipt_report_segments.append(message_receipt)
            interchange_status = EdifactInterchangeStatus.RECEIVED_SUCCESSFULLY.value
            interchange_status_segment += f' {interchange_status}:1'
        else:
            interchange_status_segment += f' {interchange_status}'
    else:
        interchange_status_segment += f' {interchange_status}'

    # segment_count is inclusive of message header and trailer
    # interchange header and trailer segments(UNB/UNZ) are not included in count
    segment_count = len(receipt_report_segments) + 1

    receipt_report_segments.extend([
        interchange_status_segment,
        f'UNT+{segment_count}+{outgoing_message_reference}',
        f'UNZ+1+{interchange_reference}'
    ])

    return "'".join(receipt_report_segments) + "'"


def _get_interchange_details(tokenized_edifact):
    interchange_header_segment = [segment for segment in tokenized_edifact if segment[0] == 'UNB'][0]

    return {
        'original_sender': interchange_header_segment[2],
        'original_recipient': interchange_header_segment[3],
        'reference': interchange_header_segment[5]
    }


def _generate_message_receipt_segment(tokenized_edifact):
    message_reference_numbers = [segment[1] for segment in tokenized_edifact if segment[0] == 'UNH']

    if message_reference_numbers and len(message_reference_numbers) == 1:
        return f'RFF+MIS:{message_reference_numbers[0]} CP'

    return None


def get_recipient_id(link_code):
    """
    Recipient id is the first part of the link code i.e. the non-numeric part
    and can be either 2 or 3 characters long
    """
    recipient_code = ''
    for i, char in enumerate(link_code):
        if char.isdigit():
            recipient_code = link_code[:i]
            break

    return recipient_code


class EdifactInterchangeStatus(Enum):
    RECEIVED_SUCCESSFULLY = 'OK'
    NO_VALID_DATA = 'NA'
