import json
import boto3
from botocore.client import Config
from common.utils import chunks
from common.log import get_internal_id

# Rather than accessing this global directly use the get_sqs_client() function which aids in testability
_SQS_CLIENT = None


def get_sqs_client():
    global _SQS_CLIENT
    if not _SQS_CLIENT:
        _SQS_CLIENT = boto3.client('sqs', config=Config(retries={'max_attempts': 3}))
    return _SQS_CLIENT


def send_new_message(queue_url, message):
    return get_sqs_client().send_message(
        QueueUrl=queue_url,
        MessageBody=json.dumps(message, indent=4, sort_keys=True, default=str))


def send_new_message_batch(queue_url, messages):
    return get_sqs_client().send_message_batch(
        QueueUrl=queue_url,
        Entries=messages)


def batch_send_messages(queue_url, messages, batch_size=10):
    failed = []

    for message in messages:
        if 'internal_id' not in message:
            message['internal_id'] = get_internal_id()

    for batch in chunks(messages, batch_size):
        messages = []
        for index, record in enumerate(batch):
            messages.append({
                'Id': str(index),
                'MessageBody': json.dumps(record)
            })

        result = send_new_message_batch(queue_url, messages)
        if result.get('Failed'):
            failed += messages
    return failed


def poll_for_messages(queue_url, max_messages=10, **kwargs):
    return get_sqs_client().receive_message(
        QueueUrl=queue_url,
        MaxNumberOfMessages=max_messages,
        **kwargs
    ).get('Messages', [])


def get_s3_data_from_queue_event(event):
    queue_message_body = event['Records'][0].get('body')
    if not queue_message_body:
        raise Exception('Missing body from queue message')
    queue_message = json.loads(queue_message_body)

    name = queue_message.get('name')
    bucket = queue_message.get('bucket')
    last_modified = queue_message.get('last_modified')
    if not name or not bucket or not last_modified:
        raise Exception('Queue message has missing parameters')

    return name, bucket, last_modified


def read_message_from_queue(event):
    queue_message_body = event['Records'][0].get('body')
    if not queue_message_body:
        raise Exception('Missing body from queue message')
    return queue_message_body
