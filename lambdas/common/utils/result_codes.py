# https://nhsd-confluence.digital.nhs.uk/pages/viewpage.action?spaceKey=CSP&title=Results+-+valid+result+and+action+code+combinations
from common.models.participant import ParticipantStatus


UNCHANGED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS = ['XUH']
INADEQUATE_CYTOLOGY_HPV_SELF_SAMPLE_ACTION_CODES_COMBINATIONS = ['X9R']
ROUTINE_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS = ['X0A']
REPEAT_ADVISED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS = ['X0R', '09R', '29R']
INADEQUATE_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS = ['XUR', '19R']
SUSPENDED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS = [
    'X0S', 'X9S', 'XUS', '09S', '19S', '29S', '39S', '49S', '59S', '69S', '79S', '89S', '99S'
]

VALID_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS = (
    ROUTINE_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS +
    REPEAT_ADVISED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS +
    INADEQUATE_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS +
    SUSPENDED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS
)

VALID_IF_SELF_SAMPLED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS = (
    UNCHANGED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS +
    INADEQUATE_CYTOLOGY_HPV_SELF_SAMPLE_ACTION_CODES_COMBINATIONS
)

RECALL_TYPES = {
    'Unchanged': UNCHANGED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS,
    ParticipantStatus.ROUTINE: ROUTINE_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS,
    ParticipantStatus.REPEAT_ADVISED: (REPEAT_ADVISED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS
                                       + INADEQUATE_CYTOLOGY_HPV_SELF_SAMPLE_ACTION_CODES_COMBINATIONS),
    ParticipantStatus.INADEQUATE: INADEQUATE_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS,
    ParticipantStatus.SUSPENDED: SUSPENDED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS
}

ABNORMAL_ACTION_CODE_COMBINATIONS_FOR_REINSTATE = ['3R', '3S', '4S', '5S', '6S', '7S', '8R', '8S', '9R', '9S',
                                                   '39S', '3QS', '40S', '49S', '4QS', '4US', '50S', '59S', '5QS',
                                                   '5US', '60S', '69S', '6QS', '6US', '70S', '79S', '7QS', '7US',
                                                   '89S', '8QS', '99S', '9QS', 'B9R', 'B9S', 'E9S', 'G9R', 'G9S',
                                                   'M9R', 'M9S', 'N9R', 'N9S', 'X9S', 'X9R']

UNCHANGED_ACTION_CODE_COMBINATIONS_FOR_REINSTATE = ['0H', '1H', '2H']

# https://nhsd-confluence.digital.nhs.uk/display/CS/Add+a+result+to+a+ceased+participant
NORMAL_RESULT_CODE_COMBINATIONS = ['0A', '0R', '0S', '09R', '09S', '0QR', '0QS', '2A', '2R', '2S', '29R', '29S', '2QR',
                                   '2QS', 'B0A', 'B0R', 'B0S', 'BUR', 'BUS', 'E0A', 'E0R', 'E0S', 'EUR', 'EUS', 'G0A',
                                   'G0R', 'G0S', 'GUR', 'GUS', 'M0A', 'M0R', 'M0S', 'MUR', 'MUS', 'N0A', 'N0R', 'N0S',
                                   'NUR', 'NUS', 'X0A', 'X0R', 'XUR', 'X0S', 'XUS']


def _ignore_none(val):
    return val if val is not None else ''


def get_concatenated_result_code_combination(result):
    cytology_result_code = _ignore_none(result.get('result_code', ''))
    hpv_result_code = _ignore_none(result.get('infection_code', ''))
    action_code = _ignore_none(result.get('action_code', ''))

    return f'{cytology_result_code}{hpv_result_code}{action_code}'
