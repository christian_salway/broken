from common.utils.dynamodb_access.table_names import TableNames
from common.utils.dynamodb_access.operations import dynamodb_get_item, dynamodb_put_item, dynamodb_update_item
from common.utils.dynamodb_helper_utils import build_update_query


def get_user_by_nhsid_useruid(nhsid_useruid):
    return dynamodb_get_item(TableNames.USERS, {
        'nhsid_useruid': nhsid_useruid
    })


def update_user(user):
    nhsid_useruid = user.get('nhsid_useruid')
    del user['nhsid_useruid']
    key = {'nhsid_useruid': nhsid_useruid}

    update_expression, expression_names, expression_values = build_update_query(user, [])

    return dynamodb_update_item(TableNames.USERS, dict(
        Key=key,
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ExpressionAttributeNames=expression_names,
        ReturnValues='NONE'
    ))


def create_user(user):
    return dynamodb_put_item(TableNames.USERS, {'nhsid_useruid': user.get('nhsid_useruid')})
