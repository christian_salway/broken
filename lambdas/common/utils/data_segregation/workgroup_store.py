from typing import List

from common.utils.data_segregation.nhais_ciphers import Cohorts


_CURRENT_WORKGROUPS = []


def set_current_workgroups(workgroups: List[Cohorts]):
    global _CURRENT_WORKGROUPS
    _CURRENT_WORKGROUPS = workgroups


def get_current_workgroups() -> List[Cohorts]:
    global _CURRENT_WORKGROUPS
    return _CURRENT_WORKGROUPS
