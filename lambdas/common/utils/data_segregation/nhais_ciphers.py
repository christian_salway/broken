from enum import Enum
from typing import List, Optional

from common.log import log
from common.log_references import CommonLogReference


ENGLISH_CIPHER_CODES = {
    'BAA', 'BD', 'BE', 'BIK', 'BIR', 'BRA', 'BRS', 'BU', 'CB', 'CH', 'COV', 'CR', 'CU', 'DCR',
    'DE', 'DN', 'DO', 'DR', 'DUD', 'EX', 'GAT', 'GG', 'GMK', 'HT', 'KC', 'KHU', 'LA', 'LD', 'LDS',
    'LIP', 'LL', 'LNA', 'LNB', 'LNC', 'LND', 'LNE', 'LNH', 'LNJ', 'LNK', 'LNL', 'LNM', 'LNN', 'LNP', 'LNR', 'LNS',
    'LNT', 'LNW', 'MAN', 'MID', 'NEW', 'NF', 'NN', 'NO', 'NR', 'OX', 'ROC', 'ROT', 'SA', 'SHE', 'SLF', 'SM', 'SO',
    'SOP', 'SPT', 'STL', 'ST', 'SU', 'SUN', 'SY', 'TE', 'TW', 'WA', 'WAK', 'WAL', 'WBH', 'WIG', 'WL', 'WMF', 'WOM',
    'WR', 'YN', 'YW'}

IM_CIPHER_CODES = {'IM'}

DMS_CIPHER_CODES = {'DMS'}

VALID_MIGRATION_CIPHER_CODES = ENGLISH_CIPHER_CODES.union(IM_CIPHER_CODES, DMS_CIPHER_CODES)

ENGLISH_COUNTRY_CODES = {'ENG', 'HJ'}

DMS_COUNTRY_CODES = {'DMS'}

IM_COUNTRY_CODES = {'IM'}

ALLOWED_COUNTRY_CODES = ENGLISH_COUNTRY_CODES.union(DMS_COUNTRY_CODES, IM_COUNTRY_CODES)

DEFAULT_COUNTRY_CODE = 'ENG'

WELSH_CIPHER_CODES = {'WGA', 'SGA', 'GWE', 'DYF', 'CLD'}

WELSH_COUNTRY_CODES = {'CYM'}

WELSH_COHORT_CODES = WELSH_CIPHER_CODES.union(WELSH_COUNTRY_CODES)

SCOTTISH_CIPHER_CODES = {'A', 'B', 'C', 'F', 'G', 'H', 'L', 'N', 'R', 'S', 'T', 'V', 'W', 'Y', 'Z'}

SCOTTISH_COUNTRY_CODES = {'SCT'}

SCOTTISH_COHORT_CODES = SCOTTISH_CIPHER_CODES.union(SCOTTISH_COUNTRY_CODES)

NORTHERN_IRELAND_COUNTRY_CODES = {'NI'}

RECOGNISED_HISTORIC_CIPHERS = {'BOL', 'BUY', 'GWY', 'HAL', 'IW', 'MGA', 'OLD', 'POS', 'SOS', 'TYN'}

RECOGNISED_CIPHER_CODES_NO_COHORT = SCOTTISH_CIPHER_CODES

RECOGNISED_COUNTRY_CODES_NO_COHORT = NORTHERN_IRELAND_COUNTRY_CODES.union(SCOTTISH_COUNTRY_CODES)

NO_COHORT_CODES = RECOGNISED_CIPHER_CODES_NO_COHORT.union(RECOGNISED_COUNTRY_CODES_NO_COHORT)

ENGLISH_COHORT_CODES = ENGLISH_CIPHER_CODES.union(ENGLISH_COUNTRY_CODES)
IM_COHORT_CODES = IM_CIPHER_CODES.union(IM_COUNTRY_CODES)
DMS_COHORT_CODES = DMS_CIPHER_CODES.union(DMS_COUNTRY_CODES)


ALL_CODES = ENGLISH_CIPHER_CODES.union(
    IM_CIPHER_CODES, DMS_CIPHER_CODES, ALLOWED_COUNTRY_CODES,
    RECOGNISED_CIPHER_CODES_NO_COHORT, RECOGNISED_COUNTRY_CODES_NO_COHORT,
    WELSH_COHORT_CODES
)


class Cohorts(str, Enum):
    ENGLISH = 'cervicalscreening'
    DMS = '13q_cervicalscreening'
    IOM = 'iom_cervicalscreening'
    WELSH = 'welsh_cervicalscreening'

    @staticmethod
    def list():
        return list(map(lambda c: c.value, Cohorts))

    def __str__(self):
        return self.value


def cohort_for_cipher(cipher: Optional[str]) -> Cohorts:
    if cipher in IM_COHORT_CODES:
        return Cohorts.IOM
    elif cipher in DMS_COHORT_CODES:
        return Cohorts.DMS
    elif cipher in WELSH_COHORT_CODES:
        return Cohorts.WELSH
    elif cipher in NO_COHORT_CODES:
        raise ValueError(
            f'The provided cipher of {cipher} is recognised but falls outside of the cohorts handled by CSMS'
        )
    return Cohorts.ENGLISH


def can_workgroup_access_cipher(workgroup: Cohorts, cipher: Optional[str]) -> bool:
    try:
        return workgroup == cohort_for_cipher(cipher)
    except ValueError:
        log({'log_reference': CommonLogReference.DATASEG0001, 'workgroup': workgroup, 'cipher': cipher})
        return False


def can_user_access_cipher(workgroups: List[Cohorts], cipher: Optional[str]) -> bool:
    return any([can_workgroup_access_cipher(workgroup, cipher) for workgroup in workgroups])
