from typing import Dict, List, Set
from common.models.participant import ParticipantSortKey
from common.utils.data_segregation.workgroup_store import get_current_workgroups
from common.utils.dynamodb_access.dynamodb_boto3 import get_dynamodb_resource
from common.utils.dynamodb_access.table_names import TableNames, get_dynamodb_table_name
from common.utils.data_segregation.nhais_ciphers import DEFAULT_COUNTRY_CODE, can_user_access_cipher
from common.log import log
from common.log_references import CommonLogReference as LogReference


def filter_items(table: TableNames, items: List[Dict]) -> List[Dict]:
    if table == TableNames.PARTICIPANTS:
        return _filter_participant_records(items)
    elif table == TableNames.RESULTS:
        return _filter_result_records(items)
    return items


def filter_before_write(table: TableNames, items: List[Dict]):
    if table == TableNames.PARTICIPANTS and items == _filter_participant_records(items):
        return
    elif table == TableNames.RESULTS and items == _filter_result_records(items):
        return
    raise ValueError('Attempting to write items which are not available to this user')


def _filter_participant_records(items: List[Dict]) -> List[Dict]:
    participant_ids_ciphers = {
        item['participant_id']: item.get('nhais_cipher', DEFAULT_COUNTRY_CODE)
        for item in items
        if 'nhais_cipher' in item and item['sort_key'] == ParticipantSortKey.PARTICIPANT
    }

    all_participant_ids = set(item['participant_id'] for item in items)
    unknown_participant_cohorts = all_participant_ids.difference(participant_ids_ciphers.keys())

    if unknown_participant_cohorts:
        additional_participant_ciphers = _nhais_ciphers_for_participant_ids(unknown_participant_cohorts)
        participant_ids_ciphers.update(additional_participant_ciphers)

    workgroups = get_current_workgroups()
    filtered_items = [
        item for item in items
        if can_user_access_cipher(workgroups, participant_ids_ciphers.get(item.get('participant_id', ''), ''))
    ]

    filtered_count = len(items) - len(filtered_items)
    if filtered_count > 0:
        log({'log_reference': LogReference.DATASEG0002, 'removed_items_count': filtered_count})
    return filtered_items


def _filter_result_records(items: List[Dict]) -> List[Dict]:
    workgroups = get_current_workgroups()
    filtered_items = [
        item for item in items
        if can_user_access_cipher(workgroups, item.get('sanitised_nhais_cipher', DEFAULT_COUNTRY_CODE))]
    filtered_count = len(items) - len(filtered_items)
    if filtered_count > 0:
        log({'log_reference': LogReference.DATASEG0003, 'removed_items_count': filtered_count})
    return filtered_items


def _nhais_ciphers_for_participant_ids(participant_ids: Set[str]) -> Dict[str, str]:
    table_name = get_dynamodb_table_name(TableNames.PARTICIPANTS)
    batch_keys = {
        table_name: {
            'Keys': [
                {
                    'participant_id': participant_id,
                    'sort_key': ParticipantSortKey.PARTICIPANT
                } for participant_id in participant_ids
            ],
            'ProjectionExpression': 'participant_id, nhais_cipher',
        }
    }
    response = get_dynamodb_resource().batch_get_item(RequestItems=batch_keys)
    ciphers_for_participants = {
        item['participant_id']: item.get('nhais_cipher', DEFAULT_COUNTRY_CODE)
        for item in response['Responses'][table_name]
    }
    return ciphers_for_participants
