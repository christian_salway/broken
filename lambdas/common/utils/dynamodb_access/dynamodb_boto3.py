from boto3 import resource, client
from botocore.client import Config
from common.utils.dynamodb_access.table_names import TableNames, get_dynamodb_table_name

_DYNAMODB_RESOURCE = None
_DYNAMODB_CLIENT = None
_DYNAMODB_TABLES = {}


def get_dynamodb_resource():
    global _DYNAMODB_RESOURCE
    if not _DYNAMODB_RESOURCE:
        _DYNAMODB_RESOURCE = resource('dynamodb', config=Config(retries={'max_attempts': 3}))
    return _DYNAMODB_RESOURCE


def get_dynamodb_client():
    global _DYNAMODB_CLIENT
    if not _DYNAMODB_CLIENT:
        _DYNAMODB_CLIENT = client('dynamodb', config=Config(retries={'max_attempts': 5, 'mode': 'standard'}))
    return _DYNAMODB_CLIENT


def get_dynamodb_table(table_enum: TableNames):
    table_name = get_dynamodb_table_name(table_enum)
    global _DYNAMODB_TABLES
    if table_name not in _DYNAMODB_TABLES.keys():
        _DYNAMODB_TABLES[table_name] = get_dynamodb_resource().Table(table_name)
    return _DYNAMODB_TABLES[table_name]
