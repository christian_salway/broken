from common.utils.dynamodb_access.dynamodb_boto3 import get_dynamodb_table


def check_dynamodb_table(table_name):
    table = get_dynamodb_table(table_name)
    table.key_schema
