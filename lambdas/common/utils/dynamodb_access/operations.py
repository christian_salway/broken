from typing import Any, Dict, List, Optional
from common.utils.dynamodb_access.dynamodb_boto3 import get_dynamodb_table, get_dynamodb_resource, get_dynamodb_client
from common.utils.dynamodb_access.table_names import TableNames


def dynamodb_put_item(
        table_enum: TableNames, record: Dict[str, Any], **kwargs):
    table = get_dynamodb_table(table_enum)
    return table.put_item(
        Item=record,
        **kwargs)


def dynamodb_query(table_enum: TableNames, query: Dict[str, Any], return_all: bool = False):
    table = get_dynamodb_table(table_enum)
    response = table.query(**query)

    return response if return_all else response.get('Items', [])


def dynamodb_paginated_query(table_enum: TableNames, query: Dict[str, Any]) -> List:
    table = get_dynamodb_table(table_enum)
    done = False
    start_key = None
    all_items = []
    while not done:
        if start_key:
            query['ExclusiveStartKey'] = start_key
        response = table.query(**query)
        all_items += response.get('Items', [])
        start_key = response.get('LastEvaluatedKey', None)
        done = start_key is None
    return all_items


def dynamodb_get_item(table_enum: TableNames, key: Dict[str, Any], kwargs: Optional[Dict[str, Any]] = None):
    if not kwargs:
        kwargs = {}
    table = get_dynamodb_table(table_enum)
    response = table.get_item(Key=key, **kwargs)

    return response.get('Item')


def dynamodb_update_item(table_enum: TableNames, update_query: Dict[str, Any]):
    table = get_dynamodb_table(table_enum)
    return table.update_item(**update_query)


def dynamodb_delete_item(table_enum: TableNames, key: Dict[str, Any]):
    table = get_dynamodb_table(table_enum)
    table.delete_item(Key=key)


def dynamodb_scan(table_enum: TableNames, scan_query: Dict[str, Any]):
    table = get_dynamodb_table(table_enum)
    response = table.scan(**scan_query)

    return response.get('Items', [])


def dynamodb_batch_write_records(table_enum: TableNames, records: List[Dict[str, Any]]):
    table = get_dynamodb_table(table_enum)
    with table.batch_writer() as batch_writer:
        for record in records:
            batch_writer.put_item(Item=record)


def dynamodb_batch_delete_records(table_enum: TableNames, records: List[Dict[str, Any]]):
    table = get_dynamodb_table(table_enum)
    with table.batch_writer() as batch_writer:
        for record in records:
            batch_writer.delete_item(Key=record)


def dynamodb_batch_get_items(batch_keys: Dict[str, Any]) -> Dict[str, Any]:
    return get_dynamodb_resource().batch_get_item(RequestItems=batch_keys)


def dynamodb_transact_write_items(transact_items):
    return get_dynamodb_client().transact_write_items(TransactItems=transact_items)


def list_dynamodb_tables():
    response = get_dynamodb_client().list_tables()
    table_names = response.get('TableNames', [])

    last_evaluated_table_name = response.get('LastEvaluatedTableName')
    while last_evaluated_table_name:
        response = get_dynamodb_client().list_tables(ExclusiveStartTableName=last_evaluated_table_name)
        table_names.extend(response.get('TableNames', []))
        last_evaluated_table_name = response.get('LastEvaluatedTableName')

    return table_names


def describe_dynamodb_table(table_name):
    response = get_dynamodb_client().describe_table(TableName=table_name)

    return response.get('Table', {})
