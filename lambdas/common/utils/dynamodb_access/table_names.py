from enum import Enum
import os
from typing import Union


class TableNames(str, Enum):
    AUDIT = 'DYNAMODB_AUDIT'
    LETTERS_CONFIG = 'DYNAMODB_LETTERS_CONFIG'
    ORGANISATIONS = 'DYNAMODB_ORGANISATIONS'
    ORGANISATION_SUPPLEMENTAL = 'DYNAMODB_ORGANISATION_SUPPLEMENTAL'
    PARTICIPANTS = 'DYNAMODB_PARTICIPANTS'
    PRINT_FILES = 'DYNAMODB_PRINT_FILES'
    REFERENCE = 'DYNAMODB_REFERENCE'
    REJECTED = 'DYNAMODB_REJECTED'
    RESULTS = 'DYNAMODB_RESULTS'
    SEQUENCES = 'DYNAMODB_SEQUENCES'
    SESSIONS = 'SESSION_TABLE_NAME'
    USERS = 'USERS_TABLE_NAME'
    PUBLIC_KEY_CACHE = 'PUBLIC_KEY_CACHE_TABLE_NAME'


def get_dynamodb_table_name(table_enum: TableNames) -> str:
    table_name = os.environ.get(table_enum.value)
    if not table_name:
        raise ValueError(f'Table name not available for {table_enum} table')
    return table_name


def get_table_enum_from_table_name(table_name: str) -> Union[TableNames, None]:
    for table_enum in TableNames:
        if table_name and table_name == os.environ.get(table_enum.value):
            return table_enum
    return None
