from typing import Any, Dict, List, Optional
from common.utils.data_segregation.data_segregation_filters import filter_before_write, filter_items
from common.utils.dynamodb_access.dynamodb_boto3 import get_dynamodb_table, get_dynamodb_resource, get_dynamodb_client
from common.utils.dynamodb_access.table_names import TableNames, get_dynamodb_table_name, get_table_enum_from_table_name
from boto3.dynamodb.types import TypeDeserializer


def segregated_query(table_enum: TableNames, query: Dict[str, Any], return_all: bool = False):
    table = get_dynamodb_table(table_enum)
    response = table.query(**query)

    items = response.get('Items', [])
    response['Items'] = filter_items(table_enum, items) if items else []

    return response if return_all else response.get('Items', [])


def segregated_put_item(table_enum: TableNames, record: Dict[str, Any], **kwargs):
    table = get_dynamodb_table(table_enum)
    filter_before_write(table_enum, [record])
    return table.put_item(
        Item=record,
        **kwargs)


def segregated_dynamodb_paginated_query(table_enum: TableNames, query: Dict[str, Any]) -> List:
    table = get_dynamodb_table(table_enum)
    done = False
    start_key = None
    all_items = []
    while not done:
        if start_key:
            query['ExclusiveStartKey'] = start_key
        response = table.query(**query)
        all_items += response.get('Items', [])
        start_key = response.get('LastEvaluatedKey', None)
        done = start_key is None
    filtered_items = filter_items(table_enum, all_items)
    return filtered_items


def segregated_dynamodb_get_item(table_enum: TableNames, key: Dict[str, Any], kwargs: Optional[Dict[str, Any]] = None):
    if not kwargs:
        kwargs = {}
    table = get_dynamodb_table(table_enum)
    response = table.get_item(Key=key, **kwargs)
    if not response.get('Item'):
        return None
    filtered = filter_items(table_enum, [response.get('Item')])

    return filtered[0] if filtered else None


def segregated_dynamodb_scan(table_enum: TableNames, scan_query: Dict[str, Any]):
    table = get_dynamodb_table(table_enum)
    response = table.scan(**scan_query)
    filtered = filter_items(table_enum, response.get('Items', []))

    return filtered


def segregated_dynamodb_paginated_scan(table_enum: TableNames, scan_query: Dict[str, Any]):
    table = get_dynamodb_table(table_enum)
    done = False
    start_key = None
    all_items = []
    while not done:
        if start_key:
            scan_query['ExclusiveStartKey'] = start_key
        response = table.scan(**scan_query)
        all_items += response.get('Items', [])
        start_key = response.get('LastEvaluatedKey', None)
        done = start_key is None
    filtered_items = filter_items(table_enum, all_items)

    return filtered_items


def segregated_dynamodb_delete_item(table_enum: TableNames, key: Dict[str, Any]):
    table = get_dynamodb_table(table_enum)
    filter_before_write(table_enum, [key])
    table.delete_item(Key=key)


def segregated_dynamodb_update_item(table_enum: TableNames, update_query: Dict[str, Any]):
    table = get_dynamodb_table(table_enum)
    filter_before_write(table_enum, [update_query['Key']])
    return table.update_item(**update_query)


def segregated_dynamodb_batch_write_records(table_enum: TableNames, records: List[Dict[str, Any]]):
    table = get_dynamodb_table(table_enum)
    filter_before_write(table_enum, records)
    with table.batch_writer() as batch_writer:
        for record in records:
            batch_writer.put_item(Item=record)


def segregated_dynamodb_batch_delete_records(table_enum: TableNames, records: List[Dict[str, Any]]):
    table = get_dynamodb_table(table_enum)
    filter_before_write(table_enum, records)
    with table.batch_writer() as batch_writer:
        for record in records:
            batch_writer.delete_item(Key=record)


def segregated_dynamodb_batch_get_items(table_enum: TableNames, query: Dict[str, Any]) -> List[Dict]:
    table_name = get_dynamodb_table_name(table_enum)

    batch_keys = {
        table_name: query
    }

    response = get_dynamodb_resource().batch_get_item(RequestItems=batch_keys)

    items = response['Responses'][table_name]

    return filter_items(table_enum, items)


def segregated_dynamodb_transact_write_items(transact_items):
    deserializer = TypeDeserializer()
    deserialized_records_by_table = {}
    for transact_item in transact_items:
        transact_operation = list(transact_item.keys())[0]
        item = transact_item[transact_operation]
        table_name = item['TableName']
        table_enum = get_table_enum_from_table_name(table_name)

        if table_enum:
            record_to_check = item.get('Item', item.get('Key'))
            deserialized_record = {k: deserializer.deserialize(v) for k, v in record_to_check.items()}
            deserialized_records_by_table.setdefault(table_enum, []).append(deserialized_record)

    for table_enum in deserialized_records_by_table.keys():
        filter_before_write(table_enum, deserialized_records_by_table[table_enum])

    return get_dynamodb_client().transact_write_items(TransactItems=transact_items)
