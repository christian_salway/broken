import json
import os
import re
from random import choice
from string import ascii_lowercase
from datetime import datetime, timezone
from functools import wraps
from typing import List
from common.log_references import CommonLogReference
from common.log import log, update_logger_metadata_fields
from common.internal_id import create_internal_id
from common.utils.s3_utils import get_object_meta_data


def exit_if_null(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            argument = args[1]
            if argument:
                return func(*args, **kwargs)
        except Exception:
            return None
    return wrapper
    """
    This decorator should be used if you do not want the function to continue
    if the input arguments are null or missing
    """


def set_internal_id(event):
    def _get_wrapped_event_from_sqs_event(sqs_event):
        try:
            wrapped_event = json.loads(sqs_event['Records'][0]['body'])
            if wrapped_event['Records'][0].get('eventSource'):
                return wrapped_event
            return None
        except Exception:
            return None

    if 'headers' in event:
        internal_id = event.get('headers', {}).get('request_id')
        if not internal_id:
            update_logger_metadata_fields({
                'internal_id': create_internal_id()
            })
            log(CommonLogReference.LAMBDA0004)
        else:
            update_logger_metadata_fields({
                'internal_id': internal_id
            })
    # when an event is triggered by s3 and goes on sqs first before reaching the lambda
    elif event.get('Records') and event['Records'][0].get('eventSource') == 'aws:sqs' and \
        (wrapped_event := _get_wrapped_event_from_sqs_event(event)) and \
            wrapped_event['Records'][0]['eventSource'] == 'aws:s3':

        bucket_name = wrapped_event['Records'][0]['s3']['bucket']['name']
        object_key = wrapped_event['Records'][0]['s3']['object']['key']
        file_meta_data = get_object_meta_data(bucket_name, object_key)
        internal_id = file_meta_data.get('internal_id')
        if not internal_id:
            update_logger_metadata_fields({
                'internal_id': create_internal_id()
            })
            log(CommonLogReference.LAMBDA0003)
        else:
            update_logger_metadata_fields({
                'internal_id': internal_id
            })

    elif event.get('Records') and event['Records'][0].get('eventSource') == 'aws:sqs':
        queue_message_body = json.loads(event['Records'][0].get('body', {}))
        internal_id = queue_message_body.get('internal_id')
        if not internal_id:
            update_logger_metadata_fields({
                'internal_id': create_internal_id()
            })
            log(CommonLogReference.LAMBDA0005)
        else:
            update_logger_metadata_fields({
                'internal_id': internal_id
            })
    elif 'internal_id' in event:
        update_logger_metadata_fields({
            'internal_id': event['internal_id']
        })
        log(CommonLogReference.LAMBDA0009)
    else:
        update_logger_metadata_fields({
            'internal_id': create_internal_id()
        })


# Demographics / migration
def validate_message_only_contains_one_record(message_records):
    records_length = len(message_records)

    if records_length != 1:
        error_message = f'Expected exactly 1 record but found {records_length} records'
        raise Exception(error_message)


# General helper functions
def chunk_list(list: List, number_per_chunk=10):
    return (list[i: i + number_per_chunk] for i in range(0, len(list), number_per_chunk))


def chunks(items, n):
    for i in range(0, len(items), n):
        yield items[i:i + n]


# For unit tests
class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data


def json_return_object(status_code, body=None):
    if body is None:
        body = {}
    return {
        'statusCode': status_code,
        'headers': {'Content-Type': 'application/json',
                    'X-Content-Type-Options': 'nosniff',
                    'Strict-Transport-Security': 'max-age=1576800'},
        'body': json.dumps(body),
        'isBase64Encoded': False
    }


def json_return_message(status_code, message):
    return json_return_object(status_code, {'message': message})


def json_return_data(status_code, data):
    return json_return_object(status_code, {'data': data})


def increment_date_by_years(date_string):
    pattern = re.compile('\\[Today(?:\\+)*(\\d)*\\]')
    matches = re.finditer(pattern, date_string)
    today = datetime.now(timezone.utc).date()
    for match in matches:
        if match:
            if match.group(1):
                today_plus_years = datetime(today.year + int(match.group(1)), today.month, today.day).date()
                date_string = date_string.replace(match.group(), today_plus_years.isoformat())
            else:
                date_string = date_string.replace(match.group(), today.isoformat())
    return json.loads(date_string)


def generate_random_length_string(length):
    return "".join(choice(ascii_lowercase) for i in range(length))


def load_json_doc(path, file_name):
    file_path = os.path.join(path, file_name)
    return json.load(open(file_path))


def _remove_empty_items_from_dictionary(current_dictionary):
    return {
        key: value
        for key, value in current_dictionary.items()
        if isinstance(value, bool) or isinstance(value, int) or value
    }


def generate_crm_list(crm_values, session, existing_crm=None):
    if not existing_crm:
        existing_crm = []

    new_crm = existing_crm.copy()
    crm_number = crm_values.get('crm_number', '')
    comments = crm_values.get('comments', '')

    first_name = session.get('user_data').get('first_name')
    last_name = session.get('user_data').get('last_name')

    new_crm.append({
        'crm_number': crm_number,
        'comments': comments,
        'first_name': first_name,
        'last_name': last_name,
        'timestamp': datetime.now(timezone.utc).isoformat(timespec='microseconds')
    })

    return new_crm
