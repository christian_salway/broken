class InvalidEdifactHeader(Exception):
    def __init__(self, message='Invalid edifact header'):
        super().__init__(message)
