import re

from common.utils.postcode_utils import is_valid_postcode, sanitise_postcode

BS7666_ADDRESS_LINE_LENGTH = 35


def parse_address_from_decoded_string(address: str):
    """
    Attempt to create an address object from the NHAIS address string.

    See:
      - CERSS-1882
      - Functional design Table A1 - Split and decode results files: https://nhsd-confluence.digital.nhs.uk/x/xAkJCQ
    """
    # Split on comma, or space
    address_list = re.split(',| ', address)

    # Remove empty strings created by double spaces, or comma-space combos
    sanitized_address_list = [item for item in address_list if item]

    # Address template is pre-populated with all fields as blank
    address_template = {
        'address_line_1': "",
        'address_line_2': "",
        'address_line_3': "",
        'address_line_4': "",
        'address_line_5': "",
        'postcode': ""
    }

    if len(sanitized_address_list) < 1:
        return None

    # Remove the postcode from the list
    sanitised_postcode, address_list_without_postcode = destructure_postcode_from_address_list(sanitized_address_list)

    for key in address_template:
        temp_line_string = ""

        # If there are remaining items, and space separated concatenation will
        # not be greater than BS7666 compliant max line length
        while len(address_list_without_postcode) > 0 and \
                (len(temp_line_string) + len(address_list_without_postcode[0])) < BS7666_ADDRESS_LINE_LENGTH:

            # Assign and remove the last item from the address list
            if temp_line_string == "":
                temp_line_string += address_list_without_postcode.pop(0)
            else:
                temp_line_string += f' {address_list_without_postcode.pop(0)}'

        # If we added any items assign the string
        if len(temp_line_string) > 0:
            address_template[key] = temp_line_string

    if sanitised_postcode:
        address_template['postcode'] = sanitised_postcode

    return address_template


def destructure_postcode_from_address_list(address_list):
    """
    Given a list of strings, determine if either of the last two entries
    are a valid postcode.

    Return the sanitized postcode, and the remaining address list items.
    """
    sanitised_postcode = None
    address_list_without_postcode = []

    # postcodes may or may not have a space so check last element alone first
    # if that fails check last two address elements together
    last_element_of_address = sanitise_postcode(address_list[-1])

    # Last item is a single string postcode
    if is_valid_postcode(last_element_of_address):
        sanitised_postcode = last_element_of_address
        address_list_without_postcode = address_list[:-1]

    # If there is more than one item, and the first item was not a valid postcode
    elif len(address_list) > 1:
        second_to_last_element_of_address = sanitise_postcode(address_list[-2])
        full_postcode = second_to_last_element_of_address + last_element_of_address

        if is_valid_postcode(full_postcode):
            sanitised_postcode = (full_postcode)
            address_list_without_postcode = address_list[:-2]

    # if there was no postcode, all fields are in the remainder
    if sanitised_postcode is None:
        address_list_without_postcode = address_list

    return sanitised_postcode, address_list_without_postcode
