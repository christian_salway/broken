from unittest import TestCase
from unittest.mock import patch


class PatchTestCase(TestCase):

    def create_patchers(self, module, patchers):
        for patcher in patchers:
            is_list = type(patcher) is list
            name = patcher[0] if is_list else patcher
            patcher_name = f'{name}_patcher'
            setattr(PatchTestCase, patcher_name, patch(f'{module}.{name}').start())

            if is_list:
                getattr(self, patcher_name).return_value = patcher[1]
