import json
from typing import Dict


def mock_api_event():
    return {
        'requestContext': {
            'authorizer': {
                'principalId': 'session_id123456'
            }
        },
        'headers': {
            'request_id': 'requestId'
        }
    }


def sqs_mock_event(body: Dict):
    return {
        'Records': [
            {
                'messageId': 'messageId',
                'body': json.dumps(body),
                'eventSource': 'aws:sqs'
            }
        ]
    }


def sns_mock_event(sns_message: Dict):
    return {
        'Records': [
            {
                'messageId': 'messageId',
                'eventSource': 'aws:sns',
                'sns': sns_message
            }
        ]
    }


def s3_mock_event(s3_object: Dict):
    return {
        'Records': [
            {
                'messageId': 'messageId',
                'eventSource': 'aws:s3',
                's3': s3_object
            }
        ]
    }


def cloudwatch_mock_event():
    return {
        'account': 'account-id',
        'region': 'us-east-2',
        'detail': {},
        'detail-type': 'Scheduled Event',
        'source': 'aws.events',
        'time': '2019-03-01T01:23:45Z',
        'id': 'cdc73f9d-aea9-11e3-9d5a-835b769c0d9c',
        'resources': [
            'arn:aws:events:us-east-1:account-id:rule/my-schedule'
        ]
    }
