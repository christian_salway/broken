import json


def assertApiResponse(self, status_code, expected_body, actual_response):
    self.assertEqual({
        'body': json.dumps(expected_body),
        'headers': {
            'Content-Type': 'application/json',
            'Strict-Transport-Security': 'max-age=1576800',
            'X-Content-Type-Options': 'nosniff'
        },
        'isBase64Encoded': False,
        'statusCode': status_code
    }, actual_response)
