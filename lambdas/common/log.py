import logging
import json
from datetime import datetime
import os
from common.log_references import BaseLogReference, CommonLogReference


logger = None
DELIMITER = ' | '
LOG_RECORD_META_DATA = {}


class KeyValueFormatter(logging.Formatter):
    def formatTime(self, record, date_format=None):
        created = datetime.fromtimestamp(record.created)
        time = created.strftime(date_format) if date_format else created.isoformat()
        return time.strip()

    def usesTime(self):
        return True

    def _serialize_message(self, message):
        def is_dictionary(string):
            try:
                return json.loads(string)
            except Exception:
                return False

        if is_dictionary(message):
            message_dict = json.loads(message)
            return DELIMITER.join(
                f'{k}={json.dumps(v)}' for k, v in message_dict.items()
            )
        if isinstance(message, str):
            return f'message={json.dumps(message)}'
        return str(message)

    def formatMessage(self, record):
        record_fields = [f'time={record.asctime}', f'level={record.levelname}']\
            + [f'{k}={v}' for k, v in LOG_RECORD_META_DATA.items()]\
            + [self._serialize_message(record.message)]
        if record.exc_info:
            record_fields += [f'exc_info={record.exc_info}']
        return DELIMITER.join(record_fields)

    def format(self, record):
        formatted = super(KeyValueFormatter, self).format(record)
        return formatted.replace('\n', '\r')


def initialize_logger(lambda_context):

    LOG_RECORD_META_DATA.clear()

    LOG_RECORD_META_DATA['environment_name'] = os.environ.get('ENVIRONMENT_NAME')
    LOG_RECORD_META_DATA['aws_account_id'] = os.environ.get('AWS_ACCOUNT_ID')
    LOG_RECORD_META_DATA['function_name'] = getattr(lambda_context, 'function_name', None)
    LOG_RECORD_META_DATA['aws_request_id'] = getattr(lambda_context, 'aws_request_id', None)

    global logger
    logger = logging.getLogger(lambda_context.function_name)

    formatter = KeyValueFormatter()

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger.handlers = []
    logger.propagate = False
    logger.addHandler(handler)
    logger.setLevel(os.environ.get('LOG_LEVEL', logging.INFO))

    logger.info(f'Logger initialized at {logging.getLevelName(logger.level)}')


def log(log_record):
    if isinstance(log_record, BaseLogReference):
        new_log_record = {
            'log_reference': log_record.name,
            'message': log_record.message,
        }
        is_exception = log_record is CommonLogReference.EXCEPTION000
        logger.log(log_record.level, json.dumps(new_log_record), exc_info=is_exception)
    elif isinstance(log_record, dict):
        log_reference = log_record.get('log_reference')
        updated_log_record = log_record
        # to log the full stack trace of an exception use either
        # log(CommonLogReference.EXCEPTION000) or
        # log({'log_reference': any_log_reference, 'add_exception_info': True})
        is_exception = log_record.get('add_exception_info', log_reference is CommonLogReference.EXCEPTION000)
        updated_log_record['message'] = log_reference.message
        updated_log_record['log_reference'] = log_reference.name
        logger.log(
            log_reference.level, json.dumps(updated_log_record), exc_info=is_exception
        )

    else:
        raise Exception(f'Unsupported {type(log_record)} type for the log record')


def get_internal_id():
    return LOG_RECORD_META_DATA['internal_id']


def update_logger_metadata_fields(fields):
    LOG_RECORD_META_DATA.update(fields)
