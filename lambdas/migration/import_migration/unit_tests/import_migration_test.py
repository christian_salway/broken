from unittest import TestCase
from mock import patch, MagicMock, Mock
from common.log_references import CommonLogReference
from common.test_mocks.mock_events import sqs_mock_event
from ddt import ddt, data, unpack

mock_environ = {
    'COMPLETED_BUCKET': 'completed bucket',
    'FAILED_BUCKET': 'failed bucket',
    'ENVIRONMENT_NAME': 'TEST123',
    'IMPORT_CSO1C_ROWS_QUEUE_URL': 'CSO1C_url',
    'IMPORT_CSO1R_ROWS_QUEUE_URL': 'CSO1R_url',
    'IMPORT_CSO2_ROWS_QUEUE_URL': 'CSO2_url',
    'IMPORT_CSO3_ROWS_QUEUE_URL': 'CSO3_url',
    'IMPORT_CSO5_ROWS_QUEUE_URL': 'CSO5_url',
    'IMPORT_CSO8_ROWS_QUEUE_URL': 'CSO8_url',
    'IMPORT_MIGRATION_QUEUE_URL': 'raw_url',
}


def create_event(body):
    sqs_event = sqs_mock_event(body)
    sqs_event['Records'][0]['messageId'] = 'a_test_id'
    sqs_event['Records'][0]['receiptHandle'] = 'a_recepit_handle'
    return sqs_event


@ddt
class TestImportMigration(TestCase):

    @patch('os.environ', mock_environ)
    @patch('boto3.client', MagicMock())
    @patch('boto3.resource', MagicMock())
    @patch('common.log.get_internal_id', Mock(return_value='internal_id'))
    def setUp(self):
        import import_migration.import_migration as _import_migration
        self.import_migration = _import_migration
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = 'import_migration_test'

    @patch('import_migration.import_migration._S3_CLIENT')
    def test_lambda_handler_CallsS3GetObjectWithTheCorrectParameters(self, s3_client_mock):
        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'my-key'}}}]})

        self.import_migration.lambda_handler(test_event, self.context)

        _, actual_call_kwargs = s3_client_mock.get_object.call_args_list[0]
        self.assertLessEqual({'Bucket': 'my-bucket', 'Key': 'my-key'}.items(), actual_call_kwargs.items())

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration._S3_CLIENT')
    def test_lambda_handler_sends_messages_to_SQS_in_batches(self, s3_client_mock,
                                                             sqs_client_mock, internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        stream_object_mock = MagicMock()
        stream_object_mock.iter_lines.return_value = iter([
            b'SOURCE_CIPHER|NHSNUM|AMENDED|SEQ|REASON|PNTD|USERID|RSN_FLAG|CEASE_CODE|CEASE_CIPHER|DELAY_DATE|AMEND_FLAG',  
            b'line 1', b'line 2', b'line 3', b'line 4', b'line 5', b'line 6', b'line 7', b'line 8', b'line 9',
            b'line 10', b'line 11', b'line 12', b'line 13', b'line 14', b'line 15', b'line 16', b'line 17', b'line 18',
            b'line 19', b'line 20', b'line 21',
        ])
        s3_client_mock.get_object.return_value = {'Body': stream_object_mock}

        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'my-key'}}}]})

        self.import_migration.lambda_handler(test_event, self.context)
        self.assertTrue(sqs_client_mock.send_message_batch.call_count == 3)

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration._S3_CLIENT')
    def test_lambda_handler_sends_each_line_in_separate_message_to_SQS_for_CSO1R_records(
            self, s3_client_mock, sqs_client_mock, internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        stream_object_mock = MagicMock()

        stream_object_mock.iter_lines.return_value = iter([
            b'SOURCE_CIPHER|NHSNUM|DOB|GENDER|OAPD|LDN|GP|PRAC|RSNFORRMVL|REMDATE|DESTINATION|TITLE|SURNAME|FIRST_FORENAME|OTHER_FORENAMES|ADDRESS1|ADDRESS2|LOCALITY|TOWN|COUNTY|POSTCODE|PREV_SUR|DEATHDATE',  
            b'line 1', b'line 2', b'line 3', b'line 4'
        ])
        s3_client_mock.get_object.return_value = {'Body': stream_object_mock}

        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'my-key'}}}]})

        self.import_migration.lambda_handler(test_event, self.context)

        self.assertTrue(sqs_client_mock.send_message_batch.call_count == 1)

        items = sqs_client_mock.send_message_batch.call_args_list[0]
        self.assertEqual(items[1]['QueueUrl'], 'CSO1R_url')
        for i, item in enumerate(items[1]['Entries'], start=1):
            expected_message = f'{{"internal_id": "test-internal-id", "file_name": "my-key", "file_part_name": "my-key", "line_index": {i}, "line": "line {i}"}}'  
            self.assertEqual(expected_message, item['MessageBody'])

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration._S3_CLIENT')
    def test_lambda_handler_sends_each_line_in_separate_message_to_SQS_for_CSO1C_records(
            self, s3_client_mock, sqs_client_mock, internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        stream_object_mock = MagicMock()

        stream_object_mock.iter_lines.return_value = iter([
            b'SOURCE_CIPHER|NHSNUM|NTD|RECALL_TYPE|RECALL_STAT|FNR_COUNT|POS_DOUBT|CEASED_FLAG|CEASED_CIPHER|NOTES1|NOTES2|NOTES3|LDN|OAPD|NOTIF_DATE|INV_DATE|PNL_DATE|PATH_ID|PNL_FNR',  
            b'line 1', b'line 2', b'line 3', b'line 4'
        ])
        s3_client_mock.get_object.return_value = {'Body': stream_object_mock}

        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'my-key'}}}]})

        self.import_migration.lambda_handler(test_event, self.context)
        self.assertEqual(sqs_client_mock.send_message_batch.call_count, 1)

        items = sqs_client_mock.send_message_batch.call_args_list[0]
        self.assertEqual(items[1]['QueueUrl'], 'CSO1C_url')
        for i, item in enumerate(items[1]['Entries'], start=1):
            expected_message = f'{{"internal_id": "test-internal-id", "file_name": "my-key", "file_part_name": "my-key", "line_index": {i}, "line": "line {i}"}}'  
            self.assertEqual(expected_message, item['MessageBody'])

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration._S3_CLIENT')
    def test_lambda_handler_sends_each_line_in_separate_message_to_SQS_for_CSO2_records(
            self, s3_client_mock, sqs_client_mock, internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        stream_object_mock = MagicMock()

        stream_object_mock.iter_lines.return_value = iter([
            b'SOURCE_CIPHER|NHSNUM|TEST_SEQ|TDATE|RES_CODE|ACT_CODE|INFECT|RPT_MNTH|LAB_LOCAL|LAB_NAT|SOURCE|SENDER|SENDER_EXIST|SENDER_NAT|NOTIF_DATE|INVITE_DATE|SLIDE|HPV|TEST_CIPHER|TEST_INPUT|COMM_1|COMM_2|COMM_3|SAMP_METH|PRAC_CODE',  
            b'line 1', b'line 2', b'line 3', b'line 4'
        ])
        s3_client_mock.get_object.return_value = {'Body': stream_object_mock}

        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'my-key'}}}]})

        self.import_migration.lambda_handler(test_event, self.context)
        sqs_client_mock.send_message_batch.assert_called()
        self.assertTrue(sqs_client_mock.send_message_batch.call_count == 1)

        items = sqs_client_mock.send_message_batch.call_args_list[0]
        self.assertEqual(items[1]['QueueUrl'], 'CSO2_url')
        for i, item in enumerate(items[1]['Entries'], start=1):
            expected_message = f'{{"internal_id": "test-internal-id", "file_name": "my-key", "file_part_name": "my-key", "line_index": {i}, "line": "line {i}"}}'  
            self.assertEqual(expected_message, item['MessageBody'])

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration._S3_CLIENT')
    def test_lambda_handler_sends_each_line_in_separate_message_to_SQS_for_CSO3_records(
            self, s3_client_mock, sqs_client_mock, internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        stream_object_mock = MagicMock()

        stream_object_mock.iter_lines.return_value = iter([
            b'SOURCE_CIPHER|NHSNUM|AMENDED|SEQ|REASON|PNTD|USERID|RSN_FLAG|CEASE_CODE|CEASE_CIPHER|DELAY_DATE|AMEND_FLAG',  
            b'line 1', b'line 2', b'line 3', b'line 4', b'line 5', b'line 6', b'line 7', b'line 8', b'line 9',
            b'line 10', b'line 11', b'line 12',
        ])
        s3_client_mock.get_object.return_value = {'Body': stream_object_mock}

        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'my-key'}}}]})

        self.import_migration.lambda_handler(test_event, self.context)
        self.assertTrue(sqs_client_mock.send_message_batch.call_count == 2)
        for call_counter, items in enumerate(sqs_client_mock.send_message_batch.call_args_list):
            self.assertEqual(items[1]['QueueUrl'], 'CSO3_url')
            for i, item in enumerate(items[1]['Entries'], start=1):
                expected_message = f'{{"internal_id": "test-internal-id", "file_name": "my-key", "file_part_name": "my-key", "line_index": {(call_counter * 10)+ i}, "line": "line {(call_counter * 10) + i}"}}'  
                self.assertEqual(expected_message, item['MessageBody'])

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._S3_CLIENT')
    @patch('import_migration.import_migration.boto3')
    def test_file_is_moved_to_completed_bucket_after_successful_file_processing(self, boto3_mock,
                                                                                s3_client_mock, internal_id_mock):

        internal_id_mock.return_value = 'test-internal-id'
        stream_object_mock = MagicMock()
        stream_object_mock.iter_lines.return_value = iter([
            b'SOURCE_CIPHER|NHSNUM|AMENDED|SEQ|REASON|PNTD|USERID|RSN_FLAG|CEASE_CODE|CEASE_CIPHER|DELAY_DATE|AMEND_FLAG',  
            b'line 1', b'line 2', b'line 3', b'line 4'
        ])
        s3_client_mock.get_object.return_value = {'Body': stream_object_mock}

        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'my-key'}}}]})
        self.import_migration.lambda_handler(test_event, self.context)
        s3_client_mock.copy.assert_called_with(
            Bucket='completed bucket',
            CopySource={'Bucket': 'my-bucket', 'Key': 'my-key'},
            ExtraArgs={'StorageClass': 'STANDARD_IA'},
            Key='TEST123-3/batch=my-key/my-key')

        s3_client_mock.delete_object.assert_called_with(Bucket='my-bucket', Key='my-key')

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._S3_CLIENT')
    @patch('import_migration.import_migration.boto3')
    def test_file_is_moved_to_failed_bucket_after_error(self, boto3_mock, s3_client_mock, internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        stream_object_mock = MagicMock()
        stream_object_mock.iter_lines.return_value = []

        s3_client_mock.get_object.side_effect = Exception()
        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'my-key'}}}]})

        self.import_migration.lambda_handler(test_event, self.context)
        s3_client_mock.copy.assert_called_with(
            Bucket='failed bucket',
            CopySource={'Bucket': 'my-bucket', 'Key': 'my-key'},
            ExtraArgs={'StorageClass': 'STANDARD_IA'},
            Key='my-key')

        s3_client_mock.delete_object.assert_called_with(Bucket='my-bucket', Key='my-key')

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._S3_CLIENT')
    @patch('import_migration.import_migration.boto3')
    def test_exception_rasied_when_file_key_empty(self, boto3_mock, s3_client_mock, internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': ''}}}]})
        with self.assertRaises(Exception):
            self.import_migration.lambda_handler(test_event, self.context)

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._S3_CLIENT')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration.boto3')
    def test_exception_rasied_and_message_visibility_changed_when_no_records_in_event_body(
            self, boto3_mock, sqs_client_mock, s3_client_mock,
            internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        test_event = create_event({'invalid': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': ''}}}]})

        with self.assertRaises(Exception) as context:
            self.import_migration.lambda_handler(test_event, self.context)
        self.assertEqual('Missing records from body', context.exception.args[0])
        self.assertTrue(sqs_client_mock.change_message_visibility.call_count == 1)
        sqs_client_mock.change_message_visibility.assert_called_with(
            QueueUrl=mock_environ['IMPORT_MIGRATION_QUEUE_URL'],
            ReceiptHandle=test_event['Records'][0]['receiptHandle'], VisibilityTimeout=1)

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._S3_CLIENT')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration.boto3')
    def test_exception_rasied_and_message_visibility_changed_when_no_body_in_event(
            self, boto3_mock, sqs_client_mock, s3_client_mock,
            internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        test_event = create_event({})

        with self.assertRaises(Exception) as context:
            self.import_migration.lambda_handler(test_event, self.context)
        self.assertEqual('Missing body from record', context.exception.args[0])
        self.assertTrue(sqs_client_mock.change_message_visibility.call_count == 1)
        sqs_client_mock.change_message_visibility.assert_called_with(
            QueueUrl=mock_environ['IMPORT_MIGRATION_QUEUE_URL'],
            ReceiptHandle=test_event['Records'][0]['receiptHandle'], VisibilityTimeout=1)

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._S3_CLIENT')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration.boto3')
    def test_exception_rasied_and_message_visibility_changed_when_no_s3_details_in_record(
            self, boto3_mock, sqs_client_mock, s3_client_mock,
            internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        test_event = create_event({'Records': [{}]})
        with self.assertRaises(Exception) as context:
            self.import_migration.lambda_handler(test_event, self.context)
        self.assertEqual('Missing s3 details from lambda event object', context.exception.args[0])
        self.assertTrue(sqs_client_mock.change_message_visibility.call_count == 1)
        sqs_client_mock.change_message_visibility.assert_called_with(
            QueueUrl=mock_environ['IMPORT_MIGRATION_QUEUE_URL'],
            ReceiptHandle=test_event['Records'][0]['receiptHandle'], VisibilityTimeout=1)

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._S3_CLIENT')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration.boto3')
    def test_exception_rasied_changed_when_no_records_in_event(
            self, boto3_mock, sqs_client_mock, s3_client_mock,
            internal_id_mock):
        test_event = {'Records': ''}

        with self.assertRaises(Exception) as context:
            self.import_migration.lambda_handler(test_event, self.context)
        self.assertEqual('string index out of range', context.exception.args[0])

    @patch('import_migration.import_migration.log')
    def test_s3_test_event_is_ignored(self, mock_logger):

        message = {
            'Service': 'Amazon S3',
            'Event': 's3:TestEvent'
        }

        self.import_migration.lambda_handler(create_event(message), self.context)

        mock_logger.assert_called_with({'log_reference': CommonLogReference.S3EVENT0000})

    def test_get_sqs_queue_throws_detailed_exception_when_header_record_does_not_match_any_header_schema(self):
        header = b'SOURCE_CIPHER|NHSNUM|AMENDED|SEQ|REASON|PNTD|USERID|CHEESE|CEASE_CODE|CEASE_CIPHER|DELAY_DATE|AMEND_FLAG|BANANA'  

        expected_1C_differences = {'record_type': '1C', 'difference_in_expected_length': -6,
                                   'missing_fields': {'LDN', 'NOTES2', 'POS_DOUBT', 'RECALL_TYPE', 'NTD', 'NOTES3',
                                                      'OAPD', 'CEASED_CIPHER', 'RECALL_STAT', 'FNR_COUNT', 'NOTES1',
                                                      'CEASED_FLAG', 'NOTIF_DATE', 'INV_DATE', 'PNL_DATE', 'PATH_ID',
                                                      'PNL_FNR'}}
        expected_1R_differences = {'record_type': '1R', 'difference_in_expected_length': -10,
                                   'missing_fields': {'RSNFORRMVL', 'GP', 'FIRST_FORENAME', 'TOWN', 'REMDATE', 'PRAC',
                                                      'POSTCODE', 'GENDER', 'ADDRESS1', 'DOB', 'SURNAME', 'COUNTY',
                                                      'TITLE', 'DESTINATION', 'ADDRESS2', 'OAPD', 'OTHER_FORENAMES',
                                                      'LOCALITY', 'LDN', 'PREV_SUR', 'DEATHDATE'}}
        expected_2_differences = {'record_type': '2', 'difference_in_expected_length': -12,
                                  'missing_fields': {'COMM_2', 'INVITE_DATE', 'TEST_CIPHER', 'HPV', 'SENDER',
                                                     'NOTIF_DATE', 'LAB_LOCAL', 'SENDER_EXIST', 'LAB_NAT', 'ACT_CODE',
                                                     'COMM_1', 'INFECT', 'TEST_INPUT', 'TDATE', 'RPT_MNTH', 'TEST_SEQ',
                                                     'COMM_3', 'RES_CODE', 'SLIDE', 'SENDER_NAT', 'SOURCE',
                                                     'SAMP_METH', 'PRAC_CODE'}}
        expected_3_differences = {'record_type': '3', 'difference_in_expected_length': 1,
                                  'missing_fields': {'RSN_FLAG'}}
        with self.assertRaises(Exception) as context:
            self.import_migration.get_sqs_queue_from_header(header)

        logged_differences = eval(context.exception.args[0].split('Unknown header schema. Comparison of supplied '
                                                                  'header with permitted header schemas: ')[1])

        self.assertDictEqual(expected_1C_differences, logged_differences[0])
        self.assertDictEqual(expected_1R_differences, logged_differences[1])
        self.assertDictEqual(expected_2_differences, logged_differences[2])
        self.assertDictEqual(expected_3_differences, logged_differences[3])

    def testing_get_sqs_queue_from_header_with_new_headers(self):
        header = b'SOURCE_CIPHER|NHSNUM|AMENDED|SEQ|REASON|PNTD|USERID|RSN_FLAG|CEASE_CODE|CEASE_CIPHER|DELAY_DATE|AMEND_FLAG|NEW|OTHER|BUT|LESS'  
        sqs_queue = self.import_migration.get_sqs_queue_from_header(header)
        self.assertEqual(sqs_queue, "CSO3_url")

    def testing_get_sqs_queue_from_header_with_too_many_new_headers(self):
        self.maxDiff = None
        header = b'SOURCE_CIPHER|NHSNUM|AMENDED|SEQ|REASON|PNTD|USERID|RSN_FLAG|CEASE_CODE|CEASE_CIPHER|DELAY_DATE|AMEND_FLAG|NEW|HEADER|BUT|MORE|THAN|EXPECTED|_|_'  
        with self.assertRaises(Exception) as context:
            self.import_migration.get_sqs_queue_from_header(header)

        logged_differences = eval(context.exception.args[0].split('Unknown header schema. Comparison of supplied '
                                                                  'header with permitted header schemas: ')[1])

        expected_1C_differences = {'record_type': '1C', 'difference_in_expected_length': 1,
                                   'missing_fields': {'CEASED_FLAG', 'NOTES3', 'FNR_COUNT', 'RECALL_TYPE', 'POS_DOUBT',
                                                      'NTD', 'RECALL_STAT', 'CEASED_CIPHER', 'NOTES1', 'NOTES2', 'OAPD',
                                                      'LDN', 'NOTIF_DATE', 'INV_DATE', 'PNL_DATE', 'PATH_ID',
                                                      'PNL_FNR'}}
        expected_1R_differences = {'record_type': '1R', 'difference_in_expected_length': -3,
                                   'missing_fields': {'TOWN', 'SURNAME', 'DESTINATION', 'OTHER_FORENAMES', 'LOCALITY',
                                                      'FIRST_FORENAME', 'RSNFORRMVL', 'PRAC', 'POSTCODE', 'TITLE',
                                                      'DOB', 'REMDATE', 'ADDRESS1', 'OAPD', 'COUNTY', 'GENDER', 'LDN',
                                                      'ADDRESS2', 'GP', 'PREV_SUR', 'DEATHDATE'}}
        expected_2_differences = {'record_type': '2', 'difference_in_expected_length': -5,
                                  'missing_fields': {'COMM_1', 'SENDER_EXIST', 'COMM_3', 'NOTIF_DATE', 'SLIDE',
                                                     'LAB_LOCAL', 'SENDER_NAT', 'RPT_MNTH', 'INFECT', 'ACT_CODE',
                                                     'COMM_2', 'TEST_SEQ', 'TDATE', 'HPV', 'TEST_INPUT', 'SENDER',
                                                     'LAB_NAT', 'SOURCE', 'TEST_CIPHER', 'INVITE_DATE', 'RES_CODE',
                                                     'SAMP_METH', 'PRAC_CODE'}}
        expected_3_differences = {'record_type': '3', 'difference_in_expected_length': 8,
                                  'missing_fields': set()}

        self.assertDictEqual(expected_1C_differences, logged_differences[0])
        self.assertDictEqual(expected_1R_differences, logged_differences[1])
        self.assertDictEqual(expected_2_differences, logged_differences[2])
        self.assertDictEqual(expected_3_differences, logged_differences[3])

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration._S3_CLIENT')
    def test_lambda_handler_sends_each_line_in_separate_message_to_SQS_for_CSO5_records(
            self, s3_client_mock, sqs_client_mock, internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        stream_object_mock = MagicMock()

        stream_object_mock.iter_lines.return_value = iter([
            b'SOURCE_CIPHER|REF_TYPE|REF_ID|REF_NAME|POSTCODE|DHA|NATIONAL|ADDRESS1|ADDRESS2|ADDRESS3|ADDRESS4|ADDRESS5|TELNO|PARENT_CODE|ACTIVE|INACTIVE_DATE|RESPONSIBLE|EMAIL1|EMAIL2|REF_SUBTYPE|SMARTCARD_ID',  
            b'line 1', b'line 2', b'line 3', b'line 4', b'line 5', b'line 6', b'line 7', b'line 8', b'line 9',
            b'line 10', b'line 11', b'line 12',
        ])
        s3_client_mock.get_object.return_value = {'Body': stream_object_mock}

        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'my-key'}}}]})

        self.import_migration.lambda_handler(test_event, self.context)
        self.assertTrue(sqs_client_mock.send_message_batch.call_count == 2)
        for call_counter, items in enumerate(sqs_client_mock.send_message_batch.call_args_list):
            self.assertEqual(items[1]['QueueUrl'], 'CSO5_url')
            for i, item in enumerate(items[1]['Entries'], start=1):
                expected_message = f'{{"internal_id": "test-internal-id", "file_name": "my-key", "file_part_name": "my-key", "line_index": {(call_counter * 10)+ i}, "line": "line {(call_counter * 10) + i}"}}'  
                self.assertEqual(expected_message, item['MessageBody'])

    @patch('import_migration.import_migration.get_internal_id')
    @patch('import_migration.import_migration._SQS_CLIENT')
    @patch('import_migration.import_migration._S3_CLIENT')
    def test_lambda_handler_sends_each_line_in_separate_message_to_SQS_for_CSO8_records(
            self, s3_client_mock, sqs_client_mock, internal_id_mock):
        internal_id_mock.return_value = 'test-internal-id'
        stream_object_mock = MagicMock()

        stream_object_mock.iter_lines.return_value = iter([
            b'SOURCE_CIPHER|NHSNUM|NOTIF-DATE|NOTIF-SEQ|NOTIF-TYPE-CODE|NOTIF-SUB1|NOTIF-SUB2|NOTIF-SUB3',
            b'line 1', b'line 2', b'line 3', b'line 4', b'line 5', b'line 6', b'line 7', b'line 8', b'line 9',
            b'line 10', b'line 11', b'line 12',
        ])
        s3_client_mock.get_object.return_value = {'Body': stream_object_mock}

        test_event = create_event({'Records': [{'s3': {'bucket': {'name': 'my-bucket'}, 'object': {'key': 'my-key'}}}]})

        self.import_migration.lambda_handler(test_event, self.context)
        self.assertTrue(sqs_client_mock.send_message_batch.call_count == 2)
        for call_counter, items in enumerate(sqs_client_mock.send_message_batch.call_args_list):
            self.assertEqual(items[1]['QueueUrl'], 'CSO8_url')
            for i, item in enumerate(items[1]['Entries'], start=1):
                expected_message = f'{{"internal_id": "test-internal-id", "file_name": "my-key", "file_part_name": "my-key", "line_index": {(call_counter * 10)+ i}, "line": "line {(call_counter * 10) + i}"}}'  
                self.assertEqual(expected_message, item['MessageBody'])

    @unpack
    @data(
        (b'SOURCE_CIPHER|NHSNUM|NTD|RECALL_TYPE|RECALL_STAT|FNR_COUNT|POS_DOUBT|CEASED_FLAG|CEASED_CIPHER|NOTES1|NOTES2|NOTES3|LDN|OAPD|NOTIF_DATE|INV_DATE|PNL_DATE|PATH_ID|PNL_FNR',  
         'CSO1C_url'),
        (b'SOURCE_CIPHER|NHSNUM|DOB|GENDER|OAPD|LDN|GP|PRAC|RSNFORRMVL|REMDATE|DESTINATION|TITLE|SURNAME|FIRST_FORENAME|OTHER_FORENAMES|ADDRESS1|ADDRESS2|LOCALITY|TOWN|COUNTY|POSTCODE|PREV_SUR|DEATHDATE',  
         'CSO1R_url'),
        (b'SOURCE_CIPHER|NHSNUM|TEST_SEQ|TDATE|RES_CODE|ACT_CODE|INFECT|RPT_MNTH|LAB_LOCAL|LAB_NAT|SOURCE|SENDER|SENDER_EXIST|SENDER_NAT|NOTIF_DATE|INVITE_DATE|SLIDE|HPV|TEST_CIPHER|TEST_INPUT|COMM_1|COMM_2|COMM_3|SAMP_METH|PRAC_CODE',  
         'CSO2_url'),
        (b'SOURCE_CIPHER|NHSNUM|AMENDED|SEQ|REASON|PNTD|USERID|RSN_FLAG|CEASE_CODE|CEASE_CIPHER|DELAY_DATE|AMEND_FLAG',  
         'CSO3_url'),
        (b'SOURCE_CIPHER|REF_TYPE|REF_ID|REF_NAME|POSTCODE|DHA|NATIONAL|ADDRESS1|ADDRESS2|ADDRESS3|ADDRESS4|ADDRESS5|TELNO|PARENT_CODE|ACTIVE|INACTIVE_DATE|RESPONSIBLE|EMAIL1|EMAIL2|REF_SUBTYPE|SMARTCARD_ID',  
         'CSO5_url'),
        (b'SOURCE_CIPHER|NHSNUM|NOTIF-DATE|NOTIF-SEQ|NOTIF-TYPE-CODE|NOTIF-SUB1|NOTIF-SUB2|NOTIF-SUB3',  
         'CSO8_url'),
        )
    def test_get_sqs_queue_returns_correct_sqs_queue_url_for_header(self, header, expected_url):
        actual_queue_url = self.import_migration.get_sqs_queue_from_header(header)

        self.assertEqual(expected_url, actual_queue_url)
