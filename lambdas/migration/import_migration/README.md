This lambda reads files uploaded to the migration-data s3 bucket. 
It splits each file line by line, and for each line sends a message to the import_migration_raw sqs queue in batches of 10.

Once a file has been fully processed it is moved to the migration-data-completed bucket and removed from the migration-data bucket. This process renames the file to allow Athena queries against the data. The main driver for this work being assurance around data exceptions and allowing Data Anlysts to directly query the data. See [Confluence](https://nhsd-confluence.digital.nhs.uk/pages/viewpage.action?spaceKey=CSP&title=Data+Exceptions+Reporting+with+Athena) for details.

If an error occurs during file processing then the file is moved to the migration-data-failed bucket and removed from the migration-data bucket.