import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    MIGR0001 = (logging.INFO, 'Migration file successfully processed')
    MIGR0002 = (logging.INFO, 'Successfully processed line')
    MIGR0003 = (logging.INFO, 'S3 file copied successfully')
    MIGR0004 = (logging.INFO, 'S3 file deleted successfully')
    MIGR0005 = (logging.ERROR, 'Error communicating with S3')
    MIGR0006 = (logging.ERROR, 'Error removing file from S3')
    MIGR0007 = (logging.INFO, 'Successfully extracted Record Type from file name')
    MIGR0008 = (logging.ERROR, 'Error processing migration file')
    MIGR0009 = (logging.ERROR, 'Lines failed to send to SQS')
