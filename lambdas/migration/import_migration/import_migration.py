import os
import json
import boto3
from botocore.client import Config
from common.log import log, update_logger_metadata_fields, get_internal_id
from common.utils.lambda_wrappers import lambda_entry_point
from common.migration_fields.CSO1C_fields import CSO1C_FIELDS
from import_migration.log_references import LogReference
from common.log_references import CommonLogReference

IMPORT_MIGRATION_QUEUE_URL = os.environ.get('IMPORT_MIGRATION_QUEUE_URL')
IMPORT_CSO1C_ROWS_QUEUE_URL = os.environ.get('IMPORT_CSO1C_ROWS_QUEUE_URL')
IMPORT_CSO2_ROWS_QUEUE_URL = os.environ.get('IMPORT_CSO2_ROWS_QUEUE_URL')
IMPORT_CSO1R_ROWS_QUEUE_URL = os.environ.get('IMPORT_CSO1R_ROWS_QUEUE_URL')
IMPORT_CSO3_ROWS_QUEUE_URL = os.environ.get('IMPORT_CSO3_ROWS_QUEUE_URL')
IMPORT_CSO5_ROWS_QUEUE_URL = os.environ.get('IMPORT_CSO5_ROWS_QUEUE_URL')
IMPORT_CSO8_ROWS_QUEUE_URL = os.environ.get('IMPORT_CSO8_ROWS_QUEUE_URL')
COMPLETED_BUCKET = os.environ.get('COMPLETED_BUCKET')
FAILED_BUCKET = os.environ.get('FAILED_BUCKET')
ENVIRONMENT_NAME = os.getenv('ENVIRONMENT_NAME')
DATA_BUCKET = None
BATCH_SIZE = 10
NEW_HEADERS_LIMIT = 5
# Rather than accessing these globals directly use the get_...() function which aids in testability
_S3_CLIENT = None
_SQS_CLIENT = None


def get_s3_client():
    global _S3_CLIENT
    if not _S3_CLIENT:
        _S3_CLIENT = boto3.client('s3', region_name='eu-west-2', config=Config(retries={'max_attempts': 3}))
    return _S3_CLIENT


def get_sqs_client():
    global _SQS_CLIENT
    if not _SQS_CLIENT:
        _SQS_CLIENT = boto3.client('sqs', config=Config(retries={'max_attempts': 3}))
    return _SQS_CLIENT


record_type_to_queue = {
    '1C': IMPORT_CSO1C_ROWS_QUEUE_URL,
    '1R': IMPORT_CSO1R_ROWS_QUEUE_URL,
    '2': IMPORT_CSO2_ROWS_QUEUE_URL,
    '3': IMPORT_CSO3_ROWS_QUEUE_URL,
    '5': IMPORT_CSO5_ROWS_QUEUE_URL,
    '8': IMPORT_CSO8_ROWS_QUEUE_URL
}


record_schemata = {
    '1C': CSO1C_FIELDS,
    '1R': [
        'SOURCE_CIPHER', 'NHSNUM', 'DOB', 'GENDER',
        'OAPD', 'LDN', 'GP', 'PRAC', 'RSNFORRMVL',
        'REMDATE', 'DESTINATION', 'TITLE', 'SURNAME',
        'FIRST_FORENAME', 'OTHER_FORENAMES',
        'ADDRESS1', 'ADDRESS2', 'LOCALITY',
        'TOWN', 'COUNTY', 'POSTCODE', 'PREV_SUR', 'DEATHDATE'
    ],
    '2': [
        'SOURCE_CIPHER', 'NHSNUM', 'TEST_SEQ', 'TDATE',
        'RES_CODE', 'ACT_CODE', 'INFECT', 'RPT_MNTH',
        'LAB_LOCAL', 'LAB_NAT', 'SOURCE', 'SENDER',
        'SENDER_EXIST', 'SENDER_NAT', 'NOTIF_DATE',
        'INVITE_DATE', 'SLIDE', 'HPV', 'TEST_CIPHER',
        'TEST_INPUT', 'COMM_1', 'COMM_2', 'COMM_3', 'SAMP_METH',
        'PRAC_CODE'
    ],
    '3': [
        'SOURCE_CIPHER', 'NHSNUM', 'AMENDED', 'SEQ',
        'REASON', 'PNTD', 'USERID', 'RSN_FLAG',
        'CEASE_CODE', 'CEASE_CIPHER', 'DELAY_DATE', 'AMEND_FLAG'
    ],
    '5': [
        'SOURCE_CIPHER', 'REF_TYPE', 'REF_ID', 'REF_NAME', 'POSTCODE', 'DHA', 'NATIONAL', 'ADDRESS1', 'ADDRESS2',
        'ADDRESS3', 'ADDRESS4', 'ADDRESS5', 'TELNO', 'PARENT_CODE', 'ACTIVE', 'INACTIVE_DATE', 'RESPONSIBLE', 'EMAIL1',
        'EMAIL2', 'REF_SUBTYPE', 'SMARTCARD_ID'
    ],
    '8': [
        'SOURCE_CIPHER', 'NHSNUM', 'NOTIF-DATE', 'NOTIF-SEQ',
        'NOTIF-TYPE-CODE', 'NOTIF-SUB1', 'NOTIF-SUB2', 'NOTIF-SUB3'
    ]
}


@lambda_entry_point
def lambda_handler(event, context):
    global DATA_BUCKET

    record = event.get('Records')[0]
    if not record:
        raise Exception('Missing record from lambda event')

    receipt_handle = record.get('receiptHandle')
    body = json.loads(record.get('body'))
    if not body:
        get_sqs_client().change_message_visibility(QueueUrl=IMPORT_MIGRATION_QUEUE_URL, ReceiptHandle=receipt_handle,
                                                   VisibilityTimeout=1)
        raise Exception('Missing body from record')

    if body.get('Event') == 's3:TestEvent':
        log({'log_reference': CommonLogReference.S3EVENT0000})
        return

    records = body.get('Records')
    if records is None:
        get_sqs_client().change_message_visibility(QueueUrl=IMPORT_MIGRATION_QUEUE_URL, ReceiptHandle=receipt_handle,
                                                   VisibilityTimeout=1)
        raise Exception('Missing records from body')

    s3_details = records[0]
    if not s3_details:
        get_sqs_client().change_message_visibility(QueueUrl=IMPORT_MIGRATION_QUEUE_URL, ReceiptHandle=receipt_handle,
                                                   VisibilityTimeout=1)
        raise Exception('Missing s3 details from lambda event object')

    bucket = s3_details.get('s3', {}).get('bucket', {})
    DATA_BUCKET = bucket.get('name', '')
    if not DATA_BUCKET:
        raise Exception('Missing bucket name from s3 details object')
    file_key = s3_details.get('s3', {}).get('object', {}).get('key', '')
    if not file_key:
        raise Exception('Missing file key from s3 details object')

    internal_id = get_internal_id()
    file_name_before_part = file_key.split('_part')[0]
    update_logger_metadata_fields({
        'file_name': file_name_before_part,
        'file_part_name': file_key
    })

    try:
        successfully_processed_lines = 0
        migration_data_streaming_body = get_s3_client().get_object(
            Bucket=DATA_BUCKET, Key=file_key)['Body']
        if not migration_data_streaming_body:
            raise Exception('Unable to fetch {} from {} s3 bucket', file_key, DATA_BUCKET)
        migration_data_lines = migration_data_streaming_body.iter_lines(chunk_size=62914560)
        migration_data_header = next(migration_data_lines)
        queue_url = get_sqs_queue_from_header(migration_data_header)
        messages = []
        for index, line in enumerate(migration_data_lines, start=1):
            message = {
                'Id': str(index),
                'MessageBody': json.dumps({
                    'internal_id': internal_id,
                    'file_name': file_name_before_part,
                    'file_part_name': file_key,
                    'line_index': index,
                    'line': line.decode('UTF-8')
                }).strip()
            }
            messages.append(message)
            if len(messages) == BATCH_SIZE:
                _send_messages(queue_url, messages)
                messages = []
            successfully_processed_lines = index
        if messages:
            _send_messages(queue_url, messages)
            successfully_processed_lines = index
        log({'log_reference': LogReference.MIGR0001})
        copy_processed_file_to_export_bucket(file_key, migration_data_header, file_name_before_part, COMPLETED_BUCKET)
        remove_processed_file_from_data_bucket(file_key)

    except Exception as e:
        log(CommonLogReference.EXCEPTION000)
        log({'log_reference': LogReference.MIGR0008,
             'error': f'{str(e)} - File moved to failed bucket: {FAILED_BUCKET}',
             'successfully_processed_lines': successfully_processed_lines})
        move_processed_file_to_processed_bucket(file_key, FAILED_BUCKET)
        remove_processed_file_from_data_bucket(file_key)


def _send_messages(queue_url, messages):
    message_response = get_sqs_client().send_message_batch(
        QueueUrl=queue_url, Entries=messages)

    for message in message_response.get('Successful', []):
        log({'log_reference': LogReference.MIGR0002, 'line_number': message['Id']})

    for message in message_response.get('Failed', []):
        log({'log_reference': LogReference.MIGR0009, 'line_number': message['Id']})


def move_processed_file_to_processed_bucket(source_file, target_bucket, destination_key=None):

    if not destination_key:
        destination_key = source_file

    try:
        get_s3_client().copy(
            CopySource={'Bucket': DATA_BUCKET, 'Key': source_file},
            Bucket=target_bucket,
            Key=destination_key,
            ExtraArgs={
                'StorageClass': 'STANDARD_IA'
            }
        )

        log({'log_reference': LogReference.MIGR0003})

    except Exception as e:
        log({'log_reference': LogReference.MIGR0005, 'error': str(e)})
        raise e


def copy_processed_file_to_export_bucket(source_file, header, file_name_before_part, target_bucket):
    file_type = get_file_type_from_header(header)
    destination_key = f"{ENVIRONMENT_NAME}-{file_type}/batch={file_name_before_part}/{source_file}"
    move_processed_file_to_processed_bucket(source_file, COMPLETED_BUCKET, destination_key)


def remove_processed_file_from_data_bucket(source_file):
    try:
        get_s3_client().delete_object(Bucket=DATA_BUCKET, Key=source_file)

        log({'log_reference': LogReference.MIGR0004})

    except Exception as e:
        log({'log_reference': LogReference.MIGR0006, 'error': str(e)})
        raise e


def get_file_type_from_header(header):
    header_keys = header.decode('utf-8').split('|')
    for record_type, record_schema in record_schemata.items():
        record_schema_length = len(record_schema)
        if (len(header_keys) <= record_schema_length + NEW_HEADERS_LIMIT
                and record_schema == header_keys[:record_schema_length]):
            return record_type


def get_sqs_queue_from_header(header):
    header_keys = header.decode('utf-8').split('|')
    for record_type, record_schema in record_schemata.items():
        record_schema_length = len(record_schema)
        if (len(header_keys) <= record_schema_length + NEW_HEADERS_LIMIT
                and record_schema == header_keys[:record_schema_length]):
            return record_type_to_queue[record_type]
    else:
        header_length = len(header_keys)
        header_set = set(header_keys)
        differences = []
        for record_type, record_schema in record_schemata.items():
            difference_in_expected_length = header_length - len(record_schema)
            missing_fields = set(record_schema) - header_set
            differences.append({
                'record_type': record_type,
                'difference_in_expected_length': difference_in_expected_length,
                'missing_fields': missing_fields
            })
        raise Exception(f'Unknown header schema. '
                        f'Comparison of supplied header with permitted header schemas: {differences}')
