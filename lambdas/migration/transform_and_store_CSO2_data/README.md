This lambda receives a record from the `sqs_CSO2_rows_to_import` SQS queue, and maps it into the schema used by the
participants table in DynamoDB.