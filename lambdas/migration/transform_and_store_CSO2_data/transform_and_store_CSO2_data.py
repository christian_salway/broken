import os
from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.audit_utils import AuditActions, AuditUsers, audit
from transform_and_store_CSO2_data.log_references import LogReference
from transform_and_store_CSO2_data.map_and_validate_CSO2_data import (
    validate_CSO2_line,
    map_CSO2_line,
)
from common.utils.transform_and_store_utils import (
    get_message_contents_from_event,
    add_update_test_record,
    remove_empty_values,
    missing_necessary_fields,
    record_is_duplicate
)

NECESSARY_FIELDS = ['NHSNUM', 'SOURCE_CIPHER', 'TDATE']


@lambda_entry_point
def lambda_handler(event, context):
    contents = get_message_contents_from_event(event)

    log({'log_reference': LogReference.CSO2MAP0001, 'message_id': contents['message_id']})

    record_line = contents['record_line']

    log({'log_reference': LogReference.CSO2MAP0002})
    log({'log_reference': LogReference.CSO2MAP0003})

    if record_line == '':
        raise Exception('Unable to map blank line')

    validation_errors = validate_CSO2_line(record_line)

    if validation_errors:
        log({'log_reference': LogReference.CSO2MAP0009, 'validation_errors': validation_errors})
        if missing_necessary_fields(validation_errors, NECESSARY_FIELDS):
            log({'log_reference': LogReference.CSO2MAP0010})
            raise Exception('NHS Number, Test Date and Source Cipher required for ingestion')
    else:
        log({'log_reference': LogReference.CSO2MAP0004})

    log({'log_reference': LogReference.CSO2MAP0005})
    data_to_store = map_CSO2_line(record_line, validation_errors)
    log({'log_reference': LogReference.CSO2MAP0006})

    log({'log_reference': LogReference.CSO2MAP0012})
    comparison_fields = ['nhs_number', 'test_date', 'slide_number']
    if record_is_duplicate(data_to_store, comparison_fields):
        log({'log_reference': LogReference.CSO2MAP0013})
        return
    log({'log_reference': LogReference.CSO2MAP0014})

    log({'log_reference': LogReference.CSO2MAP0007})
    participant = add_update_test_record(remove_empty_values(data_to_store))

    if participant:
        log({'log_reference': LogReference.CSO2MAP0008})
        if os.environ.get('ENABLE_MIGRATION_AUDIT') != 'FALSE':
            audit(action=AuditActions.STORE_CSO2,
                  user=AuditUsers.MIGRATION,
                  participant_ids=[participant['participant_id']])
    else:
        log({'log_reference': LogReference.CSO2MAP0011})
        raise Exception('Unable to find matching participant')
