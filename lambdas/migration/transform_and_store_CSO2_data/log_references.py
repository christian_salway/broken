import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    CSO2MAP0001 = (logging.INFO, 'Received message from SQS')
    CSO2MAP0002 = (logging.INFO, 'Extracted record from SQS message')
    CSO2MAP0003 = (logging.INFO, 'Attempting to validate CSO2 record')
    CSO2MAP0004 = (logging.INFO, 'Successfully validated CSO2 record')
    CSO2MAP0005 = (logging.INFO, 'Attempting to map CSO2 record to participant format')
    CSO2MAP0006 = (logging.INFO, 'Successfully mapped CSO2 record to participant format')
    CSO2MAP0007 = (logging.INFO, 'Attempting to update existing participant with CSO2 record')
    CSO2MAP0008 = (logging.INFO, 'Successfully updated existing participant with CSO2 record')
    CSO2MAP0009 = (logging.WARNING, 'CSO2 record has failed validation')
    CSO2MAP0010 = (logging.ERROR, 'CSO2 record has failed mandatory validation, record is rejected')
    CSO2MAP0011 = (logging.ERROR, 'Failed to create CS02 record - no matching participant for record nhs number')
    CSO2MAP0012 = (logging.INFO, 'Attempting to compare hashed source row with existing records')
    CSO2MAP0013 = (logging.INFO, 'This record already exists. Aborting creation of this CS02 record')
    CSO2MAP0014 = (logging.INFO, 'Confirmed record is not a duplicate')
