import common.utils.import_migration_data_validation_utils as utils
from transform_and_store_CSO2_data.unit_tests.test_data.CSO2_example_test_data import (
    CSO2_DATA_VALID_ALL_FIELDS,
    CSO2_DATA_VALID_MANDATORY_FIELDS,
    CSO2_DATA_TOO_MANY_FIELDS,
    CSO2_DATA_TOO_FEW_FIELDS,
    CSO2_ALL_FIELDS_WITH_SCHEMA_VALIDATION_INVALID,
    CSO2_ALL_FIELDS_WITH_DATA_VALIDATION_INVALID,
    CSO2_NO_FIELDS,
    DECODED_PARTICIPANT_RECORD_ALL_FIELDS,
    DECODED_PARTICIPANT_RECORD_MANDATORY_FIELDS,
    DECODED_PARTICIPANT_SCHEMA_ERRORS,
    SCHEMA_VALIDATION_ERRORS,
    DECODED_PARTICIPANT_DATA_ERRORS,
    DATA_VALIDATION_ERRORS,
    MISSING_FIELDS_ERRORS
)

from unittest import TestCase
from unittest.mock import patch, Mock
from datetime import datetime, timezone


with patch('boto3.resource'):
    import transform_and_store_CSO2_data.map_and_validate_CSO2_data as mapper


@patch.object(mapper, 'datetime', Mock(wraps=datetime))
@patch('transform_and_store_CSO2_data.map_and_validate_CSO2_data.get_hashed_value', Mock(return_value='hashed_row'))
class TestDataMapper(TestCase):

    FIXED_NOW_TIME = datetime(2020, 1, 1, 12, 14, 27, 123456, tzinfo=timezone.utc)

    def test_data_mapper_with_valid_input_with_all_fields_populated_validates_returns_expected(self):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime

        valid = mapper.validate_CSO2_line(CSO2_DATA_VALID_ALL_FIELDS)
        mapped = mapper.map_CSO2_line(CSO2_DATA_VALID_ALL_FIELDS, valid)

        self.assertDictEqual({}, valid)
        self.assertDictEqual(DECODED_PARTICIPANT_RECORD_ALL_FIELDS, mapped)

    def test_data_mapper_with_valid_input_with_only_mandatory_fields_populated_validates_returns_expected(self):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime

        valid = mapper.validate_CSO2_line(CSO2_DATA_VALID_MANDATORY_FIELDS)
        mapped = mapper.map_CSO2_line(CSO2_DATA_VALID_MANDATORY_FIELDS, valid)

        self.assertDictEqual({}, valid)
        self.assertDictEqual(DECODED_PARTICIPANT_RECORD_MANDATORY_FIELDS, mapped)

    def test_data_mapper_too_many_fields(self):

        with self.assertRaises(Exception) as context:
            mapper.validate_CSO2_line(CSO2_DATA_TOO_MANY_FIELDS)

        self.assertTrue('Malformed input: Number of fields in CSO2 result must be between 24 and 29'
                        in str(context.exception))

    def test_data_mapper_too_few_fields(self):

        with self.assertRaises(Exception) as context:
            mapper.validate_CSO2_line(CSO2_DATA_TOO_FEW_FIELDS)

        self.assertTrue('Malformed input: Number of fields in CSO2 result must be between 24 and 29'
                        in str(context.exception))

    def test_mapper_with_all_fields_breaking_schema_validation_for_all_fields_with_schema_validation(self):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        utils.NOW_DATE = self.FIXED_NOW_TIME

        valid = mapper.validate_CSO2_line(CSO2_ALL_FIELDS_WITH_SCHEMA_VALIDATION_INVALID)
        mapped = mapper.map_CSO2_line(CSO2_ALL_FIELDS_WITH_SCHEMA_VALIDATION_INVALID, valid)

        self.assertDictEqual(SCHEMA_VALIDATION_ERRORS, valid)
        self.assertDictEqual(DECODED_PARTICIPANT_SCHEMA_ERRORS, mapped)

    def test_mapper_with_all_fields_breaking_data_validation_for_all_fields_with_data_validation(self):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        utils.NOW_DATE = self.FIXED_NOW_TIME

        valid = mapper.validate_CSO2_line(CSO2_ALL_FIELDS_WITH_DATA_VALIDATION_INVALID)
        mapped = mapper.map_CSO2_line(CSO2_ALL_FIELDS_WITH_DATA_VALIDATION_INVALID, valid)

        self.assertDictEqual(DATA_VALIDATION_ERRORS, valid)
        self.assertDictEqual(DECODED_PARTICIPANT_DATA_ERRORS, mapped)

    def test_required_fields_having_missing_error_in_validation_response(self):
        valid = mapper.validate_CSO2_line(CSO2_NO_FIELDS)

        self.assertDictEqual(MISSING_FIELDS_ERRORS, valid)
