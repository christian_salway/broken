from unittest import TestCase
from unittest.mock import patch, call, Mock

from common.utils.audit_utils import AuditActions, AuditUsers
from transform_and_store_CSO2_data.log_references import LogReference

with patch('boto3.resource') as resource_mock:
    from transform_and_store_CSO2_data.transform_and_store_CSO2_data import lambda_handler


@patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.log')
@patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.get_message_contents_from_event')
class TestTransformAndStore2TestData(TestCase):

    def test_exception_thrown_when_no_record_line_found(self,
                                                        get_message_contents_from_event_mock,
                                                        log_mock):
        # Given
        get_message_contents_from_event_mock.return_value = {'message_id': '12345678', 'record_line': ''}
        event = {'example': 'event'}

        # When
        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(event, {})

        # Then
        get_message_contents_from_event_mock.assert_called_with(event)
        log_mock.assert_called()

        self.assertEqual('Unable to map blank line', context.exception.args[0])

    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.record_is_duplicate', Mock(return_value=False))
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.validate_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.map_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.add_update_test_record')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.audit')
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    def test_update_participant_is_called_correctly(self,
                                                    audit_mock,
                                                    add_update_test_record_mock,
                                                    mapper_mock,
                                                    validator_mock,
                                                    get_message_contents_from_event_mock,
                                                    log_mock):
        # Given
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        mapped_record = {'this': 'mapped', 'source_hash': 'hashed_row'}
        updated_participant = {'participant_id': 'the_participant'}

        get_message_contents_from_event_mock.return_value = record
        validator_mock.return_value = {}
        mapper_mock.return_value = mapped_record
        add_update_test_record_mock.return_value = updated_participant

        # When
        lambda_handler.__wrapped__(event, {})

        # Then
        get_message_contents_from_event_mock.assert_called_with(event)
        mapper_mock.assert_called_with(record['record_line'], {})
        add_update_test_record_mock.assert_called_with(mapped_record)

        log_calls = [call({'log_reference': LogReference.CSO2MAP0001, 'message_id': 12345678}),
                     call({'log_reference': LogReference.CSO2MAP0002}),
                     call({'log_reference': LogReference.CSO2MAP0003}),
                     call({'log_reference': LogReference.CSO2MAP0004}),
                     call({'log_reference': LogReference.CSO2MAP0005}),
                     call({'log_reference': LogReference.CSO2MAP0006}),
                     call({'log_reference': LogReference.CSO2MAP0012}),
                     call({'log_reference': LogReference.CSO2MAP0014}),
                     call({'log_reference': LogReference.CSO2MAP0007}),
                     call({'log_reference': LogReference.CSO2MAP0008})]

        log_mock.assert_has_calls(log_calls)
        audit_mock.assert_called_with(action=AuditActions.STORE_CSO2,
                                      user=AuditUsers.MIGRATION,
                                      participant_ids=['the_participant'])

    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.record_is_duplicate', Mock(return_value=False))
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.validate_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.map_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.add_update_test_record')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.audit')
    def test_mapping_successful_but_participant_not_found(self,
                                                          audit_mock,
                                                          add_update_test_record_mock,
                                                          mapper_mock,
                                                          validator_mock,
                                                          get_message_contents_from_event_mock,
                                                          log_mock):
        # Given
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        mapped_record = {'this': 'mapped'}

        get_message_contents_from_event_mock.return_value = record
        validator_mock.return_value = {}
        mapper_mock.return_value = mapped_record
        add_update_test_record_mock.return_value = None

        # When
        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(event, {})

        # Then
        get_message_contents_from_event_mock.assert_called_with(event)
        mapper_mock.assert_called_with(record['record_line'], {})
        add_update_test_record_mock.assert_called_with(mapped_record)
        audit_mock.assert_not_called()

        log_calls = [call({'log_reference': LogReference.CSO2MAP0001, 'message_id': 12345678}),
                     call({'log_reference': LogReference.CSO2MAP0002}),
                     call({'log_reference': LogReference.CSO2MAP0003}),
                     call({'log_reference': LogReference.CSO2MAP0004}),
                     call({'log_reference': LogReference.CSO2MAP0005}),
                     call({'log_reference': LogReference.CSO2MAP0006}),
                     call({'log_reference': LogReference.CSO2MAP0012}),
                     call({'log_reference': LogReference.CSO2MAP0014}),
                     call({'log_reference': LogReference.CSO2MAP0007}),
                     call({'log_reference': LogReference.CSO2MAP0011})]

        log_mock.assert_has_calls(log_calls)
        self.assertEqual(context.exception.args[0], 'Unable to find matching participant')

    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.record_is_duplicate', Mock(return_value=False))
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.validate_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.map_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.add_update_test_record')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.audit')
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    def test_invalid_data_logged_and_stored(self,
                                            audit_mock,
                                            add_update_test_record_mock,
                                            mapper_mock,
                                            validator_mock,
                                            get_message_contents_from_event_mock,
                                            log_mock):
        # Given
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        validation_error = {'DATA ERROR': 'egg too hot'}
        mapped = {'name': 'ouchie mama', 'source_hash': 'hashed_row'}
        updated_participant = {'participant_id': 'the_participant'}

        get_message_contents_from_event_mock.return_value = record
        validator_mock.return_value = validation_error
        mapper_mock.return_value = mapped
        add_update_test_record_mock.return_value = updated_participant

        # When
        lambda_handler.__wrapped__(event, {})

        # Then
        get_message_contents_from_event_mock.assert_called_with(event)
        mapper_mock.assert_called_with(record['record_line'], validation_error)
        add_update_test_record_mock.assert_called_with(mapped)

        log_calls = [call({'log_reference': LogReference.CSO2MAP0001, 'message_id': 12345678}),
                     call({'log_reference': LogReference.CSO2MAP0002}),
                     call({'log_reference': LogReference.CSO2MAP0003}),
                     call({'log_reference': LogReference.CSO2MAP0009, 'validation_errors': validation_error}),
                     call({'log_reference': LogReference.CSO2MAP0005}),
                     call({'log_reference': LogReference.CSO2MAP0006}),
                     call({'log_reference': LogReference.CSO2MAP0012}),
                     call({'log_reference': LogReference.CSO2MAP0014}),
                     call({'log_reference': LogReference.CSO2MAP0007}),
                     call({'log_reference': LogReference.CSO2MAP0008})]

        log_mock.assert_has_calls(log_calls)

        audit_mock.assert_called_with(action=AuditActions.STORE_CSO2,
                                      user=AuditUsers.MIGRATION,
                                      participant_ids=['the_participant'])

    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.validate_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.map_CSO2_line')
    @patch('common.utils.transform_and_store_utils.missing_necessary_fields')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.audit')
    def test_invalid_data_with_missing_necessary_fields_throws_exception(self,
                                                                         audit_mock,
                                                                         missing_fields_mock,
                                                                         mapper_mock,
                                                                         validator_mock,
                                                                         get_message_contents_from_event_mock,
                                                                         log_mock):
        # Given
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        validation_error = {'NHSNUM': {'schema_error': 'NHS Number is a required value'}}

        get_message_contents_from_event_mock.return_value = record
        validator_mock.return_value = validation_error
        missing_fields_mock.return_value = True

        # When
        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(event, {})

        # Then
        get_message_contents_from_event_mock.assert_called_with(event)
        mapper_mock.assert_not_called()
        audit_mock.assert_not_called()

        log_calls = [call({'log_reference': LogReference.CSO2MAP0001, 'message_id': 12345678}),
                     call({'log_reference': LogReference.CSO2MAP0002}),
                     call({'log_reference': LogReference.CSO2MAP0003}),
                     call({'log_reference': LogReference.CSO2MAP0009, 'validation_errors': validation_error})]

        log_mock.assert_has_calls(log_calls)
        expected_error = 'NHS Number, Test Date and Source Cipher required for ingestion'
        self.assertEqual(expected_error, context.exception.args[0])

    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.add_update_test_record')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.record_is_duplicate')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.map_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.validate_CSO2_line')
    def test_lambda_exits_with_log_if_record_is_duplicate(
            self, validate_line_mock, map_line_mock, duplicate_record_mock,
            update_record_mock, get_message_contents_from_event_mock, log_mock):
        test_event = {'example': 'event'}
        test_record = {'message_id': 12345678, 'record_line': 'line'}
        mapped = {'key': 'value'}

        get_message_contents_from_event_mock.return_value = test_record
        validate_line_mock.return_value = []
        map_line_mock.return_value = mapped
        duplicate_record_mock.return_value = True

        lambda_handler.__wrapped__(test_event, {})

        get_message_contents_from_event_mock.assert_called_once_with(test_event)
        validate_line_mock.assert_called_once_with('line')
        map_line_mock.assert_called_once_with('line', [])
        duplicate_record_mock.assert_called_with(mapped, ['nhs_number', 'test_date', 'slide_number'])
        update_record_mock.assert_not_called()
        log_calls = [
            call({'log_reference': LogReference.CSO2MAP0001, 'message_id': 12345678}),
            call({'log_reference': LogReference.CSO2MAP0002}),
            call({'log_reference': LogReference.CSO2MAP0003}),
            call({'log_reference': LogReference.CSO2MAP0004}),
            call({'log_reference': LogReference.CSO2MAP0005}),
            call({'log_reference': LogReference.CSO2MAP0006}),
            call({'log_reference': LogReference.CSO2MAP0012}),
            call({'log_reference': LogReference.CSO2MAP0013})
        ]
        log_mock.assert_has_calls(log_calls)

    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.validate_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.map_CSO2_line')
    @patch('common.utils.transform_and_store_utils.missing_necessary_fields')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.audit')
    def test_the_audit_is_not_being_created_when_migration_audit_is_toggled_off(self,
                                                                                audit_mock,
                                                                                missing_fields_mock,
                                                                                mapper_mock,
                                                                                validator_mock,
                                                                                get_message_contents_from_event_mock,
                                                                                log_mock):
        # Given
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        validation_error = {'NHSNUM': {'schema_error': 'NHS Number is a required value'}}

        get_message_contents_from_event_mock.return_value = record
        validator_mock.return_value = validation_error
        missing_fields_mock.return_value = True

        # When
        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(event, {})

        # Then
        get_message_contents_from_event_mock.assert_called_with(event)
        mapper_mock.assert_not_called()
        audit_mock.assert_not_called()

        log_calls = [call({'log_reference': LogReference.CSO2MAP0001, 'message_id': 12345678}),
                     call({'log_reference': LogReference.CSO2MAP0002}),
                     call({'log_reference': LogReference.CSO2MAP0003}),
                     call({'log_reference': LogReference.CSO2MAP0009, 'validation_errors': validation_error})]

        log_mock.assert_has_calls(log_calls)
        expected_error = 'NHS Number, Test Date and Source Cipher required for ingestion'
        self.assertEqual(expected_error, context.exception.args[0])

    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.record_is_duplicate', Mock(return_value=False))
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.validate_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.map_CSO2_line')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.add_update_test_record')
    @patch('transform_and_store_CSO2_data.transform_and_store_CSO2_data.audit')
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'FALSE'})
    def test_audit_not_called_when_migration_audit_is_toggled_off(self,
                                                                  audit_mock,
                                                                  add_update_test_record_mock,
                                                                  mapper_mock,
                                                                  validator_mock,
                                                                  get_message_contents_from_event_mock,
                                                                  log_mock):
        # Given
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        validation_error = {'DATA ERROR': 'egg too hot'}
        mapped = {'name': 'ouchie mama', 'source_hash': 'hashed_row'}
        updated_participant = {'participant_id': 'the_participant'}

        get_message_contents_from_event_mock.return_value = record
        validator_mock.return_value = validation_error
        mapper_mock.return_value = mapped
        add_update_test_record_mock.return_value = updated_participant

        log_calls = [call({'log_reference': LogReference.CSO2MAP0001, 'message_id': 12345678}),
                     call({'log_reference': LogReference.CSO2MAP0002}),
                     call({'log_reference': LogReference.CSO2MAP0003}),
                     call({'log_reference': LogReference.CSO2MAP0009, 'validation_errors': validation_error}),
                     call({'log_reference': LogReference.CSO2MAP0005}),
                     call({'log_reference': LogReference.CSO2MAP0006}),
                     call({'log_reference': LogReference.CSO2MAP0012}),
                     call({'log_reference': LogReference.CSO2MAP0014}),
                     call({'log_reference': LogReference.CSO2MAP0007}),
                     call({'log_reference': LogReference.CSO2MAP0008})]

        # When
        lambda_handler.__wrapped__(event, {})

        # Then
        log_mock.assert_has_calls(log_calls)
        audit_mock.assert_not_called()
