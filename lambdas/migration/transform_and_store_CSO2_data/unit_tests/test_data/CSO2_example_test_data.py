CSO2_DATA_VALID_ALL_FIELDS = \
    'BE|9100000922|12|20190627|2|A|0|36|G|61270|G|U0007|Y|860594|20190715|201708|19067309|Y|BE|20190715|stuff 1|stuff 2|stuff 3|S|A12345'  

CSO2_DATA_VALID_MANDATORY_FIELDS = \
    'BE|9100000922|12|20190627|2|A||36|G|61270|G|||||||||20190715||||||'

CSO2_DATA_TOO_MANY_FIELDS = \
    'BE|9100000922|12|20190627|2|A|0|36|G|61270|G|U0007|Y|860594|20190715|201708|19067309|N|BE|||||||||||||||||||||||'

CSO2_DATA_TOO_FEW_FIELDS = \
    'BE|9100000922|12|20190627|2|A|0|36|G|61270|G|U0007|Y|860594|20190715|201708'

CSO2_ALL_FIELDS_WITH_SCHEMA_VALIDATION_INVALID = \
    'A|123456789123456|111|12345678|AB|AB|AB|123|AB|A|AB|ABCDEFG|AB|A|1234567|12345|1|AB|A|1234567|This line is exactly eightyone characters long                                       |This line is exactly eightyone characters long                                       |This line is exactly eightyone characters long                                       |Y|A1234'  

CSO2_ALL_FIELDS_WITH_DATA_VALIDATION_INVALID = \
    'AB|1|12|19691231|A|B|A|61|A|AAAAA|A|A|A|AAAAAA|19691231|196912|AAAAAAAA|A|AA|19691231|€|€|€|N|A12345'

CSO2_NO_FIELDS = '||||||||||||||||||||||||'

DECODED_PARTICIPANT_RECORD_ALL_FIELDS = {
    'nhs_number':       '9100000922',
    'sort_key':         'RESULT#2019-06-27#2020-01-01T12:14:27.123456+00:00',
    'created':          '2020-01-01T12:14:27.123456+00:00',
    'source_hash':      'hashed_row',
    'result_date':      '2019-07-15',
    'test_date':        '2019-06-27',
    'sending_lab':      '61270',
    'result_code':      '2',
    'result':           'Negative',
    'action_code':      'A',
    'action':           'Routine',
    'recall_months':    '36',
    'slide_number':     '19067309',
    'infection_code':   '0',
    'infection_result': 'HPV negative',
    'sender_code':      'U0007',
    'sender_nat':       '860594',
    'source_code':      'G',
    'self_sample':      True,
    'hpv_primary':      True,
    'derived_sender_code': True,
    'practice_code': 'A12345',
    'nhais_cipher':     'BE',
    'migrated_2_data': {
        'received':     '2020-01-01T12:14:27.123456+00:00',
        'value':        CSO2_DATA_VALID_ALL_FIELDS,
        'validation_errors': {}
    }
}

DECODED_PARTICIPANT_RECORD_MANDATORY_FIELDS = {
    'nhs_number':       '9100000922',
    'sort_key':         'RESULT#2019-06-27#2020-01-01T12:14:27.123456+00:00',
    'created':          '2020-01-01T12:14:27.123456+00:00',
    'source_hash':      'hashed_row',
    'result_date':      '2019-07-15',
    'test_date':        '2019-06-27',
    'sending_lab':      '61270',
    'result_code':      '2',
    'result':           'Negative',
    'action_code':      'A',
    'action':           'Routine',
    'recall_months':    '36',
    'slide_number':     '',
    'infection_code':   '',
    'infection_result': '',
    'sender_code':      '',
    'sender_nat':       '',
    'source_code':      'G',
    'self_sample':      False,
    'hpv_primary':      False,
    'derived_sender_code': '',
    'practice_code':    '',
    'nhais_cipher':     'BE',
    'migrated_2_data': {
        'received':     '2020-01-01T12:14:27.123456+00:00',
        'value':        CSO2_DATA_VALID_MANDATORY_FIELDS,
        'validation_errors': {}
    }
}

SCHEMA_VALIDATION_ERRORS = {
    'ACT_CODE': {
        'data_error': "Action code not one of the expected values: ['R', 'A', 'S', 'H']",
        'schema_error': 'Action code must be 1 character long'
    },
    'COMM_1': {
        'schema_error': 'Comm must be between 1 and 80 characters (inclusive)'
    },
    'COMM_2': {
        'schema_error': 'Comm must be between 1 and 80 characters (inclusive)'
    },
    'COMM_3': {
        'schema_error': 'Comm must be between 1 and 80 characters (inclusive)'
    },
    'HPV': {
        'data_error': "HPV not one of the expected values: ['Y', 'N']",
        'schema_error': 'HPV must be 1 character long'
    },
    'INFECT': {
        'data_error': "Infection code not one of the expected values: ['0', '9', 'U', 'Q', '1', '2', '3', '4', '5', '6', '7']",  
        'schema_error': 'Infection code must be 1 character long'
    },
    'INVITE_DATE': {
        'data_error': 'Date must be between 1970-01-01 and 2020-01-01',
        'schema_error': 'Date length must be exactly 6 characters'
    },
    'LAB_LOCAL': {
        'schema_error': 'Lab local must be 1 character long'
    },
    'LAB_NAT': {
        'data_error': 'Lab national code can only contain numbers through 0 - 9',
        'schema_error': 'Lab national code must be 5 characters long'
    },
    'NHSNUM': {
        'schema_error': 'NHS number must be between 1 and 14 characters (inclusive)'
    },
    'NOTIF_DATE': {
        'data_error': 'Invalid date format. Expected format to be %Y%m%d',
        'schema_error': 'Date length must be exactly 8 characters'
    },
    'RES_CODE': {
        'data_error': "Test result code not one of the expected values: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'B', 'E', 'M', 'N', 'G', 'X']",  
        'schema_error': 'Test result code must be 1 character long'
    },
    'RPT_MNTH': {
        'data_error': 'Repeat months must be between 1 and 60 months',
        'schema_error': 'Repeat months must be between 1 and 2 characters (inclusive)'
    },
    'SENDER': {
        'schema_error': 'Sender code must be between 1 and 6 characters (inclusive)'
    },
    'SENDER_EXIST': {
        'data_error': 'Sender exists must be empty or Y',
        'schema_error': 'Sender exist must be 1 character long'
    },
    'SENDER_NAT': {
        'data_error': 'National Sender code can only contain numbers through 0 - 9',
        'schema_error': 'National Sender code must be 6 characters long'
    },
    'SLIDE': {
        'schema_error': 'Slide number must be 8 characters long'
    },
    'SOURCE': {
        'data_error': "Source code not one of the expected values: ['G', 'H', 'X', 'N']",
        'schema_error': 'Source code must be 1 character long'
    },
    'SOURCE_CIPHER': {
        'schema_error': 'Source cipher must be between 2 and 3 characters (inclusive)'
    },
    'TDATE': {
        'data_error': 'Invalid date format. Expected format to be %Y%m%d',
        'schema_error': 'Date was in an invalid format. Expected YYYYMMDD'
    },
    'TEST_CIPHER': {
        'schema_error': 'Test cipher must be between 2 and 3 characters (inclusive)'
    },
    'TEST_INPUT': {
        'data_error': 'Invalid date format. Expected format to be %Y%m%d',
    },
    'TEST_SEQ': {
        'schema_error': 'Test sequence number must be between 1 and 2 characters (inclusive)'
    },
    'SAMP_METH': {
        'schema_error': 'Sample Method must be S or null'
    },
    'PRAC_CODE': {
        'schema_error': 'GP Practice Code must be 6 characters long'
    },
}

DECODED_PARTICIPANT_SCHEMA_ERRORS = {
    'nhs_number': '123456789123456',
    'sort_key': 'RESULT#1234-56-78#2020-01-01T12:14:27.123456+00:00',
    'created': '2020-01-01T12:14:27.123456+00:00',
    'source_hash': 'hashed_row',
    'result_date': '1234-56-7',
    'test_date': '1234-56-78',
    'sending_lab': 'A',
    'result_code': 'AB',
    'result': 'AB',
    'action_code': 'AB',
    'action': 'AB',
    'recall_months': '123',
    'slide_number': '1',
    'infection_code': 'AB',
    'infection_result': '',
    'sender_code': 'ABCDEFG',
    'sender_nat': 'A',
    'source_code': 'AB',
    'self_sample': False,
    'hpv_primary': False,
    'derived_sender_code': True,
    'practice_code': 'A1234',
    'nhais_cipher': 'A',
    'migrated_2_data': {
        'received': '2020-01-01T12:14:27.123456+00:00',
        'value': CSO2_ALL_FIELDS_WITH_SCHEMA_VALIDATION_INVALID,
        'validation_errors': SCHEMA_VALIDATION_ERRORS
    }
}

DATA_VALIDATION_ERRORS = {
    'ACT_CODE': {
        'data_error': "Action code not one of the expected values: ['R', 'A', 'S', 'H']"
    },
    'COMM_1': {
        'data_error': 'Note text can only contains letters, numbers and punctuation'
    },
    'COMM_2': {
        'data_error': 'Note text can only contains letters, numbers and punctuation'
    },
    'COMM_3': {
        'data_error': 'Note text can only contains letters, numbers and punctuation'
    },
    'HPV': {
        'data_error': "HPV not one of the expected values: ['Y', 'N']"
    },
    'INFECT': {
        'data_error': "Infection code not one of the expected values: ['0', '9', 'U', 'Q', '1', '2', '3', '4', '5', '6', '7']"  
    },
    'INVITE_DATE': {
        'data_error': 'Date must be between 1970-01-01 and 2020-01-01'
    },
    'LAB_NAT': {
        'data_error': 'Lab national code can only contain numbers through 0 - 9'
    },
    'NOTIF_DATE': {
        'data_error': 'Date must be between 1970-01-01 and 2020-01-01'
    },
    'RES_CODE': {
        'data_error': "Test result code not one of the expected values: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'B', 'E', 'M', 'N', 'G', 'X']"  
    },
    'RPT_MNTH': {
        'data_error': 'Repeat months must be between 1 and 60 months'
    },
    'SENDER_EXIST': {
        'data_error': 'Sender exists must be empty or Y'
    },
    'SENDER_NAT': {
        'data_error': 'National Sender code can only contain numbers through 0 - 9'
    },
    'SLIDE': {
        'data_error': 'Slide number can only contain numbers through 0 - 9'
    },
    'SOURCE': {
        'data_error': "Source code not one of the expected values: ['G', 'H', 'X', 'N']"
    },
    'TDATE': {
        'data_error': 'Date must be between 1970-01-01 and 2020-01-01'
    },
    'TEST_INPUT': {
        'data_error': 'Date must be between 1970-01-01 and 2020-01-01'
    },
    'SAMP_METH': {
        'schema_error': 'Sample Method must be S or null'
    },
}

DECODED_PARTICIPANT_DATA_ERRORS = {
    'nhs_number': '1',
    'sort_key': 'RESULT#1969-12-31#2020-01-01T12:14:27.123456+00:00',
    'created': '2020-01-01T12:14:27.123456+00:00',
    'source_hash': 'hashed_row',
    'result_date': '1969-12-31',
    'test_date': '1969-12-31',
    'sending_lab': 'AAAAA',
    'result_code': 'A',
    'result': 'A',
    'action_code': 'B',
    'action': 'B',
    'recall_months': '61',
    'slide_number': 'AAAAAAAA',
    'infection_code': 'A',
    'infection_result': '',
    'sender_code': 'A',
    'sender_nat': 'AAAAAA',
    'source_code': 'A',
    'self_sample': False,
    'hpv_primary': False,
    'derived_sender_code': True,
    'practice_code': 'A12345',
    'nhais_cipher': 'AB',
    'migrated_2_data': {
        'received': '2020-01-01T12:14:27.123456+00:00',
        'value': CSO2_ALL_FIELDS_WITH_DATA_VALIDATION_INVALID,
        'validation_errors': DATA_VALIDATION_ERRORS
    }
}

MISSING_FIELDS_ERRORS = {
    'ACT_CODE': {'schema_error': 'Action code is a required value'},
    'LAB_LOCAL': {'schema_error': 'Lab local is a required value'},
    'LAB_NAT': {'schema_error': 'Lab national code is a required value'},
    'NHSNUM': {'schema_error': 'NHS number is a required value'},
    'RES_CODE': {'schema_error': 'Test result code is a required value'},
    'RPT_MNTH': {'schema_error': 'Repeat months is a required value'},
    'SOURCE': {'schema_error': 'Source code is a required value'},
    'SOURCE_CIPHER': {'schema_error': 'Source cipher is a required value'},
    'TDATE': {'schema_error': 'Date is a required value'},
    'TEST_SEQ': {'schema_error': 'Test sequence number is a required value'}
}
