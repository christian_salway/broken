from transform_and_store_CSO2_data.cso2_fields import (
    CSO2_Fields,
    SCHEMA_FUNCTIONS,
    DATA_FUNCTIONS
)
from common.utils.edifact_dictionaries import (
    CYTOLOGY_RESULTS,
    ACTIONS,
    INFECTION_CODES
)
from common.utils.transform_and_store_utils import get_hashed_value
from datetime import datetime, timezone


DELIMITER = '|'
CSO2_EXPECTED_FIELD_COUNT = 24
EXTRA_FIELDS_LIMIT = 5


def validate_CSO2_line(raw_data):
    data_list = raw_data.split(DELIMITER)
    if len(data_list) < CSO2_EXPECTED_FIELD_COUNT or len(data_list) > CSO2_EXPECTED_FIELD_COUNT + EXTRA_FIELDS_LIMIT:
        raise Exception(
            F'Malformed input: Number of fields in CSO2 result must be between {CSO2_EXPECTED_FIELD_COUNT} and '
            F'{CSO2_EXPECTED_FIELD_COUNT + EXTRA_FIELDS_LIMIT}')

    validation_errors = {}
    for field, validate_schema, validate_data, value in zip(CSO2_Fields, SCHEMA_FUNCTIONS, DATA_FUNCTIONS, data_list):

        schema_error = validate_schema(value) or {}
        data_error = {}
        if value:
            data_error = validate_data(value) or {}

        if schema_error or data_error:
            validation_errors[field] = {**schema_error, **data_error}

    return validation_errors


def map_CSO2_line(raw_data, validation_errors):
    data_list = raw_data.split(DELIMITER)
    action_code = data_list[CSO2_Fields.index('ACT_CODE')]
    infection_code = data_list[CSO2_Fields.index('INFECT')]
    result_code = data_list[CSO2_Fields.index('RES_CODE')]
    now = datetime.now(timezone.utc)

    mapped_data = {
            'nhs_number':       data_list[CSO2_Fields.index('NHSNUM')],
            'sort_key':         _determine_sort_key(data_list, now),
            'created':          now.isoformat(),
            'source_hash':      get_hashed_value(raw_data),
            'test_date':        format_date(data_list[CSO2_Fields.index('TDATE')]),
            'result_date':      format_date(data_list[CSO2_Fields.index('TEST_INPUT')]),
            'sending_lab':      data_list[CSO2_Fields.index('LAB_NAT')],
            'result_code':      result_code,
            'result':           CYTOLOGY_RESULTS.get(result_code, result_code),
            'action_code':      action_code,
            'action':           ACTIONS.get(action_code, action_code),
            'recall_months':    data_list[CSO2_Fields.index('RPT_MNTH')].replace('-', ''),
            'slide_number':     data_list[CSO2_Fields.index('SLIDE')],
            'infection_code':   infection_code,
            'infection_result': _determine_infection_result(infection_code),
            'sender_code':      data_list[CSO2_Fields.index('SENDER')],
            'sender_nat':       data_list[CSO2_Fields.index('SENDER_NAT')],
            'source_code':      data_list[CSO2_Fields.index('SOURCE')],
            'hpv_primary':      True if data_list[CSO2_Fields.index('HPV')] == 'Y' else False,
            'self_sample':      data_list[CSO2_Fields.index('SAMP_METH')] in ['S', 's'],
            'derived_sender_code': True if data_list[CSO2_Fields.index('SENDER_NAT')] else '',
            'practice_code':    data_list[CSO2_Fields.index('PRAC_CODE')],
            'nhais_cipher':     data_list[CSO2_Fields.index('SOURCE_CIPHER')],
            'migrated_2_data': {
                'received':     now.isoformat(),
                'value':        raw_data,
                'validation_errors': validation_errors
            }
    }

    return mapped_data


def _determine_infection_result(infection_code):
    return INFECTION_CODES.get(infection_code, '')


def _determine_sort_key(data_list, received):
    sample_date = format_date(data_list[CSO2_Fields.index('TDATE')])
    received_time = received.isoformat()
    return f"RESULT#{sample_date}#{received_time}"


def format_date(date_string):
    if date_string:
        return'{}-{}-{}'.format(date_string[0:4], date_string[4:6], date_string[6:])
    else:
        return ''
