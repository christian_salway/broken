from common.utils.import_migration_data_validation_utils import (
    ImportMigrationDataValidation as data
)
from common.utils.import_migration_schema_validation_utils import (
    ImportMigrationSchemaValidation as schema
)


SCHEMA = schema()
DATA = data()


def NO_VALIDATION(_):
    return None


def required_date(date):
    return SCHEMA.validate_date(date, True)


CSO2_Fields = (
    'SOURCE_CIPHER',  # Identifies the instance of NHAIS this record comes from
    'NHSNUM',         # NHS Number
    'TEST_SEQ',       # Test Sequence number
    'TDATE',          # Test date (ccyymmd)
    'RES_CODE',       # Test result code (0-9,B,E,M,N,G,X)
    'ACT_CODE',       # Action Code (RASH)
    'INFECT',         # Infection Code (0,9,U,Q) (Historically 1-6 may be held).
    'RPT_MNTH',       # Repeat months (-- if too large)
    'LAB_LOCAL',      # Lab Local Code - This is a code specific to the NHAIS instance that it is used on.
    'LAB_NAT',        # Lab National Code - Code identifying the Lab nationally
    'SOURCE',         # Source Code (G,H,X,N)
    'SENDER',         # Sender Code (either local GP code or local sender code)
    'SENDER_EXIST',   # Sender Exists Indicator (Either Y or Null).
    'SENDER_NAT',     # National Sender Code
    'NOTIF_DATE',     # Notification Date
    'INVITE_DATE',    # Invitation Date (CCYYMM format) - Can also be 4 or 6 "-"
    'SLIDE',          # Slide Number
    'HPV',            # HPV Primary Screening Marker
    'TEST_CIPHER',    # NHAIS Cipher doing test - Uppercase only
    'TEST_INPUT',     # Input Date of Test
    'COMM_1',         # Comments Line 1
    'COMM_2',         # Comments Line 2
    'COMM_3',         # Comments Line 3
    'SAMP_METH',      # Sample Method - Either S or null.
    'PRAC_CODE',      # GP Practice code at time of test. Present if NHAIS has it and only for tests after 01/01/2000
)


SCHEMA_FUNCTIONS = (
    SCHEMA.validate_source_cipher,
    SCHEMA.validate_nhs_number,
    SCHEMA.validate_test_sequence_number,
    required_date,
    SCHEMA.validate_test_result_code,
    SCHEMA.validate_action_code,
    SCHEMA.validate_infection_code,
    SCHEMA.validate_repeat_months,
    SCHEMA.validate_lab_local,
    SCHEMA.validate_lab_national_code,
    SCHEMA.validate_source_code,
    SCHEMA.validate_sender_code,
    SCHEMA.validate_sender_exist,
    SCHEMA.validate_national_sender_code,
    SCHEMA.validate_date,
    SCHEMA.validate_invite_date,
    SCHEMA.validate_slide_number,
    SCHEMA.validate_hpv,
    SCHEMA.validate_test_cipher,
    NO_VALIDATION,
    SCHEMA.validate_comm,
    SCHEMA.validate_comm,
    SCHEMA.validate_comm,
    SCHEMA.validate_samp_meth,
    SCHEMA.validate_gp_practice_code,
)


DATA_FUNCTIONS = (
    NO_VALIDATION,
    NO_VALIDATION,
    NO_VALIDATION,
    DATA.validate_date_with_days_after_1970,
    DATA.validate_test_result_code,
    DATA.validate_action_code,
    DATA.validate_infection_code,
    DATA.validate_repeat_months,
    NO_VALIDATION,
    DATA.validate_lab_national_code,
    DATA.validate_source_code,
    NO_VALIDATION,
    DATA.validate_sender_exists,
    DATA.validate_national_sender_code,
    DATA.validate_date_with_days_after_1970,
    DATA.validate_date_without_days_after_1970,
    DATA.validate_slide_number,
    DATA.validate_hpv_value,
    NO_VALIDATION,
    DATA.validate_date_with_days_after_1970,
    DATA.validate_text,
    DATA.validate_text,
    DATA.validate_text,
    NO_VALIDATION,
    NO_VALIDATION
)
