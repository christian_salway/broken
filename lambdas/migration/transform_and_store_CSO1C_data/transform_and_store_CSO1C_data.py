import os
from common.utils.transform_and_store_utils import (
    get_message_contents_from_event,
    update_participant_by_nhs_number,
    remove_empty_values,
    create_replace_record_in_participant_table
)
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from common.utils.audit_utils import AuditActions, AuditUsers, audit
from transform_and_store_CSO1C_data.log_references import LogReference
from transform_and_store_CSO1C_data.src.map_and_validate_CSO1C_data import map_and_validate_CSO1C_data


@lambda_entry_point
def lambda_handler(event, context):
    message_contents = get_message_contents_from_event(event)
    log({'log_reference': LogReference.TRANS1C0001})

    record_line = message_contents['record_line']

    (
        nhs_number,
        fields_to_add,
        fields_to_remove,
        episode_details,
        logging_fields
    ) = map_and_validate_CSO1C_data(record_line)
    log({'log_reference': LogReference.TRANS1C0002})

    cleaned_fields_to_add = remove_empty_values(fields_to_add)
    log({'log_reference': LogReference.TRANS1C0003})

    participant = update_participant_by_nhs_number(nhs_number, cleaned_fields_to_add, fields_to_remove)

    if participant:
        log({'log_reference': LogReference.TRANS1C0004, **logging_fields.get('recall', {})})
        if os.environ.get('ENABLE_MIGRATION_AUDIT') != 'FALSE':
            audit(action=AuditActions.STORE_CSO1C,
                  user=AuditUsers.MIGRATION,
                  participant_ids=[participant['participant_id']])

        if episode_details:
            log({'log_reference': LogReference.TRANS1C0008})
            create_episode_record(participant, episode_details, logging_fields)


def create_episode_record(participant, episode, logging_fields):
    episode = remove_empty_values(episode)
    registered_gp_practice_code = participant.get('registered_gp_practice_code')

    episode['participant_id'] = participant['participant_id']
    if registered_gp_practice_code:
        test_due_date = episode['test_due_date']
        episode['registered_gp_practice_code'] = registered_gp_practice_code
        episode['registered_gp_practice_code_test_due_date'] = f'{registered_gp_practice_code}#{test_due_date}'
    log({'log_reference': LogReference.TRANS1C0009})

    create_replace_record_in_participant_table(episode)
    log(
        {
            'log_reference': LogReference.TRANS1C0010,
            'stat_code': logging_fields.get('stat_code', 'NOT_SET'),
            'episode_status': episode['status']
        }
    )
    audit(
        action=AuditActions.CREATE_EPISODE,
        user=AuditUsers.MIGRATION,
        participant_ids=[participant['participant_id']],
        nhs_numbers=[participant.get('nhs_number', '')]
    )
