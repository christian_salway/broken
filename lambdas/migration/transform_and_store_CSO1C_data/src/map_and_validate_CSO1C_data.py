from datetime import datetime, timezone
from typing import Dict
from common.utils.import_migration_data_validation_utils import (
    ImportMigrationDataValidation as data, VALID_CEASE_CODES
)
from common.utils.import_migration_schema_validation_utils import (
    ImportMigrationSchemaValidation as schema
)
from common.migration_fields.CSO1C_fields import CSO1C_FIELDS
from common.log import log
from common.models.participant import ParticipantStatus, EpisodeStatus
from transform_and_store_CSO1C_data.log_references import LogReference
from common.utils.transform_and_store_utils import missing_necessary_fields

STATUS_DICTIONARY = {
    'N': ParticipantStatus.ROUTINE,
    'L': ParticipantStatus.CALLED,
    'R': ParticipantStatus.REPEAT_ADVISED,
    'I': ParticipantStatus.INADEQUATE,
    'S': ParticipantStatus.SUSPENDED,
    'C': ParticipantStatus.CEASED,
}

VALID_RECALL_MAP = {
    'C': ['C', 'L'],
    'I': ['1', '2', 'F', 'G', 'H', 'L', 'N', 'P', 'Z'],
    'L': ['1', '2', 'F', 'G', 'H', 'L', 'N', 'P', 'Z'],
    'N': ['1', '2', 'F', 'G', 'H', 'L', 'N', 'P', 'Z'],
    'R': ['1', '2', 'F', 'G', 'H', 'L', 'N', 'P', 'Z'],
    'S': ['1', '2', 'F', 'G', 'H', 'L', 'N', 'P', 'S', 'Z']
}


def map_and_validate_CSO1C_data(raw_data: str):
    data_fields = raw_data.split('|')

    errors = validate_CSO1C_data(raw_data, data_fields)

    if errors:
        log({'log_reference': LogReference.TRANS1C0005, 'validation_errors': errors})
        if missing_necessary_fields(errors, ['NHSNUM', 'NTD', 'RECALL_TYPE']):
            log({'log_reference': LogReference.TRANS1C0006})
            raise Exception('NHS Number, Next Test Due Date, and Recall Type are required for ingestion')
        if incorrect_status(errors):
            log({'log_reference': LogReference.TRANS1C0006})
            raise Exception('Recall Type must be one of expected values N, L, R, I, S, C')

    return _map_CSO1C_data(raw_data, data_fields, errors)


def validate_CSO1C_data(raw_data, data_fields):
    errors = {}

    _validate(data_fields, 'SOURCE_CIPHER', schema().validate_source_cipher, errors)
    _validate(data_fields, 'NHSNUM', schema().validate_nhs_number, errors)

    _validate(data_fields, 'NTD', schema().validate_date, errors, None, True)
    _validate(data_fields, 'NTD', data().validate_next_test_due_date, errors)

    _validate(data_fields, 'RECALL_TYPE', schema().validate_recall_type, errors)
    _validate(data_fields, 'RECALL_TYPE', data().validate_recall_type, errors)

    _validate(data_fields, 'RECALL_STAT', schema().validate_recall_stat, errors)
    _validate(data_fields, 'RECALL_STAT', data().validate_recall_stat, errors)

    _validate(data_fields, 'FNR_COUNT', schema().validate_fnr_count, errors)
    _validate(data_fields, 'FNR_COUNT', data().validate_fnr_count, errors)

    _validate(data_fields, 'POS_DOUBT', schema().validate_pos_doubt, errors)
    _validate(data_fields, 'POS_DOUBT', data().validate_pos_doubt, errors)

    _validate(data_fields, 'CEASED_FLAG', schema().validate_cease_code, errors)
    _validate(data_fields, 'CEASED_FLAG', data().validate_cease_code, errors)

    _validate(data_fields, 'CEASED_CIPHER', schema().validate_cease_cipher, errors)

    _validate(data_fields, 'NOTES1', schema().validate_notes, errors, None, 1)
    _validate(data_fields, 'NOTES1', data().validate_text, errors)

    _validate(data_fields, 'NOTES2', schema().validate_notes, errors, None, 2)
    _validate(data_fields, 'NOTES2', data().validate_text, errors)

    _validate(data_fields, 'NOTES3', schema().validate_notes, errors, None, 3)
    _validate(data_fields, 'NOTES3', data().validate_text, errors)

    _validate(data_fields, 'LDN', schema().validate_ldn, errors)
    _validate(data_fields, 'LDN', data().validate_ldn, errors)

    _validate(data_fields, 'OAPD', schema().validate_date, errors)
    _validate(data_fields, 'OAPD', data().validate_oapd, errors)

    _validate(data_fields, 'INV_DATE', schema().validate_invite_date, errors)

    _validate(data_fields, 'PATH_ID', schema().validate_path_id, errors)

    _validate(data_fields, 'PNL_FNR', schema().validate_pnl_fnr, errors)
    _validate(data_fields, 'PNL_FNR', data().validate_pnl_fnr, errors)

    return errors


def _map_CSO1C_data(raw_data, data_fields, validation_errors):
    nhs_number = data_fields[CSO1C_FIELDS.index('NHSNUM')]
    recall_type = data_fields[CSO1C_FIELDS.index('RECALL_TYPE')]
    invitation_date = data_fields[CSO1C_FIELDS.index('INV_DATE')]
    convert_inv_date = _convert_partial_date(invitation_date)
    path_id = data_fields[CSO1C_FIELDS.index('PATH_ID')]
    recall_stat = data_fields[CSO1C_FIELDS.index('RECALL_STAT')]
    if recall_type == 'C' and recall_stat == 'C':
        is_ceased = True
    else:
        is_ceased = data_fields[CSO1C_FIELDS.index('CEASED_FLAG')] in VALID_CEASE_CODES

    mapped_CSO1C_data = {
        'status': STATUS_DICTIONARY.get(recall_type, recall_type),
        'is_ceased': is_ceased,
        'invited_date': convert_inv_date,
        'path_id': path_id,
        'migrated_1C_data': {
            'value': raw_data,
            'received': datetime.now(timezone.utc).isoformat(),
            'validation_errors': validation_errors
        }
    }

    episode_details = get_episode_details(data_fields)
    logging_fields = {
        'recall': _validate_recall(data_fields)
    }

    if episode_details:
        logging_fields['stat_code'] = data_fields[CSO1C_FIELDS.index('RECALL_STAT')]

    if is_ceased:
        fields_to_delete_from_participant = ['next_test_due_date']
        return nhs_number, mapped_CSO1C_data, fields_to_delete_from_participant, episode_details, logging_fields
    else:
        mapped_CSO1C_data['next_test_due_date'] = format_date(data_fields[CSO1C_FIELDS.index('NTD')], date_only=True)
        return nhs_number, mapped_CSO1C_data, None, episode_details, logging_fields


def _validate(data_fields, field_name, validation_func, errors, default_value=None, *args):
    field_value = default_value
    if field_value is None:
        field_value = get_record_field_value(data_fields, CSO1C_FIELDS.index(field_name))

    validation_response = validation_func(str(field_value), *args)
    if validation_response:
        if field_name in errors:
            errors[field_name].update(validation_response)
        else:
            errors.update({field_name: validation_response})


def format_date(date_value, date_only=False, set_to_default=False):
    try:
        default_date = str(datetime.now(timezone.utc).date())
        if date_value:
            if date_only:
                return str(datetime.strptime(date_value, '%Y%m%d').date())
            else:
                return datetime.strptime(date_value, '%Y%m%d').isoformat()

        if set_to_default:
            return default_date
    except ValueError:
        return None


def get_record_field_value(record_fields, index, default_value=None):
    if index > len(record_fields) - 1:
        return default_value
    else:
        return record_fields[index]


def incorrect_status(errors):
    return 'Recall Type not one of the expected values' in errors.get('RECALL_TYPE', {}).get('data_error', '')


def _convert_partial_date(partial_date, must_be_valid=False):

    if len(partial_date) != 6 or not partial_date.isdigit():
        if must_be_valid:
            return ''
        else:
            return partial_date

    year = partial_date[:4]
    month = partial_date[4:]
    day = '01'

    return f'{year}-{month}-{day}'


def _convert_full_date(fulldate):
    if len(fulldate) != 8 or not fulldate.isdigit():
        return fulldate

    year = fulldate[:4]
    month = fulldate[4:6]
    day = fulldate[6:]

    return f'{year}-{month}-{day}'


def get_episode_details(data_fields):
    next_test_due_date = _convert_full_date(data_fields[CSO1C_FIELDS.index('NTD')])
    recall_stat = data_fields[CSO1C_FIELDS.index('RECALL_STAT')]
    notif_date = _convert_full_date(data_fields[CSO1C_FIELDS.index('NOTIF_DATE')])
    pnl_date = _convert_partial_date(data_fields[CSO1C_FIELDS.index('PNL_DATE')], True)
    pnl_fnr = data_fields[CSO1C_FIELDS.index('PNL_FNR')]
    invited_date = _convert_partial_date(data_fields[CSO1C_FIELDS.index('INV_DATE')])

    if pnl_fnr and pnl_fnr != 'UA':
        pnl_fnr = _convert_full_date(pnl_fnr)

    reminded_date = ''
    nrl_date = ''

    if recall_stat == '2':
        episode_status = EpisodeStatus.REMINDED
        reminded_date = notif_date
    elif recall_stat == 'G' or recall_stat == 'N':
        if not pnl_fnr or pnl_fnr == 'UA':
            pnl_fnr = ''
            episode_status = EpisodeStatus.PNL
        else:
            episode_status = EpisodeStatus.ACCEPTED
        pnl_date = notif_date
        invited_date = ''
    elif recall_stat == 'P':
        episode_status = EpisodeStatus.INVITED
        pnl_date = notif_date
    elif recall_stat == 'H' or recall_stat == 'F':
        episode_status = EpisodeStatus.NRL
        nrl_date = notif_date
    elif recall_stat == 'L':
        episode_status = EpisodeStatus.LOGGED
        nrl_date = notif_date
    else:
        return None

    episode_details = {
        'sort_key': f'EPISODE#{next_test_due_date}',
        'created': datetime.now(timezone.utc).isoformat(),
        'test_due_date': next_test_due_date,
        'user_id': 'transform_and_store_CSO1C_data',
        'status': episode_status,
        'live_record_status': episode_status,
        'pnl_date': pnl_date,
        'accepted_date': pnl_fnr,
        'invited_date': invited_date,
        'reminded_date': reminded_date,
        'nrl_date': nrl_date,
    }

    return episode_details


def _validate_recall(data_fields) -> Dict:
    recall_type = data_fields[CSO1C_FIELDS.index('RECALL_TYPE')]
    recall_stat = data_fields[CSO1C_FIELDS.index('RECALL_STAT')]
    return {
        'type': recall_type,
        'stat': recall_stat,
        'valid': recall_type in VALID_RECALL_MAP and recall_stat in VALID_RECALL_MAP[recall_type]
    }
