import os
import json
from mock import call, patch, Mock
from datetime import datetime, timezone
from unittest.case import TestCase
from ddt import ddt, data, unpack
from common.utils import (
    increment_date_by_years, generate_random_length_string, load_json_doc)
from common.models.participant import ParticipantStatus
from transform_and_store_CSO1C_data.log_references import LogReference


@patch('transform_and_store_CSO1C_data.src.map_and_validate_CSO1C_data.datetime', Mock(wraps=datetime))
@patch('transform_and_store_CSO1C_data.src.map_and_validate_CSO1C_data.log')
@patch('transform_and_store_CSO1C_data.src.map_and_validate_CSO1C_data.datetime')
class TestCSO1CDataMapper(TestCase):

    FIXED_NOW_TIME = datetime(2020, 3, 3, 9, 45, 23, 123456, tzinfo=timezone.utc)
    FIXED_NOW_DATE = datetime(2020, 3, 3, tzinfo=timezone.utc)

    @classmethod
    @patch('boto3.resource', Mock())
    def setUpClass(cls, *args):
        import transform_and_store_CSO1C_data.src.map_and_validate_CSO1C_data as mapper
        global mapper

    def test_map_and_validate_CSO1C_data_returns_expected_result_no_validation_warnings(self, mock_now, mock_log):
        # Arrange
        expected_nhs_number = '4056742317'
        test_data = f'BD|{expected_nhs_number}|20060519|N|2|20|Y|D|BD||||L2|19920317|20200101|202001|202001|1|UA'
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mock_now.return_value = self.FIXED_NOW_DATE
        expected_data = {
            'status': ParticipantStatus.ROUTINE,
            'is_ceased': True,
            'invited_date': '2020-01-01',
            'path_id': '1',
            'migrated_1C_data': {
                'value': test_data,
                'received': '2020-03-03T09:45:23.123456+00:00',
                'validation_errors': {}
            }
        }
        expected_episode_data = {
            'sort_key': 'EPISODE#2006-05-19',
            'created': '2020-03-03T09:45:23.123456+00:00',
            'test_due_date': '2006-05-19',
            'user_id': 'transform_and_store_CSO1C_data',
            'status': 'REMINDED',
            'live_record_status': 'REMINDED',
            'pnl_date': '2020-01-01',
            'accepted_date': 'UA',
            'invited_date': '2020-01-01',
            'reminded_date': '2020-01-01',
            'nrl_date': '',
        }
        expected_logged_fields = {
            'stat_code': '2',
            'recall': {
                'stat': '2',
                'type': 'N',
                'valid': True
            }
        }
        expected_fields_to_delete = ['next_test_due_date']

        # Act
        (
            actual_nhs_number,
            actual_data,
            actual_fields_to_delete,
            actual_episode_data,
            actual_logged_fields
        ) = mapper.map_and_validate_CSO1C_data(test_data)

        # Assert
        self.assertEqual(expected_nhs_number, actual_nhs_number)
        self.assertDictEqual(expected_data, actual_data)
        self.assertEqual(expected_episode_data, actual_episode_data)
        self.assertEqual(expected_fields_to_delete, actual_fields_to_delete)
        self.assertDictEqual(expected_logged_fields, actual_logged_fields)

    def test_map_and_validate_CSO1C_data_returns_schema_validation_errors_when_migration_data_is_invalid(self, _,
                                                                                                         mock_log):
        # Arrange
        notes_1 = generate_random_length_string(61)
        notes_2 = generate_random_length_string(61)
        notes_3 = generate_random_length_string(61)
        invalid_test_data = 'B|405674231723456|200605191|N|CC|2423|YES|D1|B|' +\
                            f'{notes_1}|{notes_2}|{notes_3}|L|1988||44|||15'
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        path = os.path.join(os.path.dirname(__file__), 'test_data')
        expected_error_dict = load_json_doc(path, 'validation_warnings_1.json').get('validation_errors')

        log_calls = [
            call({'log_reference': LogReference.TRANS1C0005, 'validation_errors': expected_error_dict})]

        # Act
        mapper.map_and_validate_CSO1C_data(invalid_test_data)

        # Assert
        mock_log.assert_has_calls(log_calls)

    def test_map_and_validate_CSO1C_data_returns_data_validation_errors_when_migration_data_is_invalid(self, mock_now,
                                                                                                       mock_log):
        # Arrange
        notes_1 = generate_random_length_string(35)
        notes_2 = '🛑🛑🛑'
        notes_3 = generate_random_length_string(59)
        invalid_test_data = \
            f'BDBD|1234567890|32000101|L|A|21|X|A|AA|{notes_1}|{notes_2}|{notes_3}|LL|19000101||202001||1|15'
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mock_now.return_value = self.FIXED_NOW_DATE

        path = os.path.join(os.path.dirname(__file__), 'test_data')
        validation_warnings = load_json_doc(path, 'validation_warnings_2.json').get('validation_errors')

        expected_error_dict = increment_date_by_years(json.dumps(validation_warnings))

        log_calls = [
            call({'log_reference': LogReference.TRANS1C0005, 'validation_errors': expected_error_dict})]

        # Act
        mapper.map_and_validate_CSO1C_data(invalid_test_data)

        # Assert
        mock_log.assert_has_calls(log_calls)

    def test_another_test(self, mock_now, mock_log):
        # Arrange
        invalid_test_data = 'BDBD|4056742317|20060519|C|C|21|Y|D|BD||🛑🛑🛑||L1|19880101||202001||01||'
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mock_now.return_value = self.FIXED_NOW_DATE
        expected_error_dict = {
            'SOURCE_CIPHER': {
                'schema_error': 'Source cipher must be between 2 and 3 characters (inclusive)'
            },
            'FNR_COUNT': {
                'data_error': 'FNR Count must be between 0 and 20 (inclusive)'
            },
            'NOTES2': {
                'data_error': 'Note text can only contains letters, numbers and punctuation'
            }
        }

        log_calls = [
            call({'log_reference': LogReference.TRANS1C0005, 'validation_errors': expected_error_dict})]

        # Act
        mapper.map_and_validate_CSO1C_data(invalid_test_data)

        # Assert
        mock_log.assert_has_calls(log_calls)

    def test_map_and_validate_CSO1C_data_returns_expected_result_with_next_test_due_date_removed(self, mock_now, _):
        # Arrange
        test_data = 'BD|4056742317|20060519|C|C|20|Y|C|BD||||L2|19920317||202001||1|UA'
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mock_now.return_value = self.FIXED_NOW_DATE
        expected_nhs_number = '4056742317'
        expected_data = {
            'is_ceased': True,
            'status': ParticipantStatus.CEASED,
            'invited_date': '2020-01-01',
            'path_id': '1',
            'migrated_1C_data': {
                'value': test_data,
                'received': '2020-03-03T09:45:23.123456+00:00',
                'validation_errors': {}
            }
        }
        expected_fields_to_delete = ['next_test_due_date']
        expected_episode_data = None
        expected_logged_fields = {
            'recall': {
                'stat': 'C',
                'type': 'C',
                'valid': True
            }
        }

        # Act
        (
            actual_nhs_number,
            actual_data,
            actual_fields_to_delete,
            actual_episode_data,
            actual_logged_fields
        ) = mapper.map_and_validate_CSO1C_data(test_data)

        # Assert
        self.assertEqual(expected_nhs_number, actual_nhs_number)
        self.assertDictEqual(expected_data, actual_data)
        self.assertEqual(expected_episode_data, actual_episode_data)
        self.assertEqual(expected_fields_to_delete, actual_fields_to_delete)
        self.assertDictEqual(expected_logged_fields, actual_logged_fields)

    def test_map_and_validate_CSO1C_data_returns_expected_result_with_next_test_due_date(self, mock_now, _):
        # Arrange
        test_data = 'BD|4056742317|20060519|L|C|20|Y||BD||||L2|19920317||202001||1|UA'
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mock_now.return_value = self.FIXED_NOW_DATE
        expected_nhs_number = '4056742317'
        expected_data = {
            'next_test_due_date': '2006-05-19',
            'is_ceased': False,
            'status': ParticipantStatus.CALLED,
            'invited_date': '2020-01-01',
            'path_id': '1',
            'migrated_1C_data': {
                'value': test_data,
                'received': '2020-03-03T09:45:23.123456+00:00',
                'validation_errors': {}
            }
        }
        expected_fields_to_delete = None
        expected_episode_data = None
        expected_logged_fields = {'recall': {'stat': 'C', 'type': 'L', 'valid': False}}

        # Act
        (
            actual_nhs_number,
            actual_data,
            actual_fields_to_delete,
            actual_episode_data,
            actual_logged_fields
        ) = mapper.map_and_validate_CSO1C_data(test_data)

        # Assert
        self.assertEqual(expected_nhs_number, actual_nhs_number)
        self.assertDictEqual(expected_data, actual_data)
        self.assertEqual(expected_episode_data, actual_episode_data)
        self.assertEqual(expected_fields_to_delete, actual_fields_to_delete)
        self.assertDictEqual(expected_logged_fields, actual_logged_fields)

    def test_map_and_validate_CSO1C_data_throws_exception_when_required_fields_are_missing(self, mock_now, mock_log):

        testing_data = [
            'BDBD|1010101010||C|C|21|Y|D|BD||$$$!||L1|19880101',  # missing ntd
            'BDBD||20060519|C|C|21|Y|D|BD||$$$!||L1|19880101',  # missing nhs number
            'BDBD|||C|C|21|Y|D|BD||$$$!||L1|19880101',  # missing both ntd and nhs number
            'BDBD|3|20060519|||21|Y|D|BD||$$$!||L1|19880101',  # missing recall type
        ]

        for test_data in testing_data:
            with self.assertRaises(Exception) as context:
                mapper.map_and_validate_CSO1C_data(test_data)

            # Assert
            self.assertEqual('NHS Number, Next Test Due Date, and Recall Type are required for ingestion',
                             context.exception.args[0])

    def test_map_and_validate_CSO1C_data_throws_exception_with_incorrect_status(self, mock_now, mock_log):

        # Arrange
        test_line = 'BDBD|3|20060519|Q|C|21|Y|D|BD||$$$!||L1|19880101||202001||1|'

        # Act
        with self.assertRaises(Exception) as context:
            mapper.map_and_validate_CSO1C_data(test_line)

        # Assert
        self.assertEqual('Recall Type must be one of expected values N, L, R, I, S, C',
                         context.exception.args[0])


@ddt
@patch('transform_and_store_CSO1C_data.src.map_and_validate_CSO1C_data.datetime', Mock(wraps=datetime))
@patch('transform_and_store_CSO1C_data.src.map_and_validate_CSO1C_data.log')
@patch('transform_and_store_CSO1C_data.src.map_and_validate_CSO1C_data.datetime')
class TestEpisodeDetailsMapper(TestCase):

    FIXED_NOW_TIME = datetime(2020, 3, 3, 9, 45, 23, 123456, tzinfo=timezone.utc)
    FIXED_NOW_DATE = datetime(2020, 3, 3, tzinfo=timezone.utc)

    @unpack
    @data(
        (
            ['20060519', '1', '20200101', '202001', '',  '20200101'],
            None
        ),
        (
            ['20060519', '2', '20200102', '202002', '201905',  '20200104'],
            ['2006-05-19', 'REMINDED', '2019-05-01', '2020-01-04', '2020-02-01', '2020-01-02', '']
        ),
        (
            ['20050519', 'G', '20200102', '202003', '',  'UA'],
            ['2005-05-19', 'PNL', '2020-01-02', '', '', '', '']
        ),
        (
            ['20050519', 'G', '20200102', '202003', '',  ''],
            ['2005-05-19', 'PNL', '2020-01-02', '', '', '', '']
        ),
        (
            ['20050519', 'G', '20200102', '202003', '',  '20200506'],
            ['2005-05-19', 'ACCEPTED', '2020-01-02', '2020-05-06', '', '', '']
        ),
        (
            ['20060519', 'N', '20200102', '202003', '',  'UA'],
            ['2006-05-19', 'PNL', '2020-01-02', '', '', '', '']
        ),
        (
            ['20060519', 'N', '20200102', '202003', '',  ''],
            ['2006-05-19', 'PNL', '2020-01-02', '', '', '', '']
        ),
        (
            ['20060519', 'N', '20200102', '202003', '',  '20200506'],
            ['2006-05-19', 'ACCEPTED', '2020-01-02', '2020-05-06', '', '', '']
        ),
        (
            ['20060519', 'P', '20200102', '202003', '',  '20200101'],
            ['2006-05-19', 'INVITED', '2020-01-02', '2020-01-01', '2020-03-01', '', '']
        ),
        (
            ['20060519', 'H', '20200102', '202003', '201908',  '20200101'],
            ['2006-05-19', 'NRL', '2019-08-01', '2020-01-01', '2020-03-01', '', '2020-01-02']
        ),
        (
            ['20060519', 'F', '20200102', '202003', '201908',  '20200101'],
            ['2006-05-19', 'NRL', '2019-08-01', '2020-01-01', '2020-03-01', '', '2020-01-02']
        ),
        (
            ['20060519', 'C', '20200102', '202003', '',  '20200101'],
            None
        ),
        (
            ['20060519', 'S', '20200102', '202003', '',  '20200101'],
            None
        ),
        (
            ['20060519', 'L', '20200102', '202003', '201908',  '20200101'],
            ['2006-05-19', 'LOGGED', '2019-08-01', '2020-01-01', '2020-03-01', '', '2020-01-02']
        ),
        (
            ['20060519', 'Z', '20200102', '202003', '',  '20200101'],
            None
        ),
        (
            ['20060519', 'Other', '20200102', '202003', '',  '20200101'],
            None
        ),
        (
            ['20060519', '2', '20200102', '202002', '19----',  '20200104'],
            ['2006-05-19', 'REMINDED', '', '2020-01-04', '2020-02-01', '2020-01-02', '']
        ),
        (
            ['20060519', '2', '20200102', '202002', '------',  '20200104'],
            ['2006-05-19', 'REMINDED', '', '2020-01-04', '2020-02-01', '2020-01-02', '']
        ),
    )
    def test_episode_details(self, data_fields_values, expected_episode_details, mock_now, mock_log):
        ntd, recall_stat, notif_date, inv_date, pnl_date, pnl_fnr = data_fields_values

        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mock_now.return_value = self.FIXED_NOW_DATE

        data_fields = [
            'BD', '4056742317', ntd, 'N', recall_stat, '20', 'Y', 'C', 'BD',
            'Note1', 'Note2', 'Note3', 'L2', '19920317', notif_date, inv_date, pnl_date, '1', pnl_fnr
        ]

        if expected_episode_details:
            (expected_ntd, expected_status, expected_pnl, expected_accepted, expected_invited,
             expected_reminded, expected_nrl) = expected_episode_details

            expected_episode = {
                'sort_key': f'EPISODE#{expected_ntd}',
                'created': '2020-03-03T09:45:23.123456+00:00',
                'test_due_date': expected_ntd,
                'user_id': 'transform_and_store_CSO1C_data',
                'status': expected_status,
                'live_record_status': expected_status,
                'pnl_date': expected_pnl,
                'accepted_date': expected_accepted,
                'invited_date': expected_invited,
                'reminded_date': expected_reminded,
                'nrl_date': expected_nrl,
            }
        else:
            expected_episode = None

        actual_episode = mapper.get_episode_details(data_fields)
        self.maxDiff = None
        self.assertEqual(actual_episode, expected_episode)


@ddt
class TestDateConversion(TestCase):

    @unpack
    @data(('202001', '2020-01-01'),
          ('999999', '9999-99-01'),
          ('000000', '0000-00-01'),
          ('20201', '20201'),
          ('20200101', '20200101'),
          ('20201x', '20201x'),
          ('a', 'a'),
          ('', ''))
    def test_covert_partial_date(self, partial_date, expected_converted_partial_date):
        actual_converted_partial_date = mapper._convert_partial_date(partial_date)
        self.assertEqual(expected_converted_partial_date, actual_converted_partial_date)

    @unpack
    @data(
        ('20200103', '2020-01-03'),
        ('99999999', '9999-99-99'),
        ('00000000', '0000-00-00'),
        ('20201', '20201'),
        ('202001', '202001'),
        ('20201x', '20201x'),
        ('a', 'a'),
        ('', '')
    )
    def test_convert_full_date(self, full_date, expected_converted_full_date):
        actual_converted_full_date = mapper._convert_full_date(full_date)
        self.assertEqual(expected_converted_full_date, actual_converted_full_date)


@ddt
class TestMapperHelpers(TestCase):

    @classmethod
    @patch('boto3.resource', Mock())
    def setUpClass(cls, *args):
        import transform_and_store_CSO1C_data.src.map_and_validate_CSO1C_data as mapper
        global mapper

    @unpack
    @data(
        ('C', 'C', True),
        ('C', 'G', False)
    )
    def test_validate_recall(self, recall_type, recall_stat, expected_valid):
        data_fields = [None, None, None, recall_type, recall_stat]
        expected_result = {
            'type': recall_type,
            'stat': recall_stat,
            'valid': expected_valid
        }

        actual_result = mapper._validate_recall(data_fields)
        self.assertDictEqual(expected_result, actual_result)
