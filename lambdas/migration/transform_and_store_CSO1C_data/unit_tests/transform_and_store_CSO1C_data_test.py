from unittest.case import TestCase
from mock import patch, Mock, call
from transform_and_store_CSO1C_data.log_references import LogReference


@patch('transform_and_store_CSO1C_data.transform_and_store_CSO1C_data.log')
@patch('transform_and_store_CSO1C_data.transform_and_store_CSO1C_data.map_and_validate_CSO1C_data')
@patch('transform_and_store_CSO1C_data.transform_and_store_CSO1C_data.get_message_contents_from_event')
@patch('transform_and_store_CSO1C_data.transform_and_store_CSO1C_data.update_participant_by_nhs_number')
@patch('transform_and_store_CSO1C_data.transform_and_store_CSO1C_data.audit')
class TestTransformAndStore1CData(TestCase):

    @classmethod
    @patch('boto3.resource', Mock())
    def setUpClass(cls, *args):
        import transform_and_store_CSO1C_data.transform_and_store_CSO1C_data as _transform_and_store_module
        global AuditActions, AuditUsers, unwrapped_handler, transform_and_store_module
        from common.utils.audit_utils import AuditActions, AuditUsers
        unwrapped_handler = _transform_and_store_module.lambda_handler.__wrapped__
        transform_and_store_module = _transform_and_store_module

    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    @patch('transform_and_store_CSO1C_data.transform_and_store_CSO1C_data.create_episode_record')
    def test_update_participant_with_migrated_record_called_successfully_given_no_fields_to_remove(
            self,
            mock_create_episode,
            mock_audit,
            mock_update_participant,
            mock_get_message_content,
            mock_mapper,
            mock_log):

        # Arrange
        event = 'some fake data'

        mock_get_message_content.return_value = {
            'message_id': '1234',
            'record_line': 'A|123|B|C'
        }

        participant_data = {'field_1': 'value_1', 'field_2': 'value_2', 'empty_field': ''}
        episode_data = {'status': 'PNL'}
        recall_logged_fields = {
            'type': 'C',
            'stat': 'G',
            'valid': False
        }
        logged_fields = {
            'stat_code': 'G',
            'recall': recall_logged_fields
        }
        mock_mapper.return_value = 'the_nhs_number', participant_data, None, episode_data, logged_fields

        cleaned_data = {'field_1': 'value_1', 'field_2': 'value_2'}

        mock_update_participant.return_value = {'participant_id': 'the_participant'}

        # Act
        unwrapped_handler(event, {})

        # Assert
        expected_log_calls = [
            call({'log_reference': LogReference.TRANS1C0001}),
            call({'log_reference': LogReference.TRANS1C0002}),
            call({'log_reference': LogReference.TRANS1C0003}),
            call({'log_reference': LogReference.TRANS1C0004, **recall_logged_fields}),
            call({'log_reference': LogReference.TRANS1C0008})
        ]

        mock_get_message_content.assert_called_with(event)
        mock_update_participant.assert_called_with('the_nhs_number', cleaned_data, None)
        mock_log.assert_has_calls(expected_log_calls)
        mock_audit.assert_called_with(action=AuditActions.STORE_CSO1C,
                                      user=AuditUsers.MIGRATION,
                                      participant_ids=['the_participant'])
        mock_create_episode.assert_called_with(
            {'participant_id': 'the_participant'},
            {'status': 'PNL'},
            {
                'stat_code': 'G',
                'recall': recall_logged_fields
            }
        )

    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    @patch('transform_and_store_CSO1C_data.transform_and_store_CSO1C_data.create_episode_record')
    def test_update_participant_with_migrated_record_called_successfully_given_fields_to_remove(
            self,
            mock_create_episode,
            mock_audit,
            mock_update_participant,
            mock_get_message_content,
            mock_mapper,
            mock_log):

        # Arrange
        event = 'some fake data'
        mock_get_message_content.return_value = {
            'message_id': '1234',
            'record_line': 'A|123|B|C'
        }

        participant_data = {'field_1': 'value_1', 'field_2': 'value_2', 'empty_field': ''}
        episode_data = {'status': 'PNL'}
        logged_fields = {'stat_code': 'G'}
        cleaned_data = {'field_1': 'value_1', 'field_2': 'value_2'}
        fields_to_remove = {'remove': 'me'}
        mock_mapper.return_value = 'the_nhs_number', participant_data, fields_to_remove, episode_data, logged_fields

        mock_update_participant.return_value = {'participant_id': 'the_participant'}

        # Act
        unwrapped_handler(event, {})

        # Assert
        expected_log_calls = [
            call({'log_reference': LogReference.TRANS1C0001}),
            call({'log_reference': LogReference.TRANS1C0002}),
            call({'log_reference': LogReference.TRANS1C0003}),
            call({'log_reference': LogReference.TRANS1C0004})
        ]

        mock_log.assert_has_calls(expected_log_calls)
        mock_get_message_content.assert_called_with(event)
        mock_update_participant.assert_called_with('the_nhs_number', cleaned_data, fields_to_remove)
        mock_audit.assert_called_with(action=AuditActions.STORE_CSO1C,
                                      user=AuditUsers.MIGRATION,
                                      participant_ids=['the_participant'])
        mock_create_episode.assert_called_with(
            {'participant_id': 'the_participant'},
            {'status': 'PNL'},
            {'stat_code': 'G'}
        )

    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    @patch('transform_and_store_CSO1C_data.transform_and_store_CSO1C_data.create_episode_record')
    def test_audit_is_not_called_given_updating_participant_is_not_successful(
            self,
            mock_create_episode,
            mock_audit,
            mock_update_participant,
            mock_get_message_content,
            mock_mapper,
            mock_log):
        event = 'some fake data'
        mock_get_message_content.return_value = {
            'message_id': '1234',
            'record_line': 'A|123|B|C'
        }

        participant_data = {'field_1': 'value_1', 'field_2': 'value_2', 'empty_field': ''}
        episode_data = {'status': 'PNL'}
        logged_fields = {'stat_code': 'G'}
        cleaned_data = {'field_1': 'value_1', 'field_2': 'value_2'}
        fields_to_remove = {'remove': 'me'}
        mock_mapper.return_value = 'the_nhs_number', participant_data, fields_to_remove, episode_data, logged_fields

        mock_update_participant.return_value = None

        # Act
        unwrapped_handler(event, {})

        # Assert
        mock_get_message_content.assert_called_with(event)
        mock_update_participant.assert_called_with('the_nhs_number', cleaned_data, fields_to_remove)
        mock_audit.assert_not_called()
        mock_create_episode.assert_not_called()

    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'FALSE'})
    @patch('transform_and_store_CSO1C_data.transform_and_store_CSO1C_data.create_episode_record')
    def test_no_audit_when_migration_audit_is_toggled_off(
            self,
            mock_create_episode,
            mock_audit,
            mock_update_participant,
            mock_get_message_content,
            mock_mapper,
            mock_log):

        # Arrange
        event = 'some fake data'
        mock_get_message_content.return_value = {
            'message_id': '1234',
            'record_line': 'A|123|B|C'
        }

        participant_data = {'field_1': 'value_1', 'field_2': 'value_2', 'empty_field': ''}
        episode_data = {'status': 'PNL'}
        logged_fields = {'stat_code': 'G'}
        fields_to_remove = {'remove': 'me'}
        mock_mapper.return_value = 'the_nhs_number', participant_data, fields_to_remove, episode_data, logged_fields

        mock_update_participant.return_value = {'participant_id': 'the_participant'}

        # Act
        unwrapped_handler(event, {})

        # Assert
        expected_log_calls = [
            call({'log_reference': LogReference.TRANS1C0001}),
            call({'log_reference': LogReference.TRANS1C0002}),
            call({'log_reference': LogReference.TRANS1C0003}),
            call({'log_reference': LogReference.TRANS1C0004})
        ]
        mock_log.assert_has_calls(expected_log_calls)
        mock_audit.assert_not_called()
        mock_create_episode.assert_called_with(
            {'participant_id': 'the_participant'},
            {'status': 'PNL'},
            {'stat_code': 'G'}
        )

    @patch('transform_and_store_CSO1C_data.transform_and_store_CSO1C_data.create_replace_record_in_participant_table')
    def test_create_episode(
            self,
            mock_create_replace_participant,
            mock_audit,
            mock_update_participant,
            mock_get_message_content,
            mock_mapper,
            mock_log):
        participant = {
            'participant_id': 'b586dd1a-2bd0-4ca4-b91c-4f2b609e654a',
            'registered_gp_practice_code': 'ABC1234',
            'nhs_number': '4056742317'
        }
        episode = {
            'sort_key': 'Episode#2006-05-19',
            'created': '2020-03-03T09:45:23.123456+00:00',
            'test_due_date': '2006-05-19',
            'user_id': 'transform_and_store_CSO1C_data',
            'status': 'REMINDED',
            'live_record_status': 'REMINDED',
            'pnl_date': '2020-01-01',
            'accepted_date': 'UA',
            'invited_date': '2020-01-01',
            'reminded_date': '2020-01-01',
            'nrl_date': '',
        }
        logging_fields = {
            'stat_code': '2'
        }
        transform_and_store_module.create_episode_record(participant, episode, logging_fields)

        expected_episode = {
            'participant_id': 'b586dd1a-2bd0-4ca4-b91c-4f2b609e654a',
            'sort_key': 'Episode#2006-05-19',
            'registered_gp_practice_code_test_due_date': 'ABC1234#2006-05-19',
            'created': '2020-03-03T09:45:23.123456+00:00',
            'test_due_date': '2006-05-19',
            'registered_gp_practice_code': 'ABC1234',
            'user_id': 'transform_and_store_CSO1C_data',
            'status': 'REMINDED',
            'live_record_status': 'REMINDED',
            'pnl_date': '2020-01-01',
            'accepted_date': 'UA',
            'invited_date': '2020-01-01',
            'reminded_date': '2020-01-01',
        }

        expected_log_calls = [
            call({'log_reference': LogReference.TRANS1C0009}),
            call(
                {
                    'log_reference': LogReference.TRANS1C0010,
                    'stat_code': '2',
                    'episode_status': 'REMINDED'
                }
            )
        ]
        mock_log.assert_has_calls(expected_log_calls)
        mock_create_replace_participant.assert_called_with(expected_episode)
        mock_audit.assert_called_with(
            action=AuditActions.CREATE_EPISODE,
            user=AuditUsers.MIGRATION,
            participant_ids=['b586dd1a-2bd0-4ca4-b91c-4f2b609e654a'],
            nhs_numbers=['4056742317']
        )
