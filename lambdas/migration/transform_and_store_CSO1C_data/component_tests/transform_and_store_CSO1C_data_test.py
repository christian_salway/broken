import json
from unittest import TestCase
from unittest.mock import patch, Mock
from boto3.dynamodb.conditions import Key
from ddt import ddt, data, unpack
from collections import OrderedDict

from common.models.participant import ParticipantSortKey


transform_and_store_module = None


valid_row = OrderedDict([
    ('SOURCE_CIPHER', 'BD'),
    ('NHSNUM', '9100000281'),
    ('NTD', '19880101'),
    ('RECALL_TYPE', 'N'),
    ('RECALL_STAT', 'G'),
    ('FNR_COUNT', '20'),
    ('POS_DOUBT', 'N'),
    ('CEASED_FLAG', 'C'),
    ('CEASED_CIPHER', 'DN'),
    ('NOTES1', 'valid notes 1'),
    ('NOTES2', 'valid notes 2'),
    ('NOTES3', 'valid notes 3'),
    ('LDN', 'L1'),
    ('OAPD', '19500101'),
    ('NOTIF_DATE', '20200101'),
    ('INV_DATE', '202001'),
    ('PNL_DATE', '202001'),
    ('PATH_ID', '13'),
    ('PNL_FNR', '20200101')
])

class_lvl_mock_env_vars = {
    'ENABLE_MIGRATION_AUDIT': 'TRUE',
    'DYNAMODB_PARTICIPANTS': 'participants-table',
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL'
}


@ddt
@patch('os.environ', class_lvl_mock_env_vars)
class TestFieldsInCSO1C(TestCase):

    mock_env_vars = {
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
        'PARTICIPANTS_TABLE_NAME': 'PARTICIPANTS_TABLE_NAME'
    }

    @classmethod
    @patch('boto3.client')
    @patch('boto3.resource')
    @patch('os.environ', mock_env_vars)
    def setUpClass(cls, boto3_resource, boto3_client, *args):
        cls.log_patcher = patch('common.log.log')
        participants_table_mock = Mock()
        boto3_table_mock = Mock()
        participants_table_mock.query.return_value = []
        boto3_table_mock.Table.return_value = participants_table_mock
        boto3_table_mock.batch_get_item.return_value = {
            'Responses': {
                'participants-table': []
            }
        }
        boto3_resource.return_value = boto3_table_mock
        cls.participants_table_mock = participants_table_mock

        sqs_client_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: sqs_client_mock if args and args[0] == 'sqs' else Mock()
        cls.sqs_client_mock = sqs_client_mock

        import transform_and_store_CSO1C_data.transform_and_store_CSO1C_data as _transform_and_store
        global transform_and_store_module
        transform_and_store_module = _transform_and_store

        from common.test_mocks.mock_events import sqs_mock_event as _sqs_mock_event
        global sqs_mock_event
        sqs_mock_event = _sqs_mock_event

    def setUp(self):
        self.participants_table_mock.reset_mock()
        self.sqs_client_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @unpack
    @data(
        ('0', '0', None, None),
        ('1', '1', None, None),
        ('99', '99', None, None),
        ('', None, 'PATH_ID value was empty, PATH_ID must be between 1 and 2 characters (inclusive), '
            'PATH_ID is expected to be an integer', None),
        ('&', '&', 'PATH_ID is expected to be an integer', None),
        ('113', '113', 'PATH_ID must be between 1 and 2 characters (inclusive)', None),
        ('a1', 'a1', 'PATH_ID is expected to be an integer', None),
    )
    def test_cso1c_with_valid_or_invalid_path_id(self, path_id, expected_path_id_converted,
                                                 schema_validation_error, data_validation_error):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123', 'sort_key': ParticipantSortKey.PARTICIPANT}]}
        self.participants_table_mock.query.side_effect = _query_mock

        event = self.build_event(valid_row, field_to_update='PATH_ID', value_to_update=path_id)

        context = Mock()
        context.function_name = ''

        transform_and_store_module.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant
        self.participants_table_mock.query.assert_called_once()
        _, kwargs = self.participants_table_mock.query.call_args
        expected_key_condition = Key('sanitised_nhs_number').eq('9100000281') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')

        # Asserts it stores the record with the correct path_id value and correct validations
        self.assertEqual(self.participants_table_mock.put_item.call_count, 2)
        _, kwargs = self.participants_table_mock.put_item.call_args_list[0]
        self.assertEqual(expected_path_id_converted, kwargs['Item'].get('path_id'))
        actual_validation_errors = kwargs['Item'].get('migrated_1C_data').get('validation_errors', {})
        self.assertEqual(actual_validation_errors.get('PATH_ID', {}).get('schema_error'), schema_validation_error)
        self.assertEqual(actual_validation_errors.get('PATH_ID', {}).get('data_error'), data_validation_error)

        # Asserts it sends audit record to the audit queue
        self.assertEqual(self.sqs_client_mock.send_message.call_count, 2)
        _, kwargs = self.sqs_client_mock.send_message.call_args_list[0]
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO1C')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(actual_audit_record_sent.get('participant_ids'), ['123'])
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('', None, None, None),
        ('202001', '2020-01-01', None, None),
        ('000000', '0000-00-01', 'Invite date was in an invalid format. Expected YYYYMM', None),
        ('999999', '9999-99-01', 'Invite date was in an invalid format. Expected YYYYMM', None),
        ('20201', '20201', 'Date length must be exactly 6 characters', None),
        ('2020111', '2020111', 'Date length must be exactly 6 characters', None),
        ('20200101', '20200101', 'Date length must be exactly 6 characters', None),
        ('aaaaaa', 'aaaaaa', 'Invite date was in an invalid format. Expected YYYYMM', None),
        ('a!', 'a!', 'Date length must be exactly 6 characters', None),
    )
    def test_cso1c_with_valid_or_invalid_inv_date(self, inv_date, expected_inv_date_converted,
                                                  schema_validation_error, data_validation_error):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123', 'sort_key': ParticipantSortKey.PARTICIPANT}]}
        self.participants_table_mock.query.side_effect = _query_mock

        event = self.build_event(valid_row, field_to_update='INV_DATE', value_to_update=inv_date)

        context = Mock()
        context.function_name = ''

        transform_and_store_module.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant
        self.participants_table_mock.query.assert_called_once()
        _, kwargs = self.participants_table_mock.query.call_args
        expected_key_condition = Key('sanitised_nhs_number').eq('9100000281') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')

        # Asserts it stores the record with the correct inv_date value and correct validations
        self.assertEqual(self.participants_table_mock.put_item.call_count, 2)
        _, kwargs = self.participants_table_mock.put_item.call_args_list[0]
        self.assertEqual(expected_inv_date_converted, kwargs['Item'].get('invited_date'))
        actual_validation_errors = kwargs['Item'].get('migrated_1C_data').get('validation_errors', {})
        self.assertEqual(actual_validation_errors.get('INV_DATE', {}).get('schema_error'), schema_validation_error)
        self.assertEqual(actual_validation_errors.get('INV_DATE', {}).get('data_error'), data_validation_error)

        # Asserts it sends audit record to the audit queue
        self.assertEqual(self.sqs_client_mock.send_message.call_count, 2)
        _, kwargs = self.sqs_client_mock.send_message.call_args_list[0]
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO1C')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertIsNotNone(actual_audit_record_sent.get('participant_ids'))
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('', None, None, None),
        ('202001', None, None, None),
        ('20200101', None, None, None),
        ('999999', None, None, None),
        ('a`£', None, None, None),
    )
    def test_cso1c_with_valid_or_invalid_pnl_date(self, pnl_date, expected_pnl_date_converted,
                                                  schema_validation_error, data_validation_error):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123', 'sort_key': ParticipantSortKey.PARTICIPANT}]}
        self.participants_table_mock.query.side_effect = _query_mock

        event = self.build_event(valid_row, field_to_update='PNL_DATE', value_to_update=pnl_date)

        context = Mock()
        context.function_name = ''

        transform_and_store_module.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant
        self.participants_table_mock.query.assert_called_once()
        _, kwargs = self.participants_table_mock.query.call_args
        expected_key_condition = Key('sanitised_nhs_number').eq('9100000281') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')

        # Asserts it stores the record with the correct inv_date value and correct validations
        self.assertEqual(self.participants_table_mock.put_item.call_count, 2)
        _, kwargs = self.participants_table_mock.put_item.call_args_list[0]
        self.assertEqual(expected_pnl_date_converted, kwargs['Item'].get('pnl_date'))
        actual_validation_errors = kwargs['Item'].get('migrated_1C_data').get('validation_errors', {})
        self.assertEqual(actual_validation_errors.get('PNL_DATE', {}).get('schema_error'), schema_validation_error)
        self.assertEqual(actual_validation_errors.get('PNL_DATE', {}).get('data_error'), data_validation_error)

        # Asserts it sends audit record to the audit queue
        self.assertEqual(self.sqs_client_mock.send_message.call_count, 2)
        _, kwargs = self.sqs_client_mock.send_message.call_args_list[0]
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO1C')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertIsNotNone(actual_audit_record_sent.get('participant_ids'))
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('', None, None, None),
        ('202001', None, None, None),
        ('20200101', None, None, None),
        ('999999', None, None, None),
        ('a`£', None, None, None),
    )
    def test_cso1c_with_valid_or_invalid_notif_date(self, notif_date, expected_notif_date_converted,
                                                    schema_validation_error, data_validation_error):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123', 'sort_key': ParticipantSortKey.PARTICIPANT}]}
        self.participants_table_mock.query.side_effect = _query_mock

        event = self.build_event(valid_row, field_to_update='NOTIF_DATE', value_to_update=notif_date)

        context = Mock()
        context.function_name = ''

        transform_and_store_module.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant
        self.participants_table_mock.query.assert_called_once()
        _, kwargs = self.participants_table_mock.query.call_args
        expected_key_condition = Key('sanitised_nhs_number').eq('9100000281') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')

        # Asserts it stores the record with the correct inv_date value and correct validations
        self.assertEqual(self.participants_table_mock.put_item.call_count, 2)
        _, kwargs = self.participants_table_mock.put_item.call_args_list[0]
        self.assertEqual(expected_notif_date_converted, kwargs['Item'].get('notif_date'))
        actual_validation_errors = kwargs['Item'].get('migrated_1C_data').get('validation_errors', {})
        self.assertEqual(actual_validation_errors.get('NOTIF_DATE', {}).get('schema_error'), schema_validation_error)
        self.assertEqual(actual_validation_errors.get('NOTIF_DATE', {}).get('data_error'), data_validation_error)

        # Asserts it sends audit record to the audit queue
        self.assertEqual(self.sqs_client_mock.send_message.call_count, 2)
        _, kwargs = self.sqs_client_mock.send_message.call_args_list[0]
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO1C')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertIsNotNone(actual_audit_record_sent.get('participant_ids'))
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    def build_event(self, row_dict, field_to_update=None, value_to_update=None):
        updated_row = {}
        updated_row.update(row_dict)
        if field_to_update:
            updated_row[field_to_update] = value_to_update
        body_dict = {
            'internal_id': 'internal_id',
            'file_name': 'file_name_before_part',
            'file_part_name': 'file_key',
            'line_index': 1,
            'line': '|'.join(updated_row.values())
        }

        return sqs_mock_event(body_dict)
