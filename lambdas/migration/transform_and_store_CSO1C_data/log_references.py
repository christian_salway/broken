import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    TRANS1C0001 = (logging.INFO, 'Received CSO1C record from SQS Queue')
    TRANS1C0002 = (logging.INFO, 'Successfully mapped raw CSO1C data to participant')
    TRANS1C0003 = (logging.INFO, 'Successfully removed blank lines')
    TRANS1C0004 = (logging.INFO, 'Participant updated with migrated CSO1C record')
    TRANS1C0005 = (logging.WARNING, 'CSO1C record has failed validation')
    TRANS1C0006 = (logging.ERROR, 'CSO1C record has failed mandatory validation, record is rejected')
    TRANS1C0007 = (logging.INFO, 'Logging LDN status')
    TRANS1C0008 = (logging.INFO, 'Attempting to create episode record for this participant')
    TRANS1C0009 = (logging.INFO, 'Updated Episode details with participant information')
    TRANS1C0010 = (logging.INFO, 'Successfully created episode record')
