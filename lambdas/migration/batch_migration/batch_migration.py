from batch_migration.log_references import LogReference
from common.log import log
from common.utils.s3_utils import delete_object_from_bucket, read_bucket
from common.utils.sqs_utils import send_new_message_batch
from common.utils.lambda_wrappers import lambda_entry_point
import json
from os import environ
from typing import Dict, List

FILE_BUCKET = environ.get('FILE_BUCKET')
FILE_BUCKET_ARN = environ.get('FILE_BUCKET_ARN')
BATCH_SIZE = environ.get('BATCH_SIZE', 10)
LOCK_FILE_NAME = environ.get('LOCKFILE_NAME', '.LOCKFILE')
IMPORT_QUEUE = environ.get('IMPORT_QUEUE')
AWS_REGION = environ.get('AWS_REGION')


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.BATCHMIG0001})
    files_in_bucket = read_bucket(FILE_BUCKET).get('Contents', [])
    file_names = [file['Key'] for file in files_in_bucket]

    if not lock_file_exists(file_names):
        log({'log_reference': LogReference.BATCHMIG0002})
        return

    log({'log_reference': LogReference.BATCHMIG0003})
    batch_file_names = next_batch_file_names(file_names)
    batch_files = next_batch_files(files_in_bucket, batch_file_names)

    if not batch_file_names:
        delete_lock_file()
        return

    send_files_to_queue(batch_files, IMPORT_QUEUE)


def lock_file_exists(file_names: List[str]) -> bool:
    return LOCK_FILE_NAME in file_names


def next_batch_file_names(file_names: List[str]) -> bool:
    sorted_file_names = sorted(file_names)
    sorted_file_names.remove(LOCK_FILE_NAME)
    return sorted_file_names[:int(BATCH_SIZE)]


def delete_lock_file():
    delete_object_from_bucket(FILE_BUCKET, LOCK_FILE_NAME)
    log({'log_reference': LogReference.BATCHMIG0004})


def next_batch_files(files: List[Dict], file_names: List[str]):
    file_map = {}
    for file in files:
        file_map[file['Key']] = file

    return [
        file_map[file_name]
        for file_name in file_names
    ]


def send_files_to_queue(files: List[Dict], queue: str):
    messages = [
        {
            'Id': str(index),
            'MessageBody': json.dumps(
                {
                    'Records': [
                        {
                            'eventVersion': '2.1',
                            'eventSource': 'aws:s3',
                            'awsRegion': AWS_REGION,
                            's3': {
                                's3SchemaVersion': '1.0',
                                'bucket': {
                                    'name': FILE_BUCKET,
                                    'arn': FILE_BUCKET_ARN
                                },
                                'object': {
                                    'key': file['Key'],
                                    'size': file['Size']
                                }
                            }
                        }
                    ]
                }
            )
        }
        for index, file in enumerate(files)
    ]

    send_new_message_batch(queue, messages)
    log(
        {
            'log_reference': LogReference.BATCHMIG0005,
            'file_names': [file['Key'] for file in files],
            'batch_size': len(files)
        }
    )
