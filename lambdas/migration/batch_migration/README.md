# Batch Migration

This lambda looks for a lock file (usually .LOCKFILE) in the import-migration bucket. When this is seen,
a batch of 100 files (configurable) is queued for import. We do this by faking the S3 change messages sent
to SQS.

Once there are no more files left, the lock file is removed. This lambda triggered on a 20 minute interval by cron (well, cloudy cron).