import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    BATCHMIG0001 = (logging.INFO, 'Checking the lock file exists.')
    BATCHMIG0002 = (logging.INFO, 'Lock file does not exist.')
    BATCHMIG0003 = (logging.INFO, 'Lock file exists.')
    BATCHMIG0004 = (logging.INFO, 'No more files to process. Deleted lock file.')
    BATCHMIG0005 = (logging.INFO, 'Added files to the queue for processing.')
