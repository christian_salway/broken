from batch_migration.log_references import LogReference
from mock import call, patch
from unittest.case import TestCase

MODULE_NAME = 'batch_migration.batch_migration'


@patch(f'{MODULE_NAME}.log')
@patch(f'{MODULE_NAME}.read_bucket')
@patch(f'{MODULE_NAME}.delete_object_from_bucket')
@patch(f'{MODULE_NAME}.send_new_message_batch')
class TestBatchMigration(TestCase):
    @patch(
        'os.environ',
        {
            'FILE_BUCKET': 'FILE_BUCKET_NAME',
            'FILE_BUCKET_ARN': 'FILE_BUCKET_ARN',
            'BATCH_SIZE': 5,
            'IMPORT_QUEUE': 'IMPORT_QUEUE_NAME',
            'AWS_REGION': 'Somewhere'
        }
    )
    def setUp(self):
        import batch_migration.batch_migration as _module
        self.module = _module
        self.unwrapped_function = self.module.lambda_handler.__wrapped__

    def test_no_action_taken_if_no_lock_file(
        self,
        mock_send_batch,
        mock_delete_object,
        mock_read_bucket,
        mock_log
    ):
        mock_read_bucket.return_value = {
            'Contents': [
                {
                    'Key': 'file1'
                }
            ]
        }

        expected_log_calls = [
            call({'log_reference': LogReference.BATCHMIG0001}),
            call({'log_reference': LogReference.BATCHMIG0002})
        ]

        self.unwrapped_function({}, {})

        mock_read_bucket.assert_called_with('FILE_BUCKET_NAME')
        mock_log.assert_has_calls(expected_log_calls)
        mock_send_batch.assert_not_called()
        mock_delete_object.assert_not_called()

    def test_lock_file_deleted_if_nothing_to_batch(
        self,
        mock_send_batch,
        mock_delete_object,
        mock_read_bucket,
        mock_log
    ):
        mock_read_bucket.return_value = {
            'Contents': [
                {
                    'Key': '.LOCKFILE'
                }
            ]
        }

        expected_log_calls = [
            call({'log_reference': LogReference.BATCHMIG0001}),
            call({'log_reference': LogReference.BATCHMIG0003}),
            call({'log_reference': LogReference.BATCHMIG0004})
        ]

        self.unwrapped_function({}, {})

        mock_read_bucket.assert_called_with('FILE_BUCKET_NAME')
        mock_log.assert_has_calls(expected_log_calls)
        mock_send_batch.assert_not_called()
        mock_delete_object.assert_called_with('FILE_BUCKET_NAME', '.LOCKFILE')

    def test_files_sent_up_to_batch_limit(
        self,
        mock_send_batch,
        mock_delete_object,
        mock_read_bucket,
        mock_log
    ):
        file_contents = [
            {
                'Key': f'File{i}',
                'Size': i
            }
            for i in range(9, -1, -1)
        ]

        mock_read_bucket.return_value = {
            'Contents': [
                {'Key': '.LOCKFILE'},
                *file_contents
            ]
        }

        expected_messages = [
            {'Id': "0", 'MessageBody': '{"Records": [{"eventVersion": "2.1", "eventSource": "aws:s3", "awsRegion": "Somewhere", "s3": {"s3SchemaVersion": "1.0", "bucket": {"name": "FILE_BUCKET_NAME", "arn": "FILE_BUCKET_ARN"}, "object": {"key": "File0", "size": 0}}}]}'}, 
            {'Id': "1", 'MessageBody': '{"Records": [{"eventVersion": "2.1", "eventSource": "aws:s3", "awsRegion": "Somewhere", "s3": {"s3SchemaVersion": "1.0", "bucket": {"name": "FILE_BUCKET_NAME", "arn": "FILE_BUCKET_ARN"}, "object": {"key": "File1", "size": 1}}}]}'}, 
            {'Id': "2", 'MessageBody': '{"Records": [{"eventVersion": "2.1", "eventSource": "aws:s3", "awsRegion": "Somewhere", "s3": {"s3SchemaVersion": "1.0", "bucket": {"name": "FILE_BUCKET_NAME", "arn": "FILE_BUCKET_ARN"}, "object": {"key": "File2", "size": 2}}}]}'}, 
            {'Id': "3", 'MessageBody': '{"Records": [{"eventVersion": "2.1", "eventSource": "aws:s3", "awsRegion": "Somewhere", "s3": {"s3SchemaVersion": "1.0", "bucket": {"name": "FILE_BUCKET_NAME", "arn": "FILE_BUCKET_ARN"}, "object": {"key": "File3", "size": 3}}}]}'}, 
            {'Id': "4", 'MessageBody': '{"Records": [{"eventVersion": "2.1", "eventSource": "aws:s3", "awsRegion": "Somewhere", "s3": {"s3SchemaVersion": "1.0", "bucket": {"name": "FILE_BUCKET_NAME", "arn": "FILE_BUCKET_ARN"}, "object": {"key": "File4", "size": 4}}}]}'}  
        ]

        expected_log_calls = [
            call({'log_reference': LogReference.BATCHMIG0001}),
            call({'log_reference': LogReference.BATCHMIG0003}),
            call(
                {
                    'log_reference': LogReference.BATCHMIG0005,
                    'file_names': ['File0', 'File1', 'File2', 'File3', 'File4'],
                    'batch_size': 5
                }
            )
        ]

        self.unwrapped_function({}, {})

        mock_read_bucket.assert_called_with('FILE_BUCKET_NAME')
        mock_log.assert_has_calls(expected_log_calls)
        mock_send_batch.assert_called_with('IMPORT_QUEUE_NAME', expected_messages)
        mock_delete_object.assert_not_called()
