import os
from datetime import datetime, timezone
from common.utils.transform_and_store_utils import (
    get_message_contents_from_event,
    create_replace_record_in_participant_table,
    get_message_body_from_event,
    record_is_duplicate
)
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from common.utils.audit_utils import AuditActions, AuditUsers, audit
from transform_and_store_CSO8_data.log_references import LogReference
from transform_and_store_CSO8_data.map_and_validate_CSO8_data import map_and_validate_CSO8_data
from common.utils.sqs_utils import send_new_message
from transform_and_store_CSO8_data.exceptions import UnsupportedNotificationTypeException

LAMBDA_DLQ_URL = os.environ.get('LAMBDA_DLQ_URL')


@lambda_entry_point
def lambda_handler(event, context):
    contents = get_message_contents_from_event(event)
    log({'log_reference': LogReference.CSO8MAP0001, 'message_id': contents['message_id']})
    record_line = contents['record_line']
    log({'log_reference': LogReference.CSO8MAP0002})

    log({'log_reference': LogReference.CSO8MAP0003})
    try:
        mapped_data = map_and_validate_CSO8_data(record_line)
    except UnsupportedNotificationTypeException as unsupported:
        log({'log_reference': LogReference.CSO8MAP0006, 'message': str(unsupported)})
        return
    except Exception:
        log({'log_reference': LogReference.CSO8MAP0005, 'add_exception_info': True})
        send_new_message(LAMBDA_DLQ_URL, get_message_body_from_event(event))
        return

    log({'log_reference': LogReference.CSO8MAP0004})

    log({'log_reference': LogReference.CSO8MAP0009})
    comparison_fields = ['nhs_number', 'notification_date', 'type']
    if record_is_duplicate(mapped_data, comparison_fields):
        log({'log_reference': LogReference.CSO8MAP0010})
        return
    log({'log_reference': LogReference.CSO8MAP0011})

    create_notification_record(record_line, mapped_data)


def create_notification_record(raw_data, mapped_data):
    migrated_8_data = mapped_data.get('migrated_8_data', {})

    additional_migrated_data_fields = {
        'received': datetime.now(timezone.utc).isoformat(),
        'value': raw_data
    }

    migrated_8_data.update(additional_migrated_data_fields)

    mapped_data['migrated_8_data'] = migrated_8_data

    create_replace_record_in_participant_table(mapped_data)
    log({'log_reference': LogReference.CSO8MAP0008})
    participant_id = mapped_data['participant_id']
    if os.environ.get('ENABLE_MIGRATION_AUDIT') != 'FALSE':
        audit(action=AuditActions.STORE_CSO8, user=AuditUsers.MIGRATION, participant_ids=[participant_id])
