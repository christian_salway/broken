from datetime import datetime
from re import match
from common.utils.transform_and_store_utils import add_hyphens_to_date
from transform_and_store_CSO8_data.cso8_fields import CSO8_FIELDS
from common.log import log
from transform_and_store_CSO8_data.log_references import LogReference

TYPE_TEXT = 'Deducted Abnormal Follow up'
XFER_IN_TEXT = 'Abnormal Follow-up for transfer in'
XFER_INTERNAL_TEXT = 'Abnormal Follow-up for internal transfer'
XFER_OUT_TEXT = 'Abnormal Follow-up for transfer out'


def map_follow_up_data(data_list):
    match_strategies = {
        'CIN': _map_abnormal_follow_up_xfer_in,
        'ID': _map_abnormal_follow_up_internal_xfer,
        'CY': _map_abnormal_follow_up_xfer_out
    }

    notif_sub1 = data_list[CSO8_FIELDS.index('NOTIF-SUB1')]

    if notif_sub1 not in match_strategies.keys():
        mapped_data = invalid_follow_up_type(notif_sub1)
    else:
        notif_sub2 = data_list[CSO8_FIELDS.index('NOTIF-SUB2')]
        mapped_data = match_strategies[notif_sub1](notif_sub2)

    return mapped_data


def invalid_follow_up_type(notif_sub1):
    VALID_FOLLOW_UP_TYPES = ['CIN', 'ID', 'CY']
    error = {
        'NOTIF-SUB1': {
            'data_error': f"{TYPE_TEXT} type '{notif_sub1}' is not one of the expected values {VALID_FOLLOW_UP_TYPES}" 
        }
    }

    log({'log_reference': LogReference.CSO8MAP0007, 'validation_errors': error})

    return {
        'type':  f'{TYPE_TEXT} - {notif_sub1}',
        'migrated_8_data': {
            'validation_errors': error
        }
    }


def _map_abnormal_follow_up_xfer_in(notif_sub2):
    valid_date, mapped_date, error = _validate_and_map_date(notif_sub2)
    if valid_date:
        return {
            'type': TYPE_TEXT,
            'subtype': f'{XFER_IN_TEXT} - Last test date: {mapped_date}'
        }
    return {
        'type': TYPE_TEXT,
        'subtype': XFER_IN_TEXT,
        'migrated_8_data': {
            'validation_errors': error
        }
    }


def _map_abnormal_follow_up_internal_xfer(notif_sub2):
    valid_date, mapped_date, error = _validate_and_map_date(notif_sub2)
    if valid_date:
        return {
            'type': TYPE_TEXT,
            'subtype': f'{XFER_INTERNAL_TEXT} - Today\'s Date: {mapped_date}'
        }
    return {
        'type': TYPE_TEXT,
        'subtype': XFER_INTERNAL_TEXT,
        'migrated_8_data': {
            'validation_errors': error
        }
    }


def _map_abnormal_follow_up_xfer_out(notif_sub2):
    valid_date, mapped_date, error = _validate_and_map_date(notif_sub2)
    if valid_date:
        return {
            'type': TYPE_TEXT,
            'subtype': f'{XFER_OUT_TEXT} - Recall Date: {mapped_date}'
        }
    return {
        'type': TYPE_TEXT,
        'subtype': XFER_OUT_TEXT,
        'migrated_8_data': {
            'validation_errors': error
        }
    }


def _validate_and_map_date(notif_sub2):
    if notif_sub2 and match(r'^\d{8}$', notif_sub2):
        try:
            datetime.strptime(notif_sub2, '%Y%m%d')
            return True, add_hyphens_to_date(notif_sub2), None
        except ValueError:
            error = {
                'NOTIF-SUB2': {
                    'data_error': f'{notif_sub2} is not a valid date'
                }
            }
            log({'log_reference': LogReference.CSO8MAP0007, 'validation_errors': error})
            return False, None, error

    error = {
        'NOTIF-SUB2': {
            'data_error': 'Date must be of the format CCYYMMDD'
        }
    }

    log({'log_reference': LogReference.CSO8MAP0007, 'validation_errors': error})
    return False, None, error
