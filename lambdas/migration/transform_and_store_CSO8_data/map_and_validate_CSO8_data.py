from datetime import datetime, timezone
from common.utils.transform_and_store_utils import (
    remove_empty_values,
    add_hyphens_to_date,
    get_hashed_value
)
from common.utils.participant_utils import get_participant_id
from common.models.participant import ParticipantSortKey
from transform_and_store_CSO8_data.map_follow_up_data import map_follow_up_data
from transform_and_store_CSO8_data.map_invitation_data import map_invitation_data
from transform_and_store_CSO8_data.map_pnl_data import map_pnl_data
from transform_and_store_CSO8_data.map_result_data import map_result_data
from transform_and_store_CSO8_data.cso8_fields import CSO8_FIELDS


DELIMITER = "|"

CSO8_EXPECTED_FIELD_COUNT = 8
EXTRA_FIELDS_LIMIT = 5
TOTAL_FIELDS_LIMIT = CSO8_EXPECTED_FIELD_COUNT + EXTRA_FIELDS_LIMIT


def map_and_validate_CSO8_data(raw_data):
    data_list = raw_data.split('|')

    if len(data_list) not in range(CSO8_EXPECTED_FIELD_COUNT, TOTAL_FIELDS_LIMIT):
        raise Exception(
            f'Malformed input: Number of fields in CSO8 result must be between {CSO8_EXPECTED_FIELD_COUNT} and '
            f'{TOTAL_FIELDS_LIMIT}')

    return _map(data_list, raw_data)


def _map(data_list, raw_data):
    match_strategies = {
        'CP': map_result_data,
        'DF': map_follow_up_data,
        'eDF': map_follow_up_data,
        'RG': map_pnl_data,
        'RP': map_invitation_data
    }

    notification_type = data_list[CSO8_FIELDS.index('NOTIF-TYPE-CODE')]
    mapped_notification_data = match_strategies[notification_type](data_list)

    nhs_number = data_list[CSO8_FIELDS.index('NHSNUM')]
    participant_id = get_participant_id(nhs_number)
    if participant_id is None:
        raise Exception("No participant found for given nhs number.")

    notification_date = add_hyphens_to_date(data_list[CSO8_FIELDS.index('NOTIF-DATE')])
    notification_sequence = data_list[CSO8_FIELDS.index('NOTIF-SEQ')]
    current_date = datetime.now(timezone.utc).isoformat()

    mapped_data = {
        'participant_id': participant_id,
        'sort_key': f'{ParticipantSortKey.NOTIFICATION}#{notification_date}#{notification_sequence}#{current_date}',
        'nhs_number': nhs_number,
        'notification_date': notification_date,
        'notification_sequence': notification_sequence,
        'created': current_date,
        'source_hash': get_hashed_value(raw_data)
    }

    mapped_data.update(mapped_notification_data)

    return remove_empty_values(mapped_data)
