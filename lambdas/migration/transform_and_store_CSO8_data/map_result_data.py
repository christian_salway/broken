from transform_and_store_CSO8_data.cso8_fields import CSO8_FIELDS
from common.log import log
from common.models.participant import CeaseReason, EpisodeStatus
from transform_and_store_CSO8_data.log_references import LogReference


def map_result_data(data_list):
    match_strategies = {
        'CEASED': _map_ceased_fields,
        'eCEASED': _map_eceased_fields,
        'GP': _map_gp_fields,
        'LT': _map_letter_fields,
        'RS': _map_letter_not_produced_fields,
        'SN': _map_smear_taker_fields,
        'SL': _map_address_slip_fields,
        'LA': _map_address_labels_fields
    }

    notif_sub1 = data_list[CSO8_FIELDS.index('NOTIF-SUB1')]

    if notif_sub1 not in match_strategies.keys():
        mapped_data = invalid_result_type(notif_sub1)
    else:
        notif_sub2 = data_list[CSO8_FIELDS.index('NOTIF-SUB2')]
        notif_sub3 = data_list[CSO8_FIELDS.index('NOTIF-SUB3')]
        mapped_data = match_strategies[notif_sub1](notif_sub2, notif_sub3)

    return mapped_data


def invalid_result_type(notif_sub1):
    VALID_RESULT_TYPES = [EpisodeStatus.CEASED.value, 'eCEASED', 'GP', 'LT', 'RS', 'SN', 'SL', 'LA']
    error = {
        'NOTIF-SUB1': {
            'data_error': f"Result type '{notif_sub1}' is not one of the expected values {VALID_RESULT_TYPES}"
        }
    }

    log({'log_reference': LogReference.CSO8MAP0007, 'validation_errors': error})

    return {
        'type':  f'Result - {notif_sub1}',
        'migrated_8_data': {
            'validation_errors': error
        }
    }


def _map_ceased_fields(notif_sub2, notif_sub3):
    ceased_reason = {
        '6': CeaseReason.DUE_TO_AGE,
        '7': CeaseReason.NO_CERVIX,
        '8': CeaseReason.PATIENT_INFORMED_CHOICE,
        '77': CeaseReason.RADIOTHERAPY,
        '99': CeaseReason.MENTAL_CAPACITY_ACT,
    }

    if notif_sub2 == 'P':
        subtype_prefix = 'Recently Registered'
    elif notif_sub2 == 'R':
        subtype_prefix = 'Recently Ceased'

    return {
        'type': 'Result - CEASED',
        'subtype': f'{subtype_prefix} - {ceased_reason.get(notif_sub3)}'
    }


def _map_eceased_fields(notif_sub2, notif_sub3):
    mapped_value = _map_ceased_fields(notif_sub2, notif_sub3)
    mapped_value['type'] = 'Result - eCEASED'
    return mapped_value


def _map_gp_fields(notif_sub2, notif_sub3):

    if notif_sub2 == 'RS':
        subtype = 'Result Only'
    elif notif_sub2 == 'RT':
        subtype = 'Result with recall GP offers test'

    return {
        'type': 'Result - GP',
        'subtype': subtype
    }


def _map_letter_fields(notif_sub2, notif_sub3):
    letter_subtypes = {
        'RT': 'Result with recall GP offers test',
        'RN': 'Result with recall GP doesn’t test',
        'SR': 'Result with recall GP offers test; GP free text in letter',
        'RS': 'Result only',
        'SS': 'Result only GP offers test; GP free text in letter',
        'CC': 'Cease At upper age limit following a 2A, 0A, B0A, E0A, G0A, M0A, N0A or X0A test',
        'CD': 'Cease At upper age limit following non-attendance',
        'CE': 'Cease Due to absence of cervix',
        'CF': 'Cease Due to informed choice',
        'CG': 'Cease Due to Mental Capacity Act'
    }

    return {
        'type': 'Result - Letter',
        'subtype': letter_subtypes.get(notif_sub2),
        'letter_template': notif_sub3
    }


def _map_letter_not_produced_fields(notif_sub2, notif_sub3):
    letter_not_produced_subtypes = {
        'NR': 'Notification not required.',
        'AB': '1st notification to patient is due but GP has not yet been informed.',
        'NS': 'Notification not specified.',
        'RTY': 'Recall Type has been changed to a CALL.',
        'IN': 'Letter as given in DD/HP is not valid, e.g. too long.',
        'NF': 'Letter as given in DD/HP/Result Notif is not found in Letter library.',
        'NNS': 'Sender has requested no correspondence.',
        'CC': 'Action Code = "Z" but Notification not specified.',
        'DI': 'District is required as Orig or Dest but is no longer specified.',
        'GI': 'GP is required as Orig or Dest but is no longer specified.',
        'LI': 'Lab is required as Orig or Dest but is no longer specified.',
        'SI': 'Sender is required as Orig or Dest but is no longer specified.',
        'SM': 'Sender is required as Orig or Dest but is not found within CY record.',
        'LM': "Lab is required as Orig or Dest but an 'X' has been given in CY.",
        'PT': 'Sender or Lab is required as Orig or Dest but there is no previous test.',
        'LB': 'Notification not required - Lab is responsible for letters.',
        'CO': 'Source 7 (consultant) - Notification not required.',
        'CA': 'Lab Address used as Correspondence Address for Result Letter.',
        'FP69': 'FP69 status is set.',
        'OD': 'Result Letter NOT produced - Test date BEFORE cut-off Date (DD)',
        'OA': 'Auto Ceased Due to Age Letter NOT produced - Test date BEFORE cut-off Date (DD).',
        'DE': 'Result Letter NOT produced - Patient deducted.',
        'AREMP': 'REMOVED PATIENTS.',
        'ZZZGP': 'ZZZ REGISTERED'
    }

    return {
        'type': 'Result - Result Letter not produced',
        'reason_not_produced_code': notif_sub2,
        'reason_not_produced': letter_not_produced_subtypes.get(notif_sub2),
        'letter_template': notif_sub3
    }


def _map_smear_taker_fields(notif_sub2, notif_sub3):
    return {
        'type': 'Result - Smear Taker',
        'subtype': f'{notif_sub2} - {notif_sub3}'
    }


def _map_address_slip_fields(notif_sub2, notif_sub3):
    address_slip_subtypes = {
        'RS': 'Result only',
        'RT': 'Result with recall GP offers test',
    }

    return {
        'type': 'Result - Address Slip',
        'subtype': address_slip_subtypes.get(notif_sub2)
    }


def _map_address_labels_fields(notif_sub2, notif_sub3):

    return {
        'type': 'Result - Address labels',
    }
