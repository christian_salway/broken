
from transform_and_store_CSO8_data.cso8_fields import CSO8_FIELDS
from common.log import log
from transform_and_store_CSO8_data.log_references import LogReference

VALID_INVITATION_TYPES = ['N1', 'N2', 'eN1', 'eN2', 'L1', 'L2', 'SL', 'LA', 'DL', 'LT', 'GP']


def map_invitation_data(data_list):
    match_strategies = {
        'DL': _map_district_list_fields,
        'LT': _map_letter_fields,
        'GP': _map_gp_fields
    }

    notif_sub1_value = data_list[CSO8_FIELDS.index('NOTIF-SUB1')]

    if notif_sub1_value not in match_strategies.keys():
        error = validate_invitation_type(notif_sub1_value)
        mapped_data = {
            'type': _map_invitation_type(notif_sub1_value)
        }
        if error:
            log({'log_reference': LogReference.CSO8MAP0007, 'validation_errors': error})
            mapped_data['migrated_8_data'] = {
                'validation_errors': error
            }
    else:
        notif_sub2_value = data_list[CSO8_FIELDS.index('NOTIF-SUB2')]
        notif_sub3_value = data_list[CSO8_FIELDS.index('NOTIF-SUB3')]
        mapped_data = match_strategies[notif_sub1_value](notif_sub2_value, notif_sub3_value)

    return mapped_data


def validate_invitation_type(notif_sub1_value):
    INVITATION_TYPE_VALIDATION_ERROR = {
        'NOTIF-SUB1': {
            'data_error': f'Invitation type is not one of the expected values {VALID_INVITATION_TYPES}'
        }
    }
    if not notif_sub1_value:
        return INVITATION_TYPE_VALIDATION_ERROR

    if notif_sub1_value not in VALID_INVITATION_TYPES:
        return INVITATION_TYPE_VALIDATION_ERROR

    return None


def _map_invitation_type(raw_value):
    invitation_type = {
        'N1': 'First Non-Responder',
        'N2': 'Final Non-Responder',
        'eN1': 'First Non-Responder - Electronic',
        'eN2': 'Final Non-Responder - Electronic',
        'L1': 'First Non-Responder - Label',
        'L2': 'Final Non-Responder - Label',
        'SL': 'GP Slips - Suspended List',
        'LA': 'Labels',
        '': 'BLANK'
    }

    return invitation_type.get(raw_value, raw_value)


def _map_district_list_fields(notif_sub2_value, notif_sub3_value):
    reason_not_produced_code = notif_sub2_value
    reason_not_produced_map = {
        'LB': 'Lab producing letters - Notification not required',
        'AB': '1st notification to patient is due but RG has not yet been run',
        'NS': 'Notification not specified',
        'IN': 'Letter as given in ID is not valid, eg too long',
        'NF': 'Letter as given in ID is not found in Letter library',
        'DI': 'District is required as Orig or Dest but is no longer specified',
        'GI': 'GP is required as Orig or Dest but is no longer specified',
        'LI': 'Lab is required as Orig or Dest but is no longer specified',
        'SI': 'Sender is required as Orig or Dest but is no longer specified',
        'SM': 'Sender is required as Orig or Dest but is not found within CY record',
        'LM': "Lab is required as Orig or Dest but an 'X' has been given in CY",
        'PT': 'Sender or Lab is required as Orig or Dest but there is no previous test',
        'FP69': 'FP69 status is set',
        'AREMP': 'REMOVED PATIENTS',
        'ZZZGP': 'ZZZ REGISTERED',
        'NR': 'Notification not req'
    }

    mapped_fields = {
        'type': 'District List',
        'reason_not_produced_code': reason_not_produced_code,
        'reason_not_produced': reason_not_produced_map[reason_not_produced_code],
        'letter_template': notif_sub3_value
    }

    return mapped_fields


def _map_letter_fields(notif_sub2_value, notif_sub3_value):
    letter_types_map = {
        'GT': ('Invitation', "GP offers test, prior advice given"),
        'NT': ('Invitation', "GP offers test, no prior advice"),
        'GN': ('Invitation', "GP doesn't test, prior advice given"),
        'NN': ('Invitation', "GP doesn't test, prior advice given"),
        'SG': ('Invitation', "GP offers test, text"),
        'PT': ('Reminder', "GP offers test"),
        'PN': ('Reminder', "GP doesn't test"),
        'ST': ('Reminder', "GP offers test, text"),
        'SW': ('Invitation', "GP offers test, 1st recall, from Final Non-Responder"),
        'SX': ('Reminder', "2nd recall, from Final Non-Responder"),
        'SY': ('Reminder', "3rd recall, from Final Non-Responder"),
        'SZ': ('Reminder', "4th recall, from Final Non-Responder")
    }

    letter_type_raw_value = notif_sub2_value
    invite_or_reminder = letter_types_map[letter_type_raw_value][0]
    letter_type = f'{invite_or_reminder} Letter'

    mapped_fields = {
        'type': letter_type,
        'subtype': letter_types_map[letter_type_raw_value][1],
        'letter_template': notif_sub3_value
    }

    return mapped_fields


def _map_gp_fields(notif_sub2_value, notif_sub3_value):
    gp_letter_types_map = {
        'GT': ('First', f'GP Code: {notif_sub3_value} - GP offers test, prior advice given'),
        'PT': ('Second', f'GP Code: {notif_sub3_value} - GP offers test'),
        'NT': ('First', f'GP Code: {notif_sub3_value} - GP offers test, no prior advice'),
        'SW': ('First', f'GP Code: {notif_sub3_value} - from Final Non-Responder'),
        'SX': ('Second', f'GP Code: {notif_sub3_value} - from Final Non-Responder')
    }

    letter_type_raw_value = notif_sub2_value
    invite_or_reminder = gp_letter_types_map[letter_type_raw_value][0]

    letter_type = f'GP Produced {invite_or_reminder} Letter'

    subtype = gp_letter_types_map[letter_type_raw_value][1]

    return {
        'type': letter_type,
        'subtype': subtype
    }
