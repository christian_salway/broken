import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    CSO8MAP0001 = (logging.INFO, 'Message received from queue')
    CSO8MAP0002 = (logging.INFO, 'Extracted Record from SQS message')
    CSO8MAP0003 = (logging.INFO, 'Attempting to parse details from CSO8')
    CSO8MAP0004 = (logging.INFO, 'Successfully parsed details from CSO8')
    CSO8MAP0005 = (logging.ERROR, 'An error occurred when attempting to map and validate data.')
    CSO8MAP0006 = (logging.INFO, 'Migration for this notification type is not yet supported')
    CSO8MAP0007 = (logging.WARNING, 'CSO8 record has failed data validation')
    CSO8MAP0008 = (logging.INFO, 'Successfully created notification record')
    CSO8MAP0009 = (logging.INFO, 'Attempting to compare hashed source row with existing records')
    CSO8MAP0010 = (logging.INFO, 'This record already exists. Aborting creation of this CS08 record')
    CSO8MAP0011 = (logging.INFO, 'Confirmed record is not a duplicate')
