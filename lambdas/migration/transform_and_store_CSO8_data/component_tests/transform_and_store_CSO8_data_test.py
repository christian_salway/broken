import json
import hashlib
from unittest import TestCase
from unittest.mock import patch, Mock
from boto3.dynamodb.conditions import Key
from ddt import ddt, data, unpack
from collections import OrderedDict
from common.models.participant import ParticipantSortKey

mock_env_vars = {
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'ENABLE_MIGRATION_AUDIT': 'TRUE',
    'DYNAMODB_PARTICIPANTS': 'participants-table',
    'PARTICIPANTS_TABLE_NAME': 'PARTICIPANTS_TABLE_NAME'
}

with patch('os.environ', mock_env_vars):
    from common.test_mocks.mock_events import sqs_mock_event
    from transform_and_store_CSO8_data import transform_and_store_CSO8_data


VALID_PNL_ROW = OrderedDict([
    ('SOURCE_CIPHER', 'BD'),
    ('NHSNUM', '9100001848'),
    ('NOTIF-DATE', '20060109'),
    ('NOTIF-SEQ', '1'),
    ('NOTIF-TYPE-CODE', 'RG'),
    ('NOTIF-SUB1', 'L'),
    ('NOTIF-SUB2', 'G'),
    ('NOTIF-SUB3', 'Paper')
])

VALID_INVITATION_ROW = OrderedDict([
    ('SOURCE_CIPHER', 'BD'),
    ('NHSNUM', '9100001848'),
    ('NOTIF-DATE', '20060109'),
    ('NOTIF-SEQ', '1'),
    ('NOTIF-TYPE-CODE', 'RP'),
    ('NOTIF-SUB1', ''),
    ('NOTIF-SUB2', ''),
    ('NOTIF-SUB3', '')
])

VALID_RESULT_ROW = OrderedDict([
    ('SOURCE_CIPHER', 'BD'),
    ('NHSNUM', '9100001848'),
    ('NOTIF-DATE', '20060109'),
    ('NOTIF-SEQ', '1'),
    ('NOTIF-TYPE-CODE', 'CP'),
    ('NOTIF-SUB1', ''),
    ('NOTIF-SUB2', ''),
    ('NOTIF-SUB3', '')
])

VALID_FOLLOW_UP_ROW = OrderedDict([
    ('SOURCE_CIPHER', 'BD'),
    ('NHSNUM', '9100001848'),
    ('NOTIF-DATE', '20060109'),
    ('NOTIF-SEQ', '1'),
    ('NOTIF-TYPE-CODE', 'DF'),
    ('NOTIF-SUB1', ''),
    ('NOTIF-SUB2', ''),
    ('NOTIF-SUB3', '')
])

participants_table_mock = Mock()
dynamodb_resource_mock = Mock()
dynamodb_resource_mock.batch_get_item.return_value = {
    'Responses': {
        'participants-table': []
    }
}
sqs_client_mock = Mock()


def _build_event_and_raw_row(row_dict, fields_to_update=None):
    updated_row = {}
    updated_row.update(row_dict)
    if fields_to_update:
        for key in fields_to_update:
            updated_row[key] = fields_to_update[key]
    raw_row = '|'.join(updated_row.values())
    body_dict = {
        'internal_id': 'internal_id',
        'file_name': 'file_name_before_part',
        'file_part_name': 'file_key',
        'line_index': 1,
        'line': raw_row
    }

    return sqs_mock_event(body_dict), raw_row


@ddt
@patch('os.environ', mock_env_vars)
@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(return_value=participants_table_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_resource',
       Mock(return_value=dynamodb_resource_mock))
class TestTransformAndStoreCSO8Data(TestCase):

    @classmethod
    @patch('boto3.client')
    @patch('os.environ', mock_env_vars)
    def setUpClass(cls, boto3_client, *args):
        cls.log_patcher = patch('common.log.log')
        participants_table_mock.query.return_value = []
        cls.participants_table_mock = participants_table_mock

        boto3_client.side_effect = lambda *args, **kwargs: sqs_client_mock if args and args[0] == 'sqs' else Mock()
        cls.sqs_client_mock = sqs_client_mock

    def setUp(self):
        self.participants_table_mock.reset_mock()
        self.sqs_client_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @unpack
    @data(('L', 'Status - CALLED'),
          ('N', 'Status - ROUTINE'),
          ('R', 'Status - REPEAT_ADVISED'),
          ('I', 'Status - INADEQUATE'),
          ('S', 'Status - SUSPENDED'),
          ('C', 'Status - CEASED'))
    def test_map_pnl_data_with_valid_sub_code_1(self, sub_code_1, expected_subtype):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        event, raw_row = _build_event_and_raw_row(VALID_PNL_ROW, fields_to_update={'NOTIF-SUB1': sub_code_1})

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('subtype'), expected_subtype)
        self.assertEqual(kwargs['Item'].get('migrated_8_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(('X', 'Status - X', "Status code is not one of the expected values ['L', 'N', 'R', 'I', 'S', 'C']"),
          ('', 'Status - BLANK', "Status code is not one of the expected values ['L', 'N', 'R', 'I', 'S', 'C']"))
    def test_map_pnl_data_with_invalid_sub_code_1(self, sub_code_1, expected_subtype, data_validation_error):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        event, raw_row = _build_event_and_raw_row(VALID_PNL_ROW, fields_to_update={'NOTIF-SUB1': sub_code_1})

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct subtype
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('subtype'), expected_subtype)
        actual_validation_errors = kwargs['Item'].get('migrated_8_data').get('validation_errors', {})
        self.assertEqual(actual_validation_errors.get('NOTIF-SUB1', {}).get('data_error'), data_validation_error)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(('', 'PNL List - Not sent'))
    def test_map_pnl_data_no_pnl_type_resolves_as_not_sent(self, sub_code_3, expected_type):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        event, raw_row = _build_event_and_raw_row(VALID_PNL_ROW, fields_to_update={'NOTIF-SUB3': sub_code_3})

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct subtype
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), expected_type)
        self.assertEqual(kwargs['Item'].get('migrated_8_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('N1', 'First Non-Responder'),
        ('N2', 'Final Non-Responder'),
        ('eN1', 'First Non-Responder - Electronic'),
        ('eN2', 'Final Non-Responder - Electronic'),
        ('L1', 'First Non-Responder - Label'),
        ('L2', 'Final Non-Responder - Label'),
        ('SL', 'GP Slips - Suspended List'),
        ('LA', 'Labels')
    )
    def test_map_invitation_data_with_valid_invitation_type(self, invitation_type, mapped_type):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        event, raw_row = _build_event_and_raw_row(
            VALID_INVITATION_ROW, fields_to_update={'NOTIF-SUB1': invitation_type})

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), mapped_type)
        self.assertEqual(kwargs['Item'].get('migrated_8_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('DL', 'LB', 'Lab producing letters - Notification not required'),
        ('DL', 'AB', '1st notification to patient is due but RG has not yet been run'),
        ('DL', 'NS', 'Notification not specified'),
        ('DL', 'IN', 'Letter as given in ID is not valid, eg too long'),
        ('DL', 'NF', 'Letter as given in ID is not found in Letter library'),
        ('DL', 'DI', 'District is required as Orig or Dest but is no longer specified'),
        ('DL', 'GI', 'GP is required as Orig or Dest but is no longer specified'),
        ('DL', 'LI', 'Lab is required as Orig or Dest but is no longer specified'),
        ('DL', 'SI', 'Sender is required as Orig or Dest but is no longer specified'),
        ('DL', 'SM', 'Sender is required as Orig or Dest but is not found within CY record'),
        ('DL', 'LM', "Lab is required as Orig or Dest but an 'X' has been given in CY"),
        ('DL', 'PT', 'Sender or Lab is required as Orig or Dest but there is no previous test'),
        ('DL', 'FP69', 'FP69 status is set'),
        ('DL', 'AREMP', 'REMOVED PATIENTS'),
        ('DL', 'ZZZGP', 'ZZZ REGISTERED'),
        ('DL', 'NR', 'Notification not req')
    )
    def test_map_invitation_data_with_valid_district_list(self, notification_type, reason_code, mapped_reason):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        fields_to_update = {
            'NOTIF-SUB1': notification_type,
            'NOTIF-SUB2': reason_code,
            'NOTIF-SUB3': 'XX'
        }

        event, raw_row = _build_event_and_raw_row(VALID_INVITATION_ROW, fields_to_update)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('reason_not_produced_code'), reason_code)
        self.assertEqual(kwargs['Item'].get('reason_not_produced'), mapped_reason)
        self.assertEqual(kwargs['Item'].get('letter_template'), 'XX')
        self.assertEqual(kwargs['Item'].get('migrated_8_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('LT', 'GT', 'Invitation', "GP offers test, prior advice given"),
        ('LT', 'NT', 'Invitation', "GP offers test, no prior advice"),
        ('LT', 'GN', 'Invitation', "GP doesn't test, prior advice given"),
        ('LT', 'NN', 'Invitation', "GP doesn't test, prior advice given"),
        ('LT', 'SG', 'Invitation', "GP offers test, text"),
        ('LT', 'PT', 'Reminder', "GP offers test"),
        ('LT', 'PN', 'Reminder', "GP doesn't test"),
        ('LT', 'ST', 'Reminder', "GP offers test, text"),
        ('LT', 'SW', 'Invitation', "GP offers test, 1st recall, from Final Non-Responder"),
        ('LT', 'SX', 'Reminder', "2nd recall, from Final Non-Responder"),
        ('LT', 'SY', 'Reminder', "3rd recall, from Final Non-Responder"),
        ('LT', 'SZ', 'Reminder', "4th recall, from Final Non-Responder")
    )
    def test_map_invitation_data_with_valid_gp_letter_types(self,
                                                            notification_type,
                                                            letter_type_raw_value,
                                                            invite_or_reminder,
                                                            mapped_subtype):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        fields_to_update = {
            'NOTIF-SUB1': notification_type,
            'NOTIF-SUB2': letter_type_raw_value,
            'NOTIF-SUB3': 'XX'
        }

        event, raw_row = _build_event_and_raw_row(VALID_INVITATION_ROW, fields_to_update)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), f'{invite_or_reminder} Letter')
        self.assertEqual(kwargs['Item'].get('subtype'), mapped_subtype)
        self.assertEqual(kwargs['Item'].get('letter_template'), 'XX')
        self.assertEqual(kwargs['Item'].get('migrated_8_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('GP', 'GT', 'First', 'GP Code: XX - GP offers test, prior advice given'),
        ('GP', 'PT', 'Second', 'GP Code: XX - GP offers test'),
        ('GP', 'NT', 'First', 'GP Code: XX - GP offers test, no prior advice')
    )
    def test_map_invitation_data_with_valid_letter_types(self,
                                                         notification_type,
                                                         letter_type_raw_value,
                                                         invite_or_reminder,
                                                         mapped_subtype):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        fields_to_update = {
            'NOTIF-SUB1': notification_type,
            'NOTIF-SUB2': letter_type_raw_value,
            'NOTIF-SUB3': 'XX'
        }

        event, raw_row = _build_event_and_raw_row(VALID_INVITATION_ROW, fields_to_update)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record

        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), f'GP Produced {invite_or_reminder} Letter')
        self.assertEqual(kwargs['Item'].get('subtype'), mapped_subtype)
        self.assertEqual(kwargs['Item'].get('migrated_8_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('XX', 'XX'),
        ('', 'BLANK')
    )
    def test_map_invitation_data_with_invalid_invitation_types(self,
                                                               invitation_type,
                                                               mapped_type):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        fields_to_update = {
            'NOTIF-SUB1': invitation_type,
        }

        event, raw_row = _build_event_and_raw_row(VALID_INVITATION_ROW, fields_to_update)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        self.maxDiff = None
        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), mapped_type)

        # Asserts mapped data contains validation error
        actual_validation_errors = kwargs['Item'].get('migrated_8_data').get('validation_errors', {})
        error_message = "Invitation type is not one of the expected values ['N1', 'N2', 'eN1', 'eN2', 'L1', 'L2', 'SL', 'LA', 'DL', 'LT', 'GP']"  
        self.assertEqual(actual_validation_errors.get('NOTIF-SUB1', {}).get('data_error'), error_message)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('CEASED', 'R', '6', 'Result - CEASED', 'Recently Ceased - DUE_TO_AGE'),
        ('eCEASED', 'R', '6', 'Result - eCEASED', 'Recently Ceased - DUE_TO_AGE'),
        ('GP', 'RS', 'O08', 'Result - GP', 'Result Only'),
        ('LT', 'RS', 'PX0A', 'Result - Letter', 'Result only'),
        ('SN', 'Card', 'Deduct', 'Result - Smear Taker', 'Card - Deduct'),
        ('SL', 'RT', '', 'Result - Address Slip', 'Result with recall GP offers test'),
        ('LA', '', '', 'Result - Address labels', None),
    )
    def test_map_result_data_with_valid_result_types(self, result_type, notif_sub2, notif_sub3,
                                                     mapped_type, mapped_subtype):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        fields_to_update = {
            'NOTIF-SUB1': result_type,
            'NOTIF-SUB2': notif_sub2,
            'NOTIF-SUB3': notif_sub3
        }

        event, raw_row = _build_event_and_raw_row(VALID_RESULT_ROW, fields_to_update)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record

        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), mapped_type)
        self.assertEqual(kwargs['Item'].get('subtype'), mapped_subtype)
        self.assertEqual(kwargs['Item'].get('migrated_8_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('LT', 'RT', '1234', 'Result with recall GP offers test'),
        ('LT', 'RN', '1234', 'Result with recall GP doesn’t test'),
        ('LT', 'SR', '1234', 'Result with recall GP offers test; GP free text in letter'),
        ('LT', 'CG', '1234', 'Cease Due to Mental Capacity Act')
    )
    def test_map_result_data_with_valid_letter_types(self, result_type, result_subtype,
                                                     letter_code, mapped_subtype):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        fields_to_update = {
            'NOTIF-SUB1': result_type,
            'NOTIF-SUB2': result_subtype,
            'NOTIF-SUB3': letter_code
        }

        event, raw_row = _build_event_and_raw_row(VALID_RESULT_ROW, fields_to_update)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record

        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), 'Result - Letter')
        self.assertEqual(kwargs['Item'].get('subtype'), mapped_subtype)
        self.assertEqual(kwargs['Item'].get('letter_template'), letter_code)
        self.assertEqual(kwargs['Item'].get('migrated_8_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('RS', 'NR', '1234', 'Notification not required.'),
        ('RS', 'AB', '1234', '1st notification to patient is due but GP has not yet been informed.'),
        ('RS', 'NS', '1234', 'Notification not specified.'),
        ('RS', 'ZZZGP', '1234', 'ZZZ REGISTERED'),
    )
    def test_map_result_data_with_valid_letter_not_produced_types(self, result_type, reason_not_produced_code,
                                                                  letter_code, reason_not_produced):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        fields_to_update = {
            'NOTIF-SUB1': result_type,
            'NOTIF-SUB2': reason_not_produced_code,
            'NOTIF-SUB3': letter_code
        }

        event, raw_row = _build_event_and_raw_row(VALID_RESULT_ROW, fields_to_update)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record

        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), 'Result - Result Letter not produced')
        self.assertEqual(kwargs['Item'].get('reason_not_produced_code'), reason_not_produced_code)
        self.assertEqual(kwargs['Item'].get('reason_not_produced'), reason_not_produced)
        self.assertEqual(kwargs['Item'].get('letter_template'), letter_code)
        self.assertEqual(kwargs['Item'].get('migrated_8_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('XX', 'XX'),
        ('', '')
    )
    def test_map_result_data_with_invalid_result_types(self, result_type, mapped_type):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        fields_to_update = {
            'NOTIF-SUB1': result_type,
            'NOTIF-SUB2': '',
            'NOTIF-SUB3': '',
        }

        event, raw_row = _build_event_and_raw_row(VALID_RESULT_ROW, fields_to_update)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        self.maxDiff = None
        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), f'Result - {mapped_type}')

        # Asserts mapped data contains validation error
        actual_validation_errors = kwargs['Item'].get('migrated_8_data').get('validation_errors', {})
        error_message = f"Result type '{mapped_type}' is not one of the expected values ['CEASED', 'eCEASED', 'GP', 'LT', 'RS', 'SN', 'SL', 'LA']"  
        self.assertEqual(actual_validation_errors.get('NOTIF-SUB1', {}).get('data_error'), error_message)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')
        self.assertTrue(True)

    def test_duplicate_record_is_not_added(self):
        test_record = {'nhs_number': '9100001848', 'notification_date': '2006-01-09', 'type': 'BLANK'}

        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': [test_record]}
        self.participants_table_mock.query.side_effect = _query_mock

        event, raw_row = _build_event_and_raw_row(VALID_INVITATION_ROW)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it does not store the record
        self.participants_table_mock.put_item.assert_not_called()

    @unpack
    @data(
        (
            'CIN',
            '20110921',
            'Deducted Abnormal Follow up',
            "Abnormal Follow-up for transfer in - Last test date: 2011-09-21"
        ),
        (
            'ID',
            '20110921',
            'Deducted Abnormal Follow up',
            "Abnormal Follow-up for internal transfer - Today's Date: 2011-09-21"
        ),
        (
            'CY',
            '20110921',
            'Deducted Abnormal Follow up',
            "Abnormal Follow-up for transfer out - Recall Date: 2011-09-21"
        )
    )
    def test_map_valid_follow_up(self, notif_sub1, notif_sub2, mapped_type, mapped_subtype):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        fields_to_update = {
            'NOTIF-SUB1': notif_sub1,
            'NOTIF-SUB2': notif_sub2
        }

        event, raw_row = _build_event_and_raw_row(VALID_FOLLOW_UP_ROW, fields_to_update)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record

        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), mapped_type)
        self.assertEqual(kwargs['Item'].get('subtype'), mapped_subtype)
        self.assertEqual(kwargs['Item'].get('migrated_8_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @unpack
    @data(
        ('CIND', ''),
        ('CI', '')
    )
    def test_map_invalid_follow_up_type(self, notif_sub1, mapped_type):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123',
                                   'sort_key': ParticipantSortKey.PARTICIPANT,
                                   'nhais_cipher': 'ENG'}]}
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        fields_to_update = {
            'NOTIF-SUB1': notif_sub1
        }

        event, raw_row = _build_event_and_raw_row(VALID_FOLLOW_UP_ROW, fields_to_update)

        context = Mock()
        context.function_name = ''

        transform_and_store_CSO8_data.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)
        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')
        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(kwargs['KeyConditionExpression'], expected_key_condition)
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        self.maxDiff = None
        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(kwargs['Item'].get('type'), f'Deducted Abnormal Follow up - {notif_sub1}')

        # Asserts mapped data contains validation error
        actual_validation_errors = kwargs['Item'].get('migrated_8_data').get('validation_errors', {})
        error_message = f"Deducted Abnormal Follow up type '{notif_sub1}' is not one of the expected values ['CIN', 'ID', 'CY']"  
        self.assertEqual(actual_validation_errors.get('NOTIF-SUB1', {}).get('data_error'), error_message)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO8')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')
        self.assertTrue(True)
