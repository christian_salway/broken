from ddt import ddt, data, unpack
from mock import patch
from transform_and_store_CSO8_data.map_follow_up_data import (
    invalid_follow_up_type,
    LogReference,
    _map_abnormal_follow_up_internal_xfer,
    _map_abnormal_follow_up_xfer_in,
    _map_abnormal_follow_up_xfer_out,
    map_follow_up_data,
    _validate_and_map_date
)
from unittest import TestCase

MODULE = 'transform_and_store_CSO8_data.map_follow_up_data'


@ddt
@patch(f'{MODULE}.log')
class TestMapFollowUpData(TestCase):

    @unpack
    @data(
        ('ABC',),
        ('CINDER',)
    )
    def test_invalid_follow_up_type(self, follow_up_type, mock_lock):
        expected_validation_errors = {
            'NOTIF-SUB1': {
                'data_error': f"Deducted Abnormal Follow up type '{follow_up_type}' is not one of the expected values ['CIN', 'ID', 'CY']" 
            }
        }

        expected_response = {
            'type': f'Deducted Abnormal Follow up - {follow_up_type}',
            'migrated_8_data': {
                'validation_errors': expected_validation_errors
            }
        }

        expected_log = {
            'log_reference': LogReference.CSO8MAP0007,
            'validation_errors': expected_validation_errors
        }

        actual_response = invalid_follow_up_type(follow_up_type)

        self.assertEqual(actual_response, expected_response)
        mock_lock.assert_called_with(expected_log)

    @unpack
    @data(
        ("20210419", "2021-04-19"),
        ("19690201", "1969-02-01")
    )
    def test_validate_and_map_date(self, given_value, expected_date, mock_log):

        success, actual_date, error = _validate_and_map_date(given_value)

        self.assertTrue(success)
        self.assertIsNone(error)
        self.assertEqual(actual_date, expected_date)

        mock_log.assert_not_called()

    @unpack
    @data(
        ('20210231', False),
        ('19691301', False),
        ('Yesterday', True),
        ('2021010', True)
    )
    def test_validate_and_map_date_invalid(self, given_value, format_error, mock_log):

        if format_error:
            expected_error = {
                'NOTIF-SUB2': {
                    'data_error': 'Date must be of the format CCYYMMDD'
                }
            }
        else:
            expected_error = {
                'NOTIF-SUB2': {
                    'data_error': f'{given_value} is not a valid date'
                }
            }

        expected_log = {
            'log_reference': LogReference.CSO8MAP0007,
            'validation_errors': expected_error
        }

        success, actual_date, error = _validate_and_map_date(given_value)

        self.assertFalse(success)
        self.assertIsNone(actual_date)
        self.assertEqual(error, expected_error)

        mock_log.assert_called_with(expected_log)

    def test_map_abnormal_follow_up_xfer_in(self, mock_log):
        expected_response = {
            'subtype': 'Abnormal Follow-up for transfer in - Last test date: 2021-02-01',
            'type': 'Deducted Abnormal Follow up'
        }

        response = _map_abnormal_follow_up_xfer_in('20210201')

        self.assertEqual(response, expected_response)
        mock_log.assert_not_called()

    def test_map_abnormal_follow_up_xfer_in_invalid(self, mock_log):
        validation_errors = {
            'NOTIF-SUB2': {
                'data_error': '20211301 is not a valid date'
            }
        }

        expected_response = {
            'subtype': 'Abnormal Follow-up for transfer in',
            'type': 'Deducted Abnormal Follow up',
            'migrated_8_data': {
                'validation_errors': validation_errors
            }
        }

        expected_log = {
            'log_reference': LogReference.CSO8MAP0007,
            'validation_errors': validation_errors
        }

        response = _map_abnormal_follow_up_xfer_in('20211301')

        self.assertEqual(response, expected_response)
        mock_log.assert_called_with(expected_log)

    def test_map_abnormal_follow_up_internal_xfer(self, mock_log):
        expected_response = {
            'subtype': "Abnormal Follow-up for internal transfer - Today's Date: 2021-02-01",
            'type': 'Deducted Abnormal Follow up'
        }

        response = _map_abnormal_follow_up_internal_xfer('20210201')

        self.assertEqual(response, expected_response)
        mock_log.assert_not_called()

    def test_map_abnormal_follow_up_internal_xfer_invalid(self, mock_log):
        validation_errors = {
            'NOTIF-SUB2': {
                'data_error': '20211301 is not a valid date'
            }
        }

        expected_response = {
            'subtype': 'Abnormal Follow-up for internal transfer',
            'type': 'Deducted Abnormal Follow up',
            'migrated_8_data': {
                'validation_errors': validation_errors
            }
        }

        expected_log = {
            'log_reference': LogReference.CSO8MAP0007,
            'validation_errors': validation_errors
        }

        response = _map_abnormal_follow_up_internal_xfer('20211301')

        self.assertEqual(response, expected_response)
        mock_log.assert_called_with(expected_log)

    def test_map_abnormal_follow_up_xfer_out(self, mock_log):
        expected_response = {
            'subtype': "Abnormal Follow-up for transfer out - Recall Date: 2021-02-01",
            'type': 'Deducted Abnormal Follow up'
        }

        response = _map_abnormal_follow_up_xfer_out('20210201')

        self.assertEqual(response, expected_response)
        mock_log.assert_not_called()

    def test_map_abnormal_follow_up_xfer_out_invalid(self, mock_log):
        validation_errors = {
            'NOTIF-SUB2': {
                'data_error': '20211301 is not a valid date'
            }
        }

        expected_response = {
            'subtype': 'Abnormal Follow-up for transfer out',
            'type': 'Deducted Abnormal Follow up',
            'migrated_8_data': {
                'validation_errors': validation_errors
            }
        }

        expected_log = {
            'log_reference': LogReference.CSO8MAP0007,
            'validation_errors': validation_errors
        }

        response = _map_abnormal_follow_up_xfer_out('20211301')

        self.assertEqual(response, expected_response)
        mock_log.assert_called_with(expected_log)

    @unpack
    @data(
        (
            [None, None, None, None, None, 'CIN', '20210419'],
            {
                'type': 'Deducted Abnormal Follow up',
                'subtype': 'Abnormal Follow-up for transfer in - Last test date: 2021-04-19'
            }
        ),
        (
            [None, None, None, None, None, 'ID', '20210301'],
            {
                'type': 'Deducted Abnormal Follow up',
                'subtype': "Abnormal Follow-up for internal transfer - Today's Date: 2021-03-01"
            }
        ),
        (
            [None, None, None, None, None, 'CY', '20210228'],
            {
                'type': 'Deducted Abnormal Follow up',
                'subtype': 'Abnormal Follow-up for transfer out - Recall Date: 2021-02-28'
            }
        )
    )
    def test_map_follow_up_data(self, data_list, expected_response, mock_log):
        actual_response = map_follow_up_data(data_list)

        self.assertEqual(actual_response, expected_response)
        mock_log.assert_not_called()

    @unpack
    @data(
        (
            [None, None, None, None, None, 'CIND', '20210419'],
            {
                'type': 'Deducted Abnormal Follow up - CIND',
                'migrated_8_data': {
                    'validation_errors': {
                        'NOTIF-SUB1': {
                            'data_error': "Deducted Abnormal Follow up type 'CIND' is not one of the expected values ['CIN', 'ID', 'CY']" 
                        }
                    }
                }
            },
            {
                'log_reference': LogReference.CSO8MAP0007,
                'validation_errors': {
                    'NOTIF-SUB1': {
                        'data_error': "Deducted Abnormal Follow up type 'CIND' is not one of the expected values ['CIN', 'ID', 'CY']" 
                    }
                }
            }
        ),
        (
            [None, None, None, None, None, 'CIN', '20210231'],
            {
                'type': 'Deducted Abnormal Follow up',
                'subtype': 'Abnormal Follow-up for transfer in',
                'migrated_8_data': {
                    'validation_errors': {
                        'NOTIF-SUB2': {
                            'data_error': '20210231 is not a valid date'
                        }
                    }
                }
            },
            {
                'log_reference': LogReference.CSO8MAP0007,
                'validation_errors': {
                    'NOTIF-SUB2': {
                        'data_error': '20210231 is not a valid date'
                    }
                }
            }
        ),
    )
    def test_map_follow_up_data_invalid(self, data_list, expected_response, expected_log, mock_log):
        actual_response = map_follow_up_data(data_list)

        self.assertEqual(actual_response, expected_response)
        mock_log.assert_called_with(expected_log)
