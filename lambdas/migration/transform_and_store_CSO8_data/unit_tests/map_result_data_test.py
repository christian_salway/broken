from unittest import TestCase
from mock import patch
from ddt import ddt, data, unpack
from transform_and_store_CSO8_data.log_references import LogReference
from transform_and_store_CSO8_data.map_result_data import (
    map_result_data,
    invalid_result_type,
    _map_ceased_fields,
    _map_eceased_fields,
    _map_gp_fields,
    _map_letter_fields,
    _map_letter_not_produced_fields,
    _map_smear_taker_fields,
    _map_address_slip_fields,
    _map_address_labels_fields
)


@ddt
class TestResultData(TestCase):

    @patch('transform_and_store_CSO8_data.map_result_data.invalid_result_type')
    def test_mapping_with_invalid_result_type(self, invalid_result_type_mock):
        raw_data = "LS|9234567890|20010101|1|CP|ZZZ|P|6"
        data_list = raw_data.split('|')
        invalid_map = {'type': 'type', 'migrated_8_data': {}}
        invalid_result_type_mock.return_value = invalid_map
        mapped_data = map_result_data(data_list)

        invalid_result_type_mock.assert_called()
        self.assertEqual(mapped_data, invalid_map)

    @patch('transform_and_store_CSO8_data.map_result_data.log')
    def test_invalid_result_type_mapped_fields(self, log_mock):
        notif_sub1_value = "ZZZ"
        mapped_data = invalid_result_type(notif_sub1_value)
        expected_validation_error = {
            'NOTIF-SUB1': {
                'data_error': "Result type 'ZZZ' is not one of the expected values ['CEASED', 'eCEASED', 'GP', 'LT', 'RS', 'SN', 'SL', 'LA']"  
            }
        }
        expected = {
            'type':  'Result - ZZZ',
            'migrated_8_data': {
                'validation_errors': expected_validation_error
            }
        }

        log_mock.assert_called_with({'log_reference': LogReference.CSO8MAP0007,
                                     'validation_errors': expected_validation_error})

        self.assertEqual(mapped_data, expected)

    @unpack
    @data(
        ('6', 'DUE_TO_AGE'),
        ('7', 'NO_CERVIX'),
        ('8', "PATIENT_INFORMED_CHOICE"),
        ('77', 'RADIOTHERAPY'),
        ('99', 'MENTAL_CAPACITY_ACT')
    )
    def test_fields_mapped_when_ceased_type(self, input_value, expected):

        mapped_1 = _map_ceased_fields('P', input_value)
        mapped_2 = _map_ceased_fields('R', input_value)

        expected_mapping_1 = {
            'type': 'Result - CEASED',
            'subtype': f'Recently Registered - {expected}'
        }
        expected_mapping_2 = {
            'type': 'Result - CEASED',
            'subtype': f'Recently Ceased - {expected}'
        }

        self.assertEqual(mapped_1, expected_mapping_1)
        self.assertEqual(mapped_2, expected_mapping_2)

    @unpack
    @data(
        ('6', 'DUE_TO_AGE'),
        ('7', 'NO_CERVIX'),
        ('8', "PATIENT_INFORMED_CHOICE"),
        ('77', 'RADIOTHERAPY'),
        ('99', 'MENTAL_CAPACITY_ACT')
    )
    def test_fields_mapped_when_eceased_type(self, input_value, expected):
        mapped_1 = _map_eceased_fields('P', input_value)
        mapped_2 = _map_eceased_fields('R', input_value)

        expected_mapping_1 = {
            'type': 'Result - eCEASED',
            'subtype': f'Recently Registered - {expected}'
        }
        expected_mapping_2 = {
            'type': 'Result - eCEASED',
            'subtype': f'Recently Ceased - {expected}'
        }

        self.assertEqual(mapped_1, expected_mapping_1)
        self.assertEqual(mapped_2, expected_mapping_2)

    @unpack
    @data(
        ('RS', 'Result Only'),
        ('RT', 'Result with recall GP offers test')
    )
    def test_fields_mapped_when_gp_type(self, input_value, expected):
        mapped = _map_gp_fields(input_value, "")

        expected_mapping = {
            'type': 'Result - GP',
            'subtype': f'{expected}'
        }

        self.assertEqual(mapped, expected_mapping)

    @unpack
    @data(
        ('RT', 'Result with recall GP offers test'),
        ('RN', 'Result with recall GP doesn’t test'),
        ('SR', 'Result with recall GP offers test; GP free text in letter'),
        ('RS', 'Result only'),
        ('SS', 'Result only GP offers test; GP free text in letter'),
        ('CC', 'Cease At upper age limit following a 2A, 0A, B0A, E0A, G0A, M0A, N0A or X0A test'),
        ('CD', 'Cease At upper age limit following non-attendance'),
        ('CE', 'Cease Due to absence of cervix'),
        ('CF', 'Cease Due to informed choice'),
        ('CG', 'Cease Due to Mental Capacity Act')
    )
    def test_fields_mapped_when_letter_type(self, input_value, expected):
        mapped = _map_letter_fields(input_value, "LetterCode")

        expected_mapping = {
            'type': 'Result - Letter',
            'subtype': expected,
            'letter_template': "LetterCode"
        }

        self.assertEqual(mapped, expected_mapping)

    @unpack
    @data(
        ('NR', 'Notification not required.'),
        ('AB', '1st notification to patient is due but GP has not yet been informed.'),
        ('NS', 'Notification not specified.'),
        ('RTY', 'Recall Type has been changed to a CALL.'),
        ('IN', 'Letter as given in DD/HP is not valid, e.g. too long.'),
        ('NF', 'Letter as given in DD/HP/Result Notif is not found in Letter library.'),
        ('NNS', 'Sender has requested no correspondence.'),
        ('CC', 'Action Code = "Z" but Notification not specified.'),
        ('DI', 'District is required as Orig or Dest but is no longer specified.'),
        ('GI', 'GP is required as Orig or Dest but is no longer specified.'),
        ('LI', 'Lab is required as Orig or Dest but is no longer specified.'),
        ('SI', 'Sender is required as Orig or Dest but is no longer specified.'),
        ('SM', 'Sender is required as Orig or Dest but is not found within CY record.'),
        ('LM', "Lab is required as Orig or Dest but an 'X' has been given in CY."),
        ('PT', 'Sender or Lab is required as Orig or Dest but there is no previous test.'),
        ('LB', 'Notification not required - Lab is responsible for letters.'),
        ('CO', 'Source 7 (consultant) - Notification not required.'),
        ('CA', 'Lab Address used as Correspondence Address for Result Letter.'),
        ('FP69', 'FP69 status is set.'),
        ('OD', 'Result Letter NOT produced - Test date BEFORE cut-off Date (DD)'),
        ('OA', 'Auto Ceased Due to Age Letter NOT produced - Test date BEFORE cut-off Date (DD).'),
        ('DE', 'Result Letter NOT produced - Patient deducted.'),
        ('AREMP', 'REMOVED PATIENTS.'),
        ('ZZZGP', 'ZZZ REGISTERED'),
    )
    def test_fields_mapped_when_letter_not_produced_type(self, input_value, expected):
        mapped = _map_letter_not_produced_fields(input_value, "LetterCode")
        expected_mapping = {
            'type': 'Result - Result Letter not produced',
            'reason_not_produced_code': input_value,
            'reason_not_produced': expected,
            'letter_template': "LetterCode"
        }

        self.assertEqual(mapped, expected_mapping)

    def test_fields_mapped_when_smear_taker_type(self):
        mapped = _map_smear_taker_fields("Card", "Deduct")
        expected_mapping = {
            'type': 'Result - Smear Taker',
            'subtype': 'Card - Deduct'
        }

        self.assertEqual(mapped, expected_mapping)

    @unpack
    @data(
        ('RS', 'Result only'),
        ('RT', 'Result with recall GP offers test')
    )
    def test_fields_mapped_when_address_slip_type(self, input_value, expected):
        mapped = _map_address_slip_fields(input_value, "")
        expected_mapping = {
            'type': 'Result - Address Slip',
            'subtype': expected
        }

        self.assertEqual(mapped, expected_mapping)

    def test_fields_mapped_when_address_labels_type(self):
        mapped = _map_address_labels_fields("", "")
        expected_mapping = {
            'type': 'Result - Address labels',
        }

        self.assertEqual(mapped, expected_mapping)
