from unittest import TestCase
from mock import patch
from ddt import ddt, data, unpack
from transform_and_store_CSO8_data.log_references import LogReference
from transform_and_store_CSO8_data.map_invitation_data import (
    _map_invitation_type,
    _map_district_list_fields,
    _map_letter_fields,
    _map_gp_fields,
    map_invitation_data,
    validate_invitation_type
)


@ddt
class TestMapInvitationData(TestCase):

    @unpack
    @data(
        ('N1', 'First Non-Responder'),
        ('N2', 'Final Non-Responder'),
        ('eN1', 'First Non-Responder - Electronic'),
        ('eN2', 'Final Non-Responder - Electronic'),
        ('L1', 'First Non-Responder - Label'),
        ('L2', 'Final Non-Responder - Label'),
        ('SL', 'GP Slips - Suspended List'),
        ('LA', 'Labels'),
        ('XX', 'XX'),
        ('', 'BLANK')
    )
    def test_map_invitation_type(self, input_value, expected):
        actual = _map_invitation_type(input_value)
        self.assertEqual(actual, expected)

    @unpack
    @data(('LB', 'Lab producing letters - Notification not required'),
          ('AB', '1st notification to patient is due but RG has not yet been run'),
          ('NS', 'Notification not specified'),
          ('IN', 'Letter as given in ID is not valid, eg too long'),
          ('NF', 'Letter as given in ID is not found in Letter library'),
          ('DI', 'District is required as Orig or Dest but is no longer specified'),
          ('GI', 'GP is required as Orig or Dest but is no longer specified'),
          ('LI', 'Lab is required as Orig or Dest but is no longer specified'),
          ('SI', 'Sender is required as Orig or Dest but is no longer specified'),
          ('SM', 'Sender is required as Orig or Dest but is not found within CY record'),
          ('LM', "Lab is required as Orig or Dest but an 'X' has been given in CY"),
          ('PT', 'Sender or Lab is required as Orig or Dest but there is no previous test'),
          ('FP69', 'FP69 status is set'),
          ('AREMP', 'REMOVED PATIENTS'),
          ('ZZZGP', 'ZZZ REGISTERED'),
          ('NR', 'Notification not req'))
    def test_map_district_list_fields(self, input_value, expected):
        expected_mapped = {
            'type': 'District List',
            'reason_not_produced_code': input_value,
            'reason_not_produced': expected,
            'letter_template': 'XX'
        }

        actual_mapped = _map_district_list_fields(input_value, 'XX')
        self.assertEqual(actual_mapped, expected_mapped)

    @unpack
    @data(
        ('GT', 'Invitation', "GP offers test, prior advice given"),
        ('NT', 'Invitation', "GP offers test, no prior advice"),
        ('GN', 'Invitation', "GP doesn't test, prior advice given"),
        ('NN', 'Invitation', "GP doesn't test, prior advice given"),
        ('SG', 'Invitation', "GP offers test, text"),
        ('PT', 'Reminder', "GP offers test"),
        ('PN', 'Reminder', "GP doesn't test"),
        ('ST', 'Reminder', "GP offers test, text"),
        ('SW', 'Invitation', "GP offers test, 1st recall, from Final Non-Responder"),
        ('SX', 'Reminder', "2nd recall, from Final Non-Responder"),
        ('SY', 'Reminder', "3rd recall, from Final Non-Responder"),
        ('SZ', 'Reminder', "4th recall, from Final Non-Responder")
    )
    def test_map_letter_fields(self, input_value, expected_type, expected_subtype):
        expected_mapped = {
            'type': f'{expected_type} Letter',
            'subtype': expected_subtype,
            'letter_template': 'XX'
        }

        actual_mapped = _map_letter_fields(input_value, 'XX')
        self.assertEqual(actual_mapped, expected_mapped)

    @unpack
    @data(
        ('GT', 'XX', 'First', 'GP Code: XX - GP offers test, prior advice given'),
        ('PT', 'XX', 'Second', 'GP Code: XX - GP offers test'),
        ('NT', 'XX', 'First', 'GP Code: XX - GP offers test, no prior advice'),
        ('SW', 'XX', 'First', 'GP Code: XX - from Final Non-Responder'),
        ('SX', 'XX', 'Second', 'GP Code: XX - from Final Non-Responder')
    )
    def test_map_gp_fields(self, letter_type_raw_value, subtype_raw_value, expected_type, expected_subtype):
        expected_mapped = {
            'type': f'GP Produced {expected_type} Letter',
            'subtype': expected_subtype,
        }

        actual_mapped = _map_gp_fields(letter_type_raw_value, subtype_raw_value)
        self.assertEqual(actual_mapped, expected_mapped)

    @patch('transform_and_store_CSO8_data.map_invitation_data._map_invitation_type')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_district_list_fields')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_letter_fields')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_gp_fields')
    def test_map_invitation_data_maps_invitation_type(self,
                                                      map_gp_fields_mock,
                                                      map_letter_fields_mock,
                                                      map_district_list_fields_mock,
                                                      map_invitation_type_mock):
        raw_data = 'BE|9100002976|20070720|1|RP|N2||'

        map_invitation_type_mock.return_value = 'value'

        expected = {'type': 'value'}

        mapped_data = map_invitation_data(raw_data.split('|'))

        self.assertEqual(mapped_data, expected)
        map_invitation_type_mock.assert_called_with('N2')
        map_gp_fields_mock.assert_not_called()
        map_letter_fields_mock.assert_not_called()
        map_district_list_fields_mock.assert_not_called()

    @patch('transform_and_store_CSO8_data.map_invitation_data._map_invitation_type')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_district_list_fields')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_letter_fields')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_gp_fields')
    def test_map_invitation_data_maps_district_list_fields(self,
                                                           map_gp_fields_mock,
                                                           map_letter_fields_mock,
                                                           map_district_list_fields_mock,
                                                           map_invitation_type_mock):
        raw_data = 'BD|9100001848|20090807|1|RP|DL|FP69|'

        map_district_list_fields_mock.return_value = {'key': 'value'}

        expected = {'key': 'value'}

        mapped_data = map_invitation_data(raw_data.split('|'))

        self.assertEqual(mapped_data, expected)
        map_district_list_fields_mock.assert_called_with('FP69', '')
        map_gp_fields_mock.assert_not_called()
        map_letter_fields_mock.assert_not_called()
        map_invitation_type_mock.assert_not_called()

    @patch('transform_and_store_CSO8_data.map_invitation_data._map_invitation_type')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_district_list_fields')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_letter_fields')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_gp_fields')
    def test_map_invitation_data_maps_letter_fields(self,
                                                    map_gp_fields_mock,
                                                    map_letter_fields_mock,
                                                    map_district_list_fields_mock,
                                                    map_invitation_type_mock):
        raw_data = 'BD|9100004952|19980428|1|RP|LT|GT|NAGL'

        map_letter_fields_mock.return_value = {'key': 'value'}

        expected = {'key': 'value'}

        mapped_data = map_invitation_data(raw_data.split('|'))

        self.assertEqual(mapped_data, expected)
        map_letter_fields_mock.assert_called_with('GT', 'NAGL')
        map_invitation_type_mock.assert_not_called()
        map_district_list_fields_mock.assert_not_called()
        map_gp_fields_mock.assert_not_called()

    @patch('transform_and_store_CSO8_data.map_invitation_data._map_invitation_type')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_district_list_fields')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_letter_fields')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_gp_fields')
    def test_map_invitation_data_maps_gp_fields(self,
                                                map_gp_fields_mock,
                                                map_letter_fields_mock,
                                                map_district_list_fields_mock,
                                                map_invitation_type_mock):
        raw_data = 'BE|9100000906|20061120|1|RP|GP|GT|N0020'

        map_gp_fields_mock.return_value = {'key': 'value'}

        expected = {'key': 'value'}

        mapped_data = map_invitation_data(raw_data.split('|'))

        self.assertEqual(mapped_data, expected)
        map_gp_fields_mock.assert_called_with('GT', 'N0020')
        map_invitation_type_mock.assert_not_called()
        map_district_list_fields_mock.assert_not_called()
        map_letter_fields_mock.assert_not_called()

    @patch('transform_and_store_CSO8_data.map_invitation_data.validate_invitation_type')
    @patch('transform_and_store_CSO8_data.map_invitation_data._map_invitation_type')
    @patch('transform_and_store_CSO8_data.map_invitation_data.log')
    def test_map_invitation_data_logs_error(self,
                                            log_mock,
                                            map_invitation_type_mock,
                                            validate_invitation_type_mock):
        raw_data = 'BE|9100000906|20061120|1|RP|XX|GT|N0020'

        map_invitation_type_mock.return_value = {'key': 'value'}
        validate_invitation_type_mock.return_value = {'field': 'error_message'}

        expected = {
            'type': {'key': 'value'},
            'migrated_8_data': {'validation_errors': {'field': 'error_message'}}
        }

        mapped_data = map_invitation_data(raw_data.split('|'))

        self.assertEqual(mapped_data, expected)
        log_object = {'log_reference': LogReference.CSO8MAP0007, 'validation_errors': {'field': 'error_message'}}
        log_mock.assert_called_with(log_object)

    @data('XX', '')
    def test_validate_invitation_type_invalid(self, notif_sub1_value):
        expected_error = {
            'NOTIF-SUB1': {
                'data_error': "Invitation type is not one of the expected values ['N1', 'N2', 'eN1', 'eN2', 'L1', 'L2', 'SL', 'LA', 'DL', 'LT', 'GP']" 
            }
        }

        error = validate_invitation_type(notif_sub1_value)

        self.assertEqual(error, expected_error)

    @data('N1', 'N2', 'eN1', 'eN2', 'L1', 'L2', 'SL', 'LA', 'DL', 'LT', 'GP')
    def test_validate_invitation_type_valid(self, notif_sub1_value):
        error = validate_invitation_type(notif_sub1_value)

        self.assertEqual(error, None)
