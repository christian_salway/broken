from unittest import TestCase
from mock import Mock, patch, call
from datetime import datetime, timezone

from transform_and_store_CSO8_data.log_references import LogReference
from transform_and_store_CSO8_data.exceptions import UnsupportedNotificationTypeException


class TestTransformAndStoreCSO8Data(TestCase):
    FIXED_NOW_TIME = datetime(2020, 3, 3, 9, 45, 23, 123456, tzinfo=timezone.utc)

    @patch('boto3.resource', Mock())
    def setUp(self):
        import transform_and_store_CSO8_data.transform_and_store_CSO8_data as _transform_and_store_CSO8_data
        import common.utils.audit_utils as _audit_utils
        self.transform_and_store_CSO8_data_module = _transform_and_store_CSO8_data
        self.audit_utils = _audit_utils
        self.date_time_patcher = patch.object(
            self.transform_and_store_CSO8_data_module, 'datetime', Mock(wraps=datetime))
        self.date_time_patcher.start()

    def tearDown(self):
        self.date_time_patcher.stop()

    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.log')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.get_message_contents_from_event')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.record_is_duplicate')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.map_and_validate_CSO8_data')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.create_notification_record')
    def test_CSO8_parsed_successfully(self,
                                      create_notification_record,
                                      map_and_validate_CSO8_data_mock,
                                      duplicate_record_mock,
                                      get_message_contents_from_event_mock,
                                      log_mock):
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        parsed_record = {'this': 'parsed'}

        get_message_contents_from_event_mock.return_value = record
        map_and_validate_CSO8_data_mock.return_value = parsed_record
        duplicate_record_mock.return_value = False

        self.transform_and_store_CSO8_data_module.lambda_handler.__wrapped__(event, {})

        get_message_contents_from_event_mock.assert_called_with(event)
        map_and_validate_CSO8_data_mock.assert_called_with(record['record_line'])
        duplicate_record_mock.assert_called_with(parsed_record, ['nhs_number', 'notification_date', 'type'])
        create_notification_record.assert_called_with('A|B|C|D', parsed_record)

        log_calls = [
            call({'log_reference': LogReference.CSO8MAP0001, 'message_id': 12345678}),
            call({'log_reference': LogReference.CSO8MAP0002}),
            call({'log_reference': LogReference.CSO8MAP0003}),
            call({'log_reference': LogReference.CSO8MAP0004}),
            call({'log_reference': LogReference.CSO8MAP0009}),
            call({'log_reference': LogReference.CSO8MAP0011})
        ]
        log_mock.assert_has_calls(log_calls)

    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.send_new_message')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.log')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.get_message_contents_from_event')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.get_message_body_from_event')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.map_and_validate_CSO8_data')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.create_notification_record')
    def test_message_info_logged_on_unsupported_notification_type(self,
                                                                  create_notification_record_mock,
                                                                  map_and_validate_CSO8_data_mock,
                                                                  get_message_body_from_event_mock,
                                                                  get_message_contents_from_event_mock,
                                                                  log_mock,
                                                                  send_new_message_mock):
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        self.transform_and_store_CSO8_data_module.datetime.now.return_value = self.FIXED_NOW_TIME

        get_message_contents_from_event_mock.return_value = record
        map_and_validate_CSO8_data_mock.side_effect = UnsupportedNotificationTypeException()

        self.transform_and_store_CSO8_data_module.lambda_handler.__wrapped__(event, {})

        get_message_contents_from_event_mock.assert_called_with(event)
        map_and_validate_CSO8_data_mock.assert_called_with(record['record_line'])
        send_new_message_mock.assert_not_called()
        create_notification_record_mock.assert_not_called()

        log_calls = [call({'log_reference': LogReference.CSO8MAP0001, 'message_id': 12345678}),
                     call({'log_reference': LogReference.CSO8MAP0002}),
                     call({'log_reference': LogReference.CSO8MAP0003}),
                     call({'log_reference': LogReference.CSO8MAP0006,
                           'message': 'Support for this notification type has not been implemented yet.'})]

        log_mock.assert_has_calls(log_calls)

    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.send_new_message')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.log')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.get_message_contents_from_event')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.get_message_body_from_event')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.map_and_validate_CSO8_data')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.create_notification_record')
    def test_message_moved_to_dlq_on_failed_validation(self,
                                                       create_notification_record_mock,
                                                       map_and_validate_CSO8_data_mock,
                                                       get_message_body_from_event_mock,
                                                       get_message_contents_from_event_mock,
                                                       log_mock,
                                                       send_new_message_mock):
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        self.transform_and_store_CSO8_data_module.datetime.now.return_value = self.FIXED_NOW_TIME

        get_message_contents_from_event_mock.return_value = record
        map_and_validate_CSO8_data_mock.side_effect = Exception()

        self.transform_and_store_CSO8_data_module.lambda_handler.__wrapped__(event, {})

        get_message_contents_from_event_mock.assert_called_with(event)
        map_and_validate_CSO8_data_mock.assert_called_with(record['record_line'])
        send_new_message_mock.assert_called()
        create_notification_record_mock.assert_not_called()

        log_calls = [call({'log_reference': LogReference.CSO8MAP0001, 'message_id': 12345678}),
                     call({'log_reference': LogReference.CSO8MAP0002}),
                     call({'log_reference': LogReference.CSO8MAP0003}),
                     call({'log_reference': LogReference.CSO8MAP0005, 'add_exception_info': True})]

        log_mock.assert_has_calls(log_calls)

    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.log')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.audit')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.create_replace_record_in_participant_table')
    def test_create_notification_record(self, create_replace_record_in_participant_table_mock, audit_mock, log_mock):
        self.transform_and_store_CSO8_data_module.datetime.now.return_value = self.FIXED_NOW_TIME

        raw_data = 'A|B|C|D'
        mapped_data = {'participant_id': '123'}

        expected_record = mapped_data.copy()
        expected_record['migrated_8_data'] = {
            'received': '2020-03-03T09:45:23.123456+00:00',
            'value': raw_data
        }

        self.transform_and_store_CSO8_data_module.create_notification_record(raw_data, mapped_data)

        create_replace_record_in_participant_table_mock.assert_called_with(expected_record)
        log_mock.assert_called_with({'log_reference': LogReference.CSO8MAP0008})
        audit_mock.assert_called_with(action=self.audit_utils.AuditActions.STORE_CSO8,
                                      user=self.audit_utils.AuditUsers.MIGRATION,
                                      participant_ids=['123'])

    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.log')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.audit')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.create_replace_record_in_participant_table')
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    def test_create_notification_record_success_not_logged_or_audited(
            self, create_replace_record_in_participant_table_mock, audit_mock, log_mock):
        self.transform_and_store_CSO8_data_module.datetime.now.return_value = self.FIXED_NOW_TIME

        raw_data = 'A|B|C|D'
        mapped_data = {}

        expected_record = mapped_data.copy()
        expected_record['migrated_8_data'] = {
            'received': '2020-03-03T09:45:23.123456+00:00',
            'value': raw_data
        }

        create_replace_record_in_participant_table_mock.side_effect = Exception()

        with self.assertRaises(Exception):
            self.transform_and_store_CSO8_data_module.create_notification_record(raw_data, mapped_data)

        create_replace_record_in_participant_table_mock.assert_called_with(expected_record)
        log_mock.assert_not_called()
        audit_mock.assert_not_called()

    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.log')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.get_message_contents_from_event')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.record_is_duplicate')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.map_and_validate_CSO8_data')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.create_notification_record')
    def test_lambda_exits_with_log_if_record_is_duplicate(
            self, create_notification_record, map_and_validate_CSO8_data_mock,
            duplicate_record_mock, get_message_contents_from_event_mock, log_mock):
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        parsed_record = {'this': 'parsed'}

        get_message_contents_from_event_mock.return_value = record
        map_and_validate_CSO8_data_mock.return_value = parsed_record
        duplicate_record_mock.return_value = True

        self.transform_and_store_CSO8_data_module.lambda_handler.__wrapped__(event, {})

        get_message_contents_from_event_mock.assert_called_with(event)
        map_and_validate_CSO8_data_mock.assert_called_with(record['record_line'])
        duplicate_record_mock.assert_called_with(parsed_record, ['nhs_number', 'notification_date', 'type'])
        create_notification_record.assert_not_called()

        log_calls = [
            call({'log_reference': LogReference.CSO8MAP0001, 'message_id': 12345678}),
            call({'log_reference': LogReference.CSO8MAP0002}),
            call({'log_reference': LogReference.CSO8MAP0003}),
            call({'log_reference': LogReference.CSO8MAP0004}),
            call({'log_reference': LogReference.CSO8MAP0009}),
            call({'log_reference': LogReference.CSO8MAP0010})
        ]
        log_mock.assert_has_calls(log_calls)

    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.log')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.audit')
    @patch('transform_and_store_CSO8_data.transform_and_store_CSO8_data.create_replace_record_in_participant_table', Mock())  
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'FALSE'})
    def test_create_notification_record_do_not_audit_when_feature_toggled(self, audit_mock, log_mock):
        self.transform_and_store_CSO8_data_module.datetime.utcnow.return_value = self.FIXED_NOW_TIME

        raw_data = 'A|B|C|D'
        mapped_data = {'participant_id': 'some_paticipant_id'}

        self.transform_and_store_CSO8_data_module.create_notification_record(raw_data, mapped_data)

        log_mock.assert_called_once()
        audit_mock.assert_not_called()
