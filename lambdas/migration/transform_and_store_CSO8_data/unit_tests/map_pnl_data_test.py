from unittest import TestCase
from mock import patch
from transform_and_store_CSO8_data.log_references import LogReference
from ddt import ddt, data, unpack
from transform_and_store_CSO8_data.map_pnl_data import map_pnl_data
from common.models.participant import ParticipantStatus


@ddt
class TestMapPNLData(TestCase):

    @unpack
    @data(('L', ParticipantStatus.CALLED),
          ('N', ParticipantStatus.ROUTINE),
          ('R', ParticipantStatus.REPEAT_ADVISED),
          ('I', ParticipantStatus.INADEQUATE),
          ('S', ParticipantStatus.SUSPENDED),
          ('C', ParticipantStatus.CEASED))
    def test_pnl_data_mapping_sub_code_1(self, input, expected_status):
        data_list = [
            'LS',
            '9234567890',
            '20010101',
            '1',
            'RG',
            input,
            'G',
            'Open Exeter'
        ]

        expected_mapped = {
            'type': 'PNL List - Open Exeter',
            'subtype': f'Status - {expected_status}'
        }

        returned_map = map_pnl_data(data_list)
        self.assertEqual(returned_map, expected_mapped)

    @unpack
    @data(('X', 'X'),
          ('', 'BLANK'))
    @patch('transform_and_store_CSO8_data.map_pnl_data.log')
    def test_pnl_data_mapping_sub_code_1_contains_error_for_invalid_status_code(self, input, expected_status, log_mock):
        data_list = [
            'LS',
            '9234567890',
            '20010101',
            '1',
            'RG',
            input,
            'G',
            'Open Exeter'
        ]

        expected_mapped = {
            'type': 'PNL List - Open Exeter',
            'subtype': f'Status - {expected_status}',
            'migrated_8_data': {
                'validation_errors': {
                    'NOTIF-SUB1': {
                        'data_error': "Status code is not one of the expected values ['L', 'N', 'R', 'I', 'S', 'C']"
                    }
                }
            }
        }

        returned_map = map_pnl_data(data_list)
        self.assertEqual(returned_map, expected_mapped)
        log_mock.assert_called_with({
            'log_reference': LogReference.CSO8MAP0007,
            'validation_errors': {
                'NOTIF-SUB1': {
                    'data_error': "Status code is not one of the expected values ['L', 'N', 'R', 'I', 'S', 'C']"
                }
            }
        })

    @unpack
    @data(('Paper', 'Paper'),
          ('Open Exeter', 'Open Exeter'),
          ('Paper & Open Exeter', 'Paper & Open Exeter'),
          ('', 'Not sent'))
    def test_pnl_data_mapping_sub_code_3(self, sub_code_3, expected):
        data_list = [
            'LS',
            '9234567890',
            '20010101',
            '1',
            'RG',
            'L',
            'N',
            sub_code_3
        ]

        expected_mapped = {
            'type': f'PNL List - {expected}',
            'subtype': 'Status - CALLED'
        }

        returned_map = map_pnl_data(data_list)
        self.assertEqual(returned_map, expected_mapped)
