from unittest import TestCase
from mock import patch, Mock
from datetime import datetime, timezone


class TestMapAndValidateCSO8Data(TestCase):

    FIXED_NOW_TIME = datetime(2020, 3, 3, 9, 45, 23, 123456, tzinfo=timezone.utc)

    @classmethod
    @patch('boto3.resource', Mock())
    def setUpClass(cls, *args):
        import transform_and_store_CSO8_data.map_and_validate_CSO8_data as map_and_validate_CSO8_module
        global map_and_validate_CSO8_module

    @patch('transform_and_store_CSO8_data.map_and_validate_CSO8_data.get_hashed_value', Mock(return_value='hashed'))
    @patch('transform_and_store_CSO8_data.map_and_validate_CSO8_data.datetime', Mock(wraps=datetime))
    @patch('transform_and_store_CSO8_data.map_and_validate_CSO8_data.get_participant_id')
    @patch('transform_and_store_CSO8_data.map_and_validate_CSO8_data.map_pnl_data')
    def test_mapper_returns_correct_dictionary(self, map_pnl_data_mock, get_participant_id_mock):
        map_and_validate_CSO8_module.datetime.now.return_value = self.FIXED_NOW_TIME
        raw = 'LS|9234567890|20010101|1|RG|L|G|Open Exeter'

        get_participant_id_mock.return_value = '98f1f2c2-fd40-4e75-9d36-d171479db19d'
        map_pnl_data_mock.return_value = {
            'type': 'PNL List - Open Exeter',
            'subtype': 'Status - CALLED'
        }

        expected_value = {
            'participant_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
            'sort_key': 'NOTIFICATION#2001-01-01#1#2020-03-03T09:45:23.123456+00:00',
            'nhs_number': '9234567890',
            'notification_date': '2001-01-01',
            'notification_sequence': '1',
            'created': '2020-03-03T09:45:23.123456+00:00',
            'type': 'PNL List - Open Exeter',
            'subtype': 'Status - CALLED',
            'source_hash': 'hashed'
        }

        data_list = raw.split('|')
        actual_value = map_and_validate_CSO8_module._map(data_list, raw)
        map_pnl_data_mock.assert_called_with(data_list)
        self.assertEqual(actual_value, expected_value)

    def test_exception_is_raised_for_for_too_few_fields(self):
        row = 'LS|9234567890|20010101|1|RG|L|'

        with self.assertRaises(Exception) as context:
            map_and_validate_CSO8_module.map_and_validate_CSO8_data(row)

        self.assertTrue(
            'Malformed input: Number of fields in CSO8 result must be between 8 and 13' in str(context.exception))

    def test_exception_is_raised_for_too_many_fields(self):
        row = 'LS|9234567890|20010101|1|RG|L|G|Open Exeter|a|b|c|d|e|f|g'

        with self.assertRaises(Exception) as context:
            map_and_validate_CSO8_module.map_and_validate_CSO8_data(row)

        self.assertTrue(
            'Malformed input: Number of fields in CSO8 result must be between 8 and 13' in str(context.exception))

    @patch('transform_and_store_CSO8_data.map_and_validate_CSO8_data.datetime', Mock(wraps=datetime))
    @patch('transform_and_store_CSO8_data.map_and_validate_CSO8_data._map')
    def test_map_and_validate_CSO8_data_returns_mapped_data(self, _map_mock):
        map_and_validate_CSO8_module.datetime.now.return_value = self.FIXED_NOW_TIME

        raw = 'LS|9234567890|20010101|1|RG|L|G|Open Exeter'

        expected_value = {
            'key1': 'value1',
            'key2': 'value2'
        }

        _map_mock.return_value = expected_value
        actual_value = map_and_validate_CSO8_module.map_and_validate_CSO8_data(raw)

        self.assertEqual(expected_value, actual_value)
