from transform_and_store_CSO8_data.cso8_fields import CSO8_FIELDS
from common.log import log
from transform_and_store_CSO8_data.log_references import LogReference
from common.models.participant import ParticipantStatus

VALID_STATUS_TYPES = {
    'L': ParticipantStatus.CALLED,
    'N': ParticipantStatus.ROUTINE,
    'R': ParticipantStatus.REPEAT_ADVISED,
    'I': ParticipantStatus.INADEQUATE,
    'S': ParticipantStatus.SUSPENDED,
    'C': ParticipantStatus.CEASED,
}

STATUS_VALIDATION_ERROR = {
    'NOTIF-SUB1': {
        'data_error': f'Status code is not one of the expected values {list(VALID_STATUS_TYPES.keys())}'
    }
}


def map_pnl_data(data_list):

    pnl_type = data_list[CSO8_FIELDS.index('NOTIF-SUB3')]
    if not pnl_type:
        pnl_type = 'Not sent'

    mapped_data = {}

    status, error = validate_status_code(data_list)

    if error:
        log({'log_reference': LogReference.CSO8MAP0007, 'validation_errors': error})
        mapped_data['migrated_8_data'] = {
            'validation_errors': error
        }

    mapped_data.update(
        {
            'type': f'PNL List - {pnl_type}',
            'subtype': f'Status - {status}'
        }
    )

    return mapped_data


def validate_status_code(data_list):

    status_code = data_list[CSO8_FIELDS.index('NOTIF-SUB1')]

    if not status_code:
        return 'BLANK', STATUS_VALIDATION_ERROR

    if status_code not in VALID_STATUS_TYPES:
        return status_code, STATUS_VALIDATION_ERROR

    return VALID_STATUS_TYPES.get(status_code), None
