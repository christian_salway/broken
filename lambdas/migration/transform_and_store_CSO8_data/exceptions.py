
class UnsupportedNotificationTypeException(Exception):
    message = 'Support for this notification type has not been implemented yet.'

    def __init__(self):
        super().__init__(self.message)
