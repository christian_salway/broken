from unittest import TestCase
from unittest.mock import patch, call, Mock

from common.utils.audit_utils import AuditActions, AuditUsers

with patch('boto3.resource') as resource_mock:
    from transform_and_store_CSO1R_data.transform_and_store_CSO1R_data import lambda_handler
    from transform_and_store_CSO1R_data.log_references import LogReference


@patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.log')
@patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.get_message_contents_from_event')
class TestTransformAndStore1RData(TestCase):

    def test_exception_thrown_when_no_record_line_found(self,
                                                        get_message_contents_from_event_mock,
                                                        log_mock):
        # Given
        get_message_contents_from_event_mock.return_value = {'message_id': '12345678', 'record_line': ''}
        event = {'example': 'event'}
        # When
        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(event, {})
        # Then
        get_message_contents_from_event_mock.assert_called_with(event)
        log_mock.assert_called()
        self.assertTrue('Unable to map blank line' in str(context.exception))

    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.create_previous_name_record')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.audit')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.update_participant_by_nhs_number')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.map_and_validate_CSO1R_data')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.log_nhs_number_info', Mock())
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.log_name_info')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.log_address_info')
    def test_update_participant_is_called_correctly(self,
                                                    log_address_info_mock,
                                                    log_name_info_mock,
                                                    mapper_mock,
                                                    update_participant_by_nhs_number_mock,
                                                    audit_mock,
                                                    create_previous_name_record_mock,
                                                    get_message_contents_from_event_mock,
                                                    log_mock):
        # Given
        nhs_number = 'the_nhs_number'
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        mapped_record = {'this': 'mapped'}
        not_stored_data = {'gender_character': 'F', 'LDN': 'L1', 'date_of_death_state': 'INCOMPLETE'}
        updated_participant = {'participant_id': 'the_participant'}
        get_message_contents_from_event_mock.return_value = record
        mapper_mock.return_value = nhs_number, mapped_record, not_stored_data, 'Smith'
        update_participant_by_nhs_number_mock.return_value = updated_participant
        # When
        lambda_handler.__wrapped__(event, {})
        # Then
        get_message_contents_from_event_mock.assert_called_with(event)
        mapper_mock.assert_called_with(record['record_line'])
        update_participant_by_nhs_number_mock.assert_called_with(nhs_number, fields_to_add_or_replace=mapped_record)
        create_previous_name_record_mock.assert_called_with({'participant_id': 'the_participant', 'last_name': 'Smith'})
        log_calls = [
            call({'log_reference': LogReference.TRANS1R0001, 'message_id': 12345678}),
            call({'log_reference': LogReference.TRANS1R0002}),
            call({'log_reference': LogReference.TRANS1R0003}),
            call({'log_reference': LogReference.TRANS1R0004}),
            call({'log_reference': LogReference.TRANS1R0005}),
            call({'log_reference': LogReference.TRANS1R0017}),
            call({'log_reference': LogReference.TRANS1R0006}),
            call({'log_reference': LogReference.TRANS1R0009, 'gender_character': 'F'}),
            call({'log_reference': LogReference.TRANS1R0014, 'LDN': 'L1'}),
            call({'log_reference': LogReference.TRANS1R0016, 'state': 'INCOMPLETE'})
        ]
        log_mock.assert_has_calls(log_calls)
        log_name_info_mock.assert_called()
        log_address_info_mock.assert_called()
        audit_mock.assert_called_with(action=AuditActions.STORE_CSO1R,
                                      user=AuditUsers.MIGRATION,
                                      participant_ids=['the_participant'])

    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.audit')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.update_participant_by_nhs_number')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.log_nhs_number_info', Mock())
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.log_name_info')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.log_address_info')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.map_and_validate_CSO1R_data')
    def test_audit_is_not_called_given_update_participant_is_not_successful(
            self,
            mapper_mock,
            log_address_info_mock,
            log_name_info_mock,
            update_participant_by_nhs_number_mock,
            audit_mock,
            get_message_contents_from_event_mock,
            log_mock):
        # Given
        nhs_number = 'the_nhs_number'
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        mapped_record = {'this': 'mapped'}
        gender_character = 'F'
        updated_participant = None
        get_message_contents_from_event_mock.return_value = record
        mapper_mock.return_value = nhs_number, mapped_record, gender_character, None
        update_participant_by_nhs_number_mock.return_value = updated_participant
        # When
        lambda_handler.__wrapped__(event, {})
        # Then
        get_message_contents_from_event_mock.assert_called_with(event)
        mapper_mock.assert_called_with(record['record_line'])
        update_participant_by_nhs_number_mock.assert_called_with(nhs_number, fields_to_add_or_replace=mapped_record)
        log_calls = [
            call({'log_reference': LogReference.TRANS1R0001, 'message_id': 12345678}),
            call({'log_reference': LogReference.TRANS1R0002}),
            call({'log_reference': LogReference.TRANS1R0003}),
            call({'log_reference': LogReference.TRANS1R0004}),
            call({'log_reference': LogReference.TRANS1R0005}),
        ]
        log_mock.assert_has_calls(log_calls)
        log_address_info_mock.assert_not_called()
        log_name_info_mock.assert_not_called()
        audit_mock.assert_not_called()

    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.audit')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.map_and_validate_CSO1R_data', Mock(return_value=('', '', {'gender_character': '', 'LDN': ''}, None))) 
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.log_nhs_number_info', Mock())
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.log_name_info')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.log_address_info')
    @patch('transform_and_store_CSO1R_data.transform_and_store_CSO1R_data.update_participant_by_nhs_number', Mock())
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'FALSE'})
    def test_no_audit_when_migration_audit_is_toggled_off(self,
                                                          log_address_info_mock,
                                                          log_name_info,
                                                          audit_mock,
                                                          get_message_contents_from_event_mock,
                                                          log_mock):
        # Given
        get_message_contents_from_event_mock.return_value = {'message_id': '12345678', 'record_line': 'something'}
        event = {'example': 'event'}

        # When
        lambda_handler.__wrapped__(event, {})

        # Then
        get_message_contents_from_event_mock.assert_called_with(event)
        log_mock.assert_called()
        log_name_info.assert_called()
        log_address_info_mock.assert_called()
        audit_mock.assert_not_called()
