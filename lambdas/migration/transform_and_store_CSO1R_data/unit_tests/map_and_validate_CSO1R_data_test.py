import os
import json
from unittest import TestCase
from unittest.mock import patch, Mock
from datetime import datetime, timezone
from common.utils import (
    increment_date_by_years, generate_random_length_string, load_json_doc)
from transform_and_store_CSO1R_data.log_references import LogReference
with patch('boto3.resource'):
    import transform_and_store_CSO1R_data.map_and_validate_CSO1R_data as mapper


@patch('transform_and_store_CSO1R_data.map_and_validate_CSO1R_data.log')
@patch('transform_and_store_CSO1R_data.map_and_validate_CSO1R_data.datetime')
class TestMap1RData(TestCase):

    FIXED_NOW_TIME = datetime(2020, 3, 3, 9, 45, 23, 123456, tzinfo=timezone.utc)
    maxDiff = None

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_cso1r_map_and_validate_returns_expected_participant_result(self,
                                                                        now_mock,
                                                                        log_mock):

        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        now_mock.return_value = datetime(2020, 3, 3)

        test_data = ('CU|9991000000|19701025|M|20090727|L1|S564|G85132|O/R|19881025|BIR|'
                     'MISS|WERNLI|JASMYNE|MASTERS|78 OAKTREE DRIVE|TOWNSVILLE|'
                     'WESTBERG|MANCHESTER|GREATER MANCHESTER|EC4A 1AD|SMITH|20101025')
        expected_nhs_number = '9991000000'
        expected_data = {
            'registered_gp_practice_code': 'G85132',
            'sanitised_first_name': 'JASMYNE',
            'sanitised_last_name': 'WERNLI',
            'sanitised_postcode': 'EC4A1AD',
            'title': 'MISS',
            'first_name': 'JASMYNE',
            'middle_names': 'MASTERS',
            'last_name': 'WERNLI',
            'active': True,
            'address': {
                'address_line_1': '78 OAKTREE DRIVE',
                'address_line_2': 'TOWNSVILLE',
                'address_line_3': 'WESTBERG',
                'address_line_4': 'MANCHESTER',
                'address_line_5': 'GREATER MANCHESTER',
                'postcode': 'EC4A 1AD'
            },
            'date_of_birth': '1970-10-25',
            'date_of_birth_and_initial': '1970-10-25W',
            'date_of_death': '2010-10-25',
            'date_of_birth_and_initial_and_postcode': '1970-10-25WEC4A1AD',
            'gender': '1',
            'migrated_1R_data': {
                'value': test_data,
                'received': '2020-03-03T09:45:23.123456+00:00'
            },
            'nhais_cipher': 'CU'
        }
        expected_not_stored = {
            'gender_character': 'M',
            'LDN': 'L1',
            'date_of_death_state': 'COMPLETE'
        }

        actual_nhs_number, actual_data, actual_not_stored, \
            actual_prev_surname = mapper.map_and_validate_CSO1R_data(test_data)

        self.assertEqual(expected_nhs_number, actual_nhs_number)
        self.assertDictEqual(expected_data, actual_data)
        self.assertEqual(expected_not_stored, actual_not_stored)
        self.assertEqual('SMITH', actual_prev_surname)
        log_mock.assert_called_with({'log_reference': LogReference.TRANS1R0011})

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_map_CSO1R_data_returns_schema_validation_errors_when_migration_data_is_invalid(self,
                                                                                            now_mock,
                                                                                            log_mock):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        path = os.path.join(os.path.dirname(__file__), 'test_data')
        expected_error_dict = load_json_doc(path, 'validation_warnings_1.json').get('validation_errors')
        now_mock.return_value = datetime(2020, 3, 3)
        long_address = generate_random_length_string(31)
        nhs_number = '999100000012123'
        test_data = (f'BD|{nhs_number}|10111946|MALE|1990|L11|1106000|L83665X|AAAA|01011990|BIRM|Acorn|'
                     'ThisSurnameIsTooLarge|ThisForenameIsTooLarge|ThisOtherForenameIsTooLarge|'
                     f'{long_address}|{long_address}|{long_address}|{long_address}|{long_address}|INCORRECT POSTCODE||')
        expected_not_stored = {'gender_character': 'MALE', 'LDN': 'L11'}

        actual_nhs_number, actual_data, actual_not_stored, \
            actual_prev_surname = mapper.map_and_validate_CSO1R_data(test_data)

        returned_validation_errors = actual_data['migrated_1R_data']['validation_errors']

        self.assertEqual(nhs_number, actual_nhs_number)
        self.assertDictEqual(expected_error_dict, returned_validation_errors)
        self.assertEqual(expected_not_stored, actual_not_stored)
        self.assertEqual(None, actual_prev_surname)

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_map_CSO1R_data_returns_data_validation_errors_when_migration_data_is_invalid(self,
                                                                                          now_mock,
                                                                                          log_mock):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        path = os.path.join(os.path.dirname(__file__), 'test_data')
        validation_warnings = load_json_doc(path, 'validation_warnings_2.json').get('validation_errors')
        expected_error_dict = increment_date_by_years(json.dumps(validation_warnings))
        now_mock.return_value = datetime(2020, 3, 3)
        nhs_number = '0123456789'
        test_data = (f'BD|{nhs_number}|18791201|A|39990429|A1|GP&^%|GP!"£$||19870101|$%^|Acorn|Little|Janice|'
                     '||33 Emmetts Park|Bere Alston|EXETER|Devon|EX2 5SE|||')
        expected_not_stored = {'gender_character': 'A', 'LDN': 'A1'}

        actual_nhs_number, actual_data, actual_not_stored, \
            actual_prev_surname = mapper.map_and_validate_CSO1R_data(test_data)

        returned_validation_errors = actual_data['migrated_1R_data']['validation_errors']

        self.assertEqual(nhs_number, actual_nhs_number)
        self.assertDictEqual(returned_validation_errors, expected_error_dict)
        self.assertEqual(expected_not_stored, actual_not_stored)
        self.assertEqual(None, actual_prev_surname)

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_map_CSO1R_data_throws_exception_when_required_fields_are_missing(self,
                                                                              now_mock,
                                                                              log_mock):
        invalid_test_data = ('||||||||||||||||||||||')
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        now_mock.return_value = datetime(2020, 3, 3)

        with self.assertRaises(Exception) as context:
            mapper.map_and_validate_CSO1R_data(invalid_test_data)

        self.assertEqual('NHS Number required for ingestion', context.exception.args[0])

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_log_nhs_number_info(self, now_mock, log_mock):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        now_mock.return_value = datetime(2020, 3, 3)
        mapper.log_nhs_number_info('9999999?')
        log_mock.assert_called_with({'log_reference': LogReference.TRANS1R0010, 'type': 'contains ?'})

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_log_nhs_number_info_len_not_10(self, now_mock, log_mock):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        now_mock.return_value = datetime(2020, 3, 3)
        mapper.log_nhs_number_info('999999')
        log_mock.assert_called_with({'log_reference': LogReference.TRANS1R0010, 'type': 'does not contain ? len != 10'})

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_log_nhs_number_info_invalid(self, now_mock, log_mock):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        now_mock.return_value = datetime(2020, 3, 3)
        mapper.log_nhs_number_info('0123456782')
        log_mock.assert_called_with({'log_reference': LogReference.TRANS1R0010, 'type': 'checksum invalid'})

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_log_nhs_number_info_invalid_char(self, now_mock, log_mock):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        now_mock.return_value = datetime(2020, 3, 3)
        mapper.log_nhs_number_info('O123/56782')
        log_mock.assert_called_with({'log_reference': LogReference.TRANS1R0010, 'type': 'checksum invalid'})

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_log_nhs_number_info_valid(self, now_mock, log_mock):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        now_mock.return_value = datetime(2020, 3, 3)
        mapper.log_nhs_number_info('2983396339')
        log_mock.assert_called_with({'log_reference': LogReference.TRANS1R0010, 'type': 'checksum valid'})

    def test_log_name_info(self, datetime_mock, log_mock):
        participant = {'first_name': 'John', 'last_name': 'Lanche'}
        mapper.log_name_info(participant)
        log_mock.assert_called_with({'log_reference': LogReference.TRANS1R0012,
                                     'first_forename': True, 'surname': True})

        participant = {}
        mapper.log_name_info(participant)
        log_mock.assert_called_with({'log_reference': LogReference.TRANS1R0012,
                                    'first_forename': False, 'surname': False})

        participant = {'first_name': '', 'last_name': ''}
        mapper.log_name_info(participant)
        log_mock.assert_called_with({'log_reference': LogReference.TRANS1R0012,
                                     'first_forename': False, 'surname': False})

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_map_CSO1R_data_logs_missing_date_of_birth(self,
                                                       now_mock,
                                                       log_mock):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        now_mock.return_value = datetime(2020, 3, 3)
        test_data = ('BD|0123456789||A|39990429|A1|GP&^%|GP!"£$||19870101|$%^|Acorn|Little|Janice|'
                     '||33 Emmetts Park|Bere Alston|EXETER|Devon|EX2 5SE||')

        mapper.map_and_validate_CSO1R_data(test_data)

        log_mock.assert_called_with({'log_reference': LogReference.TRANS1R0013})

    @patch.object(mapper, 'datetime', Mock(wraps=datetime))
    def test_map_CSO1R_data_logs_missing_invalid_cipher_and_defaults_cipher(self,
                                                                            now_mock,
                                                                            log_mock):
        mapper.datetime.now.return_value = self.FIXED_NOW_TIME
        mapper.datetime.strptime.side_effect = datetime.strptime
        now_mock.return_value = datetime(2020, 3, 3)
        test_data = ('AA|0123456789|19701025|A|39990429|A1|GP&^%|GP!"£$||19870101|$%^|Acorn|Little|Janice|'
                     '||33 Emmetts Park|Bere Alston|EXETER|Devon|EX2 5SE||')
        expected_cipher = 'ENG'
        _, actual_data, _, \
            _ = mapper.map_and_validate_CSO1R_data(test_data)

        self.assertEqual(expected_cipher, actual_data.get('nhais_cipher', ''))
        log_mock.has_calls({'log_reference': LogReference.TRANS1R0018})

    def test_log_ldn_info(self, datetime_mock, log_mock):
        data = [None, None, None, None, None, 'L1']
        mapper.log_ldn_info(data)
        log_mock.assert_called_with(
            {
                'log_reference': LogReference.TRANS1R0014,
                'LDN': 'L1'
            }
        )

        data = []
        log_mock.reset_mock()
        mapper.log_ldn_info(data)
        log_mock.assert_called_with(
            {
                'log_reference': LogReference.TRANS1R0014,
                'LDN': 'NOVALUE'
            }
        )

    def test_log_address_info_with_no_house_number_or_postcode(self, datetime_mock, log_mock):
        participant = {
            'address': {
                'address_line_1': '',
                'address_line_2': '',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': ''
            }
        }

        mapper.log_address_info(participant)

        log_mock.assert_called_with({
            'log_reference': LogReference.TRANS1R0015, 'house_number': False, 'postcode': False
        })

    def test_log_address_info_with_regular_house_no_and_postcode(self, datetime_mock, log_mock):
        participant = {
            'address': {
                'address_line_1': '14 Crimson Avenue',
                'address_line_2': '',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': 'LS14 4YY'
            }
        }

        mapper.log_address_info(participant)

        log_mock.assert_called_with({
            'log_reference': LogReference.TRANS1R0015, 'house_number': True, 'postcode': True
        })

    def test_log_address_info_with_house_number_not_in_first_line_of_address(self, datetime_mock, log_mock):
        participant = {
            'address': {
                'address_line_1': 'The Laurels',
                'address_line_2': '14 Crimson Ave',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': 'LS14 4YY'
            }
        }

        mapper.log_address_info(participant)

        log_mock.assert_called_with({
            'log_reference': LogReference.TRANS1R0015, 'house_number': True, 'postcode': True
        })

    def test_log_address_info_with_no_number_but_has_first_line_of_address(self, datetime_mock, log_mock):
        participant = {
            'address': {
                'address_line_1': 'The Laurels',
                'address_line_2': '',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': 'LS14 4YY'
            }
        }

        mapper.log_address_info(participant)

        log_mock.assert_called_with({
            'log_reference': LogReference.TRANS1R0015, 'house_number': True, 'postcode': True
        })

    def test_date_of_death_state(self, datetime_mock, log_mock):
        self.assertEqual('INCOMPLETE', mapper._calculate_date_of_death_state('202012', ''))
        self.assertEqual('NOT_CONVERTIBLE', mapper._calculate_date_of_death_state('20201231', str('20201231')))
        self.assertEqual('COMPLETE', mapper._calculate_date_of_death_state('20201231', '2020-12-31'))
