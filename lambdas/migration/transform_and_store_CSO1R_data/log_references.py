import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    TRANS1R0001 = (logging.INFO, 'Received CSO1R record from sqs')
    TRANS1R0002 = (logging.INFO, 'Participant updated with migrated 1R record')
    TRANS1R0003 = (logging.INFO, 'Attempting to map CSO1R record to participant format')
    TRANS1R0004 = (logging.INFO, 'Successfully mapped CSO1R record to participant format')
    TRANS1R0005 = (logging.INFO, 'Attempting to update existing participant with CSO1R record')
    TRANS1R0006 = (logging.INFO, 'Successfully updated existing participant with CSO1R record')
    TRANS1R0007 = (logging.WARNING, 'CSO1R record has failed validation')
    TRANS1R0008 = (logging.ERROR, 'CSO1R record has failed mandatory validation, record is rejected')
    TRANS1R0009 = (logging.INFO, 'Logging participant gender for CSO1R record')
    TRANS1R0010 = (logging.INFO, 'Logging participant NHS number type')
    TRANS1R0011 = (logging.INFO, 'Date of birth parsed successfully')
    TRANS1R0012 = (logging.INFO, 'Logging if participant name is present')
    TRANS1R0013 = (logging.INFO, 'Date of birth is missing')
    TRANS1R0014 = (logging.INFO, 'Logging LDN status')
    TRANS1R0015 = (logging.INFO, 'Logging if Address is present')
    TRANS1R0016 = (logging.INFO, 'Date of death state')
    TRANS1R0017 = (logging.INFO, 'Name has changed. Creating previous name record')
    TRANS1R0018 = (logging.INFO, 'Missing or Invalid source cipher. Defaulting to ENG')
