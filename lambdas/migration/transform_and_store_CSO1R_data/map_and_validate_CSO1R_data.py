from common.utils.participant_utils import nhs_checksum_valid, set_search_indexes
import os
from datetime import datetime, timezone
import re
from common.utils.transform_and_store_utils import (
    remove_empty_values, missing_necessary_fields)
from common.log import log
from transform_and_store_CSO1R_data.log_references import LogReference
from common.utils.import_migration_data_validation_utils import (
    ImportMigrationDataValidation as data)
from common.utils.import_migration_schema_validation_utils import (
    ImportMigrationSchemaValidation as schema)


GENDER_OPTIONS = {
    'M': '1',
    'F': '2',
    'I': '0'
}
ACTIVE_OPTIONS = {

}
UNKNOWN_GENDER = '0'

CSO1R_VALIDATION_DESIGN = {
    'SOURCE_CIPHER': {
        'schema': schema().validate_source_cipher,
        'data': data().validate_source_cipher,
    },
    'NHSNUM': {
        'schema': schema().validate_nhs_number
    },
    'DOB': {
        'schema': schema().validate_date,
        'data': data().validate_optional_date,
    },
    'GENDER': {
        'schema': schema().validate_gender,
        'data': data().validate_gender,
    },
    'OAPD': {
        'schema': schema().validate_date,
        'isRequired': True,
        'data': data().validate_oapd,
    },
    'LDN': {
        'schema': schema().validate_ldn,
        'data': data().validate_ldn,
    },
    'GP': {
        'schema': schema().validate_gp_code,
        'data': data().validate_gp_or_practice_code
    },
    'PRAC': {
        'schema': schema().validate_gp_practice_code,
        'data': data().validate_gp_or_practice_code,
    },
    'RSNFORRMVL': {
        'schema': schema().validate_reason_for_removal
    },
    'REMDATE': {
        'schema': schema().validate_date,
        'data': data().validate_optional_date
    },
    'DESTINATION': {
        'schema': schema().validate_destination,
        'data': data().validate_destination
    },
    'TITLE': {
        'schema': schema().validate_title,
        'data': data().validate_title
    },
    'SURNAME': {
        'schema': schema().validate_surname
    },
    'FIRST_FORENAME': {
        'schema': schema().validate_first_forename
    },
    'OTHER_FORENAMES': {
        'schema': schema().validate_other_forenames
    },
    'ADDRESS1': {
        'schema': schema().validate_address,
        'schema_args': 'Address 1'
    },
    'ADDRESS2': {
        'schema': schema().validate_address,
        'schema_args': 'Address 2'
    },
    'LOCALITY': {
        'schema': schema().validate_address,
        'schema_args': 'Locality'
    },
    'TOWN': {
        'schema': schema().validate_address,
        'schema_args': 'Town'
    },
    'COUNTY': {
        'schema': schema().validate_county
    },
    'POSTCODE': {
        'schema': schema().validate_postcode
    },
    'PREV_SUR': {
        'schema': schema().validate_surname
    },
    'DEATHDATE': {
        'schema': schema().validate_date,
        'data': data().validate_optional_date
    }
}
LAMBDA_DLQ_URL = os.environ.get('LAMBDA_DLQ_URL')


def map_and_validate_CSO1R_data(raw_data):
    data_list = raw_data.split('|')
    validation_response = _validate(data_list)
    if validation_response:
        log({'log_reference': LogReference.TRANS1R0007, 'validation_errors': validation_response})
        if missing_necessary_fields(validation_response, ['NHSNUM']):
            log({'log_reference': LogReference.TRANS1R0008})
            raise Exception('NHS Number required for ingestion')
        if validation_response.get('SOURCE_CIPHER'):
            log({'log_reference': LogReference.TRANS1R0018})
            data_list[0] = 'ENG'
    return _map(raw_data, data_list, validation_response)


def _validate(data_list):

    errors = {}
    for index, piece in enumerate(CSO1R_VALIDATION_DESIGN):
        response = {piece: {}}

        schema_function = CSO1R_VALIDATION_DESIGN[piece]['schema']
        if 'schema_args' in CSO1R_VALIDATION_DESIGN[piece]:
            args = CSO1R_VALIDATION_DESIGN[piece]['schema_args']
            schema_response = schema_function(data_list[index], args)
            if schema_response:
                response[piece].update(schema_response)
        elif 'isRequired' in CSO1R_VALIDATION_DESIGN[piece]:
            schema_response = schema_function(data_list[index], is_required=True)
            if schema_response:
                response[piece].update(schema_response)
        else:
            schema_response = schema_function(data_list[index])
            if schema_response:
                response[piece].update(schema_response)
        if 'data' in CSO1R_VALIDATION_DESIGN[piece]:
            data_function = CSO1R_VALIDATION_DESIGN[piece]['data']
            data_response = _validate_data_if_possible(schema_response, data_function, data_list[index])
            if data_response:
                response[piece].update(data_response)

        if response[piece]:
            errors.update(response)

    return errors


def _validate_data_if_possible(schema_error, data_validation_function, data_piece):
    if not schema_error:
        return data_validation_function(data_piece)
    elif 'schema_error' in schema_error:
        if 'is a required value' not in schema_error['schema_error']:
            return data_validation_function(data_piece)


def _map(raw_data, data_list, validation_errors):
    nhs_number = data_list[1]
    gender_character = data_list[3]
    ldn_index = list(CSO1R_VALIDATION_DESIGN.keys()).index('LDN')
    ldn = data_list[ldn_index] or 'NODATA'
    date_of_birth = ''
    if data_list[2]:
        date_of_birth = _try_to_format_date(data_list[2])
    else:
        log({'log_reference': LogReference.TRANS1R0013})

    date_of_death = ''
    date_of_death_raw = ''
    if data_list[22]:
        date_of_death_raw = data_list[22]
        date_of_death = _try_to_format_date(date_of_death_raw)

    mapped_data = {
        'registered_gp_practice_code': data_list[7],
        'title': data_list[11],
        'first_name': data_list[13],
        'middle_names': data_list[14],
        'last_name': data_list[12],
        'nhais_cipher': data_list[0],
        'address': {
            'address_line_1': data_list[15],
            'address_line_2': data_list[16],
            'address_line_3': data_list[17],
            'address_line_4': data_list[18],
            'address_line_5': data_list[19],
            'postcode': data_list[20],
        },
        'date_of_birth': date_of_birth,
        'date_of_death': date_of_death,
        'gender': convert_gender_to_numeric_format(gender_character),
        'active': is_active(data_list[5]),
        'migrated_1R_data': {
            'value': raw_data,
            'received': datetime.now(timezone.utc).isoformat(),
            'validation_errors': validation_errors
        },
    }

    set_search_indexes(mapped_data)

    not_stored_data = {
        'gender_character': gender_character,
        'LDN': ldn,
    }

    if date_of_death_raw:
        not_stored_data['date_of_death_state'] = _calculate_date_of_death_state(
            date_of_death_raw,
            date_of_death
        )

    previous_surname = data_list[21] if data_list[21] else None

    return nhs_number, remove_empty_values(mapped_data), not_stored_data, previous_surname


def is_active(ldn):
    switcher = {
        'L1': True,
        'L2': True,
        'L3': False,
        'L4': False,
        'D1': False,
        'D2': False
    }
    return switcher.get(ldn, True)


def convert_gender_to_numeric_format(gender_character):
    if gender_character in GENDER_OPTIONS:
        return GENDER_OPTIONS[gender_character]
    return UNKNOWN_GENDER


def _try_to_format_date(potential_date_value):
    try:
        date_of_birth = datetime.strptime(potential_date_value, '%Y%m%d').date().isoformat()
        log({'log_reference': LogReference.TRANS1R0011})
        return date_of_birth
    except Exception:
        return str(potential_date_value)


def log_nhs_number_info(nhs_number):
    if '?' in nhs_number:
        log({'log_reference': LogReference.TRANS1R0010, 'type': 'contains ?'})
    elif len(nhs_number) != 10:
        log({'log_reference': LogReference.TRANS1R0010, 'type': 'does not contain ? len != 10'})
    elif nhs_checksum_valid(nhs_number):
        log({'log_reference': LogReference.TRANS1R0010, 'type': 'checksum valid'})
    else:
        log({'log_reference': LogReference.TRANS1R0010, 'type': 'checksum invalid'})


def log_name_info(participant):
    first_forename = participant.get('first_name', '')
    surname = participant.get('last_name', '')
    log({'log_reference': LogReference.TRANS1R0012, 'first_forename': first_forename != '', 'surname': surname != ''})


def log_ldn_info(data_list):
    ldn_index = list(CSO1R_VALIDATION_DESIGN.keys()).index('LDN')
    if len(data_list) > ldn_index:
        log({'log_reference': LogReference.TRANS1R0014, 'LDN': data_list[ldn_index]})
    else:
        log({'log_reference': LogReference.TRANS1R0014, 'LDN': 'NOVALUE'})


def log_address_info(participant):
    address = participant.get('address', {})
    addresses_concatenated = address.get('address_line_1', '') + \
        address.get('address_line_2', '') + address.get('address_line_3', '') + \
        address.get('address_line_4', '') + address.get('address_line_5', '')
    pattern = re.compile(r'[^0-9]')
    numbers_in_address = pattern.sub('', addresses_concatenated)

    log({
        'log_reference': LogReference.TRANS1R0015,
        'house_number':  numbers_in_address != '' or address.get('address_line_1', '') != '',
        'postcode': address.get('postcode', '') != ''
    })


def _calculate_date_of_death_state(date_of_death_raw, date_of_death):
    if len(date_of_death_raw) < 8:
        return 'INCOMPLETE'

    if date_of_death_raw == date_of_death:
        return 'NOT_CONVERTIBLE'

    return 'COMPLETE'
