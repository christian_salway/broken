import os
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.transform_and_store_utils import (
    get_message_contents_from_event, update_participant_by_nhs_number
)
from common.log import log
from common.utils.audit_utils import AuditActions, AuditUsers, audit
from transform_and_store_CSO1R_data.log_references import LogReference
from transform_and_store_CSO1R_data.map_and_validate_CSO1R_data import (
    map_and_validate_CSO1R_data, log_nhs_number_info, log_name_info, log_address_info
)
from common.utils.participant_utils import create_previous_name_record


@lambda_entry_point
def lambda_handler(event, context):
    message_contents = get_message_contents_from_event(event)

    log({'log_reference': LogReference.TRANS1R0001, 'message_id': message_contents['message_id']})

    record_line = message_contents['record_line']

    log({'log_reference': LogReference.TRANS1R0002})

    if not record_line:
        raise Exception('Unable to map blank line')

    log({'log_reference': LogReference.TRANS1R0003})

    nhs_number, data_to_store, not_stored_data, previous_surname = map_and_validate_CSO1R_data(record_line)

    log({'log_reference': LogReference.TRANS1R0004})
    log({'log_reference': LogReference.TRANS1R0005})

    participant = update_participant_by_nhs_number(nhs_number, fields_to_add_or_replace=data_to_store)

    if participant:
        if previous_surname:
            log({'log_reference': LogReference.TRANS1R0017})
            participant_with_previous_surname = dict(participant)
            participant_with_previous_surname['last_name'] = previous_surname
            create_previous_name_record(participant_with_previous_surname)

        log({'log_reference': LogReference.TRANS1R0006})
        log({'log_reference': LogReference.TRANS1R0009, 'gender_character': not_stored_data['gender_character']})
        log({'log_reference': LogReference.TRANS1R0014, 'LDN': not_stored_data['LDN']})

        log_nhs_number_info(nhs_number)
        log_name_info(participant)
        log_address_info(participant)

        if 'date_of_death_state' in not_stored_data:
            log({'log_reference': LogReference.TRANS1R0016, 'state': not_stored_data['date_of_death_state']})

        if os.environ.get('ENABLE_MIGRATION_AUDIT') != 'FALSE':
            audit(action=AuditActions.STORE_CSO1R,
                  user=AuditUsers.MIGRATION,
                  participant_ids=[participant['participant_id']])
