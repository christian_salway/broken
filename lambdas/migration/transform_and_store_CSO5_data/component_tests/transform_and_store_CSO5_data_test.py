import json
from unittest import TestCase
from unittest.mock import patch, Mock
from ddt import ddt, data, unpack
from collections import OrderedDict

mock_env_vars = {
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'DYNAMODB_REFERENCE': 'DYNAMODB_REFERENCE',
    'LAMBDA_DLQ_URL': 'LAMBDA_DLQ_URL'
}

with patch('os.environ', mock_env_vars):
    from common.test_mocks.mock_events import sqs_mock_event
    from transform_and_store_CSO5_data import transform_and_store_CSO5_data

reference_table_mock = Mock()
sqs_client_mock = Mock()


valid_row = OrderedDict([
    ('SOURCE_CIPHER', 'STL'),
    ('REFERENCE_TYPE', 'S'),
    ('REF_ID', 'JCOL'),
    ('REF_ID', 'UGRAUBURGH HEALTH AUTHORITY'),
    ('POSTCODE', ''),
    ('REASON', '01'),
    ('DHA', '20040101'),
    ('NATIONAL', ''),
    ('ADDRESS1', ''),
    ('ADDRESS2', ''),
    ('ADDRESS3', ''),
    ('ADDRESS4', ''),
    ('ADDRESS5', ''),
    ('TELNO', ''),
    ('PARENT_CODE', ''),
    ('ACTIVE', ''),
    ('INACTIVE_DATE', ''),
    ('RESPONSIBLE', ''),
    ('EMAIL1', ''),
    ('EMAIL2', ''),
    ('REF_SUBTYPE', '7'),
    ('SMARTCARD_ID', ''),
])


@ddt
@patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.reference_utils.dynamodb_put_item', reference_table_mock)
class TestFieldsInCSO5(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        reference_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @unpack
    @data(
           ('L', 'Lab'),
           ('S', 'Sender'),
           ('G', 'GP'),
           ('U', 'User'),
           ('D', 'District'),
           ('P', 'Practice'),
    )
    def test_cso5_with_valid_reference_type(self, reference_type, expected_reference_type):
        event = self.build_event(valid_row, field_to_update='REFERENCE_TYPE', value_to_update=reference_type)
        context = Mock()
        context.function_name = ''
        transform_and_store_CSO5_data.lambda_handler(event, context)

        # Asserts it stores the record with the correct reference_type
        reference_table_mock.assert_called_once()
        args, _ = reference_table_mock.call_args
        self.assertEqual(args[1].get('reference_type'), expected_reference_type)
        self.assertTrue(args[1].get('source_cipher').startswith('STL'))

    @data(
           ('invalid')
    )
    def test_cso5_with_invalid_reference_type(self, reference_type):
        event = self.build_event(valid_row, field_to_update='REFERENCE_TYPE', value_to_update=reference_type)
        context = Mock()
        context.function_name = ''
        transform_and_store_CSO5_data.lambda_handler(event, context)

        # Asserts the record is not stored the correct reference_type
        reference_table_mock.assert_not_called()

        # Asserts the record is sent to the DLQ
        message_records = event.get('Records', [{}])
        message_record = message_records[0]
        message_body = json.loads(str(message_record.get('body')))
        sqs_client_mock.send_message.called_once()
        _, kwargs = sqs_client_mock.send_message.call_args
        self.assertEqual(kwargs['QueueUrl'], 'LAMBDA_DLQ_URL')
        self.assertEqual(kwargs['MessageBody'],
                         json.dumps(message_body,
                         indent=4, sort_keys=True, default=str))

    @unpack
    @data(
          ('1', 'GP'),
          ('2', 'Community Clinic'),
          ('3', 'GUM Clinic'),
          ('4', 'NHS Hospital'),
          ('5', 'DMS sender'),
          ('6', 'Other'),
          ('7', 'Consultant'),
          ('invalid', None)
    )
    def test_cso5_with_valid_and_invalid_reference_subtype(self, reference_subtype, expected_reference_subtype):
        event = self.build_event(valid_row, field_to_update='REF_SUBTYPE', value_to_update=reference_subtype)
        context = Mock()
        context.function_name = ''
        transform_and_store_CSO5_data.lambda_handler(event, context)

        # Asserts it stores the record with the correct reference_type
        reference_table_mock.assert_called_once()
        args, _ = reference_table_mock.call_args
        self.assertEqual(args[1].get('reference_subtype'), expected_reference_subtype)
        self.assertTrue(args[1].get('source_cipher').startswith('STL'))

    def build_event(self, row_dict, field_to_update=None, value_to_update=None):
        updated_row = {}
        updated_row.update(row_dict)
        if field_to_update:
            updated_row[field_to_update] = value_to_update
        body_dict = {
            'internal_id': 'internal_id',
            'file_name': 'file_name_before_part',
            'file_part_name': 'file_key',
            'line_index': 1,
            'line': '|'.join(updated_row.values())
        }
        return sqs_mock_event(body_dict)
