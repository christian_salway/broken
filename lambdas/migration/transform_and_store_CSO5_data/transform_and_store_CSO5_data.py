import os
import uuid
from datetime import datetime, timezone
from common.utils.transform_and_store_utils import (
    get_message_contents_from_event,
    get_message_body_from_event
)
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from transform_and_store_CSO5_data.log_references import LogReference
from common.utils.sqs_utils import send_new_message
from transform_and_store_CSO5_data.map_and_validate_CSO5_data import map_and_validate_CSO5_data
from common.utils.reference_utils import create_replace_reference_record

LAMBDA_DLQ_URL = os.environ.get('LAMBDA_DLQ_URL')


@lambda_entry_point
def lambda_handler(event, context):
    contents = get_message_contents_from_event(event)
    log({'log_reference': LogReference.CSO5MAP0001, 'message_id': contents['message_id']})
    record_line = contents['record_line']
    log({'log_reference': LogReference.CSO5MAP0002})

    log({'log_reference': LogReference.CSO5MAP0003})
    try:
        mapped_data = map_and_validate_CSO5_data(record_line)
    except Exception:
        log({'log_reference': LogReference.CSO5MAP0005, 'add_exception_info': True})
        send_new_message(LAMBDA_DLQ_URL, get_message_body_from_event(event))
        return

    log({'log_reference': LogReference.CSO5MAP0004})

    create_reference_record(record_line, mapped_data)


def create_reference_record(raw_data, mapped_data):
    migrated_5_data = {
        'received': datetime.now(timezone.utc).isoformat(),
        'value': raw_data
    }

    if 'validation_errors' in mapped_data:
        migrated_5_data['validation_errors'] = mapped_data['validation_errors']

    mapped_data['migrated_5_data'] = migrated_5_data
    mapped_data['record_id'] = str(uuid.uuid4())

    created_record = create_replace_reference_record(mapped_data)

    if created_record:
        log({'log_reference': LogReference.CSO5MAP0007})
