from common.utils.transform_and_store_utils import (
    remove_empty_values,
    add_hyphens_to_date
)
from common.log import log
from transform_and_store_CSO5_data.log_references import LogReference

DELIMITER = "|"

CSO5_FIELDS = (
    'SOURCE_CIPHER',
    'REF_TYPE',
    'REF_ID',
    'REF_NAME',
    'POSTCODE',
    'DHA',
    'NATIONAL',
    'ADDRESS1',
    'ADDRESS2',
    'ADDRESS3',
    'ADDRESS4',
    'ADDRESS5',
    'TELNO',
    'PARENT_CODE',
    'ACTIVE',
    'INACTIVE_DATE',
    'RESPONSIBLE',
    'EMAIL1',
    'EMAIL2',
    'REF_SUBTYPE',
    'SMARTCARD_ID'
)

REFERENCE_TYPE_MAP = {
    'L': 'Lab',
    'S': 'Sender',
    'G': 'GP',
    'U': 'User',
    'D': 'District',
    'P': 'Practice'
}

REFERENCE_SUBTYPE_MAP = {
    '1': 'GP',
    '2': 'Community Clinic',
    '3': 'GUM Clinic',
    '4': 'NHS Hospital',
    '5': 'DMS sender',
    '6': 'Other',
    '7': 'Consultant'
}


CSO5_EXPECTED_FIELD_COUNT = 21
EXTRA_FIELDS_LIMIT = 5
TOTAL_FIELDS_LIMIT = CSO5_EXPECTED_FIELD_COUNT + EXTRA_FIELDS_LIMIT


def map_and_validate_CSO5_data(raw_data):
    data_list = raw_data.split('|')

    if len(data_list) not in range(CSO5_EXPECTED_FIELD_COUNT, TOTAL_FIELDS_LIMIT):
        raise Exception(
            f'Malformed input: Number of fields in CSO5 result must be between {CSO5_EXPECTED_FIELD_COUNT} and '
            f'{TOTAL_FIELDS_LIMIT}')

    ref_type_value = data_list[CSO5_FIELDS.index('REF_TYPE')]

    if not _validate_ref_type(ref_type_value):
        log({'log_reference': LogReference.CSO5MAP0006})
        raise Exception('Reference type is invalid')

    return _map(data_list)


def _map(data_list):
    mapped_data = {
        'key': _generate_key(data_list),
        'source_cipher': data_list[CSO5_FIELDS.index('SOURCE_CIPHER')],
        'reference_type': _map_reference_type(data_list[CSO5_FIELDS.index('REF_TYPE')]),
        'reference_subtype': _map_reference_subtype(data_list[CSO5_FIELDS.index('REF_SUBTYPE')]),
        'reference_id': data_list[CSO5_FIELDS.index('REF_ID')],
        'name': data_list[CSO5_FIELDS.index('REF_NAME')],
        'dha_code': data_list[CSO5_FIELDS.index('DHA')],
        'national_code': data_list[CSO5_FIELDS.index('NATIONAL')],
        'address': {
            'address_line_1': data_list[CSO5_FIELDS.index('ADDRESS1')],
            'address_line_2': data_list[CSO5_FIELDS.index('ADDRESS2')],
            'address_line_3': data_list[CSO5_FIELDS.index('ADDRESS3')],
            'address_line_4': data_list[CSO5_FIELDS.index('ADDRESS4')],
            'address_line_5': data_list[CSO5_FIELDS.index('ADDRESS5')],
            'postcode': data_list[CSO5_FIELDS.index('POSTCODE')]
        },
        'telephone_number': data_list[CSO5_FIELDS.index('TELNO')],
        'gp_practice_code': data_list[CSO5_FIELDS.index('PARENT_CODE')],
        'active': _is_active(data_list[CSO5_FIELDS.index('ACTIVE')]),
        'inactive_from': add_hyphens_to_date(data_list[CSO5_FIELDS.index('INACTIVE_DATE')]),
        'cipher_responsible': _is_responsible(data_list[CSO5_FIELDS.index('RESPONSIBLE')]),
        'email_address_1': data_list[CSO5_FIELDS.index('EMAIL1')],
        'email_address_2': data_list[CSO5_FIELDS.index('EMAIL2')],
        'smartcard_id': data_list[CSO5_FIELDS.index('SMARTCARD_ID')],
    }

    return remove_empty_values(mapped_data)


def _validate_ref_type(ref_type_value):
    return _map_reference_type(ref_type_value) is not None


def _generate_key(data_list):
    source_cipher = data_list[CSO5_FIELDS.index('SOURCE_CIPHER')]
    reference_type = _map_reference_type(data_list[CSO5_FIELDS.index('REF_TYPE')])
    reference_id = data_list[CSO5_FIELDS.index('REF_ID')]

    return f'{source_cipher}#{reference_type}#{reference_id}'


def _is_active(value):
    return True if not value or value == 'Y' else False


def _is_responsible(value):
    return True if value == 'Y' else False


def _map_reference_type(ref_type_value):
    return REFERENCE_TYPE_MAP.get(ref_type_value)


def _map_reference_subtype(ref_subtype_value):
    return REFERENCE_SUBTYPE_MAP.get(ref_subtype_value)
