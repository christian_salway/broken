import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    CSO5MAP0001 = (logging.INFO, 'Message received from queue')
    CSO5MAP0002 = (logging.INFO, 'Extracted Record from SQS message')
    CSO5MAP0003 = (logging.INFO, 'Attempting to parse details from CSO5')
    CSO5MAP0004 = (logging.INFO, 'Successfully parsed details from CSO5')
    CSO5MAP0005 = (logging.ERROR, 'An error occurred when attempting to map and validate data.')
    CSO5MAP0006 = (logging.ERROR, 'CSO5 record has failed mandatory validation, record is rejected')
    CSO5MAP0007 = (logging.INFO, 'Reference table has been updated with CSO5 record')
