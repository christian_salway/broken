from unittest import TestCase
from mock import patch, Mock, call
from datetime import datetime, timezone
from transform_and_store_CSO5_data.log_references import LogReference


@patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
class TestTransformAndStoreCSO5Data(TestCase):

    @classmethod
    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    def setUpClass(cls):
        import transform_and_store_CSO5_data.transform_and_store_CSO5_data as transform_and_store_CSO5_data
        import common.utils.audit_utils as audit_utils
        global transform_and_store_CSO5_data, audit_utils
        cls.date_time_patcher = patch.object(transform_and_store_CSO5_data, 'datetime', Mock(wraps=datetime))
        cls.date_time_patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.date_time_patcher.stop()

    FIXED_NOW_TIME = datetime(2020, 3, 3, 9, 45, 23, 123456, tzinfo=timezone.utc)

    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.log')
    @patch('uuid.uuid4')
    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.create_replace_reference_record')
    def test_create_reference_record(self, create_replace_reference_record_mock, uuid4_mock, log_mock):

        raw_data = 'BAA|S|JCOL|UGRAUBURGH HEALTH AUTHORITY|S10 2SF|01||SHEFFIELD TEACHING HOSPITAL|JESSOP WING|TREE ROOT WALK|SHEFFIELD||0114-2268300||N|20190405||||7|'  

        transform_and_store_CSO5_data.datetime.now.return_value = self.FIXED_NOW_TIME
        uuid4_mock.return_value = 'UUID_VALUE'

        mapped_data = {
            'active': False,
            'address': {
                'address_line_1': 'SHEFFIELD TEACHING HOSPITAL',
                'address_line_2': 'JESSOP WING',
                'address_line_3': 'TREE ROOT WALK',
                'address_line_4': 'SHEFFIELD',
                'postcode': 'S10 2SF'
            },
            'cipher_responsible': False,
            'dha_code': '01',
            'inactive_from': '2019-04-05',
            'key': 'BAA#Sender#JCOL',
            'name': 'UGRAUBURGH HEALTH AUTHORITY',
            'reference_id': 'JCOL',
            'reference_subtype': 'Consultant',
            'reference_type': 'Sender',
            'source_cipher': 'BAA',
            'telephone_number': '0114-2268300'
        }

        expected_record = mapped_data.copy()
        expected_record['migrated_5_data'] = {
            'received': self.FIXED_NOW_TIME.isoformat(),
            'value': raw_data
        }
        expected_record['record_id'] = 'UUID_VALUE'

        transform_and_store_CSO5_data.create_reference_record(raw_data, mapped_data)

        create_replace_reference_record_mock.assert_called_with(expected_record)
        log_calls = [call({'log_reference': LogReference.CSO5MAP0007})]

        log_mock.assert_has_calls(log_calls)

    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.send_new_message')
    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.log')
    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.get_message_contents_from_event')
    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.get_message_body_from_event')
    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.map_and_validate_CSO5_data')
    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.create_reference_record')
    def test_message_moved_to_dlq_when_map_and_validate_fails(self,
                                                              create_reference_record_mock,
                                                              map_and_validate_CSO5_data_mock,
                                                              get_message_body_from_event_mock,
                                                              get_message_contents_from_event_mock,
                                                              log_mock,
                                                              send_new_message_mock):
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}

        get_message_contents_from_event_mock.return_value = record
        map_and_validate_CSO5_data_mock.side_effect = Exception()

        transform_and_store_CSO5_data.lambda_handler.__wrapped__(event, {})

        get_message_contents_from_event_mock.assert_called_with(event)
        map_and_validate_CSO5_data_mock.assert_called_with(record['record_line'])
        send_new_message_mock.assert_called()
        create_reference_record_mock.assert_not_called()

        log_calls = [call({'log_reference': LogReference.CSO5MAP0001, 'message_id': 12345678}),
                     call({'log_reference': LogReference.CSO5MAP0002}),
                     call({'log_reference': LogReference.CSO5MAP0003}),
                     call({'log_reference': LogReference.CSO5MAP0005, 'add_exception_info': True})]

        log_mock.assert_has_calls(log_calls)

    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.log')
    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.get_message_contents_from_event')
    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.map_and_validate_CSO5_data')
    @patch('transform_and_store_CSO5_data.transform_and_store_CSO5_data.create_reference_record')
    def test_CSO5_file_parsed_successfully(self,
                                           create_reference_record_mock,
                                           map_and_validate_CSO5_data_mock,
                                           get_message_contents_from_event_mock,
                                           log_mock):
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        parsed_record = {'this': 'mapped', 'reference_type': 'Sender'}
        updated_parsed_record = {'this': 'mapped', 'reference_type': 'Sender'}

        get_message_contents_from_event_mock.return_value = record
        map_and_validate_CSO5_data_mock.return_value = parsed_record

        transform_and_store_CSO5_data.lambda_handler.__wrapped__(event, {})

        get_message_contents_from_event_mock.assert_called_with(event)
        map_and_validate_CSO5_data_mock.assert_called_with(record['record_line'])
        create_reference_record_mock.assert_called_with('A|B|C|D', updated_parsed_record)

        log_calls = [call({'log_reference': LogReference.CSO5MAP0001, 'message_id': 12345678}),
                     call({'log_reference': LogReference.CSO5MAP0002}),
                     call({'log_reference': LogReference.CSO5MAP0003}),
                     call({'log_reference': LogReference.CSO5MAP0004})]

        log_mock.assert_has_calls(log_calls)
