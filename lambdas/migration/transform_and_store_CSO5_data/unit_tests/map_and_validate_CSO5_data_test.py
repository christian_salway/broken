from unittest import TestCase
from mock import patch
from ddt import ddt, data, unpack
with patch('boto3.resource'):
    import transform_and_store_CSO5_data.map_and_validate_CSO5_data as mapper


@ddt
class TestMapAndValidateCSO5Data(TestCase):

    @unpack
    @data(('L', True),
          ('S', True),
          ('G', True),
          ('U', True),
          ('D', True),
          ('P', True),
          ('X', False))
    def test_validate_ref_type(self, input_value, expected_return_value):

        actual_value = mapper._validate_ref_type(input_value)
        self.assertEqual(expected_return_value, actual_value)

    @unpack
    @data(('Y', True),
          ('N', False),
          ('X', False),
          (None, True))
    def test_is_active(self, input_value, expected_return_value):

        actual_value = mapper._is_active(input_value)
        self.assertEqual(expected_return_value, actual_value)

    @unpack
    @data(('Y', True),
          ('N', False),
          ('X', False),
          (None, False))
    def test_is_responsible(self, input_value, expected_return_value):

        actual_value = mapper._is_responsible(input_value)
        self.assertEqual(expected_return_value, actual_value)

    @unpack
    @data(('BAA|D|01|BARNSLEY|DL1 9AG|||CSAS|PO BOX 572|DARLINGTON|||||Y||||||', 'BAA#District#01'),
          ('BAA|G|153|DR. D F MCFEELY||01|133173|94 PARK GROVE|BARNSLEY||||022682345||N|19800101|Y|||| ', 'BAA#GP#153'),
          ('BAA|S|SHEJES|EYWOOD FAMILY PLANNING||01||CENTRAL HEALTH CLINIC|JESSOP HOSPITAL (WOMEN)|SHEFFIELD|||||N|20190405||||4|', 'BAA#Sender#SHEJES'),  
          ('BAA|U|ARUNA2|ANNETTE YATES|||||||||||N|20190803||ANNETTE.YATES@notnhs.net|||', 'BAA#User#ARUNA2'),
          ('BAA|L|1|BARNSLEY, SOUTH YORKSHIRE|S75 2EP|01|60350|BARNSLEY HOSPITAL NHS|FOUNDATION TRUST GAWBER ROAD|BARNSLEY|SOUTH YORKSHIRE||01226-730000||N||||||', 'BAA#Lab#1'))  
    def test_generate_key(self, row, expected_return_value):
        input_value = row.split('|')
        actual_value = mapper._generate_key(input_value)
        self.assertEqual(expected_return_value, actual_value)

    @unpack
    @data(('L', 'Lab'),
          ('S', 'Sender'),
          ('G', 'GP'),
          ('U', 'User'),
          ('D', 'District'),
          ('P', 'Practice'),
          ('X', None))
    def test_map_reference_type(self, input_value, expected_return_value):
        actual_value = mapper._map_reference_type(input_value)
        self.assertEqual(expected_return_value, actual_value)

    @unpack
    @data(('1', 'GP'),
          ('2', 'Community Clinic'),
          ('3', 'GUM Clinic'),
          ('4', 'NHS Hospital'),
          ('5', 'DMS sender'),
          ('6', 'Other'),
          ('7', 'Consultant'),
          ('X', None))
    def test_map_reference_subtype(self, input_value, expected_return_value):
        actual_value = mapper._map_reference_subtype(input_value)
        self.assertEqual(expected_return_value, actual_value)

    def test_mapper_returns_correct_dictionary(self):
        self.maxDiff = None
        row = 'BAA|S|JCOL|UGRAUBURGH HEALTH AUTHORITY|S10 2SF|01||SHEFFIELD TEACHING HOSPITAL|JESSOP WING|TREE ROOT WALK|SHEFFIELD||0114-2268300||N|20190405||||7|' 

        expected_value = {
            'active': False,
            'address': {
                'address_line_1': 'SHEFFIELD TEACHING HOSPITAL',
                'address_line_2': 'JESSOP WING',
                'address_line_3': 'TREE ROOT WALK',
                'address_line_4': 'SHEFFIELD',
                'postcode': 'S10 2SF'
            },
            'cipher_responsible': False,
            'dha_code': '01',
            'inactive_from': '2019-04-05',
            'key': 'BAA#Sender#JCOL',
            'name': 'UGRAUBURGH HEALTH AUTHORITY',
            'reference_id': 'JCOL',
            'reference_subtype': 'Consultant',
            'reference_type': 'Sender',
            'source_cipher': 'BAA', 'telephone_number': '0114-2268300'
        }

        input_value = row.split('|')
        actual_value = mapper._map(input_value)
        self.assertEqual(expected_value, actual_value)

    @patch('transform_and_store_CSO5_data.map_and_validate_CSO5_data.log')
    def test_exception_is_raised_for_invalid_ref_type(self, log_mock):
        row = 'BAA|X|1|BARNSLEY, SOUTH YORKSHIRE|S75 2EP|01|60350|BARNSLEY HOSPITAL NHS|FOUNDATION TRUST GAWBER ROAD|BARNSLEY|SOUTH YORKSHIRE||01226-730000||N||||||' 

        with self.assertRaises(Exception) as context:
            mapper.map_and_validate_CSO5_data(row)

        self.assertTrue('Reference type is invalid' in str(context.exception))

    @patch('transform_and_store_CSO5_data.map_and_validate_CSO5_data.log')
    def test_exception_is_raised_for_for_too_few_fields(self, log_mock):
        row = 'BAA|L|1|BARNSLEY, SOUTH YORKSHIRE|S75 2EP|01|60350|BARNSLEY HOSPITAL NHS|FOUNDATION TRUST GAWBER ROAD|BARNSLEY|SOUTH YORKSHIRE||01226-730000||N|||||' 

        with self.assertRaises(Exception) as context:
            mapper.map_and_validate_CSO5_data(row)

        self.assertTrue(
            'Malformed input: Number of fields in CSO5 result must be between 21 and 26' in str(context.exception))

    @patch('transform_and_store_CSO5_data.map_and_validate_CSO5_data.log')
    def test_exception_is_raised_for_too_many_fields(self, log_mock):
        row = 'BAA||L|1||BARNSLEY, SOUTH YORKSHIRE||S75 2EP|01|60350||BARNSLEY HOSPITAL NHS||FOUNDATION TRUST GAWBER ROAD||BARNSLEY|SOUTH YORKSHIRE||01226-730000||N||||||'  

        with self.assertRaises(Exception) as context:
            mapper.map_and_validate_CSO5_data(row)

        self.assertTrue(
            'Malformed input: Number of fields in CSO5 result must be between 21 and 26' in str(context.exception))
