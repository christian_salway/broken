import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    CSO3MAP0001 = (logging.INFO, 'Message received from queue')
    CSO3MAP0002 = (logging.INFO, 'Extracted Record from SQS message')
    CSO3MAP0003 = (logging.INFO, 'Attempting to parse details from CSO3')
    CSO3MAP0004 = (logging.INFO, 'Successfully parsed details from CSO3')
    CSO3MAP0007 = (logging.INFO, 'Attempting to create cease record')
    CSO3MAP0008 = (logging.INFO, 'Successfully created cease record')
    CSO3MAP0009 = (logging.INFO, 'Attempting to create defer record')
    CSO3MAP0010 = (logging.INFO, 'Successfully created defer record')
    CSO3MAP0011 = (logging.WARNING, 'CSO3 record has failed validation')
    CSO3MAP0012 = (logging.INFO, 'Attempting to find participant by NHS number')
    CSO3MAP0013 = (logging.INFO, 'Successfully found participant')
    CSO3MAP0014 = (logging.ERROR, 'CSO3 record has failed mandatory validation, record is rejected')
    CSO3MAP0015 = (logging.ERROR, 'Failed to create record - no matching participant for record nhs number')
    CSO3MAP0016 = (logging.ERROR, 'An error occurred when attempting to map and validate data.')
    CSO3MAP0017 = (logging.INFO, 'Attempting to compare hashed source row with existing records')
    CSO3MAP0018 = (logging.INFO, 'This record already exists. Aborting creation of this CS03 record')
    CSO3MAP0019 = (logging.INFO, 'Confirmed record is not a duplicate')
