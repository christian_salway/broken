from datetime import datetime, timezone
from unittest import TestCase
from unittest.mock import patch, call, Mock

from transform_and_store_CSO3_data.log_references import LogReference


class TestTransformAndStoreCSO3Data(TestCase):

    @classmethod
    @patch('boto3.resource', Mock())
    def setUpClass(cls):
        import transform_and_store_CSO3_data.transform_and_store_CSO3_data as transform_and_store_module
        import common.utils.audit_utils as audit_utils
        global transform_and_store_module, audit_utils
        cls.date_time_patcher = patch.object(transform_and_store_module, 'datetime', Mock(wraps=datetime))
        cls.date_time_patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.date_time_patcher.stop()

    FIXED_NOW_TIME = datetime(2020, 3, 3, 9, 45, 23, 123456, tzinfo=timezone.utc)

    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.log')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.get_message_contents_from_event')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.map_and_validate_CSO3_data')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.record_is_duplicate')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.get_participant_id')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.create_cease_or_defer_record')
    def test_CSO3_file_parsed_successfully(self,
                                           cease_or_defer_mock,
                                           get_participant_id_mock,
                                           duplicate_record_mock,
                                           parser_mock,
                                           get_message_contents_from_event_mock,
                                           log_mock):

        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        parsed_record = {'this': 'mapped', 'nhs_number': 'number1'}
        participant_id = '123'
        updated_parsed_record = {
            'this': 'mapped', 'nhs_number': 'number1', 'participant_id': participant_id}

        get_message_contents_from_event_mock.return_value = record
        parser_mock.return_value = parsed_record
        duplicate_record_mock.return_value = False
        get_participant_id_mock.return_value = participant_id

        transform_and_store_module.lambda_handler.__wrapped__(event, {})

        get_message_contents_from_event_mock.assert_called_with(event)
        parser_mock.assert_called_with(record['record_line'])
        duplicate_record_mock.assert_called_once_with(parsed_record, ['reason', 'user_id'])
        get_participant_id_mock.assert_called_with('number1')
        cease_or_defer_mock.assert_called_with('A|B|C|D', updated_parsed_record)

        log_calls = [
            call({'log_reference': LogReference.CSO3MAP0001, 'message_id': 12345678}),
            call({'log_reference': LogReference.CSO3MAP0002}),
            call({'log_reference': LogReference.CSO3MAP0003}),
            call({'log_reference': LogReference.CSO3MAP0004}),
            call({'log_reference': LogReference.CSO3MAP0017}),
            call({'log_reference': LogReference.CSO3MAP0019}),
            call({'log_reference': LogReference.CSO3MAP0012}),
            call({'log_reference': LogReference.CSO3MAP0013})
        ]

        log_mock.assert_has_calls(log_calls)

    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.log')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.create_replace_record_in_participant_table')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.remove_empty_values')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.audit')
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    def test_cease_record_is_created_successfully(self,
                                                  audit_mock,
                                                  remove_empty_values_mock,
                                                  create_replace_record_in_participant_table_mock,
                                                  log_mock):
        transform_and_store_module.datetime.now.return_value = self.FIXED_NOW_TIME

        raw_data = 'A|B|C|D'

        parsed_data = {
            'participant_id': '567',
            'nhs_number': '123',
            'date_amended': "2020-01-01",
            'reason': 'PATIENT_INFORMED_CHOICE',
            'user_id': '',
            'record_type': 'CEASE',
            'source_hash': 'hashed'
        }

        cease_record = {
            'participant_id': '567',
            'sort_key': 'CEASE#2020-01-01',
            'date_from': '2020-01-01',
            'reason': 'PATIENT_INFORMED_CHOICE',
            'user_id': '',
            'migrated_3_data': {
                'received': '2020-03-03T09:45:23.123456+00:00',
                'value': 'A|B|C|D'
            },
            'source_hash': 'hashed'
        }

        clean_expected_cease_record = {
            'participant_id': '567',
            'sort_key': 'CEASE#2020-01-01',
            'date_from': '2020-01-01',
            'reason': 'PATIENT_INFORMED_CHOICE',
            'migrated_3_data': {
                'received': '2020-03-03T09:45:23.123456+00:00',
                'value': 'A|B|C|D'
            },
            'source_hash': 'hashed'
        }

        remove_empty_values_mock.return_value = clean_expected_cease_record

        create_replace_record_in_participant_table_mock.return_value = {'ConsumedCapacity': {'CapacityUnits': 1.0}}

        transform_and_store_module.create_cease_or_defer_record(raw_data, parsed_data)

        log_calls = [call({'log_reference': LogReference.CSO3MAP0007}),
                     call({'log_reference': LogReference.CSO3MAP0008})]
        log_mock.assert_has_calls(log_calls)

        remove_empty_values_mock.assert_called_with(cease_record)
        create_replace_record_in_participant_table_mock.assert_called_with(clean_expected_cease_record)
        audit_mock.assert_called_with(action=audit_utils.AuditActions.STORE_CSO3,
                                      user=audit_utils.AuditUsers.MIGRATION,
                                      participant_ids=['567'])

    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.log')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.create_replace_record_in_participant_table')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.audit')
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    def test_cease_record_is_created_successfully_given_validation_errors(
            self, audit_mock, create_replace_record_in_participant_table_mock, log_mock):
        transform_and_store_module.datetime.now.return_value = self.FIXED_NOW_TIME

        raw_data = 'A|B|C|D'

        parsed_data = {
            'participant_id': '567',
            'nhs_number': '123',
            'date_amended': "2020-01-01",
            'reason': 'PATIENT_INFORMED_CHOICE',
            'user_id': 'RN',
            'record_type': 'CEASE',
            'validation_errors': {
                'user_id': {
                    'schema': 'must be 8 characters'
                }
            },
            'source_hash': 'hashed'
        }

        expected_cease_record = {
            'participant_id': '567',
            'sort_key': 'CEASE#2020-01-01',
            'date_from': '2020-01-01',
            'reason': 'PATIENT_INFORMED_CHOICE',
            'user_id': 'RN',
            'migrated_3_data': {
                'received': '2020-03-03T09:45:23.123456+00:00',
                'value': 'A|B|C|D',
                'validation_errors': {
                    'user_id': {
                        'schema': 'must be 8 characters'
                    }
                }
            },
            'source_hash': 'hashed'
        }

        create_replace_record_in_participant_table_mock.return_value = {'ConsumedCapacity': {'CapacityUnits': 1.0}}

        transform_and_store_module.create_cease_or_defer_record(raw_data, parsed_data)

        log_calls = [call({'log_reference': LogReference.CSO3MAP0007}),
                     call({'log_reference': LogReference.CSO3MAP0008})]
        log_mock.assert_has_calls(log_calls)

        create_replace_record_in_participant_table_mock.assert_called_with(expected_cease_record)
        audit_mock.assert_called_with(action=audit_utils.AuditActions.STORE_CSO3,
                                      user=audit_utils.AuditUsers.MIGRATION,
                                      participant_ids=['567'])

    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.log')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.create_replace_record_in_participant_table')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.audit')
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    def test_defer_record_is_created_successfully(self, audit_mock, create_replace_record_in_participant_table_mock,
                                                  log_mock):
        transform_and_store_module.datetime.now.return_value = self.FIXED_NOW_TIME

        raw_data = 'A|B|C|D'

        parsed_data = {
            'participant_id': '567',
            'nhs_number': '123',
            'date_amended': "2020-01-01",
            'reason': 'PATIENT_INFORMED_CHOICE',
            'previous_test_due_date': '2019-12-15',
            'user_id': 'RN',
            'new_test_due_date': '2020-02-12',
            'record_type': 'DEFER',
            'source_hash': 'hashed'
        }

        expected_defer_record = {
            'participant_id': '567',
            'sort_key': 'DEFER#2020-01-01',
            'date_from': '2020-01-01',
            'reason': 'PATIENT_INFORMED_CHOICE',
            'user_id': 'RN',
            'new_test_due_date': '2020-02-12',
            'previous_test_due_date': '2019-12-15',
            'migrated_3_data': {
                'received': '2020-03-03T09:45:23.123456+00:00',
                'value': 'A|B|C|D'
            },
            'source_hash': 'hashed'
        }

        create_replace_record_in_participant_table_mock.return_value = {'ConsumedCapacity': {'CapacityUnits': 1.0}}

        transform_and_store_module.create_cease_or_defer_record(raw_data, parsed_data)

        log_calls = [call({'log_reference': LogReference.CSO3MAP0009}),
                     call({'log_reference': LogReference.CSO3MAP0010})]

        log_mock.assert_has_calls(log_calls)
        create_replace_record_in_participant_table_mock.assert_called_with(expected_defer_record)
        audit_mock.assert_called_with(action=audit_utils.AuditActions.STORE_CSO3,
                                      user=audit_utils.AuditUsers.MIGRATION,
                                      participant_ids=['567'])

    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.log')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.create_replace_record_in_participant_table')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.audit')
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'TRUE'})
    def test_defer_record_is_created_successfully_given_validation_errors(
            self, audit_mock, create_replace_record_in_participant_table_mock, log_mock):
        transform_and_store_module.datetime.now.return_value = self.FIXED_NOW_TIME

        raw_data = 'A|B|C|D'

        parsed_data = {
            'participant_id': '567',
            'nhs_number': '123',
            'date_amended': "2020-01-01",
            'reason': 'PATIENT_INFORMED_CHOICE',
            'previous_test_due_date': '2019-12-15',
            'user_id': 'RN',
            'new_test_due_date': '2020-02-12',
            'record_type': 'DEFER',
            'validation_errors': {
                'user_id': {
                    'schema': 'must be 8 characters'
                }
            },
            'source_hash': 'hashed'
        }

        expected_defer_record = {
            'participant_id': '567',
            'sort_key': 'DEFER#2020-01-01',
            'date_from': '2020-01-01',
            'reason': 'PATIENT_INFORMED_CHOICE',
            'user_id': 'RN',
            'new_test_due_date': '2020-02-12',
            'previous_test_due_date': '2019-12-15',
            'migrated_3_data': {
                'received': '2020-03-03T09:45:23.123456+00:00',
                'value': 'A|B|C|D',
                'validation_errors': {
                    'user_id': {
                        'schema': 'must be 8 characters'
                    }
                }
            },
            'source_hash': 'hashed'
        }

        create_replace_record_in_participant_table_mock.return_value = {'ConsumedCapacity': {'CapacityUnits': 1.0}}

        transform_and_store_module.create_cease_or_defer_record(raw_data, parsed_data)

        log_calls = [call({'log_reference': LogReference.CSO3MAP0009}),
                     call({'log_reference': LogReference.CSO3MAP0010})]

        log_mock.assert_has_calls(log_calls)
        create_replace_record_in_participant_table_mock.assert_called_with(expected_defer_record)
        audit_mock.assert_called_with(action=audit_utils.AuditActions.STORE_CSO3,
                                      user=audit_utils.AuditUsers.MIGRATION,
                                      participant_ids=['567'])

    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.log')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.get_message_contents_from_event')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.map_and_validate_CSO3_data')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.record_is_duplicate')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.get_message_body_from_event')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.send_new_message')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.get_participant_id')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.create_cease_or_defer_record')
    def test_message_moved_to_dlq_when_map_and_validate_fails(self,
                                                              create_cease_or_defer_record_mock,
                                                              get_participant_id_mock,
                                                              send_new_message_mock,
                                                              get_message_body_from_event_mock,
                                                              duplicate_record_mock,
                                                              parser_mock,
                                                              get_message_contents_from_event_mock,
                                                              log_mock):

        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}

        parser_mock.side_effect = Exception()
        get_message_contents_from_event_mock.return_value = record

        transform_and_store_module.lambda_handler.__wrapped__(event, {})

        get_message_contents_from_event_mock.assert_called_with(event)
        parser_mock.assert_called_with(record['record_line'])
        send_new_message_mock.assert_called()
        get_message_body_from_event_mock.assert_called()
        duplicate_record_mock.assert_not_called()
        get_participant_id_mock.assert_not_called()
        create_cease_or_defer_record_mock.assert_not_called()

    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.log')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.get_message_contents_from_event')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.map_and_validate_CSO3_data')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.record_is_duplicate')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.get_participant_id')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.create_cease_or_defer_record')
    def test_lambda_exits_with_log_if_record_is_duplicate(
            self, cease_or_defer_mock, get_participant_id_mock, duplicate_record_mock,
            parser_mock, get_message_contents_from_event_mock, log_mock):
        event = {'example': 'event'}
        record = {'message_id': 12345678, 'record_line': 'A|B|C|D'}
        parsed_record = {'this': 'mapped'}

        get_message_contents_from_event_mock.return_value = record
        parser_mock.return_value = parsed_record
        duplicate_record_mock.return_value = True

        transform_and_store_module.lambda_handler.__wrapped__(event, {})

        get_message_contents_from_event_mock.assert_called_with(event)
        parser_mock.assert_called_with(record['record_line'])
        duplicate_record_mock.assert_called_once_with(parsed_record, ['reason', 'user_id'])
        get_participant_id_mock.assert_not_called()
        cease_or_defer_mock.assert_not_called()

        log_calls = [
            call({'log_reference': LogReference.CSO3MAP0001, 'message_id': 12345678}),
            call({'log_reference': LogReference.CSO3MAP0002}),
            call({'log_reference': LogReference.CSO3MAP0003}),
            call({'log_reference': LogReference.CSO3MAP0004}),
            call({'log_reference': LogReference.CSO3MAP0017}),
            call({'log_reference': LogReference.CSO3MAP0018})
        ]

        log_mock.assert_has_calls(log_calls)

    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.log')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.create_replace_record_in_participant_table')
    @patch('transform_and_store_CSO3_data.transform_and_store_CSO3_data.audit')
    @patch('os.environ', {'ENABLE_MIGRATION_AUDIT': 'FALSE'})
    def test_create_cease_or_defer_record_donot_audit_when_migration_audit_toggled_off(
            self,
            audit_mock,
            create_replace_record_in_participant_table_mock,
            log_mock):
        transform_and_store_module.datetime.utcnow.return_value = self.FIXED_NOW_TIME

        raw_data = 'A|B|C|D'

        parsed_data = {
            'participant_id': '567',
            'nhs_number': '123',
            'date_amended': "2020-01-01",
            'reason': 'PATIENT_INFORMED_CHOICE',
            'previous_test_due_date': '2019-12-15',
            'user_id': 'RN',
            'new_test_due_date': '2020-02-12',
            'record_type': 'DEFER',
            'source_hash': 'hashed'
        }

        create_replace_record_in_participant_table_mock.return_value = {'ConsumedCapacity': {'CapacityUnits': 1.0}}

        transform_and_store_module.create_cease_or_defer_record(raw_data, parsed_data)

        log_calls = [call({'log_reference': LogReference.CSO3MAP0009}),
                     call({'log_reference': LogReference.CSO3MAP0010})]

        log_mock.assert_has_calls(log_calls)
        audit_mock.assert_not_called()
