import json
from unittest import TestCase
from unittest.mock import patch, Mock
from common.utils import increment_date_by_years


@patch('transform_and_store_CSO3_data.map_and_validate_CSO3_data.get_hashed_value', Mock(return_value='hashed'))
@patch('transform_and_store_CSO3_data.map_and_validate_CSO3_data.log')
class TestTransformAndStoreCSO3Data(TestCase):

    @classmethod
    @patch('boto3.resource', Mock())
    def setUpClass(cls):
        import transform_and_store_CSO3_data.map_and_validate_CSO3_data as map_and_validate_module
        global map_and_validate_module

    def test_correct_record_type_is_detected(self, log_mock):
        DEFER_REASONS = ['1', '2', '3', '4', '5', '10', '11', '11*', '*11', '*11*']
        CEASE_REASONS = ['6', '7', '8', '9', '77', '99', '9C', '9C*', '*9C', '*9C*']

        for reason in DEFER_REASONS:
            record_type = map_and_validate_module._determine_record_type(reason)
            self.assertEqual(record_type, 'DEFER')
        for reason in CEASE_REASONS:
            record_type = map_and_validate_module._determine_record_type(reason)
            self.assertEqual(record_type, 'CEASE')

    def test_record_type_detected_with_invalid_reason(self, log_mock):
        incorrect_reason_code = '102'
        with self.assertRaises(Exception) as context:
            map_and_validate_module._determine_record_type(incorrect_reason_code)

        self.assertTrue('Reason not found in list' in str(context.exception))

    def test_determine_reason_with_invalid_reason(self, log_mock):
        incorrect_reason_code = '102'
        unknown_type = '102'
        returned_reason_code = map_and_validate_module._determine_reason(incorrect_reason_code)

        self.assertEqual(returned_reason_code, unknown_type)

    def test_determine_reason_with_asterisk_reason(self, log_mock):
        asterisk_reason_code = '1*'
        expected_reason_text = 'RECENT_TEST'
        actual_reason_code = map_and_validate_module._determine_reason(asterisk_reason_code)
        self.assertEqual(expected_reason_text, actual_reason_code)

    def test_exception_is_raised_for_require_fields(self, log_mock):

        raw_data = 'AAAAA|||1||20191215|RN|RSSNF|C|STL|20200212|AF'

        expected_exception = 'NHS Number, Amended date and Reason are required for ingestion'

        with self.assertRaises(Exception) as context:
            map_and_validate_module.map_and_validate_CSO3_data(raw_data)

        self.assertEqual(expected_exception, context.exception.args[0])

    def test_data_mapper_too_many_fields(self, log_mock):

        raw_data = 'STL|123|20200101|1|3|20191215|RN|R|C|STL|20200212|AMEND|||||||||||||||'

        with self.assertRaises(Exception) as context:
            map_and_validate_module.map_and_validate_CSO3_data(raw_data)

        self.assertTrue('Malformed input: Number of fields in CSO3 result must be between 12 and 17'
                        in str(context.exception))

    def test_data_mapper_too_few_fields(self, log_mock):

        raw_data = 'STL|123|20200101|1|3|20191215|RN|RSSNF'

        with self.assertRaises(Exception) as context:
            map_and_validate_module.map_and_validate_CSO3_data(raw_data)

        self.assertTrue('Malformed input: Number of fields in CSO3 result must be between 12 and 17'
                        in str(context.exception))

    def test_data_mapper_returns_correct_dictionary(self, log_mock):

        raw_data = 'STL|123|20200101|1|3|20191215|RN|R|C|STL|20200212|AMEND'

        expected_parsed_data = {
            'nhs_number': '123',
            'date_amended': "2020-01-01",
            'reason': 'PATIENT_INFORMED_CHOICE',
            'previous_test_due_date': '2019-12-15',
            'user_id': 'RN',
            'new_test_due_date': '2020-02-12',
            'record_type': 'DEFER',
            'source_hash': 'hashed'
        }

        parsed_data = map_and_validate_module.map_and_validate_CSO3_data(raw_data)
        self.assertEqual(expected_parsed_data, parsed_data)

    def test_asterisk_on_data_mapper(self, log_mock):

        raw_data = 'STL|123|20200101|1|3*|20191215|RN|R|C|STL|20200212|AMEND'

        expected_parsed_data = {
            'nhs_number': '123',
            'date_amended': "2020-01-01",
            'reason': 'PATIENT_INFORMED_CHOICE',
            'previous_test_due_date': '2019-12-15',
            'user_id': 'RN',
            'new_test_due_date': '2020-02-12',
            'record_type': 'DEFER',
            'source_hash': 'hashed'
        }

        parsed_data = map_and_validate_module.map_and_validate_CSO3_data(raw_data)
        self.assertEqual(expected_parsed_data, parsed_data)

    def test_data_mapper_passed_with_non_mandatory_invalid_fields_dictionary(self, log_mock):

        raw_data = 'STL|123|19690101|1A|99|201912155|R|RSRS|Z|STLL|202002122|AM'

        expected_parsed_data = {
            'nhs_number':  '123',
            'date_amended': '1969-01-01',
            'reason': 'MENTAL_CAPACITY_ACT',
            'previous_test_due_date': '2019-12-155',
            'user_id': 'R',
            'new_test_due_date': '2020-02-122',
            'record_type': 'CEASE',
            'validation_errors': {
                'amended': {
                    'data_error': 'Date must be between 1970-01-01 and [Today]'
                },
                'seq': {
                    'schema_error': 'Sequence number must be 1 character long',
                    'data_error': 'Sequence number can only contain numbers through 1 - 9'
                },
                'previous_test_due_date': {
                    'schema_error': 'Date length must be exactly 8 characters'
                },
                'user_id': {
                    'schema_error': 'User ID must be between 2 and 9 characters (inclusive)'
                },
                'reason_flag': {
                    'schema_error': 'Reason flag must be 1 character long'
                },
                'cease_code': {
                    'data_error': "Cease code not one of the expected values: ['C', 'D', 'F', 'N', 'Y', 'G', 'H']"
                },
                'cease_cipher': {
                    'schema_error': 'Cease cipher must be between 2 and 3 characters (inclusive)'
                },
                'delay_date': {
                    'schema_error': 'Date length must be exactly 8 characters'
                },
                'amend_flag': {
                    'schema_error': 'Amend flag must be 5 characters long',
                    'data_error': 'Amend flag value must be set to AMEND'
                }
            },
            'source_hash': 'hashed'
        }
        expected_parsed_data = increment_date_by_years(json.dumps(expected_parsed_data))

        parsed_data = map_and_validate_module.map_and_validate_CSO3_data(raw_data)

        self.assertEqual(expected_parsed_data, parsed_data)

    def test_data_mapper_passed_with_non_mandatory_invalid_fields_dictionary_2(self, log_mock):

        raw_data = 'STL|123|19710101|1|77|15122019|RN|R|Y|STLL|202002122|AM'

        expected_parsed_data = {
            'nhs_number':  '123',
            'date_amended': '1971-01-01',
            'reason': 'RADIOTHERAPY',
            'previous_test_due_date': '1512-20-19',
            'user_id': 'RN',
            'new_test_due_date': '2020-02-122',
            'record_type': 'CEASE',
            'validation_errors': {
                'previous_test_due_date': {
                    'schema_error': 'Date was in an invalid format. Expected YYYYMMDD'
                },
                'cease_cipher': {
                    'schema_error': 'Cease cipher must be between 2 and 3 characters (inclusive)'
                },
                'delay_date': {
                    'schema_error': 'Date length must be exactly 8 characters'
                },
                'amend_flag': {
                    'schema_error': 'Amend flag must be 5 characters long',
                    'data_error': 'Amend flag value must be set to AMEND'
                }
            },
            'source_hash': 'hashed'
        }

        parsed_data = map_and_validate_module.map_and_validate_CSO3_data(raw_data)

        self.assertEqual(expected_parsed_data, parsed_data)
