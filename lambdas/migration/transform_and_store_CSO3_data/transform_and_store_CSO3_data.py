import os
from datetime import datetime, timezone
from common.utils.transform_and_store_utils import (
    get_message_contents_from_event,
    create_replace_record_in_participant_table,
    remove_empty_values,
    get_message_body_from_event,
    record_is_duplicate
)
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from common.utils.audit_utils import AuditActions, AuditUsers, audit
from common.utils.participant_utils import get_participant_id
from transform_and_store_CSO3_data.log_references import LogReference
from transform_and_store_CSO3_data.map_and_validate_CSO3_data import map_and_validate_CSO3_data, CEASE_RECORD_TYPE
from common.utils.sqs_utils import send_new_message

LAMBDA_DLQ_URL = os.environ.get('LAMBDA_DLQ_URL')


@lambda_entry_point
def lambda_handler(event, context):
    contents = get_message_contents_from_event(event)
    log({'log_reference': LogReference.CSO3MAP0001, 'message_id': contents['message_id']})
    record_line = contents['record_line']
    log({'log_reference': LogReference.CSO3MAP0002})

    log({'log_reference': LogReference.CSO3MAP0003})
    try:
        mapped_data = map_and_validate_CSO3_data(record_line)
    except Exception:
        log({'log_reference': LogReference.CSO3MAP0016, 'add_exception_info': True})
        send_new_message(LAMBDA_DLQ_URL, get_message_body_from_event(event))
        return

    log({'log_reference': LogReference.CSO3MAP0004})

    log({'log_reference': LogReference.CSO3MAP0017})
    comparison_fields = ['reason', 'user_id']
    if record_is_duplicate(mapped_data, comparison_fields):
        log({'log_reference': LogReference.CSO3MAP0018})
        return
    log({'log_reference': LogReference.CSO3MAP0019})

    log({'log_reference': LogReference.CSO3MAP0012})

    participant_id = get_participant_id(mapped_data['nhs_number'])
    if participant_id is None:
        log({'log_reference': LogReference.CSO3MAP0015})
        raise Exception("No participant found for given nhs number.")

    log({'log_reference': LogReference.CSO3MAP0013})
    mapped_data['participant_id'] = participant_id

    create_cease_or_defer_record(record_line, mapped_data)


def create_cease_or_defer_record(raw_data, mapped_data):
    migrated_3_data = {
        'received': datetime.now(timezone.utc).isoformat(),
        'value': raw_data
    }

    if 'validation_errors' in mapped_data:
        migrated_3_data['validation_errors'] = mapped_data['validation_errors']

    date_from = mapped_data.get('date_amended', datetime.now(timezone.utc).date().isoformat())
    record_type = mapped_data['record_type']
    participant_id = mapped_data['participant_id']
    record = {
        'participant_id': participant_id,
        'sort_key': f'{record_type}#{date_from}',
        'date_from': date_from,
        'reason': mapped_data['reason'],
        'user_id': mapped_data.get('user_id', ''),
        'migrated_3_data': migrated_3_data,
        'source_hash': mapped_data['source_hash']
    }

    if record_type == CEASE_RECORD_TYPE:
        log({'log_reference': LogReference.CSO3MAP0007})
    else:
        record['previous_test_due_date'] = mapped_data.get('previous_test_due_date', '')
        record['new_test_due_date'] = mapped_data.get('new_test_due_date', '')
        log({'log_reference': LogReference.CSO3MAP0009})

    cleaned_record = remove_empty_values(record)
    created_record = create_replace_record_in_participant_table(cleaned_record)

    if created_record:
        if record_type == CEASE_RECORD_TYPE:
            log({'log_reference': LogReference.CSO3MAP0008})
        else:
            log({'log_reference': LogReference.CSO3MAP0010})
        if os.environ.get('ENABLE_MIGRATION_AUDIT') != 'FALSE':
            audit(action=AuditActions.STORE_CSO3,
                  user=AuditUsers.MIGRATION,
                  participant_ids=[participant_id])
