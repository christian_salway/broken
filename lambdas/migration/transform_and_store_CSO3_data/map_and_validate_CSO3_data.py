from common.utils.import_migration_data_validation_utils import (
    ImportMigrationDataValidation as data)
from common.utils.import_migration_schema_validation_utils import (
    ImportMigrationSchemaValidation as schema)
from common.utils.transform_and_store_utils import (
    remove_empty_values,
    add_hyphens_to_date,
    missing_necessary_fields,
    get_hashed_value
)
from common.log import log
from common.models.participant import CeaseReason, DeferReason
from transform_and_store_CSO3_data.log_references import LogReference

DELIMITER = "|"

CSO3_Fields = (
    'SOURCE_CIPHER',
    'NHSNUM',
    'AMENDED',
    'SEQ',
    'REASON',
    'PNTD',
    'USERID',
    'RSN_FLAG',
    'CEASE_CODE',
    'CEASE_CIPHER',
    'DELAY_DATE',
    'AMEND_FLAG'
)

DOB_OPTIONS = {
    'M': '1',
    'F': '2',
    'I': '0'
}

CSO3_EXPECTED_FIELD_COUNT = 12
EXTRA_FIELDS_LIMIT = 5
CEASE_RECORD_TYPE = "CEASE"
DEFER_RECORD_TYPE = "DEFER"
UNKNOWN_RECORD_TYPE = "UNKNOWN"

CSO3_VALIDATION_DESIGN = {
    'source_cipher': {
        'schema': schema().validate_source_cipher
    },
    'nhs_number': {
        'schema': schema().validate_nhs_number
    },
    'amended': {
        'schema': schema().validate_date,
        'data': data().validate_amend_date_data,
    },
    'seq': {
        'schema': schema().validate_sequence_number,
        'data': data().validate_sequence_number,
    },
    'reason': {
        'schema': schema().validate_reason
    },
    'previous_test_due_date': {
        'schema': schema().validate_previous_test_due_date
    },
    'user_id': {
        'schema': schema().validate_user_id
    },
    'reason_flag': {
        'schema': schema().validate_reason_flag
    },
    'cease_code': {
        'schema': schema().validate_cease_code,
        'data': data().validate_cease_code,
    },
    'cease_cipher': {
        'schema': schema().validate_cease_cipher
    },
    'delay_date': {
        'schema': schema().validate_date,
    },
    'amend_flag': {
        'schema': schema().validate_amend_flag,
        'data': data().validate_amend_flag,
    },
}


def map_and_validate_CSO3_data(raw_data):
    data_list = raw_data.split('|')

    if len(data_list) < CSO3_EXPECTED_FIELD_COUNT or len(data_list) > CSO3_EXPECTED_FIELD_COUNT + EXTRA_FIELDS_LIMIT:
        raise Exception(
            f'Malformed input: Number of fields in CSO3 result must be between {CSO3_EXPECTED_FIELD_COUNT} and '
            f'{CSO3_EXPECTED_FIELD_COUNT + EXTRA_FIELDS_LIMIT}')

    errors = _validate(data_list)

    if errors:
        log({'log_reference': LogReference.CSO3MAP0011, 'validation_errors': errors})
        if missing_necessary_fields(errors, ['nhs_number', 'amended', 'reason']):
            log({'log_reference': LogReference.CSO3MAP0014})
            raise Exception('NHS Number, Amended date and Reason are required for ingestion')

    return _map(data_list, errors, raw_data)


def _validate(data_list):

    errors = {}
    for index, piece in enumerate(CSO3_VALIDATION_DESIGN):
        response = {piece: {}}

        schema_function = CSO3_VALIDATION_DESIGN[piece]['schema']
        if 'schema_args' in CSO3_VALIDATION_DESIGN[piece]:
            args = CSO3_VALIDATION_DESIGN[piece]['schema_args']
            schema_response = schema_function(data_list[index], args)
            if schema_response:
                response[piece].update(schema_response)
        else:
            schema_response = schema_function(data_list[index])
            if schema_response:
                response[piece].update(schema_response)

        if 'data' in CSO3_VALIDATION_DESIGN[piece]:
            data_function = CSO3_VALIDATION_DESIGN[piece]['data']
            data_response = _validate_data_if_possible(schema_response, data_function, data_list[index])
            if data_response:
                response[piece].update(data_response)

        if response[piece]:
            errors.update(response)

    return errors


def _validate_data_if_possible(schema_error, data_validation_function, data_piece):
    if not schema_error:
        return data_validation_function(data_piece)
    elif 'schema_error' in schema_error:
        if 'is a required value' not in schema_error['schema_error']:
            return data_validation_function(data_piece)


def _map(data_list, validation_errors, raw_data):
    mapped_data = {
        'nhs_number': data_list[CSO3_Fields.index('NHSNUM')],
        'date_amended': add_hyphens_to_date(data_list[CSO3_Fields.index('AMENDED')]),
        'reason': _determine_reason(data_list[CSO3_Fields.index('REASON')]),
        'previous_test_due_date': add_hyphens_to_date(data_list[CSO3_Fields.index('PNTD')]),
        'user_id': data_list[CSO3_Fields.index('USERID')],
        'new_test_due_date': add_hyphens_to_date(data_list[CSO3_Fields.index('DELAY_DATE')]),
        'record_type':  _determine_record_type(data_list[CSO3_Fields.index('REASON')]),
        'validation_errors': validation_errors,
        'source_hash': get_hashed_value(raw_data)
    }

    return remove_empty_values(mapped_data)


def _determine_reason(reason_code):

    sanitised_reason_code = reason_code.replace('*', '')

    REASON_DICTIONARY = {
        '1': DeferReason.RECENT_TEST,
        '2': DeferReason.CURRENT_PREGNANCY,
        '3': DeferReason.PATIENT_INFORMED_CHOICE,
        '4': DeferReason.UNDER_TREATMENT_RELEVANT_TO_SCREENING,
        '5': DeferReason.ADMINISTRATIVE,
        '6': CeaseReason.DUE_TO_AGE,
        '7': CeaseReason.NO_CERVIX,
        '8': CeaseReason.PATIENT_INFORMED_CHOICE,
        '9': CeaseReason.OTHER,
        '10': DeferReason.PRACTICE_INVITATION,
        '11': DeferReason.DISCHARGE_FROM_COLPOSCOPY,
        '77': CeaseReason.RADIOTHERAPY,
        '99': CeaseReason.MENTAL_CAPACITY_ACT,
        '9C': CeaseReason.OTHER
    }

    return_reason_code = REASON_DICTIONARY[sanitised_reason_code] \
        if sanitised_reason_code in REASON_DICTIONARY else sanitised_reason_code

    return return_reason_code


def _determine_record_type(reason_code):
    sanitised_reason_code = reason_code.replace('*', '')

    DEFER_REASONS = ['1', '2', '3', '4', '5', '10', '11']
    CEASE_REASONS = ['6', '7', '8', '9', '77', '99', '9C']

    if sanitised_reason_code in DEFER_REASONS:
        return DEFER_RECORD_TYPE
    elif sanitised_reason_code in CEASE_REASONS:
        return CEASE_RECORD_TYPE
    else:
        raise Exception('Reason not found in list')
