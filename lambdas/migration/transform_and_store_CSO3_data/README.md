This lambda receives a record from the import-CSO3-rows SQS Queue and appends the record against an existing participant.
In its current state, only the "Happy Path" is considered.