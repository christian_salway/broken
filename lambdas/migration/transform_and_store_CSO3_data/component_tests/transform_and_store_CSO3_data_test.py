import json
import hashlib
from unittest import TestCase
from unittest.mock import patch, Mock
from boto3.dynamodb.conditions import Key
from ddt import ddt, data, unpack
from collections import OrderedDict

from common.models.participant import ParticipantSortKey


participants_table_mock = Mock()


valid_row = OrderedDict([
    ('SOURCE_CIPHER', 'STL'),
    ('NHSNUM', '9100001848'),
    ('AMENDED', '20200101'),
    ('SEQ', '1'),
    ('REASON', '9C'),
    ('PNTD', '20040101'),
    ('USERID', 'RN'),
    ('RSN_FLAG', 'R'),
    ('CEASE_CODE', 'C'),
    ('CEASE_CIPHER', 'STL'),
    ('DELAY_DATE', '20200101'),
    ('AMEND_FLAG', 'AMEND')
])


class_lvl_mock_env_vars = {
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'ENABLE_MIGRATION_AUDIT': 'TRUE',
    'DYNAMODB_PARTICIPANTS': 'participants-table'
}


@ddt
@patch('os.environ', class_lvl_mock_env_vars)
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(return_value=participants_table_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
class TestReasonCodeInCSO3(TestCase):

    mock_env_vars = {
        'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
        'PARTICIPANTS_TABLE_NAME': 'PARTICIPANTS_TABLE_NAME'
    }

    @classmethod
    @patch('boto3.client')
    @patch('boto3.resource')
    @patch('os.environ', mock_env_vars)
    def setUpClass(cls, boto3_resource, boto3_client, *args):
        cls.log_patcher = patch('common.log.log')
        participants_table_mock.query.side_effect = []
        cls.participants_table_mock = participants_table_mock

        sqs_client_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: sqs_client_mock if args and args[0] == 'sqs' else Mock()
        cls.sqs_client_mock = sqs_client_mock

        global transform_and_store_module
        import transform_and_store_CSO3_data.transform_and_store_CSO3_data as transform_and_store_module

        from common.test_mocks.mock_events import sqs_mock_event as _sqs_mock_event
        global sqs_mock_event
        sqs_mock_event = _sqs_mock_event

    def setUp(self):
        self.participants_table_mock.reset_mock()
        self.sqs_client_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @unpack
    @data(('1', 'RECENT_TEST', 'DEFER'),
          ('2', 'CURRENT_PREGNANCY', 'DEFER'),
          ('3', 'PATIENT_INFORMED_CHOICE', 'DEFER'),
          ('4', 'UNDER_TREATMENT_RELEVANT_TO_SCREENING', 'DEFER'),
          ('5', 'ADMINISTRATIVE', 'DEFER'),
          ('6', 'DUE_TO_AGE', 'CEASE'),
          ('7', 'NO_CERVIX', 'CEASE'),
          ('8', "PATIENT_INFORMED_CHOICE", 'CEASE'),
          ('9', 'OTHER', 'CEASE'),
          ('10', 'PRACTICE_INVITATION', 'DEFER'),
          ('11', 'DISCHARGE_FROM_COLPOSCOPY', 'DEFER'),
          ('77', 'RADIOTHERAPY', 'CEASE'),
          ('99', 'MENTAL_CAPACITY_ACT', 'CEASE'),
          ('9C', 'OTHER', 'CEASE'))
    def test_cso3_with_valid_reason_code(self, reason_code, expected_reason, cease_defer):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123', 'sort_key': ParticipantSortKey.PARTICIPANT}]}
            elif kwargs['IndexName'] == 'source-hash':
                return {'Items': []}
        self.participants_table_mock.query.side_effect = _query_mock

        event, raw_row = self.build_event_and_raw_row(valid_row, field_to_update='REASON', value_to_update=reason_code)

        context = Mock()
        context.function_name = ''

        transform_and_store_module.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing participant and record
        self.assertEqual(self.participants_table_mock.query.call_count, 2)

        _, kwargs = self.participants_table_mock.query.call_args_list[0]
        self.assertEqual(kwargs['IndexName'], 'source-hash')

        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])

        _, kwargs = self.participants_table_mock.query.call_args_list[1]
        expected_key_condition = Key('sanitised_nhs_number').eq('9100001848') & Key('sort_key').eq('PARTICIPANT')
        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])
        self.assertEqual(kwargs['IndexName'], 'sanitised-nhs-number-sort-key')

        # Asserts it stores the record with the correct reason
        self.participants_table_mock.put_item.assert_called_once()
        _, kwargs = self.participants_table_mock.put_item.call_args
        self.assertEqual(expected_reason, kwargs['Item'].get('reason'))
        self.assertTrue(kwargs['Item'].get('sort_key').startswith(cease_defer))
        self.assertEqual(kwargs['Item'].get('migrated_3_data').get('validation_errors'), None)

        # Asserts it sends audit record to the audit queue
        self.sqs_client_mock.send_message.assert_called_once()
        _, kwargs = self.sqs_client_mock.send_message.call_args
        actual_audit_record_sent = json.loads(kwargs['MessageBody'])
        self.assertEqual(actual_audit_record_sent.get('action'), 'STORE_CSO3')
        self.assertEqual(actual_audit_record_sent.get('nhsid_useruid'), 'MIGRATION_123')
        self.assertEqual(actual_audit_record_sent.get('session_id'), 'NONE')
        self.assertEqual(actual_audit_record_sent.get('participant_ids'), ['123'])
        self.assertEqual(kwargs.get('QueueUrl'), 'AUDIT_QUEUE_URL')

    @data(('01'),
          ('AA'),
          ('111'))
    def test_cso3_with_invalid_reason_code(self, reason_code):
        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'sanitised-nhs-number-sort-key':
                return {'Items': [{'participant_id': '123', 'sort_key': ParticipantSortKey.PARTICIPANT}]}
        self.participants_table_mock.query.side_effect = _query_mock

        # Asserts it does not store the record with the correct reason
        self.participants_table_mock.put_item.assert_not_called()

        # Asserts it does not send audit record to the audit queue
        self.sqs_client_mock.send_message.assert_not_called()

    def test_cso3_duplicate_record(self):
        mock_record = {
            'reason': 'OTHER',
            'user_id': 'RN'
        }

        def _query_mock(*args, **kwargs):
            if kwargs['IndexName'] == 'source-hash':
                return {'Items': [mock_record]}
        self.participants_table_mock.query.side_effect = _query_mock

        event, raw_row = self.build_event_and_raw_row(valid_row)

        context = Mock()
        context.function_name = ''

        transform_and_store_module.lambda_handler(event, context)

        # Asserts it checks the dynamodb for the existing record
        self.participants_table_mock.query.assert_called_once()
        _, kwargs = self.participants_table_mock.query.call_args
        self.assertEqual(kwargs['IndexName'], 'source-hash')
        hashed_row = hashlib.sha512((raw_row).encode('utf-8')).hexdigest()
        expected_key_condition = Key('source_hash').eq(hashed_row)
        self.assertEqual(expected_key_condition, kwargs['KeyConditionExpression'])

        # Asserts it does not store the record
        self.participants_table_mock.put_item.assert_not_called()

    def build_event_and_raw_row(self, row_dict, field_to_update=None, value_to_update=None):
        updated_row = {}
        updated_row.update(row_dict)
        if field_to_update:
            updated_row[field_to_update] = value_to_update
        raw_row = '|'.join(updated_row.values())
        body_dict = {
            'internal_id': 'internal_id',
            'file_name': 'file_name_before_part',
            'file_part_name': 'file_key',
            'line_index': 1,
            'line': raw_row
        }
        return sqs_mock_event(body_dict), raw_row
