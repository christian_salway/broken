from urllib.parse import unquote
from datetime import datetime
from common.log import log
from manage_result.log_references import LogReference
from common.utils.audit_utils import audit, AuditActions
from common.utils import json_return_object, json_return_message
from common.utils.participant_utils import (
    transaction_write_records,
    get_record_from_participant_table,
)
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator \
    import IGNORE_FOR_CALCULATIONS_MESSAGE
from manage_result.manage_result_utils import (
    RESULT_FIELDS_TO_AUDIT,
    should_update_next_test_due_date,
    autocease_if_eligible,
    get_participant_ntdd,
    get_recent_results_by_participant_id,
)


def delete_result(event_data, participant):
    log(LogReference.MANAGERESULT004)
    participant_id = event_data['path_parameters']['participant_id']
    sort_key = unquote(event_data['path_parameters']['sort_key'])
    request_body = event_data.get('body')
    next_test_due_date_value = request_body.get('next_test_due_date')
    crm_number = request_body.get('crm_number')
    comments = request_body.get('comments')

    result_to_delete = get_record_from_participant_table(participant_id, sort_key, segregate=True)

    if not result_to_delete:
        log(LogReference.MANAGERESULT013)
        return json_return_message(404, 'Result does not exist')

    results = get_recent_results_by_participant_id(participant_id, segregate=True)
    latest_result = results[0]
    previous_results = results[1:] if len(results) >= 1 else []
    recall_months = latest_result.get('recall_months')

    soft_deleted_record = result_to_delete.copy()
    soft_deleted_record['sort_key'] = 'DELETED_' + sort_key

    key_to_delete = {
        'participant_id': participant_id,
        'sort_key': result_to_delete['sort_key']
    }

    update_transaction = []
    additional_information = {}
    update_next_test_due_date = False
    if next_test_due_date_value and result_to_delete['sort_key'] == latest_result['sort_key']:
        for previous_result in previous_results:
            result_values = {
                'result_code': previous_result.get('result_code'),
                'action_code': previous_result.get('action_code'),
                'infection_code': previous_result.get('infection_code'),
                'test_date': previous_result.get('test_date'),
                'hpv_primary': previous_result.get('hpv_primary', True),
                'self_sample': previous_result.get('self_sample', False),
            }
            try:
                next_test_due_date = datetime.fromisoformat(next_test_due_date_value).date()
                current_next_test_due_date_value = participant['next_test_due_date']
                current_next_test_due_date = datetime.fromisoformat(current_next_test_due_date_value).date()
                update_next_test_due_date, updated_status = should_update_next_test_due_date(
                    participant,
                    result_values,
                    current_next_test_due_date,
                    next_test_due_date,
                )
                participant_key = {
                    'participant_id': participant_id,
                    'sort_key': 'PARTICIPANT'
                }

                if update_next_test_due_date:
                    update_transaction.append({
                        'record_keys': participant_key,
                        'update_values': {
                            'next_test_due_date': str(next_test_due_date),
                            'status': updated_status.value
                        }
                    })
                    additional_information.update({'updating_next_test_due_date_from': current_next_test_due_date_value,
                                                   'updating_next_test_due_date_to': next_test_due_date_value})
                break
            except ValueError as e:
                if str(e) == IGNORE_FOR_CALCULATIONS_MESSAGE:
                    log({'log_reference': LogReference.MANAGERESULT040})
                else:
                    log({'log_reference': LogReference.MANAGERESULT022, 'error': str(e)})
                    return json_return_message(400, 'Invalid next test due date provided')

    # This will transactionally put the soft deleted record into dynamo while deleting the original
    transaction_write_records([soft_deleted_record], [key_to_delete], update_transaction, segregate=True)

    additional_information.update({'crm_number': crm_number, 'comments': comments})

    additional_information.update({k: v for k, v in result_to_delete.items() if k in RESULT_FIELDS_TO_AUDIT})

    action = result_to_delete['action']

    if action == 'Repeat advised':
        additional_information.update({'recall_months': recall_months})

    log(LogReference.MANAGERESULT016)
    audit(
        action=AuditActions.MANUAL_DELETE_RESULT,
        session=event_data.get('session'),
        participant_ids=[participant_id],
        nhs_numbers=[participant['nhs_number']],
        additional_information=additional_information
    )
    final_participant_ntdd = next_test_due_date if update_next_test_due_date else get_participant_ntdd(participant)
    autocease_if_eligible(participant, final_participant_ntdd)
    return json_return_object(200)
