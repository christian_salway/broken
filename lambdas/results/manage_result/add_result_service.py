from datetime import datetime
from common.log import log
from manage_result.log_references import LogReference
from common.models.letters import SupressionReason
from common.models.participant import ParticipantSortKey
from common.utils.audit_utils import audit, AuditActions
from common.utils import generate_crm_list, json_return_object, json_return_message
from common.utils.participant_utils import (
    create_replace_record_in_participant_table,
    transaction_write_records,
)
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator \
    import IGNORE_FOR_CALCULATIONS_MESSAGE
from common.utils.result_letter_utils import (
    check_for_result_letter_suppression,
    add_result_letter_to_processing_queue
)
from manage_result.manage_result_utils import (
    create_result_record,
    validate_mandatory_fields,
    RESULT_FIELDS_TO_AUDIT,
    should_update_next_test_due_date,
    get_latest_test_date,
    autocease_if_eligible,
    get_participant_ntdd
)


def add_result(event_data, participant):
    log(LogReference.MANAGERESULT002)
    request_body = event_data.get('body')
    recall_months = request_body.get('recall_months', None)
    participant_id = event_data['path_parameters']['participant_id']
    next_test_due_date_value = request_body.get('next_test_due_date', None)
    current_next_test_due_date_value = request_body.get('current_next_test_due_date', None)
    next_test_due_date = None
    current_next_test_due_date = None

    if next_test_due_date_value:
        try:
            next_test_due_date = datetime.fromisoformat(next_test_due_date_value).date()
        except (TypeError, ValueError) as e:
            log({'log_reference': LogReference.MANAGERESULT023, 'error': str(e)})
            return json_return_message(400, 'Invalid next test due date value provided')

    if current_next_test_due_date_value:
        try:
            current_next_test_due_date = datetime.fromisoformat(current_next_test_due_date_value).date()
        except (TypeError, ValueError) as e:
            log({'log_reference': LogReference.MANAGERESULT024, 'error': str(e)})
            return json_return_message(400, 'Invalid current next test due date value provided')

    result_values = {key: value for key, value in request_body.items() if key not in
                     ['current_next_test_due_date', 'next_test_due_date']}
    is_valid_result, error_message = validate_mandatory_fields(result_values)

    if not is_valid_result:
        return error_message

    update_next_test_due_date = False
    test_date = datetime.fromisoformat(result_values['test_date']).date()
    latest_test_date = get_latest_test_date(participant_id)
    most_recent_result = not latest_test_date or test_date > latest_test_date
    if most_recent_result:
        try:
            update_next_test_due_date, updated_status = should_update_next_test_due_date(
                participant,
                result_values,
                current_next_test_due_date,
                next_test_due_date,
            )
        except ValueError as e:
            if str(e) == IGNORE_FOR_CALCULATIONS_MESSAGE:
                log({'log_reference': LogReference.MANAGERESULT040})
            else:
                log({'log_reference': LogReference.MANAGERESULT022, 'error': str(e)})
                return json_return_message(400, 'Invalid next test due date provided')

    result = create_result_record(result_values, participant)
    if not most_recent_result:
        suppression_reason = SupressionReason.HISTORIC_RESULT
    else:
        suppression_reason = check_for_result_letter_suppression(result)
    if suppression_reason:
        result['suppression_reason'] = suppression_reason

    crm_list = generate_crm_list(result, event_data.get('session'))
    result['crm'] = crm_list
    crm_number = result.pop('crm_number', '')
    comments = result.pop('comments', '')

    additional_information = {
        'comments': comments,
        'crm_number': crm_number
    }
    additional_information.update({k: v for k, v in result.items() if k in RESULT_FIELDS_TO_AUDIT})

    if update_next_test_due_date:
        transaction_write_records([result], [], [{
            'record_keys': {
                'participant_id': participant_id,
                'sort_key': ParticipantSortKey.PARTICIPANT.value
            },
            'update_values': {
                'status': updated_status.value,
                'next_test_due_date': str(next_test_due_date)
            }
        }], segregate=True)
        additional_information.update({'updating_next_test_due_date_from': current_next_test_due_date_value,
                                       'updating_next_test_due_date_to': next_test_due_date_value})
    else:
        create_replace_record_in_participant_table(result, segregate=True)

    action = result['action']

    if action == 'Repeat advised':
        additional_information.update({'recall_months': recall_months})

    log(LogReference.MANAGERESULT008)
    audit(
        action=AuditActions.MANUAL_ADD_RESULT,
        session=event_data.get('session'),
        participant_ids=[participant_id],
        nhs_numbers=[participant['nhs_number']],
        additional_information=additional_information
    )
    final_participant_ntdd = next_test_due_date if update_next_test_due_date else get_participant_ntdd(participant)
    can_auto_cease = autocease_if_eligible(participant, final_participant_ntdd)
    if not suppression_reason:
        add_result_letter_to_processing_queue(result, can_auto_cease)
    return json_return_object(201)
