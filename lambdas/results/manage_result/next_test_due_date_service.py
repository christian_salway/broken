from datetime import datetime
from urllib.parse import unquote

from nose.tools import nottest

from common.log import log
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator import (
    calculate_next_test_due_date_for_result, calculate_next_test_due_date,
    NO_MATCH_MESSAGE, IGNORE_FOR_CALCULATIONS_MESSAGE, INVALID_DATE_MESSAGE, NO_RULES_MESSAGE)
from common.utils import json_return_data, json_return_message
from manage_result.log_references import LogReference
from common.log_references import CommonLogReference
from manage_result.manage_result_utils import (
    get_latest_and_previous_test_data, required_update_ntdd_status, UpdateParticipantUsingResult, get_latest_test_date
)

NO_CHANGE_RESPONSE_MESSAGE = 'No next test due date change'


@nottest
def next_test_due_date(event_data, participant):
    log(LogReference.MANAGERESULT017)
    participant_id = event_data['path_parameters']['participant_id']
    request_body = event_data.get('body')
    test_date_param = request_body.get('test_date')
    result_code = request_body.get('result_code')
    infection_code = request_body.get('infection_code')
    action_code = request_body.get('action_code')
    sort_key = request_body.get('sort_key')
    hpv_primary = request_body.get('hpv_primary')
    self_sample = request_body.get('self_sample')
    try:
        test_date = datetime.fromisoformat(test_date_param).date()
    except ValueError:
        log(LogReference.MANAGERESULT018)
        return json_return_message(400, 'Invalid test_date value provided')

    """Add result"""
    if sort_key is None:
        latest_result_test_date = get_latest_test_date(participant_id)

        if latest_result_test_date and test_date <= latest_result_test_date:
            log(LogReference.MANAGERESULT019)
            return _prepare_response(test_date, result_code, infection_code, action_code, participant, hpv_primary,
                                     self_sample, NTDD_unchanged=True)

        log(LogReference.MANAGERESULT029)
        return _prepare_response(test_date, result_code, infection_code, action_code, participant, hpv_primary,
                                 self_sample)

    """Edit result"""
    latest_result, previous_result = get_latest_and_previous_test_data(participant_id)

    latest_result_test_date = _get_result_date(latest_result)
    previous_result_test_date = _get_result_date(previous_result)

    sort_key_match = sort_key == latest_result.get('sort_key')

    participant_update = required_update_ntdd_status(
        sort_key_match, test_date, latest_result_test_date, previous_result, previous_result_test_date
    )
    if participant_update == UpdateParticipantUsingResult.CURRENT:
        log(LogReference.MANAGERESULT030)
        return _prepare_response(test_date, result_code, infection_code, action_code, participant, hpv_primary,
                                 self_sample)

    elif participant_update == UpdateParticipantUsingResult.PREVIOUS:
        previous_result_code = previous_result.get('result_code')
        previous_infection_code = previous_result.get('infection_code')
        previous_action_code = previous_result.get('action_code')
        previous_hpv_primary = previous_result.get('hpv_primary', True)
        previous_self_sample = previous_result.get('self_sample', False)
        log(LogReference.MANAGERESULT031)
        return _prepare_response(previous_result_test_date, previous_result_code, previous_infection_code,
                                 previous_action_code, participant, previous_hpv_primary, previous_self_sample)

    log(LogReference.MANAGERESULT019)
    return _prepare_response(test_date, result_code, infection_code, action_code, participant, hpv_primary,
                             self_sample, NTDD_unchanged=True)


@nottest
def next_test_due_date_after_delete(event_data, participant):
    log(LogReference.MANAGERESULT025)
    participant_id = event_data['path_parameters']['participant_id']
    result_sort_key = unquote(event_data['path_parameters']['sort_key'])

    is_ceased = participant.get('is_ceased')
    if is_ceased:
        log(LogReference.MANAGERESULT042)
        return _unchanged_response()

    latest_test, previous_test = get_latest_and_previous_test_data(participant_id)
    if not latest_test:
        log(LogReference.MANAGERESULT026)
        return _unchanged_response()

    if result_sort_key != latest_test.get('sort_key'):
        log(LogReference.MANAGERESULT027)
        return _unchanged_response()

    if not previous_test:
        log(LogReference.MANAGERESULT028)
        return _unchanged_response()

    previous_test_date = datetime.fromisoformat(previous_test['test_date']).date()
    previous_recall_months = previous_test.get('recall_months')
    if (previous_recall_months):
        return _prepare_deleted_result_response_using_recall_months(previous_test_date, previous_recall_months)

    result_code = previous_test.get('result_code')
    infection_code = previous_test.get('infection_code')
    action_code = previous_test.get('action_code')
    hpv_primary = previous_test.get('hpv_primary', True)
    self_sample = previous_test.get('self_sample', False)

    return _prepare_response(previous_test_date, result_code, infection_code, action_code,
                             participant, hpv_primary, self_sample, is_delete=True)


@nottest
def _get_next_test_due_date(test_date, result_code, infection_code, action_code, participant, hpv_primary, self_sample):

    next_test_due_dates = calculate_next_test_due_date_for_result(
        test_date,
        (result_code, infection_code, action_code),
        datetime.fromisoformat(participant['date_of_birth']).date(),
        hpv_primary,
        self_sample
    )

    next_test_due_dates.pop('status')
    default_value = next_test_due_dates['default']
    if default_value:
        default_value['next_test_due_date'] = default_value['next_test_due_date'].isoformat()

    if next_test_due_dates['options']:
        for option in next_test_due_dates['options']:
            if option.get('next_test_due_date'):
                option['next_test_due_date'] = option['next_test_due_date'].isoformat()

    if next_test_due_dates['range']:
        for range_interval in next_test_due_dates['range']:
            if range_interval.get('next_test_due_date'):
                range_interval['next_test_due_date'] = range_interval['next_test_due_date'].isoformat()

    participant_current_ntdd = participant.get('next_test_due_date')
    if participant_current_ntdd:
        next_test_due_dates['current'] = participant_current_ntdd

    log(LogReference.MANAGERESULT020)
    return next_test_due_dates


def _prepare_response(
        test_date, result_code, infection_code, action_code, participant, hpv_primary, self_sample,
        NTDD_unchanged=False, is_delete=False):
    try:
        next_test_due_dates = _get_next_test_due_date(
            test_date,
            result_code, infection_code, action_code,
            participant,
            hpv_primary,
            self_sample
        )
        log(LogReference.MANAGERESULT036)
        if is_delete:
            default_ntdd = next_test_due_dates.get('default', {}).get('next_test_due_date')
            return json_return_data(200, {'next_test_due_date': default_ntdd})

        if NTDD_unchanged:
            return _unchanged_response_with_data(next_test_due_dates)

        if not next_test_due_dates['options'] and \
            not next_test_due_dates['range'] and \
                next_test_due_dates['default']['next_test_due_date'] == next_test_due_dates['current']:
            return _unchanged_response_with_data(next_test_due_dates)

        return json_return_data(200, next_test_due_dates)
    except ValueError as err:
        if str(err) == NO_MATCH_MESSAGE:
            log(LogReference.MANAGERESULT021)
            return json_return_message(400, NO_MATCH_MESSAGE)
        elif str(err) == IGNORE_FOR_CALCULATIONS_MESSAGE:
            log({'log_reference': LogReference.MANAGERESULT040})
            return _unchanged_response()
        elif str(err) == INVALID_DATE_MESSAGE:
            log({'log_reference': LogReference.MANAGERESULT018, 'add_exception_info': True})
            return json_return_message(400, INVALID_DATE_MESSAGE)
        elif str(err) == NO_RULES_MESSAGE:
            log({'log_reference': LogReference.MANAGERESULT041, 'add_exception_info': True})
            return json_return_message(400, NO_RULES_MESSAGE)
        else:
            log(CommonLogReference.EXCEPTION000)
            raise err


def _prepare_deleted_result_response_using_recall_months(test_date, recall_months):
    log(LogReference.MANAGERESULT043)
    next_test_due_date = calculate_next_test_due_date(test_date, int(recall_months))
    next_test_due_date = next_test_due_date.isoformat()
    return json_return_data(200, {'next_test_due_date': next_test_due_date})


# Small private method to encapsulate the unchanged response logic should it change any further
def _unchanged_response():
    return json_return_data(200, {'unchanged': True})


def _unchanged_response_with_data(next_test_due_dates):
    next_test_due_dates['unchanged'] = True
    return json_return_data(200, next_test_due_dates)


def _get_result_date(result):
    if result is None:
        return None

    return datetime.fromisoformat(result['test_date']).date()
