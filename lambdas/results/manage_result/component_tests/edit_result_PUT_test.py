import copy
import json
from datetime import datetime, timezone
from unittest import TestCase

from manage_result.component_tests.mock_env_vars import mock_env_vars
from mock import patch, Mock, call

from common.utils.data_segregation.nhais_ciphers import Cohorts
from common.models.participant import ParticipantSortKey, ParticipantStatus


from manage_result.manage_result import lambda_handler

example_date = datetime(2020, 10, 18, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

participants_table_mock = Mock()
sqs_audit_client_mock = Mock()
sqs_client_mock = Mock()
dynamo_client_mock = Mock()
dynamo_resource_mock = Mock()

EXPECTED_PUT_ITEM = {
    'TableName': 'PARTICIPANTS_TABLE',
    'Item': {
        'result_id': {'S': '624900b3-3466-4ad4-aac8-3e484823fca7'},
        'sending_lab': {'S': 'akdak'},
        'source_code': {'S': 'abcd'}, 'sender_code': {'S': '299205'},
        'slide_number': {'S': '249797'}, 'test_date': {'S': '2020-03-20'},
        'result_code': {'S': 'X'}, 'infection_code': {'S': '0'},
        'action_code': {'S': 'A'}, 'health_authority': {'S': 'B'},
        'result': {'S': 'No Cytology test undertaken'},
        'infection_result': {'S': 'HPV negative'},
        'action': {'S': 'Routine'}, 'participant_id': {'S': '123456'},
        'sort_key': {'S': 'RESULT#2020-03-20#2020-11-29T00:00:00.000000+00:00'},
        'nhs_number': {'S': '9999999999'}, 'sanitised_nhs_number': {'S': '9999999999'},
        'created': {'S': '2020-11-29T00:00:00.000000+00:00'}, 'crm': {'L': [{'M': {
            'comments': {'S': 'some free text'}, 'crm_number': {'S': 'CAS-12345-ABCDE'},
            'first_name': {'S': 'firsty'}, 'last_name': {'S': 'lasty'},
            'timestamp': {'S': '2020-10-18T13:48:08.000000+00:00'}}}, {'M': {
                'crm_number': {'S': '140540'}, 'comments': {'S': 'some free text'},
                'first_name': {'S': 'firsty'}, 'last_name': {'S': 'lasty'},
                'timestamp': {'S': '2020-10-18T13:48:08.000000+00:00'}}}]}
    }
}
EXPECTED_DELETE_ITEM = {
    'TableName': 'PARTICIPANTS_TABLE',
    'Key': {
        'participant_id': {'S': '123456'},
        'sort_key': {'S': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}
    }
}


SUCCESS_RESPONSE = {
    'statusCode': 200,
    'headers': {'Content-Type': 'application/json',
                'X-Content-Type-Options': 'nosniff',
                'Strict-Transport-Security': 'max-age=1576800'},
    'body': '{}',
    'isBase64Encoded': False
}
AUDIT_BODY = {
    'action': 'MANUAL_EDIT_RESULT',
    'timestamp': '2020-10-18T13:48:08+00:00',
    'internal_id': 'requestID',
    'nhsid_useruid': '23456789',
    'session_id': '987654321',
    'first_name': 'firsty',
    'last_name': 'lasty',
    'role_id': 'test_role',
    'user_organisation_code': 'test_organisation_code',
    'participant_ids': ['123456'],
    'nhs_numbers': ['9999999999'],
    'additional_information': {
        'comments': 'some free text', 'crm_number': '140540',
        'sending_lab': 'akdak', 'source_code': 'abcd', 'sender_code': '299205',
        'slide_number': '249797', 'test_date': '2020-03-20', 'result_code': 'X',
        'infection_code': '0', 'action_code': 'A', 'health_authority': 'B',
        'result': 'No Cytology test undertaken',
        'infection_result': 'HPV negative', 'action': 'Routine'
    },
}
REQUEST_BODY = {
    'sending_lab': 'akdak',
    'source_code': 'abcd',
    'sender_code': '299205',
    'slide_number': '249797',
    'test_date': '2020-03-20',
    'result_code': 'X',
    'infection_code': '0',
    'action_code': 'A',
    'health_authority': 'B',
    'crm_number': '140540',
    'comments': 'some free text',
    'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
    'current_next_test_due_date': '2023-04-01',
}


@patch('common.utils.datetime', datetime_mock)
@patch('manage_result.manage_result_utils.datetime', datetime_mock)
@patch('common.utils.audit_utils.datetime', datetime_mock)
@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_audit_client_mock))
@patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_client', Mock(return_value=dynamo_client_mock))
@patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_resource',
       Mock(return_value=dynamo_resource_mock))
@patch('common.utils.participant_utils.get_participant_by_participant_id', participants_table_mock)
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('os.environ', mock_env_vars)
class TestEditResult(TestCase):
    mock_participant_record = {
        'participant_id': '123456',
        'sort_key': 'PARTICIPANT',
        'nhs_number': '9999999999',
        'date_of_birth': '1990-01-01',
        'next_test_due_date': '2023-04-01',
        'registered_gp_practice_code': 'X29',
        'nhais_cipher': 'MAN'
    }

    mock_result = {
        'result_id': '624900b3-3466-4ad4-aac8-3e484823fca7',
        'extra_field': 'some extra field',
        'action_code': 'A',
        'infection_code': '0',
        'sending_lab': 'akdak',
        'nhs_number': '9999999999',
        'sanitised_nhs_number': '9999999999',
        'participant_id': '123456',
        'result_code': 'X',
        'sender_code': '299205',
        'slide_number': '249797',
        'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
        'source_code': 'abcd',
        'test_date': '2020-04-01',
        'crm': [
            {
                'comments': 'some free text',
                'crm_number': 'CAS-12345-ABCDE',
                'first_name': 'firsty',
                'last_name': 'lasty',
                'timestamp': '2020-10-18T13:48:08.000000+00:00'
            }
        ]
    }

    mock_filtered_participants = {mock_participant_record['participant_id']: mock_participant_record['nhais_cipher']}

    def setUp(self):
        self.log_patcher = patch('manage_result.manage_result.log')
        self.util_log_patcher = patch('manage_result.manage_result_utils.log')
        self.edit_result_log_patcher = patch('manage_result.edit_result_service.log')
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        sqs_audit_client_mock.reset_mock()
        dynamo_resource_mock.reset_mock()
        self.log_patcher.start()
        self.util_log_patcher.start()
        self.edit_result_log_patcher.start()

        dynamo_resource_mock.batch_get_item.return_value = {
            "Responses": {
                "PARTICIPANTS_TABLE": [{
                    **self.mock_participant_record,
                    'role_id': 'test_role',
                    'user_organisation_code': 'test_organisation_code'
                }]
            }
        }

    def tearDown(self):
        self.log_patcher.stop()
        self.util_log_patcher.stop()
        self.edit_result_log_patcher.stop()

    def test_PUT_edit_result_for_participant_no_ntdd_change(self):
        participants_table_mock.get_item = Mock(side_effect=[
            {'Item': self.mock_participant_record},
            {'Item': self.mock_result}
        ])
        participants_table_mock.query.side_effect = [
            {
                'Items': [{'test_date': '2022-01-01', 'sort_key': ParticipantSortKey.RESULT, 'participant_id': '12356'}]
            },
            {'Items': []}
        ]
        request_body = {
            **REQUEST_BODY,
            'test_date': '2020-04-01',
            'current_next_test_due_date': '2023-04-01',
            'next_test_due_date': '2023-04-01',
        }
        event = self._build_event('123456', '23456789', '987654321', 'PUT', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)
        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
            call(Key={'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}),
        ])

        # Assert result record updated
        expected_key = {'participant_id': {'S': '123456'},
                        'sort_key': {'S': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}}  
        update_expression = """SET #sending_lab = :sending_lab, #source_code = :source_code, #sender_code = :sender_code, #slide_number = :slide_number, #test_date = :test_date, #result_code = :result_code, #infection_code = :infection_code, #action_code = :action_code, #health_authority = :health_authority, #result = :result, #infection_result = :infection_result, #action = :action, #crm = :crm"""  
        expression_attribute_values = {
            ':sending_lab': {'S': 'akdak'},
            ':source_code': {'S': 'abcd'},
            ':sender_code': {'S': '299205'},
            ':slide_number': {'S': '249797'},
            ':test_date': {'S': '2020-04-01'},
            ':result_code': {'S': 'X'},
            ':infection_code': {'S': '0'},
            ':action_code': {'S': 'A'},
            ':health_authority': {'S': 'B'},
            ':result': {'S': 'No Cytology test undertaken'},
            ':infection_result': {'S': 'HPV negative'},
            ':action': {'S': 'Routine'},
            ':crm': {
                'L': [{
                    'M': {
                        'comments': {'S': 'some free text'},
                        'crm_number': {'S': 'CAS-12345-ABCDE'},
                        'first_name': {'S': 'firsty'},
                        'last_name': {'S': 'lasty'},
                        'timestamp': {'S': '2020-10-18T13:48:08.000000+00:00'}
                    }
                }, {
                    'M': {
                        'crm_number': {'S': '140540'},
                        'comments': {'S': 'some free text'},
                        'first_name': {'S': 'firsty'},
                        'last_name': {'S': 'lasty'},
                        'timestamp': {'S': '2020-10-18T13:48:08.000000+00:00'}
                    }
                }]
            }
        }
        expression_attribute_names = {
            '#sending_lab': 'sending_lab',
            '#source_code': 'source_code', '#sender_code': 'sender_code',
            '#slide_number': 'slide_number', '#test_date': 'test_date',
            '#result_code': 'result_code', '#infection_code': 'infection_code',
            '#action_code': 'action_code', '#health_authority': 'health_authority', '#crm': 'crm',
            '#result': 'result', '#infection_result': 'infection_result', '#action': 'action'}
        dynamo_client_mock.transact_write_items.assert_called_with(TransactItems=[{
            'Update': {
                'TableName': 'PARTICIPANTS_TABLE',
                'Key': expected_key,
                'UpdateExpression': update_expression,
                'ExpressionAttributeValues': expression_attribute_values,
                'ExpressionAttributeNames': expression_attribute_names,
            }
        }])

        # Assert edit result is audited
        audit_body = copy.deepcopy(AUDIT_BODY)
        audit_body['additional_information'].update({
            'test_date': '2020-04-01'
        })
        sqs_audit_client_mock.send_message.assert_called_with(
            QueueUrl='the_audit_queue_url',
            MessageBody=json.dumps(audit_body),
        )

        # Assert lambda response
        self.assertDictEqual(SUCCESS_RESPONSE, actual_response)

    def test_PUT_edit_latest_result_move_behind_previous(self):
        # Arrange
        participants_table_mock.get_item = Mock(side_effect=[
            {'Item': self.mock_participant_record},
            {'Item': self.mock_result}
        ])
        # The new next test due date should be based on the second most recent result
        # (the most recent is moving earlier than it)
        participants_table_mock.query.return_value = {
            'Items': [
                {
                    'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
                    'test_date': self.mock_result['test_date'],
                    'participant_id': '123456'
                },
                {
                    'sort_key': 'RESULT#2020-03-31#2020-03-31T00:00:00.000000+00:00',
                    'test_date': '2020-03-31',
                    'result_code': 'X',
                    'infection_code': '0',
                    'action_code': 'A',
                    'participant_id': '123456'
                }
            ]
        }
        request_body = {
            **REQUEST_BODY,
            'next_test_due_date': '2023-03-31',  # Three years after the new most recent result
        }
        event = self._build_event('123456', '23456789', '987654321', 'PUT', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        # Act
        actual_response = lambda_handler(event, context)

        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
            call(Key={'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}),
        ])

        # Assert result record updated
        expected_update_item = {
            'TableName': 'PARTICIPANTS_TABLE',
            'Key': {'participant_id': {'S': '123456'}, 'sort_key': {'S': 'PARTICIPANT'}},
            'UpdateExpression': 'SET #status = :status, #next_test_due_date = :next_test_due_date',
            'ExpressionAttributeValues': {':status': {'S': ParticipantStatus.ROUTINE},
                                          ':next_test_due_date': {'S': '2023-03-31'}},
            'ExpressionAttributeNames': {'#status': 'status', '#next_test_due_date': 'next_test_due_date'}
        }

        dynamo_client_mock.transact_write_items.assert_called_with(TransactItems=[
            {'Put': EXPECTED_PUT_ITEM},
            {'Delete': EXPECTED_DELETE_ITEM},
            {'Update': expected_update_item}
        ])

        # Assert edit result is audited
        audit_body = copy.deepcopy(AUDIT_BODY)
        audit_body['additional_information'].update({
            'updating_next_test_due_date_from': '2023-04-01',
            'updating_next_test_due_date_to': '2023-03-31'})
        sqs_audit_client_mock.send_message.assert_called_with(
            QueueUrl='the_audit_queue_url',
            MessageBody=json.dumps(audit_body),
        )

        # Assert lambda response
        self.assertDictEqual(SUCCESS_RESPONSE, actual_response)

    def test_PUT_edit_latest_result_move_behind_previous_but_ignore_next(self):
        # Arrange
        participants_table_mock.get_item = Mock(side_effect=[
            {'Item': self.mock_participant_record},
            {'Item': self.mock_result}
        ])
        # The new next test due date should be based on the third most recent result
        # (the most recent is moving earlier than it, second is ignored)
        participants_table_mock.query.return_value = {
            'Items': [
                {
                    'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
                    'test_date': self.mock_result['test_date'],
                    'participant_id': '123456'
                },
                {
                    'sort_key': 'RESULT#2020-03-31#2020-03-31T00:00:00.000000+00:00',
                    'test_date': '2020-03-31',
                    'result_code': 'X',
                    'infection_code': 'U',
                    'action_code': 'H',
                    'hpv_primary': True,
                    'self_sample': True,
                    'participant_id': '123456'
                },
                {
                    'sort_key': 'RESULT#2020-03-30#2020-03-30T00:00:00.000000+00:00',
                    'test_date': '2020-03-30',
                    'result_code': 'X',
                    'infection_code': '0',
                    'action_code': 'A',
                    'participant_id': '123456'
                }
            ]
        }
        request_body = {
            **REQUEST_BODY,
            'next_test_due_date': '2023-03-30',  # Three years after the new most recent result
        }
        event = self._build_event('123456', '23456789', '987654321', 'PUT', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        # Act
        actual_response = lambda_handler(event, context)

        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
            call(Key={'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}),
        ])

        # Assert result record updated
        expected_update_item = {
            'TableName': 'PARTICIPANTS_TABLE',
            'Key': {'participant_id': {'S': '123456'}, 'sort_key': {'S': 'PARTICIPANT'}},
            'UpdateExpression': 'SET #status = :status, #next_test_due_date = :next_test_due_date',
            'ExpressionAttributeValues': {':status': {'S': ParticipantStatus.ROUTINE},
                                          ':next_test_due_date': {'S': '2023-03-30'}},
            'ExpressionAttributeNames': {'#status': 'status', '#next_test_due_date': 'next_test_due_date'}
        }

        dynamo_client_mock.transact_write_items.assert_called_with(TransactItems=[
            {'Put': EXPECTED_PUT_ITEM},
            {'Delete': EXPECTED_DELETE_ITEM},
            {'Update': expected_update_item}
        ])

        # Assert edit result is audited
        audit_body = copy.deepcopy(AUDIT_BODY)
        audit_body['additional_information'].update({
            'updating_next_test_due_date_from': '2023-04-01',
            'updating_next_test_due_date_to': '2023-03-30'})
        sqs_audit_client_mock.send_message.assert_called_with(
            QueueUrl='the_audit_queue_url',
            MessageBody=json.dumps(audit_body),
        )

        # Assert lambda response
        self.assertDictEqual(SUCCESS_RESPONSE, actual_response)

    def test_PUT_edit_latest_result_move_behind_previous_but_ignore(self):
        # Arrange
        participants_table_mock.get_item = Mock(side_effect=[
            {'Item': self.mock_participant_record},
            {'Item': self.mock_result}
        ])
        # The new next test due date should be based on the third most recent result
        # (the most recent is moving earlier than it, second is ignored)
        participants_table_mock.query.return_value = {
            'Items': [
                {
                    'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
                    'test_date': self.mock_result['test_date'],
                    'result_code': 'X',
                    'infection_code': '0',
                    'action_code': 'A',
                    'participant_id': '123456'
                },
                {
                    'sort_key': 'RESULT#2020-03-31#2020-03-31T00:00:00.000000+00:00',
                    'test_date': '2020-03-31',
                    'result_code': 'X',
                    'infection_code': 'U',
                    'action_code': 'H',
                    'hpv_primary': True,
                    'self_sample': True,
                    'participant_id': '123456'
                }
            ]
        }
        request_body = {
            **REQUEST_BODY,
            'next_test_due_date': '2023-03-20',  # Three years after this result (post update)
        }
        event = self._build_event('123456', '23456789', '987654321', 'PUT', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        # Act
        actual_response = lambda_handler(event, context)

        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
            call(Key={'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}),
        ])

        # Assert result record updated
        dynamo_client_mock.transact_write_items.assert_called_with(TransactItems=[
            {'Put': EXPECTED_PUT_ITEM},
            {'Delete': EXPECTED_DELETE_ITEM},
        ])

        # Assert edit result is audited
        sqs_audit_client_mock.send_message.assert_called_with(
            QueueUrl='the_audit_queue_url',
            MessageBody=json.dumps(AUDIT_BODY),
        )

        # Assert lambda response
        self.assertDictEqual(SUCCESS_RESPONSE, actual_response)

    def test_PUT_edit_result_test_date_for_participant_when_sort_key_matched_and_no_previous_result(self):
        # Arrange
        participants_table_mock.get_item = Mock(side_effect=[
            {'Item': self.mock_participant_record},
            {'Item': self.mock_result}
        ])
        participants_table_mock.query.side_effect = [
            {
                'Items': [{
                    'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
                    'test_date': self.mock_result['test_date'],
                    'participant_id': '123456'
                }]
            },
            {'Items': []}
        ]
        request_body = {
            **REQUEST_BODY,
            'next_test_due_date': '2023-03-20',
        }
        event = self._build_event('123456', '23456789', '987654321', 'PUT', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        # Act
        actual_response = lambda_handler(event, context)

        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
            call(Key={'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}),
        ])

        # Assert result record updated
        expected_update_item = {
            'TableName': 'PARTICIPANTS_TABLE',
            'Key': {'participant_id': {'S': '123456'}, 'sort_key': {'S': 'PARTICIPANT'}},
            'UpdateExpression': 'SET #status = :status, #next_test_due_date = :next_test_due_date',
            'ExpressionAttributeValues': {':status': {'S': ParticipantStatus.ROUTINE},
                                          ':next_test_due_date': {'S': '2023-03-20'}},
            'ExpressionAttributeNames': {'#status': 'status', '#next_test_due_date': 'next_test_due_date'}
        }

        dynamo_client_mock.transact_write_items.assert_called_with(TransactItems=[
            {'Put': EXPECTED_PUT_ITEM},
            {'Delete': EXPECTED_DELETE_ITEM},
            {'Update': expected_update_item}
        ])

        # Assert edit result is audited
        audit_body = copy.deepcopy(AUDIT_BODY)
        audit_body['additional_information'].update({
            'updating_next_test_due_date_from': '2023-04-01',
            'updating_next_test_due_date_to': '2023-03-20'})
        sqs_audit_client_mock.send_message.assert_called_with(
            QueueUrl='the_audit_queue_url',
            MessageBody=json.dumps(audit_body),
        )

        # Assert lambda response
        self.assertDictEqual(SUCCESS_RESPONSE, actual_response)

    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    @patch('common.utils.data_segregation.data_segregation_filters._nhais_ciphers_for_participant_ids',
           Mock(return_value=mock_filtered_participants))
    def test_PUT_edit_result_for_participant_eligible_for_autocease(self):
        participant_record = self.mock_participant_record.copy()
        participant_record['date_of_birth'] = '1945-01-01'
        participants_table_mock.get_item = Mock(side_effect=[
            {'Item': participant_record},
            {'Item': self.mock_result}
        ])
        participants_table_mock.query.side_effect = [
            {
                'Items': [{
                    'test_date': '2020-04-01',
                    'sort_key': 'RESULT#2020-04-01#',
                    'result_code': 'X',
                    'infection_code': '0',
                    'action_code': 'A',
                    'participant_id': '123456'
                }]
            },
            {'Items': []},
            {'Items': []}
        ]

        request_body = {
            'source_code': 'abcd',
            'health_authority': 'S',
            'slide_number': '249797',
            'sending_lab': '123',
            'sender_code': '456',
            'test_date': '2020-04-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'A',
            'crm_number': '140540',
            'comments': 'some free text',
            'current_next_test_due_date': '2023-04-01',
            'next_test_due_date': None,
        }
        event = self._build_event('123456', '23456789', '987654321', 'PUT', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)

        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
            call(Key={'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}),
        ])

        # Assert result record updated
        expected_key = {'participant_id': {'S': '123456'},
                        'sort_key': {'S': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}}
        update_expression = """SET #source_code = :source_code, #health_authority = :health_authority, #slide_number = :slide_number, #sending_lab = :sending_lab, #sender_code = :sender_code, #test_date = :test_date, #result_code = :result_code, #infection_code = :infection_code, #action_code = :action_code, #result = :result, #infection_result = :infection_result, #action = :action, #crm = :crm"""  
        expression_attribute_values = {
            ':source_code': {'S': 'abcd'},
            ':health_authority': {'S': 'S'},
            ':slide_number': {'S': '249797'},
            ':sending_lab': {'S': '123'},
            ':sender_code': {'S': '456'},
            ':test_date': {'S': '2020-04-01'},
            ':result_code': {'S': 'X'},
            ':infection_code': {'S': '0'},
            ':action_code': {'S': 'A'},
            ':result': {'S': 'No Cytology test undertaken'},
            ':infection_result': {'S': 'HPV negative'},
            ':action': {'S': 'Routine'},
            ':crm': {
                'L': [{
                    'M': {
                        'comments': {'S': 'some free text'},
                        'crm_number': {'S': 'CAS-12345-ABCDE'},
                        'first_name': {'S': 'firsty'},
                        'last_name': {'S': 'lasty'},
                        'timestamp': {'S': '2020-10-18T13:48:08.000000+00:00'}
                    }
                }, {
                    'M': {
                        'crm_number': {'S': '140540'},
                        'comments': {'S': 'some free text'},
                        'first_name': {'S': 'firsty'},
                        'last_name': {'S': 'lasty'},
                        'timestamp': {'S': '2020-10-18T13:48:08.000000+00:00'}
                    }
                }]
            }
        }
        expression_attribute_names = {
            '#source_code': 'source_code', '#sending_lab': 'sending_lab', '#sender_code': 'sender_code',
            '#health_authority': 'health_authority', '#slide_number': 'slide_number',
            '#test_date': 'test_date', '#result_code': 'result_code',
            '#infection_code': 'infection_code', '#action_code': 'action_code', '#result': 'result',
            '#infection_result': 'infection_result', '#action': 'action', '#crm': 'crm'
        }
        dynamo_client_mock.transact_write_items.assert_called_with(TransactItems=[{
            'Update': {
                'TableName': 'PARTICIPANTS_TABLE',
                'Key': expected_key,
                'UpdateExpression': update_expression,
                'ExpressionAttributeValues': expression_attribute_values,
                'ExpressionAttributeNames': expression_attribute_names,
            }
        }])

        # Assert edit result and cease is audited
        sqs_audit_client_mock.send_message.assert_has_calls([
            call(
                QueueUrl='the_audit_queue_url',
                MessageBody=json.dumps({
                    'action': 'MANUAL_EDIT_RESULT',
                    'timestamp': '2020-10-18T13:48:08+00:00',
                    'internal_id': 'requestID',
                    'nhsid_useruid': '23456789',
                    'session_id': '987654321',
                    'first_name': 'firsty',
                    'last_name': 'lasty',
                    'role_id': 'test_role',
                    'user_organisation_code': 'test_organisation_code',
                    'participant_ids': ['123456'],
                    'nhs_numbers': ['9999999999'],
                    'additional_information': {
                        'comments': 'some free text', 'crm_number': '140540',
                        'source_code': 'abcd', 'health_authority': 'S', 'slide_number': '249797',
                        'sending_lab': '123', 'sender_code': '456', 'test_date': '2020-04-01',
                        'result_code': 'X', 'infection_code': '0', 'action_code': 'A',
                        'result': 'No Cytology test undertaken', 'infection_result': 'HPV negative',
                        'action': 'Routine'}})
                 ),
            call(
                QueueUrl='the_audit_queue_url',
                MessageBody=json.dumps({
                    'action': 'CEASE_EPISODE', 'timestamp': '2020-10-18T13:48:08+00:00', 'internal_id': 'requestID',
                    'nhsid_useruid': 'SYSTEM', 'session_id': 'NONE', 'participant_ids': ['123456'],
                    'nhs_numbers': ['9999999999'],
                    'additional_information': {'reason': 'AUTOCEASE_DUE_TO_AGE',
                                               'updating_next_test_due_date_from': '2023-04-01'}}))
        ])

        # Assert autocease
        sqs_client_mock.send_message.assert_has_calls([
            call(QueueUrl='the_gp_notification_url',
                 MessageBody='{\n    "internal_id": "requestID",\n    "notification_origin": "CEASED",\n    "participant_id": "123456",\n    "registered_gp_practice_code": "X29"\n}'),  
            call(QueueUrl='the_notification_queue_url',
                 MessageBody='{\n    "participant_id": "123456",\n    "template": "CUA",\n    "type": "Cease letter"\n}')  
        ])

        # Assert lambda response
        self.assertDictEqual(SUCCESS_RESPONSE, actual_response)

    def test_PUT_edit_result_fails_for_participant_in_disallowed_cohort_for_user(self):
        dynamo_client_mock.transact_write_items.reset_mock()

        participants_table_mock.get_item = Mock(side_effect=[
            {'Item': {
                **self.mock_participant_record,
                'nhais_cipher': 'IM'
            }}
        ])

        participants_table_mock.query.return_value = {
                'Items': [{'test_date': '2022-01-01', 'sort_key': ParticipantSortKey.RESULT, 'participant_id': '12356'}]
            }

        request_body = {
            **REQUEST_BODY,
            'test_date': '2020-04-01',
            'current_next_test_due_date': '2023-04-01',
            'next_test_due_date': '2023-04-01',
        }
        event = self._build_event('123456', '23456789', '987654321', 'PUT', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)
        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'})
        ])

        # Assert result record updated
        dynamo_client_mock.transact_write_items.assert_not_called()

        # Assert edit result is audited
        audit_body = copy.deepcopy(AUDIT_BODY)
        audit_body['additional_information'].update({
            'test_date': '2020-04-01'
        })
        sqs_audit_client_mock.send_message.assert_not_called()

        # Assert lambda response
        self.assertDictEqual({
            **SUCCESS_RESPONSE,
            'statusCode': 404,
            'body': '{"message": "Participant does not exist"}'
        }, actual_response)

    def _build_event(
            self,
            participant_id,
            user_uid,
            session_id,
            http_method,
            workgroups,
            body=None,
            sort_key='RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
            resource='/api/participants/{participant_id}/results/{sort_key}'):
        if not body:
            body = '{}'
        return {
            'httpMethod': http_method,
            'resource': resource,
            'pathParameters': {
                'participant_id': participant_id,
                'sort_key': sort_key
            },
            'headers': {
                'request_id': 'requestID',
            },
            'body': body,
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': json.dumps({
                        "session_id": session_id,
                        "user_data": {
                            "nhsid_useruid": user_uid,
                            "first_name": "firsty",
                            "last_name": "lasty"
                        },
                        "selected_role": {
                            "workgroups": workgroups,
                            "role_id": "test_role",
                            "organisation_code": "test_organisation_code"
                        }
                    })
                }
            }
        }
