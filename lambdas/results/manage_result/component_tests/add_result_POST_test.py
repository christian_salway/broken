import uuid

from manage_result.manage_result import lambda_handler
import copy
import json
from datetime import datetime, timezone
from unittest import TestCase

from boto3.dynamodb.conditions import Key
from boto3.dynamodb.types import TypeSerializer
from manage_result.component_tests.mock_env_vars import mock_env_vars
from mock import patch, Mock, call
from common.models.letters import SupressionReason
from common.models.participant import ParticipantSortKey, ParticipantStatus

example_date = datetime(2020, 10, 18, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

FIXED_RESULT_ID = uuid.uuid4()
uuid_mock = Mock(wraps=uuid)
uuid_mock.uuid4.return_value = FIXED_RESULT_ID

participants_table_mock = Mock()
Mock()
sqs_client_audit_mock = Mock()
sqs_client_mock = Mock()
dynamo_client_mock = Mock()
dynamo_resource_mock = Mock()
check_suppression_mock = Mock()
check_suppression_mock.return_value = None
queue_letter_mock = Mock()

EXPECTED_RESPONSE = {
    'statusCode': 201,
    'headers': {
        'Content-Type': 'application/json',
        'X-Content-Type-Options': 'nosniff',
        'Strict-Transport-Security': 'max-age=1576800'
    },
    'body': '{}',
    'isBase64Encoded': False
}
AUDIT_BODY = {
    'action': 'MANUAL_ADD_RESULT',
    'timestamp': '2020-10-18T13:48:08+00:00',
    'internal_id': 'requestID',
    'nhsid_useruid': '23456789',
    'session_id': '987654321',
    'first_name': 'firsty',
    'last_name': 'lasty',
    'role_id': 'test_role',
    'user_organisation_code': 'test_organisation_code',
    'participant_ids': ['123456'],
    'nhs_numbers': ['9999999999'],
    'additional_information': {
        'comments': 'some free text', 'crm_number': '140540',
        'sending_lab': 'akdak', 'source_code': 'abcd', 'sender_code': '299205', 'health_authority': 'B',
        'slide_number': '249797', 'test_date': '2020-04-01', 'result_code': 'X', 'infection_code': '0',
        'action_code': 'A', 'hpv_primary': True, 'self_sample': False, 'result': 'No Cytology test undertaken',
        'infection_result': 'HPV negative', 'action': 'Routine'
    }
}


@patch('common.utils.datetime', datetime_mock)
@patch('common.utils.cease_utils.datetime', datetime_mock)
@patch('manage_result.manage_result_utils.datetime', datetime_mock)
@patch('common.utils.audit_utils.datetime', datetime_mock)
@patch('manage_result.manage_result_utils.uuid', uuid_mock)
@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_audit_mock))
@patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_client', Mock(return_value=dynamo_client_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_resource',
       Mock(return_value=dynamo_resource_mock))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('manage_result.add_result_service.check_for_result_letter_suppression', check_suppression_mock)
@patch('manage_result.add_result_service.add_result_letter_to_processing_queue', queue_letter_mock)
@patch('os.environ', mock_env_vars)
class TestAddResult(TestCase):
    mock_participant_record = {
        'participant_id': '123456',
        'sort_key': 'PARTICIPANT',
        'nhs_number': '9999999999',
        'date_of_birth': '1990-01-01',
        'next_test_due_date': '2021-01-01',
        'nhais_cipher': 'MAN'
    }

    request_body = {
        'sending_lab': 'akdak',
        'source_code': 'abcd',
        'sender_code': '299205',
        'health_authority': 'B',
        'slide_number': '249797',
        'test_date': '2020-04-01',
        'result_code': 'X',
        'infection_code': '0',
        'action_code': 'A',
        'hpv_primary': True,
        'self_sample': False,
        'crm_number': '140540',
        'comments': 'some free text',
    }

    def setUp(self):
        self.log_patcher = patch('manage_result.manage_result.log')
        self.util_log_patcher = patch('manage_result.manage_result_utils.log')
        self.add_result_log_patcher = patch('manage_result.add_result_service.log')
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        sqs_client_audit_mock.reset_mock()
        dynamo_client_mock.reset_mock()
        dynamo_resource_mock.reset_mock()
        queue_letter_mock.reset_mock()
        self.log_patcher.start()
        self.util_log_patcher.start()
        self.add_result_log_patcher.start()

        dynamo_resource_mock.batch_get_item.return_value = {
            "Responses": {
                "PARTICIPANTS_TABLE": [self.mock_participant_record]
            }
        }

    def tearDown(self):
        self.log_patcher.stop()
        self.util_log_patcher.stop()
        self.add_result_log_patcher.stop()

    def test_POST_add_result_for_participant(self):
        participants_table_mock.query.return_value = {
            'Items': [
                {'test_date': '2022-01-01', 'sort_key': ParticipantSortKey.RESULT, 'participant_id': '12356'}
            ]
        }
        participants_table_mock.get_item.return_value = {'Item': self.mock_participant_record}

        event = self._build_event('123456', '23456789', '987654321', 'POST', ['cervicalscreening'],
                                  json.dumps(self.request_body))

        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)

        # Assert participant record looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
        ])

        _, kwargs = participants_table_mock.get_item.call_args

        expected_key = {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        self.assertEqual(expected_key, kwargs['Key'])

        # Assert result record created
        participants_table_mock.put_item.assert_called_once()
        expected_result_record = {
            **self.expected_result_from_request(self.request_body),
            'result_id': str(FIXED_RESULT_ID),
            'suppression_reason': SupressionReason.HISTORIC_RESULT,
        }
        participants_table_mock.put_item.assert_called_with(Item=expected_result_record)
        participants_table_mock.query.assert_called_once_with(
            KeyConditionExpression=Key('participant_id').eq('123456') & Key('sort_key').begins_with('RESULT#'),
            Limit=1,
            ScanIndexForward=False)

        # Assert add result is audited
        sqs_client_audit_mock.send_message.assert_called_with(
            QueueUrl='the_audit_queue_url',
            MessageBody=json.dumps(AUDIT_BODY)
        )
        # Assert letter is not queued
        queue_letter_mock.assert_not_called()

        # Assert lambda response
        self.assertDictEqual(EXPECTED_RESPONSE, actual_response)

    def test_POST_add_result_and_update_participant(self):
        participants_table_mock.query.return_value = {
            'Items': [
                {'test_date': '2020-01-01', 'sort_key': ParticipantSortKey.RESULT, 'participant_id': '12356'}
            ]
        }
        participants_table_mock.get_item.return_value = {'Item': self.mock_participant_record}
        request_body = {
            **self.request_body,
            'next_test_due_date': '2023-04-01',
            'current_next_test_due_date': '2021-01-01',
        }
        event = self._build_event('123456', '23456789', '987654321', 'POST', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)

        # Assert participant record looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
        ])
        _, kwargs = participants_table_mock.get_item.call_args

        expected_key = {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        self.assertEqual(expected_key, kwargs['Key'])

        # Assert result record created
        expected_result_record = {
            **self.expected_result_from_request(request_body),
            'result_id': str(FIXED_RESULT_ID),
        }
        serializer = TypeSerializer()
        expected_call = [
            {
                'Put': {
                    'TableName': 'PARTICIPANTS_TABLE',
                    'Item': {k: serializer.serialize(v) for k, v in expected_result_record.items()}
                }
            },
            {
                'Update': {
                    'TableName': 'PARTICIPANTS_TABLE',
                    'Key': {
                        'participant_id': {'S': '123456'},
                        'sort_key': {'S': 'PARTICIPANT'}
                    },
                    'UpdateExpression': 'SET #status = :status, #next_test_due_date = :next_test_due_date',
                    'ExpressionAttributeValues': {
                        ':status': {'S': ParticipantStatus.ROUTINE},
                        ':next_test_due_date': {'S': '2023-04-01'}
                    },
                    'ExpressionAttributeNames': {
                        '#status': 'status',
                        '#next_test_due_date': 'next_test_due_date'
                    }
                }
            }
        ]
        dynamo_client_mock.transact_write_items.assert_called_once_with(TransactItems=expected_call)
        participants_table_mock.query.assert_called_once_with(
            KeyConditionExpression=Key('participant_id').eq('123456') & Key('sort_key').begins_with('RESULT#'),
            Limit=1,
            ScanIndexForward=False)

        # Assert add result is audited
        audit_body = copy.deepcopy(AUDIT_BODY)
        audit_body['additional_information'].update({'updating_next_test_due_date_from': '2021-01-01',
                                                     'updating_next_test_due_date_to': '2023-04-01'})
        sqs_client_audit_mock.send_message.assert_called_with(
            QueueUrl='the_audit_queue_url',
            MessageBody=json.dumps(audit_body)
        )

        queue_letter_mock.assert_called_once_with(expected_result_record, False)

        # Assert lambda response
        self.assertDictEqual(EXPECTED_RESPONSE, actual_response)

    def test_POST_add_result_with_ignored_code(self):
        participants_table_mock.query.side_effect = [
            {'Items': [{'test_date': '2020-01-01', 'sort_key': 'PARTICIPANT', 'participant_id': '123456'}]},
            {'Items': []}
        ]
        participants_table_mock.get_item.return_value = {'Item': self.mock_participant_record}
        request_body = {
            **self.request_body,
            'result_code': 'X',
            'infection_code': 'U',
            'action_code': 'H',
            'next_test_due_date': '2023-04-01',
            'current_next_test_due_date': '2021-01-01',
            'hpv_primary': True,
            'self_sample': True,
        }
        event = self._build_event('123456', '23456789', '987654321', 'POST', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)

        # Assert participant record looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
        ])
        _, kwargs = participants_table_mock.get_item.call_args

        expected_key = {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        self.assertEqual(expected_key, kwargs['Key'])

        # Assert result record created
        participants_table_mock.put_item.assert_called_once()
        expected_result_record = {
            **self.expected_result_from_request(request_body),
            'result_id': str(FIXED_RESULT_ID),
            'result': 'No Cytology test undertaken',
            'infection_result': 'HPV inadequate',
            'action': 'Record result only',
        }
        participants_table_mock.put_item.assert_called_with(Item=expected_result_record)
        dynamo_client_mock.transact_write_items.assert_not_called()
        participants_table_mock.query.assert_called_once_with(
            KeyConditionExpression=Key('participant_id').eq('123456') & Key('sort_key').begins_with('RESULT#'),
            Limit=1,
            ScanIndexForward=False)

        # Assert add result is audited
        audit_body = copy.deepcopy(AUDIT_BODY)
        audit_body['additional_information'].update({
            'infection_result': 'HPV inadequate',
            'action': 'Record result only',
            'infection_code': 'U',
            'action_code': 'H',
            'hpv_primary': True,
            'self_sample': True,
        })
        _args, audit_call_args = sqs_client_audit_mock.send_message.call_args_list[0]
        self.assertEqual('the_audit_queue_url', audit_call_args['QueueUrl'])
        self.assertDictEqual(audit_body, json.loads(audit_call_args['MessageBody']))

        queue_letter_mock.assert_called_once_with(expected_result_record, False)

        # Assert lambda response
        self.assertDictEqual(EXPECTED_RESPONSE, actual_response)

    def test_POST_add_result_given_participant_eligible_for_autocease(self):
        participants_table_mock.query.side_effect = [{
            'Items': [{'test_date': '2020-04-01', 'result_code': 'X', 'infection_code': '0', 'action_code': 'A',
                       'sort_key': 'RESULT#2020-04-01#2020-10-18T13:48:08.000000+00:00', 'participant_id': '123456'}]
        },
            {'Items': []},
            {'Items': []}
        ]
        participant = self.mock_participant_record.copy()
        participant['date_of_birth'] = '1945-01-01'
        participants_table_mock.get_item.return_value = {'Item': participant}
        request_body = {
            **self.request_body,
            'next_test_due_date': '2021-01-01',
            'current_next_test_due_date': '2021-01-01',
        }
        event = self._build_event('123456', '23456789', '987654321', 'POST', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)

        # Assert participant record looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
        ])
        _, kwargs = participants_table_mock.get_item.call_args

        expected_key = {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        self.assertEqual(expected_key, kwargs['Key'])

        # Assert result record created
        expected_result_record = {
            **self.expected_result_from_request(request_body),
            'result_id': str(FIXED_RESULT_ID),
            'suppression_reason': SupressionReason.HISTORIC_RESULT,
            'result': 'No Cytology test undertaken',
            'infection_result': 'HPV negative',
            'action': 'Routine'
        }
        participants_table_mock.put_item.assert_has_calls([
            call(Item=expected_result_record),
            call(Item={'participant_id': '123456', 'sort_key': 'CEASE#2020-10-18',
                       'reason': 'AUTOCEASE_DUE_TO_AGE', 'user_id': 'SYSTEM',
                       'date_from': '2020-10-18T13:48:08+00:00'})
        ])
        participants_table_mock.query.assert_has_calls([
            call(KeyConditionExpression=Key('participant_id').eq('123456') & Key('sort_key').begins_with('RESULT#'),
                 Limit=1, ScanIndexForward=False)
        ])

        # Assert add result and autocease is audited
        sqs_client_audit_mock.send_message.assert_has_calls([
            call(QueueUrl='the_audit_queue_url',
                 MessageBody=json.dumps({
                     'action': 'MANUAL_ADD_RESULT',
                     'timestamp': '2020-10-18T13:48:08+00:00',
                     'internal_id': 'requestID',
                     'nhsid_useruid': '23456789',
                     'session_id': '987654321',
                     'first_name': 'firsty',
                     'last_name': 'lasty',
                     'role_id': 'test_role',
                     'user_organisation_code': 'test_organisation_code',
                     'participant_ids': ['123456'],
                     'nhs_numbers': ['9999999999'],
                     'additional_information': {
                         'comments': 'some free text', 'crm_number': '140540',
                         'sending_lab': 'akdak', 'source_code': 'abcd', 'sender_code': '299205',
                         'health_authority': 'B', 'slide_number': '249797', 'test_date': '2020-04-01',
                         'result_code': 'X', 'infection_code': '0', 'action_code': 'A', 'hpv_primary': True,
                         'self_sample': False, 'result': 'No Cytology test undertaken',
                         'infection_result': 'HPV negative', 'action': 'Routine'
                     }
                 })),
            call(MessageBody=json.dumps({'action': 'CEASE_EPISODE', 'timestamp': '2020-10-18T13:48:08+00:00',
                                         'internal_id': 'requestID', 'nhsid_useruid': 'SYSTEM', 'session_id': 'NONE',
                                         'participant_ids': ['123456'], 'nhs_numbers': ['9999999999'],
                                         'additional_information': {'reason': 'AUTOCEASE_DUE_TO_AGE',
                                                                    'updating_next_test_due_date_from': '2021-01-01'}}),
                 QueueUrl='the_audit_queue_url')
        ])

        # Assert autocease occurs
        participants_table_mock.update_item.assert_called_once_with(
            ExpressionAttributeNames={'#is_ceased': 'is_ceased',
                                      '#status': 'status', '#next_test_due_date': 'next_test_due_date'},
            ExpressionAttributeValues={':is_ceased': True, ':status': ParticipantStatus.CEASED},
            Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'},
            ReturnValues='NONE',
            UpdateExpression='SET #is_ceased = :is_ceased, #status = :status REMOVE #next_test_due_date')
        sqs_client_mock.send_message.assert_called_once_with(
            MessageBody='{\n    "participant_id": "123456",\n    "template": "CUA",\n    "type": "Cease letter"\n}',
            QueueUrl='the_notification_queue_url'
        )

        queue_letter_mock.assert_not_called()

        # Assert lambda response
        expected_response = {
            'statusCode': 201,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(expected_response, actual_response)

    def test_POST_add_result_fails_for_participant_in_disallowed_cohort_for_user(self):
        participants_table_mock.query.return_value = {
            'Items': [
                {
                    'test_date': '2022-01-01',
                    'sort_key': ParticipantSortKey.RESULT,
                    'participant_id': '12356',
                }
            ]
        }
        participants_table_mock.get_item.return_value = {'Item': {
            **self.mock_participant_record,
            'nhais_cipher': 'IM'
        }}

        event = self._build_event('123456', '23456789', '987654321', 'POST', ['cervicalscreening'],
                                  json.dumps(self.request_body))

        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)

        # Assert participant record looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
        ])

        _, kwargs = participants_table_mock.get_item.call_args

        expected_key = {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        self.assertEqual(expected_key, kwargs['Key'])

        # Assert result record created
        participants_table_mock.put_item.assert_not_called()
        expected_result_record = self.expected_result_from_request(self.request_body)

        print(expected_result_record)

        participants_table_mock.put_item.assert_not_called()
        participants_table_mock.query.assert_not_called()

        # Assert add result is audited
        sqs_client_audit_mock.send_message.assert_not_called()
        queue_letter_mock.assert_not_called()

        # Assert lambda response, expected behaviour is for a participant not to exist should a user not have access
        self.assertDictEqual({
            **EXPECTED_RESPONSE,
            'statusCode': 404,
            'body': '{"message": "Participant does not exist"}'
        }, actual_response)

    def _build_event(
            self,
            participant_id,
            user_uid,
            session_id,
            http_method,
            workgroups,
            body=None,
            resource='/api/participants/{participant_id}/results'):
        if not body:
            body = '{}'
        return {
            'httpMethod': http_method,
            'resource': resource,
            'pathParameters': {
                'participant_id': participant_id
            },
            'headers': {
                'request_id': 'requestID',
            },
            'body': body,
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': json.dumps({
                        "session_id": session_id,
                        "user_data": {
                            "nhsid_useruid": user_uid,
                            "first_name": "firsty",
                            "last_name": "lasty"
                        },
                        "selected_role": {
                            "workgroups": workgroups,
                            "role_id": "test_role",
                            "organisation_code": "test_organisation_code"
                        }
                    })
                }
            }
        }

    def expected_result_from_request(self, request_body):
        return dict(
            {key: value for key, value in request_body.items() if key not in
             ['crm_number', 'comments', 'next_test_due_date', 'current_next_test_due_date']},
            **{
                'participant_id': '123456',
                'sort_key': 'RESULT#2020-04-01#2020-10-18T13:48:08.000000+00:00',
                'nhs_number': '9999999999',
                'sanitised_nhs_number': '9999999999',
                'created': '2020-10-18T13:48:08.000000+00:00',
                'crm': [
                    {
                        'crm_number': '140540',
                        'comments': 'some free text',
                        'first_name': 'firsty',
                        'last_name': 'lasty',
                        'timestamp': '2020-10-18T13:48:08.000000+00:00'
                    }
                ],
                'result': 'No Cytology test undertaken',
                'infection_result': 'HPV negative',
                'action': 'Routine'
            }
        )
