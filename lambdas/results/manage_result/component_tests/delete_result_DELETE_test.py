import json
import copy
from unittest import TestCase
from mock import patch, Mock, call
from datetime import datetime, timezone
from boto3.dynamodb.types import TypeSerializer
from manage_result.component_tests.mock_env_vars import mock_env_vars
from common.models.participant import ParticipantStatus
from manage_result.manage_result import lambda_handler

example_date = datetime(2020, 10, 18, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

sqs_client_mock = Mock()
sqs_audit_client_mock = Mock()
dynamo_db_client_mock = Mock()
participants_table_mock = Mock()
dynamo_resource_mock = Mock()

EXPECTED_RESPONSE = {
    'statusCode': 200,
    'headers': {'Content-Type': 'application/json',
                'X-Content-Type-Options': 'nosniff',
                'Strict-Transport-Security': 'max-age=1576800'},
    'body': '{}',
    'isBase64Encoded': False
}
AUDIT_BODY = {
    'action': 'MANUAL_DELETE_RESULT',
    'timestamp': '2020-10-18T13:48:08+00:00',
    'internal_id': 'requestID',
    'nhsid_useruid': '23456789',
    'session_id': '987654321',
    'first_name': 'firsty',
    'last_name': 'lasty',
    'role_id': 'test_role',
    'user_organisation_code': 'test_organisation_code',
    'participant_ids': ['123456'],
    'nhs_numbers': ['9999999999'],
    'additional_information': {
        'action': 'Routine',
        'action_code': 'A', 'infection_code': '0',
        'sending_lab': 'akdak', 'result_code': 'X', 'sender_code': '299205', 'slide_number': '249797',
        'source_code': 'abcd', 'test_date': '2020-04-01',
        'crm_number': 'CAS-56789-QWERT',
        'comments': 'Example crm comment text'
    }
}


@patch('manage_result.manage_result_utils.datetime', datetime_mock)
@patch('common.utils.audit_utils.datetime', datetime_mock)
@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_audit_client_mock))
@patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_client',
       Mock(return_value=dynamo_db_client_mock))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_resource',
       Mock(return_value=dynamo_resource_mock))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('os.environ', mock_env_vars)
class TestDeleteResult(TestCase):
    mock_participant_record = {
        'participant_id': '123456',
        'sort_key': 'PARTICIPANT',
        'nhs_number': '9999999999',
        'next_test_due_date': '2021-01-01',
        'date_of_birth': '1990-01-01',
        'nhais_cipher': 'MAN'
    }

    mock_result = {
        'extra_field': 'some extra field',
        'action': 'Routine',
        'action_code': 'A',
        'infection_code': '0',
        'sending_lab': 'akdak',
        'nhs_number': '9999999999',
        'sanitised_nhs_number': '9999999999',
        'participant_id': '123456',
        'result_code': 'X',
        'sender_code': '299205',
        'slide_number': '249797',
        'recall_months': '06',
        'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
        'source_code': 'abcd',
        'test_date': '2020-04-01',
        'crm': [
            {
                'comments': 'some free text',
                'crm_number': 'CAS-12345-ABCDE',
                'first_name': 'firsty',
                'last_name': 'lasty',
                'timestamp': '2020-10-18T13:48:08.000000+00:00'
            }
        ]
    }

    participant_records_projection_expression = 'nhs_number, sort_key, participant_id, title, first_name, ' \
                                                'middle_names, last_name, gender, address, date_of_birth, ' \
                                                'is_ceased, is_invalid_patient, next_test_due_date, ' \
                                                'registered_gp_practice_code, infection_result, ' \
                                                'infection_code, #result, #action, action_code, test_date, ' \
                                                'recall_months, result_code, sender_code, ' \
                                                'source_code, slide_number, sending_lab, #status, ' \
                                                'invited_date, reason, date_from'

    def setUp(self):
        self.maxDiff = None
        self.log_patcher = patch('manage_result.manage_result.log')
        self.util_log_patcher = patch('manage_result.manage_result_utils.log')
        self.delete_result_log_patcher = patch('manage_result.delete_result_service.log')

        self.query_side_effect = [
            {'Items': [self.mock_result, self.mock_result]},
            {'Items': []}
        ]

        participants_table_mock.reset_mock()
        participants_table_mock.get_item = Mock(side_effect=[
            {'Item': self.mock_participant_record},
            {'Item': self.mock_result}
        ])
        participants_table_mock.query.side_effect = self.query_side_effect
        sqs_audit_client_mock.reset_mock()
        sqs_client_mock.reset_mock()
        dynamo_db_client_mock.reset_mock()
        dynamo_resource_mock.reset_mock()
        self.log_patcher.start()
        self.util_log_patcher.start()
        self.delete_result_log_patcher.start()

        dynamo_resource_mock.batch_get_item.return_value = {
            "Responses": {
                "PARTICIPANTS_TABLE": [{
                    **self.mock_participant_record,
                    'role_id': 'test_role',
                    'user_organisation_code': 'test_organisation_code'
                }]
            }
        }

    def tearDown(self):
        self.log_patcher.stop()
        self.util_log_patcher.stop()
        self.delete_result_log_patcher.stop()

    def test_DELETE_delete_result_for_participant_no_ntdd_change(self):
        request_body = {'crm_number': 'CAS-56789-QWERT', 'comments': 'Example crm comment text'}
        event = self._build_event('123456', '23456789', '987654321', 'DELETE', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)
        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
            call(Key={'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}),
        ])

        # Assert result record deleted
        soft_deleted_record = self.mock_result.copy()
        soft_deleted_record['sort_key'] = 'DELETED_' + soft_deleted_record['sort_key']

        expected_key = {'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}

        serializer = TypeSerializer()
        serialized_expected_record = {k: serializer.serialize(v) for k, v in soft_deleted_record.items()}
        serialized_expected_key = {k: serializer.serialize(v) for k, v in expected_key.items()}

        dynamo_db_client_mock.transact_write_items.assert_called_with(
            TransactItems=[
                {
                    'Put': {
                        'TableName': 'PARTICIPANTS_TABLE',
                        'Item': serialized_expected_record,
                    }
                },
                {
                    'Delete': {
                        'TableName': 'PARTICIPANTS_TABLE',
                        'Key': serialized_expected_key
                    }
                }
            ])

        # Assert add result is audited
        audit_body = copy.deepcopy(AUDIT_BODY)
        _args, audit_call_args = sqs_audit_client_mock.send_message.call_args_list[0]
        self.assertEqual('the_audit_queue_url', audit_call_args['QueueUrl'])
        self.assertDictEqual(audit_body, json.loads(audit_call_args['MessageBody']))

        # Assert lambda response
        self.assertDictEqual(EXPECTED_RESPONSE, actual_response)

    def test_DELETE_delete_result_for_participant_no_ntdd_change_and_eligible_for_autocease(self):
        participant_record = self.mock_participant_record.copy()
        participant_record['date_of_birth'] = '1945-01-01'

        participants_table_mock.query.side_effect = self.query_side_effect + [{'Items': []}]

        participants_table_mock.get_item = Mock(side_effect=[
            {'Item': participant_record},
            {'Item': self.mock_result}
        ])

        request_body = {'crm_number': 'CAS-56789-QWERT', 'comments': 'Example crm comment text'}
        event = self._build_event('123456', '23456789', '987654321', 'DELETE', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)

        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
            call(Key={'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}),
        ])

        # Assert result record deleted
        soft_deleted_record = self.mock_result.copy()
        soft_deleted_record['sort_key'] = 'DELETED_' + soft_deleted_record['sort_key']

        expected_key = {'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}

        serializer = TypeSerializer()
        serialized_expected_record = {k: serializer.serialize(v) for k, v in soft_deleted_record.items()}
        serialized_expected_key = {k: serializer.serialize(v) for k, v in expected_key.items()}

        dynamo_db_client_mock.transact_write_items.assert_called_with(
            TransactItems=[
                {
                    'Put': {
                        'TableName': 'PARTICIPANTS_TABLE',
                        'Item': serialized_expected_record,
                    }
                },
                {
                    'Delete': {
                        'TableName': 'PARTICIPANTS_TABLE',
                        'Key': serialized_expected_key
                    }
                }
            ])

        # Assert add result is audited
        _args, audit_call_args = sqs_audit_client_mock.send_message.call_args_list[0]
        self.assertEqual('the_audit_queue_url', audit_call_args['QueueUrl'])
        self.assertDictEqual(AUDIT_BODY, json.loads(audit_call_args['MessageBody']))

        # Assert autocease
        participants_table_mock.update_item.assert_called_once_with(
            ExpressionAttributeNames={'#is_ceased': 'is_ceased',
                                      '#status': 'status', '#next_test_due_date': 'next_test_due_date'},
            ExpressionAttributeValues={':is_ceased': True, ':status': ParticipantStatus.CEASED},
            Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'},
            ReturnValues='NONE',
            UpdateExpression='SET #is_ceased = :is_ceased, #status = :status REMOVE #next_test_due_date')
        sqs_client_mock.send_message.assert_called_once_with(
            MessageBody='{\n    "participant_id": "123456",\n    "template": "CUA",\n    "type": "Cease letter"\n}',
            QueueUrl='the_notification_queue_url'
        )

        # Assert lambda response
        self.assertDictEqual(EXPECTED_RESPONSE, actual_response)

    def test_DELETE_delete_result_for_participant_with_ntdd_change(self):
        request_body = {'next_test_due_date': '2023-04-01',
                        'crm_number': 'CAS-56789-QWERT',
                        'comments': 'Example crm comment text'}
        event = self._build_event('123456', '23456789', '987654321', 'DELETE', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)
        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
            call(Key={'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}),
        ])

        # Assert result record deleted
        soft_deleted_record = self.mock_result.copy()
        soft_deleted_record['sort_key'] = 'DELETED_' + soft_deleted_record['sort_key']

        expected_key = {'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}

        serializer = TypeSerializer()
        serialized_expected_record = {k: serializer.serialize(v) for k, v in soft_deleted_record.items()}
        serialized_expected_key = {k: serializer.serialize(v) for k, v in expected_key.items()}

        dynamo_db_client_mock.transact_write_items.assert_called_with(
            TransactItems=[
                {
                    'Put': {
                        'TableName': 'PARTICIPANTS_TABLE',
                        'Item': serialized_expected_record,
                    }
                },
                {
                    'Delete': {
                        'TableName': 'PARTICIPANTS_TABLE',
                        'Key': serialized_expected_key
                    }
                },
                {
                    'Update': {
                        'TableName': 'PARTICIPANTS_TABLE',
                        'Key': {
                            'participant_id': {'S': '123456'},
                            'sort_key': {'S': 'PARTICIPANT'}
                        },
                        'UpdateExpression': 'SET #next_test_due_date = :next_test_due_date, #status = :status',
                        'ExpressionAttributeValues': {
                            ':next_test_due_date': {'S': '2023-04-01'},
                            ':status': {'S': ParticipantStatus.ROUTINE},
                        },
                        'ExpressionAttributeNames': {
                            '#next_test_due_date': 'next_test_due_date',
                            '#status': 'status',
                        }
                    }
                }
            ])

        # Assert add result is audited
        _args, audit_call_args = sqs_audit_client_mock.send_message.call_args_list[0]
        self.assertEqual('the_audit_queue_url', audit_call_args['QueueUrl'])
        audit_body = copy.deepcopy(AUDIT_BODY)
        audit_body['additional_information'].update({
            'updating_next_test_due_date_from': '2021-01-01',
            'updating_next_test_due_date_to': '2023-04-01'
        })
        self.assertDictEqual(audit_body, json.loads(audit_call_args['MessageBody']))

        # Assert lambda response
        self.assertDictEqual(EXPECTED_RESPONSE, actual_response)

    def test_DELETE_delete_result_where_next_ignored(self):
        request_body = {'next_test_due_date': '2023-03-30',
                        'crm_number': 'CAS-56789-QWERT',
                        'comments': 'Example crm comment text'}
        event = self._build_event('123456', '23456789', '987654321', 'DELETE', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        ignored_result = {
            **self.mock_result,
            'test_date': '2020-03-31',
            'result_code': 'X',
            'infection_code': 'U',
            'action_code': 'H',
            'hpv_primary': True,
            'self_sample': True,
        }
        older_result = {
            **self.mock_result,
            'test_date': '2020-03-30',
        }
        participants_table_mock.query.side_effect = [
            {'Items': [self.mock_result, ignored_result, older_result]}
        ]

        actual_response = lambda_handler(event, context)
        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
            call(Key={'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}),
        ])

        # Assert result record deleted
        soft_deleted_record = self.mock_result.copy()
        soft_deleted_record['sort_key'] = 'DELETED_' + soft_deleted_record['sort_key']

        expected_key = {'participant_id': '123456', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}

        serializer = TypeSerializer()
        serialized_expected_record = {k: serializer.serialize(v) for k, v in soft_deleted_record.items()}
        serialized_expected_key = {k: serializer.serialize(v) for k, v in expected_key.items()}

        dynamo_db_client_mock.transact_write_items.assert_called_with(
            TransactItems=[
                {
                    'Put': {
                        'TableName': 'PARTICIPANTS_TABLE',
                        'Item': serialized_expected_record,
                    }
                },
                {
                    'Delete': {
                        'TableName': 'PARTICIPANTS_TABLE',
                        'Key': serialized_expected_key
                    }
                },
                {
                    'Update': {
                        'TableName': 'PARTICIPANTS_TABLE',
                        'Key': {
                            'participant_id': {'S': '123456'},
                            'sort_key': {'S': 'PARTICIPANT'}
                        },
                        'UpdateExpression': 'SET #next_test_due_date = :next_test_due_date, #status = :status',
                        'ExpressionAttributeValues': {
                            ':next_test_due_date': {'S': '2023-03-30'},
                            ':status': {'S': ParticipantStatus.ROUTINE},
                        },
                        'ExpressionAttributeNames': {
                            '#next_test_due_date': 'next_test_due_date',
                            '#status': 'status',
                        }
                    }
                }
            ])

        # Assert add result is audited
        _args, audit_call_args = sqs_audit_client_mock.send_message.call_args_list[0]
        self.assertEqual('the_audit_queue_url', audit_call_args['QueueUrl'])
        audit_body = copy.deepcopy(AUDIT_BODY)
        audit_body['additional_information'].update({
            'updating_next_test_due_date_from': '2021-01-01',
            'updating_next_test_due_date_to': '2023-03-30'
        })
        self.assertDictEqual(audit_body, json.loads(audit_call_args['MessageBody']))

        # Assert lambda response
        self.assertDictEqual(EXPECTED_RESPONSE, actual_response)

    def test_DELETE_delete_result_fails_for_participant_in_disallowed_cohort_for_user(self):
        request_body = dict()
        event = self._build_event('123456', '23456789', '987654321', 'DELETE', ['cervicalscreening'],
                                  json.dumps(request_body))
        context = Mock()
        context.function_name = ''

        participants_table_mock.get_item = Mock(return_value={'Item': {
            **self.mock_participant_record,
            'nhais_cipher': 'IM'
        }})

        actual_response = lambda_handler(event, context)
        # Assert participant and test result looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
        ])

        # Assert the soft delete write was not called
        dynamo_db_client_mock.transact_write_items.assert_not_called()

        # Assert lambda response, expected behaviour is for a participant not to exist should a user not have access
        self.assertDictEqual({
            **EXPECTED_RESPONSE,
            'statusCode': 404,
            'body': '{"message": "Participant does not exist"}'
        }, actual_response)

    def _build_event(
            self,
            participant_id,
            user_uid,
            session_id,
            http_method,
            workgroups,
            body=None,
            sort_key='RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
            resource='/api/participants/{participant_id}/results/{sort_key}'):
        if not body:
            body = '{}'
        return {
            'httpMethod': http_method,
            'resource': resource,
            'pathParameters': {
                'participant_id': participant_id,
                'sort_key': sort_key
            },
            'headers': {
                'request_id': 'requestID',
            },
            'body': body,
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': json.dumps({
                        "session_id": session_id,
                        "user_data": {
                            "nhsid_useruid": user_uid,
                            "first_name": "firsty",
                            "last_name": "lasty"
                        },
                        "selected_role": {
                            "workgroups": workgroups,
                            "role_id": "test_role",
                            "organisation_code": "test_organisation_code"
                        }
                    })
                }
            }
        }
