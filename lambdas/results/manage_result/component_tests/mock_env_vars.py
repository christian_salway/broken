mock_env_vars = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE',
    'AUDIT_QUEUE_URL': 'the_audit_queue_url',
    'NOTIFICATION_QUEUE_URL': 'the_notification_queue_url',
    'GP_NOTIFICATION_QUEUE_URL': 'the_gp_notification_url'
}
