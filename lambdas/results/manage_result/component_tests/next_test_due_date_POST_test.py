import json
from unittest import TestCase
from mock import patch, Mock, call, MagicMock
from datetime import datetime, timezone
from boto3.dynamodb.conditions import Key
from manage_result.manage_result import lambda_handler

example_date = datetime(2020, 10, 18, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

mock_env_vars = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS_TABLE',
}

participants_table_mock = MagicMock()
dynamo_resource_mock = Mock()


@patch('manage_result.manage_result_utils.datetime', datetime_mock)
@patch('common.utils.audit_utils.datetime', datetime_mock)
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(return_value=participants_table_mock))
@patch('common.utils.next_test_due_date_calculators.next_test_due_date_calculator.datetime',
       datetime_mock)
@patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_resource',
       Mock(return_value=dynamo_resource_mock))
@patch('os.environ', mock_env_vars)
class TestGetNextTestDueDate(TestCase):

    mock_participant_record = {
        'participant_id': '123456',
        'sort_key': 'PARTICIPANT',
        'nhs_number': '9999999999',
        'date_of_birth': '1990-03-16',
        'next_test_due_date': '2021-02-01',
    }

    mock_result_record = {
        'test_date': '2021-01-01',
    }

    mock_result_records = [{
        'sort_key': 'RESULT#2021-03-01',
        'test_date': '2020-12-01',
        'participant_id': '123456'
    }
    ]

    def setUp(self):
        self.log_patcher = patch('manage_result.manage_result.log')
        self.util_log_patcher = patch('manage_result.manage_result_utils.log')
        self.next_test_due_date_log_patcher = patch('manage_result.next_test_due_date_service.log')
        participants_table_mock.reset_mock()
        dynamo_resource_mock.reset_mock()
        self.log_patcher.start()
        self.util_log_patcher.start()
        self.next_test_due_date_log_patcher.start()

        dynamo_resource_mock.batch_get_item.return_value = {
            "Responses": {
                "PARTICIPANTS_TABLE": [self.mock_participant_record]
            }
        }

    def tearDown(self):
        self.log_patcher.stop()
        self.util_log_patcher.stop()
        self.next_test_due_date_log_patcher.stop()

    def test_POST_next_test_due_date_for_participant(self):
        self.maxDiff = None
        participants_table_mock.get_item.return_value = {'Item': self.mock_participant_record}
        participants_table_mock.query.return_value = {'Items': self.mock_result_records}
        body = {
            'test_date': '2021-01-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'R',
            'sort_key': 'RESULT#2021-03-01',
            'hpv_primary': True,
            'self_sample': False
        }
        event = self._build_event('123456', ['cervicalscreening'], json.dumps(body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)

        # Assert participant record looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
        ])

        _, kwargs = participants_table_mock.get_item.call_args

        expected_key = {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        self.assertEqual(expected_key, kwargs['Key'])

        # Assert result record looked up from DynamoDB
        participants_table_mock.query.assert_has_calls([
            call(KeyConditionExpression=Key('participant_id').eq('123456') & Key('sort_key').begins_with('RESULT#'),
                 Limit=2,
                 ScanIndexForward=False)
        ])

        # Assert lambda response
        expected_body = {
            'data': {
                'default': {
                    'interval': 36,
                    'next_test_due_date': '2024-01-01',
                },
                'options': [{
                    'interval': 12,
                    'next_test_due_date': '2022-01-01',
                }, {
                    'interval': 6,
                    'next_test_due_date': '2021-07-01',
                }],
                'range': None,
                'current': '2021-02-01',
            }
        }
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': json.dumps(expected_body),
            'isBase64Encoded': False
        }
        self.assertDictEqual(expected_response, actual_response)

    def test_POST_next_test_fails_for_participant_in_disallowed_cohort_for_user(self):
        self.maxDiff = None
        participants_table_mock.get_item.return_value = {'Item': {
            **self.mock_participant_record,
            'nhais_cipher': 'IM'
        }}
        participants_table_mock.query.return_value = {'Items': self.mock_result_records}
        body = {
            'test_date': '2021-01-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'R',
            'sort_key': 'RESULT#2021-03-01',
        }
        event = self._build_event('123456', ['cervicalscreening'], json.dumps(body))
        context = Mock()
        context.function_name = ''

        actual_response = lambda_handler(event, context)

        # Assert participant record looked up from DynamoDB
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'}),
        ])

        _, kwargs = participants_table_mock.get_item.call_args

        expected_key = {'participant_id': '123456', 'sort_key': 'PARTICIPANT'}
        self.assertEqual(expected_key, kwargs['Key'])

        # Assert result record looked up from DynamoDB
        participants_table_mock.query.assert_not_called()

        # Assert lambda response, expected behaviour is for a participant not to exist should a user not have access
        self.assertDictEqual({
            'statusCode': 404,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Participant does not exist"}',
            'isBase64Encoded': False
        }, actual_response)

    def _build_event(
            self,
            participant_id,
            workgroups,
            body):
        return {
            'httpMethod': 'POST',
            'resource': '/api/participants/{participant_id}/results/next-test-due-date',
            'pathParameters': {
                'participant_id': participant_id
            },
            'headers': {
                'request_id': 'requestID',
            },
            'body': body,
            'requestContext': {
                'authorizer': {
                    'principalId': 'blah',
                    'session': json.dumps({
                        "session_id": '987654321',
                        "user_data": {
                            "nhsid_useruid": '23456789',
                            "first_name": "firsty",
                            "last_name": "lasty"
                        },
                        "selected_role": {
                            "workgroups": workgroups,
                            "role_id": "test_role",
                            "organisation_code": "test_organisation_code"
                        }
                    })
                }
            }
        }
