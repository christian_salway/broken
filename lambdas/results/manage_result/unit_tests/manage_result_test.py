from unittest import TestCase
from mock import patch, call
from common.utils import json_return_object
from common.test_assertions.api_assertions import assertApiResponse
from manage_result.log_references import LogReference
from manage_result import manage_result


PARTICIPANT_ID = '123'
NHS_NUMBER = '9999999999'
PARTICIPANT = {
    'participant_id': PARTICIPANT_ID,
    'nhs_number': NHS_NUMBER
}
REQUEST_BODY = {
    'sending_lab': 'akdak',
    'source_code': 'abcd',
    'sender_code': '299205',
    'slide_number': '249797',
    'test_date': '2020-04-01',
    'result_code': 'X',
    'infection_code': '0',
    'action_code': 'A',
    'crm_number': '140540',
    'comments': 'some free text',
    'unexpected_field': 'unexpected_value'
}
SESSION = {'session_key': '1234567890'}


class TestLambdaHandler(TestCase):

    module = 'manage_result.manage_result'

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.add_result')
    def test_manage_result_add_result(
            self,
            add_result_mock,
            request_info_mock,
            get_participant_mock,
            log_mock):
        request_info_mock.return_value = post_request_info(REQUEST_BODY)
        get_participant_mock.return_value = {'Item': PARTICIPANT}
        add_result_mock.return_value = json_return_object(201)

        actual_response = manage_result.lambda_handler.__wrapped__.__wrapped__({}, {})

        assertApiResponse(self, 201, {}, actual_response)
        get_participant_mock.assert_called_with('123', segregate=True)

        expected_log_calls = [
            call({'log_reference': LogReference.MANAGERESULT001, 'request_keys': list(REQUEST_BODY.keys())}),
        ]
        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.edit_result')
    def test_manage_result_edit_result(
            self,
            edit_result_mock,
            request_info_mock,
            get_participant_mock,
            log_mock):
        request_info_mock.return_value = put_delete_request_info(REQUEST_BODY, 'PUT')
        get_participant_mock.return_value = {'Item': PARTICIPANT}
        edit_result_mock.return_value = json_return_object(200)

        actual_response = manage_result.lambda_handler.__wrapped__.__wrapped__({}, {})

        assertApiResponse(self, 200, {}, actual_response)
        get_participant_mock.assert_called_with('123', segregate=True)

        expected_log_calls = [
            call({'log_reference': LogReference.MANAGERESULT001, 'request_keys': list(REQUEST_BODY.keys())}),
        ]
        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.delete_result')
    def test_manage_result_delete_result(
            self,
            delete_result_mock,
            request_info_mock,
            get_participant_mock,
            log_mock):
        request_info_mock.return_value = put_delete_request_info(REQUEST_BODY, 'DELETE')
        get_participant_mock.return_value = {'Item': PARTICIPANT}
        delete_result_mock.return_value = json_return_object(200)

        actual_response = manage_result.lambda_handler.__wrapped__.__wrapped__({}, {})

        assertApiResponse(self, 200, {}, actual_response)
        get_participant_mock.assert_called_with('123', segregate=True)

        expected_log_calls = [
            call({'log_reference': LogReference.MANAGERESULT001, 'request_keys': list(REQUEST_BODY.keys())}),
        ]
        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_manage_result_unknown_participant(
            self, request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = post_request_info(REQUEST_BODY)
        get_participant_mock.return_value = {}

        actual_response = manage_result.lambda_handler.__wrapped__.__wrapped__({}, {})

        assertApiResponse(self, 404, {'message': 'Participant does not exist'}, actual_response)
        get_participant_mock.assert_called_with('123', segregate=True)

        expected_log_calls = [
            call({'log_reference': LogReference.MANAGERESULT001, 'request_keys': list(REQUEST_BODY.keys())}),
            call(LogReference.MANAGERESULT005),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.get_participant_by_participant_id')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_manage_result_bad_request(self, request_info_mock, get_participant_mock, log_mock):
        request_info_mock.return_value = {
            'path_parameters': {'participant_id': '123'},
            'body': REQUEST_BODY,
            'session': SESSION,
        }, 'INVALID', '/api/participants/{participant_id}/results'
        get_participant_mock.return_value = {'Item': PARTICIPANT}

        actual_response = manage_result.lambda_handler.__wrapped__.__wrapped__({}, {})

        assertApiResponse(self, 400, {'message': 'unrecognised method/resource combination'}, actual_response)


def post_request_info(request_body):
    return {
        'path_parameters': {'participant_id': '123'},
        'body': request_body,
        'session': SESSION
    }, 'POST', '/api/participants/{participant_id}/results'


def put_delete_request_info(request_body, http_method):
    return {
        'path_parameters': {
            'participant_id': '123',
            'sort_key': 'RESULT_SORT_KEY'
            },
        'body': request_body,
        'session': SESSION
    }, http_method, '/api/participants/{participant_id}/results/{sort_key}'
