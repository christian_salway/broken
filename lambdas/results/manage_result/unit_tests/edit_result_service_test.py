from datetime import date
from unittest import TestCase
from urllib.parse import quote

from ddt import ddt, data, unpack
from mock import patch, call, Mock

from manage_result import edit_result_service
from common.models.participant import ParticipantStatus
from common.test_assertions.api_assertions import assertApiResponse
from common.utils import audit_utils
from manage_result.log_references import LogReference
from manage_result.unit_tests.manage_result_utils_test import (
    PARTICIPANT_ID,
    PARTICIPANT,
    SESSION,
    REQUEST_BODY,
    EXPECTED_RESULT,
)

log_mock = Mock()
audit_mock = Mock()
validate_mandatory_fields_mock = Mock()
get_recent_results_mock = Mock()
create_result_record_mock = Mock()
generate_crm_mock = Mock()
module = 'manage_result.edit_result_service'


@ddt
@patch(f'{module}.log', log_mock)
@patch(f'{module}.audit', audit_mock)
@patch(f'{module}.validate_mandatory_fields', validate_mandatory_fields_mock)
@patch(f'{module}.get_recent_results_by_participant_id', get_recent_results_mock)
@patch(f'{module}.create_result_record', create_result_record_mock)
@patch(f'{module}.generate_crm_list', generate_crm_mock)
class TestEditResultService(TestCase):

    def setUp(self):
        log_mock.reset_mock()
        audit_mock.reset_mock()
        validate_mandatory_fields_mock.reset_mock()
        create_result_record_mock.reset_mock()
        generate_crm_mock.reset_mock()
        self.log_patcher = patch('common.log.logger').start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.validate_edit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.check_if_test_date_change')
    def test_edit_result_unchanged_test_date(
            self,
            test_date_change_mock,
            get_test_result_mock,
            validate_edit_mock,
            transaction_write_records_mock,
            autocease_mock
    ):
        request_body = REQUEST_BODY.copy()

        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': quote('RESULT#TEST_DATE#CREATED_DATE')
            },
            'session': SESSION
        }
        validate_edit_mock.return_value = True, {}
        test_date_change_mock.return_value = False
        get_test_result_mock.return_value = EXPECTED_RESULT
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']
        create_result_record_mock.return_value = result_record_mock
        generate_crm_mock.return_value = [
            {
                'crm_number': '', 'comments': '', 'first_name': 'firsty',
                'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            },
            {
                'crm_number': '140540', 'comments': 'some free text',
                'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            }
        ]
        latest_test = {
            'sort_key': 'NOMATCH',
            'test_date': '2021-01-01',
        }
        previous_test = None
        get_recent_results_mock.return_value = [latest_test, previous_test]

        actual_response = edit_result_service.edit_result(event_data, PARTICIPANT)

        assertApiResponse(self, 200, {}, actual_response)
        get_test_result_mock.assert_called_with('123', 'RESULT#TEST_DATE#CREATED_DATE', segregate=True)
        create_result_record_mock.assert_not_called()
        expected_key = {'participant_id': '123', 'sort_key': 'RESULT#TEST_DATE#CREATED_DATE'}
        expected_update = {
            'sending_lab': 'akdak', 'source_code': 'abcd',
            'sender_code': '299205', 'slide_number': '249797',
            'test_date': '2020-04-01', 'result_code': 'X', 'infection_code': '0',
            'action_code': 'A', 'action': 'Routine', 'result': 'No Cytology test undertaken',
            'infection_result': 'HPV negative', 'health_authority': 'B', 'recall_months': '36',
            'crm': [
                {
                    'crm_number': '', 'comments': '', 'first_name': 'firsty',
                    'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
                },
                {
                    'crm_number': '140540', 'comments': 'some free text',
                    'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
                }
            ]
        }
        transaction_write_records_mock.assert_called_once_with([], [], [{
            'record_keys': expected_key,
            'update_values': expected_update,
            'delete_values': []
        }], segregate=True)

        expected_log_calls = [
            call(LogReference.MANAGERESULT012),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_EDIT_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'action': 'Routine', 'action_code': 'A', 'comments': 'some free text',
                                        'crm_number': '140540', 'infection_code': '0', 'health_authority': 'B',
                                        'infection_result': 'HPV negative', 'sending_lab': 'akdak',
                                        'result': 'No Cytology test undertaken', 'result_code': 'X',
                                        'sender_code': '299205', 'slide_number': '249797', 'source_code': 'abcd',
                                        'test_date': '2020-04-01'}
            )
        ])
        autocease_mock.assert_called_once_with(PARTICIPANT, date(2021, 2, 1))

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.generate_results_dict', Mock())
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.validate_edit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.check_if_test_date_change')
    def test_edit_result_changed_test_date_when_sort_key_not_matched_and_date_more_recent_than_latest_result_date(
            self,
            test_date_change_mock,
            get_test_result_mock,
            validate_edit_mock,
            transaction_write_records_mock,
            autocease_mock
    ):
        request_body = REQUEST_BODY.copy()
        request_body.update({
            'next_test_due_date': '2023-04-01',
            'current_next_test_due_date': '2021-02-01',
        })
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT#TEST_DATE#CREATED_DATE'
            },
            'session': SESSION
        }

        validate_edit_mock.return_value = True, {}
        test_date_change_mock.return_value = True
        get_test_result_mock.return_value = EXPECTED_RESULT
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']
        create_result_record_mock.return_value = result_record_mock
        generate_crm_mock.return_value = [
            {
                'crm_number': '', 'comments': '', 'first_name': 'firsty',
                'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            },
            {
                'crm_number': '140540', 'comments': 'some free text',
                'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            }
        ]
        latest_test = {
            'sort_key': 'NOMATCH',
            'test_date': '2019-01-01',
        }
        previous_test = None
        get_recent_results_mock.return_value = [latest_test, previous_test]

        actual_response = edit_result_service.edit_result(event_data, PARTICIPANT)

        assertApiResponse(self, 200, {}, actual_response)
        get_test_result_mock.assert_called_with('123', 'RESULT#TEST_DATE#CREATED_DATE', segregate=True)
        create_result_record_mock.assert_called_once()
        transaction_write_records_mock.assert_called_once()

        expected_log_calls = [
            call(LogReference.MANAGERESULT012),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_EDIT_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'comments': 'some free text', 'crm_number': '140540',
                                        'action': 'Routine', 'action_code': 'A',
                                        'infection_code': '0', 'infection_result': 'HPV negative',
                                        'sending_lab': 'akdak', 'result': 'No Cytology test undertaken',
                                        'result_code': 'X', 'sender_code': '299205', 'slide_number': '249797',
                                        'source_code': 'abcd', 'test_date': '2020-04-01', 'health_authority': 'B',
                                        'updating_next_test_due_date_from': '2021-02-01',
                                        'updating_next_test_due_date_to': '2023-04-01'}
            )
        ])
        autocease_mock.assert_called_once_with({'participant_id': '123', 'nhs_number': '9999999999',
                                                'date_of_birth': '1990-01-01', 'next_test_due_date': '2021-02-01',
                                                'is_ceased': False},
                                               date(2023, 4, 1))

    @patch(f'{module}.generate_results_dict', Mock())
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.validate_edit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.check_if_test_date_change')
    def test_edit_result_changed_test_date_when_sort_key_matched_and_no_previous_result(
            self,
            test_date_change_mock,
            get_test_result_mock,
            validate_edit_mock,
            transaction_write_records_mock):
        request_body = REQUEST_BODY.copy()
        request_body.update({
            'next_test_due_date': '2023-04-01',
            'current_next_test_due_date': '2021-02-01',
        })
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT#TEST_DATE#CREATED_DATE'
            },
            'session': SESSION
        }

        validate_edit_mock.return_value = True, {}
        test_date_change_mock.return_value = True
        get_test_result_mock.return_value = EXPECTED_RESULT
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']
        create_result_record_mock.return_value = result_record_mock
        generate_crm_mock.return_value = [
            {
                'crm_number': '', 'comments': '', 'first_name': 'firsty',
                'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            },
            {
                'crm_number': '140540', 'comments': 'some free text',
                'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            }
        ]
        latest_test = {
            'sort_key': 'RESULT#TEST_DATE#CREATED_DATE',
            'test_date': '2020-01-01',
        }
        previous_test = None
        get_recent_results_mock.return_value = [latest_test, previous_test]

        actual_response = edit_result_service.edit_result(event_data, PARTICIPANT)

        assertApiResponse(self, 200, {}, actual_response)
        get_test_result_mock.assert_called_with('123', 'RESULT#TEST_DATE#CREATED_DATE', segregate=True)
        create_result_record_mock.assert_called_once()
        transaction_write_records_mock.assert_called_once()

        expected_log_calls = [
            call(LogReference.MANAGERESULT012),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_EDIT_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'comments': 'some free text', 'crm_number': '140540',
                                        'action': 'Routine', 'action_code': 'A',
                                        'infection_code': '0', 'infection_result': 'HPV negative',
                                        'sending_lab': 'akdak', 'result': 'No Cytology test undertaken',
                                        'result_code': 'X', 'sender_code': '299205', 'slide_number': '249797',
                                        'source_code': 'abcd', 'test_date': '2020-04-01', 'health_authority': 'B',
                                        'updating_next_test_due_date_from': '2021-02-01',
                                        'updating_next_test_due_date_to': '2023-04-01'}
            )
        ])

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.generate_results_dict', Mock())
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.validate_edit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.check_if_test_date_change')
    def test_edit_result_changed_test_date_when_sort_key_matched_and_date_more_recent_than_previous_result_date(
            self,
            test_date_change_mock,
            get_test_result_mock,
            validate_edit_mock,
            transaction_write_records_mock,
            autocease_mock
    ):
        request_body = REQUEST_BODY.copy()
        request_body.update({
            'next_test_due_date': '2023-04-01',
            'current_next_test_due_date': '2021-02-01',
        })
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT#TEST_DATE#CREATED_DATE'
            },
            'session': SESSION
        }

        validate_edit_mock.return_value = True, {}
        test_date_change_mock.return_value = True
        get_test_result_mock.return_value = EXPECTED_RESULT
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']
        create_result_record_mock.return_value = result_record_mock
        generate_crm_mock.return_value = [
            {
                'crm_number': '', 'comments': '', 'first_name': 'firsty',
                'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            },
            {
                'crm_number': '140540', 'comments': 'some free text',
                'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            }
        ]
        latest_test = {
            'sort_key': 'RESULT#TEST_DATE#CREATED_DATE',
            'test_date': '2020-01-01',
        }
        previous_test = {
            'sort_key': 'Previous',
            'test_date': '2019-01-01',
        }
        get_recent_results_mock.return_value = [latest_test, previous_test]

        actual_response = edit_result_service.edit_result(event_data, PARTICIPANT)

        assertApiResponse(self, 200, {}, actual_response)
        get_test_result_mock.assert_called_with('123', 'RESULT#TEST_DATE#CREATED_DATE', segregate=True)
        create_result_record_mock.assert_called_once()
        transaction_write_records_mock.assert_called_once()

        expected_log_calls = [
            call(LogReference.MANAGERESULT012),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_EDIT_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'comments': 'some free text', 'crm_number': '140540',
                                        'action': 'Routine', 'action_code': 'A',
                                        'infection_code': '0', 'infection_result': 'HPV negative',
                                        'sending_lab': 'akdak', 'result': 'No Cytology test undertaken',
                                        'result_code': 'X', 'sender_code': '299205', 'slide_number': '249797',
                                        'source_code': 'abcd', 'test_date': '2020-04-01', 'health_authority': 'B',
                                        'updating_next_test_due_date_from': '2021-02-01',
                                        'updating_next_test_due_date_to': '2023-04-01'}
            )
        ])
        autocease_mock.assert_called_once_with(PARTICIPANT, date(2023, 4, 1))

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.generate_results_dict', Mock())
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.validate_edit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.check_if_test_date_change')
    def test_edit_result_changed_test_date_when_sort_key_matched_and_date_older_or_equal_to_previous_result_date(
            self,
            test_date_change_mock,
            get_test_result_mock,
            validate_edit_mock,
            transaction_write_records_mock,
            autocease_mock
    ):
        request_body = REQUEST_BODY.copy()
        request_body.update({
            'test_date': '2020-01-01',
            'next_test_due_date': '2023-02-01',
            'current_next_test_due_date': '2021-02-01',
        })
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT#TEST_DATE#CREATED_DATE'
            },
            'session': SESSION
        }

        validate_edit_mock.return_value = True, {}
        test_date_change_mock.return_value = True
        get_test_result_mock.return_value = EXPECTED_RESULT
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']
        create_result_record_mock.return_value = result_record_mock
        generate_crm_mock.return_value = [
            {
                'crm_number': '', 'comments': '', 'first_name': 'firsty',
                'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            },
            {
                'crm_number': '140540', 'comments': 'some free text',
                'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            }
        ]
        latest_test = {
            'sort_key': 'RESULT#TEST_DATE#CREATED_DATE',
            'test_date': '2020-03-01',
        }
        previous_test = {
            'sort_key': 'Previous',
            'test_date': '2020-02-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'A',
        }
        get_recent_results_mock.return_value = [latest_test, previous_test]

        actual_response = edit_result_service.edit_result(event_data, PARTICIPANT)

        assertApiResponse(self, 200, {}, actual_response)
        get_test_result_mock.assert_called_with('123', 'RESULT#TEST_DATE#CREATED_DATE', segregate=True)
        create_result_record_mock.assert_called_once()
        transaction_write_records_mock.assert_called_once()

        expected_log_calls = [
            call(LogReference.MANAGERESULT012),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_EDIT_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'comments': 'some free text', 'crm_number': '140540',
                                        'action': 'Routine', 'action_code': 'A',
                                        'infection_code': '0', 'infection_result': 'HPV negative',
                                        'sending_lab': 'akdak', 'result': 'No Cytology test undertaken',
                                        'result_code': 'X', 'sender_code': '299205', 'slide_number': '249797',
                                        'source_code': 'abcd', 'test_date': '2020-04-01', 'health_authority': 'B',
                                        'updating_next_test_due_date_from': '2021-02-01',
                                        'updating_next_test_due_date_to': '2023-02-01'}
            )
        ])
        autocease_mock.assert_called_once_with(PARTICIPANT, date(2023, 2, 1))

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.generate_results_dict', Mock())
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.validate_edit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.check_if_test_date_change')
    def test_edit_result_invalid_result(
            self,
            test_date_change_mock,
            get_test_result_mock,
            validate_edit_mock,
            transaction_write_records_mock,
            autocease_mock
    ):
        request_body = REQUEST_BODY.copy()
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT#TEST_DATE#CREATED_DATE'
            },
            'session': SESSION
        }
        latest_test = {
            'sort_key': 'NOMATCH',
            'test_date': '2021-01-01',
        }
        previous_test = None
        get_recent_results_mock.return_value = [latest_test, previous_test]

        validate_edit_mock.return_value = False, {'statusCode': 400, 'body': 'SOME_ERROR'}
        test_date_change_mock.return_value = False
        get_test_result_mock.return_value = EXPECTED_RESULT

        actual_response = edit_result_service.edit_result(event_data, PARTICIPANT)

        self.assertDictEqual(actual_response, {'statusCode': 400, 'body': 'SOME_ERROR'})
        get_test_result_mock.assert_called_with('123', 'RESULT#TEST_DATE#CREATED_DATE', segregate=True)
        create_result_record_mock.assert_not_called()
        transaction_write_records_mock.assert_not_called()

        log_mock.assert_has_calls([call(LogReference.MANAGERESULT003)])

        audit_mock.assert_not_called()
        autocease_mock.assert_not_called()

    def test_validate_edit(self):
        existing_result = EXPECTED_RESULT.copy()
        request_body = REQUEST_BODY.copy()
        validate_mandatory_fields_mock.return_value = True, None

        actual_is_valid_bool, actual_response = edit_result_service.validate_edit(existing_result, request_body)

        self.assertEqual(None, actual_response)
        self.assertTrue(actual_is_valid_bool)
        log_mock.assert_not_called()

    def test_validate_edit_invalid_fields(self):
        existing_result = EXPECTED_RESULT.copy()
        request_body = REQUEST_BODY.copy()
        validate_mandatory_fields_mock.return_value = False, {'statusCode': 400, 'body': 'SOME_ERROR'}

        actual_is_valid_bool, actual_response = edit_result_service.validate_edit(existing_result, request_body)

        self.assertDictEqual(actual_response, {'statusCode': 400, 'body': 'SOME_ERROR'})
        self.assertFalse(actual_is_valid_bool)

        log_mock.assert_not_called()

    def test_validate_edit_no_existing_result(self):
        existing_result = None
        request_body = REQUEST_BODY.copy()
        validate_mandatory_fields_mock.return_value = True, {}

        actual_is_valid_bool, actual_response = edit_result_service.validate_edit(existing_result, request_body)

        assertApiResponse(self, 404, {'message': 'Result does not exist'}, actual_response)
        self.assertFalse(actual_is_valid_bool)

        expected_log_calls = [
            call(LogReference.MANAGERESULT013),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @unpack
    @data(('2020-04-01', False), ('2020-04-02', True), ('2019-04-01', True))
    def test_check_if_test_date_change(self, new_date, expected_result):
        request_body = REQUEST_BODY.copy()
        existing_result = EXPECTED_RESULT.copy()
        request_body['test_date'] = new_date

        actual_result = edit_result_service.check_if_test_date_change(existing_result, request_body)
        self.assertEqual(expected_result, actual_result)

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.should_update_next_test_due_date')
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.validate_edit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.check_if_test_date_change')
    def test_update_participant(
            self,
            test_date_change_mock,
            get_test_result_mock,
            validate_edit_mock,
            transaction_write_records_mock,
            should_update_participant_mock,
            autocease_mock
    ):
        request_body = REQUEST_BODY.copy()
        request_body.update({
            'next_test_due_date': '2021-01-04',
            'current_next_test_due_date': '2017-01-04',
        })
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': quote('RESULT#TEST_DATE#CREATED_DATE')
            },
            'session': SESSION
        }
        validate_edit_mock.return_value = True, {}
        test_date_change_mock.return_value = False
        get_test_result_mock.return_value = EXPECTED_RESULT
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']
        create_result_record_mock.return_value = result_record_mock
        generate_crm_mock.return_value = [
            {
                'crm_number': '', 'comments': '', 'first_name': 'firsty',
                'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            },
            {
                'crm_number': '140540', 'comments': 'some free text',
                'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            }
        ]
        latest_test = {
            'sort_key': 'RESULT#TEST_DATE#CREATED_DATE',
            'test_date': '2021-01-04',
        }
        previous_test = None
        get_recent_results_mock.return_value = [latest_test, previous_test]

        should_update_participant_mock.return_value = (True, ParticipantStatus.ROUTINE)

        actual_response = edit_result_service.edit_result(event_data, PARTICIPANT)

        assertApiResponse(self, 200, {}, actual_response)
        get_test_result_mock.assert_called_with('123', 'RESULT#TEST_DATE#CREATED_DATE', segregate=True)
        create_result_record_mock.assert_not_called()
        expected_key = {'participant_id': '123', 'sort_key': 'RESULT#TEST_DATE#CREATED_DATE'}
        expected_update = {
            'sending_lab': 'akdak', 'source_code': 'abcd',
            'sender_code': '299205', 'slide_number': '249797',
            'test_date': '2020-04-01', 'result_code': 'X', 'infection_code': '0',
            'action_code': 'A', 'action': 'Routine', 'result': 'No Cytology test undertaken',
            'infection_result': 'HPV negative', 'health_authority': 'B', 'recall_months': '36',
            'crm': [
                {
                    'crm_number': '', 'comments': '', 'first_name': 'firsty',
                    'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
                },
                {
                    'crm_number': '140540', 'comments': 'some free text',
                    'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
                }
            ]
        }
        transaction_write_records_mock.assert_called_once_with([], [], [{
            'record_keys': {'participant_id': '123', 'sort_key': 'PARTICIPANT'},
            'update_values': {'status': ParticipantStatus.ROUTINE, 'next_test_due_date': '2021-01-04'},
        }, {
            'record_keys': expected_key,
            'update_values': expected_update,
            'delete_values': [],
        }], segregate=True)

        expected_log_calls = [
            call(LogReference.MANAGERESULT012),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_EDIT_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'action': 'Routine', 'action_code': 'A', 'comments': 'some free text',
                                        'crm_number': '140540', 'infection_code': '0', 'health_authority': 'B',
                                        'infection_result': 'HPV negative', 'sending_lab': 'akdak',
                                        'result': 'No Cytology test undertaken', 'result_code': 'X',
                                        'sender_code': '299205', 'slide_number': '249797', 'source_code': 'abcd',
                                        'test_date': '2020-04-01', 'updating_next_test_due_date_from': '2017-01-04',
                                        'updating_next_test_due_date_to': '2021-01-04'}
            )
        ])
        autocease_mock.assert_called_once_with(PARTICIPANT, date(2021, 1, 4))

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.should_update_next_test_due_date')
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.validate_edit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.check_if_test_date_change')
    def test_update_participant_should_add_Recall_months_to_audit_if_action_is_repeat_advised(
            self,
            test_date_change_mock,
            get_test_result_mock,
            validate_edit_mock,
            transaction_write_records_mock,
            should_update_participant_mock,
            autocease_mock
    ):
        request_body = REQUEST_BODY.copy()
        request_body['action_code'] = 'R'
        request_body.update({
            'next_test_due_date': '2021-01-04',
            'current_next_test_due_date': '2017-01-04',
        })
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': quote('RESULT#TEST_DATE#CREATED_DATE')
            },
            'session': SESSION
        }
        validate_edit_mock.return_value = True, {}
        test_date_change_mock.return_value = False
        get_test_result_mock.return_value = EXPECTED_RESULT
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['action'] = 'Repeat advised'
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']
        create_result_record_mock.return_value = result_record_mock
        generate_crm_mock.return_value = [
            {
                'crm_number': '', 'comments': '', 'first_name': 'firsty',
                'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            },
            {
                'crm_number': '140540', 'comments': 'some free text',
                'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
            }
        ]
        latest_test = {
            'sort_key': 'RESULT#TEST_DATE#CREATED_DATE',
            'test_date': '2021-01-04',
        }
        previous_test = None
        get_recent_results_mock.return_value = [latest_test, previous_test]

        should_update_participant_mock.return_value = (True, ParticipantStatus.ROUTINE)

        actual_response = edit_result_service.edit_result(event_data, PARTICIPANT)

        assertApiResponse(self, 200, {}, actual_response)
        get_test_result_mock.assert_called_with('123', 'RESULT#TEST_DATE#CREATED_DATE', segregate=True)
        create_result_record_mock.assert_not_called()
        expected_key = {'participant_id': '123', 'sort_key': 'RESULT#TEST_DATE#CREATED_DATE'}
        expected_update = {
            'sending_lab': 'akdak', 'source_code': 'abcd',
            'sender_code': '299205', 'slide_number': '249797',
            'test_date': '2020-04-01', 'result_code': 'X', 'infection_code': '0',
            'action_code': 'R', 'action': 'Repeat advised', 'result': 'No Cytology test undertaken',
            'infection_result': 'HPV negative', 'health_authority': 'B', 'recall_months': '36',
            'crm': [
                {
                    'crm_number': '', 'comments': '', 'first_name': 'firsty',
                    'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
                },
                {
                    'crm_number': '140540', 'comments': 'some free text',
                    'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-11-29T00:00:00.000000'
                }
            ]
        }
        transaction_write_records_mock.assert_called_once_with([], [], [{
            'record_keys': {'participant_id': '123', 'sort_key': 'PARTICIPANT'},
            'update_values': {'status': ParticipantStatus.ROUTINE, 'next_test_due_date': '2021-01-04'},
        }, {
            'record_keys': expected_key,
            'update_values': expected_update,
            'delete_values': [],
        }], segregate=True)

        expected_log_calls = [
            call(LogReference.MANAGERESULT012),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_EDIT_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'action': 'Repeat advised', 'action_code': 'R', 'comments': 'some free text',
                                        'crm_number': '140540', 'infection_code': '0', 'health_authority': 'B',
                                        'infection_result': 'HPV negative', 'sending_lab': 'akdak',
                                        'result': 'No Cytology test undertaken', 'result_code': 'X',
                                        'sender_code': '299205', 'slide_number': '249797', 'source_code': 'abcd',
                                        'test_date': '2020-04-01', 'updating_next_test_due_date_from': '2017-01-04',
                                        'updating_next_test_due_date_to': '2021-01-04', 'recall_months': '36'}
            )
        ])
        autocease_mock.assert_called_once_with(PARTICIPANT, date(2021, 1, 4))
