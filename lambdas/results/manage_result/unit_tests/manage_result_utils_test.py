import uuid
from unittest import TestCase
from mock import patch, Mock, call
from datetime import datetime, timedelta, timezone, date
from ddt import ddt, data, unpack
from boto3.dynamodb.conditions import Key
from manage_result.log_references import LogReference
from common.models.participant import ParticipantStatus
from common.test_assertions.api_assertions import assertApiResponse
from common.utils.audit_utils import AuditActions, AuditUsers
from common.models.participant import CeaseReason
from manage_result import manage_result_utils
from common.utils.dynamodb_access.table_names import TableNames

FIXED_NOW_TIME = datetime(2020, 11, 29, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = FIXED_NOW_TIME

FIXED_RESULT_ID = uuid.uuid4()
uuid_mock = Mock(wraps=uuid)
uuid_mock.uuid4.return_value = FIXED_RESULT_ID

PARTICIPANT_ID = '123'
NHS_NUMBER = '9999999999'
CORRESPONDING_SORT_KEY = 'RESULT#2020-04-01#2020-11-29T00:00:00.000+00:00'
PARTICIPANT = {
    'participant_id': PARTICIPANT_ID,
    'nhs_number': NHS_NUMBER,
    'date_of_birth': '1990-01-01',
    'next_test_due_date': '2021-02-01',
    'is_ceased': False
}
PARTICIPANT_WITHOUT_NTDD = {
    'participant_id': PARTICIPANT_ID,
    'nhs_number': NHS_NUMBER,
    'date_of_birth': '1990-01-01',
}

REQUEST_BODY = {
    'sending_lab': 'akdak',
    'source_code': 'abcd',
    'sender_code': '299205',
    'health_authority': 'B',
    'slide_number': '249797',
    'test_date': '2020-04-01',
    'result_code': 'X',
    'infection_code': '0',
    'action_code': 'A',
    'crm_number': '140540',
    'comments': 'some free text',
    'unexpected_field': 'unexpected_value',
    'recall_months': '36'
}

NO_INFECTION_CODE_REQUEST_BODY = {
    'sending_lab': 'akdak',
    'source_code': 'abcd',
    'sender_code': '299205',
    'health_authority': 'B',
    'slide_number': '249797',
    'test_date': '2020-04-01',
    'result_code': 'X',
    'hpv_primary': False,
    'action_code': 'A',
    'crm_number': '140540',
    'comments': 'some free text',
    'unexpected_field': 'unexpected_value',
    'recall_months': '36'
}

EXPECTED_RESULT = {
    'result_id': str(FIXED_RESULT_ID),
    'action': 'Routine',
    'action_code': 'A',
    'health_authority': 'B',
    'infection_code': '0',
    'infection_result': 'HPV negative',
    'sending_lab': 'akdak',
    'nhs_number': '9999999999',
    'participant_id': '123',
    'result': 'No Cytology test undertaken',
    'result_code': 'X',
    'sanitised_nhs_number': '9999999999',
    'sender_code': '299205',
    'slide_number': '249797',
    'recall_months': '36',
    'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
    'source_code': 'abcd',
    'test_date': '2020-04-01',
    'created': '2020-11-29T00:00:00.000000+00:00',
    'crm': [
        {
            'crm_number': '140540',
            'comments': 'some free text',
            'first_name': 'firsty',
            'last_name': 'lasty',
            'timestamp': '2020-10-18T13:48:08.000000',
        }
    ]
}
SESSION = {
    'session_key': '1234567890',
    'user_data': {
        'first_name': 'firsty',
        'last_name': 'lasty'
    }
}
CALCULATE_MOCK_RESULT = {
    'default': {
        'interval': 12,
        'next_test_due_date': date(2022, 2, 1),
    },
    'options': [{
        'interval': 6,
        'next_test_due_date': date(2021, 8, 1),
    }, {
        'interval': 36,
        'next_test_due_date': date(2024, 2, 1),
    }],
    'range': None,
    'status': ParticipantStatus.ROUTINE,
}
RANGE_6_TO_12 = [{'min_interval': 6, 'max_interval': 12},
                 {'interval': 6, 'next_test_due_date': date(2021, 8, 1)},
                 {'interval': 7, 'next_test_due_date': date(2021, 9, 1)},
                 {'interval': 8, 'next_test_due_date': date(2021, 10, 1)},
                 {'interval': 9, 'next_test_due_date': date(2021, 11, 1)},
                 {'interval': 10, 'next_test_due_date': date(2021, 12, 1)},
                 {'interval': 11, 'next_test_due_date': date(2022, 1, 1)},
                 {'interval': 12, 'next_test_due_date': date(2022, 2, 1)}]
CALCULATE_MOCK_RESULT_RANGE = {
    'default': None,
    'options': None,
    'range': RANGE_6_TO_12,
    'status': ParticipantStatus.ROUTINE,
}


@ddt
class TestManageResultUtils(TestCase):
    module = 'manage_result.manage_result_utils'

    @patch('manage_result.manage_result_utils.datetime', datetime_mock)
    @patch('manage_result.manage_result_utils.uuid', uuid_mock)
    def test_create_result_record_with_all_fields(self):
        request_body = REQUEST_BODY.copy()
        expected_result = EXPECTED_RESULT.copy()
        del expected_result['crm']
        expected_result['crm_number'] = '140540'
        expected_result['comments'] = 'some free text'
        actual_result = manage_result_utils.create_result_record(request_body, PARTICIPANT)
        self.assertDictEqual(expected_result, actual_result)

    @patch('manage_result.manage_result_utils.datetime', datetime_mock)
    @patch('manage_result.manage_result_utils.uuid', uuid_mock)
    def test_create_result_record_with_recall_months_if_action_code_is_R(self):
        request_body = {
            'sending_lab': 'akdak',
            'source_code': 'abcd',
            'sender_code': '299205',
            'health_authority': 'B',
            'slide_number': '249797',
            'test_date': '2020-04-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'R',
            'crm_number': '140540',
            'comments': 'some free text',
            'unexpected_field': 'unexpected_value',
            'recall_months': '36'
        }
        expected_result = {
            'result_id': str(FIXED_RESULT_ID),
            'action': 'Repeat advised',
            'action_code': 'R',
            'health_authority': 'B',
            'infection_code': '0',
            'infection_result': 'HPV negative',
            'sending_lab': 'akdak',
            'nhs_number': '9999999999',
            'participant_id': '123',
            'result': 'No Cytology test undertaken',
            'result_code': 'X',
            'sanitised_nhs_number': '9999999999',
            'sender_code': '299205',
            'slide_number': '249797',
            'recall_months': '36',
            'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
            'source_code': 'abcd',
            'test_date': '2020-04-01',
            'created': '2020-11-29T00:00:00.000000+00:00',
            'crm': [
                {
                    'crm_number': '140540',
                    'comments': 'some free text',
                    'first_name': 'firsty',
                    'last_name': 'lasty',
                    'timestamp': '2020-10-18T13:48:08.000000',
                }
            ]
        }
        del expected_result['crm']
        expected_result['crm_number'] = '140540'
        expected_result['comments'] = 'some free text'
        actual_result = manage_result_utils.create_result_record(request_body, PARTICIPANT)
        self.assertDictEqual(expected_result, actual_result)

    @patch('manage_result.manage_result_utils.datetime', datetime_mock)
    @patch('manage_result.manage_result_utils.uuid', uuid_mock)
    def test_create_result_record_with_only_mandatory_fields(self):
        request_body = {'test_date': '2020-04-01'}
        expected_result = {'result_id': str(FIXED_RESULT_ID),
                           'participant_id': PARTICIPANT_ID,
                           'nhs_number': NHS_NUMBER,
                           'sanitised_nhs_number': NHS_NUMBER,
                           'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
                           'test_date': '2020-04-01',
                           'created': '2020-11-29T00:00:00.000000+00:00',
                           'infection_result': 'Unknown', 'result': 'Unknown', 'action': 'Unknown'}
        actual_result = manage_result_utils.create_result_record(request_body, PARTICIPANT)
        self.assertDictEqual(expected_result, actual_result)

    @patch(f'{module}.log')
    def test_validate_mandatory_fields(
            self, log_mock):
        request_body = REQUEST_BODY.copy()

        actual_is_valid_bool, actual_response = manage_result_utils.validate_mandatory_fields(request_body)

        self.assertEqual(None, actual_response)

        self.assertTrue(actual_is_valid_bool)

        log_mock.assert_not_called()

    @data(
        {},
        {key: value for key, value in REQUEST_BODY.items() if key != 'test_date'}
    )
    @patch(f'{module}.log')
    def test_validate_mandatory_fields_missing_mandatory_fields(
            self, request_body, log_mock):

        actual_is_valid_bool, actual_response = manage_result_utils.validate_mandatory_fields(request_body)

        assertApiResponse(self, 400, {'message': 'Missing mandatory fields in the request'}, actual_response)

        self.assertFalse(actual_is_valid_bool)

        expected_log_calls = [
            call(LogReference.MANAGERESULT006),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.log')
    @patch(f'{module}.is_valid_health_authority')
    def test_validate_mandatory_fields_valid_single_character_health_authority(
            self, mock_health_authority_validator, log_mock):
        request_body = {'test_date': '2020-08-12', 'health_authority': 'A'}
        mock_health_authority_validator.return_value = True

        actual_is_valid_bool, actual_response = manage_result_utils.validate_mandatory_fields(
            request_body)

        self.assertEqual(None, actual_response)

        self.assertTrue(actual_is_valid_bool)

        mock_health_authority_validator.assert_called_with('A')

        log_mock.assert_not_called()

    @patch(f'{module}.log')
    @patch(f'{module}.is_valid_health_authority')
    def test_validate_mandatory_fields_valid_IOM_health_authority(
            self, mock_health_authority_validator, log_mock):
        request_body = {'test_date': '2020-08-12', 'health_authority': 'IM'}
        mock_health_authority_validator.return_value = True

        actual_is_valid_bool, actual_response = manage_result_utils.validate_mandatory_fields(
            request_body)

        self.assertEqual(None, actual_response)

        self.assertTrue(actual_is_valid_bool)

        mock_health_authority_validator.assert_called_with('IM')

        log_mock.assert_not_called()

    @patch(f'{module}.log')
    @patch(f'{module}.is_valid_health_authority')
    def test_validate_mandatory_fields_invalid_health_authority(
            self, mock_health_authority_validator, log_mock):
        request_body = {'test_date': '2020-08-12', 'health_authority': 'ENG'}
        mock_health_authority_validator.return_value = False

        actual_is_valid_bool, actual_response = manage_result_utils.validate_mandatory_fields(request_body)

        assertApiResponse(self, 400, {'message': 'Invalid health authority'}, actual_response)

        self.assertFalse(actual_is_valid_bool)

        mock_health_authority_validator.assert_called_with('ENG')

        expected_log_calls = [
            call(LogReference.MANAGERESULT010),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{module}.is_valid_test_date')
    @patch(f'{module}.log')
    def test_validate_mandatory_fields_invalid_date(
            self, log_mock, valid_date_mock):
        request_body = {key: value for key, value in REQUEST_BODY.items()}
        invalid_test_date = str((FIXED_NOW_TIME + timedelta(days=1)).date())
        request_body['test_date'] = invalid_test_date

        valid_date_mock.return_value = False

        actual_is_valid_bool, actual_response = manage_result_utils.validate_mandatory_fields(request_body)

        assertApiResponse(self, 400, {'message': 'Wrong test date is provided'}, actual_response)
        self.assertFalse(actual_is_valid_bool)

        expected_log_calls = [
            call(LogReference.MANAGERESULT007),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        valid_date_mock.assert_has_calls([call(invalid_test_date)])

    @unpack
    @data((1, False), (0, True), (-1, True))
    @patch('manage_result.manage_result_utils.datetime', datetime_mock)
    def test_date_validation(self, add_days, expected):
        input_date = FIXED_NOW_TIME + timedelta(days=add_days)
        date_str = str(input_date.date())
        actual = manage_result_utils.is_valid_test_date(date_str)
        self.assertEqual(expected, actual)

    def test_date_validation_invalid_date(self):
        date_str = 'invalid'
        actual = manage_result_utils.is_valid_test_date(date_str)
        self.assertEqual(False, actual)

    @data('A', 'B', 'C', 'F', 'G', 'H', 'L', 'N', 'R', 'S', 'T', 'V', 'W', 'Y', 'Z')
    def test_is_valid_health_authority_returns_true_for_valid_health_authority(self, health_authority):
        is_valid = manage_result_utils.is_valid_health_authority(health_authority)
        self.assertTrue(is_valid)

    @data(None, 'invalid', '', 'm', 'a')
    def test_is_valid_health_authority_returns_false_for_invalid_health_authority(self, health_authority):
        is_valid = manage_result_utils.is_valid_health_authority(health_authority)
        self.assertFalse(is_valid)

    @unpack
    @data(
        (PARTICIPANT, REQUEST_BODY, date(2021, 2, 1), date(2022, 2, 1), (True, ParticipantStatus.ROUTINE), CALCULATE_MOCK_RESULT),  
        (PARTICIPANT, NO_INFECTION_CODE_REQUEST_BODY, date(2021, 2, 1), date(2022, 2, 1), (True, ParticipantStatus.ROUTINE), CALCULATE_MOCK_RESULT),  
        (PARTICIPANT, REQUEST_BODY, date(2021, 2, 1), date(2021, 8, 1), (True, ParticipantStatus.ROUTINE), CALCULATE_MOCK_RESULT),  
        (PARTICIPANT, REQUEST_BODY, date(2021, 2, 1), date(2024, 2, 1), (True, ParticipantStatus.ROUTINE), CALCULATE_MOCK_RESULT),  
        (PARTICIPANT, REQUEST_BODY, date(2021, 2, 1), date(2021, 2, 1), (False, None), CALCULATE_MOCK_RESULT),
        (PARTICIPANT, REQUEST_BODY, date(2021, 2, 1), date(2021, 8, 1), (True, ParticipantStatus.ROUTINE), CALCULATE_MOCK_RESULT_RANGE),  
        # When current_ntdd is None, we calculate a ntdd
        (PARTICIPANT_WITHOUT_NTDD, REQUEST_BODY, None, date(2022, 2, 1), (True, ParticipantStatus.ROUTINE), CALCULATE_MOCK_RESULT),  
    )
    @patch('manage_result.manage_result_utils.calculate_next_test_due_date_for_result')
    def should_update_next_test_due_date(
        self, participant, result, current_ntdd, new_ntdd, expected, calculate_value, calculate_mock
    ):
        # Arrange
        calculate_mock.return_value = calculate_value
        # Act
        actual_result = manage_result_utils.should_update_next_test_due_date(
            participant, result, current_ntdd, new_ntdd
        )
        # Assert
        self.assertEqual(actual_result, expected)

    @unpack
    @data((PARTICIPANT, REQUEST_BODY, date(2021, 2, 2), date(2022, 2, 1), 'Provided current_next_test_due_date (2021-02-02) does not match participant record (2021-02-01)'),  
          (PARTICIPANT, REQUEST_BODY, date(2021, 2, 1), date(2022, 2, 2), 'Provided new_next_test_due_date 2022-02-02 not a valid option: options are: [datetime.date(2022, 2, 1), datetime.date(2021, 8, 1), datetime.date(2024, 2, 1)]'),  
          (PARTICIPANT, REQUEST_BODY, None, date(2022, 2, 1), 'Provided current_next_test_due_date (None) does not match participant record (2021-02-01)'),)  
    @patch('manage_result.manage_result_utils.calculate_next_test_due_date_for_result')
    def should_update_next_test_due_date_exceptions(
            self, participant, result, current_ntdd, new_ntdd, expected, calculate_mock):
        # Arrange
        calculate_mock.return_value = CALCULATE_MOCK_RESULT
        # Act
        with self.assertRaises(Exception) as context:
            manage_result_utils.should_update_next_test_due_date(
                participant, result, current_ntdd, new_ntdd)
        # Assert
        self.assertEqual(expected, context.exception.args[0])

    @unpack
    @data(([], None),
          ([{'test_date': '2021-01-01'}], date(2021, 1, 1)))
    @patch('manage_result.manage_result_utils.get_recent_results_by_participant_id')
    def test_latest_result_date(self, results, expected, query_mock):
        query_mock.return_value = results
        actual = manage_result_utils.get_latest_test_date('123')
        self.assertEqual(actual, expected)
        query_mock.assert_called_once_with('123', 1, segregate=True)

    @unpack
    @data(([], (None, None)),
          ([EXPECTED_RESULT], (EXPECTED_RESULT, None)),
          ([EXPECTED_RESULT, {**EXPECTED_RESULT, 'test_date': '2017-01-02'}],
           (EXPECTED_RESULT, {**EXPECTED_RESULT, 'test_date': '2017-01-02'})))
    @patch('manage_result.manage_result_utils.get_recent_results_by_participant_id')
    def test_get_latest_and_previous_test_data(self, results, expected, query_mock):
        query_mock.return_value = results
        actual = manage_result_utils.get_latest_and_previous_test_data('123')
        self.assertEqual(actual, expected)
        query_mock.assert_called_once_with('123', 2, segregate=True)

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.cease_participant_and_send_notification_messages')
    @patch(f'{module}.participant_eligible_for_auto_cease')
    def test_autocease_if_eligible_given_participant_is_eligible(
            self, is_eligible_mock, cease_mock, audit_mock, log_mock):
        participant = {'participant_id': 'the_participant_id',
                       'sort_key': 'PARTICIPANT',
                       'nhs_number': 'the_nhs_number'}
        next_test_due_date = date(2020, 1, 2)
        is_eligible_mock.return_value = True

        manage_result_utils.autocease_if_eligible(participant, next_test_due_date)

        is_eligible_mock.assert_called_once_with(participant, next_test_due_date, segregate=True)
        cease_mock.assert_called_once_with(participant, 'SYSTEM', {'reason': 'AUTOCEASE_DUE_TO_AGE'}, segregate=True)
        audit_mock.assert_called_once_with(
            action=AuditActions.CEASE_EPISODE,
            user=AuditUsers.SYSTEM,
            participant_ids=['the_participant_id'],
            nhs_numbers=['the_nhs_number'],
            additional_information={'reason': CeaseReason.AUTOCEASE_DUE_TO_AGE,
                                    'updating_next_test_due_date_from': '2020-01-02'})
        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT037),
            call(LogReference.MANAGERESULT038)
        ])

    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.cease_participant_and_send_notification_messages')
    @patch(f'{module}.participant_eligible_for_auto_cease')
    def test_autocease_if_eligible_given_participant_is_not_eligible(
            self, is_eligible_mock, cease_mock, audit_mock, log_mock):
        participant = {'participant_id': 'the_participant_id', 'sort_key': 'PARTICIPANT'}
        next_test_due_date = 'the_next_test_due_date'
        is_eligible_mock.return_value = False

        manage_result_utils.autocease_if_eligible(participant, next_test_due_date)

        is_eligible_mock.assert_called_once_with(participant, next_test_due_date, segregate=True)
        cease_mock.assert_not_called()
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT037),
            call(LogReference.MANAGERESULT039)
        ])

    @unpack
    @data(({'participant_id': '1'}, None),
          ({'next_test_due_date': ''}, None),
          ({'next_test_due_date': '2020-12-31'}, date(2020, 12, 31)))
    def test_get_participant_ntdd(self, participant, expected_ntdd):
        actual_ntdd = manage_result_utils.get_participant_ntdd(participant)
        self.assertEqual(expected_ntdd, actual_ntdd)

    @unpack
    @data((False, date(2021, 1, 5), date(2021, 1, 1), None, None, 'CURRENT', LogReference.MANAGERESULT032),
          (True, date(2021, 1, 1), date(2021, 1, 1), None, None, 'CURRENT', LogReference.MANAGERESULT033),
          (True, date(2021, 1, 1), date(2021, 1, 5), {'sort_key': 'Previous', 'test_date': '2020-12-01'}, date(2020, 12, 1), 'CURRENT', LogReference.MANAGERESULT033),  
          (True, date(2020, 10, 1), date(2021, 1, 5), {'sort_key': 'Previous', 'test_date': '2020-12-01'}, date(2020, 12, 1), 'PREVIOUS', LogReference.MANAGERESULT034),  
          (False, date(2020, 1, 1), date(2021, 1, 5), None, None, 'NONE', LogReference.MANAGERESULT035))
    @patch(f'{module}.log')
    def test_required_update_ntdd_status(
            self,
            sort_key_match,
            test_date,
            latest_result_test_date,
            previous_result,
            previous_result_test_date,
            expected,
            expected_log_call,
            log_mock):
        actual = manage_result_utils.required_update_ntdd_status(sort_key_match, test_date, latest_result_test_date,
                                                                 previous_result, previous_result_test_date)
        self.assertEqual(actual, expected)
        expected_log_calls = [
            call(expected_log_call),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch('manage_result.manage_result_utils.dynamodb_query', return_value=Mock())
    def test_get_recent_results_by_participant_id_no_limit(self, query_mock):
        # Act
        manage_result_utils.get_recent_results_by_participant_id('123456')
        # Assert
        condition_expression = Key('participant_id').eq('123456') & Key('sort_key').begins_with('RESULT#')
        query_mock.assert_called_once_with(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=condition_expression, ScanIndexForward=False
        ))

    @patch('manage_result.manage_result_utils.dynamodb_query', return_value=Mock())
    def test_get_recent_results_by_participant_id_with_limit(self, query_mock):
        # Act
        manage_result_utils.get_recent_results_by_participant_id('123456', 3)
        # Assert
        condition_expression = Key('participant_id').eq('123456') & Key('sort_key').begins_with('RESULT#')
        query_mock.assert_called_once_with(TableNames.PARTICIPANTS, dict(
            KeyConditionExpression=condition_expression, ScanIndexForward=False, Limit=3
        ))
