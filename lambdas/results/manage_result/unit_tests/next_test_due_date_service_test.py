from datetime import date
from unittest import TestCase
from mock import patch, call, Mock
import copy
from manage_result.unit_tests.manage_result_utils_test import (
    PARTICIPANT_ID,
    PARTICIPANT,
    SESSION,
)
from manage_result.log_references import LogReference
from common.test_assertions.api_assertions import assertApiResponse
from manage_result.next_test_due_date_service import next_test_due_date, next_test_due_date_after_delete

EVENT_DATA = {
    'body': {
        'test_date': '2021-01-01',
        'result_code': 'X',
        'infection_code': '0',
        'action_code': 'R',
        'sort_key': 'RESULT#2021-03-01',
        'hpv_primary': True,
        'self_sample': False
    },
    'path_parameters': {
        'participant_id': PARTICIPANT_ID,
    },
    'session': SESSION
}

EXPECTED_RESULT_DATA = {
    'data': {
        'default': {
            'interval': 36,
            'next_test_due_date': date(2024, 1, 1),
        },
        'options': [{
                    'interval': 12,
                    'next_test_due_date': date(2022, 1, 1),
                    }, {
                    'interval': 6,
                    'next_test_due_date': date(2021, 7, 1),
                    }],
        'range': None,
        'current': '2021-02-01'
    }
}

RANGE_12_TO_36 = [{'min_interval': 12, 'max_interval': 36},
                  {'interval': 12, 'next_test_due_date': date(2022, 1, 1)},
                  {'interval': 13, 'next_test_due_date': date(2022, 2, 1)},
                  {'interval': 14, 'next_test_due_date': date(2022, 3, 1)},
                  {'interval': 15, 'next_test_due_date': date(2022, 4, 1)},
                  {'interval': 16, 'next_test_due_date': date(2022, 5, 1)},
                  {'interval': 17, 'next_test_due_date': date(2022, 6, 1)},
                  {'interval': 18, 'next_test_due_date': date(2022, 7, 1)},
                  {'interval': 19, 'next_test_due_date': date(2022, 8, 1)},
                  {'interval': 20, 'next_test_due_date': date(2022, 9, 1)},
                  {'interval': 21, 'next_test_due_date': date(2022, 10, 1)},
                  {'interval': 22, 'next_test_due_date': date(2022, 11, 1)},
                  {'interval': 23, 'next_test_due_date': date(2022, 12, 1)},
                  {'interval': 24, 'next_test_due_date': date(2023, 1, 1)},
                  {'interval': 25, 'next_test_due_date': date(2023, 2, 1)},
                  {'interval': 26, 'next_test_due_date': date(2023, 3, 1)},
                  {'interval': 27, 'next_test_due_date': date(2023, 4, 1)},
                  {'interval': 28, 'next_test_due_date': date(2023, 5, 1)},
                  {'interval': 29, 'next_test_due_date': date(2023, 6, 1)},
                  {'interval': 30, 'next_test_due_date': date(2023, 7, 1)},
                  {'interval': 31, 'next_test_due_date': date(2023, 8, 1)},
                  {'interval': 32, 'next_test_due_date': date(2023, 9, 1)},
                  {'interval': 33, 'next_test_due_date': date(2023, 10, 1)},
                  {'interval': 34, 'next_test_due_date': date(2023, 11, 1)},
                  {'interval': 35, 'next_test_due_date': date(2023, 12, 1)},
                  {'interval': 36, 'next_test_due_date': date(2024, 1, 1)}]

EXPECTED_RESULT_DATA_FOR_MIN_MAX_RANGE = {
    'data': {
        'default': None,
        'options': None,
        'range': RANGE_12_TO_36,
        'current': '2021-02-01'
    }
}

EXPECTED_UNCHANGED_RESULT = {
    'data': {
        'unchanged': True
    }
}


get_latest_and_previous_test_data_mock = Mock()
get_latest_test_date_mock = Mock()
calculate_next_test_due_date_mock = Mock()
calculate_next_test_due_date_for_result_mock = Mock()
log_mock = Mock()

module = 'manage_result.next_test_due_date_service'


@patch(f'{module}.log', log_mock)
@patch(f'{module}.calculate_next_test_due_date', calculate_next_test_due_date_mock)
@patch(f'{module}.calculate_next_test_due_date_for_result', calculate_next_test_due_date_for_result_mock)
@patch(f'{module}.get_latest_and_previous_test_data', get_latest_and_previous_test_data_mock)
@patch(f'{module}.get_latest_test_date', get_latest_test_date_mock)
class TestNextTestDueDateService(TestCase):

    def setUp(self):
        self.maxDiff = None
        get_latest_test_date_mock.reset_mock()
        calculate_next_test_due_date_for_result_mock.reset_mock(return_value=True, side_effect=True)
        log_mock.reset_mock()
        self.log_patcher = patch('common.log.logger').start()

    def tearDown(self):
        self.log_patcher.stop()

    def test_invalid_test_date(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        event_data['body']['test_date'] = 'not a valid date'
        expected_result = {
            'message': 'Invalid test_date value provided',
        }

        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 400, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT018),
        ])

    def test_no_sort_key_provided_and_no_latest_result_date(self):
        # Arrange
        participant = copy.deepcopy(PARTICIPANT)
        participant.pop('next_test_due_date', None)

        event_data = copy.deepcopy(EVENT_DATA)
        event_data['body']['sort_key'] = None

        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA)

        util_result = expected_result['data'].copy()
        util_result['status'] = 'R (normal)'

        calculate_next_test_due_date_for_result_mock.return_value = util_result
        get_latest_test_date_mock.return_value = None

        # Act
        actual_response = next_test_due_date(event_data, participant)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT029),
            call(LogReference.MANAGERESULT020),
            call(LogReference.MANAGERESULT036),
        ])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), True, False)])

    def test_no_sort_key_provided_and_latest_result_date_older(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        event_data['body']['sort_key'] = None

        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA)
        util_result = expected_result['data'].copy()
        expected_result['data'].update({'current': '2021-02-01'})
        util_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = util_result
        get_latest_test_date_mock.return_value = date(2020, 1, 1)

        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT029),
            call(LogReference.MANAGERESULT020),
            call(LogReference.MANAGERESULT036),
        ])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), True, False)])

    def test_no_sort_key_provided_and_latest_result_date_equal_or_newer(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        event_data['body']['sort_key'] = None
        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA)

        util_result = expected_result['data'].copy()
        util_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = util_result

        expected_result['data']['unchanged'] = True

        get_latest_test_date_mock.return_value = date(2021, 1, 1)

        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT019),
        ])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), True, False)])

    def test_sort_key_not_matched_and_date_older_or_equal_to_latest_result_date(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA)

        util_result = expected_result['data'].copy()
        util_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = util_result

        expected_result['data']['unchanged'] = True

        latest_test = {
            'sort_key': 'NOMATCH',
            'test_date': '2021-01-01',
        }
        previous_test = None
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT019),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), True, False)])

    def test_sort_key_not_matched_and_date_more_recent_than_latest_result_date(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA)
        expected_result['data'].update({'current': '2021-02-01'})
        util_result = expected_result['data'].copy()
        util_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = util_result
        latest_test = {
            'sort_key': 'NOMATCH',
            'test_date': '2020-12-01',
        }
        previous_test = None
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT030),
            call(LogReference.MANAGERESULT020),
            call(LogReference.MANAGERESULT036),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), True, False)])

    def test_sort_key_matched_and_no_previous_result(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA)
        util_result = expected_result['data'].copy()
        util_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = util_result
        latest_test = {
            'sort_key': 'RESULT#2021-03-01',
            'test_date': '2020-12-01',
        }
        previous_test = None
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT030),
            call(LogReference.MANAGERESULT020),
            call(LogReference.MANAGERESULT036),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), True, False)])

    def test_no_options_and_default_matches_existing_next_test_due_date(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA)
        expected_result['data']['options'] = None
        util_result = expected_result['data'].copy()
        util_result['default']['next_test_due_date'] = date(2021, 2, 1)
        util_result['status'] = 'R (normal)'

        calculate_next_test_due_date_for_result_mock.return_value = util_result

        expected_result['data']['unchanged'] = True

        latest_test = {
            'sort_key': 'RESULT#2021-03-01',
            'test_date': '2020-12-01',
        }
        previous_test = None
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT030),
            call(LogReference.MANAGERESULT020),
            call(LogReference.MANAGERESULT036),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), True, False)])

    def test_sort_key_matched_and_date_more_recent_than_previous_result_date(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA)
        expected_result['data'].update({'current': '2021-02-01'})
        util_result = expected_result['data'].copy()
        util_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = util_result
        latest_test = {
            'sort_key': 'RESULT#2021-03-01',
            'test_date': '2020-12-20',
        }
        previous_test = {
            'sort_key': 'Previous',
            'test_date': '2020-12-01',
        }
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT030),
            call(LogReference.MANAGERESULT020),
            call(LogReference.MANAGERESULT036),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), True, False)])

    def test_sort_key_matched_and_date_older_or_equal_to_previous_result_date(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        event_data['body']['test_date'] = '2020-10-01'
        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA)
        expected_result['data'].update({'current': '2021-02-01'})
        util_result = expected_result['data'].copy()
        util_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = util_result
        latest_test = {
            'sort_key': 'RESULT#2021-03-01',
            'test_date': '2020-12-20',
        }
        previous_test = {
            'sort_key': 'Previous',
            'test_date': '2020-11-01',
            'result_code': 'A',
            'infection_code': 'B',
            'action_code': 'C',
            'hpv_primary': False,
            'self_sample': False
        }
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT031),
            call(LogReference.MANAGERESULT020),
            call(LogReference.MANAGERESULT036),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2020, 11, 1), ('A', 'B', 'C'), date(1990, 1, 1), False, False)])

    def test_invalid_result_codes(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        event_data['body']['test_date'] = '2021-01-10'
        event_data['body'].update({
            'result_code': 'N',
            'infection_code': 'U',
            'action_code': 'L',
        })

        latest_test = {
            'sort_key': 'RESULT#2021-03-01',
            'test_date': '2021-01-20',
        }
        previous_test = None
        calculate_next_test_due_date_for_result_mock.side_effect = ValueError(
            'No matching profile for results found when calculating next test due date')
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test
        expected_response = {'message': 'No matching profile for results found when calculating next test due date'}

        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 400, expected_response, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT017),
            call(LogReference.MANAGERESULT030),
            call(LogReference.MANAGERESULT021),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2021, 1, 10), ('N', 'U', 'L'), date(1990, 1, 1), True, False)])

    def test_next_test_due_date_after_delete_participant_is_ceased(self):
        # Arrange
        event_data = {
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT'
            },
            'session': SESSION
        }

        latest_test = None
        previous_test = {
            'test_date': '2021-01-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'R',
        }
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        participant = copy.deepcopy(PARTICIPANT)
        participant['is_ceased'] = True
        actual_response = next_test_due_date_after_delete(event_data, participant)

        # Assert
        assertApiResponse(self, 200, EXPECTED_UNCHANGED_RESULT, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT025),
            call(LogReference.MANAGERESULT042),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])

    def test_next_test_due_date_after_delete_no_latest_test(self):
        # Arrange
        event_data = {
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT'
            },
            'session': SESSION
        }

        latest_test = None
        previous_test = {
            'test_date': '2021-01-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'R',
        }
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date_after_delete(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, EXPECTED_UNCHANGED_RESULT, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT025),
            call(LogReference.MANAGERESULT026),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])

    def test_next_test_due_date_after_delete_no_latest_test_match(self):
        # Arrange
        event_data = {
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'NOMATCH#111'
            },
            'session': SESSION
        }

        latest_test = {
            'sort_key': 'NOMATCH#999'
        }
        previous_test = {
            'test_date': '2021-01-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'R',
        }
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date_after_delete(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, EXPECTED_UNCHANGED_RESULT, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT025),
            call(LogReference.MANAGERESULT027),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])

    def test_next_test_due_date_after_delete_no_previous_test(self):
        # Arrange
        event_data = {
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT'
            },
            'session': SESSION
        }

        latest_test = {
            'sort_key': 'RESULT'
        }
        previous_test = None
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date_after_delete(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, EXPECTED_UNCHANGED_RESULT, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT025),
            call(LogReference.MANAGERESULT028),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])

    def test_get_next_test_due_date_uses_previous_recall_months_on_delete(self):
        # Arrange
        event_data = {
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT'
            },
            'session': SESSION
        }
        expected_result = {
            'data': {
                'next_test_due_date': '2026-01-01'
            },
        }
        util_result = date(2026, 1, 1)

        latest_test = {
            'sort_key': 'RESULT'
        }
        previous_test = {
            'test_date': '2021-01-01',
            'recall_months': '60',
        }
        calculate_next_test_due_date_mock.return_value = util_result
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date_after_delete(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT025),
            call(LogReference.MANAGERESULT043),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])
        calculate_next_test_due_date_mock.assert_has_calls([call(date(2021, 1, 1), 60)])

    def test_get_next_test_due_date_on_delete(self):
        # Arrange
        event_data = {
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT'
            },
            'session': SESSION
        }
        expected_result = {
            'data': {
                'next_test_due_date': '2024-01-01'
            },
        }
        util_result = {
            'default': {
                'interval': 36,
                'next_test_due_date': date(2024, 1, 1),
            },
            'options': [{
                'interval': 12,
                'next_test_due_date': date(2022, 1, 1),
            }, {
                'interval': 6,
                'next_test_due_date': date(2021, 7, 1),
            }],
            'range': None
        }
        latest_test = {
            'sort_key': 'RESULT'
        }
        previous_test = {
            'test_date': '2021-01-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'R',
        }
        util_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = util_result
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        actual_response = next_test_due_date_after_delete(event_data, PARTICIPANT)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)

        log_mock.assert_has_calls([
            call(LogReference.MANAGERESULT025),
            call(LogReference.MANAGERESULT020),
        ])
        get_latest_and_previous_test_data_mock.assert_has_calls([call(PARTICIPANT_ID)])
        calculate_next_test_due_date_for_result_mock.assert_has_calls(
            [call(date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), True, False)])

    def test_next_test_due_date_service_uses_the_hpv_primary_and_self_samlple_flag_from_the_HTTP_request(self):
        event_data = copy.deepcopy(EVENT_DATA)
        event_data['body']['hpv_primary'] = False
        event_data['body']['sort_key'] = None
        get_latest_test_date_mock.return_value = None
        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA)
        util_result = expected_result['data'].copy()
        util_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = util_result
        # Act
        next_test_due_date(event_data, PARTICIPANT)

        calculate_next_test_due_date_for_result_mock.assert_called_with(
            date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), False, False)

    def test_next_test_due_date_service_uses_the_hpv_primary_and_self_sample_flag_from_the_result_to_delete(self):
        # Arrange
        event_data = {
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT'
            },
            'session': SESSION
        }
        util_result = {
            'default': {
                'interval': 36,
                'next_test_due_date': date(2024, 1, 1),
            },
            'options': [{
                'interval': 12,
                'next_test_due_date': date(2022, 1, 1),
            }, {
                'interval': 6,
                'next_test_due_date': date(2021, 7, 1),
            }],
            'range': None
        }
        latest_test = {
            'sort_key': 'RESULT'
        }
        previous_test = {
            'test_date': '2021-01-01',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'R',
            'hpv_primary': True,
            'self_sample': True
        }
        util_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = util_result
        get_latest_and_previous_test_data_mock.return_value = latest_test, previous_test

        # Act
        next_test_due_date_after_delete(event_data, PARTICIPANT)

        # Assert
        calculate_next_test_due_date_for_result_mock.assert_called_with(
            date(2021, 1, 1), ('X', '0', 'R'), date(1990, 1, 1), True, True)

    def test_next_test_due_date_service_adds_test_date_to_option_when_returning_recall_months_range(self):
        # Arrange
        event_data = copy.deepcopy(EVENT_DATA)
        event_data['body']['hpv_primary'] = False
        event_data['body']['sort_key'] = None
        event_data['body']['result_code'] = 'B'

        get_latest_test_date_mock.return_value = None
        expected_result = copy.deepcopy(EXPECTED_RESULT_DATA_FOR_MIN_MAX_RANGE)
        ntdd_result = expected_result['data'].copy()
        # ntdd_result['options'][0].pop('test_date')
        ntdd_result['status'] = 'R (normal)'
        calculate_next_test_due_date_for_result_mock.return_value = ntdd_result
        # Act
        actual_response = next_test_due_date(event_data, PARTICIPANT)

        calculate_next_test_due_date_for_result_mock.assert_called_with(
            date(2021, 1, 1), ('B', '0', 'R'), date(1990, 1, 1), False, False)

        # Assert
        assertApiResponse(self, 200, expected_result, actual_response)
