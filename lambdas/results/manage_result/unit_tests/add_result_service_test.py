import json
from datetime import date
from unittest import TestCase
from mock import patch, call, Mock
from ddt import ddt, data, unpack
from manage_result.unit_tests.manage_result_utils_test import (
    PARTICIPANT_ID,
    PARTICIPANT,
    SESSION,
    REQUEST_BODY,
    EXPECTED_RESULT,
)
from manage_result.log_references import LogReference
from common.models.letters import SupressionReason
from common.test_assertions.api_assertions import assertApiResponse
from manage_result import add_result_service
from common.utils import audit_utils
from common.models.participant import ParticipantStatus

log_mock = Mock()
audit_mock = Mock()
validate_mandatory_fields_mock = Mock()
create_result_mock = Mock()
generate_crm_list_mock = Mock()
create_participant_mock = Mock()
autocease_mock = Mock()
check_suppression_mock = Mock()
queue_letter_mock = Mock()
module = 'manage_result.add_result_service'


@ddt
@patch(f'{module}.log', log_mock)
@patch(f'{module}.audit', audit_mock)
@patch(f'{module}.validate_mandatory_fields', validate_mandatory_fields_mock)
@patch(f'{module}.create_result_record', create_result_mock)
@patch(f'{module}.generate_crm_list', generate_crm_list_mock)
@patch(f'{module}.create_replace_record_in_participant_table', create_participant_mock)
@patch(f'{module}.autocease_if_eligible', autocease_mock)
@patch(f'{module}.check_for_result_letter_suppression', check_suppression_mock)
@patch(f'{module}.add_result_letter_to_processing_queue', queue_letter_mock)
class TestAddResultService(TestCase):

    def setUp(self):
        log_mock.reset_mock()
        audit_mock.reset_mock()
        validate_mandatory_fields_mock.reset_mock()
        create_result_mock.reset_mock()
        generate_crm_list_mock.reset_mock()
        create_participant_mock.reset_mock()
        autocease_mock.reset_mock()
        queue_letter_mock.reset_mock()

    @patch(f'{module}.get_latest_test_date')
    def test_add_result(
            self,
            get_latest_test_date_mock):
        request_body = REQUEST_BODY.copy()
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
            },
            'session': SESSION
        }

        validate_mandatory_fields_mock.return_value = True, {}
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']

        check_suppression_mock.return_value = None

        generate_crm_list_mock.return_value = [{
            'crm_number': '140540', 'comments': 'some free text',
            'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-10-18T13:48:08.000000'}]

        create_result_mock.return_value = result_record_mock
        get_latest_test_date_mock.return_value = date(2022, 1, 1)
        autocease_mock.return_value = False

        actual_response = add_result_service.add_result(event_data, PARTICIPANT)

        assertApiResponse(self, 201, {}, actual_response)

        get_latest_test_date_mock.assert_called_once_with('123')
        expected_result = {**EXPECTED_RESULT, 'suppression_reason': SupressionReason.HISTORIC_RESULT}
        create_participant_mock.assert_called_with(expected_result, segregate=True)

        expected_log_calls = [
            call(LogReference.MANAGERESULT008),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_ADD_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'action': 'Routine', 'action_code': 'A', 'comments': 'some free text',
                                        'crm_number': '140540', 'infection_code': '0', 'health_authority': 'B',
                                        'infection_result': 'HPV negative', 'sending_lab': 'akdak',
                                        'result': 'No Cytology test undertaken', 'result_code': 'X',
                                        'sender_code': '299205', 'slide_number': '249797', 'source_code': 'abcd',
                                        'test_date': '2020-04-01'}
            )
        ])
        expected_participant = {'participant_id': '123', 'nhs_number': '9999999999', 'date_of_birth': '1990-01-01',
                                'next_test_due_date': '2021-02-01', 'is_ceased': False}
        autocease_mock.assert_called_once_with(expected_participant, date(2021, 2, 1))
        queue_letter_mock.assert_not_called()

    @patch(f'{module}.should_update_next_test_due_date')
    @patch(f'{module}.get_latest_test_date')
    def test_add_result_without_ntdd_update(
            self,
            get_latest_test_date_mock,
            should_update_mock):
        request_body = REQUEST_BODY.copy()
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
            },
            'session': SESSION
        }

        validate_mandatory_fields_mock.return_value = True, {}
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']

        check_suppression_mock.return_value = None

        generate_crm_list_mock.return_value = [{
            'crm_number': '140540', 'comments': 'some free text',
            'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-10-18T13:48:08.000000'}]

        create_result_mock.return_value = result_record_mock
        get_latest_test_date_mock.return_value = None
        should_update_mock.return_value = (False, None)
        autocease_mock.return_value = False

        actual_response = add_result_service.add_result(event_data, PARTICIPANT)

        assertApiResponse(self, 201, {}, actual_response)

        get_latest_test_date_mock.assert_called_once_with('123')
        create_participant_mock.assert_called_with(EXPECTED_RESULT, segregate=True)

        expected_log_calls = [
            call(LogReference.MANAGERESULT008),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_ADD_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'action': 'Routine', 'action_code': 'A', 'comments': 'some free text',
                                        'crm_number': '140540', 'infection_code': '0', 'health_authority': 'B',
                                        'infection_result': 'HPV negative', 'sending_lab': 'akdak',
                                        'result': 'No Cytology test undertaken', 'result_code': 'X',
                                        'sender_code': '299205', 'slide_number': '249797', 'source_code': 'abcd',
                                        'test_date': '2020-04-01'}

            )
        ])
        expected_participant = {'participant_id': '123', 'nhs_number': '9999999999', 'date_of_birth': '1990-01-01',
                                'next_test_due_date': '2021-02-01', 'is_ceased': False}
        autocease_mock.assert_called_once_with(expected_participant, date(2021, 2, 1))
        queue_letter_mock.assert_called_once_with(EXPECTED_RESULT, False)

    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.should_update_next_test_due_date')
    @patch(f'{module}.get_latest_test_date')
    def test_add_result_with_ntdd_update(
            self,
            get_latest_date_mock,
            should_update_mock,
            transaction_write_mock):
        request_body = REQUEST_BODY.copy()
        request_body.update({
            'next_test_due_date': '2021-01-04',
            'current_next_test_due_date': '2020-01-04',
        })
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
            },
            'session': SESSION
        }

        validate_mandatory_fields_mock.return_value = True, {}
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']

        check_suppression_mock.return_value = None

        generate_crm_list_mock.return_value = [{
            'crm_number': '140540', 'comments': 'some free text',
            'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-10-18T13:48:08.000000'}]

        create_result_mock.return_value = result_record_mock
        get_latest_date_mock.return_value = None
        should_update_mock.return_value = (True, ParticipantStatus.ROUTINE)
        autocease_mock.return_value = False

        actual_response = add_result_service.add_result(event_data, PARTICIPANT)

        assertApiResponse(self, 201, {}, actual_response)

        get_latest_date_mock.assert_called_once_with('123')

        transaction_write_mock.assert_called_once_with([EXPECTED_RESULT], [], [{
            'record_keys': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'PARTICIPANT',
            },
            'update_values': {
                'status': ParticipantStatus.ROUTINE,
                'next_test_due_date': '2021-01-04',
            }
        }], segregate=True)

        expected_log_calls = [
            call(LogReference.MANAGERESULT008),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_ADD_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={
                    'comments': 'some free text', 'crm_number': '140540',
                    'action': 'Routine', 'action_code': 'A', 'health_authority': 'B',
                    'infection_code': '0', 'infection_result': 'HPV negative',
                    'sending_lab': 'akdak', 'result': 'No Cytology test undertaken',
                    'result_code': 'X', 'sender_code': '299205', 'slide_number': '249797',
                    'source_code': 'abcd', 'test_date': '2020-04-01',
                    'updating_next_test_due_date_from': '2020-01-04', 'updating_next_test_due_date_to': '2021-01-04'
                }
            )
        ])
        expected_participant = {'participant_id': '123', 'nhs_number': '9999999999', 'date_of_birth': '1990-01-01',
                                'next_test_due_date': '2021-02-01', 'is_ceased': False}
        autocease_mock.assert_called_once_with(expected_participant, date(2021, 1, 4))
        queue_letter_mock.assert_called_once_with(EXPECTED_RESULT, False)

    def test_add_result_invalid_result(self):
        request_body = REQUEST_BODY.copy()
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
            },
            'session': SESSION
        }

        validate_mandatory_fields_mock.return_value = False, {'statusCode': 400, 'body': 'SOME_ERROR'}

        actual_response = add_result_service.add_result(event_data, PARTICIPANT)

        self.assertDictEqual(actual_response, {'statusCode': 400, 'body': 'SOME_ERROR'})
        create_result_mock.assert_not_called()
        create_participant_mock.assert_not_called()

        log_mock.assert_has_calls([call(LogReference.MANAGERESULT002)])
        audit_mock.assert_not_called()
        queue_letter_mock.assert_not_called()

    @unpack
    @data(
        ('not a date', '2021-01-02', 'Invalid next test due date value provided'),
        ('2021-01-01', 'not a date', 'Invalid current next test due date value provided'),
    )
    def test_add_result_invalid_ntdd(
            self,
            next_test_due_date,
            current_next_test_due_date,
            response_error):
        request_body = REQUEST_BODY.copy()
        request_body.update({
            'next_test_due_date': next_test_due_date,
            'current_next_test_due_date': current_next_test_due_date,
        })
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
            },
            'session': SESSION
        }

        validate_mandatory_fields_mock.return_value = True, {}

        actual_response = add_result_service.add_result(event_data, PARTICIPANT)

        self.assertDictEqual(actual_response, {
            'statusCode': 400,
            'body': json.dumps({
                'message': response_error
            }),
            'headers': {
                'Content-Type': 'application/json',
                'Strict-Transport-Security': 'max-age=1576800',
                'X-Content-Type-Options': 'nosniff'
            },
            'isBase64Encoded': False,
        })
        create_result_mock.assert_not_called()
        create_participant_mock.assert_not_called()

        log_mock.assert_has_calls([call(LogReference.MANAGERESULT002)])
        audit_mock.assert_not_called()
        queue_letter_mock.assert_not_called()

    @patch(f'{module}.get_latest_test_date')
    def test_add_result_should_add_recall_months_to_audit_if_action_is_repeat_advised(
            self,
            get_latest_test_date_mock):
        request_body = REQUEST_BODY.copy()
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
            },
            'session': SESSION
        }

        validate_mandatory_fields_mock.return_value = True, {}
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['action'] = 'Repeat advised'
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']

        generate_crm_list_mock.return_value = [{
            'crm_number': '140540', 'comments': 'some free text',
            'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-10-18T13:48:08.000000'}]

        create_result_mock.return_value = result_record_mock
        get_latest_test_date_mock.return_value = date(2022, 1, 1)

        actual_response = add_result_service.add_result(event_data, PARTICIPANT)

        assertApiResponse(self, 201, {}, actual_response)

        get_latest_test_date_mock.assert_called_once_with('123')
        expected_create_participant_call = {
            **EXPECTED_RESULT,
            'action': 'Repeat advised',
            'suppression_reason': SupressionReason.HISTORIC_RESULT}
        create_participant_mock.assert_called_with(expected_create_participant_call, segregate=True)

        expected_log_calls = [
            call(LogReference.MANAGERESULT008),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_ADD_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'action': 'Repeat advised', 'action_code': 'A', 'comments': 'some free text',
                                        'crm_number': '140540', 'infection_code': '0', 'health_authority': 'B',
                                        'infection_result': 'HPV negative', 'sending_lab': 'akdak',
                                        'result': 'No Cytology test undertaken', 'result_code': 'X',
                                        'sender_code': '299205', 'slide_number': '249797', 'source_code': 'abcd',
                                        'test_date': '2020-04-01', 'recall_months': '36'}
            )
        ])
        expected_participant = {'participant_id': '123', 'nhs_number': '9999999999', 'date_of_birth': '1990-01-01',
                                'next_test_due_date': '2021-02-01', 'is_ceased': False}
        autocease_mock.assert_called_once_with(expected_participant, date(2021, 2, 1))
        queue_letter_mock.assert_not_called()

    @patch(f'{module}.should_update_next_test_due_date')
    @patch(f'{module}.get_latest_test_date')
    def test_add_result_with_suppressed_letter(
            self,
            get_latest_test_date_mock,
            should_update_mock):
        request_body = REQUEST_BODY.copy()
        event_data = {
            'body': request_body,
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
            },
            'session': SESSION
        }

        validate_mandatory_fields_mock.return_value = True, {}
        result_record_mock = EXPECTED_RESULT.copy()
        result_record_mock['crm_number'] = request_body['crm_number']
        result_record_mock['comments'] = request_body['comments']

        check_suppression_mock.return_value = 'Example reason for letter suppression'

        generate_crm_list_mock.return_value = [{
            'crm_number': '140540', 'comments': 'some free text',
            'first_name': 'firsty', 'last_name': 'lasty', 'timestamp': '2020-10-18T13:48:08.000000'}]

        create_result_mock.return_value = result_record_mock
        get_latest_test_date_mock.return_value = None
        should_update_mock.return_value = (False, None)
        autocease_mock.return_value = False

        actual_response = add_result_service.add_result(event_data, PARTICIPANT)

        assertApiResponse(self, 201, {}, actual_response)

        get_latest_test_date_mock.assert_called_once_with('123')
        expected_result = {**EXPECTED_RESULT, 'suppression_reason': 'Example reason for letter suppression'}
        create_participant_mock.assert_called_with(expected_result, segregate=True)

        expected_log_calls = [
            call(LogReference.MANAGERESULT008),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=audit_utils.AuditActions.MANUAL_ADD_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={'action': 'Routine', 'action_code': 'A', 'comments': 'some free text',
                                        'crm_number': '140540', 'infection_code': '0', 'health_authority': 'B',
                                        'infection_result': 'HPV negative', 'sending_lab': 'akdak',
                                        'result': 'No Cytology test undertaken', 'result_code': 'X',
                                        'sender_code': '299205', 'slide_number': '249797', 'source_code': 'abcd',
                                        'test_date': '2020-04-01'}

            )
        ])
        expected_participant = {'participant_id': '123', 'nhs_number': '9999999999', 'date_of_birth': '1990-01-01',
                                'next_test_due_date': '2021-02-01', 'is_ceased': False}
        autocease_mock.assert_called_once_with(expected_participant, date(2021, 2, 1))
        queue_letter_mock.assert_not_called()
