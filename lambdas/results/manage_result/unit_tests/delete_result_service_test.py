from datetime import date
from unittest import TestCase
from mock import patch, call
from manage_result.unit_tests.manage_result_utils_test import (
    PARTICIPANT_ID,
    PARTICIPANT,
    SESSION,
    EXPECTED_RESULT,
)
from common.models.participant import ParticipantStatus
from manage_result.log_references import LogReference
from common.test_assertions.api_assertions import assertApiResponse
from manage_result import delete_result_service
from common.utils.audit_utils import AuditActions


class TestDeleteResultService(TestCase):

    module = 'manage_result.delete_result_service'

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.get_recent_results_by_participant_id')
    def test_delete_service_no_body(self, get_recent_results_mock,
                                    get_test_result_mock, audit_mock,
                                    log_mock, transaction_write_records_mock, autocease_mock):
        get_test_result_mock.return_value = EXPECTED_RESULT
        get_recent_results_mock.return_value = [EXPECTED_RESULT]
        event_data = {
            'body': {},
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'
            },
            'session': SESSION
        }
        actual_response = delete_result_service.delete_result(event_data, PARTICIPANT)

        assertApiResponse(self, 200, {}, actual_response)
        soft_deleted_record = EXPECTED_RESULT.copy()
        soft_deleted_record['sort_key'] = 'DELETED_RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'
        transaction_write_records_mock.assert_called_with(
            [soft_deleted_record],
            [{'participant_id': '123', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}],
            [],
            segregate=True
        )
        expected_log_calls = [
            call(LogReference.MANAGERESULT004),
            call(LogReference.MANAGERESULT016)
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=AuditActions.MANUAL_DELETE_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={
                    'action': 'Routine', 'action_code': 'A',
                    'infection_code': '0', 'infection_result': 'HPV negative',
                    'sending_lab': 'akdak', 'result': 'No Cytology test undertaken', 'result_code': 'X',
                    'sender_code': '299205', 'slide_number': '249797', 'source_code': 'abcd',
                    'test_date': '2020-04-01', 'health_authority': 'B',
                    'crm_number': None,
                    'comments': None
                }
            )
        ])
        autocease_mock.assert_called_once_with(PARTICIPANT, date(2021, 2, 1))

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.get_recent_results_by_participant_id')
    def test_delete_service_no_existing_result(self, get_recent_results_mock,
                                               get_test_result_mock, audit_mock, log_mock, autocease_mock):
        get_test_result_mock.return_value = None
        get_recent_results_mock.return_value = []
        event_data = {
            'body': {'crm_number': 'CAS-56789-QWERT', 'comments': 'Example crm comment text'},
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'
            },
            'session': SESSION
        }
        actual_response = delete_result_service.delete_result(event_data, PARTICIPANT)

        assertApiResponse(self, 404, {'message': 'Result does not exist'}, actual_response)

        expected_log_calls = [
            call(LogReference.MANAGERESULT004),
            call(LogReference.MANAGERESULT013),
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_not_called()
        autocease_mock.assert_not_called()

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.get_recent_results_by_participant_id')
    @patch(f'{module}.should_update_next_test_due_date')
    def test_delete_service_ntdd(self, should_update_mock, get_recent_results_mock,
                                 get_test_result_mock, audit_mock, log_mock,
                                 transaction_write_records_mock, autocease_mock):
        get_test_result_mock.return_value = EXPECTED_RESULT
        get_recent_results_mock.return_value = [EXPECTED_RESULT, {**EXPECTED_RESULT, 'test_date': '2020-12-25'}]
        should_update_mock.return_value = (True, ParticipantStatus.ROUTINE)
        event_data = {
            'body': {'next_test_due_date': '2021-05-01',
                     'crm_number': 'CAS-56789-QWERT',
                     'comments': 'Example crm comment text'},
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'
            },
            'session': SESSION
        }
        actual_response = delete_result_service.delete_result(event_data, PARTICIPANT)

        assertApiResponse(self, 200, {}, actual_response)
        soft_deleted_record = EXPECTED_RESULT.copy()
        soft_deleted_record['sort_key'] = 'DELETED_RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'
        transaction_write_records_mock.assert_called_with(
            [soft_deleted_record],
            [{'participant_id': '123', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}],
            [{
                'record_keys': {'participant_id': '123', 'sort_key': 'PARTICIPANT'},
                'update_values': {
                    'next_test_due_date': '2021-05-01',
                    'status': ParticipantStatus.ROUTINE,
                }
            }],
            segregate=True
        )
        expected_log_calls = [
            call(LogReference.MANAGERESULT004),
            call(LogReference.MANAGERESULT016)
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=AuditActions.MANUAL_DELETE_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={
                    'action': 'Routine', 'action_code': 'A', 'infection_code': '0',
                    'infection_result': 'HPV negative', 'sending_lab': 'akdak', 'result': 'No Cytology test undertaken',
                    'result_code': 'X', 'sender_code': '299205', 'slide_number': '249797', 'source_code': 'abcd',
                    'test_date': '2020-04-01', 'health_authority': 'B',
                    'updating_next_test_due_date_from': '2021-02-01', 'updating_next_test_due_date_to': '2021-05-01',
                    'crm_number': 'CAS-56789-QWERT', 'comments': 'Example crm comment text'
                }
            )
        ])
        autocease_mock.assert_called_once_with(PARTICIPANT, date(2021, 5, 1))

    @patch(f'{module}.autocease_if_eligible')
    @patch(f'{module}.transaction_write_records')
    @patch(f'{module}.log')
    @patch(f'{module}.audit')
    @patch(f'{module}.get_record_from_participant_table')
    @patch(f'{module}.get_recent_results_by_participant_id')
    @patch(f'{module}.should_update_next_test_due_date')
    def test_delete_service_add_recall_months_to_audit_if_action_is_repeat_advised(self,
                                                                                   should_update_mock,
                                                                                   get_recent_results_mock,
                                                                                   get_test_result_mock,
                                                                                   audit_mock,
                                                                                   log_mock,
                                                                                   transaction_write_records_mock,
                                                                                   autocease_mock):
        expected_result = {
            'action': 'Repeat advised',
            'action_code': 'R',
            'health_authority': 'B',
            'infection_code': '0',
            'infection_result': 'HPV negative',
            'sending_lab': 'akdak',
            'nhs_number': '9999999999',
            'participant_id': '123',
            'result': 'No Cytology test undertaken',
            'result_code': 'X',
            'sanitised_nhs_number': '9999999999',
            'sender_code': '299205',
            'slide_number': '249797',
            'recall_months': '36',
            'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00',
            'source_code': 'abcd',
            'test_date': '2020-04-01',
            'created': '2020-11-29T00:00:00.000000+00:00',
            'crm': [
                {
                    'crm_number': '140540',
                    'comments': 'some free text',
                    'first_name': 'firsty',
                    'last_name': 'lasty',
                    'timestamp': '2020-10-18T13:48:08.000000',
                }
            ]
        }
        get_test_result_mock.return_value = expected_result
        get_recent_results_mock.return_value = [expected_result, {**expected_result, 'test_date': '2020-12-25'}]
        should_update_mock.return_value = (True, ParticipantStatus.ROUTINE)
        event_data = {
            'body': {'next_test_due_date': '2021-05-01',
                     'recall_months': '36',
                     'crm_number': 'CAS-56789-QWERT',
                     'comments': 'Example crm comment text'},
            'path_parameters': {
                'participant_id': PARTICIPANT_ID,
                'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'
            },
            'session': SESSION
        }
        actual_response = delete_result_service.delete_result(event_data, PARTICIPANT)

        assertApiResponse(self, 200, {}, actual_response)
        soft_deleted_record = expected_result.copy()
        soft_deleted_record['sort_key'] = 'DELETED_RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'
        transaction_write_records_mock.assert_called_with(
            [soft_deleted_record],
            [{'participant_id': '123', 'sort_key': 'RESULT#2020-04-01#2020-11-29T00:00:00.000000+00:00'}],
            [{
                'record_keys': {'participant_id': '123', 'sort_key': 'PARTICIPANT'},
                'update_values': {
                    'next_test_due_date': '2021-05-01',
                    'status': ParticipantStatus.ROUTINE,
                }
            }],
            segregate=True
        )
        expected_log_calls = [
            call(LogReference.MANAGERESULT004),
            call(LogReference.MANAGERESULT016)
        ]

        log_mock.assert_has_calls(expected_log_calls)
        audit_mock.assert_has_calls([
            call(
                action=AuditActions.MANUAL_DELETE_RESULT,
                session=SESSION,
                participant_ids=[PARTICIPANT['participant_id']],
                nhs_numbers=[PARTICIPANT['nhs_number']],
                additional_information={
                    'action': 'Repeat advised', 'action_code': 'R', 'infection_code': '0',
                    'infection_result': 'HPV negative', 'sending_lab': 'akdak', 'result': 'No Cytology test undertaken',
                    'result_code': 'X', 'sender_code': '299205', 'slide_number': '249797', 'source_code': 'abcd',
                    'test_date': '2020-04-01', 'health_authority': 'B', 'recall_months': '36',
                    'updating_next_test_due_date_from': '2021-02-01', 'updating_next_test_due_date_to': '2021-05-01',
                    'crm_number': 'CAS-56789-QWERT', 'comments': 'Example crm comment text'
                }
            )
        ])
        autocease_mock.assert_called_once_with(PARTICIPANT, date(2021, 5, 1))
