This lambda is being triggered by a POST request to the endpoint /participants/{participant_id}/result **OR**
a PUT request to the endpoint /participants/{participant_id}/result/{sort_key}
it accepts a body like
```
{
  "sending_lab": "akdak",
  "source_code": "abcd",
  "sender_code": "299205",
  "slide_number": "249797",
  "test_date": "2020-04-01",
  "result_code": "X",
  "infection_code": "0",
  "action_code": "A", 
  "next_test_due_date": null,         
  "crm_number": "140540",
  "comments": "some free text"
}
```

and it adds a new [result record](https://nhsd-confluence.digital.nhs.uk/pages/viewpage.action?pageId=108284435) to the participant **OR**
updates a result for a participant
