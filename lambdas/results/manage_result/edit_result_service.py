from datetime import datetime
from urllib.parse import unquote

from common.log import log
from common.models.participant import ParticipantSortKey
from common.utils.audit_utils import audit, AuditActions
from common.utils.participant_utils import (
    get_record_from_participant_table,
    transaction_write_records,
)
from common.utils import generate_crm_list, json_return_message, json_return_object
from manage_result.log_references import LogReference
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator \
    import IGNORE_FOR_CALCULATIONS_MESSAGE
from manage_result.manage_result_utils import (
    create_result_record,
    generate_results_dict,
    validate_mandatory_fields,
    RESULT_FIELDS_TO_AUDIT,
    should_update_next_test_due_date,
    autocease_if_eligible,
    get_participant_ntdd,
    get_recent_results_by_participant_id,
    required_update_ntdd_status,
    UpdateParticipantUsingResult
)
from manage_result.next_test_due_date_service import _get_result_date


def edit_result(event_data, participant):
    log(LogReference.MANAGERESULT003)
    request_body = event_data.get('body')
    participant_id = event_data['path_parameters']['participant_id']
    sort_key = unquote(event_data['path_parameters']['sort_key'])
    recall_months = request_body.get('recall_months', None)
    next_test_due_date_value = request_body.get('next_test_due_date', None)
    current_next_test_due_date_value = request_body.get('current_next_test_due_date', None)
    next_test_due_date = None
    current_next_test_due_date = None

    if next_test_due_date_value:
        try:
            next_test_due_date = datetime.fromisoformat(next_test_due_date_value).date()
        except (TypeError, ValueError) as e:
            log({'log_reference': LogReference.MANAGERESULT023, 'error': str(e)})
            return json_return_message(400, 'Invalid next test due date value provided')

    if current_next_test_due_date_value:
        try:
            current_next_test_due_date = datetime.fromisoformat(current_next_test_due_date_value).date()
        except (TypeError, ValueError) as e:
            log({'log_reference': LogReference.MANAGERESULT024, 'error': str(e)})
            return json_return_message(400, 'Invalid current next test due date value provided')

    existing_result = get_record_from_participant_table(participant_id, sort_key, segregate=True)

    result_values = {key: value for key, value in request_body.items() if key not in
                     ['current_next_test_due_date', 'next_test_due_date']}
    is_valid_result, error_message = validate_edit(existing_result, result_values)

    if not is_valid_result:
        return error_message

    test_date = datetime.fromisoformat(result_values['test_date']).date()

    results = get_recent_results_by_participant_id(participant_id, segregate=True)
    latest_result = results[0]
    previous_results = results[1:] if len(results) >= 1 else []

    latest_result_test_date = _get_result_date(latest_result)

    item_updates = []
    sort_key_match = sort_key == latest_result.get('sort_key')
    try:
        item_updates = _generate_participant_update(
            result_values, previous_results, participant, sort_key_match, test_date, latest_result_test_date,
            current_next_test_due_date, next_test_due_date
        )
    except ValueError as e:
        if str(e) == IGNORE_FOR_CALCULATIONS_MESSAGE:
            log({'log_reference': LogReference.MANAGERESULT040})
        else:
            log({'log_reference': LogReference.MANAGERESULT037, 'error': str(e)})
            return json_return_message(400, 'Invalid next test due date provided')

    updating_next_test_due_date = len(item_updates) > 0

    if check_if_test_date_change(existing_result, result_values):
        key_to_delete = {
            'participant_id': participant_id,
            'sort_key': existing_result['sort_key']
        }
        edited_result = create_result_record(result_values, participant)
        edited_result['result_id'] = existing_result['result_id']
        new_crm = generate_crm_list(edited_result, event_data.get('session'), existing_result.get('crm', []))
        edited_result['crm'] = new_crm
        crm_number = edited_result.pop('crm_number', None)
        comments = edited_result.pop('comments', None)
        # Retain original created date
        existing_created_date = existing_result.get('created')
        if not existing_created_date:
            existing_created_date = existing_result.get('sort_key').split('#')[2]
        new_sort_key = edited_result['sort_key']
        split_new_sort_key = new_sort_key.split('#')
        split_new_sort_key[2] = existing_created_date
        edited_sort_key = '#'.join(split_new_sort_key)
        edited_result.update({
            'sort_key': edited_sort_key,
            'created': existing_created_date
        })
        # Transactionally put edited record into dynamo while deleting original record
        transaction_write_records([edited_result], [key_to_delete], item_updates, segregate=True)

    else:
        key = {
            'participant_id': participant_id,
            'sort_key': sort_key
        }

        update_attributes = generate_results_dict(result_values)

        new_crm = generate_crm_list(update_attributes, event_data.get('session'), existing_result.get('crm', []))
        update_attributes['crm'] = new_crm
        crm_number = update_attributes.pop('crm_number', None)
        comments = update_attributes.pop('comments', None)

        existing_result_attributes = generate_results_dict(existing_result)

        attributes_to_delete = [x for x in existing_result_attributes.keys() if x not in update_attributes.keys()]

        item_updates.append({
            'record_keys': key,
            'update_values': update_attributes,
            'delete_values': attributes_to_delete,
        })
        transaction_write_records([], [], item_updates, segregate=True)
        edited_result = update_attributes

    additional_information = {
        'comments': comments,
        'crm_number': crm_number
    }
    additional_information.update({k: v for k, v in edited_result.items() if k in RESULT_FIELDS_TO_AUDIT})

    additional_information.update({'updating_next_test_due_date_from': current_next_test_due_date_value,
                                   'updating_next_test_due_date_to': next_test_due_date_value}) \
        if updating_next_test_due_date else None

    action = edited_result['action']

    if action == 'Repeat advised':
        additional_information.update({'recall_months': recall_months})

    log(LogReference.MANAGERESULT012)
    audit(
        action=AuditActions.MANUAL_EDIT_RESULT,
        session=event_data.get('session'),
        participant_ids=[participant_id],
        nhs_numbers=[participant['nhs_number']],
        additional_information=additional_information
    )
    final_participant_ntdd = next_test_due_date if updating_next_test_due_date else get_participant_ntdd(participant)
    autocease_if_eligible(participant, final_participant_ntdd)
    return json_return_object(200)


def _update_status(
    result_values, previous_results, participant,
    sort_key_match, test_date, latest_result_test_date,
    current_next_test_due_date, next_test_due_date
):
    for previous_result in previous_results:
        previous_result_test_date = _get_result_date(previous_result)
        try:
            participant_update = required_update_ntdd_status(
                sort_key_match, test_date, latest_result_test_date, previous_result, previous_result_test_date
            )
            if participant_update == UpdateParticipantUsingResult.NONE:
                return False, None

            if participant_update == UpdateParticipantUsingResult.CURRENT:
                correct_result_values = result_values
            elif participant_update == UpdateParticipantUsingResult.PREVIOUS:
                correct_result_values = previous_result
            return should_update_next_test_due_date(
                participant,
                correct_result_values,
                current_next_test_due_date,
                next_test_due_date,
            )
        except ValueError as e:
            if str(e) == IGNORE_FOR_CALCULATIONS_MESSAGE:
                log({'log_reference': LogReference.MANAGERESULT037})
            else:
                raise e
    return False, None


def _generate_participant_update(
    result_values, previous_results, participant, sort_key_match, test_date, latest_result_test_date,
    current_next_test_due_date, next_test_due_date
):
    if previous_results:
        update_next_test_due_date, updated_status = _update_status(
            result_values, previous_results, participant,
            sort_key_match, test_date, latest_result_test_date,
            current_next_test_due_date, next_test_due_date
        )
    else:
        update_next_test_due_date, updated_status = should_update_next_test_due_date(
            participant,
            result_values,
            current_next_test_due_date,
            next_test_due_date,
        )

    return [{
        'record_keys': {
            'participant_id': participant['participant_id'],
            'sort_key': ParticipantSortKey.PARTICIPANT.value
        },
        'update_values': {
            'status': updated_status.value,
            'next_test_due_date': str(next_test_due_date)
        }
    }] if update_next_test_due_date else []


def validate_edit(existing_result, request_body):
    is_valid_result = False
    if not existing_result:
        log(LogReference.MANAGERESULT013)
        return is_valid_result, json_return_message(404, 'Result does not exist')

    is_valid_result, error_message = validate_mandatory_fields(request_body)

    if not is_valid_result:
        return is_valid_result, error_message

    return is_valid_result, None


def check_if_test_date_change(existing_result, request_body):
    existing_date = datetime.fromisoformat(existing_result.get('test_date')).date()
    new_date = datetime.fromisoformat(request_body.get('test_date')).date()

    return existing_date != new_date
