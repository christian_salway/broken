from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_message
from common.utils.api_utils import get_api_request_info_from_event
from common.utils.participant_utils import get_participant_by_participant_id
from manage_result.log_references import LogReference
from manage_result.add_result_service import add_result
from manage_result.edit_result_service import edit_result
from manage_result.delete_result_service import delete_result
from manage_result.next_test_due_date_service import next_test_due_date, next_test_due_date_after_delete


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    BASE_URL = '/api/participants/{participant_id}/results'
    REQUEST_FUNCTIONS = {
        ('POST', BASE_URL): add_result,
        ('PUT', BASE_URL + '/{sort_key}'): edit_result,
        ('DELETE', BASE_URL + '/{sort_key}'): delete_result,
        ('POST', f'{BASE_URL}/next-test-due-date'): next_test_due_date,
        ('GET', BASE_URL + '/next-test-due-date-after-delete/{sort_key}'): next_test_due_date_after_delete,
    }
    event_data, http_method, resource = get_api_request_info_from_event(event)
    request_body = event_data.get('body')
    log({
        'log_reference': LogReference.MANAGERESULT001,
        'request_keys': list(request_body.keys()),
    })

    participant_id = event_data['path_parameters']['participant_id']
    participant = get_participant_by_participant_id(participant_id, segregate=True)

    if not participant:
        log(LogReference.MANAGERESULT005)
        return json_return_message(404, 'Participant does not exist')
    response = REQUEST_FUNCTIONS.get((http_method, resource), bad_request)(event_data, participant)

    return response


def bad_request(event_data, participant):
    log({'log_reference': LogReference.MANAGERESULT011})
    return json_return_message(400, 'unrecognised method/resource combination')
