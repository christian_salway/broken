import uuid
from enum import Enum
from datetime import datetime, timezone
from nose.tools import nottest
from boto3.dynamodb.conditions import Key
from common.log import log
from common.models.participant import CeaseReason
from common.utils import json_return_message
from common.utils.dynamodb_access.segregated_operations import segregated_query
from common.utils.nhs_number_utils import sanitise_nhs_number
from common.utils.cease_utils import (
    participant_eligible_for_auto_cease,
    cease_participant_and_send_notification_messages
)
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.utils.next_test_due_date_calculators.next_test_due_date_calculator \
    import calculate_next_test_due_date_for_result
from common.utils.edifact_dictionaries import (
    CYTOLOGY_RESULTS,
    ACTIONS,
    INFECTION_CODES
)
from common.utils.data_segregation.nhais_ciphers import (
    DMS_CIPHER_CODES,
    ENGLISH_CIPHER_CODES,
    IM_CIPHER_CODES,
    NORTHERN_IRELAND_COUNTRY_CODES,
    RECOGNISED_HISTORIC_CIPHERS,
    WELSH_CIPHER_CODES,
)
from common.utils.edifact_utils import UNKNOWN_CODE_VALUE
from manage_result.log_references import LogReference
from common.utils.dynamodb_access.operations import dynamodb_query
from common.utils.dynamodb_access.table_names import TableNames


VALID_HEALTH_AUTHORITIES = {'A', 'B', 'C', 'F', 'G', 'H', 'L', 'N', 'R', 'S', 'T', 'V', 'W', 'Y', 'Z'}
EXPECTED_KEYS = ['sender_source_type', 'source_code', 'slide_number', 'test_date', 'result_code', 'infection_code',
                 'action_code', 'crm_number', 'comments', 'hpv_primary', 'self_sample', 'sending_lab', 'sender_code',
                 'health_authority', 'recall_months']
RESULT_FIELDS_TO_AUDIT = ['crm_number', 'comments', 'test_date', 'slide_number', 'action', 'action_code', 'result',
                          'result_code', 'infection_result', 'infection_code', 'sending_lab', 'source_code',
                          'sender_code', 'health_authority', 'hpv_primary', 'self_sample']


def validate_mandatory_fields(request_body):
    is_valid = False
    if not request_body or not request_body.get('test_date'):
        log(LogReference.MANAGERESULT006)
        return is_valid, json_return_message(400, 'Missing mandatory fields in the request')

    if 'health_authority' in request_body:
        valid_health_authority = is_valid_health_authority(request_body.get('health_authority'))
        if not valid_health_authority:
            log(LogReference.MANAGERESULT010)
            return is_valid, json_return_message(400, 'Invalid health authority')

    if not is_valid_test_date(request_body.get('test_date')):
        log(LogReference.MANAGERESULT007)
        return is_valid, json_return_message(400, 'Wrong test date is provided')

    is_valid = True
    return is_valid, None


def create_result_record(request_body, participant):
    result = generate_results_dict(request_body)

    test_date = request_body['test_date']
    created = datetime.now(timezone.utc).isoformat(timespec='microseconds')
    nhs_number = participant['nhs_number']
    result.update({
        'result_id': str(uuid.uuid4()),
        'participant_id': participant['participant_id'],
        'sort_key': f'RESULT#{test_date}#{created}',
        'nhs_number': nhs_number,
        'sanitised_nhs_number': sanitise_nhs_number(nhs_number),
        'created': created,
    })

    if request_body.get('action_code', '') == "R":
        result['recall_months'] = request_body['recall_months']
    return result


def generate_results_dict(request_body):
    result = {key: value for (key, value) in request_body.items() if key in EXPECTED_KEYS}

    result_string = CYTOLOGY_RESULTS.get(result.get('result_code'), UNKNOWN_CODE_VALUE)
    infection_result = INFECTION_CODES.get(result.get('infection_code'), UNKNOWN_CODE_VALUE)
    action = ACTIONS.get(result.get('action_code'), UNKNOWN_CODE_VALUE)

    result.update({
        'result': result_string,
        'infection_result': infection_result,
        'action': action
    })

    return result


def is_valid_test_date(date_string):
    try:
        date = datetime.fromisoformat(date_string).date()
        today = datetime.now(timezone.utc).date()
        if date > today:
            return False
    except ValueError:
        return False

    return True


def is_valid_health_authority(health_authority):
    if not health_authority:
        return False
    return health_authority in VALID_HEALTH_AUTHORITIES.union(ENGLISH_CIPHER_CODES, WELSH_CIPHER_CODES,
                                                              DMS_CIPHER_CODES, IM_CIPHER_CODES,
                                                              NORTHERN_IRELAND_COUNTRY_CODES,
                                                              RECOGNISED_HISTORIC_CIPHERS)


def should_update_next_test_due_date(participant, result, current_next_test_due_date, new_next_test_due_date):
    participant_ntdd_value = participant.get('next_test_due_date', None)
    participant_ntdd = None

    # When participant has no next test due date on their record
    if participant_ntdd_value is not None:
        participant_ntdd = datetime.fromisoformat(participant_ntdd_value).date()

    if current_next_test_due_date != participant_ntdd:
        raise ValueError(
            f'Provided current_next_test_due_date ({current_next_test_due_date}) does not'
            f' match participant record ({participant_ntdd})'
        )
    if current_next_test_due_date == new_next_test_due_date:
        return False, None
    test_date = datetime.fromisoformat(result['test_date']).date()
    next_test_due_dates = calculate_next_test_due_date_for_result(
        test_date,
        (result['result_code'], result.get('infection_code'), result['action_code']),
        datetime.fromisoformat(participant['date_of_birth']).date(),
        result.get('hpv_primary', True),
        result.get('self_sample', False),
    )
    valid_dates = [next_test_due_dates['default']['next_test_due_date']] if next_test_due_dates['default'] else []

    if next_test_due_dates['options']:
        for option in next_test_due_dates['options']:
            if option.get('next_test_due_date'):
                valid_dates.append(option['next_test_due_date'])
            else:
                raise ValueError('Invalid option')

    if next_test_due_dates['range']:
        for range_interval in next_test_due_dates['range'][1:]:
            if range_interval.get('next_test_due_date'):
                valid_dates.append(range_interval['next_test_due_date'])
            else:
                raise ValueError('Invalid range interval')

    if new_next_test_due_date not in valid_dates:
        if new_next_test_due_date is None:
            return False, None
        raise ValueError(
            f'Provided new_next_test_due_date {new_next_test_due_date} not a valid option: options are: {valid_dates}')

    return True, next_test_due_dates['status']


@nottest
def get_latest_test_date(participant_id):
    results = get_recent_results_by_participant_id(participant_id, 1, segregate=True)
    return (datetime.fromisoformat(results[0]['test_date']).date()
            if len(results) > 0 else None)


@nottest
def get_latest_and_previous_test_data(participant_id):
    results = get_recent_results_by_participant_id(participant_id, 2, segregate=True)
    latest_test = results[0] if len(results) > 0 else None
    previous_test = results[1] if len(results) > 1 else None

    return latest_test, previous_test


def get_recent_results_by_participant_id(participant_id, limit=None, segregate=False):
    condition_expression = Key('participant_id').eq(participant_id) & Key('sort_key').begins_with('RESULT#')
    args = dict(
        KeyConditionExpression=condition_expression,
        ScanIndexForward=False
    )
    if limit:
        args.update(dict(
            Limit=limit,
        ))

    query_func = segregated_query if segregate else dynamodb_query

    return query_func(TableNames.PARTICIPANTS, args)


def autocease_if_eligible(participant, next_test_due_date: datetime.date):
    log(LogReference.MANAGERESULT037)
    if participant_eligible_for_auto_cease(participant, next_test_due_date, segregate=True):
        log(LogReference.MANAGERESULT038)
        reason = CeaseReason.AUTOCEASE_DUE_TO_AGE
        cease_participant_and_send_notification_messages(
            participant, AuditUsers.SYSTEM.value, {'reason': reason}, segregate=True
        )
        audit(action=AuditActions.CEASE_EPISODE,
              user=AuditUsers.SYSTEM,
              participant_ids=[participant['participant_id']],
              nhs_numbers=[participant['nhs_number']],
              additional_information={
                  'reason': reason,
                  'updating_next_test_due_date_from': next_test_due_date.isoformat()
              })
        return True
    else:
        log(LogReference.MANAGERESULT039)
        return False


def get_participant_ntdd(participant):
    return datetime.fromisoformat(participant['next_test_due_date']).date() \
        if participant.get('next_test_due_date') \
        else None


class UpdateParticipantUsingResult(str, Enum):
    CURRENT = 'CURRENT'
    PREVIOUS = 'PREVIOUS'
    NONE = 'NONE'


def required_update_ntdd_status(
    sort_key_match, test_date, latest_result_test_date, previous_result, previous_result_test_date
):
    if not sort_key_match and test_date > latest_result_test_date:
        log(LogReference.MANAGERESULT032)
        return UpdateParticipantUsingResult.CURRENT

    elif sort_key_match and \
            (previous_result is None or
             test_date > previous_result_test_date):
        log(LogReference.MANAGERESULT033)
        return UpdateParticipantUsingResult.CURRENT

    elif sort_key_match and test_date <= previous_result_test_date:
        log(LogReference.MANAGERESULT034)
        return UpdateParticipantUsingResult.PREVIOUS

    log(LogReference.MANAGERESULT035)
    return UpdateParticipantUsingResult.NONE
