from common.utils.lambda_wrappers import lambda_entry_point
from common.utils import json_return_object, json_return_message
from common.log import log
from common.utils.api_utils import get_api_request_info_from_event
from pause_participant.log_references import LogReference
from pause_participant.pause_participant_service import (
    pause_participant,
    unpause_participant
)


@lambda_entry_point
def lambda_handler(event, context):
    base_url = '/api/csas/results'
    request_functions = {
        ('POST', base_url + '/pause-participant/{participant_id}'): pause_participant,
        ('POST', base_url + '/unpause-participant/{participant_id}'): unpause_participant
    }
    event_data, http_method, resource = get_api_request_info_from_event(event)
    log({'log_reference': LogReference.PAUSEPART0001, 'http_method': http_method})

    try:
        response = request_functions.get((http_method, resource), bad_request)(event_data)
    except Exception as e:
        log({'log_reference': LogReference.PAUSEPART0006, 'error': str(e)})
        return json_return_message(500, 'Cannot complete request')

    log({'log_reference': LogReference.PAUSEPART0002})

    if (response):
        return json_return_object(200, {'data': response})

    return json_return_object(200)


def bad_request(event_data):
    log({'log_reference': LogReference.PAUSEPART0005})
    raise Exception('Unrecognised method/resource combination')
