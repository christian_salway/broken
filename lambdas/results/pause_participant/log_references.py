import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    PAUSEPART0001 = (logging.INFO, 'Received request to pause participant')
    PAUSEPART0002 = (logging.INFO, 'Request successful')
    PAUSEPART0003 = (logging.ERROR, 'Error pausing participant')
    PAUSEPART0004 = (logging.ERROR, 'Error unpausing participant')
    PAUSEPART0005 = (logging.ERROR, 'Requested method/resource not supported')
    PAUSEPART0006 = (logging.ERROR, 'Request unsuccessful')
