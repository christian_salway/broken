from mock import Mock, patch, call
from unittest import TestCase
from pause_participant.log_references import LogReference

MODULE = 'pause_participant.pause_participant'

mock_env_vars = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS'
}


@patch('common.log.logging')
@patch(f'{MODULE}.log')
@patch(f'{MODULE}.get_api_request_info_from_event')
class TestPauseParticipantService(TestCase):

    @patch('boto3.resource', Mock())
    def setUp(self):
        import pause_participant.pause_participant as _pause_participant_module
        self.pause_participant_module = _pause_participant_module
        self.context = Mock()
        self.context.function_name = ''
        self.event = {
            'body': {
                'workflow_state': 'NOT STARTED'
            },
            'path_parameters': {
                'participant_id': '1',
                'crm_number': '2'
            },
            'requestContext': {'authorizer': {'session': '{"session_id": "12345678"}', 'principalId': 'blah'}},
            'headers': {'request_id': 'blah'}
        }
        self.bad_request_response = 'Request failure'
        self.internal_server_error = 'Cannot complete request'

    @patch(f'{MODULE}.pause_participant')
    def test_lambda_handler_calls_pause_participant(
            self,
            pause_participant_mock,
            get_request_info_mock,
            log_mock,
            common_log_mock):
        get_request_info_mock.return_value = (
            {
                'headers': None,
                'body': {
                    'crm_number': '2'
                },
                'path_parameters': {
                    'participant_id': '1'
                }
            },
            'POST',
            '/api/csas/results/pause-participant/{participant_id}'
        )

        pause_participant_mock.return_value = {}

        actual_response = self.pause_participant_module.lambda_handler(self.event, self.context)
        self.assertEqual(200, actual_response['statusCode'])

        expected_pause_participant_call = [
            call({'headers': None, 'body': {'crm_number': '2'}, 'path_parameters': {
                'participant_id': '1'}})]

        pause_participant_mock.assert_has_calls(expected_pause_participant_call)

        expected_log_calls = [call({'log_reference': LogReference.PAUSEPART0001, 'http_method': 'POST'}),
                              call({'log_reference': LogReference.PAUSEPART0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.unpause_participant')
    def test_lambda_handler_calls_unpause_participant(
            self,
            unpause_participant_mock,
            get_request_info_mock,
            log_mock,
            common_log_mock):
        get_request_info_mock.return_value = (
            {
                'headers': None,
                'body': '{}',
                'path_parameters': {
                    'participant_id': '1'
                }
            },
            'POST',
            '/api/csas/results/unpause-participant/{participant_id}'
        )

        unpause_participant_mock.return_value = {}

        actual_response = self.pause_participant_module.lambda_handler(self.event, self.context)
        self.assertEqual(200, actual_response['statusCode'])

        expected_unpause_participant_call = [
            call({'headers': None, 'body': '{}', 'path_parameters': {
                'participant_id': '1'}})]

        unpause_participant_mock.assert_has_calls(expected_unpause_participant_call)

        expected_log_calls = [call({'log_reference': LogReference.PAUSEPART0001, 'http_method': 'POST'}),
                              call({'log_reference': LogReference.PAUSEPART0002})]

        log_mock.assert_has_calls(expected_log_calls)

    def test_lambda_handler_returns_500_on_bad_request(
            self,
            get_request_info_mock,
            log_mock,
            common_log_mock):
        get_request_info_mock.return_value = (
            {
                'headers': None,
                'body': '{}',
            },
            'GET',
            '/api/csas/results/BADURL'
        )

        actual_response = self.pause_participant_module.lambda_handler(self.event, self.context)
        self.assertEqual(500, actual_response['statusCode'])

        expected_log_calls = [call({'log_reference': LogReference.PAUSEPART0001, 'http_method': 'GET'}),
                              call({'log_reference': LogReference.PAUSEPART0005}),
                              call({'log_reference': LogReference.PAUSEPART0006,
                                    'error': 'Unrecognised method/resource combination'})]

        log_mock.assert_has_calls(expected_log_calls)
