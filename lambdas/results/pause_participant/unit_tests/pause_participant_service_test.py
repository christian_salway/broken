from mock import patch, call
from unittest import TestCase
from pause_participant.log_references import LogReference

MODULE = 'pause_participant.pause_participant_service'


class TestPauseParticipantService(TestCase):

    def setUp(self):
        import pause_participant.pause_participant_service as _service_module
        self.service_module = _service_module

    @patch(f'{MODULE}.update_episodes_and_pause_participant_for_manually_added_result')
    def test_pause_participant_passes_data_to_update_function(self, update_episodes_and_pause_mock):
        event_data = {
            'path_parameters': {
                'participant_id': '1',
            },
            'body': {
                'crm_number': '2'
            }
        }

        self.service_module.pause_participant(event_data)

        update_episodes_and_pause_mock.assert_called_once_with('1', '2', None)

    @patch(f'{MODULE}.update_episodes_and_pause_participant_for_manually_added_result')
    @patch(f'{MODULE}.log')
    def test_pause_participant_raises_exception_on_error(self, log_mock, update_episodes_and_pause_mock):
        event_data = {
            'path_parameters': {
                'participant_id': '1',
                'crm_number': '2'
            }
        }

        update_episodes_and_pause_mock.side_effect = Exception()

        with self.assertRaises(Exception):
            self.service_module.pause_participant(event_data)

        expected_log_call = [call({'log_reference': LogReference.PAUSEPART0003})]

        log_mock.assert_has_calls(expected_log_call)

    @patch(f'{MODULE}.update_episodes_and_unpause_participant')
    def test_unpause_participant_passes_data_to_update_function(self, update_episodes_and_unpause_mock):
        event_data = {
            'path_parameters': {
                'participant_id': '1'
            },
            'body': {
                'crm_number': 'CAS-12345-ABCDE',
                'comment': 'A comment'
            }
        }

        self.service_module.unpause_participant(event_data)

        update_episodes_and_unpause_mock.assert_called_once_with('1', 'CAS-12345-ABCDE', 'A comment')

    @patch(f'{MODULE}.update_episodes_and_unpause_participant')
    @patch(f'{MODULE}.log')
    def test_unpause_participant_raises_exception_on_error(self, log_mock, update_episodes_and_unpause_mock):
        event_data = {
            'path_parameters': {
                'participant_id': '1'
            }
        }

        update_episodes_and_unpause_mock.side_effect = Exception()

        with self.assertRaises(Exception):
            self.service_module.unpause_participant(event_data)

        expected_log_call = [call({'log_reference': LogReference.PAUSEPART0004})]

        log_mock.assert_has_calls(expected_log_call)
