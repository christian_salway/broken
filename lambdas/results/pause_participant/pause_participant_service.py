from common.log import log
from .log_references import LogReference
from common.utils.match_result_utils import (
    update_episodes_and_pause_participant_for_manually_added_result,
    update_episodes_and_unpause_participant
)


def pause_participant(event_data):
    try:
        body = event_data['body']
        participant_id = event_data['path_parameters']['participant_id']
        crm_number = body.get('crm_number')
        comment = body.get('comment')
        update_episodes_and_pause_participant_for_manually_added_result(participant_id, crm_number, comment)
    except Exception as e:
        log({'log_reference': LogReference.PAUSEPART0003})
        raise e


def unpause_participant(event_data):
    try:
        body = event_data['body']
        participant_id = event_data['path_parameters']['participant_id']
        crm_number = body.get('crm_number')
        comment = body.get('comment')
        update_episodes_and_unpause_participant(participant_id, crm_number, comment)
    except Exception as e:
        log({'log_reference': LogReference.PAUSEPART0004})
        raise e
