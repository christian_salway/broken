import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    DCD0001 = (logging.INFO, 'Received request to decode lab result')
    DCD0002 = (logging.INFO, 'Successfully received lab result from DynamoDB')
    DCD0003 = (logging.ERROR, 'Could not find lab result for requested result_id')
    DCD0004 = (logging.INFO, 'Successfully decoded lab result')
    DCD0005 = (logging.INFO, 'Updated result in DynamoDB with decoded lab data')
    DCD0006 = (logging.INFO, 'Lab result failed post decoding validation')
    DCD0007 = (logging.INFO, 'Got sender address from nhs cipher')
