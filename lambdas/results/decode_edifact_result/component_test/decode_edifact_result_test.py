import json
import os
from unittest import TestCase
from mock import call, patch, Mock
from datetime import datetime, timezone
from dateutil.relativedelta import relativedelta
from boto3.dynamodb.conditions import Key, Attr
from common.test_mocks.mock_events import sqs_mock_event
from common.utils.dynamodb_access.table_names import TableNames
from decode_edifact_result.decode_edifact_result import lambda_handler
from common.models.participant import ParticipantStatus

sqs_client_mock = Mock()

mock_results_table = Mock()
organisation_directory_utils_table_mock = Mock()
reference_utils_table_mock = Mock()

sqs_identify_duplicate_results_queue_mock = Mock()
sqs_identify_duplicate_results_queue_mock.return_value = 'THE_SQS_QUEUE_URL'

example_date = datetime(2021, 2, 15, 17, 52, 0, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

recent_test_date = (datetime.now(timezone.utc).date()
                    - relativedelta(weeks=1))

DIRECTORY_NAME = os.path.dirname(__file__)

mock_env_vars = {
    'DYNAMODB_RESULTS': 'results_table',
    'DYNAMODB_ORGANISATIONS': 'organisations_table',
    'DYNAMODB_REFERENCE': 'reference_table',
}

mock_event = {
    "internal_id": "internal_id",
    "result_id": "12345678",
    "received_time": "Christmas"
}

mock_results_table_item = {'Item': {
    "result_id": "12345678",
    "raw": "PNA+PAT+9999999999:OPI+++SU:SURNAME1+FS:FORENAME1 FORENAME2'DTM+329:19011965:951"
           + "'NAD+PAT++20 THE STREET SUPERCITY COUNTY:SHIRE ZG97 0QS' S03+3'TST+RCD+X:ZZZ+SNO:"
           + recent_test_date.strftime('%y') + "035247+A:ZZZ'DTM+119:" + recent_test_date.strftime('%Y%m%d')
           + ":102'HEA+INM+0:ZZZ'HEA+ETP+Y:ZZZ'QTY+961:36'S01+1'NAD+SRC+000001:ZZZ'NAD+SND+K81052:ZZZ'S02+2'",
    "received_time": "Christmas",
    "sending_lab": "RSH3",
    "slide_number": recent_test_date.strftime('%y') + "035247",
    "edifact_type": 9,
}}

mock_results_table_item_with_errors = {'Item': {
    "result_id": "12345678",
    "raw": "DTM+329:19011965:951'NAD+PAT++20 THE STREET SUPERCITY COUNTY:SHIRE ZG97 0QS' S03+3'TST+RCD+X:ZZZ+SNO:19115631+A:ZZZ'DTM+119:20191213:102'HEA+INM+0:ZZZ'PCD+Y'HEA+ETP+Y:ZZZ'QTY+961:36'S01+1'NAD+SRC+000001:ZZZ'NAD+SND+K81052:ZZZ'S02+2'",  
    "received_time": "Christmas",
    "sending_lab": "RSH3",
    "slide_number": "19035247",
    "edifact_type": 9,
}}

with open(os.path.join(DIRECTORY_NAME,
                       'test_data/cached_organisations_table_response.json')) as table_response:
    mock_organisation_table_items = json.load(table_response)


def get_table_side_effect(table_enum):
    tables = {
        TableNames.RESULTS: mock_results_table,
        TableNames.ORGANISATIONS: organisation_directory_utils_table_mock,
        TableNames.REFERENCE: reference_utils_table_mock
    }
    return tables[table_enum]


@patch('common.utils.sqs_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('decode_edifact_result.decode_edifact_result.sqs_identify_duplicate_results_queue',
       sqs_identify_duplicate_results_queue_mock)
@patch('os.environ', mock_env_vars)
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(side_effect=get_table_side_effect))
class TestDecodeEdifactResult(TestCase):

    def setUp(self):
        sqs_client_mock.reset_mock()
        mock_results_table.reset_mock()
        organisation_directory_utils_table_mock.reset_mock()
        reference_utils_table_mock.reset_mock()

    @patch('common.utils.organisation_directory_utils.time', Mock(return_value=12345))
    @patch('decode_edifact_result.decode_edifact_result.datetime', datetime_mock)
    def test_add_result_to_queue_if_passed_validation(self):
        # Arrange
        event = sqs_mock_event(mock_event)
        context = Mock()
        context.function_name = 'decode_edifact_result_test'
        mock_results_table.get_item.return_value = mock_results_table_item
        organisation_directory_utils_table_mock.query.return_value = mock_organisation_table_items

        # Act
        lambda_handler(event, context)

        # Assert
        # get_result_by_result_id
        mock_results_table.get_item.assert_called_with(Key={
            'result_id': '12345678',
            'received_time': 'Christmas'
        })
        # add_sender_details_to_result
        organisation_directory_utils_table_mock.query.assert_called_with(
            KeyConditionExpression=Key('organisation_code').eq('K81052'),
            FilterExpression=Attr('expires').gt(12345)
        )
        reference_utils_table_mock.assert_not_called()
        # update_result
        mock_results_table.update_item.assert_called_with(
            Key={
                'result_id': '12345678',
                'received_time': 'Christmas',
            },
            UpdateExpression='SET #decoded.#self_sample = :self_sample, '
                             '#decoded.#hpv_primary = :hpv_primary, '
                             '#decoded.#nhs_number = :nhs_number, '
                             '#decoded.#last_name = :last_name, '
                             '#decoded.#first_name = :first_name, '
                             '#decoded.#date_of_birth = :date_of_birth, '
                             '#decoded.#address = :address, '
                             '#decoded.#slide_number = :slide_number, '
                             '#decoded.#action_code = :action_code, '
                             '#decoded.#action = :action, '
                             '#decoded.#result_code = :result_code, '
                             '#decoded.#result = :result, '
                             '#decoded.#test_date = :test_date, '
                             '#decoded.#infection_code = :infection_code, '
                             '#decoded.#infection_result = :infection_result, '
                             '#decoded.#recall_months = :recall_months, '
                             '#decoded.#sender_source_type = :sender_source_type, '
                             '#decoded.#source_code = :source_code, '
                             '#decoded.#sender_code = :sender_code, '
                             '#decoded.#sanitised_postcode = :sanitised_postcode, '
                             '#decoded.#status = :status, '
                             '#decoded.#sanitised_lab_address = :sanitised_lab_address, '
                             '#decoded.#sender_name = :sender_name, '
                             '#decoded.#sender_address = :sender_address, '
                             '#decoded_time = :decoded_time, '
                             '#slide_number = :slide_number',
            ExpressionAttributeValues={
                ':decoded_time': '2021-02-15T17:52:00+00:00',
                ':self_sample': False,
                ':hpv_primary': True,
                ':nhs_number': '9999999999',
                ':last_name': 'SURNAME1',
                ':first_name': 'FORENAME1 FORENAME2',
                ':date_of_birth': '1965-01-19',
                ':address': '20 THE STREET SUPERCITY COUNTYSHIRE ZG97 0QS',
                ':slide_number': recent_test_date.strftime('%y') + "035247",
                ':action_code': 'A',
                ':action': 'Routine',
                ':result_code': 'X',
                ':result': 'No Cytology test undertaken',
                ':test_date': recent_test_date.isoformat(),
                ':infection_code': '0',
                ':infection_result': 'HPV negative',
                ':recall_months': '36',
                ':source_code': 'G',
                ':sender_source_type': '000001',
                ':sender_code': 'K81052',
                ':sender_name': '108 RAWLING ROAD(RAWLING ROAD PRACTICE)',
                ':sender_address': {
                    'address_line_1': '108 RAWLING ROAD',
                    'address_line_2': 'BENSHAM',
                    'address_line_3': 'GATESHEAD',
                    'address_line_4': 'TYNE AND WEAR',
                    'address_line_5': 'ENGLAND',
                    'postcode': 'NE8 4QR'
                },
                ':sanitised_postcode': 'ZG970QS',
                ':status': ParticipantStatus.ROUTINE,
                ':sanitised_lab_address': {
                    'address_line_1': '20 THE STREET SUPERCITY COUNTYSHIRE',
                    'address_line_2': '',
                    'address_line_3': '',
                    'address_line_4': '',
                    'address_line_5': '',
                    'postcode': 'ZG970QS'
                }
            },
            ExpressionAttributeNames={
                '#decoded': 'decoded',
                '#decoded_time': 'decoded_time',
                '#self_sample': 'self_sample',
                '#hpv_primary': 'hpv_primary',
                '#nhs_number': 'nhs_number',
                '#last_name': 'last_name',
                '#first_name': 'first_name',
                '#date_of_birth': 'date_of_birth',
                '#address': 'address',
                '#slide_number': 'slide_number',
                '#action_code': 'action_code',
                '#action': 'action',
                '#result_code': 'result_code',
                '#result': 'result',
                '#test_date': 'test_date',
                '#infection_code': 'infection_code',
                '#infection_result': 'infection_result',
                '#recall_months': 'recall_months',
                '#sender_source_type': 'sender_source_type',
                '#source_code': 'source_code',
                '#sender_code': 'sender_code',
                '#sender_name': 'sender_name',
                '#sender_address': 'sender_address',
                '#sanitised_postcode': 'sanitised_postcode',
                '#status': 'status',
                '#sanitised_lab_address': 'sanitised_lab_address'
            }
        )
        # send_new_message
        sqs_client_mock.send_message.assert_called_with(
            QueueUrl='THE_SQS_QUEUE_URL',
            MessageBody=json.dumps({
                'result_id': '12345678',
                'received_time': 'Christmas',
                'internal_id': 'internal_id'
            }, indent=4, sort_keys=True, default=str))

    @patch('common.utils.organisation_directory_utils.time', Mock(return_value=12345))
    @patch('decode_edifact_result.decode_edifact_result.datetime', datetime_mock)
    def test_reject_result_and_dont_add_to_queue_if_failed_validation(
            self):
        # Arrange
        event = sqs_mock_event(mock_event)
        context = Mock()
        context.function_name = 'decode_edifact_result_test'
        mock_results_table.get_item.return_value = mock_results_table_item_with_errors
        organisation_directory_utils_table_mock.query.return_value = mock_organisation_table_items

        # Act
        lambda_handler(event, context)

        # Assert
        # get_result_by_result_id
        mock_results_table.get_item.assert_called_with(Key={
            'result_id': '12345678',
            'received_time': 'Christmas'
        })
        # add_sender_details_to_result
        organisation_directory_utils_table_mock.query.assert_called_with(
            KeyConditionExpression=Key('organisation_code').eq('K81052'),
            FilterExpression=Attr('expires').gt(12345)
        )
        reference_utils_table_mock.assert_not_called()
        # update_result
        initial_update_call = call(
            Key={
                'result_id': '12345678',
                'received_time': 'Christmas'
            },
            UpdateExpression='SET #decoded.#self_sample = :self_sample, '
                             '#decoded.#hpv_primary = :hpv_primary, '
                             '#decoded.#date_of_birth = :date_of_birth, '
                             '#decoded.#address = :address, '
                             '#decoded.#slide_number = :slide_number, '
                             '#decoded.#action_code = :action_code, '
                             '#decoded.#action = :action, '
                             '#decoded.#result_code = :result_code, '
                             '#decoded.#result = :result, '
                             '#decoded.#test_date = :test_date, '
                             '#decoded.#infection_code = :infection_code, '
                             '#decoded.#infection_result = :infection_result, '
                             '#decoded.#recall_months = :recall_months, '
                             '#decoded.#sender_source_type = :sender_source_type, '
                             '#decoded.#source_code = :source_code, '
                             '#decoded.#sender_code = :sender_code, '
                             '#decoded.#sanitised_postcode = :sanitised_postcode, '
                             '#decoded.#status = :status, '
                             '#decoded.#sanitised_lab_address = :sanitised_lab_address, '
                             '#decoded.#validation_errors = :validation_errors, '
                             '#decoded.#sender_name = :sender_name, '
                             '#decoded.#sender_address = :sender_address, '
                             '#decoded_time = :decoded_time, '
                             '#slide_number = :slide_number',
            ExpressionAttributeValues={
                ':decoded_time': '2021-02-15T17:52:00+00:00',
                ':self_sample': False, ':hpv_primary': True, ':date_of_birth': '1965-01-19',
                ':address': '20 THE STREET SUPERCITY COUNTYSHIRE ZG97 0QS',
                ':slide_number': '19115631',
                ':action_code': 'A',
                ':action': 'Routine',
                ':result_code': 'X',
                ':result': 'No Cytology test undertaken',
                ':test_date': '2019-12-13',
                ':infection_code': '0',
                ':infection_result': 'HPV negative',
                ':recall_months': '36',
                ':sender_source_type': '000001',
                ':source_code': 'G',
                ':sender_source_type': '000001',
                ':sender_code': 'K81052',
                ':sender_name': '108 RAWLING ROAD(RAWLING ROAD PRACTICE)',
                ':sender_address': {
                    'address_line_1': '108 RAWLING ROAD',
                    'address_line_2': 'BENSHAM',
                    'address_line_3': 'GATESHEAD',
                    'address_line_4': 'TYNE AND WEAR',
                    'address_line_5': 'ENGLAND',
                    'postcode': 'NE8 4QR'
                },
                ':sanitised_postcode': 'ZG970QS',
                ':status': ParticipantStatus.ROUTINE,
                ':sanitised_lab_address': {
                    'address_line_1': '20 THE STREET SUPERCITY COUNTYSHIRE',
                    'address_line_2': '',
                    'address_line_3': '',
                    'address_line_4': '',
                    'address_line_5': '',
                    'postcode': 'ZG970QS'
                },
                ':validation_errors': [
                    'Missing NHS number',
                    'Missing first name',
                    'Missing last name',
                    'Test date > 1 year old',
                    'Invalid slide number year'
                ]},
            ExpressionAttributeNames={
                '#decoded': 'decoded',
                '#decoded_time': 'decoded_time',
                '#self_sample': 'self_sample',
                '#hpv_primary': 'hpv_primary',
                '#date_of_birth': 'date_of_birth',
                '#address': 'address',
                '#slide_number': 'slide_number',
                '#action_code': 'action_code',
                '#action': 'action',
                '#result_code': 'result_code',
                '#result': 'result',
                '#test_date': 'test_date',
                '#infection_code': 'infection_code',
                '#infection_result': 'infection_result',
                '#recall_months': 'recall_months',
                '#sender_source_type': 'sender_source_type',
                '#source_code': 'source_code',
                '#sender_code': 'sender_code',
                '#sender_name': 'sender_name',
                '#sender_address': 'sender_address',
                '#sanitised_postcode': 'sanitised_postcode',
                '#status': 'status',
                '#sanitised_lab_address': 'sanitised_lab_address',
                '#validation_errors': 'validation_errors'
            }
        )

        self.assertEqual(mock_results_table.update_item.call_count, 2)
        mock_results_table.update_item.assert_has_calls([initial_update_call])

        # send_new_message
        sqs_client_mock.send_message.assert_called_with(
            QueueUrl='THE_SQS_QUEUE_URL',
            MessageBody=json.dumps({
                'result_id': '12345678',
                'received_time': 'Christmas',
                'internal_id': 'internal_id'
            }, indent=4, sort_keys=True, default=str))
