from datetime import datetime, timezone
from unittest.case import TestCase

from ddt import data, ddt, unpack
from mock import Mock, patch

import decode_edifact_result.result_validator as environment
from common.models.participant import ParticipantStatus
from common.models.result import SELF_SAMPLE_SENDER_CODE
from decode_edifact_result.result_validator import \
    sanitise_lab_address, validate_result_and_set_recall_details, \
    _validate_result_codes, _validate_required_fields_present, _validate_slide_number, \
    _validate_date_of_birth_and_test_date, \
    _validate_recall_months

VALID_SENDER_MOCK = {
    'name': 'Sender Name',
    'address': {
        'address_line_1': '41',
        'address_line_2': 'Sender street',
        'address_line_3': 'Sender city',
        'address_line_4': 'Sendershire',
        'address_line_5': 'Sendoria',
        'postcode': 'SN3 4DR'
    }
}


@patch.object(environment, 'datetime', Mock(wraps=datetime))
@ddt
class TestResultValidator(TestCase):
    def setUp(self):
        self.maxDiff = None

    FIXED_NOW_TIME = datetime(2020, 8, 1, 9, 45, 23, tzinfo=timezone.utc)

    def test_validator_sets_validation_errors_on_decoded_result_if_errors(self):
        edifact_type = 3
        decoded_result = {'address': 'nonsense'}

        expected_result = {
            'address': 'nonsense',
            'sanitised_lab_address': {
                'address_line_1': 'nonsense',
                'address_line_2': '',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': ''
            },
            'validation_errors': [
                'Missing NHS number', 'Missing first name', 'Missing last name',
                'Missing date of birth', 'Missing test date', 'Missing slide number',
                'Missing sender code', 'Postcode is missing or invalid',
                "Result code '', infection code '' and action code '' are not a valid combination",
            ]
        }

        validated_result = validate_result_and_set_recall_details(edifact_type, decoded_result)

        self.assertDictEqual(expected_result, validated_result)

    def test___validate_result_codes_for_type_9(self):
        edifact_type = 9
        decoded_result = {
            'decoded_key': 'decoded_value',
            'result_code': 'X',
            'infection_code': '',
            'action_code': 'R'
        }
        validation_errors = []

        _validate_result_codes(decoded_result, validation_errors, edifact_type)

        self.assertListEqual(validation_errors, ['Value for infection_code is missing'])

    @unpack
    @data(('X', 'U', 'H', False, ["Result code 'X', infection code 'U' and action code 'H' are only a valid combination for self sampled results"]),  
          ('X', '9', 'R', False, ["Result code 'X', infection code '9' and action code 'R' are only a valid combination for self sampled results"]),  
          ('X', 'U', 'H', True, []),
          ('X', '9', 'R', True, []))
    def test__validate_result_codes_sets_correct_errors_for_type_3(
            self, result_code, infection_code, action_code, self_sample, expected_errors):
        edifact_type = 3
        decoded_result = {
            'decoded_key': 'decoded_value',
            'result_code': result_code,
            'infection_code': infection_code,
            'action_code': action_code,
            'self_sample': self_sample,
        }
        validation_errors = []

        _validate_result_codes(decoded_result, validation_errors, edifact_type)

        self.assertListEqual(validation_errors, expected_errors)

    def test_validator_sets_correct_errors_when_mandatory_fields_missing(self):
        decoded_result = {'decoded_key': 'decoded_value'}
        validation_errors = []
        expected_validation_errors = [
            'Missing NHS number', 'Missing first name', 'Missing last name',
            'Missing address', 'Missing date of birth', 'Missing test date',
            'Missing slide number', 'Missing sender code'
        ]

        _validate_required_fields_present(decoded_result, validation_errors)

        self.assertListEqual(expected_validation_errors, validation_errors)

    def test_validator_sets_no_errors_when_mandatory_fields_all_present(self):
        validation_errors = []
        decoded_result = {
            'nhs_number': '9900000000', 'first_name': 'test', 'last_name': 'person', 'address': 'place',
            'date_of_birth': '26091994', 'test_date': '2020-08-01', 'slide_number': '123456', 'sender_code': 'code'
        }

        _validate_required_fields_present(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 0)

    def test_validator_sets_no_errors_when_nhs_number_has_question_mark(self):
        validation_errors = []
        decoded_result = {
            'nhs_number': '?900000000', 'first_name': 'test', 'last_name': 'person', 'address': 'place',
            'date_of_birth': '26091994', 'test_date': '2020-08-01', 'slide_number': '123456', 'sender_code': 'code'
        }

        _validate_required_fields_present(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 0)

    @unpack
    @data(
        (
            {
                'date_of_birth': '17101994', 'date_of_birth_format': '%d%m%Y',
                'test_date': '20200731', 'test_date_format': '%Y%m%d'
            },
            {
                'date_of_birth': '1994-10-17', 'test_date': '2020-07-31'
            }
        ),
        (
            {
                'date_of_birth': '19941017', 'date_of_birth_format': '%Y%m%d',
                'test_date': '16374', 'test_date_format': '%d%m%Y'
            },
            {
                'date_of_birth': '1994-10-17', 'test_date': '16374',
                'validation_errors': ['Invalid test date']
            }
        ),
        (
            {
                'date_of_birth': '16374', 'date_of_birth_format': '%Y%m%d',
                'test_date': '20200731', 'test_date_format': '%Y%m%d'
            },
            {
                'date_of_birth': '16374', 'test_date': '2020-07-31',
                'validation_errors': ['Invalid date of birth']
            }
        ),
        (
            {
                'date_of_birth': '20200801', 'date_of_birth_format': '%Y%m%d',
                'test_date': '20200731', 'test_date_format': '%Y%m%d'
            },
            {
                'date_of_birth': '2020-08-01', 'test_date': '2020-07-31',
                'validation_errors': ['Test date is before date of birth']
            }
        ),
        (
            {
                'date_of_birth': '17101994', 'date_of_birth_format': '%d%m%Y',
                'test_date': '02082020', 'test_date_format': '%d%m%Y'
            },
            {
                'date_of_birth': '1994-10-17', 'test_date': '2020-08-02',
                'validation_errors': ['Test date is in the future']
            }
        ),
        (
            {
                'test_date': '02082020', 'test_date_format': '%d%m%Y'
            },
            {
                'test_date': '2020-08-02',
            }
        ),
        (
            {
                'date_of_birth': '17101994', 'date_of_birth_format': '%d%m%Y',
            },
            {
                'date_of_birth': '1994-10-17',
            }
        ),
        (
            {
                'date_of_birth': '17101994', 'date_of_birth_format': '%d%m%Y',
                'test_date': '20190731', 'test_date_format': '%Y%m%d'
            },
            {
                'date_of_birth': '1994-10-17', 'test_date': '2019-07-31',
                'validation_errors': ['Test date > 1 year old']
            }
        ),
        (
            {
                'date_of_birth': '17101994', 'date_of_birth_format': '%d%m%Y',
                'test_date': '20190801', 'test_date_format': '%Y%m%d'
            },
            {
                'date_of_birth': '1994-10-17', 'test_date': '2019-08-01'
            }
        )
    )
    @patch('decode_edifact_result.result_validator._calculate_today_date_minus_11_years')
    @patch('decode_edifact_result.result_validator.validate_edifact_postcode')
    @patch('decode_edifact_result.result_validator._validate_required_fields_present')
    @patch('decode_edifact_result.result_validator._validate_result_codes')
    def test_validator_sets_date_validation_errors_if_date_of_birth_or_test_date_invalid(
            self, decoded_result, expected_validated_result, mock_result_code_validator,
            mock_field_validator, mock_postcode_validator, date_minus_11_years_mock):
        date_minus_11_years_mock.return_value = self.FIXED_NOW_TIME.date().isoformat()
        environment.datetime.now.return_value = self.FIXED_NOW_TIME
        edifact_type = 9

        actual_validated_result = validate_result_and_set_recall_details(
            edifact_type, decoded_result)

        self.assertDictEqual(expected_validated_result, actual_validated_result)

    @unpack
    @data(
        (
            'an address with no postcode',
            {
                'address_line_1': 'an address with no postcode',
                'address_line_2': '',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': ''
            },
            ['Postcode is missing or invalid']
        ),
        (
            'address with valid postcode by is_valid but no is_completed Exeter',
            {
                'address_line_1': 'address with valid postcode by',
                'address_line_2': 'is_valid but no is_completed',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': 'EXETER'
            },
            ['Postcode is missing or invalid']
        )
    )
    @patch('decode_edifact_result.result_validator._validate_required_fields_present')
    @patch('decode_edifact_result.result_validator._validate_result_codes')
    def test_validator_sets_correct_error_when_address_has_no_valid_complete_postcode(
            self, address, sanitised_address, validation_errors, mock_result_code_validator, mock_field_validator):
        edifact_type = 9
        decoded_result = {'address': address}
        expected_result = {'address': address, 'sanitised_lab_address': sanitised_address,
                           'validation_errors': validation_errors}

        validated_result = validate_result_and_set_recall_details(edifact_type, decoded_result)

        self.assertDictEqual(expected_result, validated_result)

    @patch('decode_edifact_result.result_validator._validate_result_codes')
    @patch('decode_edifact_result.result_validator._validate_required_fields_present')
    def test_validator_sets_sanitised_postcode_when_address_postcode_has_no_spaces(
            self, mock_field_validator, mock_result_code_validator):
        edifact_type = 3
        decoded_result = {'address': 'an address ls12Eq'}
        expected_result = {
            'address': 'an address ls12Eq',
            'sanitised_lab_address': {
                'address_line_1': 'an address',
                'address_line_2': '',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': 'LS12EQ'
            },
            'sanitised_postcode': 'LS12EQ'}

        validated_result = validate_result_and_set_recall_details(edifact_type, decoded_result)

        self.assertDictEqual(expected_result, validated_result)

    @patch('decode_edifact_result.result_validator._validate_result_codes')
    @patch('decode_edifact_result.result_validator._validate_required_fields_present')
    def test_validator_sets_sanitised_postcode_when_address_postcode_has_spaces(
            self, mock_field_validator, mock_result_code_validator):
        edifact_type = 3
        decoded_result = {'address': 'an address ls1 2Eq'}
        expected_result = {
            'address': 'an address ls1 2Eq',
            'sanitised_lab_address': {
                'address_line_1': 'an address',
                'address_line_2': '',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': 'LS12EQ'
            }, 'sanitised_postcode': 'LS12EQ'}

        validated_result = validate_result_and_set_recall_details(edifact_type, decoded_result)

        self.assertDictEqual(expected_result, validated_result)

    @patch('decode_edifact_result.result_validator._set_status_for_result', Mock())
    @patch('decode_edifact_result.result_validator.validate_edifact_postcode', Mock())
    @patch('decode_edifact_result.result_validator._validate_required_fields_present', Mock())
    def test_validator_sets_error_when_result_and_action_codes_not_a_valid_combination(self):
        edifact_type = 3
        decoded_result = {'result_code': 'W', 'infection_code': 'Z', 'action_code': 'T'}
        expected_result = {'result_code': 'W', 'infection_code': 'Z', 'action_code': 'T',
                           'validation_errors': ["Result code 'W', infection code 'Z' and action code 'T' are not a "
                                                 "valid combination"]}

        validated_result = validate_result_and_set_recall_details(edifact_type, decoded_result)

        self.assertDictEqual(expected_result, validated_result)

    @unpack
    @data(('X', 'U', 'H', True,  '36', SELF_SAMPLE_SENDER_CODE, 'Unchanged'),
          ('X', '9', 'R', True,  '3',  SELF_SAMPLE_SENDER_CODE, ParticipantStatus.REPEAT_ADVISED),
          ('X', '0', 'R', False, '36', '', ParticipantStatus.REPEAT_ADVISED),
          ('X', '0', 'A', False, '36', '', ParticipantStatus.ROUTINE),
          ('X', 'U', 'R', False, '3',  '', ParticipantStatus.INADEQUATE),
          ('X', 'U', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('X', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('0', '9', 'R', False, '12', '', ParticipantStatus.REPEAT_ADVISED),
          ('0', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('1', '9', 'R', False, '3',  '', ParticipantStatus.INADEQUATE),
          ('1', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('2', '9', 'R', False, '12', '', ParticipantStatus.REPEAT_ADVISED),
          ('2', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('3', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('4', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('5', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('6', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('7', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('8', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED),
          ('9', '9', 'S', False, '36', '', ParticipantStatus.SUSPENDED))
    @patch('decode_edifact_result.result_validator.validate_edifact_postcode')
    @patch('decode_edifact_result.result_validator._validate_required_fields_present')
    def test_validator_allows_valid_action_and_result_codes_and_sets_recall_status(
            self, result_code, infection_code, action_code, self_sample, recall_months, sender_code,
            expected_recall_status,
            mock_field_validator, mock_postcode_validator):
        edifact_type = 3
        decoded_result = {'result_code': result_code, 'infection_code': infection_code, 'action_code': action_code,
                          'self_sample': self_sample, 'sender_code': sender_code, 'recall_months': recall_months}
        expected_result = {'result_code': result_code, 'infection_code': infection_code, 'action_code': action_code,
                           'self_sample': self_sample, 'sender_code': sender_code, 'recall_months': recall_months,
                           'status': expected_recall_status}

        validated_result = validate_result_and_set_recall_details(edifact_type, decoded_result)

        self.assertDictEqual(expected_result, validated_result)

    @unpack
    @data(('R', 3), ('S', 3), ('A', 3))
    @patch('decode_edifact_result.result_validator.validate_edifact_postcode')
    @patch('decode_edifact_result.result_validator._validate_required_fields_present')
    def test_validator_does_not_change_recall_months_if_already_set(
            self, action_code, expected_recall_months, mock_field_validator, mock_postcode_validator):
        edifact_type = 3
        decoded_result = {'result_code': '1', 'infection_code': '9', 'action_code': action_code, 'recall_months': 3}

        validated_result = validate_result_and_set_recall_details(edifact_type, decoded_result)

        self.assertEqual(expected_recall_months, validated_result['recall_months'])

    def test_validator_sets_error_when_result_codes_missing(self):
        decoded_result = {'result_code': 'X', 'infection_code': 'Y', 'action_code': ''}
        expected_validation_errors = ['Value for action_code is missing']
        validation_errors = []

        _validate_result_codes(decoded_result, validation_errors, 9)

        self.assertEqual(validation_errors, expected_validation_errors)

    def test_validate_address_is_sanitised_correctly(self):
        decoded_result = {'address': '308 Negra Arroyo Lane, Albuquerque, New Mexico, XX10 0AX'}
        expected_decoded_result = dict(decoded_result)
        expected_decoded_result['sanitised_lab_address'] = {
            'address_line_1': '308 Negra Arroyo Lane Albuquerque',
            'address_line_2': 'New Mexico',
            'address_line_3': '',
            'address_line_4': '',
            'address_line_5': '',
            'postcode': 'XX100AX'
        }

        sanitise_lab_address(decoded_result)

        self.assertEqual(decoded_result, expected_decoded_result)

    def test_validate_address_is_sanitised_does_nothing_when_address_is_missing(self):
        decoded_result = {'address': ''}
        expected_decoded_result = dict(decoded_result)

        sanitise_lab_address(decoded_result)

        self.assertEqual(decoded_result, expected_decoded_result)

    @unpack
    @data([' '], ['          '], ['           '])
    def test_validate_nhs_number_when_value_is_any_number_of_spaces(self, blank_nhs_number):
        validation_errors = []
        decoded_result = {
            'nhs_number': blank_nhs_number, 'first_name': 'test', 'last_name': 'person', 'address': 'place',
            'date_of_birth': '26091994', 'test_date': '2020-08-01', 'slide_number': '123456', 'sender_code': 'code'
        }

        _validate_required_fields_present(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 0)

    @unpack
    @data([' '], ['          '], ['           '])
    def test_validate_first_name_when_value_is_any_number_of_spaces(self, blank_first_name):
        validation_errors = []
        decoded_result = {
            'nhs_number': '1234567890', 'first_name': blank_first_name, 'last_name': 'person', 'address': 'place',
            'date_of_birth': '26091994', 'test_date': '2020-08-01', 'slide_number': '123456', 'sender_code': 'code'
        }

        _validate_required_fields_present(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 0)

    @unpack
    @data([' '], ['          '], ['           '])
    def test_validate_last_name_when_value_is_any_number_of_spaces(self, blank_last_name):
        validation_errors = []
        decoded_result = {
            'nhs_number': '1234567890', 'first_name': 'test', 'last_name': blank_last_name, 'address': 'place',
            'date_of_birth': '26091994', 'test_date': '2020-08-01', 'slide_number': '123456', 'sender_code': 'code'
        }

        _validate_required_fields_present(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 0)

    @unpack
    @data([' '], ['          '], ['           '])
    def test_validate_address_when_value_is_any_number_of_spaces(self, blank_address):
        validation_errors = []
        decoded_result = {
            'nhs_number': '1234567890', 'first_name': 'test', 'last_name': 'person', 'address': blank_address,
            'date_of_birth': '26091994', 'test_date': '2020-08-01', 'slide_number': '123456', 'sender_code': 'code'
        }

        _validate_required_fields_present(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 0)

    def test_validate_date_of_birth_and_test_date_fails_when_age_is_less_than_11(self):
        environment.datetime.now.return_value = self.FIXED_NOW_TIME
        expected_decoded_result = {
            'date_of_birth': '2011-06-21',
            'test_date': '2020-01-06'
        }

        validation_errors = []
        decoded_result = {
            'date_of_birth': '21062011',
            'date_of_birth_format': '%d%m%Y',
            'test_date': '20200106',
            'test_date_format': '%Y%m%d'
        }

        _validate_date_of_birth_and_test_date(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 1)
        self.assertEqual('Participant age < 11', validation_errors[0])
        self.assertEqual(expected_decoded_result, decoded_result)

    @unpack
    @data(['123abc12'], ['999999ab'], ['123_A*B-'])
    def test_validate_slide_number_fails_when_characters_are_alphanumeric(self, slide_number):
        validation_errors = []
        decoded_result = {
            'slide_number': slide_number
        }

        _validate_slide_number(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 1)
        self.assertEqual('Invalid slide number format', validation_errors[0])

    @unpack
    @data(['1234567'], ['123456789'])
    def test_validate_slide_number_fails_when_length_not_equal_to_8(self, slide_number):
        validation_errors = []
        decoded_result = {
            'slide_number': slide_number
        }

        _validate_slide_number(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 1)
        self.assertEqual('Invalid slide number format', validation_errors[0])

    @unpack
    @data(['20693274'])
    def test_validate_slide_number_passes_when_first_two_digits_match_current_year(self, slide_number):
        environment.datetime.now.return_value = self.FIXED_NOW_TIME

        validation_errors = []
        decoded_result = {
            'slide_number': slide_number
        }

        _validate_slide_number(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 0)

    @unpack
    @data(['12693274'])
    def test_validate_slide_number_fails_when_first_two_digits_do_not_match_current_year(self, slide_number):
        environment.datetime.now.return_value = self.FIXED_NOW_TIME
        validation_errors = []
        decoded_result = {
            'slide_number': slide_number
        }

        _validate_slide_number(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 1)
        self.assertEqual('Invalid slide number year', validation_errors[0])

    @unpack
    @data(['19693274'])
    def test_validate_slide_number_passes_in_jan_if_digits_match_last_year(self, slide_number):
        environment.datetime.now.return_value = datetime(2020, 1, 15, 9, 45, 23, tzinfo=timezone.utc)

        validation_errors = []
        decoded_result = {
            'slide_number': slide_number
        }

        _validate_slide_number(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 0)

    @unpack
    @data(['18693274'])
    def test_validate_slide_number_fails_in_jan_if_digits_match_year_older_than_last(self, slide_number):
        environment.datetime.now.return_value = datetime(2020, 1, 15, 9, 45, 23, tzinfo=timezone.utc)

        validation_errors = []
        decoded_result = {
            'slide_number': slide_number
        }

        _validate_slide_number(decoded_result, validation_errors)

        self.assertTrue(len(validation_errors) == 1)
        self.assertEqual('Invalid slide number year', validation_errors[0])

    @unpack
    @data(
        ({'result_code': 'X', 'infection_code': '0', 'action_code': 'R', 'self_sample': False, 'recall_months': '2'}, 2),     
        ({'result_code': 'X', 'infection_code': 'U', 'action_code': 'R', 'self_sample': False, 'recall_months': '2'}, 2),     
        ({'result_code': 'X', 'infection_code': '9', 'action_code': 'R', 'self_sample': True,  'recall_months': '2'}, 2),     
        ({'result_code': '1', 'infection_code': '9', 'action_code': 'R', 'self_sample': False, 'recall_months': '2'}, 2),     
        ({'result_code': '2', 'infection_code': '9', 'action_code': 'R', 'self_sample': False, 'recall_months': '2'}, 2),     
        ({'result_code': '0', 'infection_code': '9', 'action_code': 'R', 'self_sample': False, 'recall_months': '2'}, 2)      
    )
    def test_validate_recall_months_rejects_invalid_values(self, decoded_result, recall_months):
        validation_errors = []
        expected_validation_errors = [
            f'Invalid recall months: {recall_months}'
        ]

        _validate_recall_months(decoded_result, validation_errors)

        self.assertListEqual(expected_validation_errors, validation_errors)

    @unpack
    @data(
        ({'result_code': 'X', 'infection_code': '0', 'action_code': 'R', 'self_sample': False, 'recall_months': '6'},  []),   
        ({'result_code': 'X', 'infection_code': '0', 'action_code': 'R', 'self_sample': False, 'recall_months': '12'}, []),   
        ({'result_code': 'X', 'infection_code': '0', 'action_code': 'R', 'self_sample': False, 'recall_months': '36'}, []),   
        ({'result_code': 'X', 'infection_code': 'U', 'action_code': 'R', 'self_sample': False, 'recall_months': '3'},  []),   
        ({'result_code': 'X', 'infection_code': '9', 'action_code': 'R', 'self_sample': True,  'recall_months': '3'},  []),   
        ({'result_code': '1', 'infection_code': '9', 'action_code': 'R', 'self_sample': False, 'recall_months': '3'},  []),   
        ({'result_code': '2', 'infection_code': '9', 'action_code': 'R', 'self_sample': False, 'recall_months': '12'}, []),   
        ({'result_code': '0', 'infection_code': '9', 'action_code': 'R', 'self_sample': False, 'recall_months': '12'}, [])    
    )
    def test_validate_recall_months_accepts_valid_values(self, decoded_result, expected_validation_errors):
        validation_errors = []

        _validate_recall_months(decoded_result, validation_errors)

        self.assertListEqual(expected_validation_errors, validation_errors)
