from unittest import TestCase
from mock import patch

from common.utils.dynamodb_access.table_names import TableNames

with patch('boto3.resource') as resource_mock:
    from decode_edifact_result.result_service import (
        get_result_by_result_id,
        update_result,
        add_sender_details_to_result
    )

EXAMPLE_RESULT_ID = '1234567'
EXAMPLE_RECEIVED_TIME = '2020-01-03T09:52:12.345'
EXAMPLE_DECODED_TIME = '2020-02-13T14:19:23.111'

EMPTY_RESULT = None
EXAMPLE_RESULT = {
    "result_id": "1234567",
    "raw": "blahblahblah",
    "received_time": "2020-01-03T09:52:12.345"
}


@patch('decode_edifact_result.result_service.log')
class TestResultService(TestCase):

    @patch('decode_edifact_result.result_service.dynamodb_get_item')
    def test_dynamodb_result_is_retrieved_correctly(self, get_item_mock, log_mock):

        # Given
        get_item_mock.return_value = EXAMPLE_RESULT

        # When
        expected_response = {
            "result_id": "1234567",
            "raw": "blahblahblah",
            "received_time": "2020-01-03T09:52:12.345"
        }
        actual_response = get_result_by_result_id(EXAMPLE_RESULT_ID, EXAMPLE_RECEIVED_TIME)

        # Then
        get_item_mock.assert_called_with(TableNames.RESULTS, {
            'result_id': EXAMPLE_RESULT_ID,
            'received_time': EXAMPLE_RECEIVED_TIME
        })
        self.assertEqual(expected_response, actual_response)

    @patch('decode_edifact_result.result_service.dynamodb_update_item')
    def test_dynamodb_result_is_updated_correctly(self, dynamodb_update_item, log_mock):

        # When
        decoded_result = {
            "nhs_number": "9999999999",
            "slide_number": "654654654",
            "forename_1": "FORENAME1"
        }
        decoded_time = '2020-02-13T14:19:23.111'
        update_result(EXAMPLE_RESULT_ID, EXAMPLE_RECEIVED_TIME, decoded_result, decoded_time, '654654654')

        # Then
        expected_update_expression = (
            "SET #decoded.#nhs_number = :nhs_number, "
            "#decoded.#slide_number = :slide_number, "
            "#decoded.#forename_1 = :forename_1, "
            "#decoded_time = :decoded_time, "
            "#slide_number = :slide_number"
        )
        expected_attribute_values = {
            ":decoded_time": '2020-02-13T14:19:23.111',
            ":nhs_number": '9999999999',
            ":slide_number": '654654654',
            ":forename_1": 'FORENAME1'
        }
        expected_attribute_names = {
            '#decoded': 'decoded',
            '#decoded_time': 'decoded_time',
            '#nhs_number': 'nhs_number',
            '#slide_number': 'slide_number',
            '#forename_1': 'forename_1',
        }

        dynamodb_update_item.assert_called_with(TableNames.RESULTS, dict(
            Key={'result_id': EXAMPLE_RESULT_ID, 'received_time': EXAMPLE_RECEIVED_TIME},
            UpdateExpression=expected_update_expression,
            ExpressionAttributeValues=expected_attribute_values,
            ExpressionAttributeNames=expected_attribute_names
        ))

    @patch('decode_edifact_result.result_service.dynamodb_update_item')
    def test_dynamodb_result_is_updated_correctly_when_no_slide_number(self, dynamodb_update_mock, log_mock):

        # When
        decoded_result = {
            "nhs_number": "9999999999",
            "slide_number": "654654654",
            "forename_1": "FORENAME1"
        }
        decoded_time = '2020-02-13T14:19:23.111'
        update_result(EXAMPLE_RESULT_ID, EXAMPLE_RECEIVED_TIME, decoded_result, decoded_time, None)

        # Then
        expected_update_expression = (
            "SET #decoded.#nhs_number = :nhs_number, "
            "#decoded.#slide_number = :slide_number, "
            "#decoded.#forename_1 = :forename_1, "
            "#decoded_time = :decoded_time"
        )
        expected_attribute_values = {
            ":decoded_time": '2020-02-13T14:19:23.111',
            ":nhs_number": '9999999999',
            ":slide_number": '654654654',
            ":forename_1": 'FORENAME1'
        }
        expected_attribute_names = {
            '#decoded': 'decoded',
            '#decoded_time': 'decoded_time',
            '#nhs_number': 'nhs_number',
            '#slide_number': 'slide_number',
            '#forename_1': 'forename_1',
        }

        dynamodb_update_mock.assert_called_with(TableNames.RESULTS, dict(
            Key={'result_id': EXAMPLE_RESULT_ID, 'received_time': EXAMPLE_RECEIVED_TIME},
            UpdateExpression=expected_update_expression,
            ExpressionAttributeValues=expected_attribute_values,
            ExpressionAttributeNames=expected_attribute_names
        ))

    @patch('decode_edifact_result.result_service.dynamodb_get_item')
    def test_dynamodb_no_result_throws_exception(self, get_item_mock, log_mock):

        # Given
        get_item_mock.return_value = EMPTY_RESULT

        # When
        with self.assertRaises(Exception) as context:
            get_result_by_result_id(EXAMPLE_RESULT_ID, EXAMPLE_RECEIVED_TIME)

        # Then
        expected_exception_message = ('Could not find result in DynamoDB with result_id: 1234567, '
                                      'received_time: 2020-01-03T09:52:12.345')

        self.assertTrue(expected_exception_message in str(context.exception))

    @patch('decode_edifact_result.result_service.get_sender_details_and_validate')
    def test_add_sender_details_to_result(self, get_sender_details_mock, log_mock):
        stored_result = {'sanitised_nhais_cipher': 'WIG', 'sending_lab': 'WIG1'}
        decoded_result = {'sender_code': '714522', 'sender_source_type': '0001', 'test_date': '2021-01-01'}

        get_sender_details_mock.return_value = {
            'name': 'Lloyds Pharamcy',
            'address': {
                'address_line_1': 'Address 1',
                'address_line_2': 'Address 2',
                'address_line_3': 'Address 3',
                'address_line_4': 'Townville',
                'address_line_5': 'Regionland',
                'postcode': 'QQ0 1BP',
            }
        }
        expected_sender_address = {
            'name': 'Lloyds Pharamcy',
            'address': {
                'address_line_1': 'Address 1',
                'address_line_2': 'Address 2',
                'address_line_3': 'Address 3',
                'address_line_4': 'Townville',
                'address_line_5': 'Regionland',
                'postcode': 'QQ0 1BP',
            }
        }

        sender_details = add_sender_details_to_result(stored_result, decoded_result)
        self.assertEqual(sender_details, expected_sender_address)
        get_sender_details_mock.assert_called_with('WIG', '714522', '0001', '2021-01-01')

    @patch('decode_edifact_result.result_service.get_sender_details_and_validate')
    def test_add_sender_details_to_result_when_sender_not_found(self, get_sender_details_mock, log_mock):
        stored_result = {'sanitised_nhais_cipher': 'WIG', 'sending_lab': 'WIG1'}
        decoded_result = {'sender_code': '714522', 'sender_source_type': '0001', 'test_date': '2020-04-23'}

        get_sender_details_mock.return_value = {
            'message': 'Could not retrieve sender details with given parameters',
            'sender_details': {'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'source_type': '0001'}
        }

        expected_sender_address = {
            'message': 'Could not retrieve sender details with given parameters',
            'sender_details': {'NHAIS_cipher': 'WIG', 'sender_code': '714522', 'source_type': '0001'}
        }

        sender_details = add_sender_details_to_result(stored_result, decoded_result)
        self.assertEqual(sender_details, expected_sender_address)
        get_sender_details_mock.assert_called_with('WIG', '714522', '0001', '2020-04-23')
