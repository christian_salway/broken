import datetime
from unittest.case import TestCase

from mock import patch, Mock, call

from common.test_mocks.mock_events import sqs_mock_event
from decode_edifact_result.log_references import LogReference
from decode_edifact_result.decode_edifact_result import lambda_handler

EXAMPLE_SQS_EVENT = sqs_mock_event({
    'internal_id': 'internal_id',
    'result_id': '12345678',
    'received_time': 'Christmas'
})

sqs_identify_duplicate_results_queue_mock = Mock()
sqs_identify_duplicate_results_queue_mock.return_value = 'match_queue_url'


@patch('decode_edifact_result.decode_edifact_result.sqs_identify_duplicate_results_queue',
       sqs_identify_duplicate_results_queue_mock)
class TestDecodeEDIFACTResult(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = 'decode_edifact_result_test'

    def tearDown(self):
        self.log_patcher.stop()

    @patch('decode_edifact_result.decode_edifact_result.log')
    @patch('decode_edifact_result.decode_edifact_result.decode_lab_result')
    @patch('decode_edifact_result.decode_edifact_result.get_result_by_result_id')
    @patch('decode_edifact_result.decode_edifact_result.validate_result_and_set_recall_details')
    @patch('decode_edifact_result.decode_edifact_result.update_result')
    @patch('decode_edifact_result.decode_edifact_result.send_new_message')
    def test_throws_exception_when_participant_not_found(self, sqs_mock, update_result_mock, validator_mock,
                                                         get_result_mock, decoder_mock, log_mock):

        # Given
        get_result_mock.side_effect = Exception('I am an exception')

        # When
        with self.assertRaises(Exception) as context:
            lambda_handler(EXAMPLE_SQS_EVENT, self.context)

        # Then
        self.assertTrue('I am an exception' in str(context.exception))
        self.assertFalse(decoder_mock.called)
        self.assertFalse(validator_mock.called)
        self.assertFalse(update_result_mock.called)
        self.assertFalse(sqs_mock.called)

    @patch('decode_edifact_result.decode_edifact_result.log')
    @patch('decode_edifact_result.decode_edifact_result.datetime')
    @patch('decode_edifact_result.decode_edifact_result.decode_lab_result')
    @patch('decode_edifact_result.decode_edifact_result.get_result_by_result_id')
    @patch('decode_edifact_result.decode_edifact_result.tokenize_edifact')
    @patch('decode_edifact_result.decode_edifact_result.validate_result_and_set_recall_details')
    @patch('decode_edifact_result.decode_edifact_result.update_result')
    @patch('decode_edifact_result.decode_edifact_result.send_new_message')
    @patch('decode_edifact_result.decode_edifact_result.add_sender_details_to_result')
    def test_adds_result_to_queue_if_decoded_successfully(
            self, add_sender_mock, sqs_mock, update_result_mock, validator_mock, extract_mock,
            get_result_mock, decoder_mock, datetime_mock, log_mock):

        # Given
        get_result_mock.return_value = {'raw': 'aabbcc', 'edifact_type': 'type', 'slide_number': 'slide_number'}
        extract_mock.return_value = ['extracted', 'values']
        decoder_mock.return_value = {'decoded_key': 'decoded_value', 'slide_number': 'slide_number'}
        validator_mock.return_value = {'validated_key': 'validated_value'}
        datetime_mock.now.return_value = datetime.datetime(2020, 2, 14, tzinfo=datetime.timezone.utc)
        add_sender_mock.return_value = {
            'name': 'Some organisation'
        }

        # When
        lambda_handler(EXAMPLE_SQS_EVENT, self.context)

        # Then
        get_result_mock.assert_called_with('12345678', 'Christmas')
        extract_mock.assert_called_with('aabbcc')
        decoder_mock.assert_called_with(['extracted', 'values'])
        validator_mock.assert_called_with('type', {
            'decoded_key': 'decoded_value',
            'slide_number': 'slide_number',
            'sender_name': 'Some organisation'
        })
        update_result_mock.assert_called_with('12345678', 'Christmas',
                                              {'validated_key': 'validated_value'},
                                              '2020-02-14T00:00:00+00:00', 'slide_number')
        sqs_mock.assert_called_with(
            'match_queue_url', {
                'internal_id': 'internal_id',
                'result_id': '12345678',
                'received_time': 'Christmas'
            }
        )

    @patch('decode_edifact_result.decode_edifact_result.log')
    @patch('decode_edifact_result.decode_edifact_result.datetime')
    @patch('decode_edifact_result.decode_edifact_result.decode_lab_result')
    @patch('decode_edifact_result.decode_edifact_result.get_result_by_result_id')
    @patch('decode_edifact_result.decode_edifact_result.tokenize_edifact')
    @patch('decode_edifact_result.decode_edifact_result.validate_result_and_set_recall_details')
    @patch('decode_edifact_result.decode_edifact_result.update_result')
    @patch('decode_edifact_result.decode_edifact_result.update_result_record')
    @patch('decode_edifact_result.decode_edifact_result.send_new_message')
    def test_reject_result_and_dont_send_to_sqs_if_validation_errors_present(
            self, sqs_mock, update_errors_mock, update_result_mock, validator_mock,
            extract_mock, get_result_mock, decoder_mock, datetime_mock, log_mock):

        # Given
        get_result_mock.return_value = {
            'raw': 'aabbcc', 'edifact_type': 'type', 'slide_number': 'slide_number',
            'test_date': '2021-08-25'
        }
        extract_mock.return_value = ['extracted', 'values']
        decoder_mock.return_value = {
            'decoded_key': 'decoded_value', 'slide_number': 'slide_number', 'test_date': '2021-08-25'}
        validator_mock.return_value = {'key': 'value', 'validation_errors': ['some_errors']}
        datetime_mock.now.return_value = datetime.datetime(2020, 2, 14, tzinfo=datetime.timezone.utc)

        sender_details = {
            'message': 'Could not retrieve sender details with given parameters',
            'sender_details': {'NHAIS_cipher': None, 'sender_code': None, 'sender_source_type': None},
            'failure_message': 'Sender not found'
        }

        # When
        lambda_handler(EXAMPLE_SQS_EVENT, self.context)

        # Then
        get_result_mock.assert_called_with('12345678', 'Christmas')
        extract_mock.assert_called_with('aabbcc')
        decoder_mock.assert_called_with(['extracted', 'values'])
        validator_mock.assert_called_with('type', {
            'decoded_key': 'decoded_value',
            'slide_number': 'slide_number',
            'test_date': '2021-08-25'
        })
        update_result_mock.assert_called_with(
            '12345678', 'Christmas',
            {'key': 'value', 'validation_errors': ['some_errors', 'Sender not found']},
            '2020-02-14T00:00:00+00:00', 'slide_number')

        update_errors_mock.assert_called_with(
            {'result_id': '12345678', 'received_time': 'Christmas'},
            {'validation_errors': ['some_errors', 'Sender not found']})

        sqs_mock.assert_called_with(
            'match_queue_url', {
                'internal_id': 'internal_id',
                'result_id': '12345678',
                'received_time': 'Christmas'
            })

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.DCD0001}),
            call({'log_reference': LogReference.DCD0004, 'result_id': '12345678'}),
            call({'log_reference': LogReference.DCD0007, 'sender_details': sender_details}),
            call({'log_reference': LogReference.DCD0006, 'reason': 'Missing or invalid data'}),
        ])
