import os
import json

from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.result_utils import update_result_record
from common.utils.sqs_utils import send_new_message
from decode_edifact_result.result_service import get_result_by_result_id, update_result, add_sender_details_to_result
from common.utils.edifact_utils import (
    decode_lab_result, tokenize_edifact
)
from common.log import log, get_internal_id
from decode_edifact_result.log_references import LogReference
from datetime import datetime, timezone

from decode_edifact_result.result_validator import validate_result_and_set_recall_details


def sqs_identify_duplicate_results_queue():
    return os.environ.get('SQS_QUEUE_URL')


@lambda_entry_point
def lambda_handler(event, context):

    log({'log_reference': LogReference.DCD0001})

    body = event['Records'][0]['body']
    queue_message = json.loads(body)

    result_id = queue_message['result_id']
    received_time = queue_message['received_time']
    stored_result = get_result_by_result_id(result_id, received_time)

    raw_lab_result = stored_result['raw']
    una_header = stored_result.get('una_header', '')
    extracted_result = tokenize_edifact(una_header + raw_lab_result)
    decoded_result = decode_lab_result(extracted_result)

    log({'log_reference': LogReference.DCD0004, 'result_id': result_id})

    edifact_type = stored_result['edifact_type']
    validated_decoded_result = validate_result_and_set_recall_details(edifact_type, decoded_result)

    sender_details = add_sender_details_to_result(stored_result, decoded_result)
    log({'log_reference': LogReference.DCD0007, 'sender_details': sender_details})
    if sender_details and not sender_details.get('message'):
        if sender_details.get('name'):
            decoded_result['sender_name'] = sender_details['name']
        if sender_details.get('address'):
            decoded_result['sender_address'] = sender_details['address']

    if sender_details:
        sender_validation_error = sender_details.get('failure_message')
        validation_errors = validated_decoded_result.get('validation_errors')
        if sender_validation_error and not validation_errors:
            validated_decoded_result['validation_errors'] = [sender_validation_error]
        elif sender_validation_error and validation_errors:
            validation_errors.append(sender_validation_error)

    decoded_time = datetime.now(timezone.utc).isoformat()
    update_result(result_id, received_time, validated_decoded_result, decoded_time, decoded_result.get('slide_number'))

    if validated_decoded_result.get('validation_errors'):
        key = {
            'result_id': result_id,
            'received_time': received_time
        }

        log({'log_reference': LogReference.DCD0006, 'reason': 'Missing or invalid data'})
        update_result_record(key, {'validation_errors': validated_decoded_result.get('validation_errors')})

    send_new_message(sqs_identify_duplicate_results_queue(), {
        'result_id': result_id,
        'received_time': received_time,
        'internal_id': get_internal_id()
    })
