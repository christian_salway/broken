from datetime import datetime, timezone

from dateutil.relativedelta import relativedelta

from common.utils.address_utils import parse_address_from_decoded_string
from common.utils.edifact_utils import validate_edifact_postcode
from common.utils.result_codes import (
    RECALL_TYPES,
    VALID_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS,
    VALID_IF_SELF_SAMPLED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS,
    get_concatenated_result_code_combination
)

REQUIRED_FIELDS = {
    'nhs_number': 'Missing NHS number',
    'first_name': 'Missing first name',
    'last_name': 'Missing last name',
    'address': 'Missing address',
    'date_of_birth': 'Missing date of birth',
    'test_date': 'Missing test date',
    'slide_number': 'Missing slide number',
    'sender_code': 'Missing sender code'
}

TYPE_3_EDIFACT = 3
TYPE_9_EDIFACT = 9


def validate_result_and_set_recall_details(edifact_type, decoded_result):
    validation_errors = []

    _validate_required_fields_present(decoded_result, validation_errors)
    _validate_date_of_birth_and_test_date(decoded_result, validation_errors)
    validate_edifact_postcode(decoded_result, validation_errors)
    _validate_result_codes(decoded_result, validation_errors, edifact_type)
    _set_status_for_result(decoded_result)
    _validate_recall_months(decoded_result, validation_errors)
    _validate_slide_number(decoded_result, validation_errors)

    sanitise_lab_address(decoded_result)

    if validation_errors:
        decoded_result['validation_errors'] = validation_errors

    return decoded_result


def sanitise_lab_address(decoded_result):
    address = decoded_result.get('address', '')

    sanitised_lab_address = parse_address_from_decoded_string(address)

    if sanitised_lab_address:
        decoded_result['sanitised_lab_address'] = sanitised_lab_address


def _validate_required_fields_present(decoded_result, validation_errors):
    for required_field in REQUIRED_FIELDS:
        if not decoded_result.get(required_field):
            error_message = REQUIRED_FIELDS[required_field]
            validation_errors.append(error_message)


def _calculate_today_date_minus_11_years():
    today_date_minus_11_years = datetime.now(timezone.utc) - relativedelta(years=11)
    return today_date_minus_11_years.date().isoformat()


def _validate_date_of_birth_and_test_date(decoded_result, validation_errors):
    # Get values
    decoded_date_of_birth = decoded_result.get('date_of_birth')
    date_of_birth_format = decoded_result.get('date_of_birth_format')
    decoded_test_date = decoded_result.get('test_date')
    test_date_format = decoded_result.get('test_date_format')
    today_date = datetime.now(timezone.utc).date().isoformat()

    # If either date is not present
    dates_are_both_valid = decoded_date_of_birth and decoded_test_date

    # Get formatted value of date_of_birth if it is present
    if decoded_date_of_birth:
        try:
            formatted_date_of_birth = _formatted_date_from_edifact_format(decoded_date_of_birth, date_of_birth_format)
            todays_date_minus_11_years = _calculate_today_date_minus_11_years()
            # If the date of birth is after today's date minus 11 years, it means they are younger than 11 years old
            if (todays_date_minus_11_years and formatted_date_of_birth > todays_date_minus_11_years):
                dates_are_both_valid = False
                validation_errors.append('Participant age < 11')

            decoded_result['date_of_birth'] = formatted_date_of_birth
        except Exception:
            dates_are_both_valid = False
            validation_errors.append('Invalid date of birth')

    # Get formatted value of test_date if it is present
    if decoded_test_date:
        try:
            formatted_test_date = _formatted_date_from_edifact_format(decoded_test_date, test_date_format)
            decoded_result['test_date'] = formatted_test_date
        except Exception:
            dates_are_both_valid = False
            validation_errors.append('Invalid test date')

    # Fields are added by edifact_utils to use for the above validation but are not part of the result table schema
    decoded_result.pop('date_of_birth_format', None)
    decoded_result.pop('test_date_format', None)

    if dates_are_both_valid:
        if formatted_date_of_birth > formatted_test_date:
            validation_errors.append('Test date is before date of birth')

        today_date = datetime.now(timezone.utc).date()
        if formatted_test_date > today_date.isoformat():
            validation_errors.append('Test date is in the future')

        one_year_ago = today_date - relativedelta(months=+12)
        if formatted_test_date < one_year_ago.isoformat():
            validation_errors.append('Test date > 1 year old')


def _formatted_date_from_edifact_format(decoded_edifact_date: str, decoded_edifact_date_format: str) -> str:
    return datetime.strptime(decoded_edifact_date, decoded_edifact_date_format).date().isoformat()


def _validate_result_codes(decoded_result, validation_errors, edifact_type):
    cytology_result_code = decoded_result.get('result_code', '')
    hpv_result_code = decoded_result.get('infection_code', '')
    action_code = decoded_result.get('action_code', '')
    concatenated_codes = get_concatenated_result_code_combination(decoded_result)

    codes = {
        'result_code': cytology_result_code,
        'infection_code': hpv_result_code,
        'action_code': action_code
    }

    if edifact_type == TYPE_9_EDIFACT and '' in codes.values():
        [validation_errors.append(f'Value for {key} is missing') for key, value in codes.items() if value == '']
    elif concatenated_codes in VALID_IF_SELF_SAMPLED_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS:
        is_self_sample = decoded_result.get('self_sample', False)
        if is_self_sample is False:
            validation_errors.append(f"Result code '{cytology_result_code}', infection code '{hpv_result_code}' and "
                                     f"action code '{action_code}' are only a valid combination for self sampled "
                                     "results")
    elif concatenated_codes not in VALID_CYTOLOGY_HPV_ACTION_CODES_COMBINATIONS:
        validation_errors.append(f"Result code '{cytology_result_code}', infection code '{hpv_result_code}' and "
                                 f"action code '{action_code}' are not a valid combination")


def _set_status_for_result(decoded_result):
    concatenated_result_and_action_codes = get_concatenated_result_code_combination(decoded_result)
    for recall_type, valid_combinations in RECALL_TYPES.items():
        if concatenated_result_and_action_codes in valid_combinations:
            decoded_result['status'] = recall_type
            break


# see result validation A18
LAB_RECALL_COMBINATIONS_SUBJECT_TO_VALIDATION = {
    ('X', '0', 'R', False): ['6', '12', '36'],
    ('X', 'U', 'R', False): ['3'],
    ('X', '9', 'R', True):  ['3'],
    ('1', '9', 'R', False): ['3'],
    ('2', '9', 'R', False): ['12'],
    ('0', '9', 'R', False): ['12']
}


def _validate_recall_months(decoded_result, validation_errors):
    recall_months = decoded_result.get('recall_months', '')
    if recall_months != '':
        comnination = (
            decoded_result.get('result_code', ''),
            decoded_result.get('infection_code', ''),
            decoded_result.get('action_code', ''),
            decoded_result.get('self_sample', False)
        )
        if comnination in LAB_RECALL_COMBINATIONS_SUBJECT_TO_VALIDATION and \
                recall_months not in LAB_RECALL_COMBINATIONS_SUBJECT_TO_VALIDATION[comnination]:
            validation_errors.append(f'Invalid recall months: {recall_months}')


def _validate_slide_number(decoded_result, validation_errors):
    slide_number = decoded_result.get('slide_number')
    if not slide_number:
        return

    if not slide_number.isnumeric() or len(slide_number) != 8:
        validation_errors.append('Invalid slide number format')
    else:
        now = datetime.now(timezone.utc)
        current_year_as_yy = now.year - 2000
        first_two_digits = int(slide_number) // 1000000
        if first_two_digits != current_year_as_yy and not janAllowance(now.month, first_two_digits, current_year_as_yy):
            validation_errors.append('Invalid slide number year')


def janAllowance(monthNumber, first_two_digits, current_year_as_yy):
    # During January the first two digits may match the year just ended.
    return monthNumber == 1 and first_two_digits == current_year_as_yy - 1
