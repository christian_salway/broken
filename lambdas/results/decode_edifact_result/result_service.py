from common.log import log
from decode_edifact_result.log_references import LogReference
from common.utils.sender_utils import get_sender_details_and_validate
from common.utils.dynamodb_access.operations import dynamodb_get_item, dynamodb_update_item
from common.utils.dynamodb_access.table_names import TableNames


def get_result_by_result_id(result_id, received_time):
    lab_result = dynamodb_get_item(TableNames.RESULTS, {
        'result_id': result_id,
        'received_time': received_time
    })
    if lab_result is None:
        log({'log_reference': LogReference.DCD0003})
        raise Exception('Could not find result in DynamoDB with '
                        f'result_id: {result_id}, received_time: {received_time}')
    else:
        log({'log_reference': LogReference.DCD0002, 'result_id': result_id})
        return lab_result


def update_result(result_id, received_time, decoded_result: dict, decoded_time, slide_number):
    expression_keys = []
    expression_values = {
        ':decoded_time': decoded_time
    }
    expression_names = {
        '#decoded': 'decoded',
        '#decoded_time': 'decoded_time'
    }

    for key, value in decoded_result.items():
        if value != '':
            expression_keys.append(f'#decoded.#{key} = :{key}')
            expression_values[f':{key}'] = value
            expression_names[f'#{key}'] = key

    update_expression = 'SET ' + ', '.join(expression_keys) + \
        ', #decoded_time = :decoded_time'

    if slide_number:
        expression_names['#slide_number'] = 'slide_number'
        expression_values[':slide_number'] = slide_number
        update_expression += ', #slide_number = :slide_number'

    # Send through the sort key
    dynamodb_update_item(TableNames.RESULTS, dict(
        Key={
            'result_id': result_id,
            'received_time': received_time
        },
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ExpressionAttributeNames=expression_names
    ))

    log({'log_reference': LogReference.DCD0005})


def add_sender_details_to_result(stored_result, decoded_result):
    test_date = decoded_result.get('test_date')
    if not test_date:
        return

    cipher = stored_result.get('sanitised_nhais_cipher')
    sender_code = decoded_result.get('sender_code')
    sender_source_type = decoded_result.get('sender_source_type')
    return get_sender_details_and_validate(cipher, sender_code, sender_source_type, test_date)
