# Decode Edifact

- [Decode Edifact](#decode-edifact)
  - [Process](#process)
  - [Architecture](#architecture)

## Process

>Previous Step is [split_result_files](../split_results_file/README.md)

>Next step is [match_result](../match_result/README.md)

- This lambda is triggered by messages placed on the results-to-decode SQS queue by the split_result_files lambda
- If it successfully decodes the EDIFACT message to our internal JSON format, it updates the result record in DynamoDB Results table, for the result it is processing.
- If it fails to decode, an exception is raised to ensure the message is retried
- A result will not be rejected at this point if validation errors are found e.g. some fields are missing from the edifact. Instead,
a list of errors will be set against the result, which will be used later in the flow.

## Architecture

See the technical overview for more information: https://nhsd-confluence.digital.nhs.uk/display/CSP/Results+-+Technical+Overview
