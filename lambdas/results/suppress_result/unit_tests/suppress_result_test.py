import json
from unittest import TestCase
from mock import patch, Mock

path = 'suppress_result.suppress_result'


@patch('common.utils.audit_utils.get_sqs_client', Mock())
class TestSuppressResult(TestCase):

    def setUp(self):
        import suppress_result.suppress_result as _suppress_result
        import common.utils.audit_utils as _audit_utils
        import common.utils.participant_utils as _participant_utils
        import common.utils.notification_utils as _notification_utils

        self.suppress_result_module = _suppress_result
        self.audit_utils = _audit_utils
        self.participant_utils = _participant_utils
        self.notification_utils = _notification_utils
        self.log_patcher = patch('common.log.logger').start()
        self.context = Mock()
        self.context.function_name = ''
        self.mock_user_id = {'participant_id': '123456'}

        self.mock_session = {
            'session_id': '12345678',
            'user_data': {
                'nhsid_useruid': self.mock_user_id
            }
        }

        self.mock_event_body = {'crm_number': 'CAS-12345-ABCDE', 'comments': 'some comments'}

        self.mock_event = {
            'requestContext': {'authorizer': {
                'session': json.dumps(self.mock_session), 'principalId': 'blah'
            }},
            'headers': {'request_id': 'blah'},
            'pathParameters': {'participant_id': '123456', 'sort_key': 'RESULT#123456'},
            'body': json.dumps(self.mock_event_body)
        }

    def tearDown(self):
        self.log_patcher.stop()

    @patch(f'{path}.get_notifications_from_result')
    def test_lambda_returns_400_when_request_body_is_missing(self, get_notification_mock):
        self.mock_event.pop('body')
        get_notification_mock.return_value = []
        response = self.suppress_result_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(400, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Missing or malformed request body.'}), response['body'])

    @patch(f'{path}.get_notifications_from_result')
    def test_lambda_returns_400_when_sort_key_validation_fails(self, get_notification_mock):
        self.mock_event['pathParameters'] = {'participant_id': '123456', 'sort_key': 'NOTIFICATION#123456'}
        get_notification_mock.return_value = []
        response = self.suppress_result_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(400, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Supplied sort_key does not start with \"RESULT\".'}), response['body'])

    @patch(f'{path}.get_notifications_from_result')
    def test_lambda_returns_404_when_notification_not_found(self, get_notification_mock):
        get_notification_mock.return_value = []
        response = self.suppress_result_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(404, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Notification not found.'}), response['body'])

    @patch(f'{path}.get_notifications_from_result')
    def test_lambda_returns_400_when_live_record_status_is_not_pending_print_file(self, get_notification_mock):
        get_notification_mock.return_value = [{
            'participant_id': '123456',
            'sort_key': 'NOTIFICATION#123456',
            'record_id': 'RESULT#123456',
            'type': 'Result letter',
            'status': 'SENT',
            'live_record_status': 'IN_PRINT_FILE'
        }]

        response = self.suppress_result_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(400, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Live record status is not \"PENDING_PRINT_FILE\".'}), response['body'])

    @patch(f'{path}.update_participant_record')
    @patch(f'{path}.get_notifications_from_result')
    def test_lambda_returns_500_when_fail_to_update_record(self, get_notification_mock,
                                                           update_participant_record_mock):
        get_notification_mock.return_value = [{
            'participant_id': '123456',
            'sort_key': 'NOTIFICATION#123456',
            'record_id': 'RESULT#123456',
            'type': 'Result letter',
            'status': 'PROCESSING',
            'live_record_status': 'PENDING_PRINT_FILE'
        }]

        update_participant_record_mock.side_effect = Exception('Something went wrong.')
        response = self.suppress_result_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(500, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Something went wrong.'}), response['body'])

    @patch(f'{path}.update_participant_record')
    @patch(f'{path}.get_notifications_from_result')
    def test_lambda_returns_200_when_result_letter_is_suppressed(self, get_notification_mock,
                                                                 update_participant_record_mock):
        get_notification_mock.return_value = [{
            'participant_id': '123456',
            'sort_key': 'NOTIFICATION#123456',
            'record_id': 'RESULT#123456',
            'type': 'Result letter',
            'status': 'PROCESSING',
            'live_record_status': 'PENDING_PRINT_FILE'
        }]

        update_participant_record_mock.return_value = {}
        response = self.suppress_result_module.lambda_handler(self.mock_event, self.context)
        self.assertEqual(200, response['statusCode'])
        self.assertEqual(json.dumps({'message': 'Successfully suppressed result letter.'}), response['body'])
