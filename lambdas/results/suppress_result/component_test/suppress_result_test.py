import json
from unittest import TestCase
from mock import patch, Mock
from boto3.dynamodb.conditions import Key, Attr
from datetime import datetime, timezone
from common.models.notification import NotificationType

mock_participant_record = {
    'participant_id': '123456',
    'sort_key': 'NOTIFICATION#123456',
    'record_id': 'RESULT#123456',
    'type': 'Result letter',
    'status': 'PROCESSING',
    'live_record_status': 'PENDING_PRINT_FILE'
}
mock_env_vars = {
    'AUDIT_QUEUE_URL': 'AUDIT_QUEUE_URL',
    'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
}

AUDIT_BODY = {
    'action': 'RESULT_LETTER_CANCELLED',
    'timestamp': '2020-01-23T13:48:08.123456+00:00',
    'internal_id': '1',
    'nhsid_useruid': 'SYSTEM',
    'session_id': 'NONE',
    'participant_ids': ['123456'],
    'additional_information': {
        'crm_number': 'CAS-12345-ABCDE',
        'comments': 'some comments'
    }
}

participants_table_mock = Mock()
sqs_client_mock = Mock()
example_date = datetime(2020, 1, 23, 13, 48, 8, 123456, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date

with patch('datetime.datetime', datetime_mock):
    with patch('common.log.get_internal_id', Mock(return_value='1')):
        with patch('os.environ', mock_env_vars):
            from suppress_result import suppress_result


@patch('common.utils.audit_utils.get_sqs_client', Mock(return_value=sqs_client_mock))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(return_value=participants_table_mock))
@patch('os.environ', mock_env_vars)
class TestSuppressResult(TestCase):

    def setUp(self):
        self.log_patcher = patch('common.log.log')
        participants_table_mock.reset_mock()
        sqs_client_mock.reset_mock()
        self.notifications_mock_response = {'Items': [mock_participant_record]}
        participants_table_mock.query.return_value = self.notifications_mock_response
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    def test_suppress_letter(self):
        session = {
            "session_id": "12345678",
            "user_data": {'nhsid_useruid': 'NHS12345'},
            'selected_role': {
                'organisation_code': 'DXX',
                'organisation_name': 'YORKSHIRE AND THE HUMBER',
                'role_id': 'R8010',
                'role_name': 'CSAS Team Member'
            },
            'access_groups': {
                'pnl': False,
                'sample_taker': False,
                'lab': False,
                'colposcopy': False,
                'csas': True,
                'view_participant': False
            }
        }

        self.mock_event_body = {'crm_number': 'CAS-12345-ABCDE', 'comments': 'some comments'}

        event = {
            'requestContext': {'authorizer': {
                'session': json.dumps(session), 'principalId': 'blah'
            }},
            'headers': {'request_id': 'blah'},
            'pathParameters': {'participant_id': '123456', 'sort_key': 'RESULT#123456'},
            'body': json.dumps(self.mock_event_body)
        }
        context = Mock()
        context.function_name = ''

        suppress_result.lambda_handler(event, context)

        participants_table_mock.query.assert_called_with(
            KeyConditionExpression=Key('participant_id').eq('123456') &
            Key('sort_key').begins_with('NOTIFICATION'),
            FilterExpression=Attr('type').eq(NotificationType.RESULT.value) & (
                Attr('record_id').exists() &
                Attr('record_id').eq('RESULT#123456')),
            ScanIndexForward=False
        )

        participants_table_mock.update_item.assert_called_once()

        participants_table_mock.update_item.called_with(
            Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'},
            UpdateExpression='SET #status = :status REMOVE #live_record_status',
            ReturnValues='NONE',
        )

        sqs_client_mock.send_message.assert_called_once_with(
            QueueUrl='AUDIT_QUEUE_URL',
            MessageBody=json.dumps(AUDIT_BODY))
