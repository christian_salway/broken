import logging
from common.log import BaseLogReference


class LogReference(BaseLogReference):
    SUPPLETTER0001 = (logging.INFO, 'Attempting to suppress result letter')
    SUPPLETTER0002 = (logging.INFO, 'Notification record found')
    SUPPLETTER0003 = (logging.INFO, 'Updating notification to suppress result letter')
    SUPPLETTER0004 = (logging.INFO, 'Successfully suppressed result letter')
    SUPPLETTER0007 = (logging.INFO, 'Required request parameters not supplied')
    SUPPLETTER0008 = (logging.INFO, 'Cannot suppress result letter')
    SUPPLETTER0009 = (logging.ERROR, 'Failed to suppress letter')
