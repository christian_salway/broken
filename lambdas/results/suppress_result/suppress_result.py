import json
from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from suppress_result.log_references import LogReference
from common.utils import json_return_message
from common.models.participant import ParticipantSortKey
from common.utils.notification_utils import \
    get_notifications_from_result
from common.utils.participant_utils import update_participant_record
from common.models.letters import NotificationStatus
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from urllib.parse import unquote


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    log({'log_reference': LogReference.SUPPLETTER0001})

    participant_id = event['pathParameters']['participant_id']
    record_id = unquote(event['pathParameters']['sort_key'])  # sort_key of a result is the record_id for notification

    try:
        request_body = json.loads(event['body'])
    except Exception:
        return build_bad_request_response('Missing or malformed request body.')

    if not record_id.startswith(ParticipantSortKey.RESULT):
        return build_bad_request_response('Supplied sort_key does not start with "RESULT".')

    notification_records = get_notifications_from_result(
        participant_id, record_id)

    if not notification_records:
        return build_not_found_response('Notification not found.')

    notification_record = notification_records[0]

    log({'log_reference': LogReference.SUPPLETTER0002})

    live_record_status = notification_record.get('live_record_status')

    if live_record_status != NotificationStatus.PENDING_PRINT_FILE:
        return build_bad_request_response(f'Live record status is not "{NotificationStatus.PENDING_PRINT_FILE}".')

    log({'log_reference': LogReference.SUPPLETTER0003})

    sort_key = notification_record['sort_key']
    crm_number = request_body.get('crm_number', '')
    comments = request_body.get('comments', '')

    try:
        update_participant_record(
            record_key={'participant_id': participant_id, 'sort_key': sort_key},
            update_attributes={'status': NotificationStatus.SUPPRESSED},
            delete_attributes=['live_record_status']
        )

        audit(
            action=AuditActions.RESULT_LETTER_CANCELLED,
            user=AuditUsers.SYSTEM,
            participant_ids=[participant_id],
            additional_information={
                'crm_number': crm_number,
                'comments': comments})

        log({'log_reference': LogReference.SUPPLETTER0004})

        return json_return_message(200, 'Successfully suppressed result letter.')
    except Exception as e:
        return build_internal_server_error_response(str(e))


def build_bad_request_response(error_message):
    log({'log_reference': LogReference.SUPPLETTER0007, 'Error': error_message})
    return json_return_message(400, error_message)


def build_not_found_response(error_message):
    log({'log_reference': LogReference.SUPPLETTER0008, 'Error': error_message})
    return json_return_message(404, error_message)


def build_internal_server_error_response(error_message):
    log({'log_reference': LogReference.SUPPLETTER0009, 'Error': error_message})
    return json_return_message(500, error_message)
