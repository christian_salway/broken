# Split Result File

- [Split Result File](#split-result-file)
  - [Process](#process)
  - [Architecture](#architecture)
## Process

>Previous step is [../integrations/list_s3_files](../../integrations/list_s3_files/README.md)

>Next step is [decode_edifact_result](../decode_edifact_result/README.md)

Lambda subscribes to the results-files-to-process SQS queue receiving only one message at a time.
Each message refers to one EDIFACT results file in s3.
For each result within that file a record is saved in DynamoDB and a message sent to the results_to_decode SQS queue.
Each file that is successfully processed is moved to the results-data-completed bucket.
If the EDIFACT message is not valid then the file being processed is moved to the results-data-failed bucket.

[JIRA](https://nhsd-jira.digital.nhs.uk/browse/SP-696)
[JIRA](https://nhsd-jira.digital.nhs.uk/browse/SP-951)

## Architecture

See the technical overview for more information: https://nhsd-confluence.digital.nhs.uk/display/CSP/Results+-+Technical+Overview
