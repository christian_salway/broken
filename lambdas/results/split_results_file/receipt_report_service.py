from datetime import datetime, timezone
from os import environ
from uuid import uuid4

from split_results_file.log_references import LogReference
from common.log import log
from common.utils.edifact_write_utils import generate_receipt_report
from common.utils.s3_utils import put_object
from common.utils.sqs_utils import send_new_message

MESH_OUTBOUND_MESSAGES_BUCKET = environ.get('MESH_OUTBOUND_MESSAGES_BUCKET')
MESH_FILES_TO_SEND_QUEUE_URL = environ.get('MESH_FILES_TO_SEND_QUEUE_URL')
RECEIPT_REPORT_FROM_MAILBOX_ID = environ.get('RECEIPT_REPORT_FROM_MAILBOX_ID')
RECEIPT_REPORT_TO_MAILBOX_ID = environ.get('RECEIPT_REPORT_TO_MAILBOX_ID')


def create_and_queue_receipt_report(raw_edifact, received_time, is_success):
    try:
        log({'log_reference': LogReference.SPLIT0016})
        receipt_report = generate_receipt_report(raw_edifact, received_time, is_success)
        log({'log_reference': LogReference.SPLIT0017})

        formatted_datetime = datetime.now(timezone.utc).strftime('%Y%m%d%H%M%S%f%z')
        # Append a partial UUID to the file name in case the datetime isn't unique enough
        # This is the same format as the results files received from MESH
        key = str(uuid4())[:6].upper()
        object_key = f'results_acks/{formatted_datetime}_{key}.dat'

        log({'log_reference': LogReference.SPLIT0018, 'bucket_name': MESH_OUTBOUND_MESSAGES_BUCKET})
        put_object(MESH_OUTBOUND_MESSAGES_BUCKET, receipt_report, object_key)
        log({'log_reference': LogReference.SPLIT0019})

        message = get_sqs_message_format()
        message['object_key'] = object_key

        log({'log_reference': LogReference.SPLIT0020, 'queue_url': MESH_FILES_TO_SEND_QUEUE_URL})
        send_new_message(MESH_FILES_TO_SEND_QUEUE_URL, message)
        log({'log_reference': LogReference.SPLIT0021})
    except Exception as e:
        log({'log_reference': LogReference.SPLIT0022, 'error': str(e)})
        raise e


def get_sqs_message_format():
    return {
        'from_mailbox': RECEIPT_REPORT_FROM_MAILBOX_ID,
        'to_mailbox': RECEIPT_REPORT_TO_MAILBOX_ID,
        'workflow_id': '[TBC]',
        'bucket_name': 'mesh-outbound-messages'
    }
