import os
import uuid
import re

from split_results_file.log_references import LogReference
from common.log import log, get_internal_id
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.s3_utils import (
    get_file_contents_from_bucket,
    move_object_to_bucket_and_transition_to_infrequent_access_class
)
from common.utils.sqs_utils import send_new_message, get_s3_data_from_queue_event
from common.utils.result_utils import create_replace_record
from common.utils.edifact_utils import (
    get_una_header,
    split_raw_edifact_into_raw_lab_results,
    decode_lab_result,
    tokenize_edifact
)
from common.models.result import ResultSendingLabCodes
from common.utils.data_segregation.nhais_ciphers import VALID_MIGRATION_CIPHER_CODES

from split_results_file.duplicate_results_check import (
    reject_results_with_duplicate_demographics
)
from split_results_file.receipt_report_service import create_and_queue_receipt_report


COMPLETED_BUCKET = os.environ.get('RESULTS_COMPLETED_BUCKET')
FAILED_BUCKET = os.environ.get('RESULTS_FAILED_BUCKET')
RESULTS_TO_DECODE_SQS_URL = os.environ.get('RESULTS_TO_DECODE_SQS_URL')


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.SPLIT0001})
    file_name, bucket, last_modified = get_s3_data_from_queue_event(event)
    log({'log_reference': LogReference.SPLIT0002, 'file_name': file_name})

    try:
        file_response = get_file_contents_from_bucket(bucket, file_name)
        file_contents = file_response['body'].replace('\n', '').replace('\r', '')
        log({'log_reference': LogReference.SPLIT0003})

        results, una_header = get_result_items_from_edifact(file_contents, last_modified, file_name)
        create_and_queue_receipt_report(file_contents, last_modified, True)
        results = reject_results_with_duplicate_demographics(results, una_header)
        log({'log_reference': LogReference.SPLIT0004, 'number_of_results': len(results)})
    except Exception as e:
        log({'log_reference': LogReference.SPLIT0014, 'error': str(e)})
        move_file_to_processed_bucket(bucket, FAILED_BUCKET, file_name)
        log({'log_reference': LogReference.SPLIT0015})
        return True

    write_result_items_to_dynamo_db_table(results)
    log({'log_reference': LogReference.SPLIT0005})

    add_results_to_sqs_queue(results)
    log({'log_reference': LogReference.SPLIT0006})

    move_file_to_processed_bucket(bucket, COMPLETED_BUCKET, file_name)


def get_result_items_from_edifact(raw_edifact, received_time, file_name):
    try:
        raw_edifact = raw_edifact.replace('\r', '').replace('\n', '')
        una_header = get_una_header(raw_edifact)
        edifact_type, split_edifact = split_raw_edifact_into_raw_lab_results(raw_edifact)
        header = split_edifact.pop(0)
        split_header = tokenize_edifact(header)
        decoded_header = decode_lab_result(split_header)
        sending_lab = decoded_header.get('sending_lab')
        raw_nhais_cipher = decoded_header.get('raw_nhais_cipher')
        cytology_lab_code = decoded_header.get('cytology_lab_code')
        pathology_lab = decoded_header.get('pathology_lab')
        raw_nhais_cipher = decoded_header.get('raw_nhais_cipher')
        unb_header = get_unb_header(una_header, header)
        edifact_headers = {
            'una_header': una_header,
            'unb_header': unb_header
        }
        log({'log_reference': LogReference.SPLIT0023, 'headers': edifact_headers})

        # Raise exception if NHAIS Cipher is None/empty or does not exist in list of 84 recognised Ciphers
        # Causes results file to be rejected
        if not raw_nhais_cipher or not nhais_cipher_is_valid(raw_nhais_cipher):
            raise Exception("Edifact file contains empty or invalid NHAIS Cipher")

        sending_lab_valid = sending_lab_is_valid(sending_lab)

        # Raise exception if Sending Lab is not in the allow list, to cause results file to be rejected
        if not sending_lab_valid:
            raise Exception("Edifact file contains an invalid sending lab")

        return [create_result_item(
                edifact_type, edifact_headers, raw_result, received_time, sending_lab,
                cytology_lab_code, pathology_lab, raw_nhais_cipher, file_name, index)
                for index, raw_result in enumerate(split_edifact)], una_header
    except Exception as e:
        log({'log_reference': LogReference.SPLIT0010, 'error': str(e)})
        create_and_queue_receipt_report(raw_edifact, received_time, False)
        raise e


def sending_lab_is_valid(sending_lab):
    sending_lab_codes = [item.value for item in ResultSendingLabCodes]
    return sending_lab in sending_lab_codes


def get_unb_header(una_header, header):
    return header[len(una_header):len(header)]


def create_result_item(edifact_type, edifact_headers, raw_result, received_time, sending_lab, cytology_lab_code,
                       pathology_lab, raw_nhais_cipher, file_name, index):
    result_item = {
        'result_id': str(uuid.uuid4()),
        'source_file_identifier': build_source_file_identifier(file_name, str(index), sending_lab, received_time),
        'edifact_type': edifact_type,
        'raw': raw_result,
        'received_time': received_time,
        'source_file': file_name,
        'record_index': index,
        'sending_lab': sending_lab,
        'raw_nhais_cipher': raw_nhais_cipher,
        'sanitised_nhais_cipher': sanitise_nhais_cipher(raw_nhais_cipher),
        'decoded': {}
    }

    if edifact_headers and edifact_headers['una_header']:
        result_item['una_header'] = edifact_headers['una_header']

    if edifact_headers and edifact_headers['unb_header']:
        result_item['unb_header'] = edifact_headers['unb_header']

    if cytology_lab_code is not None and pathology_lab is not None:
        result_item.update(decoded={'cytology_lab_code': cytology_lab_code, 'pathology_lab': pathology_lab})
    elif cytology_lab_code is not None:
        result_item.update(decoded={'cytology_lab_code': cytology_lab_code})
    elif pathology_lab is not None:
        result_item.update(decoded={'pathology_lab': pathology_lab})

    return result_item


def build_source_file_identifier(file_name, index, sending_lab, received_time):
    return ''.join([file_name, str(index), sending_lab, received_time])


def nhais_cipher_is_valid(raw_nhais_cipher_string):
    return sanitise_nhais_cipher(raw_nhais_cipher_string) in VALID_MIGRATION_CIPHER_CODES


def sanitise_nhais_cipher(raw_nhais_cipher_string):
    if raw_nhais_cipher_string:
        return re.sub(r'\d', "", raw_nhais_cipher_string).upper()
    else:
        return None


def write_result_items_to_dynamo_db_table(results):
    try:
        [create_replace_record(result) for result in results]
    except Exception as e:
        log({'log_reference': LogReference.SPLIT0011, 'error': str(e)})
        raise e


def add_results_to_sqs_queue(results):
    internal_id = get_internal_id()
    try:
        for result in results:
            queue_message = {
                'result_id': result['result_id'],
                'received_time': result['received_time'],
                'internal_id': internal_id
            }
            send_new_message(RESULTS_TO_DECODE_SQS_URL, queue_message)
    except Exception as e:
        log({'log_reference': LogReference.SPLIT0012, 'error': str(e)})
        raise e


def move_file_to_processed_bucket(source_bucket, target_bucket, file_name):
    try:
        move_object_to_bucket_and_transition_to_infrequent_access_class(source_bucket, target_bucket, file_name)
    except Exception as e:
        log({'log_reference': LogReference.SPLIT0013, 'error': str(e)})
        raise e
