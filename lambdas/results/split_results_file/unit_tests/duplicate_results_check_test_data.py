EDIFACT_UNA_HEADER_RAW = "UNA:+.? '"

T3_LAB_RESULT_WITH_NHS_NAME_ADDR_PC_DOB_1 = (
    "PAD+9918246533:801+++BARRINGT PICKLED++:ARMAT++++800:11091983:911'"
    "NHS+1:838+G6053954:814:202'"
    "NHS+1445:839'"
    "DTM+832:20161208:102'"
    "RAR+X:843+R:864+834:36:912'"
    "PCD+840:00000001+++Y:841+U:842'"
    "NAD+HP++67 JENNERT RUE LEIGHTON BUZZARD LU6: 5BT")

T3_LAB_RESULT_WITH_NHS_NAME_ADDR_PC_DOB_2 = (
    "PAD+9918246533:801+++BARRINGT PICKLED++:ARMAT++++800:11091983:911'"
    "NHS+1:838+G6053954:814:202'"
    "NHS+1445:839'"
    "DTM+832:20171208:102'"
    "RAR+X:843+R:864+834:36:912'"
    "PCD+840:00000002+++Y:841+U:842'"
    "NAD+HP++67 JENNERT RUE LEIGHTON BUZZARD LU6: 5BT")

T3_LAB_RESULT_WITH_NHS_NAME_ADDR_PC_DOB_3 = (
    "PAD+9918246533:801+++BARRINGT PICKLED++:ARMAT++++800:11091983:911'"
    "NHS+1:838+G6053954:814:202'"
    "NHS+1445:839'"
    "DTM+832:20181208:102'"
    "RAR+X:843+R:864+834:36:912'"
    "PCD+840:00000003+++Y:841+U:842'"
    "NAD+HP++67 JENNERT RUE LEIGHTON BUZZARD LU6: 5BT")

T3_LAB_RESULT_WITH_NHS_NAME_ADDR_PC_DOB_4 = (
    "PAD+9918246533:801+++BARRINGT PICKLED++:ARMAT++++800:11091983:911'"
    "NHS+1:838+G6053954:814:202'"
    "NHS+1445:839'"
    "DTM+832:20191208:102'"
    "RAR+X:843+R:864+834:36:912'"
    "PCD+840:00000004+++Y:841+U:842'"
    "NAD+HP++67 JENNERT RUE LEIGHTON BUZZARD LU6: 5BT")

T3_LAB_RESULT_WITH_NHS_NAME_ADDR_PC_DOB_NO_TEST_DATE = (
    "PAD+:801+++++:ERIC++++800:09071989:911'"
    "NHS+1:838+G2959814:814:202'"
    "NHS+567:839'"
    "RAR+X:843+A:864+834:36:912'"
    "PCD+840:19543034++++0:842+Y'"
    "NAD+HP++5 SOVERIGNE ROAD LEIGHTON BUZZARD B:EDFORDSHIRE LU3 8RW"
    )

T3_LAB_RESULT_WITH_NO_NHS_NAME_OR_DOB = (
    "NHS+1:838+G6053954:814:202'"
    "NHS+1445:839'"
    "DTM+832:20191208:102'"
    "RAR+X:843+R:864+834:36:912'"
    "PCD+840:00000004+++Y:841+U:842'"
    "NAD+HP++67 JENNERT RUE LEIGHTON BUZZARD LU6: 5BT")


EXAMPLE_RESULT_ITEM_TYPE_3_1 = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_WITH_NHS_NAME_ADDR_PC_DOB_1,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 1,
    'sending_lab': 'NCL3',
    'decoded': {
        'cytology_lab_code': 'I'
    }
}

EXAMPLE_RESULT_ITEM_TYPE_3_2 = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19e',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_WITH_NHS_NAME_ADDR_PC_DOB_2,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 2,
    'sending_lab': 'NCL3',
    'decoded': {
        'cytology_lab_code': 'I'
    }
}

EXAMPLE_RESULT_ITEM_TYPE_3_3 = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_WITH_NHS_NAME_ADDR_PC_DOB_3,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 3,
    'sending_lab': 'NCL3',
    'decoded': {
        'cytology_lab_code': 'I'
    }
}

EXAMPLE_RESULT_ITEM_TYPE_3_4 = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19e',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_WITH_NHS_NAME_ADDR_PC_DOB_4,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 4,
    'sending_lab': 'NCL3',
    'decoded': {
        'cytology_lab_code': 'I'
    }
}

EXAMPLE_RESULT_ITEM_TYPE_3_WITH_NO_TEST_DATE = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_WITH_NHS_NAME_ADDR_PC_DOB_NO_TEST_DATE,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 1,
    'sending_lab': 'NCL3',
    'decoded': {
        'cytology_lab_code': 'I'
    }
}

EXAMPLE_RESULT_ITEM_TYPE_3_WITH_NO_NHS_NAME_OR_DOB = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_WITH_NO_NHS_NAME_OR_DOB,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 1,
    'sending_lab': 'NCL3',
    'decoded': {
        'cytology_lab_code': 'I'
    }
}
