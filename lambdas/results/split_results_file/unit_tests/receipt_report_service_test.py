from unittest import TestCase
from mock import call, patch
from datetime import datetime
from split_results_file.log_references import LogReference


class TestReceiptReportService(TestCase):

    mock_env_vars = {
        'MESH_OUTBOUND_MESSAGES_BUCKET': 'test_environment-mesh-outbound-messages',
        'MESH_FILES_TO_SEND_QUEUE_URL': 'mesh_file_to_send_queue_url',
        'RECEIPT_REPORT_FROM_MAILBOX_ID': 'from_mailbox_id',
        'RECEIPT_REPORT_TO_MAILBOX_ID': 'to_mailbox_id'
    }

    message_to_queue = {
        'from_mailbox': 'from_mailbox_id',
        'to_mailbox': 'to_mailbox_id',
        'workflow_id': '[TBC]',
        'bucket_name': 'mesh-outbound-messages',
        'object_key': 'results_acks/20210205081548123456_EA0F82.dat'
    }

    @classmethod
    @patch('os.environ', mock_env_vars)
    def setUpClass(cls):
        from split_results_file.receipt_report_service import (
            create_and_queue_receipt_report as _create_and_queue_receipt_report,
        )
        global create_and_queue_receipt_report
        create_and_queue_receipt_report = _create_and_queue_receipt_report

    @patch('split_results_file.receipt_report_service.uuid4')
    @patch('split_results_file.receipt_report_service.log')
    @patch('split_results_file.receipt_report_service.datetime')
    @patch('split_results_file.receipt_report_service.put_object')
    @patch('split_results_file.receipt_report_service.send_new_message')
    @patch('split_results_file.receipt_report_service.generate_receipt_report')
    def test_receipt_created_and_queued_on_successful_receipt_of_edifact(self,
                                                                         generate_receipt_report_mock,
                                                                         send_new_message_mock,
                                                                         put_object_mock,
                                                                         datetime_mock,
                                                                         log_mock,
                                                                         uuid4_mock):
        raw_edifact = 'test_receipt_report'
        generate_receipt_report_mock.return_value = raw_edifact

        datetime_mock.now.return_value = datetime(2021, 2, 5, 8, 15, 48, 123456)
        uuid4_mock.return_value = 'ea0f821a-f1af-494e-9cc5-cb8dbe3c4ad3'

        expected_log = [
            call({'log_reference': LogReference.SPLIT0016}),
            call({'log_reference': LogReference.SPLIT0017}),
            call({'log_reference': LogReference.SPLIT0018, 'bucket_name': 'test_environment-mesh-outbound-messages'}),
            call({'log_reference': LogReference.SPLIT0019}),
            call({'log_reference': LogReference.SPLIT0020, 'queue_url': 'mesh_file_to_send_queue_url'}),
            call({'log_reference': LogReference.SPLIT0021})
        ]

        create_and_queue_receipt_report(raw_edifact, '2021-02-05', True)

        log_mock.assert_has_calls(expected_log)

        put_object_mock.assert_called_with('test_environment-mesh-outbound-messages',
                                           'test_receipt_report',
                                           'results_acks/20210205081548123456_EA0F82.dat')
        send_new_message_mock.assert_called_with('mesh_file_to_send_queue_url', self.message_to_queue)

    @patch('split_results_file.receipt_report_service.log')
    @patch('split_results_file.receipt_report_service.generate_receipt_report')
    def test_error_logged_when_create_and_queue_receipt_report_fails(self,
                                                                     generate_receipt_report_mock,
                                                                     log_mock):
        generate_receipt_report_mock.side_effect = Exception('ERROR')

        with self.assertRaises(Exception):
            create_and_queue_receipt_report('test_receipt_report', '2020-01-01', True)

        log_mock.assert_called_with({'log_reference': LogReference.SPLIT0022, 'error': 'ERROR'})
