from unittest import TestCase
from ddt import ddt, data
from mock import patch, call, MagicMock, Mock
import json
from botocore.exceptions import ClientError
from common.unit_tests.utils_tests.participant_utils_test import EXAMPLE_UUID
from split_results_file.log_references import LogReference
from common.test_mocks.mock_events import sqs_mock_event
from .split_results_file_test_data import (
    EDIFACT_UNA_HEADER_RAW,
    EDIFACT_HEADER_RAW,
    T3_LAB_RESULT_3,
    EXAMPLE_TYPE_3_WITH_3_RESULTS_SPLIT,
    EXAMPLE_TYPE_9_WITH_3_RESULTS_SPLIT,
    EXAMPLE_RESULT_ITEM_TYPE_3,
    EXAMPLE_RESULT_ITEM_TYPE_3_WITH_CYTOLOGY_LAB_CODE,
    EXAMPLE_RESULT_ITEM_TYPE_3_WITH_PATHOLOGY_LAB,
    EXAMPLE_RESULT_ITEM_TYPE_3_WITH_CYTOLOGY_LAB_CODE_AND_PATHOLOGY_LAB,
    EXAMPLE_TYPE_3_RAW,
    EXAMPLE_TYPE_3_RAW_WITH_LF,
    EXAMPLE_TYPE_3_RAW_WITH_CRLF,
    EXAMPLE_RESULT_ITEM_TYPE_3_WOL3_1,
    EXAMPLE_RESULT_ITEM_TYPE_3_WOL3_2,
    EXAMPLE_RESULT_ITEM_TYPE_3_WOL3_3,
    EXAMPLE_RESULT_ITEM_TYPE_9_CR01_1,
    EXAMPLE_RESULT_ITEM_TYPE_9_CR01_2,
    EXAMPLE_RESULT_ITEM_TYPE_9_CR01_3
)


@ddt
class TestSplitResultsFiles(TestCase):

    mock_env_vars = {
        'RESULTS_COMPLETED_BUCKET': 'results_completed_bucket',
        'RESULTS_FAILED_BUCKET': 'results_failed_bucket',
        'RESULTS_TO_DECODE_SQS_URL': 'results_to_decode_sqs_url'
    }
    EXAMPLE_UUID = '98f1f2c2-fd40-4e75-9d36-d171479db19d'

    @patch('os.environ', mock_env_vars)
    @patch('boto3.resource', MagicMock())
    @patch('boto3.client', MagicMock())
    def setUp(self):
        import split_results_file.split_results_file as _split_results_file
        self.split_results_file = _split_results_file
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = 'split_results_file_test'

    @patch('split_results_file.split_results_file.log')
    @patch('split_results_file.split_results_file.get_s3_data_from_queue_event')
    @patch('split_results_file.split_results_file.get_file_contents_from_bucket')
    @patch('split_results_file.split_results_file.get_result_items_from_edifact')
    @patch('split_results_file.split_results_file.reject_results_with_duplicate_demographics')
    @patch('split_results_file.split_results_file.write_result_items_to_dynamo_db_table')
    @patch('split_results_file.split_results_file.add_results_to_sqs_queue')
    @patch('split_results_file.split_results_file.move_file_to_processed_bucket')
    @patch('split_results_file.split_results_file.create_and_queue_receipt_report')
    def test_edifact_is_split_and_sent_to_dynamo_db_and_sqs_queue(self,
                                                                  create_and_queue_receipt_report_mock,
                                                                  move_file_to_processed_bucket_mock,
                                                                  add_results_to_sqs_queue_mock,
                                                                  write_result_items_to_dynamo_db_table_mock,
                                                                  reject_results_with_duplicate_demographics_mock,
                                                                  get_result_items_from_edifact_mock,
                                                                  get_file_contents_from_bucket_mock,
                                                                  get_s3_data_from_queue_event_mock,
                                                                  log_mock):
        file_name = 'edifact-file-t3.dat'
        bucket = 'example_bucket_name'
        last_modified = '2020-10-10'
        message = {
            'name': file_name,
            'bucket': bucket,
            'last_modified': last_modified,
            'internal_id': '1'
        }
        event = sqs_mock_event(message)
        file_contents = "TestFileContentsString"
        results = [{'results_id': 'uuid', 'raw': 'SaturdayNight', 'received_time': '2020-10-10'}]
        edifact_header = ''

        get_s3_data_from_queue_event_mock.return_value = (file_name, bucket, last_modified)
        get_file_contents_from_bucket_mock.return_value = {
            'body': file_contents,
            'internal_id': 'internal_id'
        }
        get_result_items_from_edifact_mock.return_value = results, edifact_header
        reject_results_with_duplicate_demographics_mock.return_value = results

        self.split_results_file.lambda_handler(event, self.context)

        log_calls = [call({'log_reference': LogReference.SPLIT0001}),
                     call({'log_reference': LogReference.SPLIT0002, 'file_name': 'edifact-file-t3.dat'}),
                     call({'log_reference': LogReference.SPLIT0003}),
                     call({'log_reference': LogReference.SPLIT0004, 'number_of_results': 1}),
                     call({'log_reference': LogReference.SPLIT0005}),
                     call({'log_reference': LogReference.SPLIT0006})]

        log_mock.assert_has_calls(log_calls)
        get_s3_data_from_queue_event_mock.assert_called_with(event)
        get_file_contents_from_bucket_mock.assert_called_with(bucket, file_name)
        get_result_items_from_edifact_mock.assert_called_with(file_contents, last_modified, file_name)
        create_and_queue_receipt_report_mock.assert_called_with(file_contents, last_modified, True)
        reject_results_with_duplicate_demographics_mock.assert_called_with(results, edifact_header)
        write_result_items_to_dynamo_db_table_mock.assert_called_with(results)
        add_results_to_sqs_queue_mock.assert_called_with(results)
        move_file_to_processed_bucket_mock.assert_called_with(bucket, 'results_completed_bucket', file_name)

    @patch('split_results_file.split_results_file.log')
    @patch('split_results_file.split_results_file.get_s3_data_from_queue_event')
    @patch('split_results_file.split_results_file.get_file_contents_from_bucket')
    @patch('split_results_file.split_results_file.get_result_items_from_edifact')
    @patch('split_results_file.split_results_file.write_result_items_to_dynamo_db_table')
    @patch('split_results_file.split_results_file.add_results_to_sqs_queue')
    @patch('split_results_file.split_results_file.move_file_to_processed_bucket')
    def test_file_is_moved_to_failed_bucket_given_error_getting_file_contents(
            self,
            move_file_to_processed_bucket_mock,
            add_results_to_sqs_queue_mock,
            write_result_items_to_dynamo_db_table_mock,
            get_result_items_from_edifact_mock,
            get_file_contents_from_bucket_mock,
            get_s3_data_from_queue_event_mock,
            log_mock):
        file_name = 'edifact-file-t3.dat'
        bucket = 'example_bucket_name'
        last_modified = '2020-10-10'
        message = {
            'name': file_name,
            'bucket': bucket,
            'last_modified': last_modified,
            'internal_id': '1'
        }
        event = sqs_mock_event(message)

        get_s3_data_from_queue_event_mock.return_value = (file_name, bucket, last_modified)
        get_file_contents_from_bucket_mock.side_effect = Exception('No such key "Body"')

        self.split_results_file.lambda_handler(event, self.context)

        expected_log_calls = [call({'log_reference': LogReference.SPLIT0001}),
                              call({'log_reference': LogReference.SPLIT0002, 'file_name': 'edifact-file-t3.dat'}),
                              call({'log_reference': LogReference.SPLIT0014, 'error': 'No such key "Body"'}),
                              call({'log_reference': LogReference.SPLIT0015})]

        log_mock.assert_has_calls(expected_log_calls)
        get_s3_data_from_queue_event_mock.assert_called_with(event)
        get_file_contents_from_bucket_mock.assert_called_with(bucket, file_name)
        move_file_to_processed_bucket_mock.assert_called_with(bucket, 'results_failed_bucket', file_name)
        get_result_items_from_edifact_mock.assert_not_called()
        write_result_items_to_dynamo_db_table_mock.assert_not_called()
        add_results_to_sqs_queue_mock.assert_not_called()

    @patch('split_results_file.split_results_file.log')
    @patch('split_results_file.split_results_file.get_s3_data_from_queue_event')
    @patch('split_results_file.split_results_file.get_file_contents_from_bucket')
    @patch('split_results_file.split_results_file.get_result_items_from_edifact')
    @patch('split_results_file.split_results_file.write_result_items_to_dynamo_db_table')
    @patch('split_results_file.split_results_file.add_results_to_sqs_queue')
    @patch('split_results_file.split_results_file.move_file_to_processed_bucket')
    def test_file_is_moved_to_failed_bucket_given_error_getting_result_items_from_file(
            self,
            move_file_to_processed_bucket_mock,
            add_results_to_sqs_queue_mock,
            write_result_items_to_dynamo_db_table_mock,
            get_result_items_from_edifact_mock,
            get_file_contents_from_bucket_mock,
            get_s3_data_from_queue_event_mock,
            log_mock):
        file_name = 'edifact-file-t3.dat'
        bucket = 'example_bucket_name'
        last_modified = '2020-10-10'
        message = {
            'name': file_name,
            'bucket': bucket,
            'last_modified': last_modified,
            'internal_id': '1'
        }
        event = sqs_mock_event(message)
        file_contents = "TestFileContentsString"

        get_s3_data_from_queue_event_mock.return_value = (file_name, bucket, last_modified)
        get_file_contents_from_bucket_mock.return_value = {
            'body': file_contents,
            'internal_id': 'internal_id'
        }
        get_result_items_from_edifact_mock.side_effect = Exception('Index out of range')

        self.split_results_file.lambda_handler(event, self.context)

        expected_log_calls = [call({'log_reference': LogReference.SPLIT0001}),
                              call({'log_reference': LogReference.SPLIT0002, 'file_name': 'edifact-file-t3.dat'}),
                              call({'log_reference': LogReference.SPLIT0003}),
                              call({'log_reference': LogReference.SPLIT0014, 'error': 'Index out of range'}),
                              call({'log_reference': LogReference.SPLIT0015})]

        log_mock.assert_has_calls(expected_log_calls)
        get_s3_data_from_queue_event_mock.assert_called_with(event)
        get_file_contents_from_bucket_mock.assert_called_with(bucket, file_name)
        get_result_items_from_edifact_mock.assert_called_with(file_contents, last_modified, file_name)
        move_file_to_processed_bucket_mock.assert_called_with(bucket, 'results_failed_bucket', file_name)
        write_result_items_to_dynamo_db_table_mock.assert_not_called()
        add_results_to_sqs_queue_mock.assert_not_called()

    def test_extracting_data_from_sqs_queue_message(self):
        expected_file_name = 'edifact-file-t3.dat'
        expected_bucket = 'example_bucket_name'
        expected_last_modified = '2020-10-10'

        message_as_dict = {'name': expected_file_name,
                           'bucket': expected_bucket,
                           'last_modified': expected_last_modified}
        message = json.dumps(message_as_dict)
        event = {'Records': [{'body': message}]}

        file_name, bucket, last_modified = self.split_results_file.get_s3_data_from_queue_event(event)

        self.assertEqual(expected_file_name, file_name)
        self.assertEqual(expected_bucket, bucket)
        self.assertEqual(expected_last_modified, last_modified)

    @data(
        (),
        (EDIFACT_UNA_HEADER_RAW)
    )
    @patch('split_results_file.split_results_file.split_raw_edifact_into_raw_lab_results')
    @patch('split_results_file.split_results_file.get_una_header')
    @patch('split_results_file.split_results_file.uuid')
    @patch('split_results_file.split_results_file.build_source_file_identifier')
    def test_getting_results_from_an_edifact_type_3_file(self,
                                                         edifact_header,
                                                         build_source_identifier_mock,
                                                         uuid_mock,
                                                         get_una_header_mock,
                                                         split_raw_edifact_into_raw_lab_results_mock):
        raw_edifact = 'RAW_EDIFACT'
        split_edifact = EXAMPLE_TYPE_3_WITH_3_RESULTS_SPLIT.copy()

        example_result_items = [EXAMPLE_RESULT_ITEM_TYPE_3_WOL3_1,
                                EXAMPLE_RESULT_ITEM_TYPE_3_WOL3_2,
                                EXAMPLE_RESULT_ITEM_TYPE_3_WOL3_3]

        if edifact_header:
            split_edifact[0] = edifact_header + split_edifact[0]
            for result in example_result_items:
                result['una_header'] = edifact_header

        split_raw_edifact_into_raw_lab_results_mock.return_value = 3, split_edifact

        get_una_header_mock.return_value = edifact_header
        uuid_mock.uuid4.return_value = EXAMPLE_UUID
        build_source_identifier_mock.return_value = 'file_name.dat0WOL32020-10-10'
        received_time = '2020-10-10'
        file_name = 'file_name.dat'
        expected_results = example_result_items, edifact_header
        results = self.split_results_file.get_result_items_from_edifact(raw_edifact, received_time, file_name)

        split_raw_edifact_into_raw_lab_results_mock.assert_called_with(raw_edifact)
        self.assertEqual(expected_results, results)

    @data(
        (),
        (EDIFACT_UNA_HEADER_RAW)
    )
    @patch('split_results_file.split_results_file.split_raw_edifact_into_raw_lab_results')
    @patch('split_results_file.split_results_file.get_una_header')
    @patch('split_results_file.split_results_file.uuid')
    @patch('split_results_file.split_results_file.build_source_file_identifier')
    def test_getting_results_from_an_edifact_type_9_file(self,
                                                         edifact_header,
                                                         build_source_identifier_mock,
                                                         uuid_mock,
                                                         get_una_header_mock,
                                                         split_raw_edifact_into_raw_lab_results_mock):
        raw_edifact = 'RAW_EDIFACT'
        split_edifact = EXAMPLE_TYPE_9_WITH_3_RESULTS_SPLIT.copy()

        example_result_items = [EXAMPLE_RESULT_ITEM_TYPE_9_CR01_1,
                                EXAMPLE_RESULT_ITEM_TYPE_9_CR01_2,
                                EXAMPLE_RESULT_ITEM_TYPE_9_CR01_3]
        if (edifact_header):
            split_edifact[0] = edifact_header + split_edifact[0]
            for result in example_result_items:
                result['una_header'] = edifact_header

        split_raw_edifact_into_raw_lab_results_mock.return_value = 9, split_edifact
        get_una_header_mock.return_value = edifact_header
        uuid_mock.uuid4.return_value = EXAMPLE_UUID
        build_source_identifier_mock.return_value = 'file_name.dat0WOL32020-10-10'
        received_time = '2020-10-10'
        file_name = 'filename.dat'
        expected_results = example_result_items, edifact_header
        results = self.split_results_file.get_result_items_from_edifact(raw_edifact, received_time, file_name)

        split_raw_edifact_into_raw_lab_results_mock.assert_called_with(raw_edifact)
        self.assertEqual(expected_results, results)

    @data(
        (EXAMPLE_TYPE_3_RAW_WITH_LF),
        (EXAMPLE_TYPE_3_RAW_WITH_CRLF)
    )
    @patch('split_results_file.split_results_file.create_result_item')
    @patch('split_results_file.split_results_file.decode_lab_result')
    @patch('split_results_file.split_results_file.tokenize_edifact')
    @patch('split_results_file.split_results_file.get_una_header')
    @patch('split_results_file.split_results_file.split_raw_edifact_into_raw_lab_results')
    @patch('split_results_file.split_results_file.log')
    def test_getting_results_from_a_raw_edifact_with_CR_AND_LF_characters(
            self,
            edifact_example,
            log_mock,
            split_raw_edifact_into_raw_lab_results_mock,
            get_una_header_mock,
            tokenize_edifact_mock,
            decode_result_mock,
            create_result_mock):
        received_time = '2020-10-10'
        file_name = 'filename.dat'

        decode_result_mock.return_value = {
            'sending_lab': 'NCL3',
            'raw_nhais_cipher': 'WIG1'
        }

        split_edifact = EXAMPLE_TYPE_3_WITH_3_RESULTS_SPLIT.copy()
        split_raw_edifact_into_raw_lab_results_mock.return_value = 3, split_edifact

        # Act
        self.split_results_file.get_result_items_from_edifact(edifact_example, received_time, file_name)
        # Assert
        split_raw_edifact_into_raw_lab_results_mock.assert_called_with(EXAMPLE_TYPE_3_RAW)

    @patch('split_results_file.split_results_file.log')
    @patch('split_results_file.split_results_file.split_raw_edifact_into_raw_lab_results')
    def test_getting_results_from_edifact_when_the_format_is_incorrect(self,
                                                                       split_raw_edifact_into_raw_lab_results_mock,
                                                                       log_mock):
        raw_edifact = 'NOT EDIFACT'
        received_time = '2020-10-10'
        file_name = 'filename.dat'
        split_raw_edifact_into_raw_lab_results_mock.side_effect = Exception('Incorrect format')

        with self.assertRaises(Exception):
            self.split_results_file.get_result_items_from_edifact(raw_edifact, received_time, file_name)
        log_mock.assert_called()

    @data(
        ({}),
        (EDIFACT_HEADER_RAW)
    )
    @patch('split_results_file.split_results_file.uuid')
    def test_creating_a_result_item_from_result_edifact_partial_with_cytology_lab_code(self, edifact_header, mock_uuid):
        edifact_type = 3
        raw_result = T3_LAB_RESULT_3
        received_time = '2020-10-10'
        sending_lab = 'NCL3'
        cytology_lab_code = '62010'
        mock_uuid.uuid4.return_value = '98f1f2c2-fd40-4e75-9d36-d171479db19d'
        file_name = 'file_name.dat'
        raw_nhais_cipher = 'TEST1'
        index = 3

        result_item = self.split_results_file.create_result_item(
            edifact_type, edifact_header, raw_result, received_time, sending_lab, cytology_lab_code, None,
            raw_nhais_cipher, file_name, index)

        expected_result = EXAMPLE_RESULT_ITEM_TYPE_3_WITH_CYTOLOGY_LAB_CODE
        if edifact_header and edifact_header['una_header']:
            expected_result['una_header'] = edifact_header['una_header']

        self.assertEqual(expected_result, result_item)

    @data(
        ({}),
        (EDIFACT_HEADER_RAW)
    )
    @patch('split_results_file.split_results_file.uuid')
    def test_creating_a_result_item_from_result_edifact_partial_with_pathology_lab(self, edifact_header, mock_uuid):
        edifact_type = 3
        raw_result = T3_LAB_RESULT_3
        received_time = '2020-10-10'
        sending_lab = 'NCL3'
        pathology_lab = 'lab1'
        mock_uuid.uuid4.return_value = '98f1f2c2-fd40-4e75-9d36-d171479db19d'
        file_name = 'file_name.dat'
        raw_nhais_cipher = 'TEST1'
        index = 3

        result_item = self.split_results_file.create_result_item(
            edifact_type, edifact_header, raw_result, received_time, sending_lab, None, pathology_lab,
            raw_nhais_cipher, file_name, index)

        expected_result = EXAMPLE_RESULT_ITEM_TYPE_3_WITH_PATHOLOGY_LAB

        if edifact_header and edifact_header['una_header']:
            expected_result['una_header'] = edifact_header['una_header']

        self.assertEqual(expected_result, result_item)

    @data(
        ({}),
        (EDIFACT_HEADER_RAW)
    )
    @patch('split_results_file.split_results_file.uuid')
    def test_creating_a_result_item_from_result_edifact_partial_with_cytology_lab_code_and_pathology_lab(
        self, edifact_header, mock_uuid
    ):
        edifact_type = 3
        edifact_header = ''
        raw_result = T3_LAB_RESULT_3
        received_time = '2020-10-10'
        sending_lab = 'NCL3'
        cytology_lab_code = '62010'
        pathology_lab = 'lab1'
        mock_uuid.uuid4.return_value = '98f1f2c2-fd40-4e75-9d36-d171479db19d'
        file_name = 'file_name.dat'
        raw_nhais_cipher = 'TEST1'
        index = 3

        result_item = self.split_results_file.create_result_item(
            edifact_type, edifact_header, raw_result, received_time, sending_lab, cytology_lab_code, pathology_lab,
            raw_nhais_cipher, file_name, index)

        expected_result = EXAMPLE_RESULT_ITEM_TYPE_3_WITH_CYTOLOGY_LAB_CODE_AND_PATHOLOGY_LAB
        if edifact_header and edifact_header['una_header']:
            expected_result['una_header'] = edifact_header['una_header']
        self.assertEqual(expected_result, result_item)

    @patch('split_results_file.split_results_file.create_replace_record')
    def test_storing_results_to_dynamo_db_table(self, create_replace_record_mock):
        results = [EXAMPLE_RESULT_ITEM_TYPE_3, EXAMPLE_RESULT_ITEM_TYPE_3, EXAMPLE_RESULT_ITEM_TYPE_3]
        self.split_results_file.write_result_items_to_dynamo_db_table(results)

        create_replace_record_mock.assert_called_with(results[0])
        create_replace_record_mock.assert_called_with(results[1])
        create_replace_record_mock.assert_called_with(results[2])

    @patch('split_results_file.split_results_file.log')
    @patch('split_results_file.split_results_file.create_replace_record')
    def test_failing_to_write_to_dynamo_db_table(self, create_replace_record_mock, log_mock):
        results = [EXAMPLE_RESULT_ITEM_TYPE_3, EXAMPLE_RESULT_ITEM_TYPE_3, EXAMPLE_RESULT_ITEM_TYPE_3]
        create_replace_record_mock.side_effect = \
            ClientError(error_response={'Error': {'Code': 'InternalFailure'}},
                        operation_name='put_item')

        with self.assertRaises(Exception):
            self.split_results_file.write_result_items_to_dynamo_db_table(results)
        log_mock.assert_called()

    @patch('split_results_file.split_results_file.get_internal_id')
    @patch('split_results_file.split_results_file.send_new_message')
    def test_adding_results_to_sqs_queue(self, send_new_message_mock, get_internal_id_mock):
        results = [EXAMPLE_RESULT_ITEM_TYPE_3]
        get_internal_id_mock.return_value = '1'
        exepected_queue_item = {
            'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
            'received_time': '2020-10-10',
            'internal_id': '1'
        }
        example_sqs_url = "example_sqs_url.com"
        self.split_results_file.RESULTS_TO_DECODE_SQS_URL = example_sqs_url

        self.split_results_file.add_results_to_sqs_queue(results)
        send_new_message_mock.assert_called_with(example_sqs_url, exepected_queue_item)

    @patch('split_results_file.split_results_file.log')
    @patch('split_results_file.split_results_file.send_new_message')
    def test_failing_to_add_results_to_sqs_queue(self, send_new_message_mock, log_mock):
        results = [EXAMPLE_RESULT_ITEM_TYPE_3]
        send_new_message_mock.side_effect = \
            ClientError(error_response={'Error': {'Code': 'InternalFailure'}},
                        operation_name='send_message')

        with self.assertRaises(Exception):
            self.split_results_file.add_results_to_sqs_queue(results)
        log_mock.assert_called()

    @patch('split_results_file.split_results_file.move_object_to_bucket_and_transition_to_infrequent_access_class')
    def test_move_file_to_processed_bucket_calls_move_object_with_correct_parameters(self, move_object_to_bucket_mock):
        file_name = 'edifact-file-t3.dat'
        bucket = 'example_bucket_name'
        example_completed_bucket = "example_completed_bucket_name"
        self.split_results_file.COMPLETED_BUCKET = example_completed_bucket

        self.split_results_file.move_file_to_processed_bucket(bucket, example_completed_bucket, file_name)
        move_object_to_bucket_mock.assert_called_with(bucket, example_completed_bucket, file_name)

    @patch('split_results_file.split_results_file.log')
    @patch('split_results_file.split_results_file.move_object_to_bucket_and_transition_to_infrequent_access_class')
    def test_failing_to_move_file_to_processed_bucket_throws_exception(self, move_object_to_bucket_mock, log_mock):
        file_name = 'edifact-file-t3.dat'
        bucket = 'example_bucket_name'
        target_bucket = 'example_second_bucket_name'

        move_object_to_bucket_mock.side_effect = \
            ClientError(error_response={'Error': {'Code': 'InternalFailure'}},
                        operation_name='send_message')

        expected_error_message = 'An error occurred (InternalFailure) when calling the send_message operation: Unknown'

        with self.assertRaises(Exception):
            self.split_results_file.move_file_to_processed_bucket(bucket, target_bucket, file_name)

        error_log = log_mock.call_args_list[0][0][0]
        self.assertDictEqual({'log_reference': LogReference.SPLIT0013, 'error': expected_error_message}, error_log)

    @patch('split_results_file.split_results_file.split_raw_edifact_into_raw_lab_results')
    @patch('split_results_file.split_results_file.create_and_queue_receipt_report')
    def test_failure_receipt_report_generated_on_split_error(
        self,
        create_and_queue_receipt_report_mock,
        split_raw_edifact_into_raw_lab_results_mock
    ):
        split_raw_edifact_into_raw_lab_results_mock.side_effect = Exception()

        with self.assertRaises(Exception):
            self.split_results_file.get_result_items_from_edifact(EXAMPLE_TYPE_3_RAW, 'received_time', 'file_name')

        split_raw_edifact_into_raw_lab_results_mock.assert_called()
        create_and_queue_receipt_report_mock.assert_called_with(EXAMPLE_TYPE_3_RAW, 'received_time', False)

    @patch('split_results_file.split_results_file.get_una_header')
    @patch('split_results_file.split_results_file.tokenize_edifact')
    @patch('split_results_file.split_results_file.create_and_queue_receipt_report')
    def test_failure_receipt_report_generated_on_tokenize_error(
        self,
        create_and_queue_receipt_report_mock,
        tokenize_edifact_mock,
        get_una_header_mock
    ):
        get_una_header_mock.return_value = ''
        tokenize_edifact_mock.side_effect = Exception()

        with self.assertRaises(Exception):
            self.split_results_file.get_result_items_from_edifact(EXAMPLE_TYPE_3_RAW, 'received_time', 'file_name')

        create_and_queue_receipt_report_mock.assert_called_with(EXAMPLE_TYPE_3_RAW, 'received_time', False)

    @patch('split_results_file.split_results_file.decode_lab_result')
    @patch('split_results_file.split_results_file.get_una_header')
    @patch('split_results_file.split_results_file.split_raw_edifact_into_raw_lab_results')
    @patch('split_results_file.split_results_file.tokenize_edifact')
    @patch('split_results_file.split_results_file.create_and_queue_receipt_report')
    def test_failure_receipt_report_generated_on_decode_error(
        self,
        create_and_queue_receipt_report_mock,
        tokenize_edifact_mock,
        split_raw_edifact_into_raw_lab_results_mock,
        get_una_header_mock,
        decode_lab_result_mock
    ):
        get_una_header_mock.return_value = ''
        split_raw_edifact_into_raw_lab_results_mock.return_value = [], ''
        tokenize_edifact_mock.return_value = []
        decode_lab_result_mock.side_effect = Exception()

        with self.assertRaises(Exception):
            self.split_results_file.get_result_items_from_edifact(EXAMPLE_TYPE_3_RAW, 'received_time', 'file_name')

        get_una_header_mock.assert_called_with(EXAMPLE_TYPE_3_RAW)
        split_raw_edifact_into_raw_lab_results_mock.assert_called_with(EXAMPLE_TYPE_3_RAW)
        create_and_queue_receipt_report_mock.assert_called_with(EXAMPLE_TYPE_3_RAW, 'received_time', False)

    @patch('split_results_file.split_results_file.decode_lab_result')
    @patch('split_results_file.split_results_file.get_una_header')
    @patch('split_results_file.split_results_file.split_raw_edifact_into_raw_lab_results')
    @patch('split_results_file.split_results_file.tokenize_edifact')
    @patch('split_results_file.split_results_file.create_result_item')
    @patch('split_results_file.split_results_file.create_and_queue_receipt_report')
    def test_failure_receipt_report_generated_on_create_result_error(
        self,
        create_and_queue_receipt_report_mock,
        create_result_item_mock,
        tokenize_edifact_mock,
        split_raw_edifact_into_raw_lab_results_mock,
        get_una_header_mock,
        decode_lab_result_mock
    ):
        get_una_header_mock.return_value = ''
        split_raw_edifact_into_raw_lab_results_mock.return_value = [], ''
        tokenize_edifact_mock.return_value = []
        decode_lab_result_mock.return_value = {}

        create_result_item_mock.side_effect = Exception()

        with self.assertRaises(Exception):
            self.split_results_file.get_result_items_from_edifact(EXAMPLE_TYPE_3_RAW, 'received_time', 'file_name')

        create_and_queue_receipt_report_mock.assert_called_with(EXAMPLE_TYPE_3_RAW, 'received_time', False)

    def test_sanitise_nhais_cipher_to_remove_numbers(self):

        # Given
        strings_to_sanitise = ['WIG123', '12WIG34', '']
        expected_strings = ['WIG', 'WIG', None]

        # When
        results = [self.split_results_file.sanitise_nhais_cipher(string) for string in strings_to_sanitise]

        # Then
        self.assertEqual(expected_strings, results)

    def test_nhais_cipher_is_valid_returns_true_for_valid_ciphers(self):

        # Arrange
        valid_nhais_ciphers = ['GAT', 'BRA', 'LNK', 'WAK', 'ROC']

        # Act
        for cipher in valid_nhais_ciphers:
            is_valid = self.split_results_file.nhais_cipher_is_valid(cipher)
            # Assert
            self.assertTrue(is_valid)

    def test_nhais_cipher_is_valid_returns_false_for_invalid_cipher(self):

        # Arrange
        invalid_nhais_cipher = 'BAD'

        # Act
        is_valid = self.split_results_file.nhais_cipher_is_valid(invalid_nhais_cipher)

        # Assert
        self.assertFalse(is_valid)

    @data(
        (True),
        (False)
    )
    @patch('split_results_file.split_results_file.log')
    @patch('split_results_file.split_results_file.create_and_queue_receipt_report')
    @patch('split_results_file.split_results_file.create_result_item')
    @patch('split_results_file.split_results_file.decode_lab_result')
    @patch('split_results_file.split_results_file.tokenize_edifact')
    @patch('split_results_file.split_results_file.split_raw_edifact_into_raw_lab_results')
    @patch('split_results_file.split_results_file.get_una_header')
    def test_exception_thrown_on_empty_or_nonetype_nhais_cipher(
        self,
        is_empty_nhais_cipher,
        get_una_header_mock,
        split_raw_edifact_into_raw_lab_results_mock,
        tokenize_edifact_mock,
        decode_lab_result_mock,
        create_result_item_mock,
        create_and_queue_receipt_report_mock,
        log_mock
    ):
        get_una_header_mock.return_value = ''
        split_raw_edifact_into_raw_lab_results_mock.return_value = '', ['']
        tokenize_edifact_mock.return_value = []
        if is_empty_nhais_cipher:
            decode_lab_result_mock.return_value = {
                'sending_lab': '1', 'cytology_lab_code': '2', 'pathology_lab': '3', 'raw_nhais_cipher': ''
            }
        else:
            decode_lab_result_mock.return_value = {'sending_lab': '1', 'cytology_lab_code': '2', 'pathology_lab': '3'}

        with self.assertRaises(Exception):
            self.split_results_file.get_result_items_from_edifact(
                'raw_edifact', 'received_time', 'file_name'
            )
        create_and_queue_receipt_report_mock.assert_called_once_with('raw_edifact', 'received_time', False)

    @patch('split_results_file.split_results_file.log')
    @patch('split_results_file.split_results_file.create_and_queue_receipt_report')
    @patch('split_results_file.split_results_file.create_result_item')
    @patch('split_results_file.split_results_file.decode_lab_result')
    @patch('split_results_file.split_results_file.tokenize_edifact')
    @patch('split_results_file.split_results_file.split_raw_edifact_into_raw_lab_results')
    @patch('split_results_file.split_results_file.get_una_header')
    def test_exception_thrown_on_invalid_nhais_cipher(
        self,
        get_una_header_mock,
        split_raw_edifact_into_raw_lab_results_mock,
        tokenize_edifact_mock,
        decode_lab_result_mock,
        create_result_item_mock,
        create_and_queue_receipt_report_mock,
        log_mock
    ):
        get_una_header_mock.return_value = ''
        split_raw_edifact_into_raw_lab_results_mock.return_value = '', ['']
        tokenize_edifact_mock.return_value = []
        decode_lab_result_mock.return_value = {
            'sending_lab': '1', 'cytology_lab_code': '2', 'pathology_lab': '3', 'raw_nhais_cipher': 'BAD'
        }

        with self.assertRaises(Exception):
            self.split_results_file.get_result_items_from_edifact(
                'raw_edifact', 'received_time', 'file_name'
            )
        create_and_queue_receipt_report_mock.assert_called_once_with('raw_edifact', 'received_time', False)
