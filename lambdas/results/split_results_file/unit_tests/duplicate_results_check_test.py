from unittest import TestCase
from ddt import ddt, data
from mock import patch, MagicMock, Mock

from .duplicate_results_check_test_data import (
    EDIFACT_UNA_HEADER_RAW,
    EXAMPLE_RESULT_ITEM_TYPE_3_1,
    EXAMPLE_RESULT_ITEM_TYPE_3_2,
    EXAMPLE_RESULT_ITEM_TYPE_3_3,
    EXAMPLE_RESULT_ITEM_TYPE_3_4,
    EXAMPLE_RESULT_ITEM_TYPE_3_WITH_NO_TEST_DATE,
    EXAMPLE_RESULT_ITEM_TYPE_3_WITH_NO_NHS_NAME_OR_DOB
)


@ddt
class TestDuplicateResultsCheck(TestCase):

    @patch('boto3.resource', MagicMock())
    @patch('boto3.client', MagicMock())
    def setUp(self):
        import split_results_file.duplicate_results_check as _duplicate_results_check
        self.duplicate_results_check = _duplicate_results_check
        self.context = Mock()
        self.context.function_name = 'duplicate_results_check_test'

    @data(
        (''),
        (EDIFACT_UNA_HEADER_RAW)
    )
    def test_older_results_with_same_demographics_but_different_slide_numbers_are_rejected(self, edifact_header):
        # 4 results with the same demographics but different slide numbers and test dates
        results = [
            EXAMPLE_RESULT_ITEM_TYPE_3_1.copy(),  # oldest
            EXAMPLE_RESULT_ITEM_TYPE_3_2.copy(),  # then this one
            EXAMPLE_RESULT_ITEM_TYPE_3_3.copy(),  # then this one
            EXAMPLE_RESULT_ITEM_TYPE_3_4.copy()   # newest
        ]
        results = self.duplicate_results_check.reject_results_with_duplicate_demographics(results, edifact_header)
        # all but newest should be rejected
        for item in results[:-1]:
            self.assertEqual(item.get('workflow_state'), 'rejected')
            self.assertEqual(item.get('rejected_reason'), 'duplicate demographics')
        self.assertEqual(results[-1].get('workflow_state'), None)
        self.assertEqual(results[-1].get('rejected_reason'), None)

    @data(
        (''),
        (EDIFACT_UNA_HEADER_RAW)
    )
    def test_results_with_same_demographics_and_same_slide_numbers_are_not_rejected(self, edifact_header):
        # 4 results with the same demographics and same slide numbers
        results = [
            EXAMPLE_RESULT_ITEM_TYPE_3_1.copy(),
            EXAMPLE_RESULT_ITEM_TYPE_3_1.copy(),
            EXAMPLE_RESULT_ITEM_TYPE_3_1.copy(),
            EXAMPLE_RESULT_ITEM_TYPE_3_1.copy()
        ]
        results = self.duplicate_results_check.reject_results_with_duplicate_demographics(results, edifact_header)
        # no result should be rejected
        for item in results:
            self.assertEqual(item.get('workflow_state'), None)
            self.assertEqual(item.get('rejected_reason'), None)

    @data(
        (''),
        (EDIFACT_UNA_HEADER_RAW)
    )
    def test_results_with_same_demographics_and_missing_demographics_fields_are_ignored_safely(self, edifact_header):
        results = [
            EXAMPLE_RESULT_ITEM_TYPE_3_WITH_NO_TEST_DATE.copy(),
            EXAMPLE_RESULT_ITEM_TYPE_3_WITH_NO_NHS_NAME_OR_DOB.copy()
        ]
        results = self.duplicate_results_check.reject_results_with_duplicate_demographics(results, edifact_header)
        # no result should be rejected
        for item in results:
            self.assertEqual(item.get('workflow_state'), None)
            self.assertEqual(item.get('rejected_reason'), None)
