EDIFACT_UNA_HEADER_RAW = "UNA:+.? '"

T3_EDIFACT_HEADER_RAW = (
    "UNA:+.? 'UNB+UNOA:2+WOL3+WIG1+191025:1144+00002155++CYTFH++1'UNH+1+CYTFH:0:2:FH'BGM++003+"
    "243:201910251144:306+62'RFF+CSF:131'DTM+829:20191021:102'DTM+830:20191021:102'DT"
    "M+831:20191025:102'NHS+66124:837"
)

T3_UNB_HEADER_RAW = (
    "UNB+UNOA:2+WOL3+WIG1+191025:1144+00002155++CYTFH++1'UNH+1+CYTFH:0:2:FH'BGM++003+"
    "243:201910251144:306+62'RFF+CSF:131'DTM+829:20191021:102'DTM+830:20191021:102'DT"
    "M+831:20191025:102'NHS+62010:837"
)

T3_LAB_RESULT_1 = (
    "PAD+NHS_NUMBER:801+++SURNAME++:FORENAME++++800:01091983:911'"
    "NHS+7:838+1:814:202'"
    "NHS+DIMLGH:839'"
    "DTM+832:20191021:102'"
    "RAR+2:843+S:864+834:00:912'"
    "PCD+840:19035247'"
    "NAD+HP++AAAAAAAAA A AAAA AAAAA AAAA")

T3_LAB_RESULT_2 = (
    "PAD+NHS_NUMBER:801+++SURNAME++:FORENAME++++800:01091983:911'"
    "NHS+7:838+1:814:202'"
    "NHS+DIMLGH:839'"
    "DTM+832:20191021:102'"
    "RAR+2:843+S:864+834:00:912'"
    "PCD+840:19035247'"
    "NAD+HP++AAAAAAAAA A AAAA AAAAA AAAAAA"
)

T3_LAB_RESULT_3 = (
    "PAD+NHS_NUMBER:801+++SSSSSS++:FORENAMEFFFF++++800:01091983:911'"
    "NHS+7:838+1:814:202'"
    "NHS+DIMLGH:839'"
    "DTM+832:20191021:102'"
    "RAR+2:843+S:864+834:00:912'"
    "PCD+840:19035248'"
    "NAD+HP++AA AAA AAA"
)

T3_EDIFACT_FOOTER_RAW = (
    "UNT+22+1'UNZ+1+00002155'"
)

T9_LAB_RESULT_2 = (
    "S01+1'"
    "NAD+SRC+000001:ZZZ'"
    "NAD+'"
    "SND+714522:ZZZ'"
    "S02+2'"
    "PNA+PAT+NNNNNNNNNN:OPI+++SU:SSSSSS+FS:FFFFF'"
    "DTM+329:DDDDD'"
    "DDD:951'"
    "NAD+PAT++AAAA 2, AAAAAAAA AAAA AA:AAAAA AAAAAA AAAAA LOOE PPPP P'"
    "S03+3'"
    "TST+RCD+X:ZZZ+SNO:19093000+A:ZZZ'"
    "DTM+119:20191022:102'"
    "HEA+INM+0:ZZZ'"
    "HEA+ETP'"
    "+Y:ZZZ'"
    "QTY+961:36'"
)

T9_LAB_RESULT_4 = (
    "S01+1'"
    "NAD+SRC+000001:ZZZ'"
    "NAD+SND+710522:ZZZ'"
    "S02+2'"
    "PNA+PAT+NNNNNNNNNN:OPI+++SU:SSSSSSS+FS:FFFFFF'"
    "DTM+329:01091983:951'"
    "NAD+PAT++AAAA AAAAAAA AAAAAAAAA PPPP:PPP'"
    "S03+3'"
    "TST+RCD+2:ZZZ+SN'"
    "O:19093282+R:ZZZ'"
    "DTM+119:20191021:102'"
    "HEA+INM+9:ZZZ'"
    "HEA+ETP+Y:ZZZ'"
    "QTY+961:12'"
)

T9_LAB_RESULT_5 = (
    "S01+1'"
    "NAD+SRC+000001:ZZZ'"
    "NAD+SND+711296:ZZZ'"
    "S02+2'"
    "PNA+PAT+NNNNNNNNNN:OPI+++SU:SSSSS+FS:FFFFFFF'"
    "DTM+329:16101987:951'"
    "NAD+PAT++AA AAAA AAAA AAAAAAAA PPPP PPP'"
    "S03+3'"
    "TST+RCD+8:ZZZ+SNO:19091776+S:ZZZ'"
    "DTM+119:20191017:102'"
    "HEA+INM+9:ZZZ'"
    "HEA+ETP+Y:ZZZ'"
)

T9_UNB_HEADER_RAW = (
    "UNB+UNOA:2+NBT3+CR01+191029:1113+00000114++FHSPRV'UNH+1+FHSPRV:0:2:FH:FHS003'BGM"
    "+++9'NAD+FHS+5QP:954'NAD+PTH+1:963'DTM+137:201910291113:203'DTM+194:191017:101'D"
    "TM+206:191022:101'RFF+TN:01197"
)

T9_HEADER_RAW = (
    "UNB+UNOA:2+NBT3+CR01+191029:1113+00000114++FHSPRV'UNH+1+FHSPRV:0:2:FH:FHS003'BGM"
    "+++9'NAD+FHS+5QP:954'NAD+PTH+1:963'DTM+137:201910291113:203'DTM+194:191017:101'D"
    "TM+206:191022:101'RFF+TN:01197"
)

EXAMPLE_TYPE_3_WITH_3_RESULTS_SPLIT = [T3_UNB_HEADER_RAW, T3_LAB_RESULT_2, T3_LAB_RESULT_1, T3_LAB_RESULT_3]
EXAMPLE_TYPE_9_WITH_3_RESULTS_SPLIT = [T9_HEADER_RAW, T9_LAB_RESULT_5, T9_LAB_RESULT_2, T9_LAB_RESULT_4]

EXAMPLE_RESULT_ITEM_TYPE_3 = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_3,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 3,
    'sending_lab': 'NCL3',
    'decoded': {
        'cytology_lab_code': '62010'
    },
    'unb_header': T3_UNB_HEADER_RAW

}
EXAMPLE_RESULT_ITEM_TYPE_3_WOL3 = {
    **EXAMPLE_RESULT_ITEM_TYPE_3,
    'source_file_identifier': 'file_name.dat0WOL32020-10-10',
    'sending_lab': 'WOL3',
    'raw_nhais_cipher': 'WIG1',
    'sanitised_nhais_cipher': 'WIG',
}

EXAMPLE_RESULT_ITEM_TYPE_3_WOL3_1 = {
    **EXAMPLE_RESULT_ITEM_TYPE_3_WOL3,
    'raw': T3_LAB_RESULT_2,
    'record_index': 0,
}

EXAMPLE_RESULT_ITEM_TYPE_3_WOL3_2 = {
    **EXAMPLE_RESULT_ITEM_TYPE_3_WOL3,
    'raw': T3_LAB_RESULT_1,
    'record_index': 1,
}

EXAMPLE_RESULT_ITEM_TYPE_3_WOL3_3 = {
    **EXAMPLE_RESULT_ITEM_TYPE_3_WOL3,
    'raw': T3_LAB_RESULT_3,
    'record_index': 2,
}

EXAMPLE_RESULT_ITEM_TYPE_3_WITH_EMPTY_DECODED_OBJ = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_3,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 3,
    'sending_lab': 'NCL3',
    'decoded': {}
}

EXAMPLE_RESULT_ITEM_TYPE_3_WITH_CYTOLOGY_LAB_CODE = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_3,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 3,
    'sending_lab': 'NCL3',
    'raw_nhais_cipher': 'TEST1',
    'sanitised_nhais_cipher': 'TEST',
    'decoded': {'cytology_lab_code': '62010'}
}

EXAMPLE_RESULT_ITEM_TYPE_3_WITH_PATHOLOGY_LAB = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_3,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 3,
    'sending_lab': 'NCL3',
    'raw_nhais_cipher': 'TEST1',
    'sanitised_nhais_cipher': 'TEST',
    'decoded': {'pathology_lab': 'lab1'}
}

EXAMPLE_RESULT_ITEM_TYPE_3_WITH_CYTOLOGY_LAB_CODE_AND_PATHOLOGY_LAB = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
    'source_file_identifier': 'file_name.dat3NCL32020-10-10',
    'edifact_type': 3,
    'raw': T3_LAB_RESULT_3,
    'received_time': '2020-10-10',
    'source_file': 'file_name.dat',
    'record_index': 3,
    'sending_lab': 'NCL3',
    'raw_nhais_cipher': 'TEST1',
    'sanitised_nhais_cipher': 'TEST',
    'decoded': {'cytology_lab_code': '62010', 'pathology_lab': 'lab1'}
}

EXAMPLE_RESULT_ITEM_TYPE_9 = {
    'result_id': '98f1f2c2-fd40-4e75-9d36-d171479db19d',
    'source_file_identifier': 'file_name.dat0WOL32020-10-10',
    'edifact_type': 9,
    'raw': T9_LAB_RESULT_5,
    'received_time': '2020-10-10',
    'source_file': 'filename.dat',
    'record_index': 0,
    'sending_lab': 'NBT3',
    'raw_nhais_cipher': 'CR01',
    'sanitised_nhais_cipher': 'CR',
    'decoded': {
        'pathology_lab': '1'
    },
    'unb_header': T9_UNB_HEADER_RAW

}

EXAMPLE_RESULT_ITEM_TYPE_9_CR01_1 = {
    **EXAMPLE_RESULT_ITEM_TYPE_9,
}

EXAMPLE_RESULT_ITEM_TYPE_9_CR01_2 = {
    **EXAMPLE_RESULT_ITEM_TYPE_9,
    'raw': T9_LAB_RESULT_2,
    'record_index': 1,
}

EXAMPLE_RESULT_ITEM_TYPE_9_CR01_3 = {
    **EXAMPLE_RESULT_ITEM_TYPE_9,
    'raw': T9_LAB_RESULT_4,
    'record_index': 2,
}


EXAMPLE_TYPE_3_RAW = (
    f"{T3_EDIFACT_HEADER_RAW}'"
    f"{T3_LAB_RESULT_1}'"
    f"{T3_LAB_RESULT_2}'"
    f"{T3_EDIFACT_FOOTER_RAW}"
)

EXAMPLE_TYPE_3_RAW_WITH_LF = (
    f"{T3_EDIFACT_HEADER_RAW}'\n"
    f"{T3_LAB_RESULT_1}'\n"
    f"{T3_LAB_RESULT_2}'\n"
    f"{T3_EDIFACT_FOOTER_RAW}\n"
)

EXAMPLE_TYPE_3_RAW_WITH_CRLF = (
    f"{T3_EDIFACT_HEADER_RAW}'\r\n"
    f"{T3_LAB_RESULT_1}'\r\n"
    f"{T3_LAB_RESULT_2}'\r\n"
    f"{T3_EDIFACT_FOOTER_RAW}\r\n"
)

EDIFACT_HEADER_RAW = {
    'una_header': EDIFACT_UNA_HEADER_RAW,
    'unb_header': ''
}
