import logging
from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    SPLIT0001 = (logging.INFO, 'Message received from queue')
    SPLIT0002 = (logging.INFO, 'Messages Successfully parsed from queue')
    SPLIT0003 = (logging.INFO, 'File read from S3 sucessfully')
    SPLIT0004 = (logging.INFO, 'Edifact successfully split into results')
    SPLIT0005 = (logging.INFO, 'Results successfully written to DyamoDB table')
    SPLIT0006 = (logging.INFO, 'Results successfully published to SQS queue')
    SPLIT0007 = (logging.INFO, 'File moved to completed bucket successfully')
    SPLIT0014 = (logging.INFO, 'Moving file to failed bucket')
    SPLIT0015 = (logging.INFO, 'File moved to failed bucket successfully')
    SPLIT0016 = (logging.INFO, 'Generating receipt report')
    SPLIT0017 = (logging.INFO, 'Receipt report generated successfully')
    SPLIT0018 = (logging.INFO, 'Uploading receipt report')
    SPLIT0019 = (logging.INFO, 'Receipt report uploaded successfully')
    SPLIT0020 = (logging.INFO, 'Queueing receipt report for transmission to MESH')
    SPLIT0021 = (logging.INFO, 'Receipt report queued successfully for transmission to MESH')
    SPLIT0023 = (logging.INFO, 'Edifact headers')

    SPLIT0010 = (logging.ERROR, 'Failed to split Edifact file')
    SPLIT0011 = (logging.ERROR, 'Failed to write to DynamoDB table')
    SPLIT0012 = (logging.ERROR, 'Failed to publish Message to SQS queue')
    SPLIT0013 = (logging.ERROR, 'Failed to move file from S3 Bucket')
    SPLIT0022 = (logging.ERROR, 'Failed to create and queue receipt report')
