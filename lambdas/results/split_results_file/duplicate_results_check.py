import hashlib
from datetime import datetime

from common.utils.edifact_utils import (
    tokenize_edifact,
    decode_lab_result,
    validate_edifact_postcode
)


FIELDS_TO_MATCH = [
    'first_name',
    'last_name',
    'date_of_birth',
    'nhs_number',
    'sanitised_postcode'
]

FIELDS_TO_EXTRACT = FIELDS_TO_MATCH + [
    'slide_number',
    'test_date',
    'test_date_format'
]


def reject_results_with_duplicate_demographics(results, una_header):
    """Reject results from same file with same demographics but different slide numbers.
       Where demographics match, and slide numbers are different, the newest result
       is accepted and all others are rejected.
       Newest is based on test date.
       Results with the same slide numbers are left unaffected here, as they are dealt
       with elsewhere.
    """
    decoded_items = _get_decoded_items_with_hashed_demographics(results, una_header)
    hash_dict = {}
    for index, decoded_item in enumerate(decoded_items):
        demo_hash = decoded_item.get('demographics_hash')
        if demo_hash:
            hash_dict[demo_hash] = hash_dict.get(demo_hash, []) + [index]
    for demo_hash, indices in hash_dict.items():
        if len(indices) > 1:
            uniques = _filter_to_get_items_with_unique_slide_numbers(indices, decoded_items)
            rejects = _filter_to_get_all_but_newest_item_by_test_date(uniques, decoded_items)
            for index in rejects:
                results[index]['workflow_state'] = 'rejected'
                results[index]['rejected_reason'] = 'duplicate demographics'
    return results


def _get_decoded_items_with_hashed_demographics(results, una_header):
    decoded_items = []
    for result in results:
        split_raw = tokenize_edifact(una_header + result['raw'])
        decoded_result = decode_lab_result(split_raw)
        validation_errors = []
        validate_edifact_postcode(decoded_result, validation_errors)
        field_dict = _get_field_dict(decoded_result)
        _add_demographics_hash_if_all_fields_present(field_dict)
        decoded_items.append(field_dict)
    return decoded_items


def _get_field_dict(decoded_result):
    field_dict = {}
    for field in FIELDS_TO_EXTRACT:
        field_dict[field] = decoded_result.get(field)
    return field_dict


def _add_demographics_hash_if_all_fields_present(field_dict):
    if all(field_dict.values()):
        fields = ''.join([field_dict[field] for field in FIELDS_TO_MATCH])
        field_dict['demographics_hash'] = hashlib.md5(fields.encode('utf-8')).hexdigest()


def _filter_to_get_items_with_unique_slide_numbers(indices, decoded_items):
    slide_dict = {}
    for index in indices:
        # slide_number field is known to be present at this point
        slide_number = decoded_items[index]['slide_number']
        slide_dict[slide_number] = slide_dict.get(slide_number, []) + [index]
    results = []
    for _, val in slide_dict.items():
        if len(val) == 1:  # only 1 record with this slide number
            results.append(val[0])
    return results


def _filter_to_get_all_but_newest_item_by_test_date(indices, decoded_items):
    results = indices.copy()
    if (len(indices)):
        test_dates = [
            # test_date and test_date_format fields are known to be present at this point
            datetime.strptime(decoded_items[index]['test_date'], decoded_items[index]['test_date_format'])
            for index in indices
        ]
        del results[test_dates.index(max(test_dates))]
    return results
