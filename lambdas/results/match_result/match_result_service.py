from datetime import datetime, timezone
from common.utils.result_utils import update_result_record
from common.log import log
from common.gender_codes import GenderCode
from match_result.log_references import LogReference

from common.models.result import ResultWorkflowState, ResultSecondaryStatus, ResultRejectionReason


def update_result(record, matched_participant):
    key = {
        'result_id': record['result_id'],
        'received_time': record['received_time']
    }
    attributes_to_update = {
        'matched_time': datetime.now(timezone.utc).isoformat(),
        'matched_to_participant': matched_participant['participant']['participant_id']
    }
    update_result_record(key, attributes_to_update)


def mark_result_for_manual_matching(record, participant_ids):
    key = {
        'result_id': record['result_id'],
        'received_time': record['received_time']
    }
    gender = record['decoded'].get('sex')
    if gender and gender == GenderCode.MALE.value:
        log({'log_reference': LogReference.MATCHR0008, 'reason': ResultRejectionReason.MALE_NOT_IN_COHORT})
        attributes_to_update = {
            'workflow_state': ResultWorkflowState.REJECTED,
            'rejected_reason': ResultRejectionReason.MALE_NOT_IN_COHORT,
            'possible_match_participants': participant_ids
        }
    else:
        attributes_to_update = {
            'workflow_state': ResultWorkflowState.PROCESS,
            'secondary_status': ResultSecondaryStatus.NOT_STARTED,
            'possible_match_participants': participant_ids
        }
    update_result_record(key, attributes_to_update)


def single_match_found(response):
    return response is not None and response.get('participant')


def get_participants_for_manual_matching(match_response):
    participants = []
    if match_response:
        participants = match_response.get(('manual_match_participants'), [])
    return participants


def convert_result(result):
    # Matching only requires the decoded part of the result record.
    # address is a string in decoded result but a dictionary is required to set
    # search indices for matching. Use sanitised_postcode in decoded result to
    # create an address dictionary with just the postcode.
    decoded_result = result['decoded']
    decoded_result['address'] = {'postcode': decoded_result.get('sanitised_postcode', '')}
    return decoded_result


def validate_routine_status():
    # TODO implement CYABC validation logic as part of sp-1558
    pass
