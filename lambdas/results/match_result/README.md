# Match Result

- [Match Result](#match-result)
  - [Process](#process)
  - [Architecture](#architecture)

## Process

>Previous step is [decode_edifact_result](../decode_edifact_result/README.md)

>Next step is [post_match_validate_result](../post_match_validate_result/README.md)

This lambda is triggered by messages placed on the results_to_match SQS queue.

It retrieves the result record and checks that it has not been matched already.

If it successfully matches a record to a participant, it will update the results dynamodb table with the following items:
    ["matched_time"] -> utc.now()
    ["match_method"] -> the matching strategy used
    ["matched_to_participant] -> the participant used to match the record with

If it fails, an exception is raised to ensure the message is retried. If it fails three times it goes to the dead letter queue.

If matching succeeds then a message is placed on the results_to_post_match_validate SQS queue.

## Architecture

See the technical overview for more information: https://nhsd-confluence.digital.nhs.uk/display/CSP/Results+-+Technical+Overview
