import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    MATCHR0001 = (logging.INFO, 'Received request to match a result')
    MATCHR0002 = (logging.INFO, 'Converting result to begin match')
    MATCHR0003 = (logging.INFO, 'Attempting to match converted result to a participant')
    MATCHR0004 = (logging.INFO, 'Match for participant found')
    MATCHR0005 = (logging.INFO, 'Updating participant record')
    MATCHR0006 = (logging.INFO, 'Updating result record')
    MATCHR0007 = (logging.INFO, 'Manual matching required')
    MATCHR0008 = (logging.INFO, 'Result rejected')

    MATCHRERR0001 = (logging.ERROR, 'Unable to find a result record')
    MATCHRERR0002 = (logging.INFO, 'Result has already been matched')
