import json
import os

from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.match_result_utils import (
    get_result_record, result_matched_already)
from common.utils.participant_match_utils import attempt_to_match
from common.utils.sqs_utils import send_new_message
from common.models.participant import ParticipantStatus
from match_result.log_references import LogReference
from match_result.match_result_service import (
    convert_result, get_participants_for_manual_matching,
    mark_result_for_manual_matching, single_match_found, update_result,
    validate_routine_status)


def sqs_post_match_validate_queue():
    return os.environ.get('SQS_QUEUE_URL')


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.MATCHR0001})

    message = event['Records'][0]
    message_information = json.loads(message['body'])

    result_id = message_information['result_id']
    received_time = message_information['received_time']
    internal_id = message_information['internal_id']

    result_record = get_result_record(result_id, received_time)

    if not result_record:
        log({'log_reference': LogReference.MATCHRERR0001,
             'result_id': result_id,
             'received_time': received_time})
        raise Exception('Result not found')

    if result_matched_already(result_record):
        log({'log_reference': LogReference.MATCHRERR0002})
        return

    log({'log_reference': LogReference.MATCHR0002})
    converted_result = convert_result(result_record)

    log({'log_reference': LogReference.MATCHR0003})
    match_response = attempt_to_match(converted_result)

    if not single_match_found(match_response):
        log({'log_reference': LogReference.MATCHR0007})
        participants = get_participants_for_manual_matching(match_response)

        mark_result_for_manual_matching(
            result_record,
            participants
        )
        return

    log({'log_reference': LogReference.MATCHR0004})

    log({'log_reference': LogReference.MATCHR0005})

    decoded_result = result_record['decoded']
    recall_type = decoded_result.get('status')

    if recall_type == ParticipantStatus.ROUTINE:
        validate_routine_status()

    log({'log_reference': LogReference.MATCHR0006})
    update_result(result_record, match_response)

    send_new_message(sqs_post_match_validate_queue(), {
        'result_id': result_id,
        'received_time': received_time,
        'internal_id': internal_id,
        'letter_address': match_response['participant']['letter_address']
    })
