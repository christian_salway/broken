import json
import os
from unittest.case import TestCase

from common.test_mocks.mock_events import sqs_mock_event
from match_result.log_references import LogReference
from mock import Mock, call, patch

with patch('boto3.resource') as resource_mock, patch('boto3.client') as client_mock:
    from match_result.match_result import lambda_handler


mock_sqs_event = sqs_mock_event({
    "internal_id": "QWERTY123",
    "result_id": "540407ef-2578-4cf2-82c8-0ab735330267",
    "received_time": "2020-01030T09:52:12.345"
})

sqs_post_match_validate_queue_mock = Mock()
sqs_post_match_validate_queue_mock.return_value = 'post_match_validate_queue_url'

path = 'match_result.match_result'


@patch(f'{path}.sqs_post_match_validate_queue', sqs_post_match_validate_queue_mock)
@patch(f'{path}.mark_result_for_manual_matching')
@patch(f'{path}.log')
@patch(f'{path}.get_result_record')
@patch(f'{path}.attempt_to_match')
@patch(f'{path}.update_result')
@patch(f'{path}.send_new_message')
class TestMatchResult(TestCase):

    def test_exception_raised_when_unable_to_find_result_record(
            self, sqs_mock, update_result_mock, attempt_to_match_mock,
            get_result_record_mock, log_mock, manual_matching_mock):

        get_result_record_mock.return_value = None

        with self.assertRaises(Exception):
            lambda_handler.__wrapped__(mock_sqs_event, {})

        get_result_record_mock.assert_called()
        attempt_to_match_mock.assert_not_called()
        update_result_mock.assert_not_called()
        sqs_mock.assert_not_called()

    def test_lambda_exits_when_result_already_matched(
            self, sqs_mock, update_result_mock, attempt_to_match_mock,
            get_result_record_mock, log_mock, manual_matching_mock):
        result_record = {
            'matched_time': 'something'
        }

        get_result_record_mock.return_value = result_record

        response = lambda_handler.__wrapped__(mock_sqs_event, {})

        self.assertEqual(None, response)

        attempt_to_match_mock.assert_not_called()
        update_result_mock.assert_not_called()
        sqs_mock.assert_not_called()

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MATCHR0001}),
            call({'log_reference': LogReference.MATCHRERR0002})
        ])

    def test_result_marked_for_manual_matching_when_unable_to_match_to_participant(
            self, sqs_mock, update_result_mock, attempt_to_match_mock,
            get_result_record_mock, log_mock, manual_matching_mock):
        result_record = json.loads(load_result_record('example_record_1.json'))

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        get_result_record_mock.return_value = result_record
        attempt_to_match_mock.return_value = None

        lambda_handler.__wrapped__(mock_sqs_event, {})

        get_result_record_mock.assert_called_with(result_id, received_time)
        attempt_to_match_mock.assert_called()
        manual_matching_mock.assert_called_once()
        update_result_mock.assert_not_called()
        sqs_mock.assert_not_called()

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MATCHR0001}),
            call({'log_reference': LogReference.MATCHR0002}),
            call({'log_reference': LogReference.MATCHR0003}),
            call({'log_reference': LogReference.MATCHR0007})
        ])

    def test_result_marked_for_manual_matching_when_matched_multiple_participants(
            self, sqs_mock, update_result_mock, attempt_to_match_mock,
            get_result_record_mock, log_mock, manual_matching_mock):
        result_record = json.loads(load_result_record('example_record_1.json'))

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        get_result_record_mock.return_value = result_record

        sample_participants = [
            {
                'participant_id': 'matched_on_me'
            }
        ]
        attempt_to_match_mock.return_value = {
            'match_strategy': 'manual_match_required',
            'manual_match_participants': sample_participants
        }

        lambda_handler.__wrapped__(mock_sqs_event, {})

        get_result_record_mock.assert_called_with(result_id, received_time)
        update_result_mock.assert_not_called()
        manual_matching_mock.assert_called_once()
        sqs_mock.assert_not_called()

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MATCHR0001}),
            call({'log_reference': LogReference.MATCHR0002}),
            call({'log_reference': LogReference.MATCHR0003}),
            call({'log_reference': LogReference.MATCHR0007})
        ])

    def test_exception_raised_when_unable_to_update_result(
            self, sqs_mock, update_result_mock, attempt_to_match_mock,
            get_result_record_mock, log_mock, manual_matching_mock):

        result_record = load_result_record('example_record_1.json')

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        matched_participant = mock_matched_participant()

        get_result_record_mock.return_value = json.loads(result_record)
        attempt_to_match_mock.return_value = matched_participant
        update_result_mock.side_effect = Mock(side_effect=Exception)

        with self.assertRaises(Exception):
            lambda_handler.__wrapped__(mock_sqs_event, {})

        get_result_record_mock.assert_called_with(result_id, received_time)
        attempt_to_match_mock.assert_called()
        update_result_mock.assert_called()
        sqs_mock.assert_not_called()

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MATCHR0001}),
            call({'log_reference': LogReference.MATCHR0002}),
            call({'log_reference': LogReference.MATCHR0003}),
            call({'log_reference': LogReference.MATCHR0004}),
            call({'log_reference': LogReference.MATCHR0005}),
            call({'log_reference': LogReference.MATCHR0006})
        ])

    def test_match_result_completes_successfully_with_address(
            self, sqs_mock, update_result_mock, attempt_to_match_mock,
            get_result_record_mock, log_mock, manual_matching_mock):

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"
        result_record = json.loads(load_result_record('example_record_1.json'))

        matched_participant = mock_matched_participant()
        get_result_record_mock.return_value = result_record
        attempt_to_match_mock.return_value = matched_participant

        lambda_handler.__wrapped__(mock_sqs_event, {})

        get_result_record_mock.assert_called_with(result_id, received_time)
        attempt_to_match_mock.assert_called()
        update_result_mock.assert_called_with(result_record, matched_participant)

        sqs_mock.assert_called_with(
            'post_match_validate_queue_url', {
                'internal_id': 'QWERTY123',
                'result_id': '540407ef-2578-4cf2-82c8-0ab735330267',
                'received_time': '2020-01030T09:52:12.345',
                'letter_address': 'letter_address'
            }
        )

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MATCHR0001}),
            call({'log_reference': LogReference.MATCHR0002}),
            call({'log_reference': LogReference.MATCHR0003}),
            call({'log_reference': LogReference.MATCHR0004}),
            call({'log_reference': LogReference.MATCHR0005}),
            call({'log_reference': LogReference.MATCHR0006})
        ])

    def test_match_result_completes_successfully_without_address(
            self, sqs_mock, update_result_mock, attempt_to_match_mock,
            get_result_record_mock, log_mock, manual_matching_mock):

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"
        result_record = json.loads(load_result_record('example_record_2.json'))

        matched_participant = mock_matched_participant()
        get_result_record_mock.return_value = result_record
        attempt_to_match_mock.return_value = matched_participant

        lambda_handler.__wrapped__(mock_sqs_event, {})

        get_result_record_mock.assert_called_with(result_id, received_time)
        attempt_to_match_mock.assert_called()
        update_result_mock.assert_called_with(result_record, matched_participant)

        sqs_mock.assert_called_with(
            'post_match_validate_queue_url', {
                'internal_id': 'QWERTY123',
                'result_id': '540407ef-2578-4cf2-82c8-0ab735330267',
                'received_time': '2020-01030T09:52:12.345',
                'letter_address': 'letter_address'
            }
        )

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.MATCHR0001}),
            call({'log_reference': LogReference.MATCHR0002}),
            call({'log_reference': LogReference.MATCHR0003}),
            call({'log_reference': LogReference.MATCHR0004}),
            call({'log_reference': LogReference.MATCHR0005}),
            call({'log_reference': LogReference.MATCHR0006})
        ])


def load_result_record(name):
    current_path = os.path.dirname(os.path.realpath(__file__))
    full_file_path = os.path.join(current_path, 'test_data', name)
    return open(full_file_path).read()


def mock_matched_participant():
    return {
        'match_strategy': 0,
        'participant': {
            'participant_id': '0',
            'letter_address': 'letter_address'
        }
    }
