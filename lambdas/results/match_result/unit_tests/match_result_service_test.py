import json
import os
from datetime import datetime, timezone
from unittest.case import TestCase
from mock import ANY, patch

from common.models.result import ResultRejectionReason


with patch('boto3.resource') as resource_mock:
    from match_result.match_result_service import (
        convert_result, mark_result_for_manual_matching, update_result)


class TestMatchServiceResult(TestCase):

    FIXED_NOW_TIME = datetime(2020, 4, 20, 10, 0, 0, tzinfo=timezone.utc)

    @patch('match_result.match_result_service.update_result_record')
    def test_update_result(self, update_result_record_mock):
        current_path = os.path.dirname(os.path.realpath(__file__))
        file_name = 'example_record_1.json'
        full_file_path = os.path.join(current_path, 'test_data', file_name)
        record = json.loads(open(full_file_path).read())

        matched_participant = {
            'match_strategy': 0,
            'participant': {
                'participant_id': '37f4524d-e59f-41d3-9071-97eec87955c7'
            }
        }
        key = {
            'result_id': record['result_id'],
            'received_time': record['received_time']
        }
        attributes_to_update = {
            'matched_time': ANY,
            'matched_to_participant': matched_participant['participant']['participant_id']
        }

        update_result(record, matched_participant)

        update_result_record_mock.assert_called_with(key, attributes_to_update)

    @patch('match_result.match_result_service.update_result_record')
    @patch('match_result.match_result_service.log')
    def test_mark_result_for_manual_match(self, log_mock, update_result_record_mock):
        current_path = os.path.dirname(os.path.realpath(__file__))
        file_name = 'example_record_1.json'
        full_file_path = os.path.join(current_path, 'test_data', file_name)
        record = json.loads(open(full_file_path).read())

        sample_participants = [
            {
                'participant_id': 1
            },
            {
                'participant_id': 2
            }
        ]

        key = {
            'result_id': record['result_id'],
            'received_time': record['received_time']
        }
        attributes_to_update = {
            'workflow_state': 'process',
            'secondary_status': 'NOT STARTED',
            'possible_match_participants': sample_participants
        }

        mark_result_for_manual_matching(record, sample_participants)

        update_result_record_mock.assert_called_with(key, attributes_to_update)

    @patch('match_result.match_result_service.update_result_record')
    @patch('match_result.match_result_service.log')
    def test_mark_result_for_manual_match_should_reject_result_for_male_not_in_cohort(self,
                                                                                      log_mock,
                                                                                      update_result_record_mock):
        current_path = os.path.dirname(os.path.realpath(__file__))
        file_name = 'example_record_3.json'
        full_file_path = os.path.join(current_path, 'test_data', file_name)
        record = json.loads(open(full_file_path).read())

        sample_participants = [
            {
                'participant_id': 1
            }
        ]

        key = {
            'result_id': record['result_id'],
            'received_time': record['received_time']
        }
        attributes_to_update = {
            'workflow_state': 'rejected',
            'rejected_reason': ResultRejectionReason.MALE_NOT_IN_COHORT,
            'possible_match_participants': sample_participants
        }

        mark_result_for_manual_matching(record, sample_participants)

        update_result_record_mock.assert_called_with(key, attributes_to_update)

    def test_convert_result(self):
        incoming_result = {
            'result_id': '1234',
            'decoded': {
                'address': 'biglongstringofstuff',
                'sanitised_postcode': 'LS3 4AA'
            }
        }

        converted_result = convert_result(incoming_result)

        self.assertEqual(converted_result,
                         {'address': {'postcode': 'LS3 4AA'},
                          'sanitised_postcode': 'LS3 4AA'
                          })
