# Post Match Validate


- [Post Match Validate](#post-match-validate)
  - [Process](#process)
  - [Architecture](#architecture)

>Previous step is [match_result](../match_result/README.md)

>Next step is [None](./) post match validation is the final step in the auto match process

## Process

This lambda is triggered by messages placed on the `results_to_post_match_validate` **SQS queue**.

It retrieves the result record before validating it against a number of criteria.

If the result is rejected for whatever reason then the result record is set to rejected and processing on it goes no further.

It will update the participants dynamodb table with the following items:
    ["next_test_due_date"] ->  next_test_due_date + next_test_due_date

It will update any episode record with the following information:
    ["state"]
    ["result_date"]
    ["result_id"]

Finally, it will create a new RESULT record in the Participants table:
    ["participant_id"]  -> Participants id
    ["sort_key"]        -> RESULT# { Test date from edifact record and date of creation }
    ["nhs_number"]      -> NHS number found during the match
    ["created"]         -> current datetime stamp
    ["result_date"]     -> Test completion date from edifact record

If it fails, an exception is raised to ensure the message is retried. If it fails three times it goes to the dead letter queue

## Architecture

See the technical overview for more information: https://nhsd-confluence.digital.nhs.uk/display/CSP/Results+-+Technical+Overview
