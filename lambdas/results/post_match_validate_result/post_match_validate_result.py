import json

from common.log import log
from common.models.participant import CeaseReason
from common.utils.audit_utils import (
    AuditActions,
    AuditUsers,
    audit
)
from common.utils.cease_utils import (
    cease_participant_and_send_notification_messages,
    participant_eligible_for_auto_cease
)
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.match_result_utils import (
    get_result_record, validate_matched_result,
    calculate_and_update_ntdd, reject_result_and_pause_participant
)
from common.utils.participant_utils import get_participant_by_participant_id
from common.utils.result_letter_utils import (
    check_for_result_letter_suppression,
    add_result_letter_to_processing_queue
)
from post_match_validate_result.log_references import LogReference
from post_match_validate_result.post_match_validate_result_service import (
    add_participant_result_record, update_episode_information)
from common.services.reinstate.reinstate import reinstate_participant_by_participant_id
from common.services.reinstate.reinstate_result import ReinstateResult

RESULT_FIELDS_TO_AUDIT = [
    'test_date', 'slide_number', 'action', 'action_code', 'result', 'result_code',
    'infection_result', 'infection_code', 'source_code', 'sender_code', 'sender_source_type'
]


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.PSTMTCH0001})

    message = event['Records'][0]
    message_information = json.loads(message['body'])
    result_id = message_information['result_id']
    received_time = message_information['received_time']

    result_record = get_result_record(result_id, received_time)

    if not result_record:
        log({
            'log_reference': LogReference.PSTMTCHERR0001,
            'result_id': result_id,
            'received_time': received_time
        })
        raise Exception('Result not found')

    log({'log_reference': LogReference.PSTMTCH0004})

    decoded_result = result_record['decoded']
    participant_id = result_record['matched_to_participant']

    rejected_reason = validate_matched_result(decoded_result, participant_id)

    if rejected_reason:
        log({'log_reference': LogReference.PSTMTCH0005, 'reason': rejected_reason})
        reject_result_and_pause_participant(rejected_reason, participant_id, result_record)
        return

    participant = get_participant_by_participant_id(participant_id)

    if participant is None:
        log({
            'log_reference': LogReference.PSTMTCHERR0003,
            'participant_id': participant_id
        })
        raise Exception('Participant not found')

    participant['letter_address'] = message_information['letter_address']

    reinstate_response = reinstate_participant_by_participant_id(participant_id, AuditUsers.MATCH_RESULT.value)
    participant_has_been_reinstated = reinstate_response.result == ReinstateResult.REINSTATED
    if participant_has_been_reinstated:
        audit(
            action=AuditActions.REINSTATE_PARTICIPANT,
            user=AuditUsers.MATCH_RESULT,
            participant_ids=[participant_id],
            additional_information={
                'updating_next_test_due_date_to': reinstate_response.to_data()['next_test_due_date']
            }
        )

    next_test_due_date = None
    recall_type = decoded_result['status']
    if not participant_has_been_reinstated:
        next_test_due_date = calculate_and_update_ntdd(decoded_result, participant, recall_type)

    log({'log_reference': LogReference.PSTMTCH0003})

    participant_result_record = add_participant_result_record(decoded_result, result_id, participant)

    # if next test due date is none participant was reinstated or result should not affect next test due date
    can_auto_cease = participant_eligible_for_auto_cease(participant, next_test_due_date) \
        if next_test_due_date else False
    # If Autoceasing don't change episode status to completed as cease_participant
    # changes it from live to ceased.
    log({'log_reference': LogReference.PSTMTCH0002})
    update_episode_information(
        result_id,
        received_time,
        participant_id,
        can_auto_cease
    )

    if can_auto_cease:
        cease_participant_and_send_notification_messages(
            participant, AuditUsers.SYSTEM.value, {'reason': CeaseReason.AUTOCEASE_DUE_TO_AGE}, send_letter=False
        )
        audit(action=AuditActions.CEASE_EPISODE, user=AuditUsers.SYSTEM, participant_ids=[participant_id],
              additional_information={'updating_next_test_due_date_from': participant['next_test_due_date']})

    if not check_for_result_letter_suppression(participant_result_record):
        add_result_letter_to_processing_queue(participant_result_record, can_auto_cease)

    additional_information = {k: v for k, v in participant_result_record.items() if k in RESULT_FIELDS_TO_AUDIT}
    try:
        additional_information.update({'lab_address': decoded_result['address']})
    except KeyError:
        additional_information.update({'lab_address': ''})
    # only audit next test due date change if it changed and was not already audited by actions above
    if next_test_due_date and not can_auto_cease:
        additional_information.update({'updating_next_test_due_date_from': participant['next_test_due_date'],
                                       'updating_next_test_due_date_to': next_test_due_date.isoformat()})

    action = participant_result_record['action']

    if action == 'Repeat advised':
        additional_information.update({'recall_months': decoded_result['recall_months']})

    audit(
        action=AuditActions.RESULT_MATCHED,
        user=AuditUsers.MATCH_RESULT,
        participant_ids=[participant_id],
        additional_information=additional_information
    )
