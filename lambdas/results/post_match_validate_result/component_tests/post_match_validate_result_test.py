from unittest import TestCase
from mock import patch, Mock, call
from common.test_mocks.mock_events import sqs_mock_event
from boto3.dynamodb.conditions import Key, Attr
import json
from datetime import datetime, timezone, date
from common.utils.dynamodb_access.table_names import TableNames

from post_match_validate_result import post_match_validate_result

mock_env_vars = {
    'DYNAMODB_PARTICIPANTS': 'DYNAMODB_PARTICIPANTS',
    'DYNAMODB_RESULTS': 'DYNAMODB_RESULTS',
    'AUDIT_QUEUE_URL': 'MOCK_AUDIT_QUEUE_URL',
    'NOTIFICATION_QUEUE_URL': 'MOCK_NOTIFICATION_QUEUE_URL',
    'RESULT_LETTER_CUT_OFF_MONTHS': '3',
}

results_table_mock = Mock()
participants_table_mock = Mock()
sqs_audit_client_mock = Mock()
sqs_letter_processing_mock = Mock()

MODULE = 'post_match_validate_result.post_match_validate_result'
MODULE_UTILS = 'common.utils'

example_datetime = datetime(2020, 10, 18, 13, 48, 8, tzinfo=timezone.utc)
example_date = date(2020, 10, 18)
datetime_mock = Mock(wraps=datetime)
date_mock = Mock(wraps=date)
datetime_mock.now.return_value = example_datetime
date_mock.today.return_value = example_date


def get_table_side_effect(table_enum):
    tables = {
        TableNames.RESULTS: results_table_mock,
        TableNames.PARTICIPANTS: participants_table_mock
    }
    return tables[table_enum]


@patch('common.utils.audit_utils.datetime', datetime_mock)
@patch('common.utils.cease_utils.datetime', datetime_mock)
@patch('common.utils.result_letter_utils.date', date_mock)
@patch('post_match_validate_result.post_match_validate_result_service.datetime', datetime_mock)
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(side_effect=get_table_side_effect))
@patch(f'{MODULE_UTILS}.audit_utils.get_sqs_client', Mock(return_value=sqs_audit_client_mock))
@patch(f'{MODULE_UTILS}.sqs_utils.get_sqs_client', Mock(return_value=sqs_letter_processing_mock))
class TestPostMatchValidateResult(TestCase):

    mock_participant_record = {
        'participant_id': '123456',
        'sort_key': 'PARTICIPANT',
        'nhs_number': '9999999999',
        'next_test_due_date': '2021-01-01',
        'date_of_birth': '1990-01-01',
    }

    mock_result_record = {
        'result_id': '23456',
        'received_time': '2021-05-06',
        'matched_to_participant': '123456',
        'decoded': {
            'action': 'Some action',
            'date_of_birth': '1980-01-01',
            'action_code': 'R',
            'address': 'AAAAAAAAA A AAAA AAAAA AAAA M2 6LW',
            'hpv_primary': True,
            'infection_code': '0',
            'infection_result': 'HPV positive',
            'recall_months': '2',
            'result': 'A result',
            'result_code': 'X',
            'slide_number': '4949',
            'source_code': 'G',
            'sender_source_type': '1',
            'self_sample': False,
            'sender_code': '555',
            'status': 'status',
            'test_date': '2020-09-03',
        }
    }

    mock_participant_and_results_for_reinstate = [
        {
            'participant_id': '123456',
            'sort_key': 'PARTICIPANT',
            'is_ceased': True,
            'date_of_birth': '1992-01-01'
        },
        {
            'participant_id': '123456',
            'action_code': 'S',
            'infection_code': '0',
            'result_code': 'X',
            'sort_key': 'RESULT#',
            'test_date': '2020-02-03'
        },
        {
            'participant_id': '123456',
            'sort_key': 'CEASE#',
            'reason': 'Due to age',
            'date_from': '2020-01-01'
        }
    ]

    mock_live_episodes = [{
        'sort_key': 'EPISODE#'
    }]

    def setUp(self):
        self.log_patcher = patch('common.log.logging')
        self.env_patcher = patch('os.environ', mock_env_vars)
        participants_table_mock.reset_mock()
        participants_table_mock.get_item = Mock(side_effect=[
            {'Item': self.mock_participant_record},
            {'Item': self.mock_participant_record},
            {'Item': self.mock_participant_record},
            {'Item': self.mock_participant_record}
        ])
        participants_table_mock.query = Mock(side_effect=[
            {
                'Items': [{
                    'test_date': '2021-10-10',
                    'sort_key': 'RESULT#whenever'
                }]
            },
            {'Items': None},
            {
                'Items': self.mock_participant_and_results_for_reinstate
            },
            {
                'Items': self.mock_live_episodes
            }
        ])
        results_table_mock.reset_mock()
        results_table_mock.get_item.return_value = {'Item': self.mock_result_record}
        self.log_patcher.start()
        self.env_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()
        self.env_patcher.stop()

    def test_post_match_validate_result_happy_path(self):
        """
        Assert that given a matched result
        When
          - Is validate by post match validation
        It
          - Updates the NTDD
          - Completes all other live episodes
          - Sends result letter to participant
        """
        # Arrange
        context = Mock()
        context.function_name = ''
        event = {
            'result_id': '1',
            'received_time': '2021-05-04',
            'letter_address': {
                'address_line_1': 'Some Street'
            },
            'internal_id': 'mock-internal-id'
        }
        participant_and_results_projection = 'nhs_number, sort_key, participant_id, title, first_name, middle_names, last_name, gender, address, date_of_birth, date_of_death, is_ceased, is_invalid_patient, next_test_due_date, registered_gp_practice_code, infection_result, infection_code, #result, #action, action_code, test_date, recall_months, result_code, sender_code, source_code, sender_source_type, slide_number, sending_lab, #status, suppression_reason, invited_date, reason, date_from, is_fp69, event_text, health_authority, crm, active, reason_for_removal_code, reason_for_removal_effective_from_date, hpv_primary, self_sample, practice_code, nhais_cipher, is_paused, is_paused_received_time'  

        # Act
        post_match_validate_result.lambda_handler(sqs_mock_event(event), context)

        # Assert
        participants_table_mock.get_item.assert_has_calls([
            call(Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'})
        ])

        participants_table_mock.query.assert_has_calls([
            call(KeyConditionExpression=Key('participant_id').eq('123456') & Key('sort_key').begins_with('RESULT#')),
            call(
                KeyConditionExpression=Key('participant_id').eq('123456'),
                ProjectionExpression=participant_and_results_projection,
                ExpressionAttributeNames={'#result': 'result', '#action': 'action', '#status': 'status'},
                ScanIndexForward=False),
            call(
                KeyConditionExpression=Key('participant_id').eq('123456'),
                ProjectionExpression=participant_and_results_projection,
                ExpressionAttributeNames={'#result': 'result', '#action': 'action', '#status': 'status'},
                ScanIndexForward=False),
            call(
                KeyConditionExpression=Key('participant_id').eq('123456') & Key('sort_key').begins_with('EPISODE#'),
                FilterExpression=Attr('live_record_status').exists(),
                ProjectionExpression='#live_record_status, #HMR101_form_data, #participant_id, #sort_key, #status, #user_id, #test_date, #test_due_date', 
                ExpressionAttributeNames={
                    '#live_record_status': 'live_record_status', '#HMR101_form_data': 'HMR101_form_data',
                    '#participant_id': 'participant_id', '#sort_key': 'sort_key', '#status': 'status',
                    '#user_id': 'user_id', '#test_date': 'test_date', '#test_due_date': 'test_due_date'
                })
        ])

        results_table_mock.get_item.assert_has_calls([
            call(Key={'result_id': '1', 'received_time': '2021-05-04'})
        ])

        participants_table_mock.update_item.assert_has_calls([
            # Set next_test_due_date in calculate_and_update_ntdd
            call(
                Key={'participant_id': '123456', 'sort_key': 'PARTICIPANT'},
                UpdateExpression='SET #next_test_due_date = :next_test_due_date, #status = :status',
                ExpressionAttributeValues={':next_test_due_date': '2020-11-03', ':status': 'status'},
                ExpressionAttributeNames={'#next_test_due_date': 'next_test_due_date', '#status': 'status'},
                ReturnValues='NONE'
            ),
            # Set all live_record_status.exist() to COMPLETED when match succeeds
            call(
                Key={'participant_id': '123456', 'sort_key': 'EPISODE#'},
                UpdateExpression='SET #result_date = :result_date, #result_id = :result_id, #status = :status REMOVE #live_record_status', 
                ExpressionAttributeValues={':result_date': '2021-05-04', ':result_id': '1', ':status': 'COMPLETED'},
                ExpressionAttributeNames={'#result_date': 'result_date', '#result_id': 'result_id', '#status': 'status', '#live_record_status': 'live_record_status'}, 
                ReturnValues='NONE'
            )
        ])

        # SQS Audit client calls
        sqs_audit_client_mock.send_message.assert_has_calls([
            call(
                QueueUrl='MOCK_AUDIT_QUEUE_URL',
                MessageBody=json.dumps({
                    "action": "RESULT_MATCHED",
                    "timestamp": "2020-10-18T13:48:08+00:00",
                    "internal_id": "mock-internal-id",
                    "nhsid_useruid": "MATCH_RESULT",
                    "session_id": "NONE",
                    "participant_ids": ["123456"],
                    "additional_information": {
                        "slide_number": "4949",
                        "test_date": "2020-09-03",
                        "action": "Some action",
                        "action_code": "R",
                        "result": "A result",
                        "result_code": "X",
                        "infection_result": "HPV positive",
                        "infection_code": "0",
                        "source_code": "G",
                        "sender_source_type": "1",
                        "sender_code": "555",
                        "lab_address": "AAAAAAAAA A AAAA AAAAA AAAA M2 6LW",
                        "updating_next_test_due_date_from": "2021-01-01",
                        "updating_next_test_due_date_to": "2020-11-03"
                        }
                    })
            ),

        ])

        # SQS Create result letter calls
        sqs_letter_processing_mock.send_message.assert_has_calls([
            call(
                QueueUrl='MOCK_NOTIFICATION_QUEUE_URL',
                MessageBody=json.dumps(
                    {
                        "address": {
                            "address_line_1": "Some Street"
                        },
                        "internal_id": "mock-internal-id",
                        "participant_id": "123456",
                        "sort_key": "RESULT#2020-09-03#2020-10-18T13:48:08+00:00",
                        "template": "PX0R",
                        "type": "Result letter"
                    },
                    indent=4,
                    sort_keys=True,
                    default=str)
                )
        ])
