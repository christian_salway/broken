from unittest import TestCase

from boto3.dynamodb.conditions import Key
from mock import patch, Mock, call

from common.utils.dynamodb_access.table_names import TableNames
from post_match_validate_result.post_match_validate_result import lambda_handler

from common.utils.data_segregation.nhais_ciphers import Cohorts


result_table_mock = Mock()
participants_table_mock = Mock()
dynamodb_resource_mock = Mock()

mock_env_vars = {
    'DYNAMODB_RESULTS': 'MOCK_RESULT_TABLE',
    'DYNAMODB_PARTICIPANTS': 'participants-table'
    # 'DYNAMODB_PARTICIPANTS': 'MOCK_PARTICIPANT_TABLE'
}


def get_table_side_effect(table_enum):
    tables = {
        TableNames.RESULTS: result_table_mock,
        TableNames.PARTICIPANTS: participants_table_mock
    }
    return tables[table_enum]


@patch('common.utils.data_segregation.data_segregation_filters.get_dynamodb_resource',
       Mock(return_value=dynamodb_resource_mock))
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table', Mock(side_effect=get_table_side_effect))
@patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
       Mock(side_effect=get_table_side_effect))
@patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
       Mock(return_value=[Cohorts.ENGLISH]))
@patch('os.environ', mock_env_vars)
class TestPostMatchValidateResultReject(TestCase):

    def setUp(self):
        self.logging_patcher = patch('common.log.logging')
        self.log_patcher = patch('post_match_validate_result.post_match_validate_result.log')
        result_table_mock.reset_mock()
        participants_table_mock.reset_mock()
        self.log_patcher.start()
        self.logging_patcher.start()
        dynamodb_resource_mock.batch_get_item.return_value = {
            'Responses': {
                'participants-table': []
            }
        }

    def tearDown(self):
        self.log_patcher.stop()

    @patch('common.utils.participant_utils.audit')
    def test_post_match_validate_result_rejects_result(self, audit_mock):
        event = {
            'Records': [
                {
                    'body': '{"result_id": "the_result_id", "received_time": "the_received_time"}'
                }
            ]
        }
        context = Mock()
        context.function_name = ''

        result_table_mock.get_item.return_value = {'Item': {
            'decoded': {
                'result_code': 'X',
                'infection_code': 'U',
                'action_code': 'R'
            },
            'result_id': 'the_result_id',
            'received_time': 'the_received_time',
            'matched_to_participant': 'the_matched_participant_id'
        }}

        def _query_mock(*args, **kwargs):
            if kwargs['KeyConditionExpression'] == Key('participant_id').eq('the_matched_participant_id')\
                    & Key('sort_key').begins_with('EPISODE#'):
                return{
                        'Items':
                        [
                            {
                                'result_code': '1',
                                'live_record_status': 'INVITED',
                                'status': 'INVITED',
                                'sort_key': 'RESULT#1',
                                'participant_id': 'the_matched_participant_id'
                            },
                            {
                                'result_code': 'X',
                                'live_record_status': 'INVITED',
                                'status': 'INVITED',
                                'sort_key': 'RESULT#2',
                                'participant_id': 'the_matched_participant_id'
                            }
                        ]
                    }
            if kwargs['KeyConditionExpression'] == Key('participant_id').eq('the_matched_participant_id')\
                    & Key('sort_key').begins_with('RESULT#'):
                return{
                            'Items':
                            [
                                {'result_code': '1', 'infection_code': '9', 'action_code': 'R', 'sort_key': 'RESULT#1',
                                 'participant_id': 'the_matched_participant_id'},
                                {'result_code': 'X', 'infection_code': '0', 'action_code': 'A', 'sort_key': 'RESULT#2',
                                 'participant_id': 'the_matched_participant_id'}
                            ]
                    }
            if kwargs['KeyConditionExpression'] == Key('participant_id').eq('the_matched_participant_id'):
                return{
                            'Items':
                            [
                                {'result_code': '1', 'infection_code': '9', 'action_code': 'R', 'sort_key': 'RESULT#1',
                                 'participant_id': 'the_matched_participant_id'},
                                {'result_code': 'X', 'infection_code': '0', 'action_code': 'A', 'sort_key': 'RESULT#2',
                                 'participant_id': 'the_matched_participant_id'}
                            ]
                    }

        participants_table_mock.query.side_effect = _query_mock

        lambda_handler(event, context)

        # Assert result retrieved from results table using keys from event body
        result_table_mock.get_item.assert_called_once_with(Key={
            'result_id': 'the_result_id',
            'received_time': 'the_received_time'
        })

        # Assert all participant previous results retrieved from participants table
        # Also assert that participant and test results are retrieved from participants table
        participants_table_mock.query.assert_has_calls([
            call(KeyConditionExpression=Key('participant_id').eq(
                'the_matched_participant_id') & Key('sort_key').begins_with('RESULT#')),
            call(KeyConditionExpression=Key('participant_id').eq(
                'the_matched_participant_id'),
                ProjectionExpression='nhs_number, sort_key, participant_id, title, first_name, middle_names,' +
                ' last_name, gender, address, date_of_birth, date_of_death, is_ceased, is_invalid_patient,' +
                ' next_test_due_date, registered_gp_practice_code, infection_result, infection_code,' +
                ' #result, #action, action_code, test_date, recall_months, result_code, sender_code,' +
                ' source_code, sender_source_type, slide_number, sending_lab, #status, suppression_reason,' +
                ' invited_date, reason, date_from, is_fp69, event_text, health_authority, crm, active,' +
                ' reason_for_removal_code, reason_for_removal_effective_from_date, hpv_primary, self_sample,' +
                ' practice_code, nhais_cipher, is_paused, is_paused_received_time',
                ExpressionAttributeNames={'#result': 'result', '#action': 'action', '#status': 'status'},
                ScanIndexForward=False)]
        )
        # Assert result in result table is marked as rejected
        result_table_mock.update_item.assert_called_once_with(
            Key={'result_id': 'the_result_id', 'received_time': 'the_received_time'},
            UpdateExpression='SET #workflow_state = :workflow_state, #rejected_reason = :rejected_reason',
            ExpressionAttributeValues={':workflow_state': 'rejected', ':rejected_reason': '2nd (inad) Colp Res'},
            ExpressionAttributeNames={'#workflow_state': 'workflow_state', '#rejected_reason': 'rejected_reason'},
            ReturnValues='NONE'
        )
