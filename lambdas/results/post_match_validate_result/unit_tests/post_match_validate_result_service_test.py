import json
import os
from datetime import datetime, timezone
from unittest.case import TestCase

from mock import patch, call, Mock

from common.models.participant import EpisodeStatus
from post_match_validate_result.log_references import LogReference
from common.models.participant import ParticipantStatus

from post_match_validate_result.post_match_validate_result_service import (
    update_episode_information,
    add_participant_result_record
)

MODULE = 'post_match_validate_result.post_match_validate_result_service'
MATCH_RESULT_UTILS_MODULE = 'common.utils.match_result_utils'


@patch('os.environ', {'DYNAMODB_PARTICIPANTS': 'participants-table'})
class TestPostMatchValidateService(TestCase):

    FIXED_NOW_TIME = datetime(2020, 4, 20, 10, 0, 0, tzinfo=timezone.utc)

    @patch(f'{MODULE}.log')
    @patch(f'{MODULE}.update_episode_to_completion_state')
    @patch(f'{MODULE}.query_for_live_episode')
    def test_update_episode_information_with_no_live_episodes(
            self, query_episodes_mock, update_episode_to_completion_state_mock, log_mock):
        query_episodes_mock.return_value = []
        result_id = 'the_result_id'
        received_time = 'the_received_time'
        participant_id = 'the_participant_id'

        update_episode_information(result_id, received_time, participant_id, False)

        query_episodes_mock.assert_called_with(participant_id)
        update_episode_to_completion_state_mock.assert_not_called()
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.PSTMTCHERR0002, 'result_id': result_id, 'received_time': received_time,
                  'participant_id': participant_id})]
        )

    @patch(f'{MODULE}.log')
    @patch(f'{MODULE}.update_episode_to_completion_state')
    @patch(f'{MODULE}.query_for_live_episode')
    def test_update_episode_information_with_one_live_episode(
            self, query_episodes_mock, update_episode_to_completion_state_mock, log_mock):
        query_episodes_mock.return_value = [{'sort_key': 'the_sort_key', 'status': EpisodeStatus.INVITED.value}]
        result_id = 'the_result_id'
        received_time = 'the_received_time'
        participant_id = 'the_participant_id'

        update_episode_information(result_id, received_time, participant_id, False)

        query_episodes_mock.assert_called_with(participant_id)
        update_episode_to_completion_state_mock.assert_called_with(participant_id, 'the_sort_key', received_time,
                                                                   result_id, False, 'COMPLETED')
        log_mock.assert_not_called()

    @patch(f'{MODULE}.log')
    @patch(f'{MODULE}.update_episode_to_completion_state')
    @patch(f'{MODULE}.query_for_live_episode')
    def test_update_episode_information_with_two_live_episodes_one_walkin_one_invited(
            self, query_episodes_mock, update_episode_to_completion_state_mock, log_mock):
        query_episodes_mock.return_value = [{'sort_key': 'sort_key_one', 'status': EpisodeStatus.WALKIN.value},
                                            {'sort_key': 'sort_key_two', 'status': EpisodeStatus.INVITED.value}]
        result_id = 'the_result_id'
        received_time = 'the_received_time'
        participant_id = 'the_participant_id'

        update_episode_information(result_id, received_time, participant_id, True)

        query_episodes_mock.assert_called_with(participant_id)

        expected_update_calls = [
            call(participant_id, 'sort_key_one', received_time, result_id, True, EpisodeStatus.COMPLETED.value),
            call(participant_id, 'sort_key_two', received_time, result_id, True, EpisodeStatus.CLOSED.value)
        ]
        update_episode_to_completion_state_mock.assert_has_calls(expected_update_calls)
        log_mock.assert_not_called()

    @patch(f'{MATCH_RESULT_UTILS_MODULE}.log', Mock())
    @patch(f'{MODULE}.create_replace_record_in_participant_table')
    @patch(f'{MODULE}.get_current_iso_datetime')
    def test_add_participant_result_record(self, get_current_iso_datetime_mock,
                                           create_replace_record_in_participant_table_mock):
        # Arrange
        current_path = os.path.dirname(os.path.realpath(__file__))
        file_name = 'example_record_1.json'
        full_file_path = os.path.join(current_path, 'test_data', file_name)
        with open(full_file_path) as file:
            record = json.loads(file.read())

        matched_participant = {
            'match_strategy': 0,
            'participant': {
                'participant_id': '37f4524d-e59f-41d3-9071-97eec87955c7',
                'nhs_number': '87654321',
                'letter_address': 'letter_address'
            }
        }

        decoded_record = record['decoded']
        get_current_iso_datetime_mock.return_value = self.FIXED_NOW_TIME
        created = self.FIXED_NOW_TIME
        result_id = 'the_result_id'

        result_record = {
            'participant_id': matched_participant['participant']['participant_id'],
            'sort_key': f'RESULT#2019-10-21#{created}',
            'result_id': result_id,
            'nhs_number': matched_participant['participant']['nhs_number'],
            'sanitised_nhs_number': matched_participant['participant']['nhs_number'],
            'slide_number': '19035247',
            'created': created,
            'test_date': '2019-10-21',
            'result_date': created,
            'next_test_due_date': '2022-10-21',
            'action': 'Suspend recall',
            'action_code': 'A',
            'result': 'Negative',
            'result_code': 'X',
            'infection_result': 'HPV positive',
            'infection_code': '0',
            'sender_source_type': '7',
            'source_code': 'H',
            'sender_code': 'K81052',
            'self_sample': False,
            'hpv_primary': True,
            'status': ParticipantStatus.SUSPENDED,
            'letter_address': 'letter_address'
        }

        # Act
        actual_result = add_participant_result_record(decoded_record, result_id, matched_participant['participant'])

        # Assert
        create_replace_record_in_participant_table_mock.assert_called_with(result_record)
        self.assertDictEqual(result_record, actual_result)
