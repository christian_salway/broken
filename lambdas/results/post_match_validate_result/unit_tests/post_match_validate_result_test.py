import json
import os
from unittest.case import TestCase
from datetime import date

from mock import ANY, Mock, call, patch
from common.models.participant import CeaseReason
from post_match_validate_result.log_references import LogReference
from common.services.reinstate.reinstate_response import ReinstateResponse
from common.services.reinstate.reinstate_result import ReinstateResult

with patch('boto3.resource') as resource_mock, patch('boto3.client') as client_mock:
    from common.utils.audit_utils import (
        AuditActions, AuditUsers)
    from post_match_validate_result.post_match_validate_result import \
        lambda_handler


path = 'post_match_validate_result.post_match_validate_result'

PARTICIPANT = {
    'participant_id': '0'
}

PARTICIPANT_ID = PARTICIPANT['participant_id']

get_result_record_mock = Mock()
validate_matched_result_mock = Mock()
reject_result_mock = Mock()
get_participant_by_participant_id_mock = Mock()
reinstate_participant_by_participant_id_mock = Mock()
calculate_and_update_ntdd_mock = Mock()
add_participant_result_record_mock = Mock()
participant_eligible_for_auto_cease_mock = Mock()
update_episode_information_mock = Mock()
cease_participant_and_send_notification_messages_mock = Mock()
check_for_result_letter_suppression_mock = Mock()
add_result_letter_to_processing_queue_mock = Mock()
audit_mock = Mock()

# `reset_mock` does not clear side_effect or return_value
# **clear_all_effects as key word args into the `reset_mock`
# function to avoid lingering side_effects and return_value
clear_all_effects = {
    'side_effect': True,
    'return_value': True
}


@patch(f'{path}.get_result_record', get_result_record_mock)
@patch(f'{path}.validate_matched_result', validate_matched_result_mock)
@patch(f'{path}.reject_result_and_pause_participant', reject_result_mock)
@patch(f'{path}.get_participant_by_participant_id', get_participant_by_participant_id_mock)
@patch(f'{path}.reinstate_participant_by_participant_id', reinstate_participant_by_participant_id_mock)
@patch(f'{path}.calculate_and_update_ntdd', calculate_and_update_ntdd_mock)
@patch(f'{path}.add_participant_result_record', add_participant_result_record_mock)
@patch(f'{path}.participant_eligible_for_auto_cease', participant_eligible_for_auto_cease_mock)
@patch(f'{path}.update_episode_information', update_episode_information_mock)
@patch(f'{path}.cease_participant_and_send_notification_messages', cease_participant_and_send_notification_messages_mock)   
@patch(f'{path}.check_for_result_letter_suppression', check_for_result_letter_suppression_mock)
@patch(f'{path}.add_result_letter_to_processing_queue', add_result_letter_to_processing_queue_mock)
@patch(f'{path}.audit', audit_mock)
class TestPostMatchValidate(TestCase):

    def setUp(self):
        self.log_patcher = patch(f'{path}.log').start()
        get_result_record_mock.reset_mock(**clear_all_effects)
        validate_matched_result_mock.reset_mock(**clear_all_effects)
        reject_result_mock.reset_mock(**clear_all_effects)
        get_participant_by_participant_id_mock.reset_mock(**clear_all_effects)
        reinstate_participant_by_participant_id_mock.reset_mock(**clear_all_effects)
        calculate_and_update_ntdd_mock.reset_mock(**clear_all_effects)
        add_participant_result_record_mock.reset_mock(**clear_all_effects)
        participant_eligible_for_auto_cease_mock.reset_mock(**clear_all_effects)
        update_episode_information_mock.reset_mock(**clear_all_effects)
        cease_participant_and_send_notification_messages_mock.reset_mock(**clear_all_effects)
        check_for_result_letter_suppression_mock.reset_mock(**clear_all_effects)
        add_result_letter_to_processing_queue_mock.reset_mock(**clear_all_effects)
        audit_mock.reset_mock()

    def tearDown(self):
        patch.stopall()

    def test_exception_raised_when_unable_to_find_result_record(self):
        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        lambda_payload = {
            'Records': [{
                'body': json.dumps({
                    "result_id": result_id,
                    "received_time": received_time,
                    "workflow_history": [{'activate': 'check - accept'}]
                })
            }]
        }

        get_result_record_mock.return_value = None

        with self.assertRaises(Exception) as exception_context:
            lambda_handler.__wrapped__(lambda_payload, {})

        self.assertEqual(str(exception_context.exception), 'Result not found')

        get_result_record_mock.assert_called_once_with(result_id, received_time)
        validate_matched_result_mock.assert_not_called()
        reject_result_mock.assert_not_called()
        get_participant_by_participant_id_mock.assert_not_called()
        reinstate_participant_by_participant_id_mock.assert_not_called()
        calculate_and_update_ntdd_mock.assert_not_called()
        add_participant_result_record_mock.assert_not_called()
        participant_eligible_for_auto_cease_mock.assert_not_called()
        update_episode_information_mock.assert_not_called()
        cease_participant_and_send_notification_messages_mock.assert_not_called()
        add_result_letter_to_processing_queue_mock.assert_not_called()
        audit_mock.assert_not_called()

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PSTMTCH0001}),
        ])

    def test_rejected_when_fails_post_match_validation(self):
        result_record = json.loads(load_result_record('example_record_1.json'))
        result_record['matched_to_participant'] = PARTICIPANT_ID

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        lambda_payload = {
            'Records': [{
                'body': json.dumps({
                    "result_id": result_id,
                    "received_time": received_time
                })
            }]
        }

        get_result_record_mock.return_value = result_record

        validate_matched_result_mock.return_value = 'INVALID'

        lambda_handler.__wrapped__(lambda_payload, {})

        get_result_record_mock.assert_called_once_with(result_id, received_time)
        validate_matched_result_mock.asssert_called_once()
        reject_result_mock.assert_called_once_with('INVALID', PARTICIPANT_ID, result_record)
        get_participant_by_participant_id_mock.assert_not_called()
        reinstate_participant_by_participant_id_mock.assert_not_called()
        calculate_and_update_ntdd_mock.assert_not_called()
        add_participant_result_record_mock.assert_not_called()
        participant_eligible_for_auto_cease_mock.assert_not_called()
        update_episode_information_mock.assert_not_called()
        cease_participant_and_send_notification_messages_mock.assert_not_called()
        add_result_letter_to_processing_queue_mock.assert_not_called()
        audit_mock.assert_not_called()

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PSTMTCH0001}),
            call({'log_reference': LogReference.PSTMTCH0004}),
            call({'log_reference': LogReference.PSTMTCH0005, 'reason': ANY})
        ])

    def test_exception_raised_when_unable_to_find_participant_record(self):
        result_record = json.loads(load_result_record('example_record_1.json'))
        result_record['matched_to_participant'] = PARTICIPANT_ID

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        lambda_payload = {
            'Records': [{
                'body': json.dumps({
                    "result_id": result_id,
                    "received_time": received_time
                })
            }]
        }

        get_result_record_mock.return_value = result_record
        validate_matched_result_mock.return_value = None
        get_participant_by_participant_id_mock.return_value = None

        with self.assertRaises(Exception) as exception_context:
            lambda_handler.__wrapped__(lambda_payload, {})

        self.assertEqual(str(exception_context.exception), 'Participant not found')

        get_result_record_mock.assert_called_with(result_id, received_time)
        validate_matched_result_mock.asssert_called_once()
        reject_result_mock.assert_not_called()
        get_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID)
        reinstate_participant_by_participant_id_mock.assert_not_called()
        calculate_and_update_ntdd_mock.assert_not_called()
        add_participant_result_record_mock.assert_not_called()
        participant_eligible_for_auto_cease_mock.assert_not_called()
        update_episode_information_mock.assert_not_called()
        cease_participant_and_send_notification_messages_mock.assert_not_called()
        add_result_letter_to_processing_queue_mock.assert_not_called()
        audit_mock.assert_not_called()

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PSTMTCH0001}),
            call({'log_reference': LogReference.PSTMTCH0004}),
            call({'log_reference': LogReference.PSTMTCHERR0003, 'participant_id': PARTICIPANT_ID})
        ])

    def test_exception_raised_when_unable_to_create_result_record(self):
        result_record = json.loads(load_result_record('example_record_1.json'))
        result_record['matched_to_participant'] = PARTICIPANT_ID

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        lambda_payload = {
            'Records': [{
                'body': json.dumps({
                    "result_id": result_id,
                    "received_time": received_time,
                    "letter_address": 'letter_addres'
                })
            }]
        }

        get_result_record_mock.return_value = result_record
        validate_matched_result_mock.return_value = None
        get_participant_by_participant_id_mock.return_value = PARTICIPANT
        reinstate_participant_by_participant_id_mock.return_value = ReinstateResponse()
        participant_eligible_for_auto_cease_mock.return_value = False
        add_participant_result_record_mock.side_effect = Mock(side_effect=Exception('Cannot add result'))

        with self.assertRaises(Exception) as exception_context:
            lambda_handler.__wrapped__(lambda_payload, {})

        self.assertEqual(str(exception_context.exception), 'Cannot add result')

        get_result_record_mock.assert_called_with(result_id, received_time)
        validate_matched_result_mock.asssert_called_once()
        reject_result_mock.assert_not_called()
        get_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID)
        reinstate_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID, AuditUsers.MATCH_RESULT.value)
        calculate_and_update_ntdd_mock.asssert_called_once()
        add_participant_result_record_mock.assert_called_once_with(
            result_record['decoded'],  result_id, PARTICIPANT
        )
        participant_eligible_for_auto_cease_mock.assert_not_called()
        update_episode_information_mock.assert_not_called()
        cease_participant_and_send_notification_messages_mock.assert_not_called()
        add_result_letter_to_processing_queue_mock.assert_not_called()
        audit_mock.assert_not_called()

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PSTMTCH0001}),
            call({'log_reference': LogReference.PSTMTCH0004}),
            call({'log_reference': LogReference.PSTMTCH0003})
        ])

    def test_exception_raised_when_unable_to_update_episode_information(self):
        result_record = json.loads(load_result_record('example_record_1.json'))
        result_record['matched_to_participant'] = PARTICIPANT_ID

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        lambda_payload = {
            'Records': [{
                'body': json.dumps({
                    "result_id": result_id,
                    "received_time": received_time,
                    "letter_address": 'letter_address'
                })
            }]
        }

        get_result_record_mock.return_value = result_record
        validate_matched_result_mock.return_value = None
        get_participant_by_participant_id_mock.return_value = PARTICIPANT
        reinstate_participant_by_participant_id_mock.return_value = ReinstateResponse()
        participant_eligible_for_auto_cease_mock.return_value = False
        update_episode_information_mock.side_effect = Mock(side_effect=Exception)

        with self.assertRaises(Exception):
            lambda_handler.__wrapped__(lambda_payload, {})

        get_result_record_mock.assert_called_with(result_id, received_time)
        validate_matched_result_mock.asssert_called_once()
        reject_result_mock.assert_not_called()
        get_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID)
        reinstate_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID, AuditUsers.MATCH_RESULT.value)
        calculate_and_update_ntdd_mock.asssert_called_once()
        add_participant_result_record_mock.assert_called_once()
        participant_eligible_for_auto_cease_mock.assert_called_once()
        update_episode_information_mock.assert_called_with(
            result_id, received_time, PARTICIPANT_ID, False)
        cease_participant_and_send_notification_messages_mock.assert_not_called()
        add_result_letter_to_processing_queue_mock.assert_not_called()
        audit_mock.assert_not_called()

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PSTMTCH0001}),
            call({'log_reference': LogReference.PSTMTCH0004}),
            call({'log_reference': LogReference.PSTMTCH0003}),
            call({'log_reference': LogReference.PSTMTCH0002})
        ])

    @patch('post_match_validate_result.post_match_validate_result_service.log', Mock())
    def test_post_match_validate_result_completes_successfully(self):
        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        result_record = json.loads(load_result_record('example_record_1.json'))
        result_record['matched_to_participant'] = PARTICIPANT_ID

        lambda_payload = {
            'Records': [{
                'body': json.dumps({
                    "result_id": result_id,
                    "received_time": received_time,
                    "letter_address": 'letter_address'
                })
            }]
        }

        participant = {
            'participant_id': '0',
            'next_test_due_date': '2018-01-07'
        }

        get_result_record_mock.return_value = result_record
        validate_matched_result_mock.return_value = None
        get_participant_by_participant_id_mock.return_value = participant
        reinstate_participant_by_participant_id_mock.return_value = ReinstateResponse()
        participant_eligible_for_auto_cease_mock.return_value = True
        check_for_result_letter_suppression_mock.return_value = None

        participant_result_record = {'test_date': '1', 'slide_number': '2', 'action': '3',
                                     'action_code': '4', 'result': '5', 'result_code': '6',
                                     'infection_result': '6', 'infection_code': '7',
                                     'key_to_ignore_in_audit': '8', 'letter_address': 'letter_address',
                                     'recall_months': '36'}

        add_participant_result_record_mock.return_value = participant_result_record

        lambda_handler.__wrapped__(lambda_payload, {})

        get_result_record_mock.assert_called_once_with(result_id, received_time)
        validate_matched_result_mock.assert_called_once_with(result_record['decoded'], '0')
        reject_result_mock.assert_not_called()
        get_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID)
        reinstate_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID, AuditUsers.MATCH_RESULT.value)
        calculate_and_update_ntdd_mock.assert_called_once()
        add_participant_result_record_mock.assert_called_once_with(
            result_record['decoded'], result_id, participant)
        participant_eligible_for_auto_cease_mock.assert_called_once()
        update_episode_information_mock.assert_called_once_with(
            result_id, received_time, PARTICIPANT_ID, True)
        cease_participant_and_send_notification_messages_mock.assert_called_once_with(
            {'participant_id': '0', 'next_test_due_date': '2018-01-07', 'letter_address': 'letter_address'}, 'SYSTEM',
            {'reason': CeaseReason.AUTOCEASE_DUE_TO_AGE}, send_letter=False)
        add_result_letter_to_processing_queue_mock.assert_called_once_with(participant_result_record, True)
        audit_mock.assert_has_calls([
            call(action=AuditActions.CEASE_EPISODE, user=AuditUsers.SYSTEM, participant_ids=['0'],
                 additional_information={'updating_next_test_due_date_from': '2018-01-07'}),
            call(
                action=AuditActions.RESULT_MATCHED,
                user=AuditUsers.MATCH_RESULT,
                participant_ids=['0'],
                additional_information={'test_date': '1', 'slide_number': '2', 'action': '3', 'action_code': '4',
                                        'result': '5', 'result_code': '6', 'infection_result': '6',
                                        'infection_code': '7', 'lab_address': 'AAAAAAAAA A AAAA AAAAA AAAA M2 6LW'}
            )
        ])

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PSTMTCH0001}),
            call({'log_reference': LogReference.PSTMTCH0004}),
            call({'log_reference': LogReference.PSTMTCH0003}),
            call({'log_reference': LogReference.PSTMTCH0002})
        ])

    @patch('post_match_validate_result.post_match_validate_result_service.log', Mock())
    def test_post_match_validate_result_completes_successfully_on_reinstate(self):
        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        result_record = json.loads(load_result_record('example_record_1.json'))
        result_record['matched_to_participant'] = PARTICIPANT_ID

        lambda_payload = {
            'Records': [{
                'body': json.dumps({
                    "result_id": result_id,
                    "received_time": received_time,
                    "letter_address": 'letter_address'
                })
            }]
        }

        participant = {
            'participant_id': PARTICIPANT_ID,
            'next_test_due_date': '2018-01-07'
        }

        get_result_record_mock.return_value = result_record
        validate_matched_result_mock.return_value = None
        get_participant_by_participant_id_mock.return_value = participant
        reinstate_participant_by_participant_id_mock.return_value = ReinstateResponse(
            result=ReinstateResult.REINSTATED,
            next_test_due_date=date(2021, 1, 1)
        )
        participant_eligible_for_auto_cease_mock.return_value = False
        check_for_result_letter_suppression_mock.return_value = None

        participant_result_record = {'test_date': '1', 'slide_number': '2', 'action': '3',
                                     'action_code': '4', 'result': '5', 'result_code': '6',
                                     'infection_result': '6', 'infection_code': '7',
                                     'key_to_ignore_in_audit': '8', 'letter_address': 'letter_address',
                                     'recall_months': '36'}

        add_participant_result_record_mock.return_value = participant_result_record

        lambda_handler.__wrapped__(lambda_payload, {})

        get_result_record_mock.assert_called_with(result_id, received_time)
        validate_matched_result_mock.assert_called_once_with(result_record['decoded'], '0')
        reject_result_mock.assert_not_called()
        get_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID)
        reinstate_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID, AuditUsers.MATCH_RESULT.value)
        calculate_and_update_ntdd_mock.assert_not_called()
        add_participant_result_record_mock.assert_called_once_with(
            result_record['decoded'], result_id, participant)
        update_episode_information_mock.assert_called_once_with(
            result_id, received_time, PARTICIPANT_ID, False)
        cease_participant_and_send_notification_messages_mock.assert_not_called()
        add_result_letter_to_processing_queue_mock.assert_called_once_with(participant_result_record, False)
        audit_mock.assert_has_calls([
            call(
                action=AuditActions.RESULT_MATCHED,
                user=AuditUsers.MATCH_RESULT,
                participant_ids=['0'],
                additional_information={'test_date': '1', 'slide_number': '2', 'action': '3', 'action_code': '4',
                                        'result': '5', 'result_code': '6', 'infection_result': '6',
                                        'infection_code': '7', 'lab_address': 'AAAAAAAAA A AAAA AAAAA AAAA M2 6LW'}
            )
        ])

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PSTMTCH0001}),
            call({'log_reference': LogReference.PSTMTCH0004}),
            call({'log_reference': LogReference.PSTMTCH0003}),
            call({'log_reference': LogReference.PSTMTCH0002})
        ])

    @patch('post_match_validate_result.post_match_validate_result_service.log', Mock())
    def test_post_match_validate_result_completes_successfully_without_address(self):
        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        result_record = json.loads(load_result_record('example_record_2.json'))
        result_record['matched_to_participant'] = PARTICIPANT_ID

        lambda_payload = {
            'Records': [{
                'body': json.dumps({
                    "result_id": result_id,
                    "received_time": received_time,
                    "letter_address": 'letter_address'
                })
            }]
        }

        participant = {
            'participant_id': '0',
            'next_test_due_date': '2018-01-07'
        }

        get_result_record_mock.return_value = result_record
        validate_matched_result_mock.return_value = None
        get_participant_by_participant_id_mock.return_value = participant
        reinstate_participant_by_participant_id_mock.return_value = ReinstateResponse()
        participant_eligible_for_auto_cease_mock.return_value = True
        check_for_result_letter_suppression_mock.return_value = None

        participant_result_record = {'test_date': '1', 'slide_number': '2', 'action': '3',
                                     'action_code': '4', 'result': '5', 'result_code': '6',
                                     'infection_result': '6', 'infection_code': '7',
                                     'key_to_ignore_in_audit': '8', 'letter_address': 'letter_address'}

        add_participant_result_record_mock.return_value = participant_result_record

        lambda_handler.__wrapped__(lambda_payload, {})

        get_result_record_mock.assert_called_with(result_id, received_time)
        validate_matched_result_mock.assert_called_once_with(result_record['decoded'], '0')
        reject_result_mock.assert_not_called()
        get_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID)
        reinstate_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID, AuditUsers.MATCH_RESULT.value)
        calculate_and_update_ntdd_mock.assert_called_once()
        add_participant_result_record_mock.assert_called_once_with(
            result_record['decoded'], result_id, participant)
        update_episode_information_mock.assert_called_once_with(
            result_id, received_time, PARTICIPANT_ID, True)
        cease_participant_and_send_notification_messages_mock.assert_called_once_with(
            {'participant_id': '0', 'next_test_due_date': '2018-01-07', 'letter_address': 'letter_address'}, 'SYSTEM',
            {'reason': CeaseReason.AUTOCEASE_DUE_TO_AGE}, send_letter=False)
        add_result_letter_to_processing_queue_mock.assert_called_once_with(participant_result_record, True)
        audit_mock.assert_has_calls([
            call(action=AuditActions.CEASE_EPISODE, user=AuditUsers.SYSTEM, participant_ids=['0'],
                 additional_information={'updating_next_test_due_date_from': '2018-01-07'}),
            call(
                action=AuditActions.RESULT_MATCHED,
                user=AuditUsers.MATCH_RESULT,
                participant_ids=['0'],
                additional_information={'test_date': '1', 'slide_number': '2', 'action': '3', 'action_code': '4',
                                        'result': '5', 'result_code': '6', 'infection_result': '6',
                                        'infection_code': '7', 'lab_address': ''}
            )
        ])

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PSTMTCH0001}),
            call({'log_reference': LogReference.PSTMTCH0004}),
            call({'log_reference': LogReference.PSTMTCH0003}),
            call({'log_reference': LogReference.PSTMTCH0002})
        ])

    @patch('post_match_validate_result.post_match_validate_result_service.log', Mock())
    def test_post_match_validate_result_adds_recall_months_to_audit_if_action_is_repeat_advised(self):
        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        result_record = json.loads(load_result_record('example_record_1.json'))
        result_record['decoded']['action'] = 'Repeat advised'
        result_record['matched_to_participant'] = PARTICIPANT_ID

        lambda_payload = {
            'Records': [{
                'body': json.dumps({
                    "result_id": result_id,
                    "received_time": received_time,
                    "letter_address": 'letter_address'
                })
            }]
        }

        participant = {
            'participant_id': '0',
            'next_test_due_date': '2018-01-07'
        }

        get_result_record_mock.return_value = result_record
        validate_matched_result_mock.return_value = None
        get_participant_by_participant_id_mock.return_value = participant
        reinstate_participant_by_participant_id_mock.return_value = ReinstateResponse()
        participant_eligible_for_auto_cease_mock.return_value = True
        check_for_result_letter_suppression_mock.return_value = None

        participant_result_record = {'test_date': '1', 'slide_number': '2', 'action': 'Repeat advised',
                                     'action_code': '4', 'result': '5', 'result_code': '6',
                                     'infection_result': '6', 'infection_code': '7',
                                     'key_to_ignore_in_audit': '8', 'letter_address': 'letter_address'}

        add_participant_result_record_mock.return_value = participant_result_record

        lambda_handler.__wrapped__(lambda_payload, {})

        get_result_record_mock.assert_called_with(result_id, received_time)
        validate_matched_result_mock.assert_called_once_with(result_record['decoded'], '0')
        reject_result_mock.assert_not_called()
        get_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID)
        reinstate_participant_by_participant_id_mock.assert_called_with(PARTICIPANT_ID, AuditUsers.MATCH_RESULT.value)
        calculate_and_update_ntdd_mock.assert_called_once()
        add_participant_result_record_mock.assert_called_once_with(
            result_record['decoded'], result_id, participant)
        update_episode_information_mock.assert_called_once_with(
            result_id, received_time, PARTICIPANT_ID, True)
        cease_participant_and_send_notification_messages_mock.assert_called_once_with(
            {'participant_id': '0', 'next_test_due_date': '2018-01-07', 'letter_address': 'letter_address'}, 'SYSTEM',
            {'reason': CeaseReason.AUTOCEASE_DUE_TO_AGE}, send_letter=False)
        add_result_letter_to_processing_queue_mock.assert_called_once_with(participant_result_record, True)
        audit_mock.assert_has_calls([
            call(action=AuditActions.CEASE_EPISODE, user=AuditUsers.SYSTEM, participant_ids=['0'],
                 additional_information={'updating_next_test_due_date_from': '2018-01-07'}),
            call(
                action=AuditActions.RESULT_MATCHED,
                user=AuditUsers.MATCH_RESULT,
                participant_ids=['0'],
                additional_information={'test_date': '1', 'slide_number': '2', 'action': 'Repeat advised',
                                        'action_code': '4', 'result': '5', 'result_code': '6',
                                        'infection_result': '6', 'infection_code': '7',
                                        'lab_address': 'AAAAAAAAA A AAAA AAAAA AAAA M2 6LW', 'recall_months': '00'}
            )
        ])

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.PSTMTCH0001}),
            call({'log_reference': LogReference.PSTMTCH0004}),
            call({'log_reference': LogReference.PSTMTCH0003}),
            call({'log_reference': LogReference.PSTMTCH0002})
        ])


def load_result_record(name):
    current_path = os.path.dirname(os.path.realpath(__file__))
    full_file_path = os.path.join(current_path, 'test_data', name)
    with open(full_file_path) as file:
        return file.read()
