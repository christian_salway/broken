from datetime import datetime, timezone

from common.log import log
from common.utils.match_result_utils import (
    create_participant_result_record)
from common.utils.participant_episode_utils import (
    query_for_live_episode, update_episode_to_completion_state)
from common.utils.participant_utils import \
    create_replace_record_in_participant_table
from common.utils.result_utils import \
    update_result_record
from post_match_validate_result.log_references import LogReference
from common.models.participant import EpisodeStatus


def update_result(record, matched_participant):
    key = {
        'result_id': record['result_id'],
        'received_time': record['received_time']
    }
    attributes_to_update = {
        'matched_time': datetime.now(timezone.utc).isoformat(),
        'matched_to_participant': matched_participant['participant']['participant_id'],
    }
    update_result_record(key, attributes_to_update)


def update_episode_information(result_id, received_time, participant_id, autocease):
    live_episodes = query_for_live_episode(participant_id)
    print(live_episodes)
    number_of_live_episodes = len(live_episodes)

    if number_of_live_episodes == 0:
        log({
            'log_reference': LogReference.PSTMTCHERR0002,
            'result_id': result_id,
            'received_time': received_time,
            'participant_id': participant_id
        })
        return

    if number_of_live_episodes == 1:
        episode = live_episodes[0]
        sort_key = episode['sort_key']
        update_episode_to_completion_state(
            participant_id, sort_key, received_time, result_id, autocease, EpisodeStatus.COMPLETED.value)
        return

    for episode in live_episodes:
        sort_key = episode['sort_key']
        update_episode_to_completion_state(
            participant_id, sort_key, received_time, result_id, autocease,
            EpisodeStatus.COMPLETED.value if episode.get('status')
            == EpisodeStatus.WALKIN.value else EpisodeStatus.CLOSED.value)


def add_participant_result_record(decoded_record, result_id, matched_participant):
    result_record = create_participant_result_record(decoded_record, result_id,
                                                     matched_participant, get_current_iso_datetime())
    create_replace_record_in_participant_table(result_record)
    return result_record


def get_current_iso_datetime():
    return datetime.now(timezone.utc).isoformat()
