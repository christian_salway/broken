import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    PSTMTCH0001 = (logging.INFO, 'Received request to match a result')
    PSTMTCH0002 = (logging.INFO, 'Updating episode record')
    PSTMTCH0003 = (logging.INFO, 'Creating new participant result record')
    PSTMTCH0004 = (logging.INFO, 'Attempting post match validation')
    PSTMTCH0005 = (logging.WARNING, 'Record failed post match validation')

    PSTMTCHERR0001 = (logging.ERROR, 'Unable to find a result record')
    PSTMTCHERR0002 = (logging.INFO, 'Unable to find an episode record')
    PSTMTCHERR0003 = (logging.ERROR, 'Unable to find a participant record')
