from datetime import datetime, timezone
from common.log import log
from manage_manual_result_matching.log_references import LogReference
from manage_manual_result_matching.manage_manual_result_matching_utils import (
    extract_data_from_event,
    build_update_object,
    generate_additional_information,
    transaction_write_records
)
from common.utils.audit_utils import audit, AuditActions
from common.utils.dynamodb_helper_utils import (
    build_update_record, serialize_update, build_put_record, serialize_item
)
from common.models.result import ResultAction
from common.utils.dynamodb_access.table_names import TableNames


def delete_rejected_result(event_data):
    log({'log_reference': LogReference.REJDELMTCH0001})

    try:
        result_id = event_data['path_parameters']['result_id']
        sending_lab = event_data['path_parameters']['sending_lab']

        session_user_data = event_data['session']['user_data']
        user_name = f'{session_user_data["first_name"]} {session_user_data["last_name"]}'

        workflow_state, secondary_status, workflow_history, received_time, decoded_record = \
            extract_data_from_event(event_data)

        update_attributes = {
            'workflow_state': workflow_state,
            'secondary_status': secondary_status
        }

        if workflow_history:
            for history in workflow_history:
                if len(history.get('user', '')) == 0:
                    history['user'] = user_name

            update_attributes.update({'workflow_history': workflow_history})

        put_transact_items = []
        update_transact_items = []

        update = build_update_object(update_attributes, result_id, received_time)

        put_transact_items = [generate_audit_result_updates(sending_lab, decoded_record)]
        log({'log_reference': LogReference.REJDELMTCH0005})

        update_transact_items.append(build_update_record(serialize_update(update), TableNames.RESULTS))
        transaction_write_records(put_transact_items, update_transact_items)

        audit(
            action=AuditActions.UPDATE_UNMATCHED_RESULT,
            session=event_data.get('session'),
            additional_information=generate_additional_information(result_id, workflow_history)
        )
        log({'log_reference': LogReference.REJDELMTCH0003})
    except Exception as e:
        log({'log_reference': LogReference.REJDELMTCH0004})
        raise e
    return {'message': 'record updated'}


def generate_audit_result_updates(sending_lab, decoded_record):
    created = datetime.now(timezone.utc).isoformat(timespec='microseconds')
    result_record = {
        'result_id': f'AUDITRESULT#{created}',
        'received_time': f'{created}',
        'sending_lab': sending_lab,
        'workflow_history': [{
            'action': ResultAction.REJECTED_DELETE_ACTION.value
        }]
    }
    if decoded_record:
        result_record.update({
            'decoded': {
                'nhs_number': decoded_record.get('nhs_number')
            }
        })

    return build_put_record(serialize_item(result_record), TableNames.RESULTS)
