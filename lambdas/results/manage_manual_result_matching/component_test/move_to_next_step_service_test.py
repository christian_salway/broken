from unittest import TestCase

from mock import Mock, call, patch
from manage_manual_result_matching.log_references import LogReference

from manage_manual_result_matching.move_to_next_step_service import move_to_next_step
from common.utils.dynamodb_access.table_names import TableNames

event_data_mock = {
    'path_parameters': {
        'result_id': "1122123132"
    },
    'body': {
        'received_time': '123',
        'workflow_state': 'process'
    }
}

results_table_mock = Mock()


@patch('manage_manual_result_matching.move_to_next_step_service.dynamodb_update_item', results_table_mock)
class ComponentTestMoveToNextStepService(TestCase):

    # Happy path test for move to next step
    @patch('manage_manual_result_matching.move_to_next_step_service.log')
    def test_move_to_next_step_service(self, log_mock):
        # Arrange
        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0015})]
        expected_key = {
            'result_id': '1122123132',
            'received_time': '123'
        }
        expected_update_expression = 'SET #secondary_status = :secondary_status'
        expected_expression_values = {':secondary_status': 'WITH PROCESSOR'}
        expected_expression_names = {'#secondary_status': 'secondary_status'}

        # Act
        # Escape the lock checking decorator as it is common to all services
        move_to_next_step.__wrapped__(event_data_mock)

        # Assert
        results_table_mock.assert_called_with(TableNames.RESULTS, dict(
            Key=expected_key,
            UpdateExpression=expected_update_expression,
            ExpressionAttributeValues=expected_expression_values,
            ExpressionAttributeNames=expected_expression_names,
            ReturnValues='NONE'
        ))
        log_mock.assert_has_calls(expected_log_calls)
