from unittest import TestCase
from mock import Mock, call, patch
from common.log_references import CommonLogReference

from manage_manual_result_matching.log_references import LogReference
from manage_manual_result_matching.manage_manual_result_matching_service import get_unmatched_lab_results
from common.utils.data_segregation.nhais_ciphers import Cohorts
from boto3.dynamodb.conditions import Key
from copy import deepcopy

SENDING_LAB_CODE = "NBT3"

EVENT_DATA_MOCK = {
    'path_parameters': {
        'sending_lab': SENDING_LAB_CODE
    }
}

RETRIEVED_RESULTS_LIST = {
    "Items": [
        {
            "result_id": "IOM_unmatched_result",
            "sending_lab": SENDING_LAB_CODE,
            "sanitised_nhais_cipher": "IM",
            "possible_match_participants": [
                {
                    "participant_id": "iom-patient"
                }
            ],
            'workflow_state': "process",
            'validation_errors': None,
            'secondary_status': "WITH PROCESSOR",
            'workflow_history': None,
            'user_id': None,
            'user_name': None,
            'role_id': None,
            'lock_timestamp': None,
            'received_time': "2021-05-28 09:54:36+00:00",
            'rejected_reason': None,
            "decoded": {}
        },
        {
            "result_id": "English_unmatched_result",
            "sending_lab": SENDING_LAB_CODE,
            "sanitised_nhais_cipher": "BRS",
            "possible_match_participants": [
                {
                    "participant_id": "iom-patient"
                },
                {
                    "participant_id": "english-patient"
                }
            ],
            'workflow_state': "process",
            'validation_errors': None,
            'secondary_status': "WITH PROCESSOR",
            'workflow_history': None,
            'user_id': None,
            'user_name': None,
            'role_id': None,
            'lock_timestamp': None,
            'received_time': "2021-05-28 09:54:36+00:00",
            'rejected_reason': None,
            "decoded": {}
        }
    ]
}

EXPECTED_ENGLISH_USER_RESPONSE = [{
    "result_id": "English_unmatched_result",
    "sending_lab": SENDING_LAB_CODE,
    "possible_match_participants": [
        {
            "participant_id": "iom-patient"
        },
        {
            "participant_id": "english-patient"
        }
    ],
    'workflow_state': "process",
    'matched_to_participant': None,
    'validation_errors': None,
    'secondary_status': "WITH PROCESSOR",
    'workflow_history': None,
    'user_id': None,
    'user_name': None,
    'role_id': None,
    'lock_timestamp': None,
    'received_time': "2021-05-28 09:54:36+00:00",
    'rejected_reason': None,
    "decoded": {}
}]

EXPECTED_IOM_USER_RESPONSE = [{
    "result_id": "IOM_unmatched_result",
    "sending_lab": SENDING_LAB_CODE,
    "possible_match_participants": [
        {
            "participant_id": "iom-patient"
        }
    ],
    'workflow_state': "process",
    'matched_to_participant': None,
    'validation_errors': None,
    'secondary_status': "WITH PROCESSOR",
    'workflow_history': None,
    'user_id': None,
    'user_name': None,
    'role_id': None,
    'lock_timestamp': None,
    'received_time': "2021-05-28 09:54:36+00:00",
    'rejected_reason': None,
    "decoded": {}
}]

PROJECTION_EXPRESSION = ', '.join([
    'result_id',
    'sending_lab',
    'decoded',
    'workflow_state',
    'validation_errors',
    'received_time',
    'secondary_status',
    'workflow_history',
    'user_id',
    'user_name',
    'lock_timestamp',
    'possible_match_participants',
    'rejected_reason',
    'crm_number',
    'duplicates',
    'sanitised_nhais_cipher',
    'matched_to_participant'
])

EXPECTED_LOG_CALLS = [
    call({'log_reference': LogReference.MNGRESMTCH0011}),
    call({
        'log_reference': LogReference.MNGRESMTCH0014,
        'unmatched_result_count': 1
    })]
EXPECTED_FILTER_LOG_CALLS = [call({'log_reference': CommonLogReference.DATASEG0003, 'removed_items_count': 1})]

results_table_mock = Mock()
log_mock = Mock()
filter_log_mock = Mock()


class ComponentTestManageManualMatchingService(TestCase):

    @patch('manage_manual_result_matching.manage_manual_result_matching_service.log',
           log_mock)
    @patch('common.utils.data_segregation.data_segregation_filters.log', filter_log_mock)
    @patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
           Mock(return_value=results_table_mock))
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.ENGLISH]))
    def test_get_unmatched_lab_results_english_segregation(self):
        # Arrange
        results_table_mock.query.return_value = deepcopy(RETRIEVED_RESULTS_LIST)

        # Act
        actual_response = get_unmatched_lab_results(EVENT_DATA_MOCK)

        # Assert
        self.assertEqual(actual_response, EXPECTED_ENGLISH_USER_RESPONSE)

        results_table_mock.query.assert_called_with(
            IndexName='sending-lab',
            KeyConditionExpression=Key('sending_lab').eq(SENDING_LAB_CODE),
            ProjectionExpression=PROJECTION_EXPRESSION
        )
        log_mock.assert_has_calls(EXPECTED_LOG_CALLS)
        filter_log_mock.assert_has_calls(EXPECTED_FILTER_LOG_CALLS)

    @patch('manage_manual_result_matching.manage_manual_result_matching_service.log',
           log_mock)
    @patch('common.utils.data_segregation.data_segregation_filters.log', filter_log_mock)
    @patch('common.utils.dynamodb_access.segregated_operations.get_dynamodb_table',
           Mock(return_value=results_table_mock))
    @patch('common.utils.data_segregation.data_segregation_filters.get_current_workgroups',
           Mock(return_value=[Cohorts.IOM]))
    def test_get_unmatched_lab_results_iom_segregation(self):
        # Arrange
        results_table_mock.query.return_value = deepcopy(RETRIEVED_RESULTS_LIST)

        # Act
        actual_response = get_unmatched_lab_results(EVENT_DATA_MOCK)

        # Assert
        self.assertEqual(actual_response, EXPECTED_IOM_USER_RESPONSE)

        results_table_mock.query.assert_called_with(
            IndexName='sending-lab',
            KeyConditionExpression=Key('sending_lab').eq(SENDING_LAB_CODE),
            ProjectionExpression=PROJECTION_EXPRESSION
        )
        log_mock.assert_has_calls(EXPECTED_LOG_CALLS)
        filter_log_mock.assert_has_calls(EXPECTED_FILTER_LOG_CALLS)
