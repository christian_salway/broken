from datetime import datetime, timezone
from common.log import log
from manage_manual_result_matching.log_references import LogReference
from common.utils.dynamodb_helper_utils import (
    build_update_record, serialize_update, build_put_record, serialize_item
)
from common.utils.match_result_utils import (
    generate_reinstate_participant_transactions, calculate_and_update_ntdd
)
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from common.utils.participant_episode_utils import (
    query_for_live_episode
)
from common.utils.result_letter_utils import (
    check_for_result_letter_suppression,
    add_result_letter_to_processing_queue
)
from common.utils.match_result_utils import (
    create_participant_result_record
)
from common.utils.cease_utils import (
    participant_eligible_for_auto_cease,
    cease_participant_and_send_notification_messages
)
from common.models.participant import CeaseReason
from manage_manual_result_matching.manage_manual_result_matching_utils import (
    extract_data_from_event,
    validate_check_result,
    build_update_object,
    get_matched_participant_from_workflow_history,
    transaction_write_records,
    generate_additional_information
)
from common.models.result import ResultAddressSelection
from common.models.participant import EpisodeStatus
from common.utils.dynamodb_access.table_names import TableNames

RESULT_FIELDS_TO_AUDIT = ['test_date', 'slide_number', 'action', 'action_code', 'result', 'result_code',
                          'infection_result', 'infection_code', 'source_code', 'sender_code', 'sender_source_type'
                          ]


def accept_match(event_data):
    log({'log_reference': LogReference.ACCPTMTCH0001})

    try:
        result_id = event_data['path_parameters']['result_id']

        session_user_data = event_data['session']['user_data']
        user_name = f'{session_user_data["first_name"]} {session_user_data["last_name"]}'

        workflow_state, secondary_status, workflow_history, received_time, decoded_record = \
            extract_data_from_event(event_data)

        # Validate the result passed in if its workflow_state is "check"
        new_workflow_state, new_secondary_status, rejected_reason = validate_check_result(
            workflow_history, workflow_state, secondary_status, decoded_record, result_id, received_time
        )

        update_attributes = {
            'workflow_state': new_workflow_state,
            'secondary_status': new_secondary_status,
        }

        if workflow_history:
            for history in workflow_history:
                if len(history.get('user', '')) == 0:
                    history['user'] = user_name

            update_attributes.update({'workflow_history': workflow_history})

        put_transact_items = []
        update_transact_items = []

        update = build_update_object(update_attributes, result_id, received_time, rejected_reason)

        accepted_participant_updates, participant_result_record, matched_participant = \
            generate_accepted_participant_update(decoded_record, workflow_history, update_attributes, result_id)

        put_transact_items = [accepted_participant_updates[0]]
        participant = accepted_participant_updates[1]
        participant_id = participant['participant_id']

        reinstate_and_episode_updates, reinstate_put, updated_next_test_due_date = \
            check_if_participant_can_be_reinstated_and_build_episode_updates(
                decoded_record, received_time, result_id, participant)

        if reinstate_and_episode_updates:
            update_transact_items.extend(reinstate_and_episode_updates)

        if reinstate_put:
            log({'log_reference': LogReference.ACCPTMTCH0002, 'participant_id': participant_id})
            put_transact_items.append(reinstate_put)

        update['update_values'] = update_attributes
        log({'log_reference': LogReference.ACCPTMTCH0011})

        serialized_update = serialize_update(update)
        update_record = build_update_record(serialized_update, TableNames.RESULTS)
        update_transact_items.append(update_record)

        transaction_write_records(put_transact_items, update_transact_items)

        original_next_test_due_date = matched_participant['next_test_due_date']
        # ensure matched participant object matches database for autocease logic as ntdd has been updated above
        matched_participant['next_test_due_date'] = updated_next_test_due_date
        if reinstate_put:
            audit(
                action=AuditActions.REINSTATE_PARTICIPANT,
                user=AuditUsers.MATCH_RESULT,
                participant_ids=[participant_id],
                nhs_numbers=[matched_participant['nhs_number']],
                additional_information={
                    'updating_next_test_due_date_to': updated_next_test_due_date}
            )

        additional_information = {
            k: v for k, v in participant_result_record.items() if k in RESULT_FIELDS_TO_AUDIT}
        try:
            additional_information.update({'lab_address': decoded_record['address']})
        except KeyError:
            additional_information.update({'lab_address': ''})
        if not reinstate_put:
            additional_information.update(
                {
                    'updating_next_test_due_date_from': original_next_test_due_date,
                    'updating_next_test_due_date_to': updated_next_test_due_date
                }
            )

        if decoded_record['action'] == 'Repeat advised':
            additional_information.update({'recall_months': decoded_record['recall_months']})

        audit(
            action=AuditActions.MANUAL_MATCH_RESULT,
            session=event_data.get('session'),
            participant_ids=[participant_id],
            nhs_numbers=[matched_participant['nhs_number']],
            additional_information=additional_information
        )
        was_autoceased = autocease_if_eligible(matched_participant)
        if not check_for_result_letter_suppression(participant_result_record):
            add_result_letter_to_processing_queue(participant_result_record, was_autoceased)

        audit(
            action=AuditActions.UPDATE_UNMATCHED_RESULT,
            session=event_data.get('session'),
            additional_information=generate_additional_information(result_id, workflow_history)
        )
        log({'log_reference': LogReference.ACCPTMTCH0009})
    except Exception as e:
        log({'log_reference': LogReference.ACCPTMTCH0010})
        raise e
    return {'message': 'record updated'}


def generate_accepted_participant_update(decoded_record, workflow_history, update_attributes, result_id):
    try:
        matched_participant = get_matched_participant_from_workflow_history(workflow_history)
        participant_id = matched_participant['participant_id']

        selection, new_letter_address = get_selection_and_new_address_from_workflow_history(workflow_history)
        if new_letter_address:
            letter_address = new_letter_address
        else:
            letter_address = _get_letter_address(matched_participant, selection, decoded_record)

        matched_participant['letter_address'] = letter_address

        participant_result_record = create_participant_result_record(
            decoded_record, result_id, matched_participant, datetime.now(timezone.utc).isoformat())

        update_attributes.update({
            'matched_time': datetime.now(timezone.utc).isoformat(),
            'matched_to_participant': participant_id
        })
        return (build_put_record(
            serialize_item(participant_result_record), TableNames.PARTICIPANTS), matched_participant), participant_result_record, matched_participant  
    except Exception as e:
        log({'log_reference': LogReference.ACCPTMTCH0003})
        raise e


def check_if_participant_can_be_reinstated_and_build_episode_updates(
        decoded_record, received_time, result_id, participant):
    log({'log_reference': LogReference.ACCPTMTCH0004})

    update_transact_items = []

    next_test_due_date = None
    participant_id = participant['participant_id']

    test_date = decoded_record.get('test_date')
    recall_type = decoded_record.get('status')
    participant_update, reinstate_put, reinstated_next_test_due_date = \
        generate_reinstate_participant_transactions(participant_id, TableNames.PARTICIPANTS, test_date)

    if participant_update:
        update_transact_items.append(participant_update)

    if not reinstate_put:
        log({'log_reference': LogReference.ACCPTMTCH0005})
        next_test_due_date = calculate_and_update_ntdd(decoded_record, participant, recall_type)

    can_auto_cease = participant_eligible_for_auto_cease(participant, next_test_due_date) \
        if next_test_due_date else None

    # Episode Updates
    episode_updates = build_updates_for_episodes(participant_id, received_time, result_id, can_auto_cease)
    if episode_updates:
        update_transact_items.extend(episode_updates)

    next_test_due_date_value = str(next_test_due_date) if next_test_due_date else None
    final_ntdd = reinstated_next_test_due_date if reinstate_put else next_test_due_date_value
    return update_transact_items, reinstate_put, final_ntdd


def build_updates_for_episodes(participant_id, result_date, result_id, auto_cease):
    live_episodes = query_for_live_episode(participant_id)
    if live_episodes:
        log({'log_reference': LogReference.EPISODE0001})
        live_episode_updates = []
        for live_episode in live_episodes:
            update_attributes = {
                'result_date': result_date,
                'result_id': result_id
            }

            delete_attributes = []
            if not auto_cease:
                delete_attributes = ['live_record_status']

            if live_episode['live_record_status'] == EpisodeStatus.WALKIN.value:
                update_attributes.update({'status': EpisodeStatus.COMPLETED.value})
            else:
                update_attributes.update({'status': EpisodeStatus.CLOSED.value})

            update = {
                'record_keys': {
                    'participant_id': participant_id,
                    'sort_key': live_episode['sort_key']
                },
                'update_values': update_attributes,
                'delete_values': delete_attributes
            }
            live_episode_updates.append(build_update_record(serialize_update(update), TableNames.PARTICIPANTS))
        log({'log_reference': LogReference.EPISODE0002})
        return live_episode_updates


def autocease_if_eligible(participant):
    log(LogReference.ACCPTMTCH0006)

    participant_next_test_due_date = participant.get('next_test_due_date')
    next_test_due_date = datetime.fromisoformat(participant_next_test_due_date).date() \
        if participant_next_test_due_date else None

    can_autocease = participant_eligible_for_auto_cease(participant, next_test_due_date)
    if can_autocease:
        log(LogReference.ACCPTMTCH0007)

        reason = CeaseReason.AUTOCEASE_DUE_TO_AGE
        cease_participant_and_send_notification_messages(
            participant, AuditUsers.SYSTEM.value, {'reason': reason}
        )
        audit(action=AuditActions.CEASE_EPISODE,
              user=AuditUsers.SYSTEM,
              participant_ids=[participant['participant_id']],
              nhs_numbers=[participant['nhs_number']],
              additional_information={'reason': reason,
                                      'updating_next_test_due_date_from': participant_next_test_due_date})
        return can_autocease
    else:
        log(LogReference.ACCPTMTCH0008)


def _get_letter_address(matched_participant, selection, decoded_result):
    if selection == ResultAddressSelection.ACTION_SEND_TO_LAB:
        return decoded_result['sanitised_lab_address']
    elif selection == ResultAddressSelection.ACTION_SEND_TO_SYSTEM:
        return matched_participant['address']
    else:
        return decoded_result['sender_address']


def get_selection_and_new_address_from_workflow_history(history):
    workflow_history_items = _get_workflow_history_items_with_selected_participant_id(history)
    if workflow_history_items:
        selection = workflow_history_items[-1]['selection']
        if selection == ResultAddressSelection.ACTION_NEW_ADDRESS:
            new_letter_address = workflow_history_items[- 1]['address']
            return selection, new_letter_address
        return selection, None


def _get_workflow_history_items_with_selected_participant_id(history):
    return [
        history_item for history_item in history if history_item.get('selected_participant_id')
    ]
