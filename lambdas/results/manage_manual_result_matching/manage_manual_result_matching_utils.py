from common.utils.participant_utils import (
    get_participants_by_participant_ids
)
from common.utils.match_result_utils import (
    validate_matched_result
)
from common.utils.result_utils import (
    get_result
)
from common.models.result import (
    ResultWorkflowState,
    ResultSecondaryStatus
)
from common.log import log
from common.utils.dynamodb_access.operations import dynamodb_transact_write_items
from manage_manual_result_matching.log_references import LogReference

SENDING_LAB_GSI = 'sending-lab'


def extract_data_from_event(event_data):
    body = event_data['body']
    workflow_state = body['workflow_state']
    secondary_status = body['secondary_status']
    workflow_history = body.get('workflow_history')
    received_time = body['received_time']
    decoded_record = body.get('decoded')

    return workflow_state, secondary_status, workflow_history, received_time, decoded_record


def generate_additional_information(result_id, workflow_history: None):
    additional_information = {
        'result_id': result_id
    }

    if workflow_history:
        last_event = _get_workflow_history_items_with_selected_participant_id(workflow_history)
        if last_event:
            additional_information["selection"] = last_event[-1].get('selection')
            additional_information["participant"] = last_event[-1].get('selected_participant_id')

    return additional_information


def get_participant_id(history):
    workflow_history_items = _get_workflow_history_items_with_selected_participant_id(history)
    if workflow_history_items:
        participant_id = workflow_history_items[-1]['selected_participant_id']
        return participant_id


def _get_workflow_history_items_with_selected_participant_id(history):
    return [
        history_item for history_item in history if history_item.get('selected_participant_id')
    ]


def get_matched_participant_from_workflow_history(history):
    participant_id = get_participant_id(history)
    if participant_id:
        participants = get_participants_by_participant_ids([participant_id])
        if participants:
            return participants[0]


def build_update_object(update_attributes, result_id, received_time, rejected_reason=None):
    delete_attributes = []

    # If a rejected reason doesn't exist, we're accepting the match and so
    # we want to delete the workflow_state and secondary_status
    if not rejected_reason:
        update_attributes.pop('workflow_state', None)
        update_attributes.pop('secondary_status', None)

        delete_attributes = ['workflow_state', 'secondary_status']

    return {
        'record_keys': {
            'result_id': result_id,
            'received_time': received_time
        },
        'update_values': update_attributes,
        'delete_values': delete_attributes
    }


def validate_check_result(
        workflow_history, workflow_state, secondary_status, decoded_record, result_id, received_time):
    rejected_reason = None
    if workflow_history and workflow_state == ResultWorkflowState.CHECK:
        participant_record = get_matched_participant_from_workflow_history(workflow_history)
        if not participant_record:
            log({'log_reference': LogReference.MNGRESMTCH0020})
            return workflow_state, secondary_status, rejected_reason

        participant_id = participant_record['participant_id']
        if not decoded_record:
            decoded_record = get_result(result_id, received_time).get('decoded')

        if decoded_record:
            log({'log_reference': LogReference.MNGRESMTCH0017})
            rejected_reason = validate_matched_result(decoded_record, participant_id)
            if rejected_reason:
                workflow_state = ResultWorkflowState.REJECTED
                secondary_status = ResultSecondaryStatus.NOT_ACTIONED
                log({'log_reference': LogReference.MNGRESMTCH0018, 'reason': rejected_reason})
            else:
                log({'log_reference': LogReference.MNGRESMTCH0019})
        else:
            log({'log_reference': LogReference.MNGRESMTCH0021})

    return workflow_state, secondary_status, rejected_reason


def transaction_write_records(put_records, update_records):
    transact_items = [
        *put_records,
        *update_records,
    ]

    response = dynamodb_transact_write_items(transact_items)
    return response
