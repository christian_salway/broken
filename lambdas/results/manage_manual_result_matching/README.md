This lambda is for managing a result through the manual matching process. There are six endpoints:
- GET the list of results for a given lab.
- GET a specific result.
- POST to update a specific result.
- POST to lock and unlock a result record.
- POST to trigger the post-match validation process.