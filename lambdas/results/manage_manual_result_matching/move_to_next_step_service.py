from common.log import log
from common.models.result import ResultSecondaryStatus, ResultWorkflowState
from common.utils.dynamodb_access.operations import dynamodb_update_item
from common.utils.dynamodb_access.table_names import TableNames
from .log_references import LogReference

from common.utils.result_record_locking_utils import do_acquire_lock
from common.utils.dynamodb_helper_utils import build_update_query


@do_acquire_lock
def move_to_next_step(event_data):
    try:
        result_id = event_data['path_parameters']['result_id']
        received_time = event_data['body']['received_time']
        workflow_state = event_data['body'].get('workflow_state')

        secondary_status = get_next_secondary_status(workflow_state)
        if secondary_status:
            key = {
                'result_id': result_id,
                'received_time': received_time
            }

            attributes_to_update = {
                'secondary_status': secondary_status
            }

            update_expression, expression_names, expression_values = build_update_query(attributes_to_update)
            log({'log_reference': LogReference.MNGRESMTCH0015})
            return dynamodb_update_item(TableNames.RESULTS, dict(
                Key=key,
                UpdateExpression=update_expression,
                ExpressionAttributeValues=expression_values,
                ExpressionAttributeNames=expression_names,
                ReturnValues='NONE'
            ))
    except Exception as e:
        log({'log_reference': LogReference.MNGRESMTCH0016})
        raise e


def get_next_secondary_status(workflow_state):
    if workflow_state == ResultWorkflowState.PROCESS:
        return ResultSecondaryStatus.WITH_PROCESSOR

    if workflow_state == ResultWorkflowState.CHECK:
        return ResultSecondaryStatus.WITH_CHECKER
