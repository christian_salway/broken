from typing import Optional
from boto3.dynamodb.conditions import Key
from common.utils.dynamodb_helper_utils import build_update_query
from common.log import log
from common.utils.result_record_locking_utils import do_acquire_lock, release_lock
from manage_manual_result_matching.log_references import LogReference
from common.utils.audit_utils import audit, AuditActions
from common.utils.result_utils import (
    get_results_by_result_keys,
    get_result
)
from common.utils.match_result_utils import (
    pause_participant_for_lab_or_manually_added_result
)
from common.utils.participant_utils import (
    get_participants_by_participant_ids,
    get_participant_by_participant_id
)
from common.utils.sender_utils import get_sender_details_and_validate
from common.models.result import ResultAction
from manage_manual_result_matching.manage_manual_result_matching_utils import (
    extract_data_from_event,
    validate_check_result,
    generate_additional_information
)
from manage_manual_result_matching.accept_match_service import (
    accept_match
)
from manage_manual_result_matching.rejected_delete_service import delete_rejected_result
from common.utils.dynamodb_access.operations import dynamodb_query, dynamodb_update_item
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.dynamodb_access.segregated_operations import segregated_query

SENDING_LAB_GSI = 'sending-lab'

PROJECTION_EXPRESSION = ', '.join([
    'result_id',
    'sending_lab',
    'decoded',
    'workflow_state',
    'validation_errors',
    'received_time',
    'secondary_status',
    'workflow_history',
    'user_id',
    'user_name',
    'lock_timestamp',
    'possible_match_participants',
    'rejected_reason',
    'crm_number',
    'duplicates',
    'sanitised_nhais_cipher',
    'matched_to_participant'
])

PROJECTION_EXPRESSION_FOR_DUPLICATES = ', '.join([
    'result_id',
    'sending_lab',
    'decoded',
    'received_time',
    'workflow_state',
    'workflow_history',
    'rejected_reason',
    'sanitised_nhais_cipher'
])


@do_acquire_lock
def get_unmatched_result_with_participant_matches(event_data):
    log({'log_reference': LogReference.MNGRESMTCH0005})

    try:
        result_id = event_data['path_parameters']['result_id']
        condition_expression = Key('result_id').eq(result_id)

        response = dynamodb_query(TableNames.RESULTS, dict(
            KeyConditionExpression=condition_expression,
            ProjectionExpression=PROJECTION_EXPRESSION
        ))

        log({'log_reference': LogReference.MNGRESMTCH0006})

        audit(AuditActions.GET_UNMATCHED_RESULT, session=event_data.get('session'))
    except Exception as e:
        log({'log_reference': LogReference.MNGRESMTCH0007})
        raise e
    return get_participant_matches_for_result(response, event_data)


def get_duplicate_test_result_details(duplicate_result_keys):
    try:
        if not duplicate_result_keys:
            return

        log({'log_reference': LogReference.DUPRES0001})
        duplicate_results = get_results_by_result_keys(duplicate_result_keys, PROJECTION_EXPRESSION_FOR_DUPLICATES)
        for duplicate_result in duplicate_results:
            decoded = duplicate_result['decoded']
            cipher = duplicate_result['sanitised_nhais_cipher']
            organisation_code = decoded.get('sender_code')
            sender_details = get_sender_details_and_validate(
                cipher, organisation_code, decoded.get('source_code'), decoded.get('test_date'))

            if sender_details and not sender_details.get('message'):
                log({'log_reference': LogReference.DUPRES0002})
                # Update the decoded object of the duplicate result with the sender details
                duplicate_result['decoded'].update({
                    'sender_name': sender_details.get('name'),
                    'sender_address': sender_details.get('address'),
                    'organisation_code': organisation_code
                })
            else:
                duplicate_result['decoded'].update({
                    'sender': sender_details
                })
                log({'log_reference': LogReference.DUPRES0003})

            log({'log_reference': LogReference.DUPRES0004})

        return duplicate_results
    except Exception as e:
        log({'log_reference': LogReference.DUPRES0005})
        raise e


def get_participant_matches_for_result(results, event_data):
    if not results:
        return

    session_user_data = event_data['session']['user_data']
    user_name = f'{session_user_data["first_name"]} {session_user_data["last_name"]}'

    first_result = results[0]
    decoded_result = convert_to_api_format(first_result, user_name)

    duplicate_result_keys = first_result.get('duplicates')
    duplicate_results = get_duplicate_test_result_details(duplicate_result_keys)
    if duplicate_results:
        decoded_result.update({'duplicate_results': duplicate_results})

    response = {
        'result': decoded_result
    }

    if decoded_result.get('possible_match_participants'):
        response['participants'] = get_possible_match_participants(decoded_result['possible_match_participants'])

    if decoded_result.get('matched_to_participant'):
        response['matched_participant'] = get_participant_by_participant_id(decoded_result['matched_to_participant'])

    return response


def get_possible_match_participants(possible_match_participants):
    participant_ids = [participant['participant_id'] for participant in possible_match_participants]
    participants = get_participants_by_participant_ids(participant_ids)

    return map_participants(participants)


def map_participants(query_items):
    participants = []

    for query_item in query_items:
        participants.append({
            'participant_id': query_item['participant_id'],
            'nhs_number': query_item['nhs_number'],
            'title': query_item['title'],
            'first_name': query_item['first_name'],
            'middle_names': query_item.get('middle_names'),
            'last_name': query_item['last_name'],
            'date_of_birth': query_item['date_of_birth'],
            'gender': query_item['gender'],
            'address': query_item['address'],
            'is_ceased': query_item.get('is_ceased')
        })

    return participants


@do_acquire_lock
@release_lock
def update_unmatched_result(event_data):
    log({'log_reference': LogReference.MNGRESMTCH0008})

    try:
        result_id = event_data['path_parameters']['result_id']

        session_user_data = event_data['session']['user_data']
        user_name = f'{session_user_data["first_name"]} {session_user_data["last_name"]}'

        workflow_state, secondary_status, workflow_history, received_time, decoded_record = \
            extract_data_from_event(event_data)

        # Validate the result passed in if its workflow_state is "check"
        new_workflow_state, new_secondary_status, rejected_reason = validate_check_result(
            workflow_history, workflow_state, secondary_status, decoded_record, result_id, received_time
        )

        update_attributes = {
            'workflow_state': new_workflow_state,
            'secondary_status': new_secondary_status,
        }

        if rejected_reason:
            update_attributes['rejected_reason'] = rejected_reason

        action = None
        delete_attributes = []

        if workflow_history:
            for history in workflow_history:
                if len(history.get('user', '')) == 0:
                    history['user'] = user_name

            update_attributes.update({'workflow_history': workflow_history})
            action = workflow_history[-1].get('action')

        if action != ResultAction.CHECK_ACCEPT_ACTION.value and action != ResultAction.REJECTED_DELETE_ACTION.value:
            update_in_process_result(update_attributes, delete_attributes,
                                     result_id, received_time)

        elif action == ResultAction.CHECK_ACCEPT_ACTION.value:
            accept_match(event_data)

        elif action == ResultAction.REJECTED_DELETE_ACTION.value:
            delete_rejected_result(event_data)

        audit(
            action=AuditActions.UPDATE_UNMATCHED_RESULT,
            session=event_data.get('session'),
            additional_information=generate_additional_information(result_id, workflow_history)
        )
        log({'log_reference': LogReference.MNGRESMTCH0009})
    except Exception as e:
        log({'log_reference': LogReference.MNGRESMTCH0010})
        raise e
    return {'message': 'record updated'}


def update_in_process_result(update_attributes, delete_attributes, result_id, received_time):
    update_expression, expression_names, expression_values = build_update_query(
            update_attributes, delete_attributes=delete_attributes)

    dynamodb_update_item(TableNames.RESULTS, dict(
        Key={'result_id': result_id, 'received_time': received_time},
        UpdateExpression=update_expression,
        ExpressionAttributeValues=expression_values,
        ExpressionAttributeNames=expression_names,
        ReturnValues='NONE'
    ))


def get_unmatched_lab_results(event_data):
    try:
        sending_lab = event_data['path_parameters']['sending_lab']
        key_condition = Key('sending_lab').eq(sending_lab)
        log({'log_reference': LogReference.MNGRESMTCH0011})

        items = segregated_query(TableNames.RESULTS, dict(
            IndexName=SENDING_LAB_GSI,
            KeyConditionExpression=key_condition,
            ProjectionExpression=PROJECTION_EXPRESSION
        ))

        log({
            'log_reference': LogReference.MNGRESMTCH0014,
            'unmatched_result_count': len(items)
        })
    except Exception as e:
        log({'log_reference': LogReference.MNGRESMTCH0016})
        raise e
    return [convert_to_api_format(item) for item in items]


def convert_to_api_format(unmatched_lab_result, user_name=None):
    decoded_attributes = unmatched_lab_result.get('decoded')

    def safe_titleize_field(_decoded_attributes: dict, name: str) -> Optional[str]:
        attr = _decoded_attributes.get(name)

        if isinstance(attr, str):
            return attr.title()

        return None

    first_name = safe_titleize_field(decoded_attributes, 'first_name')
    last_name = safe_titleize_field(decoded_attributes, 'last_name')

    if first_name:
        decoded_attributes.update({'first_name': first_name})
    if last_name:
        decoded_attributes.update({'last_name': last_name})

    return {
        'result_id': unmatched_lab_result['result_id'],
        'sending_lab': unmatched_lab_result['sending_lab'],
        'decoded': unmatched_lab_result['decoded'],
        'workflow_state': unmatched_lab_result['workflow_state'],
        'validation_errors': unmatched_lab_result.get('validation_errors'),
        'secondary_status': unmatched_lab_result.get('secondary_status'),
        'workflow_history': unmatched_lab_result.get('workflow_history', []),
        'user_id': unmatched_lab_result.get('user_id'),
        'user_name': unmatched_lab_result.get('user_name') or user_name,
        'role_id': unmatched_lab_result.get('role_id'),
        'lock_timestamp': unmatched_lab_result.get('lock_timestamp'),
        'received_time': unmatched_lab_result['received_time'],
        'rejected_reason': unmatched_lab_result.get('rejected_reason'),
        'possible_match_participants': unmatched_lab_result.get('possible_match_participants'),
        'matched_to_participant': unmatched_lab_result.get('matched_to_participant')
    }


def pause_participant(event_data):
    result_id = event_data['path_parameters']['result_id']
    workflow_state, secondary_status, workflow_history, received_time, decoded_record = \
        extract_data_from_event(event_data)

    result = get_result(result_id, received_time)
    pause_participant_for_lab_or_manually_added_result(result['matched_participant_id'], result_id, True)
