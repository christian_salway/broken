from common.log import log
from common.utils.result_record_locking_utils import do_release_lock, LockException
from manage_manual_result_matching.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.utils import json_return_object, json_return_message
from common.utils.api_utils import get_api_request_info_from_event
from manage_manual_result_matching.manage_manual_result_matching_service import (
    get_unmatched_lab_results,
    get_unmatched_result_with_participant_matches,
    update_unmatched_result
)
from manage_manual_result_matching.move_to_next_step_service import move_to_next_step


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    base_url = '/api/csas/results'
    request_functions = {
        ('POST', base_url + '/{sending_lab}/{result_id}/lock'): move_to_next_step,
        ('POST', base_url + '/{sending_lab}/{result_id}/unlock'): do_release_lock,
        ('POST', base_url + '/{sending_lab}/{result_id}'): update_unmatched_result,
        ('GET', base_url + '/{sending_lab}'): get_unmatched_lab_results,
        ('GET', base_url + '/{sending_lab}/{result_id}'):
            get_unmatched_result_with_participant_matches,
    }
    event_data, http_method, resource = get_api_request_info_from_event(event)
    log({'log_reference': LogReference.MNGRESMTCH0001, 'http_method': http_method})

    try:
        response = request_functions.get((http_method, resource), bad_request)(event_data)
    except KeyError as e:
        log({'log_reference': LogReference.MNGRESMTCH0003, 'error': str(e)})
        return json_return_message(400, str(e))
    except LockException:
        log({'log_reference': LogReference.MNGRESMTCH0003, 'error': 'Failed to acquire or release record lock'})
        return json_return_message(409, 'Failed to acquire or release record lock')
    except Exception as e:
        log({'log_reference': LogReference.MNGRESMTCH0003, 'error': str(e)})
        return json_return_message(500, 'Cannot complete request')

    log({'log_reference': LogReference.MNGRESMTCH0002})

    if (response):
        return json_return_object(200, {'data': response})

    return json_return_object(200)


def bad_request(event_data):
    log({'log_reference': LogReference.MNGRESMTCH0004})
    raise Exception('unrecognised method/resource combination')
