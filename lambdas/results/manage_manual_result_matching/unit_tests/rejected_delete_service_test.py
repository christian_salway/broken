import datetime
from manage_manual_result_matching.log_references import LogReference
from mock import Mock, call, patch
from unittest import TestCase
from common.models.result import (
    ResultWorkflowState,
    ResultSecondaryStatus,
    ResultRejectionReason
)
from common.models.participant import ParticipantStatus


MODULE = 'manage_manual_result_matching.rejected_delete_service'
UTILS_MOUDLE = 'manage_manual_result_matching.manage_manual_result_matching_utils'

DELETE_REJECTED_EVENT_DATA = {
    'body': {
        'user_id': 'userid1',
        'user_name': 'Jim Smith',
        'workflow_state': ResultWorkflowState.REJECTED,
        'secondary_status': ResultSecondaryStatus.DUPLICATE_SLIDE,
        'rejected_reasion': ResultRejectionReason.CEASED_NO_CERVIX,
        'workflow_history': [
            {
                'step': 'reject',
                'action': 'delete'
            }
        ],
        'received_time': '14:44',
        'sending_lab': 'CHR3',
        'decoded': {
            'test_date': '2021-02-28',
            'nhs_number': '9999999999',
            'slide_number': '12345678',
            'action': 'Routine',
            'action_code': 'A',
            'recall_months': '06',
            'result': 'No Cytology test undertaken',
            'result_code': 'X',
            'infection_result': 'HPV negative',
            'infection_code': '0',
            'source_code': '1',
            'sender_code': '567',
            'status': ParticipantStatus.REPEAT_ADVISED,
            'address': 'Lab address',
            'self_sample': False,
            'hpv_primary': True
        }
    },
    'path_parameters': {
        'result_id': '1',
        'sending_lab': 'CHR3'
    },
    'session': {
        'user_data': {
            'nhsid_useruid': 'userid1',
            'first_name': 'Jim',
            'last_name': 'Smith'
        },
    }
}


TRANSACT_PUT_ITEM = {
                        'TableName': 'RESULTS',
                        "Item": {
                            "result_id": {
                                "S": "AUDITRESULT#2021-01-25T12:15:08.132000+00:00"
                            },
                            "received_time": {
                                "S": "2021-01-25T12:15:08.132000+00:00"
                            },
                            "sending_lab": {
                                "S": "CHR3"
                            },
                            "workflow_history": {
                                "L": [{"M": {"action": {"S": "delete"}}}]
                            },
                            'decoded': {
                                'M': {
                                    'nhs_number': {'S': '9999999999'}
                                }
                            }
                        }
                    }

TRANSACT_UPDATE_COMPLETE_ITEM = {
                        "TableName": "RESULTS",
                        "Key": {
                            "result_id": {
                                "S": "1"
                            },
                            "received_time": {
                                "S": "14:44"
                            }
                        },
                        "UpdateExpression": "SET #workflow_history = :workflow_history \
REMOVE #workflow_state, #secondary_status",
                        "ExpressionAttributeValues": {
                            ":workflow_history": {
                                "L": [
                                    {
                                        "M": {
                                            "step": {
                                                "S": "reject"
                                            },
                                            "action": {
                                                "S": "delete"
                                            },
                                            "user": {
                                                "S": "Jim Smith"
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        "ExpressionAttributeNames": {
                            "#workflow_history": "workflow_history",
                            "#workflow_state": "workflow_state",
                            "#secondary_status": "secondary_status"
                        }
                    }

mock_env_vars = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS',
    'DYNAMODB_RESULTS': 'RESULTS',
}


@patch('os.environ', mock_env_vars)
@patch(f'{UTILS_MOUDLE}.dynamodb_transact_write_items')
@patch(f'{MODULE}.log')
class TestRejectedDeleteService(TestCase):

    @patch('boto3.resource', Mock())
    def setUp(self):
        import manage_manual_result_matching.rejected_delete_service as _service_module
        self.service_module = _service_module

    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MOUDLE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MOUDLE}.validate_matched_result')
    @patch(f'{MODULE}.transaction_write_records')
    @patch(f'{MODULE}.datetime')
    def test_record_can_be_deleted(
            self, datetime_mock, dynamodb_client_mock, validate_matched_result_mock,
            query_participants_mock, audit_mock, log_mock, table_mock):

        # Arrange
        validate_matched_result_mock.return_value = 'rejected'
        datetime_mock.now.return_value = datetime.datetime(2021, 1, 25, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        query_participants_mock.return_value = [{
                'participant_id': '1234',
                'nhs_number': '1234567890',
                'is_ceased': True,
                'date_of_birth': '1990-01-01',
                'next_test_due_date': '2020-12-25'
        }]
        # Act
        self.service_module.delete_rejected_result(DELETE_REJECTED_EVENT_DATA)

        # Act
        dynamodb_client_mock.assert_called_with([
                {
                    'Put': TRANSACT_PUT_ITEM
                }
            ],
            [
                {
                    'Update': TRANSACT_UPDATE_COMPLETE_ITEM
                }
            ]
        )

        expected_log_calls = [call({'log_reference': LogReference.REJDELMTCH0001}),
                              call({'log_reference': LogReference.REJDELMTCH0005}),
                              call({'log_reference': LogReference.REJDELMTCH0003})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MOUDLE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MOUDLE}.validate_matched_result')
    @patch(f'{UTILS_MOUDLE}.dynamodb_transact_write_items')
    def test_deleting_record_fails(
            self, dynamodb_client_mock, validate_matched_result_mock,
            query_participants_mock, audit_mock, log_mock, table_mock):
        # Arrange
        validate_matched_result_mock.return_value = None

        # Act
        with self.assertRaises(Exception):
            self.service_module.delete_rejected_result({})

        expected_log_calls = [call({'log_reference': LogReference.REJDELMTCH0001}),
                              call({'log_reference': LogReference.REJDELMTCH0004})]

        log_mock.assert_has_calls(expected_log_calls)
