import os
import json
import datetime
from datetime import date
from dateutil.parser import parse
from unittest import TestCase
from common.utils.audit_utils import AuditActions, AuditUsers
from common.models.participant import CeaseReason, EpisodeStatus, ParticipantStatus
from common.models.letters import SupressionReason
from manage_manual_result_matching.log_references import LogReference
from mock import Mock, call, patch
from common.models.result import ResultWorkflowState, ResultSecondaryStatus
from common.log_references import CommonLogReference

MODULE = 'manage_manual_result_matching.accept_match_service'
UTILS_MODULE = 'manage_manual_result_matching.manage_manual_result_matching_utils'
RESULT_UTILS_MODULE = 'common.utils.result_utils'
MATCH_RESULT_UTILS_MODULE = 'common.utils.match_result_utils'

PROJECTION_EXPRESSION = ', '.join([
    'result_id',
    'sending_lab',
    'decoded',
    'workflow_state',
    'validation_errors',
    'received_time',
    'secondary_status',
    'workflow_history',
    'user_id',
    'user_name',
    'lock_timestamp',
    'possible_match_participants',
    'rejected_reason',
    'crm_number',
    'duplicates'
])

PROJECTION_EXPRESSION_FOR_DUPLICATES = ', '.join([
    'result_id',
    'sending_lab',
    'decoded',
    'received_time',
    'workflow_state',
    'workflow_history',
    'rejected_reason'
])

ACCEPT_MATCH_EVENT_DATA = {
    'body': {
        'user_id': 'userid1',
        'user_name': 'Jim Smith',
        'workflow_state': ResultWorkflowState.CHECK,
        'secondary_status': ResultSecondaryStatus.WITH_CHECKER,
        'workflow_history': [
            {
                'action': "check",
                'comment': "Some comment from the processor",
                'selected_participant_id': "1234",
                'selection': "Send to lab address",
                'step': "process",
                'timestamp': "2021-03-22T09:32:26.891Z",
                'user': "T Processor",
            },
            {
                'step': 'check',
                'action': 'accept'
            }
        ],
        'received_time': '14:44',
        'decoded': {
            'test_date': '2021-02-28',
            'date_of_birth': '1980-01-01',
            'slide_number': '12345678',
            'action': 'Routine',
            'action_code': 'A',
            'recall_months': '06',
            'result': 'No Cytology test undertaken',
            'result_code': 'X',
            'infection_result': 'HPV negative',
            'infection_code': '0',
            'source_code': 'G',
            'sender_source_type': '1',
            'sender_code': '567',
            'status': ParticipantStatus.REPEAT_ADVISED,
            'address': '45 BARROW DITCHE LEIGHTON BUZZARD BEDS LU8 3EH',
            'sanitised_lab_address': {
                'address_line_1': '45 BARROW DITCHE LEIGHTON BUZZARD',
                'address_line_2': 'BEDS',
                'address_line_3': '',
                'address_line_4': '',
                'address_line_5': '',
                'postcode': 'LU83EH'
            },
            'self_sample': False,
            'hpv_primary': True
        }
    },
    'path_parameters': {
        'result_id': '1'
    },
    'session': {
        'user_data': {
            'nhsid_useruid': 'userid1',
            'first_name': 'Jim',
            'last_name': 'Smith'
        },
    }
}

QUERY_EPISODE_MOCK = [{
    'sort_key': 'EPISODE#',
    'live_record_status': EpisodeStatus.WALKIN.value
},
    {
    'sort_key': 'EPISODE#',
    'live_record_status': EpisodeStatus.OTHER.value
}]


PARTICIPANT_2 = [{
    'participant_id': 'test_participant',
    'nhs_number': '9876543210',
    'title': 'Miss',
    'first_name': 'Camila',
    'middle_names': 'Marta',
    'last_name': 'Cambello',
    'date_of_birth': '1997-03-03',
    'gender': 2,
    'address': 'system address'
}]

RESULT_MOCK = {
    'Item': {
        'result_id': '0',
        'decoded': {
            'test_date': date.today().strftime('%Y-%m-%d'),
            'date_of_birth': '1980-01-01',
            'source_code': 'H',
            'sender_source_type': '2',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'A',
            'hpv_primary': True
        }
    }
}
RESULT_MOCK_SENDER_SOURCE_TYPE_1 = {
    'Item': {
        'result_id': '0',
        'decoded': {
            'test_date': date.today().strftime('%Y-%m-%d'),
            'date_of_birth': '1980-01-01',
            'source_code': 'G',
            'sender_source_type': '1',
            'result_code': 'X',
            'infection_code': '0',
            'action_code': 'A',
            'hpv_primary': True
        }
    }
}
TRANSACT_PUT_ITEM = {
    'TableName': 'PARTICIPANTS',
    'Item': {
        'participant_id': {'S': '1'},
        'sort_key': {'S': 'RESULT#2021-02-28#2021-01-25T12:15:08.132000+00:00'},
        'result_id': {'S': '1'},
        'nhs_number': {'S': '1234567890'},
        'sanitised_nhs_number': {'S': '1234567890'},
        'slide_number': {'S': '12345678'},
        'created': {'S': '2021-01-25T12:15:08.132000+00:00'},
        'test_date': {'S': '2021-02-28'},
        'result_date': {'S': '2021-01-25T12:15:08.132000+00:00'},
        'next_test_due_date': {'S': '2024-02-28'},
        'action': {'S': 'Routine'},
        'action_code': {'S': 'A'},
        'result': {'S': 'No Cytology test undertaken'},
        'result_code': {'S': 'X'},
        'infection_result': {'S': 'HPV negative'},
        'infection_code': {'S': '0'},
        'source_code': {'S': 'G'},
        'sender_source_type': {'S': '1'},
        'sender_code': {'S': '567'},
        'self_sample': {'BOOL': False},
        'status': {'S': ParticipantStatus.REPEAT_ADVISED},
        'letter_address': {'M': {
            'address_line_1': {'S': '45 BARROW DITCHE LEIGHTON BUZZARD'},
            'address_line_2': {'S': 'BEDS'},
            'address_line_3': {'S': ''},
            'address_line_4': {'S': ''},
            'address_line_5': {'S': ''},
            'postcode': {'S': 'LU83EH'}
        }},
        'hpv_primary': {'BOOL': True}
    }
}

TRANSACT_UPDATE_COMPLETE_ITEM = {
    'TableName': 'PARTICIPANTS',
    'Key': {
        'participant_id': {'S': '1'},
        'sort_key': {'S': 'EPISODE#'}
    },
    'UpdateExpression': 'SET #result_date = :result_date, #result_id = :result_id, ' +
    '#status = :status REMOVE #live_record_status',
                        'ExpressionAttributeValues': {
                            ':result_date': {'S': '14:44'},
                            ':result_id': {'S': '1'},
                            ':status': {'S': EpisodeStatus.COMPLETED.value}
    },
    'ExpressionAttributeNames': {
                            '#result_date': 'result_date',
                            '#result_id': 'result_id',
                            '#status': 'status',
                            '#live_record_status': 'live_record_status'
    }
}

TRANSACT_UPDATE_CLOSED_ITEM = {
    'TableName': 'PARTICIPANTS',
    'Key': {
        'participant_id': {'S': '1'},
        'sort_key': {'S': 'EPISODE#'}
    },
    'UpdateExpression': 'SET #result_date = :result_date, #result_id = :result_id, ' +
    '#status = :status REMOVE #live_record_status',
    'ExpressionAttributeValues': {
        ':result_date': {'S': '14:44'},
        ':result_id': {'S': '1'},
        ':status': {'S': f'{EpisodeStatus.CLOSED}'}
    },
    'ExpressionAttributeNames': {
        '#result_date': 'result_date',
        '#result_id': 'result_id',
        '#status': 'status',
        '#live_record_status': 'live_record_status'
    }
}


mock_env_vars = {
    'DYNAMODB_PARTICIPANTS': 'PARTICIPANTS',
    'DYNAMODB_RESULTS': 'RESULTS',
}


@patch('os.environ', mock_env_vars)
@patch(f'{RESULT_UTILS_MODULE}.dynamodb_update_item')
@patch(f'{MODULE}.log')
class TestAcceptMatchService(TestCase):

    @patch('boto3.resource', Mock())
    def setUp(self):
        import manage_manual_result_matching.accept_match_service as _service_module
        self.service_module = _service_module

    @patch(f'{MODULE}.autocease_if_eligible')
    @patch(f'{UTILS_MODULE}.get_result')
    @patch(f'{UTILS_MODULE}.dynamodb_transact_write_items')
    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{MODULE}.datetime')
    @patch(f'{MODULE}.query_for_live_episode')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{MODULE}.add_result_letter_to_processing_queue')
    @patch(f'{MODULE}.generate_reinstate_participant_transactions')
    @patch(f'{MODULE}.calculate_and_update_ntdd')
    @patch(f'{MODULE}.participant_eligible_for_auto_cease')
    @patch(f'{UTILS_MODULE}.log')
    @patch('common.utils.result_letter_utils.check_for_result_letter_suppress_reason')
    @patch('common.utils.result_letter_utils.log')
    @patch('common.utils.match_result_utils.log', Mock())
    def test_accept_match_succeeds_with_lab_address(
            self, result_utils_log_mock, check_letter_mock, utils_log_mock, is_eligible_mock, ntdd_mock,
            reinstate_participant_mock, add_result_letter_to_processing_queue_mock, validate_matched_mock,
            query_participants_mock, query_episode_mock, datetime_mock,
            participant_workflow_mock, audit_mock, dynamodb_client_mock,
            get_result_mock, autocease_mock, log_mock, dynamodb_update_mock):
        # Arrange
        datetime_mock.now.return_value = datetime.datetime(2021, 1, 25, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        participant_workflow_mock.return_value = {
            'participant_id': '1',
            'nhs_number': '1234567890',
            'is_ceased': True,
            'date_of_birth': '1990-01-01',
            'next_test_due_date': '2020-12-25'
        }
        query_episode_mock.return_value = QUERY_EPISODE_MOCK

        get_result_mock.return_value = RESULT_MOCK_SENDER_SOURCE_TYPE_1
        is_eligible_mock.return_value = False
        autocease_mock.return_value = False
        check_letter_mock.return_value = None

        query_participants_mock.return_value = [{
            'participant_id': '1',
            'nhs_number': '1234567890',
            'is_ceased': True,
            'date_of_birth': '1990-01-01',
            'next_test_due_date': '2020-12-25'
        }]
        validate_matched_mock.return_value = None
        reinstate_participant_mock.return_value = None, None, None
        accept_match_event_data = self._read_json_file('accept_match_send_to_lab_address.json')
        ntdd_mock.return_value = '2022-01-01'

        # Act
        self.service_module.accept_match(accept_match_event_data)

        # Assert
        dynamodb_client_mock.assert_called_with(
            [
                {
                    'Put': TRANSACT_PUT_ITEM
                },
                {
                    'Update': TRANSACT_UPDATE_COMPLETE_ITEM
                },
                {
                    'Update': TRANSACT_UPDATE_CLOSED_ITEM
                },
                {
                    'Update': {
                        'TableName': 'RESULTS',
                        'Key': {
                            'result_id': {'S': '1'},
                            'received_time': {'S': '14:44'}
                        },
                        'UpdateExpression': 'SET #workflow_history = :workflow_history, ' +
                                            '#matched_time = :matched_time, ' +
                                            '#matched_to_participant = :matched_to_participant ' +
                                            'REMOVE #workflow_state, #secondary_status',
                        'ExpressionAttributeValues': {
                            ':workflow_history': {
                                'L': [
                                    {'M': {
                                            'action': {'S': 'check'},
                                            'comment': {'S': 'Some comment from the processor'},
                                            'selected_participant_id': {'S': '1234'},
                                            'selection': {'S': 'Send to lab address'},
                                            'step': {'S': 'process'},
                                            'timestamp': {'S': '2021-03-22T09:32:26.891Z'},
                                            'user': {'S': 'T Processor'},
                                    }},
                                    {'M': {
                                        'step': {'S': 'check'},
                                        'action': {'S': 'accept'},
                                        'user': {'S': 'Jim Smith'}
                                    }}
                                ]
                            },
                            ':matched_time': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            ':matched_to_participant': {'S': '1'}
                        },
                        'ExpressionAttributeNames': {
                            '#workflow_history': 'workflow_history',
                            '#matched_time': 'matched_time',
                            '#matched_to_participant': 'matched_to_participant',
                            '#workflow_state': 'workflow_state',
                            '#secondary_status': 'secondary_status'
                        }
                    }
                }
            ])
        dynamodb_update_mock.assert_not_called()

        add_result_letter_to_processing_queue_mock.assert_called()

        audit_mock.assert_has_calls(
            [call(action=AuditActions.MANUAL_MATCH_RESULT,
                  additional_information={'slide_number': '12345678', 'test_date': '2021-02-28', 'action': 'Routine',
                                          'action_code': 'A', 'result': 'No Cytology test undertaken',
                                          'result_code': 'X', 'infection_result': 'HPV negative',
                                          'infection_code': '0', 'source_code': 'G', 'sender_source_type': '1',
                                          'sender_code': '567',
                                          'lab_address': '45 BARROW DITCHE LEIGHTON BUZZARD BEDS LU8 3EH',
                                          'updating_next_test_due_date_from': '2020-12-25',
                                          'updating_next_test_due_date_to': '2022-01-01'},
                  nhs_numbers=['1234567890'],
                  participant_ids=['1'],
                  session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}}),
             call(action=AuditActions.UPDATE_UNMATCHED_RESULT,
                  additional_information={'result_id': '1', 'selection': 'Send to lab address', 'participant': '1234'},
                  session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}})]
        )
        autocease_mock.assert_called_once_with({'participant_id': '1', 'nhs_number': '1234567890', 'is_ceased': True,
                                                'date_of_birth': '1990-01-01', 'next_test_due_date': '2022-01-01',
                                                'letter_address':
                                                    {'address_line_1': '45 BARROW DITCHE LEIGHTON BUZZARD',
                                                     'address_line_2': 'BEDS',
                                                     'address_line_3': '',
                                                     'address_line_4': '',
                                                     'address_line_5': '',
                                                     'postcode': 'LU83EH'}
                                                })

        expected_log_calls = [call({'log_reference': LogReference.ACCPTMTCH0001}),
                              call({'log_reference': LogReference.ACCPTMTCH0004}),
                              call({'log_reference': LogReference.ACCPTMTCH0005}),
                              call({'log_reference': LogReference.EPISODE0001}),
                              call({'log_reference': LogReference.EPISODE0002}),
                              call({'log_reference': LogReference.ACCPTMTCH0011}),
                              call({'log_reference': LogReference.ACCPTMTCH0009})]

        expected_utils_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0017}),
                                    call({'log_reference': LogReference.MNGRESMTCH0019})]

        log_mock.assert_has_calls(expected_log_calls)
        utils_log_mock.assert_has_calls(expected_utils_log_calls)

    @patch(f'{MODULE}.autocease_if_eligible')
    @patch(f'{UTILS_MODULE}.get_result')
    @patch(f'{UTILS_MODULE}.dynamodb_transact_write_items')
    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{MODULE}.datetime')
    @patch(f'{MODULE}.query_for_live_episode')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{MODULE}.add_result_letter_to_processing_queue')
    @patch(f'{MODULE}.generate_reinstate_participant_transactions')
    @patch(f'{MODULE}.calculate_and_update_ntdd')
    @patch(f'{MODULE}.participant_eligible_for_auto_cease')
    @patch(f'{UTILS_MODULE}.log')
    @patch('common.utils.result_letter_utils.check_for_result_letter_suppress_reason')
    @patch('common.utils.result_letter_utils.log')
    @patch('common.utils.match_result_utils.log', Mock())
    def test_accept_match_succeeds_with_system_address(
            self, result_utils_log_mock, check_letter_mock, utils_log_mock, is_eligible_mock, ntdd_mock,
            reinstate_participant_mock, add_result_letter_to_processing_queue_mock, validate_matched_mock,
            query_participants_mock, query_episode_mock, datetime_mock, participant_workflow_mock,
            audit_mock, dynamodb_client_mock, get_result_mock, autocease_mock, log_mock, table_mock):
        datetime_mock.now.return_value = datetime.datetime(2021, 1, 25, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        participant_workflow_mock.return_value = {
            'participant_id': '1',
            'nhs_number': '1234567890',
            'date_of_birth': '1990-01-01',
            'address': 'System address',
            'next_test_due_date': '2020-12-25'
        }

        query_episode_mock.return_value = QUERY_EPISODE_MOCK

        participant_items = [{
            'participant_id': '1',
            'nhs_number': '1234567890',
            'date_of_birth': '1990-01-01',
            'address': 'System address',
            'next_test_due_date': '2020-12-25'
        }]

        get_result_mock.return_value = RESULT_MOCK

        is_eligible_mock.return_value = False
        autocease_mock.return_value = False
        query_participants_mock.return_value = participant_items
        validate_matched_mock.return_value = None
        reinstate_participant_mock.return_value = None, None, None
        check_letter_mock.return_value = None
        add_result_letter_to_processing_queue_mock.return_value = None
        accept_match_event_data = self._read_json_file('accept_match_send_to_system_address.json')
        ntdd_mock.return_value = parse('2022-01-01')

        # Act
        self.service_module.accept_match(accept_match_event_data)

        # Assert
        dynamodb_client_mock.assert_called_with(
            [
                {
                    'Put': {
                        'TableName': 'PARTICIPANTS',
                        'Item': {
                            'participant_id': {'S': '1'},
                            'sort_key': {'S': 'RESULT#2021-02-28#2021-01-25T12:15:08.132000+00:00'},
                            'result_id': {'S': '1'},
                            'nhs_number': {'S': '1234567890'},
                            'sanitised_nhs_number': {'S': '1234567890'},
                            'slide_number': {'S': '12345678'},
                            'created': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            'test_date': {'S': '2021-02-28'},
                            'result_date': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            'next_test_due_date': {'S': '2024-02-28'},
                            'action': {'S': 'Routine'},
                            'action_code': {'S': 'A'},
                            'result': {'S': 'No Cytology test undertaken'},
                            'result_code': {'S': 'X'},
                            'infection_result': {'S': 'HPV negative'},
                            'infection_code': {'S': '0'},
                            'source_code': {'S': 'G'},
                            'sender_source_type': {'S': '1'},
                            'sender_code': {'S': '567'},
                            'self_sample': {'BOOL': False},
                            'status': {'S': ParticipantStatus.REPEAT_ADVISED},
                            'letter_address': {'S': 'System address'},
                            'hpv_primary': {'BOOL': True},
                        }
                    }
                },
                {
                    'Update': TRANSACT_UPDATE_COMPLETE_ITEM
                },
                {
                    'Update': TRANSACT_UPDATE_CLOSED_ITEM
                },
                {
                    'Update': {
                        'TableName': 'RESULTS',
                        'Key': {
                            'result_id': {'S': '1'},
                            'received_time': {'S': '14:44'}
                        },
                        'UpdateExpression': 'SET #workflow_history = :workflow_history, ' +
                                            '#matched_time = :matched_time, ' +
                                            '#matched_to_participant = :matched_to_participant ' +
                                            'REMOVE #workflow_state, #secondary_status',
                        'ExpressionAttributeValues': {
                            ':workflow_history': {
                                'L': [
                                    {'M': {
                                            'action': {'S': 'check'},
                                            'comment': {'S': 'Some comment from the processor'},
                                            'selected_participant_id': {'S': '1234'},
                                            'selection': {'S': 'Send to system address'},
                                            'step': {'S': 'process'},
                                            'timestamp': {'S': '2021-03-22T09:32:26.891Z'},
                                            'user': {'S': 'T Processor'},
                                    }},
                                    {'M': {
                                        'step': {'S': 'check'},
                                        'action': {'S': 'accept'},
                                        'user': {'S': 'Jim Smith'}
                                    }}
                                ]
                            },
                            ':matched_time': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            ':matched_to_participant': {'S': '1'}
                        },
                        'ExpressionAttributeNames': {
                            '#workflow_history': 'workflow_history',
                            '#matched_time': 'matched_time',
                            '#matched_to_participant': 'matched_to_participant',
                            '#workflow_state': 'workflow_state',
                            '#secondary_status': 'secondary_status'
                        }
                    }
                }
            ]
        )
        table_mock.update_item.assert_not_called()

        add_result_letter_to_processing_queue_mock.assert_called_once_with(
            {
                'participant_id': '1', 'sort_key': 'RESULT#2021-02-28#2021-01-25T12:15:08.132000+00:00',
                'result_id': '1', 'nhs_number': '1234567890', 'sanitised_nhs_number': '1234567890',
                'slide_number': '12345678', 'created': '2021-01-25T12:15:08.132000+00:00', 'test_date': '2021-02-28',
                'result_date': '2021-01-25T12:15:08.132000+00:00', 'next_test_due_date': '2024-02-28',
                'action': 'Routine', 'action_code': 'A', 'result': 'No Cytology test undertaken',
                'result_code': 'X', 'infection_result': 'HPV negative', 'infection_code': '0', 'source_code': 'G',
                'sender_source_type': '1', 'sender_code': '567', 'self_sample': False,
                'status': ParticipantStatus.REPEAT_ADVISED, 'letter_address': 'System address', 'hpv_primary': True
            }, False)

        audit_mock.assert_has_calls([
            call(action=AuditActions.MANUAL_MATCH_RESULT,
                 additional_information={'slide_number': '12345678', 'test_date': '2021-02-28', 'action': 'Routine',
                                         'action_code': 'A', 'result': 'No Cytology test undertaken',
                                         'result_code': 'X', 'infection_result': 'HPV negative', 'infection_code': '0',
                                         'source_code': 'G', 'sender_source_type': '1', 'sender_code': '567',
                                         'lab_address': 'Lab address', 'updating_next_test_due_date_from': '2020-12-25',
                                         'updating_next_test_due_date_to': '2022-01-01 00:00:00'},
                 nhs_numbers=['1234567890'],
                 participant_ids=['1'],
                 session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}}),
            call(action=AuditActions.UPDATE_UNMATCHED_RESULT,
                 additional_information={'result_id': '1', 'selection': 'Send to system address',
                                         'participant': '1234'},
                 session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}})])

        expected_log_calls = [call({'log_reference': LogReference.ACCPTMTCH0001}),
                              call({'log_reference': LogReference.ACCPTMTCH0004}),
                              call({'log_reference': LogReference.ACCPTMTCH0005}),
                              call({'log_reference': LogReference.EPISODE0001}),
                              call({'log_reference': LogReference.EPISODE0002}),
                              call({'log_reference': LogReference.ACCPTMTCH0011}),
                              call({'log_reference': LogReference.ACCPTMTCH0009})]

        expected_utils_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0017}),
                                    call({'log_reference': LogReference.MNGRESMTCH0019})]

        log_mock.assert_has_calls(expected_log_calls)
        utils_log_mock.assert_has_calls(expected_utils_log_calls)

    @patch(f'{MODULE}.autocease_if_eligible')
    @patch(f'{UTILS_MODULE}.get_result')
    @patch(f'{UTILS_MODULE}.dynamodb_transact_write_items')
    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{MODULE}.datetime')
    @patch(f'{MODULE}.query_for_live_episode')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{MODULE}.add_result_letter_to_processing_queue')
    @patch(f'{MODULE}.generate_reinstate_participant_transactions')
    @patch(f'{MODULE}.calculate_and_update_ntdd')
    @patch(f'{MODULE}.participant_eligible_for_auto_cease')
    @patch(f'{UTILS_MODULE}.log')
    @patch('common.utils.result_letter_utils.check_for_result_letter_suppress_reason')
    @patch('common.utils.result_letter_utils.log')
    @patch('common.utils.match_result_utils.log', Mock())
    def test_accept_match_succeeds_with_sender_address(
            self, result_utils_log_mock, check_letter_mock, utils_log_mock, is_eligible_mock, ntdd_mock,
            reinstate_participant_mock, add_result_letter_to_processing_queue_mock, validate_matched_mock,
            query_participants_mock, query_episode_mock, datetime_mock,
            participant_workflow_mock, audit_mock, dynamodb_client_mock,
            get_result_mock, autocease_mock, log_mock, table_mock):
        datetime_mock.now.return_value = datetime.datetime(2021, 1, 25, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        participant_workflow_mock.return_value = {
            'participant_id': '1',
            'nhs_number': '1234567890',
            'address': 'System address',
            'date_of_birth': '1990-01-01',
            'next_test_due_date': '2017-01-03'
        }

        query_episode_mock.return_value = QUERY_EPISODE_MOCK

        participant_items = [{
            'participant_id': '1',
            'nhs_number': '1234567890',
            'address': 'System address',
            'date_of_birth': '1990-01-01',
            'next_test_due_date': '2017-01-03'
        }]

        get_result_mock.return_value = RESULT_MOCK

        is_eligible_mock.return_value = False
        autocease_mock.return_value = False
        query_participants_mock.return_value = participant_items
        validate_matched_mock.return_value = None
        check_letter_mock.return_value = None
        accept_match_event_data = self._read_json_file('accept_match_send_to_sender_address.json')
        reinstate_participant_mock.return_value = None, None, None
        ntdd_mock.return_value = date(2022, 1, 1)

        # Act
        self.service_module.accept_match(accept_match_event_data)

        # Assert
        dynamodb_client_mock.assert_called_with(
            [
                {
                    'Put': {
                        'TableName': 'PARTICIPANTS',
                        'Item': {
                            'participant_id': {'S': '1'},
                            'sort_key': {'S': 'RESULT#2021-02-28#2021-01-25T12:15:08.132000+00:00'},
                            'result_id': {'S': '1'},
                            'nhs_number': {'S': '1234567890'},
                            'sanitised_nhs_number': {'S': '1234567890'},
                            'slide_number': {'S': '12345678'},
                            'created': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            'test_date': {'S': '2021-02-28'},
                            'result_date': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            'next_test_due_date': {'S': '2024-02-28'},
                            'action': {'S': 'Routine'},
                            'action_code': {'S': 'A'},
                            'result': {'S': 'No Cytology test undertaken'},
                            'result_code': {'S': 'X'},
                            'infection_result': {'S': 'HPV negative'},
                            'infection_code': {'S': '0'},
                            'source_code': {'S': 'G'},
                            'sender_source_type': {'S': '1'},
                            'sender_code': {'S': '567'},
                            'self_sample': {'BOOL': False},
                            'hpv_primary': {'BOOL': True},
                            'status': {'S': ParticipantStatus.REPEAT_ADVISED},
                            'letter_address': {'S': 'Sender address'}
                        }
                    }
                },
                {
                    'Update': TRANSACT_UPDATE_COMPLETE_ITEM
                },
                {
                    'Update': TRANSACT_UPDATE_CLOSED_ITEM
                },
                {
                    'Update': {
                        'TableName': 'RESULTS',
                        'Key': {
                            'result_id': {'S': '1'},
                            'received_time': {'S': '14:44'}
                        },
                        'UpdateExpression': 'SET #workflow_history = :workflow_history, ' +
                                            '#matched_time = :matched_time, ' +
                                            '#matched_to_participant = :matched_to_participant ' +
                                            'REMOVE #workflow_state, #secondary_status',
                        'ExpressionAttributeValues': {
                            ':workflow_history': {
                                'L': [
                                    {'M': {
                                            'action': {'S': 'check'},
                                            'comment': {'S': 'Some comment from the processor'},
                                            'selected_participant_id': {'S': '1234'},
                                            'selection': {'S': 'Send to sender address'},
                                            'step': {'S': 'process'},
                                            'timestamp': {'S': '2021-03-22T09:32:26.891Z'},
                                            'user': {'S': 'T Processor'},
                                    }},
                                    {'M': {
                                        'step': {'S': 'check'},
                                        'action': {'S': 'accept'},
                                        'user': {'S': 'Jim Smith'}
                                    }}
                                ]
                            },
                            ':matched_time': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            ':matched_to_participant': {'S': '1'}
                        },
                        'ExpressionAttributeNames': {
                            '#workflow_history': 'workflow_history',
                            '#matched_time': 'matched_time',
                            '#matched_to_participant': 'matched_to_participant',
                            '#workflow_state': 'workflow_state',
                            '#secondary_status': 'secondary_status'
                        }
                    }
                }
            ])
        table_mock.update_item.assert_not_called()

        add_result_letter_to_processing_queue_mock.assert_called_once_with(
            {
                'participant_id': '1', 'sort_key': 'RESULT#2021-02-28#2021-01-25T12:15:08.132000+00:00',
                'result_id': '1', 'nhs_number': '1234567890', 'sanitised_nhs_number': '1234567890',
                'slide_number': '12345678', 'created': '2021-01-25T12:15:08.132000+00:00', 'test_date': '2021-02-28',
                'result_date': '2021-01-25T12:15:08.132000+00:00', 'next_test_due_date': '2024-02-28',
                'action': 'Routine', 'action_code': 'A', 'result': 'No Cytology test undertaken',
                'result_code': 'X', 'infection_result': 'HPV negative', 'infection_code': '0',
                'source_code': 'G', 'sender_source_type': '1', 'sender_code': '567', 'self_sample': False,
                'status': ParticipantStatus.REPEAT_ADVISED,
                'letter_address': 'Sender address', 'hpv_primary': True
            }, False)

        audit_mock.assert_has_calls([
            call(action=AuditActions.MANUAL_MATCH_RESULT,
                 additional_information={'slide_number': '12345678', 'test_date': '2021-02-28', 'action': 'Routine',
                                         'action_code': 'A', 'result': 'No Cytology test undertaken',
                                         'result_code': 'X', 'infection_result': 'HPV negative', 'infection_code': '0',
                                         'source_code': 'G', 'sender_source_type': '1', 'sender_code': '567',
                                         'lab_address': 'Lab address',
                                         'updating_next_test_due_date_from': '2017-01-03',
                                         'updating_next_test_due_date_to': '2022-01-01'},
                 nhs_numbers=['1234567890'],
                 participant_ids=['1'],
                 session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}}),
            call(action=AuditActions.UPDATE_UNMATCHED_RESULT,
                 additional_information={'result_id': '1', 'selection': 'Send to sender address',
                                         'participant': '1234'},
                 session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}})])

        autocease_mock.assert_called_once_with({'participant_id': '1', 'nhs_number': '1234567890',
                                                'address': 'System address', 'date_of_birth': '1990-01-01',
                                                'next_test_due_date': '2022-01-01', 'letter_address': 'Sender address'})

        expected_log_calls = [call({'log_reference': LogReference.ACCPTMTCH0001}),
                              call({'log_reference': LogReference.ACCPTMTCH0004}),
                              call({'log_reference': LogReference.ACCPTMTCH0005}),
                              call({'log_reference': LogReference.EPISODE0001}),
                              call({'log_reference': LogReference.EPISODE0002}),
                              call({'log_reference': LogReference.ACCPTMTCH0011}),
                              call({'log_reference': LogReference.ACCPTMTCH0009})]

        expected_utils_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0017}),
                                    call({'log_reference': LogReference.MNGRESMTCH0019})]

        log_mock.assert_has_calls(expected_log_calls)
        utils_log_mock.assert_has_calls(expected_utils_log_calls)

    @patch(f'{MODULE}.autocease_if_eligible')
    @patch(f'{UTILS_MODULE}.get_result')
    @patch(f'{UTILS_MODULE}.dynamodb_transact_write_items')
    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{MODULE}.datetime')
    @patch(f'{MODULE}.query_for_live_episode')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{MODULE}.add_result_letter_to_processing_queue')
    @patch(f'{MODULE}.generate_reinstate_participant_transactions')
    @patch(f'{MODULE}.calculate_and_update_ntdd')
    @patch(f'{MODULE}.participant_eligible_for_auto_cease')
    @patch(f'{UTILS_MODULE}.log')
    @patch('common.utils.result_letter_utils.check_for_result_letter_suppress_reason')
    @patch('common.utils.result_letter_utils.log')
    @patch('common.utils.match_result_utils.log', Mock())
    def test_accept_match_succeeds_with_new_address(
            self, result_utils_log_mock, check_letter_mock, utils_log_mock, is_eligible_mock, ntdd_mock,
            reinstate_participant_mock, add_result_letter_to_processing_queue_mock, validate_matched_mock,
            query_participants_mock, query_episode_mock, datetime_mock,
            participant_workflow_mock, audit_mock, dynamodb_client_mock,
            get_result_mock, autocease_mock, log_mock, table_mock):
        datetime_mock.now.return_value = datetime.datetime(2021, 1, 25, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        participant_workflow_mock.return_value = {
            'participant_id': '1',
            'nhs_number': '1234567890',
            'date_of_birth': '1990-01-01',
            'next_test_due_date': 'the_starting_ntdd'
        }

        query_episode_mock.return_value = QUERY_EPISODE_MOCK

        participant_items = [{
            'participant_id': '1',
            'nhs_number': '1234567890',
            'date_of_birth': '1990-01-01',
            'next_test_due_date': 'the_starting_ntdd'
        }]

        get_result_mock.return_value = RESULT_MOCK_SENDER_SOURCE_TYPE_1

        is_eligible_mock.return_value = False
        query_participants_mock.return_value = participant_items
        validate_matched_mock.return_value = None
        check_letter_mock.return_value = None
        accept_match_event_data = self._read_json_file('accept_match_send_to_new_address.json')
        reinstate_participant_mock.return_value = None, None, None
        ntdd_mock.return_value = date(2022, 1, 1)

        # Act
        self.service_module.accept_match(accept_match_event_data)

        # Assert
        dynamodb_client_mock.assert_called_with(
            [
                {
                    'Put': {
                        'TableName': 'PARTICIPANTS',
                        'Item': {
                            'participant_id': {'S': '1'},
                            'sort_key': {'S': 'RESULT#2021-02-28#2021-01-25T12:15:08.132000+00:00'},
                            'result_id': {'S': '1'},
                            'nhs_number': {'S': '1234567890'},
                            'sanitised_nhs_number': {'S': '1234567890'},
                            'slide_number': {'S': '12345678'},
                            'created': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            'test_date': {'S': '2021-02-28'},
                            'result_date': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            'next_test_due_date': {'S': '2024-02-28'},
                            'action': {'S': 'Routine'},
                            'action_code': {'S': 'A'},
                            'result': {'S': 'No Cytology test undertaken'},
                            'result_code': {'S': 'X'},
                            'infection_result': {'S': 'HPV negative'},
                            'infection_code': {'S': '0'},
                            'source_code': {'S': 'G'},
                            'sender_source_type': {'S': '1'},
                            'sender_code': {'S': '567'},
                            'self_sample': {'BOOL': False},
                            'hpv_primary': {'BOOL': True},
                            'status': {'S': ParticipantStatus.REPEAT_ADVISED},
                            'letter_address': {'S': 'New address'}
                        }
                    }
                },
                {
                    'Update': TRANSACT_UPDATE_COMPLETE_ITEM
                },
                {
                    'Update': TRANSACT_UPDATE_CLOSED_ITEM
                },
                {
                    'Update': {
                        'TableName': 'RESULTS',
                        'Key': {
                            'result_id': {'S': '1'},
                            'received_time': {'S': '14:44'}
                        },
                        'UpdateExpression': 'SET #workflow_history = :workflow_history, ' +
                                            '#matched_time = :matched_time, ' +
                                            '#matched_to_participant = :matched_to_participant ' +
                                            'REMOVE #workflow_state, #secondary_status',
                        'ExpressionAttributeValues': {
                            ':workflow_history': {
                                'L': [
                                    {'M': {
                                            'action': {'S': 'check'},
                                            'comment': {'S': 'Some comment from the processor'},
                                            'selected_participant_id': {'S': '1234'},
                                            'selection': {'S': 'Enter a new address'},
                                            'step': {'S': 'process'},
                                            'timestamp': {'S': '2021-03-22T09:32:26.891Z'},
                                            'user': {'S': 'T Processor'},
                                            'address': {'S': 'New address'},
                                    }},
                                    {'M': {
                                        'step': {'S': 'check'},
                                        'action': {'S': 'accept'},
                                        'user': {'S': 'Jim Smith'}
                                    }}
                                ]
                            },
                            ':matched_time': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            ':matched_to_participant': {'S': '1'}
                        },
                        'ExpressionAttributeNames': {
                            '#workflow_history': 'workflow_history',
                            '#matched_time': 'matched_time',
                            '#matched_to_participant': 'matched_to_participant',
                            '#workflow_state': 'workflow_state',
                            '#secondary_status': 'secondary_status'
                        }
                    }
                }
            ])
        table_mock.update_item.assert_not_called()

        add_result_letter_to_processing_queue_mock.assert_called()

        audit_mock.assert_has_calls([
            call(action=AuditActions.MANUAL_MATCH_RESULT,
                 additional_information={'slide_number': '12345678', 'test_date': '2021-02-28', 'action': 'Routine',
                                         'action_code': 'A', 'result': 'No Cytology test undertaken',
                                         'result_code': 'X', 'infection_result': 'HPV negative', 'infection_code': '0',
                                         'source_code': 'G', 'sender_source_type': '1', 'sender_code': '567',
                                         'lab_address': 'Lab address',
                                         'updating_next_test_due_date_from': 'the_starting_ntdd',
                                         'updating_next_test_due_date_to': '2022-01-01'},
                 nhs_numbers=['1234567890'],
                 participant_ids=['1'],
                 session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}}),
            call(action=AuditActions.UPDATE_UNMATCHED_RESULT,
                 additional_information={'result_id': '1', 'selection': 'Enter a new address', 'participant': '1234'},
                 session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}})])

        expected_log_calls = [call({'log_reference': LogReference.ACCPTMTCH0001}),
                              call({'log_reference': LogReference.ACCPTMTCH0004}),
                              call({'log_reference': LogReference.ACCPTMTCH0005}),
                              call({'log_reference': LogReference.EPISODE0001}),
                              call({'log_reference': LogReference.EPISODE0002}),
                              call({'log_reference': LogReference.ACCPTMTCH0011}),
                              call({'log_reference': LogReference.ACCPTMTCH0009})]

        log_mock.assert_has_calls(expected_log_calls)
        autocease_mock.assert_called_once_with({'participant_id': '1', 'nhs_number': '1234567890',
                                                'date_of_birth': '1990-01-01', 'next_test_due_date': '2022-01-01',
                                                'letter_address': 'New address'})

    @patch(f'{UTILS_MODULE}.get_result')
    @patch(f'{UTILS_MODULE}.dynamodb_transact_write_items')
    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{MODULE}.datetime')
    @patch(f'{MODULE}.query_for_live_episode')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{MODULE}.add_result_letter_to_processing_queue')
    @patch(f'{MODULE}.generate_reinstate_participant_transactions')
    @patch(f'{UTILS_MODULE}.log')
    def test_accept_match_succeeds_with_new_address_fail(
            self, utils_log_mock, reinstate_participant_mock, add_result_letter_to_processing_queue_mock,
            validate_matched_mock, query_participants_mock, query_episode_mock, datetime_mock,
            participant_workflow_mock, audit_mock, dynamodb_client_mock, get_result_mock, log_mock, table_mock):
        datetime_mock.now.return_value = datetime.datetime(2021, 1, 25, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        participant_workflow_mock.return_value = {
            'participant_id': '1',
            'nhs_number': '1234567890'
        }

        query_episode_mock.return_value = QUERY_EPISODE_MOCK

        participant_items = PARTICIPANT_2

        get_result_mock.return_value = RESULT_MOCK

        query_participants_mock.return_value = participant_items
        validate_matched_mock.return_value = None
        reinstate_participant_mock.return_value = None, None
        accept_match_event_data = self._read_json_file('accept_match_send_to_new_address_fail.json')

        # Act
        with self.assertRaises(Exception):
            self.service_module.accept_match(accept_match_event_data)

        # Assert

        expected_log_calls = [call({'log_reference': LogReference.ACCPTMTCH0001}),
                              call({'log_reference': LogReference.ACCPTMTCH0003}),
                              call({'log_reference': LogReference.ACCPTMTCH0010})]

        expected_utils_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0017}),
                                    call({'log_reference': LogReference.MNGRESMTCH0019})]

        log_mock.assert_has_calls(expected_log_calls)
        utils_log_mock.assert_has_calls(expected_utils_log_calls)

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{UTILS_MODULE}.log')
    def test_accept_match_fails(
            self, utils_log_mock, validate_matched_result_mock, query_participants_mock,
            participant_workflow_mock, audit_mock, log_mock, table_mock):
        # Arrange
        validate_matched_result_mock.return_value = None
        participant_workflow_mock.return_value = {
            'participant_id': '1234'
        }

        # Act
        with self.assertRaises(Exception):
            self.service_module.accept_match(ACCEPT_MATCH_EVENT_DATA)

        expected_log_calls = [call({'log_reference': LogReference.ACCPTMTCH0001}),
                              call({'log_reference': LogReference.ACCPTMTCH0003}),
                              call({'log_reference': LogReference.ACCPTMTCH0010})]

        expected_utils_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0017}),
                                    call({'log_reference': LogReference.MNGRESMTCH0019})]

        log_mock.assert_has_calls(expected_log_calls)
        utils_log_mock.assert_has_calls(expected_utils_log_calls)

    @patch(f'{MODULE}.generate_reinstate_participant_transactions')
    @patch(f'{MODULE}.participant_eligible_for_auto_cease')
    @patch(f'{MODULE}.build_updates_for_episodes')
    @patch(f'{MODULE}.cease_participant_and_send_notification_messages')
    @patch(f'{MODULE}.audit')
    @patch(f'{MODULE}.add_result_letter_to_processing_queue')
    @patch(f'{MODULE}.calculate_and_update_ntdd')
    def test_check_if_participant_can_be_reinstated_auto_cease(
            self, ntdd_mock, letter_mock, audit_mock, notification_mock, episode_update_mock,
            auto_cease_mock, reinstate_participant_mock, log_mock, table_mock):
        # Arrange
        decoded_record = {
            'test_date': '2021-05-02',
            'status': ParticipantStatus.ROUTINE
        }
        participant = {
            'participant_id': '1',
            'is_ceased': True,
            'date_of_birth': '1930-10-10',
            'next_test_due_date': '2021-10-10'
        }

        episode_update_mock.return_value = []
        auto_cease_mock.return_value = True
        reinstate_participant_mock.return_value = {}, {}, None

        # Act
        update_transact_items, reinstate_put, reinstated_ntdd = \
            self.service_module.check_if_participant_can_be_reinstated_and_build_episode_updates(
                decoded_record, '2021-04-01', '1', participant)

        # Assert
        episode_update_mock.assert_called_with('1', '2021-04-01', '1', True)
        self.assertEqual([], update_transact_items)
        self.assertEqual({}, reinstate_put)

    @patch(f'{MODULE}.autocease_if_eligible')
    @patch(f'{UTILS_MODULE}.get_result')
    @patch(f'{UTILS_MODULE}.dynamodb_transact_write_items')
    @patch(f'{MODULE}.audit')
    @patch('common.utils.result_letter_utils.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{MODULE}.datetime')
    @patch(f'{MODULE}.query_for_live_episode')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{MODULE}.add_result_letter_to_processing_queue')
    @patch(f'{MODULE}.generate_reinstate_participant_transactions')
    @patch(f'{MODULE}.calculate_and_update_ntdd')
    @patch(f'{MODULE}.participant_eligible_for_auto_cease')
    @patch('common.utils.result_letter_utils.check_for_result_letter_suppress_reason')
    @patch('common.utils.result_letter_utils.log')
    @patch(f'{UTILS_MODULE}.log')
    @patch('common.utils.match_result_utils.log', Mock())
    def test_letter_suppressed_successfully(
            self, utils_log_mock, result_letter_log_mock, check_letter_mock,
            is_eligible_mock, ntdd_mock, reinstate_participant_mock,
            add_result_letter_to_processing_queue_mock, validate_matched_mock,
            query_participants_mock, query_episode_mock, datetime_mock,
            participant_workflow_mock, result_letter_utils_audit_mock, audit_mock, dynamodb_client_mock,
            get_result_mock, autocease_mock, log_mock, table_mock):
        # Arrange
        datetime_mock.now.return_value = datetime.datetime(2021, 1, 25, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        participant_workflow_mock.return_value = {
            'participant_id': '1',
            'nhs_number': '1234567890',
            'date_of_birth': '1997-03-03',
            'is_ceased': False,
            'next_test_due_date': '2020-12-25'
        }

        query_episode_mock.return_value = [{
            'sort_key': 'EPISODE#',
            'live_record_status': EpisodeStatus.WALKIN.value
        },
            {
                'sort_key': 'EPISODE#',
                'live_record_status': EpisodeStatus.OTHER.value
        }]

        participant_items = [{
            'participant_id': '1',
            'nhs_number': '1234567890',
            'date_of_birth': '1997-03-03',
            'is_ceased': False,
            'next_test_due_date': '2020-12-25'
        }]

        get_result_mock.return_value = {
            'Item': {
                'result_id': '0',
                'decoded': {
                    'test_date': '2020-09-22',
                    'date_of_birth': '1980-01-01',
                    'source_code': 'H',
                    'sender_source_type': '7',
                    'result_code': 'X',
                    'infection_code': '0',
                    'action_code': 'A',
                    'hpv_primary': True
                }
            }
        }

        query_participants_mock.return_value = participant_items
        validate_matched_mock.return_value = None
        reinstate_participant_mock.return_value = None, None, None
        add_result_letter_to_processing_queue_mock.return_value = None
        ntdd_mock.return_value = date(2022, 2, 1)
        check_letter_mock.return_value = SupressionReason.SOURCE_TYPE_7

        # Act
        self.service_module.accept_match(ACCEPT_MATCH_EVENT_DATA)

        # Assert
        add_result_letter_to_processing_queue_mock.assert_not_called()

        audit_mock.assert_has_calls([
            call(action=AuditActions.MANUAL_MATCH_RESULT,
                 additional_information={'slide_number': '12345678', 'test_date': '2021-02-28', 'action': 'Routine',
                                         'action_code': 'A', 'result': 'No Cytology test undertaken',
                                         'result_code': 'X', 'infection_result': 'HPV negative', 'infection_code': '0',
                                         'source_code': 'G', 'sender_source_type': '1', 'sender_code': '567',
                                         'lab_address': '45 BARROW DITCHE LEIGHTON BUZZARD BEDS LU8 3EH',
                                         'updating_next_test_due_date_from': '2020-12-25',
                                         'updating_next_test_due_date_to': '2022-02-01'},
                 nhs_numbers=['1234567890'],
                 participant_ids=['1'],
                 session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}}),
            call(action=AuditActions.UPDATE_UNMATCHED_RESULT,
                 additional_information={'result_id': '1', 'selection': 'Send to lab address', 'participant': '1234'},
                 session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}})])

        expected_log_calls = [call({'log_reference': LogReference.ACCPTMTCH0001}),
                              call({'log_reference': LogReference.ACCPTMTCH0004}),
                              call({'log_reference': LogReference.ACCPTMTCH0005}),
                              call({'log_reference': LogReference.EPISODE0001}),
                              call({'log_reference': LogReference.EPISODE0002}),
                              call({'log_reference': LogReference.ACCPTMTCH0011}),
                              call({'log_reference': LogReference.ACCPTMTCH0009})]

        expected_utils_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0017}),
                                    call({'log_reference': LogReference.MNGRESMTCH0019})]
        expected_result_letter_log_calls = [call({'log_reference': CommonLogReference.MATCHRLET0004,
                                                  'participant id': '1',
                                                  'result_id': '1',
                                                  'suppression_reason':
                                                  SupressionReason.SOURCE_TYPE_7})]

        log_mock.assert_has_calls(expected_log_calls)
        utils_log_mock.assert_has_calls(expected_utils_log_calls)
        result_letter_log_mock.assert_has_calls(expected_result_letter_log_calls)

        autocease_mock.assert_called_once_with({'participant_id': '1', 'nhs_number': '1234567890',
                                                'date_of_birth': '1997-03-03', 'is_ceased': False,
                                                'next_test_due_date': '2022-02-01', 'letter_address':
                                                    {
                                                        'address_line_1': '45 BARROW DITCHE LEIGHTON BUZZARD',
                                                        'address_line_2': 'BEDS',
                                                        'address_line_3': '',
                                                        'address_line_4': '',
                                                        'address_line_5': '',
                                                        'postcode': 'LU83EH'
                                                    }
                                                })
        result_letter_utils_audit_mock.assert_called_once_with(
            action=AuditActions.RESULT_LETTER_SUPPRESSED,
            user=AuditUsers.SYSTEM,
            additional_information={
                'test_date': '2021-02-28',
                'suppression_reason': 'Suppressing result letter as lab will send their own letter'},
            participant_ids=['1'])

    @patch(f'{MODULE}.autocease_if_eligible')
    @patch(f'{UTILS_MODULE}.get_result')
    @patch(f'{UTILS_MODULE}.dynamodb_transact_write_items')
    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{MODULE}.datetime')
    @patch(f'{MODULE}.query_for_live_episode')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{MODULE}.add_result_letter_to_processing_queue')
    @patch(f'{MODULE}.generate_reinstate_participant_transactions')
    @patch(f'{MODULE}.calculate_and_update_ntdd')
    @patch(f'{MODULE}.participant_eligible_for_auto_cease')
    @patch('common.utils.result_letter_utils.check_for_result_letter_suppress_reason')
    @patch(f'{UTILS_MODULE}.log')
    def test_letter_suppression_handles_and_rethrows_exception(
            self, utils_log_mock, check_letter_mock, is_eligible_mock, ntdd_mock, reinstate_participant_mock,
            add_result_letter_to_processing_queue_mock, validate_matched_mock,
            query_participants_mock, query_episode_mock, datetime_mock,
            participant_workflow_mock, audit_mock, dynamodb_client_mock,
            get_result_mock, autocease_mock, log_mock, table_mock):
        # Arrange
        datetime_mock.now.return_value = datetime.datetime(2021, 1, 25, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        participant_workflow_mock.return_value = {
            'participant_id': '1',
            'nhs_number': '1234567890',
            'date_of_birth': '1997-03-03',
            'is_ceased': False,
            'next_test_due_date': '2020-12-25'
        }

        query_episode_mock.return_value = [{
            'sort_key': 'EPISODE#',
            'live_record_status': EpisodeStatus.WALKIN.value
        },
            {
                'sort_key': 'EPISODE#',
                'live_record_status': EpisodeStatus.OTHER.value
        }]

        participant_items = [{
            'participant_id': '1',
            'nhs_number': '1234567890',
            'date_of_birth': '1997-03-03',
            'is_ceased': False,
            'next_test_due_date': '2020-12-25'
        }]

        get_result_mock.return_value = {
            'Item': {
                'result_id': '0',
                'decoded': {
                    'test_date': '2020-09-22',
                    'date_of_birth': '1997-03-03',
                    'source_code': 'H',
                    'sender_source_type': '7',
                    'result_code': 'X',
                    'infection_code': '0',
                    'action_code': 'A',
                    'hpv_primary': True
                }
            }
        }

        query_participants_mock.return_value = participant_items
        validate_matched_mock.return_value = None
        reinstate_participant_mock.return_value = None, None, None
        ntdd_mock.return_value = date(2022, 2, 1)
        check_letter_mock.side_effect = Mock(side_effect=Exception('Test'))

        # Act
        with self.assertRaises(Exception):
            self.service_module.accept_match(ACCEPT_MATCH_EVENT_DATA)

        # Assert
        add_result_letter_to_processing_queue_mock.assert_not_called()
        # autocease_mock.assert_not_called()

        # TODO
        # audit_mock.assert_has_calls([
        #    call(action=AuditActions.MANUAL_MATCH_RESULT,
        #         additional_information={'slide_number': '12345678', 'test_date': '2021-02-28', 'action': 'Routine',
        #                                 'action_code': 'A', 'result': 'No Cytology test undertaken',
        #                                 'result_code': 'X', 'infection_result': 'HPV negative', 'infection_code': '0',
        #                                 'source_code': 'G', 'sender_source_type': '1', 'sender_code': '567',
        #                                 'lab_address': '45 BARROW DITCHE LEIGHTON BUZZARD BEDS LU8 3EH',
        #                                 'updating_next_test_due_date_from': '2020-12-25',
        #                                 'updating_next_test_due_date_to': '2022-02-01'},
        #         nhs_numbers=['1234567890'],
        #         participant_ids=['1'],
        #         session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}})
        # ])

        # expected_log_calls = [call({'log_reference': LogReference.ACCPTMTCH0001}),
        #                      call({'log_reference': LogReference.ACCPTMTCH0004}),
        #                      call({'log_reference': LogReference.ACCPTMTCH0005}),
        #                      call({'log_reference': LogReference.EPISODE0001}),
        #                      call({'log_reference': LogReference.EPISODE0002}),
        #                      call({'log_reference': LogReference.ACCPTMTCH0011}),
        #                      call({'log_reference': LogReference.ACCPTMTCH0010})]

        # expected_utils_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0017}),
        #                            call({'log_reference': LogReference.MNGRESMTCH0019})]

        # log_mock.assert_has_calls(expected_log_calls)
        # utils_log_mock.assert_has_calls(expected_utils_log_calls)

    @patch(f'{MODULE}.autocease_if_eligible')
    @patch(f'{UTILS_MODULE}.get_result')
    @patch(f'{UTILS_MODULE}.dynamodb_transact_write_items')
    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{MODULE}.datetime')
    @patch(f'{MODULE}.query_for_live_episode')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{MODULE}.add_result_letter_to_processing_queue')
    @patch(f'{MODULE}.generate_reinstate_participant_transactions')
    @patch(f'{MODULE}.calculate_and_update_ntdd')
    @patch(f'{MODULE}.participant_eligible_for_auto_cease')
    @patch(f'{UTILS_MODULE}.log')
    @patch('common.utils.result_letter_utils.check_for_result_letter_suppress_reason')
    @patch('common.utils.result_letter_utils.log')
    @patch(f'{MATCH_RESULT_UTILS_MODULE}.log')
    def test_accept_match_adds_recall_months_to_audit_if_action_is_repeat_advised(
            self, match_result_utils_log_mock, result_utils_log_mock, check_letter_mock, utils_log_mock,
            is_eligible_mock, ntdd_mock,
            reinstate_participant_mock, add_result_letter_to_processing_queue_mock, validate_matched_mock,
            query_participants_mock, query_episode_mock, datetime_mock, participant_workflow_mock,
            audit_mock, dynamodb_client_mock, get_result_mock, autocease_mock, log_mock, table_mock):
        datetime_mock.now.return_value = datetime.datetime(2021, 1, 25, 12, 15, 8, 132000, tzinfo=datetime.timezone.utc)

        participant_workflow_mock.return_value = {
            'participant_id': '1',
            'nhs_number': '1234567890',
            'date_of_birth': '1990-01-01',
            'address': 'System address',
            'next_test_due_date': '2020-12-25'
        }

        query_episode_mock.return_value = QUERY_EPISODE_MOCK

        participant_items = [{
            'participant_id': '1',
            'nhs_number': '1234567890',
            'date_of_birth': '1990-01-01',
            'address': 'System address',
            'next_test_due_date': '2020-12-25'
        }]

        get_result_mock.return_value = RESULT_MOCK

        is_eligible_mock.return_value = False
        autocease_mock.return_value = False
        query_participants_mock.return_value = participant_items
        validate_matched_mock.return_value = None
        reinstate_participant_mock.return_value = None, None, None
        check_letter_mock.return_value = None
        add_result_letter_to_processing_queue_mock.return_value = None
        accept_match_event_data = self._read_json_file('accept_match_with_action_repeat_advised.json')
        ntdd_mock.return_value = parse('2022-01-01')

        # Act
        self.service_module.accept_match(accept_match_event_data)

        # Assert
        dynamodb_client_mock.assert_called_with(
            [
                {
                    'Put': {
                        'TableName': 'PARTICIPANTS',
                        'Item': {
                            'participant_id': {'S': '1'},
                            'sort_key': {'S': 'RESULT#2021-02-28#2021-01-25T12:15:08.132000+00:00'},
                            'result_id': {'S': '1'},
                            'nhs_number': {'S': '1234567890'},
                            'sanitised_nhs_number': {'S': '1234567890'},
                            'slide_number': {'S': '12345678'},
                            'created': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            'test_date': {'S': '2021-02-28'},
                            'result_date': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            'next_test_due_date': {'S': '2024-02-28'},
                            'action': {'S': 'Repeat advised'},
                            'action_code': {'S': 'R'},
                            'result': {'S': 'No Cytology test undertaken'},
                            'result_code': {'S': 'X'},
                            'infection_result': {'S': 'HPV negative'},
                            'infection_code': {'S': '0'},
                            'source_code': {'S': 'G'},
                            'sender_source_type': {'S': '1'},
                            'sender_code': {'S': '567'},
                            'self_sample': {'BOOL': False},
                            'status': {'S': ParticipantStatus.REPEAT_ADVISED},
                            'letter_address': {'S': 'System address'},
                            'hpv_primary': {'BOOL': False},
                            'recall_months': {'S': '36'}
                        }
                    }
                },
                {
                    'Update': TRANSACT_UPDATE_COMPLETE_ITEM
                },
                {
                    'Update': TRANSACT_UPDATE_CLOSED_ITEM
                },
                {
                    'Update': {
                        'TableName': 'RESULTS',
                        'Key': {
                            'result_id': {'S': '1'},
                            'received_time': {'S': '14:44'}
                        },
                        'UpdateExpression': 'SET #workflow_history = :workflow_history, ' +
                                            '#matched_time = :matched_time, ' +
                                            '#matched_to_participant = :matched_to_participant ' +
                                            'REMOVE #workflow_state, #secondary_status',
                        'ExpressionAttributeValues': {
                            ':workflow_history': {
                                'L': [
                                    {'M': {
                                            'action': {'S': 'check'},
                                            'comment': {'S': 'Some comment from the processor'},
                                            'selected_participant_id': {'S': '1234'},
                                            'selection': {'S': 'Send to system address'},
                                            'step': {'S': 'process'},
                                            'timestamp': {'S': '2021-03-22T09:32:26.891Z'},
                                            'user': {'S': 'T Processor'},
                                    }},
                                    {'M': {
                                        'step': {'S': 'check'},
                                        'action': {'S': 'accept'},
                                        'user': {'S': 'Jim Smith'}
                                    }}
                                ]
                            },
                            ':matched_time': {'S': '2021-01-25T12:15:08.132000+00:00'},
                            ':matched_to_participant': {'S': '1'}
                        },
                        'ExpressionAttributeNames': {
                            '#workflow_history': 'workflow_history',
                            '#matched_time': 'matched_time',
                            '#matched_to_participant': 'matched_to_participant',
                            '#workflow_state': 'workflow_state',
                            '#secondary_status': 'secondary_status'
                        }
                    }
                }
            ]
        )
        table_mock.update_item.assert_not_called()

        add_result_letter_to_processing_queue_mock.assert_called_once_with(
            {
                'participant_id': '1', 'sort_key': 'RESULT#2021-02-28#2021-01-25T12:15:08.132000+00:00',
                'result_id': '1', 'nhs_number': '1234567890', 'sanitised_nhs_number': '1234567890',
                'slide_number': '12345678', 'created': '2021-01-25T12:15:08.132000+00:00', 'test_date': '2021-02-28',
                'result_date': '2021-01-25T12:15:08.132000+00:00', 'next_test_due_date': '2024-02-28',
                'action': 'Repeat advised', 'action_code': 'R', 'result': 'No Cytology test undertaken',
                'result_code': 'X', 'infection_result': 'HPV negative', 'infection_code': '0', 'source_code': 'G',
                'sender_source_type': '1', 'sender_code': '567', 'self_sample': False,
                'status': ParticipantStatus.REPEAT_ADVISED, 'letter_address': 'System address', 'hpv_primary': False,
                'recall_months': '36'
            }, False)

        audit_mock.assert_has_calls([
            call(action=AuditActions.MANUAL_MATCH_RESULT,
                 additional_information={'slide_number': '12345678', 'test_date': '2021-02-28',
                                         'action': 'Repeat advised',
                                         'action_code': 'R', 'result': 'No Cytology test undertaken',
                                         'result_code': 'X',
                                         'infection_result': 'HPV negative', 'infection_code': '0',
                                         'source_code': 'G', 'sender_source_type': '1', 'sender_code': '567',
                                         'lab_address': 'Lab address',
                                         'updating_next_test_due_date_from': '2020-12-25',
                                         'updating_next_test_due_date_to': '2022-01-01 00:00:00',
                                         'recall_months': '36'},
                 nhs_numbers=['1234567890'],
                 participant_ids=['1'],
                 session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}}),
            call(action=AuditActions.UPDATE_UNMATCHED_RESULT,
                 additional_information={'result_id': '1', 'selection': 'Send to system address',
                                         'participant': '1234'},
                 session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}})])

        expected_log_calls = [call({'log_reference': LogReference.ACCPTMTCH0001}),
                              call({'log_reference': LogReference.ACCPTMTCH0004}),
                              call({'log_reference': LogReference.ACCPTMTCH0005}),
                              call({'log_reference': LogReference.EPISODE0001}),
                              call({'log_reference': LogReference.EPISODE0002}),
                              call({'log_reference': LogReference.ACCPTMTCH0011}),
                              call({'log_reference': LogReference.ACCPTMTCH0009})]

        expected_utils_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0017}),
                                    call({'log_reference': LogReference.MNGRESMTCH0019})]

        match_result_utils_log_mock.assert_has_calls([call({'log_reference': CommonLogReference.NTDDCALCULATION0001})])

        log_mock.assert_has_calls(expected_log_calls)
        utils_log_mock.assert_has_calls(expected_utils_log_calls)

    @patch(f'{MODULE}.audit')
    @patch(f'{MODULE}.cease_participant_and_send_notification_messages')
    @patch(f'{MODULE}.participant_eligible_for_auto_cease')
    def test_autocease_if_eligible_given_participant_is_eligible(
            self, is_eligible_mock, cease_mock, audit_mock, log_mock, table_mock):
        participant = {'participant_id': 'the_participant_id',
                       'next_test_due_date': '2020-01-03',
                       'nhs_number': '9999999999'}
        is_eligible_mock.return_value = True

        self.service_module.autocease_if_eligible(participant)

        is_eligible_mock.assert_called_once_with(participant, date(2020, 1, 3))
        cease_mock.assert_called_once_with(participant, 'SYSTEM', {'reason': CeaseReason.AUTOCEASE_DUE_TO_AGE})
        audit_mock.assert_called_once_with(action=AuditActions.CEASE_EPISODE,
                                           additional_information={'reason': CeaseReason.AUTOCEASE_DUE_TO_AGE,
                                                                   'updating_next_test_due_date_from': '2020-01-03'},
                                           user=AuditUsers.SYSTEM,
                                           participant_ids=['the_participant_id'],
                                           nhs_numbers=['9999999999'])
        log_mock.assert_has_calls([
            call(LogReference.ACCPTMTCH0006),
            call(LogReference.ACCPTMTCH0007)
        ])

    @patch(f'{MODULE}.audit')
    @patch(f'{MODULE}.cease_participant_and_send_notification_messages')
    @patch(f'{MODULE}.participant_eligible_for_auto_cease')
    def test_autocease_if_eligible_given_participant_is_not_eligible(
            self, is_eligible_mock, cease_mock, audit_mock, log_mock, table_mock):
        participant = {'participant_id': 'the_participant_id', 'sort_key': 'PARTICIPANT'}

        is_eligible_mock.return_value = False

        self.service_module.autocease_if_eligible(participant)

        is_eligible_mock.assert_called_once_with(participant, None)
        cease_mock.assert_not_called()
        audit_mock.assert_not_called()
        log_mock.assert_has_calls([
            call(LogReference.ACCPTMTCH0006),
            call(LogReference.ACCPTMTCH0008)
        ])

    def _read_json_file(self, file_name):
        current_path = os.path.dirname(os.path.realpath(__file__))
        full_file_path = os.path.join(current_path, 'test_data', file_name)
        return json.loads(open(full_file_path).read())
