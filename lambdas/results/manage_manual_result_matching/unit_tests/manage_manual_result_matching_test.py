from unittest import TestCase
from mock import patch, Mock, call
from common.utils.result_record_locking_utils import LockException
from manage_manual_result_matching.log_references import LogReference
from common.test_assertions.api_assertions import assertApiResponse

MODULE = 'manage_manual_result_matching.manage_manual_result_matching'


@patch('common.log.logging')
@patch(f'{MODULE}.log')
@patch(f'{MODULE}.get_api_request_info_from_event')
class TestManageResultMatching(TestCase):

    def setUp(self):
        import manage_manual_result_matching.manage_manual_result_matching as _manage_result_matching
        self.manage_result_matching_module = _manage_result_matching
        self.context = Mock()
        self.context.function_name = ''
        self.event = {
            'body': {
                'workflow_state': 'NOT STARTED'
            },
            'path_parameters': {
                'sending_lab': 'manchester',
                'process_result_id': '1'
            },
            'requestContext': {'authorizer': {'session': '{"session_id": "12345678"}', 'principalId': 'blah'}},
            'headers': {'request_id': 'blah'}
        }
        self.bad_request_response = 'Request failure'
        self.internal_server_error = 'Cannot complete request'

    @patch(f'{MODULE}.update_unmatched_result')
    def test_lambda_handler_updates_workflow_state(
            self, update_unmatched_result_mock, get_request_info_mock, log_mock, common_log_mock):
        get_request_info_mock.return_value = (
            {
                'headers': None,
                'body': '{}',
                'pathParameters': {
                    'sending_lab': 'manchester',
                    'process_result_id': '1'
                }
            },
            'POST',
            '/api/csas/results/{sending_lab}/{result_id}'
        )

        update_unmatched_result_mock.return_value = {}

        actual_response = self.manage_result_matching_module.lambda_handler(self.event, self.context)
        self.assertEqual(200, actual_response['statusCode'])

        expected_update_unmatched_result_call = [
            call({'headers': None, 'body': '{}', 'pathParameters': {
                'sending_lab': 'manchester', 'process_result_id': '1'}})]

        update_unmatched_result_mock.assert_has_calls(expected_update_unmatched_result_call)

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0001, 'http_method': 'POST'}),
                              call({'log_reference': LogReference.MNGRESMTCH0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.get_unmatched_result_with_participant_matches')
    def test_lambda_handler_gets_unmatched_result(
            self, get_unmatched_result_mock, get_request_info_mock, log_mock, common_log_mock):
        get_request_info_mock.return_value = (
            {
                'headers': None,
                'body': '{}',
                'pathParameters': {
                    'sending_lab': 'manchester',
                    'process_result_id': '1'
                }
            },
            'GET',
            '/api/csas/results/{sending_lab}/{result_id}'
        )

        get_unmatched_result_mock.return_value = {}

        actual_response = self.manage_result_matching_module.lambda_handler(self.event, self.context)
        self.assertEqual(200, actual_response['statusCode'])

        expected_get_unmatched_result_call = [
            call({'headers': None, 'body': '{}', 'pathParameters': {
                'sending_lab': 'manchester', 'process_result_id': '1'}})]

        get_unmatched_result_mock.assert_has_calls(expected_get_unmatched_result_call)

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0001, 'http_method': 'GET'}),
                              call({'log_reference': LogReference.MNGRESMTCH0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.update_unmatched_result')
    def test_lambda_returns_400_for_bad_type_update_request(
            self, update_unmatched_result_mock, get_request_info_mock, log_mock, common_log_mock):
        get_request_info_mock.return_value = (
            {
                'headers': None,
                'body': '{}',
            },
            'POST',
            '/api/csas/results/{sending_lab}/{result_id}'
        )
        update_unmatched_result_mock.side_effect = Mock(side_effect=KeyError(self.bad_request_response))

        actual_response = self.manage_result_matching_module.lambda_handler(self.event, self.context)

        assertApiResponse(self, 400, {'message': "'Request failure'"}, actual_response)

        expected_calls = [call({'log_reference': LogReference.MNGRESMTCH0001, 'http_method': 'POST'}),
                          call({'log_reference': LogReference.MNGRESMTCH0003,
                                'error': "'Request failure'"})]

        log_mock.assert_has_calls(expected_calls)

    @patch(f'{MODULE}.update_unmatched_result')
    def test_lambda_returns_500_for_invalid_request(
            self, update_unmatched_result_mock, get_request_info_mock, log_mock, common_log_mock):
        get_request_info_mock.return_value = (
            {
                'headers': None,
                'body': '{}',
            },
            'POST',
            '/api/csas/results/{sending_lab}/{result_id}'
        )
        update_unmatched_result_mock.side_effect = Mock(side_effect=Exception(self.internal_server_error))

        actual_response = self.manage_result_matching_module.lambda_handler(self.event, self.context)

        self.assertEqual(500, actual_response['statusCode'])
        self.assertEqual('{"message": "Cannot complete request"}', actual_response['body'])

        expected_calls = [call({'log_reference': LogReference.MNGRESMTCH0001, 'http_method': 'POST'}),
                          call({'log_reference': LogReference.MNGRESMTCH0003,
                                'error': 'Cannot complete request'})]

        log_mock.assert_has_calls(expected_calls)

    @patch(f'{MODULE}.update_unmatched_result')
    def test_lambda_returns_409_when_lock_fails(
            self, update_unmatched_result_mock, get_request_info_mock, log_mock, common_log_mock):
        get_request_info_mock.return_value = (
            {
                'headers': None,
                'body': '{}',
            },
            'POST',
            '/api/csas/results/{sending_lab}/{result_id}/lock'
        )
        update_unmatched_result_mock.side_effect = Mock(side_effect=LockException())

        actual_response = self.manage_result_matching_module.lambda_handler(self.event, self.context)

        self.assertEqual(409, actual_response['statusCode'])
        self.assertEqual('{"message": "Failed to acquire or release record lock"}', actual_response['body'])

        expected_calls = [call({'log_reference': LogReference.MNGRESMTCH0001, 'http_method': 'POST'}),
                          call({'log_reference': LogReference.MNGRESMTCH0003,
                                'error': 'Failed to acquire or release record lock'})]

        log_mock.assert_has_calls(expected_calls)

    @patch(f'{MODULE}.get_unmatched_lab_results')
    def test_lambda_returns_200_for_successful_get_request(self,
                                                           get_results_mock,
                                                           event_data_mock,
                                                           log_mock,
                                                           common_log_mock):
        sending_lab = 'Manchester'
        event_data_mock.return_value = (
            {
                'pathParameters': {
                    'sending_lab': sending_lab,
                },
                'headers': None,
                'body': '{}'
            },
            'GET',
            '/api/csas/results/{sending_lab}'
        )

        return_results = [{
                'result_id': '1',
                'patient_name': 'Jane Smith',
                'nhs_number': '2345678901',
                'workflow_state': {
                    'status': 'process',
                    'secondary_status': 'NOT STARTED'
                },
                'processor_name': '',
                'date_received': '01-01-2020',
                'time_received': '01-01-2020',
                'sending_lab': sending_lab
            }, {
                'result_id': '2',
                'patient_name': 'Jane Doe',
                'nhs_number': '987654321',
                'workflow_state': {
                    'status': 'process',
                    'secondary_status': 'WITH PROCESSOR'
                },
                'processor_name': 'Roy Smith',
                'date_received': '01-01-2020',
                'time_received': '01-01-2020',
                'sending_lab': sending_lab
            }, {
                'result_id': '3',
                'patient_name': 'Jane Smith',
                'nhs_number': '123456789',
                'workflow_state': {
                    'status': 'query',
                    'secondary_status': 'NOT STARTED'
                },
                'processor_name': '',
                'date_received': '01-01-2020',
                'time_received': '01-01-2020',
                'sending_lab': sending_lab
        }]

        get_results_mock.return_value = return_results

        actual_response = self.manage_result_matching_module.lambda_handler(self.event, self.context)

        assertApiResponse(self, 200, {'data': return_results}, actual_response)

        expected_log_calls = [
            call({'log_reference': LogReference.MNGRESMTCH0001, 'http_method': 'GET'}),
            call({'log_reference': LogReference.MNGRESMTCH0002})
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.move_to_next_step')
    def test_lambda_returns_200_for_successful_post_request(self,
                                                            update_results_mock,
                                                            event_data_mock,
                                                            log_mock,
                                                            common_log_mock):

        event_data_mock.return_value = (
            {
                'body': {
                    'result_id': '1',
                },
                'session': {
                    'user_data': {
                        'nhsid_useruid': '123456789'
                    }
                },
                'header': None
            },
            'POST',
            '/api/csas/results/{sending_lab}/{result_id}/lock'
        )

        update_results_mock.return_value = {}

        actual_response = self.manage_result_matching_module.lambda_handler(self.event, self.context)

        assertApiResponse(self, 200, {}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0001,  'http_method': 'POST'}),
                              call({'log_reference': LogReference.MNGRESMTCH0002})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.move_to_next_step')
    def test_lambda_returns_500_when_exception_received_from_service_module(self,
                                                                            update_results_mock,
                                                                            event_data_mock,
                                                                            log_mock,
                                                                            common_log_mock):

        event_data_mock.return_value = (
            {
                'body': {
                    'result_id': '1',
                    'sending_lab': 'CHR3'
                },
                'session': {
                    'user_data': {
                        'nhsid_useruid': '123456789'
                    }
                },
                'header': None
            },
            'POST',
            '/api/csas/results/{sending_lab}/{result_id}/lock'
        )

        update_results_mock.side_effect = Exception('Failed test')

        actual_response = self.manage_result_matching_module.lambda_handler(self.event, self.context)

        assertApiResponse(self, 500, {'message': 'Cannot complete request'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0001, 'http_method': 'POST'}),
                              call({'log_reference': LogReference.MNGRESMTCH0003, 'error': 'Failed test'})]
        log_mock.assert_has_calls(expected_log_calls)

    def test_lambda_raises_exception_when_bad_http_method(self, event_data_mock, log_mock, common_log_mock):

        event_data_mock.return_value = (
            {
                'body': {
                    'result_id': '1',
                    'workflow_state':  {
                        'status': 'process',
                        'secondary_status': 'WITH PROCESSOR'
                    },
                    'workflow_history': {
                            'action': 'new action'
                    },
                },
                'session': {
                    'user_data': {
                        'nhsid_useruid': '123456789'
                    }
                },
                'header': None
            },
            'DELETE',
            '/api/csas/results/{sending_lab}'
        )

        actual_response = self.manage_result_matching_module.lambda_handler(self.event, self.context)

        assertApiResponse(self, 500, {'message': 'Cannot complete request'}, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0001, 'http_method': 'DELETE'}),
                              call({'log_reference': LogReference.MNGRESMTCH0004}),
                              call({'log_reference': LogReference.MNGRESMTCH0003,
                                    'error': 'unrecognised method/resource combination'})]
        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.move_to_next_step')
    def test_failure_to_lock_raises_409(self, update_results_mock, event_data_mock, log_mock, common_log_mock):

        event_data_mock.return_value = (
            {
                'body': {
                    'result_id': '1',
                },
                'session': {
                    'user_data': {
                        'nhsid_useruid': '123456789'
                    }
                },
                'header': None
            },
            'POST',
            '/api/csas/results/{sending_lab}/{result_id}/lock'
        )

        update_results_mock.side_effect = LockException()

        actual_response = self.manage_result_matching_module.lambda_handler(self.event, self.context)

        assertApiResponse(
            self,
            409,
            {'message': 'Failed to acquire or release record lock'},
            actual_response
        )

        expected_log_calls = [
            call({'log_reference': LogReference.MNGRESMTCH0001, 'http_method': 'POST'}),
            call({
                'log_reference': LogReference.MNGRESMTCH0003,
                'error': 'Failed to acquire or release record lock'})
        ]
        log_mock.assert_has_calls(expected_log_calls)
