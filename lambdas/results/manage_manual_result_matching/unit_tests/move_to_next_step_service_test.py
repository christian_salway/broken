from unittest import TestCase
from ddt import data, ddt, unpack
from mock import patch
from manage_manual_result_matching.log_references import LogReference
from manage_manual_result_matching.move_to_next_step_service import get_next_secondary_status, move_to_next_step


@ddt
class UnitTestMoveToNextStepService(TestCase):

    # Arrange
    @data(
        ('process', 'WITH PROCESSOR'),
        ('check', 'WITH CHECKER'),
        ('unknown', None)
    )
    @unpack
    def test_get_next_secondary_status(self, workflow_state, next_secondary_workflow_status):
        # Act
        result = get_next_secondary_status(workflow_state)

        # Assert
        self.assertEqual(result, next_secondary_workflow_status)

    @patch('manage_manual_result_matching.move_to_next_step_service.log')
    def test_move_to_next_step_service_error_handling(self, log_mock):
        """
        Test error handling when event_data has missing data, KeyError
        """
        # Arrange, Act, Assert
        self.assertRaises(KeyError, move_to_next_step.__wrapped__, {
            'path_parameters': {
                'reason': 'result_id is a required path parameter'
            }
        })

        log_mock.assert_called_once_with({'log_reference': LogReference.MNGRESMTCH0016})
