import os
import json
from datetime import date
from unittest import TestCase
from boto3.dynamodb.conditions import Key
from common.utils.audit_utils import AuditActions
from manage_manual_result_matching.log_references import LogReference
from mock import Mock, call, patch
from common.models.result import ResultWorkflowState, ResultSecondaryStatus
from common.models.participant import ParticipantStatus
from common.utils.dynamodb_access.table_names import TableNames


MODULE = 'manage_manual_result_matching.manage_manual_result_matching_service'
UTILS_MODULE = 'manage_manual_result_matching.manage_manual_result_matching_utils'
RESULT_UTILS_MODULE = 'common.utils.result_utils'

PROJECTION_EXPRESSION = ', '.join([
    'result_id',
    'sending_lab',
    'decoded',
    'workflow_state',
    'validation_errors',
    'received_time',
    'secondary_status',
    'workflow_history',
    'user_id',
    'user_name',
    'lock_timestamp',
    'possible_match_participants',
    'rejected_reason',
    'crm_number',
    'duplicates',
    'sanitised_nhais_cipher',
    'matched_to_participant'
])

PROJECTION_EXPRESSION_FOR_DUPLICATES = ', '.join([
    'result_id',
    'sending_lab',
    'decoded',
    'received_time',
    'workflow_state',
    'workflow_history',
    'rejected_reason'
])

ACCEPT_MATCH_EVENT_DATA = {
    'body': {
        'user_id': 'userid1',
        'user_name': 'Jim Smith',
        'workflow_state': ResultWorkflowState.CHECK,
        'secondary_status': ResultSecondaryStatus.WITH_CHECKER,
        'workflow_history': [
            {
                'action': "check",
                'comment': "Some comment from the processor",
                'selected_participant_id': "1234",
                'selection': "Send to lab address",
                'step': "process",
                'timestamp': "2021-03-22T09:32:26.891Z",
                'user': "T Processor",
            },
            {
                'step': 'check',
                'action': 'accept'
            }
        ],
        'received_time': '14:44',
        'decoded': {
            'test_date': '2021-02-28',
            'slide_number': '12345678',
            'action': 'Routine',
            'action_code': 'A',
            'recall_months': '06',
            'result': 'No Cytology test undertaken',
            'result_code': 'X',
            'infection_result': 'HPV negative',
            'infection_code': '0',
            'source_code': '1',
            'sender_code': '567',
            'status': ParticipantStatus.REPEAT_ADVISED,
            'address': 'Lab address',
            'self_sample': False,
            'hpv_primary': True
        }
    },
    'path_parameters': {
        'result_id': '1'
    },
    'session': {
        'user_data': {
            'nhsid_useruid': 'userid1',
            'first_name': 'Jim',
            'last_name': 'Smith'
        },
    }
}

PARTICIPANT_1 = {
                'participant_id': 'test_participant',
                'nhs_number': '9876543210',
                'title': 'Miss',
                'first_name': 'Camila',
                'middle_names': 'Marta',
                'last_name': 'Cambello',
                'date_of_birth': '1997-03-03',
                'gender': 2,
                'address': ''
            }

RESULT_MOCK = {
            'Item': {'result_id': '0',
                     'decoded': {'test_date': date.today().strftime('%Y-%m-%d'),
                                 'source_code': '2'}
                     }
            }


@patch(f'{MODULE}.dynamodb_update_item')
@patch(f'{MODULE}.dynamodb_query')
@patch(f'{MODULE}.log')
class TestManageManualResultMatchingService(TestCase):

    @patch('boto3.resource', Mock())
    def setUp(self):
        import manage_manual_result_matching.manage_manual_result_matching_service as _service_module
        self.service_module = _service_module

    @patch(f'{MODULE}.get_participants_by_participant_ids')
    @patch(f'{MODULE}.audit')
    def test_query_for_getting_unmatched_result_succeeds(
            self, audit_mock, query_participants_mock, log_mock, query_mock, update_mock):
        # Arrange
        query_mock.return_value = [{
            'result_id': '1',
            'sending_lab': 'CHR3',
            'decoded': {
                'nhs_number': '1234',
                'first_name': 'MARY-JANE',
                'last_name': 'O\'SULLIVAN',
                'date_of_birth': '1979-09-01',
                'address': '5 Pudsey Lane, TownVille, M1 1AP',
            },
            'workflow_state': ResultWorkflowState.PROCESS,
            'validation_errors': None,
            'secondary_status': ResultSecondaryStatus.NOT_STARTED,
            'workflow_history': [],
            'user_id': None,
            'user_name': 'Jim Smith',
            'lock_timestamp': None,
            'received_time': '2020-01-01T09:30:00',
            'rejected_reason': None,
            'matched_to_participant': None,
            'possible_match_participants': [
                {
                    'participant_id': 'test_participant'
                }
            ]
        }]

        query_participants_mock.return_value = [
            {
                'participant_id': 'test_participant',
                'nhs_number': '9876543210',
                'title': 'Miss',
                'first_name': 'Camila',
                'middle_names': 'Marta',
                'last_name': 'Cambello',
                'date_of_birth': '1997-03-03',
                'gender': 2,
                'address': {
                    'address_line_1': 'Flat 6',
                    'address_line_2': 'Ann skyway',
                    'address_line_3': 'river',
                    'address_line_4': 'Fake',
                    'address_line_5': 'Mitchellfurt',
                    'postcode': 'E0 0HY'
                }
            }
        ]

        event_data = {
            'path_parameters': {
                'result_id': '1'
            },
            'session': {
                'user_data': {
                    'first_name': 'Jim',
                    'last_name': 'Smith'
                }
            }
        }

        # Act
        actual_response = self.service_module.get_unmatched_result_with_participant_matches.__wrapped__(event_data)

        # Assert
        expected_condition = Key('result_id').eq('1')
        query_mock.assert_called_with(TableNames.RESULTS, dict(
            KeyConditionExpression=expected_condition,
            ProjectionExpression=PROJECTION_EXPRESSION
        ))

        query_participants_mock.assert_called_with(['test_participant'])

        expected_response = {
            'result': {
                'result_id': '1',
                'sending_lab': 'CHR3',
                'decoded': {
                    'nhs_number': '1234',
                    'date_of_birth': '1979-09-01',
                    'first_name': 'Mary-Jane',
                    'last_name': 'O\'Sullivan',
                    'address': '5 Pudsey Lane, TownVille, M1 1AP'
                },
                'workflow_state': ResultWorkflowState.PROCESS,
                'validation_errors': None,
                'secondary_status': 'NOT STARTED',
                'workflow_history': [],
                'user_id': None,
                'user_name': 'Jim Smith',
                'role_id': None,
                'lock_timestamp': None,
                'received_time': '2020-01-01T09:30:00',
                'rejected_reason': None,
                'matched_to_participant': None,
                'possible_match_participants': [
                    {
                        'participant_id': 'test_participant'
                    }
                ]
            },
            'participants': [
                {
                    'participant_id': 'test_participant',
                    'nhs_number': '9876543210',
                    'title': 'Miss',
                    'first_name': 'Camila',
                    'middle_names': 'Marta',
                    'last_name': 'Cambello',
                    'date_of_birth': '1997-03-03',
                    'gender': 2,
                    'is_ceased': None,
                    'address': {
                        'address_line_1': 'Flat 6',
                        'address_line_2': 'Ann skyway',
                        'address_line_3': 'river',
                        'address_line_4': 'Fake',
                        'address_line_5': 'Mitchellfurt',
                        'postcode': 'E0 0HY'
                    }
                }
            ]
        }

        self.maxDiff = None
        self.assertEqual(expected_response, actual_response)

        audit_mock.assert_called_with(
            AuditActions.GET_UNMATCHED_RESULT,
            session={'user_data': {'first_name': 'Jim', 'last_name': 'Smith'}}
        )

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0005}),
                              call({'log_reference': LogReference.MNGRESMTCH0006})]

        log_mock.assert_has_calls(expected_log_calls)

    def test_query_for_getting_unmatched_result_throws_exception(self, log_mock, query_mock, update_mock):
        # Arrange
        query_mock.side_effect = Exception('Test')

        # Act
        event_data = {
            'path_parameters': {
                'patient_name': 'Jane Smith'
            }
        }
        with self.assertRaises(Exception):
            self.service_module.get_unmatched_result_with_participant_matches.__wrapped__(event_data)

        # Assert
        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0005}),
                              call({'log_reference': LogReference.MNGRESMTCH0007})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch('common.utils.result_letter_utils.add_result_letter_to_processing_queue')
    def test_update_item_for_unmatched_result_succeeds_for_system_details(
            self, add_result_letter_to_processing_queue_mock, validate_matched_mock,
            matched_participant_mock, audit_mock, log_mock, query_mock, update_mock):
        # Arrange
        event_data = {
            'body': {
                'nhs_number': '12345',
                'workflow_state': ResultWorkflowState.PROCESS,
                'validation_errors': None,
                'secondary_status': ResultSecondaryStatus.WITH_PROCESSOR,
                'workflow_history': [
                    {
                        'selected_participant_id': 'ABC123',
                        'comments': 'comment',
                        'selection': 'Send to lab address'
                    }
                ],
                'details_type': 'System',
                'received_time': '14:44',
                'address_line_1': '5 Test Street',
                'town': 'Some Town',
                'postcode': 'S11 3ER',
                'patient_name': 'John Smith',
                'participant_id': '1234',
                'user_name': 'Jim Smith'
            },
            'path_parameters': {
                'result_id': '1'
            },
            'session': {
                'user_data': {
                    'nhsid_useruid': 'userid1',
                    'first_name': 'Jim',
                    'last_name': 'Smith'
                },
            }
        }

        matched_participant_mock.return_value = {'participant_id': '1231231229'}
        validate_matched_mock.return_value = None
        # Act
        actual_response = self.service_module.update_unmatched_result.__wrapped__.__wrapped__(event_data)

        # Assert
        update_mock.assert_called_with(TableNames.RESULTS, dict(
            Key={
                'result_id': '1',
                'received_time': '14:44'
            },
            UpdateExpression='SET #workflow_state = :workflow_state, ' +
                             '#secondary_status = :secondary_status, #workflow_history = :workflow_history',
            ExpressionAttributeValues={
                ':workflow_state': 'process',
                ':secondary_status': 'WITH PROCESSOR',
                ':workflow_history': [
                    {
                        'selected_participant_id': 'ABC123',
                        'comments': 'comment',
                        'selection': 'Send to lab address',
                        'user': 'Jim Smith'
                    }
                ]
            },
            ExpressionAttributeNames={
                '#workflow_state': 'workflow_state',
                '#secondary_status': 'secondary_status',
                '#workflow_history': 'workflow_history'
            },
            ReturnValues='NONE'
        ))

        expected_response = {'message': 'record updated'}
        self.assertEqual(expected_response, actual_response)

        add_result_letter_to_processing_queue_mock.assert_not_called()

        audit_mock.assert_called_with(
            action=AuditActions.UPDATE_UNMATCHED_RESULT,
            session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}},
            additional_information={
                'result_id': '1',
                'selection': 'Send to lab address',
                'participant': 'ABC123'
            }
        )

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0008}),
                              call({'log_reference': LogReference.MNGRESMTCH0009})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch('common.utils.result_letter_utils.add_result_letter_to_processing_queue')
    def test_update_item_for_unmatched_result_succeeds_for_lab_details(
            self, add_result_letter_to_processing_queue_mock, validate_matched_mock,
            matched_participant_mock, audit_mock, log_mock, query_mock, update_mock):
        # Arrange
        event_data = {
            'body': {
                'user_id': 'userid1',
                'nhs_number': '12345',
                'workflow_state': ResultWorkflowState.PROCESS,
                'validation_errors': None,
                'secondary_status': ResultSecondaryStatus.WITH_PROCESSOR,
                'workflow_history': [{'comments': 'comment'}],
                'details_type': 'Lab',
                'received_time': '14:44',
                'address_line_1': '5 Test Street',
                'town': 'Some Town',
                'postcode': 'S11 3ER',
                'patient_name': 'John Smith',
                'participant_id': '1234',
                'user_name': 'Jim Smith'
            },
            'path_parameters': {
                'result_id': '1'
            },
            'session': {
                'user_data': {
                    'nhsid_useruid': 'userid1',
                    'first_name': 'Jim',
                    'last_name': 'Smith'
                },
            }
        }

        matched_participant_mock.return_value = {'participant_id': '1231231229'}
        validate_matched_mock.return_value = None
        # Act
        actual_response = self.service_module.update_unmatched_result.__wrapped__.__wrapped__(event_data)

        # Assert
        update_mock.assert_called_with(TableNames.RESULTS, dict(
            Key={'result_id': '1', 'received_time': '14:44'},
            UpdateExpression='SET #workflow_state = :workflow_state, ' +
                             '#secondary_status = :secondary_status, #workflow_history = :workflow_history',
            ExpressionAttributeValues={
                ':workflow_state': 'process',
                ':secondary_status': 'WITH PROCESSOR',
                ':workflow_history': [{'comments': 'comment', 'user': 'Jim Smith'}]
            },
            ExpressionAttributeNames={
                '#workflow_state': 'workflow_state',
                '#secondary_status': 'secondary_status',
                '#workflow_history': 'workflow_history'
            },
            ReturnValues='NONE'
        ))

        expected_response = {'message': 'record updated'}
        self.assertEqual(expected_response, actual_response)

        add_result_letter_to_processing_queue_mock.assert_not_called()

        audit_mock.assert_called_with(
            action=AuditActions.UPDATE_UNMATCHED_RESULT,
            session={'user_data': {'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'}},
            additional_information={'result_id': '1'}
        )

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0008}),
                              call({'log_reference': LogReference.MNGRESMTCH0009})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.accept_match')
    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.log')
    @patch(f'{UTILS_MODULE}.get_result')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    def test_accept_match_succeeds(
            self, accept_match_mock, audit_mock, query_participants_mock,
            participant_workflow_mock, utils_log_mock, get_result_mock,
            validate_matched_mock, log_mock, query_mock, update_mock):
        # Arrange
        participant_workflow_mock.return_value = {
            'participant_id': '1',
            'nhs_number': '1234567890',
            'is_ceased': True,
            'date_of_birth': '1990-01-01',
            'next_test_due_date': '2020-12-25'
        }

        get_result_mock.return_value = RESULT_MOCK

        query_participants_mock.return_value = PARTICIPANT_1
        validate_matched_mock.return_value = None
        accept_match_mock.return_value = {'message': 'record updated'}
        accept_match_event_data = self._read_json_file('accept_match_send_to_lab_address.json')
        # ntdd_mock.return_value = '2022-01-01'

        # Act
        actual_response = self.service_module.update_unmatched_result.__wrapped__.__wrapped__(accept_match_event_data)
        expected_response = {'message': 'record updated'}
        self.assertEqual(expected_response, actual_response)

        update_mock.assert_not_called()

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0008}),
                              call({'log_reference': LogReference.MNGRESMTCH0009})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.get_matched_participant_from_workflow_history')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{UTILS_MODULE}.validate_matched_result')
    def test_accept_match_fails(
            self, validate_matched_result_mock, query_participants_mock,
            participant_workflow_mock, audit_mock, log_mock, query_mock, update_mock):
        # Arrange
        validate_matched_result_mock.return_value = None
        participant_workflow_mock.return_value = {
            'participant_id': '1234'
        }

        # Act
        with self.assertRaises(Exception):
            self.service_module.update_unmatched_result.__wrapped__.__wrapped__(ACCEPT_MATCH_EVENT_DATA)

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0008}),
                              call({'log_reference': LogReference.MNGRESMTCH0010})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.get_participants_by_participant_ids')
    def test_update_item_for_unmatched_result_throws_exception(
        self, query_participants_mock, log_mock, query_mock, update_mock
    ):
        # Arrange
        update_mock.side_effect = Exception('Test')

        event_data = {
            'body': {
                'workflow_state': {
                    'status': ResultWorkflowState.PROCESS,
                    'secondary_status': ResultSecondaryStatus.WITH_PROCESSOR
                }
            },
            'path_parameters': {
                'result_id': '1'
            }
        }

        participant_items = [{
            'participant_id': 'test_participant',
            'nhs_number': '9876543210',
            'title': 'Miss',
            'first_name': 'Camila',
            'middle_names': 'Marta',
            'last_name': 'Cambello',
            'date_of_birth': '1997-03-03',
            'gender': 2,
            "address": {
                'address_line_1': 'Flat 6',
                'address_line_2': 'Ann skyway',
                'address_line_3': 'river',
                'address_line_4': 'Fake',
                'address_line_5': 'Mitchellfurt',
                'postcode': 'E0 0HY'
            }
        }]

        query_participants_mock.return_value = participant_items

        # Act
        with self.assertRaises(Exception):
            self.service_module.update_unmatched_result.__wrapped__(event_data)

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0008}),
                              call({'log_reference': LogReference.MNGRESMTCH0010})]

        # Assert
        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.segregated_query')
    def test_should_get_non_match_result_records_correctly(
            self, segregated_query_mock, log_mock, query_mock, update_mock):
        # given

        event_data = {
            'body': {
                'blah': '12'
            },
            'path_parameters': {
                'sending_lab': 'hello'
            },
            'session': 'session'
        }

        result_records = [
            {
                'result_id': '1',
                'decoded': {
                    'nhs_number': '123456789',
                    'first_name': 'MARY-JANE',
                    'last_name': 'O\'SULLIVAN',
                    'date_of_birth': '1979-09-01',
                    'address': '5 Pudsey Lane, TownVille, M1 1AP'
                },
                'workflow_state': ResultWorkflowState.PROCESS,
                'validation_errors': None,
                'secondary_status': ResultSecondaryStatus.NOT_STARTED,
                'received_time': '2020-01-01T01:20:30Z',
                'workflow_history': [
                    {'action': 'save'},
                    {'action': 'started'}
                ],
                'sending_lab': 'hello',
            }
        ]

        expected_response = [
            {
                'result_id': '1',
                'sending_lab': 'hello',
                'decoded': {
                    'nhs_number': '123456789',
                    'first_name': 'Mary-Jane',
                    'last_name': "O'Sullivan",
                    'date_of_birth': '1979-09-01',
                    'address': '5 Pudsey Lane, TownVille, M1 1AP'
                },
                'workflow_state': ResultWorkflowState.PROCESS,
                'validation_errors': None,
                'secondary_status': ResultSecondaryStatus.NOT_STARTED,
                'workflow_history': [
                    {'action': 'save'},
                    {'action': 'started'}
                ],
                'user_id': None,
                'user_name': None,
                'role_id': None,
                'lock_timestamp': None,
                'received_time': '2020-01-01T01:20:30Z',
                'rejected_reason': None,
                'possible_match_participants': None,
                'matched_to_participant': None
            }
        ]
        key_condition = Key('sending_lab').eq('hello')
        # when
        segregated_query_mock.return_value = result_records
        actual_response = self.service_module.get_unmatched_lab_results(event_data)
        # then
        segregated_query_mock.assert_called_with(TableNames.RESULTS, dict(
            IndexName='sending-lab',
            KeyConditionExpression=key_condition,
            ProjectionExpression=PROJECTION_EXPRESSION
        ))
        self.assertEqual(expected_response, actual_response)
        expected_log_calls = [
            call({'log_reference': LogReference.MNGRESMTCH0011}),
            call({'log_reference': LogReference.MNGRESMTCH0014, 'unmatched_result_count': 1}),
        ]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.log')
    def test_post_match_validation_updates_rejected(
            self, utils_log_mock, audit_mock, get_participant_mock,
            validate_matched_result_mock, log_mock,
            query_mock, update_mock):
        # Arrange
        event_data = {
            'body': {
                'user_id': 'userid1',
                'user_name': 'Jim Smith',
                'workflow_state': ResultWorkflowState.CHECK,
                'secondary_status': '',
                'workflow_history': [
                    {'selected_participant_id': '2'}, {'selected_participant_id': '4'},
                    {'action': 'check'}],
                'received_time': '14:44',
                'decoded': {
                    'slide_number': '1234'
                }
            },
            'path_parameters': {
                'result_id': '1'
            },
            'session': {
                'user_data': {
                    'nhsid_useruid': 'userid1',
                    'first_name': 'Jim',
                    'last_name': 'Smith'
                },
                'selected_role': {
                    'role_id': '1'
                }
            }
        }

        participant_items = [{
            'participant_id': '4',
            'nhs_number': '9876543210',
            'title': 'Miss',
            'first_name': 'Camila',
            'middle_names': 'Marta',
            'last_name': 'Cambello',
            'date_of_birth': '1997-03-03',
            'gender': 2,
            'address': ''
        }]
        get_participant_mock.return_value = participant_items
        validate_matched_result_mock.return_value = 'Test Rejection Reason'

        # Act
        actual_response = self.service_module.update_unmatched_result.__wrapped__.__wrapped__(event_data)

        # Assert
        update_mock.assert_called_with(TableNames.RESULTS, dict(
            Key={
                'result_id': '1',
                'received_time': '14:44'
            },
            UpdateExpression='SET #workflow_state = :workflow_state, ' +
                             '#secondary_status = :secondary_status, #rejected_reason = :rejected_reason, ' +
                             '#workflow_history = :workflow_history',
            ExpressionAttributeValues={
                ':workflow_state': 'rejected',
                ':secondary_status': ResultSecondaryStatus.NOT_ACTIONED,
                ':rejected_reason': 'Test Rejection Reason',
                ':workflow_history': [
                    {'selected_participant_id': '2', 'user': 'Jim Smith'},
                    {'selected_participant_id': '4', 'user': 'Jim Smith'},
                    {'action': 'check', 'user': 'Jim Smith'}]
            },
            ExpressionAttributeNames={
                '#workflow_state': 'workflow_state',
                '#secondary_status': 'secondary_status',
                '#rejected_reason': 'rejected_reason',
                '#workflow_history': 'workflow_history'
            },
            ReturnValues='NONE'
        ))

        expected_response = {'message': 'record updated'}
        self.assertEqual(expected_response, actual_response)

        audit_mock.assert_called_with(
            action=AuditActions.UPDATE_UNMATCHED_RESULT,
            session={'user_data': {
                'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'},
                'selected_role': {'role_id': '1'}},
            additional_information={'result_id': '1', 'selection': None, 'participant': '4'}
        )
        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0008}),
                              call({'log_reference': LogReference.MNGRESMTCH0009})]

        expected_utils_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0017}),
                                    call({'log_reference': LogReference.MNGRESMTCH0018,
                                          'reason': 'Test Rejection Reason'})]

        log_mock.assert_has_calls(expected_log_calls)
        utils_log_mock.assert_has_calls(expected_utils_log_calls)

    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{MODULE}.audit')
    @patch(f'{UTILS_MODULE}.log')
    def test_post_match_validation_updates_not_decoded(
            self, utils_log_mock, audit_mock, get_participant_mock,
            validate_matched_result_mock, log_mock,
            query_mock, update_mock):
        # Arrange
        event_data = {
            'body': {
                'user_id': 'userid1',
                'user_name': 'Jim Smith',
                'workflow_state': ResultWorkflowState.CHECK,
                'secondary_status': '',
                'workflow_history': [
                    {'selected_participant_id': '2'}, {'selected_participant_id': '4'},
                    {'action': 'check'}],
                'received_time': '14:44',
                'decoded': {
                    'slide_number': '1234'
                }
            },
            'path_parameters': {
                'result_id': '1'
            },
            'session': {
                'user_data': {
                    'nhsid_useruid': 'userid1',
                    'first_name': 'Jim',
                    'last_name': 'Smith'
                },
                'selected_role': {
                    'role_id': '1'
                }
            }
        }

        participant_items = [{
            'participant_id': '4',
            'nhs_number': '9876543210',
            'title': 'Miss',
            'first_name': 'Camila',
            'middle_names': 'Marta',
            'last_name': 'Cambello',
            'date_of_birth': '1997-03-03',
            'gender': 2,
            'address': ''
        }]
        get_participant_mock.return_value = participant_items
        validate_matched_result_mock.return_value = None

        # Act
        actual_response = self.service_module.update_unmatched_result.__wrapped__.__wrapped__(event_data)

        # Assert
        update_mock.assert_called_with(TableNames.RESULTS, dict(
            Key={
                'result_id': '1',
                'received_time': '14:44'
            },
            UpdateExpression='SET #workflow_state = :workflow_state, ' +
                             '#secondary_status = :secondary_status, ' +
                             '#workflow_history = :workflow_history',
            ExpressionAttributeValues={
                ':workflow_state': 'check',
                ':secondary_status': '',
                ':workflow_history': [
                    {'selected_participant_id': '2', 'user': 'Jim Smith'},
                    {'selected_participant_id': '4', 'user': 'Jim Smith'},
                    {'action': 'check', 'user': 'Jim Smith'}]
            },
            ExpressionAttributeNames={
                '#workflow_state': 'workflow_state',
                '#secondary_status': 'secondary_status',
                '#workflow_history': 'workflow_history'
            },
            ReturnValues='NONE'
        ))

        expected_response = {'message': 'record updated'}
        self.assertEqual(expected_response, actual_response)

        audit_mock.assert_called_with(
            action=AuditActions.UPDATE_UNMATCHED_RESULT,
            session={'user_data': {
                'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'},
                'selected_role': {'role_id': '1'}},
            additional_information={'result_id': '1', 'selection': None, 'participant': '4'}
        )
        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0008}),
                              call({'log_reference': LogReference.MNGRESMTCH0009})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{UTILS_MODULE}.validate_matched_result')
    @patch(f'{UTILS_MODULE}.get_participants_by_participant_ids')
    @patch(f'{MODULE}.audit')
    def test_post_match_validation_updates_no_history(self, audit_mock, query_participants_mock,
                                                      validate_matched_result_mock, log_mock,
                                                      query_mock, update_mock):
        # Arrange
        event_data = {
            'body': {
                'user_id': 'userid1',
                'user_name': 'Jim Smith',
                'workflow_state': ResultWorkflowState.CHECK,
                'secondary_status': '',
                'received_time': '14:44'
            },
            'path_parameters': {
                'result_id': '1'
            },
            'session': {
                'user_data': {
                    'nhsid_useruid': 'userid1',
                    'first_name': 'Jim',
                    'last_name': 'Smith'
                },
                'selected_role': {
                    'role_id': '1'
                }
            }
        }

        participant_items = [{
            'participant_id': 'test_participant',
            'nhs_number': '9876543210',
            'title': 'Miss',
            'first_name': 'Camila',
            'middle_names': 'Marta',
            'last_name': 'Cambello',
            'date_of_birth': '1997-03-03',
            'gender': 2
        }]

        query_participants_mock.return_value = participant_items

        # Act
        actual_response = self.service_module.update_unmatched_result.__wrapped__.__wrapped__(event_data)

        # Assert
        update_mock.assert_called_with(TableNames.RESULTS, dict(
            Key={
                'result_id': '1',
                'received_time': '14:44'
            },
            UpdateExpression='SET #workflow_state = :workflow_state, ' +
                             '#secondary_status = :secondary_status',

            ExpressionAttributeValues={
                ':workflow_state': 'check',
                ':secondary_status': '',
            },
            ExpressionAttributeNames={
                '#workflow_state': 'workflow_state',
                '#secondary_status': 'secondary_status',
            },
            ReturnValues='NONE'
        ))

        expected_response = {'message': 'record updated'}
        self.assertEqual(expected_response, actual_response)

        validate_matched_result_mock.assert_not_called()

        audit_mock.assert_called_with(
            action=AuditActions.UPDATE_UNMATCHED_RESULT,
            session={'user_data': {
                'nhsid_useruid': 'userid1', 'first_name': 'Jim', 'last_name': 'Smith'},
                'selected_role': {'role_id': '1'}},
            additional_information={'result_id': '1'}
        )
        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0008}),
                              call({'log_reference': LogReference.MNGRESMTCH0009})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.get_sender_details_and_validate')
    @patch(f'{MODULE}.audit')
    @patch(f'{MODULE}.get_results_by_result_keys')
    def test_get_sender_details_for_rejected_result_succeeds(
            self, get_results_mock, audit_mock, sender_details_mock, log_mock, query_mock, update_mock):
        # Arrange
        sender_details_mock.return_value = {
            'name': 'Lloyds Pharmacy',
            'address': {
                'address_line_1': 'BROADGATE LANE',
                'address_line_2': 'HORSFORTH',
                'address_line_3': 'LEEDS',
                'address_line_4': 'WEST YORKSHIRE',
                'postcode': 'LS18 4SE'
            }
        }

        get_results_mock.return_value = [{
            'result_id': '2',
            'sending_lab': 'CHR3',
            'decoded': {
                'nhs_number': '1234',
                'first_name': 'MARY-JANE',
                'last_name': 'O\'SULLIVAN',
                'date_of_birth': '1979-09-01',
                'address': '5 Pudsey Lane, TownVille, M1 1AP',
            },
            'rejected_reason': 'Slide ref duplicate',
            'validation_errors': ['Slide ref duplicate'],
            'sanitised_nhais_cipher': 'WIG'
        }]

        # Act
        actual_response = self.service_module.get_duplicate_test_result_details([{'result_id': '2'}])

        # Assert
        expected_response = [{
            'result_id': '2',
            'sending_lab': 'CHR3',
            'decoded': {
                'nhs_number': '1234',
                'first_name': 'MARY-JANE',
                'last_name': "O'SULLIVAN",
                'date_of_birth': '1979-09-01',
                'address': '5 Pudsey Lane, TownVille, M1 1AP',
                'sender_name': 'Lloyds Pharmacy',
                'sender_address': {
                    'address_line_1': 'BROADGATE LANE',
                    'address_line_2': 'HORSFORTH',
                    'address_line_3': 'LEEDS',
                    'address_line_4': 'WEST YORKSHIRE',
                    'postcode': 'LS18 4SE'
                },
                'organisation_code': None
            },
            'rejected_reason': 'Slide ref duplicate',
            'validation_errors': ['Slide ref duplicate'],
            'sanitised_nhais_cipher': 'WIG'
        }]

        self.assertEqual(expected_response, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.DUPRES0001}),
                              call({'log_reference': LogReference.DUPRES0002}),
                              call({'log_reference': LogReference.DUPRES0004})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.get_sender_details_and_validate')
    @patch(f'{MODULE}.audit')
    @patch(f'{MODULE}.get_results_by_result_keys')
    def test_get_duplicate_test_result_sender_details_not_found(
            self, get_results_mock, audit_mock, sender_details_mock, log_mock, query_mock, update_mock):
        # Arrange
        query_mock.return_value = [{
            'result_id': '2',
            'sending_lab': 'CHR3',
            'decoded': {
                'nhs_number': '1234',
                'first_name': 'MARY-JANE',
                'last_name': 'O\'SULLIVAN',
                'date_of_birth': '1979-09-01',
                'address': '5 Pudsey Lane, TownVille, M1 1AP',
            },
            'rejected_reason': 'Slide ref duplicate',
            'validation_errors': ['Slide ref duplicate'],
            'sanitised_nhais_cipher': 'WIG'
        }]

        get_results_mock.return_value = [{
            'result_id': '2',
            'sending_lab': 'CHR3',
            'decoded': {
                'nhs_number': '1234',
                'first_name': 'MARY-JANE',
                'last_name': 'O\'SULLIVAN',
                'date_of_birth': '1979-09-01',
                'address': '5 Pudsey Lane, TownVille, M1 1AP',
            },
            'rejected_reason': 'Slide ref duplicate',
            'validation_errors': ['Slide ref duplicate'],
            'sanitised_nhais_cipher': 'WIG'
        }]

        sender_details_mock.return_value = {
            'message': 'Sender not found',
            'sender_details': {'NHAIS_cipher': 'WIG'}
        }

        # Act
        actual_response = self.service_module.get_duplicate_test_result_details([{'result_id': '2'}])

        # Assert
        expected_response = [{
            'result_id': '2',
            'sending_lab': 'CHR3',
            'decoded': {
                'nhs_number': '1234',
                'first_name': 'MARY-JANE',
                'last_name': "O'SULLIVAN",
                'date_of_birth': '1979-09-01',
                'address': '5 Pudsey Lane, TownVille, M1 1AP',
                'sender': {
                    'message': 'Sender not found',
                    'sender_details': {'NHAIS_cipher': 'WIG'}
                }
            },
            'rejected_reason': 'Slide ref duplicate',
            'validation_errors': ['Slide ref duplicate'],
            'sanitised_nhais_cipher': 'WIG'
        }]

        self.assertEqual(expected_response, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.DUPRES0001}),
                              call({'log_reference': LogReference.DUPRES0003}),
                              call({'log_reference': LogReference.DUPRES0004})]

        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.get_sender_details_and_validate')
    @patch(f'{MODULE}.audit')
    def test_get_duplicate_test_result_throws_exception(
            self, audit_mock, sender_details_mock, log_mock, query_mock, update_mock):
        # Arrange
        query_mock.return_value = [{
            'result_id': '2',
            'sending_lab': 'CHR3',
            'decoded': {
                'nhs_number': '1234',
                'first_name': 'MARY-JANE',
                'last_name': 'O\'SULLIVAN',
                'date_of_birth': '1979-09-01',
                'address': '5 Pudsey Lane, TownVille, M1 1AP',
            },
            'rejected_reason': 'Slide ref duplicate',
            'validation_errors': ['Slide ref duplicate']
        }]

        sender_details_mock.side_effect = Mock(side_effect=Exception('Attempt to load duplicate test result failed'))

        # Act
        with self.assertRaises(Exception):
            self.service_module.get_duplicate_test_result_details(['2'])

        expected_log_calls = [call({'log_reference': LogReference.DUPRES0001}),
                              call({'log_reference': LogReference.DUPRES0005})]

        # Assert
        log_mock.assert_has_calls(expected_log_calls)

    @patch(f'{MODULE}.get_participants_by_participant_ids')
    @patch(f'{MODULE}.get_participant_by_participant_id')
    @patch(f'{MODULE}.audit')
    def test_query_for_getting_unmatched_rejected_result_succeeds(
            self, audit_mock, query_participant_mock, query_participants_mock, log_mock, query_mock, update_mock):
        # Arrange
        query_mock.return_value = [{
            'result_id': '1',
            'sending_lab': 'CHR3',
            'decoded': {
                'nhs_number': '1234',
                'first_name': 'MARY-JANE',
                'last_name': 'O\'SULLIVAN',
                'date_of_birth': '1979-09-01',
                'address': '5 Pudsey Lane, TownVille, M1 1AP',
            },
            'workflow_state': ResultWorkflowState.PROCESS,
            'validation_errors': None,
            'secondary_status': ResultSecondaryStatus.NOT_STARTED,
            'workflow_history': [],
            'user_id': None,
            'user_name': 'Jim Smith',
            'lock_timestamp': None,
            'received_time': '2020-01-01T09:30:00',
            'rejected_reason': None,
            'matched_to_participant': 'test'
        }]

        query_participant_mock.return_value = {
            'participant_id': 'test_participant',
            'nhs_number': '9876543210',
            'title': 'Miss',
            'first_name': 'Camila',
            'middle_names': 'Marta',
            'last_name': 'Cambello',
            'date_of_birth': '1997-03-03',
            'gender': 2,
            'address': {
                'address_line_1': 'Flat 6',
                'address_line_2': 'Ann skyway',
                'address_line_3': 'river',
                'address_line_4': 'Fake',
                'address_line_5': 'Mitchellfurt',
                'postcode': 'E0 0HY'
            }
        }

        event_data = {
            'path_parameters': {
                'result_id': '1'
            },
            'session': {
                'user_data': {
                    'first_name': 'Jim',
                    'last_name': 'Smith'
                }
            }
        }

        # Act
        actual_response = self.service_module.get_unmatched_result_with_participant_matches.__wrapped__(event_data)

        # Assert
        expected_condition = Key('result_id').eq('1')
        query_mock.assert_called_with(TableNames.RESULTS, dict(
            KeyConditionExpression=expected_condition,
            ProjectionExpression=PROJECTION_EXPRESSION
        ))

        query_participants_mock.assert_not_called()

        expected_response = {
            'result': {
                'result_id': '1',
                'sending_lab': 'CHR3',
                'decoded': {
                    'nhs_number': '1234',
                    'date_of_birth': '1979-09-01',
                    'first_name': 'Mary-Jane',
                    'last_name': 'O\'Sullivan',
                    'address': '5 Pudsey Lane, TownVille, M1 1AP'
                },
                'workflow_state': ResultWorkflowState.PROCESS,
                'validation_errors': None,
                'secondary_status': ResultSecondaryStatus.NOT_STARTED,
                'workflow_history': [],
                'user_id': None,
                'user_name': 'Jim Smith',
                'role_id': None,
                'lock_timestamp': None,
                'received_time': '2020-01-01T09:30:00',
                'rejected_reason': None,
                'possible_match_participants': None,
                'matched_to_participant': 'test'
            },
            'matched_participant': {
                'participant_id': 'test_participant',
                'nhs_number': '9876543210',
                'title': 'Miss',
                'first_name': 'Camila',
                'middle_names': 'Marta',
                'last_name': 'Cambello',
                'date_of_birth': '1997-03-03',
                'gender': 2,
                'address': {
                    'address_line_1': 'Flat 6',
                    'address_line_2': 'Ann skyway',
                    'address_line_3': 'river',
                    'address_line_4': 'Fake',
                    'address_line_5': 'Mitchellfurt',
                    'postcode': 'E0 0HY'
                }
            }
        }

        self.maxDiff = None
        self.assertEqual(expected_response, actual_response)

        audit_mock.assert_called_with(
            AuditActions.GET_UNMATCHED_RESULT,
            session={'user_data': {'first_name': 'Jim', 'last_name': 'Smith'}}
        )

        expected_log_calls = [call({'log_reference': LogReference.MNGRESMTCH0005}),
                              call({'log_reference': LogReference.MNGRESMTCH0006})]

        log_mock.assert_has_calls(expected_log_calls)

    def _read_json_file(self, file_name):
        current_path = os.path.dirname(os.path.realpath(__file__))
        full_file_path = os.path.join(current_path, 'test_data', file_name)
        with open(full_file_path) as file:
            return json.loads(file.read())
