from common.log import log
from common.utils import json_return_message, json_return_object
from common.utils.reference_utils import get_reference_by_cipher_and_key
from manage_non_gp_senders.log_references import LogReference


def get_sender_by_cipher_and_code(event_data):
    cipher = event_data['path_parameters']['cipher']
    sender_code = event_data['path_parameters']['sender_code']

    key = f'{cipher}#Sender#{sender_code}'
    sender = get_reference_by_cipher_and_key(cipher, key)

    if not sender:
        log({
            'log_reference': LogReference.MANAGESENDERS410,
            'cipher': cipher,
            'sender_code': sender_code,
        })
        return json_return_message(404, 'No sender found')

    log(LogReference.MANAGESENDERS200)
    return json_return_object(200, sender)
