from common.log import log
from common.utils import generate_crm_list, json_return_message, json_return_object
from common.utils.dynamodb_access.operations import dynamodb_update_item
from common.utils.dynamodb_access.table_names import TableNames
from common.utils.dynamodb_helper_utils import build_update_query
from common.utils.reference_utils import get_reference_by_cipher_and_key
from manage_non_gp_senders.log_references import LogReference

VALID_NON_GP_REFERENCE_SUBTYPES = ["2", "3", "4", "5", "6", "7"]


def change_sender_activation(new_value, event_data):
    cipher = event_data['path_parameters']['cipher']
    sender_code = event_data['path_parameters']['sender_code']

    key = f'{cipher}#Sender#{sender_code}'
    sender = get_reference_by_cipher_and_key(cipher, key)

    if not sender:
        log({
            'log_reference': LogReference.MANAGESENDERS410,
            'cipher': cipher,
            'sender_code': sender_code,
        })
        return json_return_message(404, 'No sender found')

    log(LogReference.MANAGESENDERS200)
    sender_validation_error = can_update_state(sender, new_value)
    if sender_validation_error:
        return json_return_message(409, sender_validation_error)

    update_activation(event_data['body'], event_data['session'], sender, new_value)
    return json_return_object(200)


def can_update_state(sender, new_value):
    if sender.get('active', True) == new_value:
        log({
            'log_reference': LogReference.MANAGESENDERS420,
            'cipher': sender['source_cipher'],
            'sender_code': sender['reference_id'],
            'activate': sender['active'],
        })
        return 'Sender is already in the requested state'
    if sender['reference_subtype'] not in VALID_NON_GP_REFERENCE_SUBTYPES:
        log({
            'log_reference': LogReference.MANAGESENDERS421,
            'cipher': sender['source_cipher'],
            'sender_code': sender['reference_id'],
            'source_type': sender['reference_subtype'],
        })
        return 'Cannot edit sender with this source type'


def update_activation(crm, session, sender, new_value):
    updates = {
        'active': new_value,
    }
    if crm.get('crm_number') or crm.get('comments'):
        new_crm = generate_crm_list(crm, session, sender.get('crm', []))
        updates['crm'] = new_crm

    expression, names, values = build_update_query(updates)
    dynamodb_update_item(TableNames.REFERENCE, dict(
        Key={'source_cipher': sender['source_cipher'], 'key': sender['key']},
        UpdateExpression=expression,
        ExpressionAttributeValues=values,
        ExpressionAttributeNames=names,
        ReturnValues='NONE'
    ))
