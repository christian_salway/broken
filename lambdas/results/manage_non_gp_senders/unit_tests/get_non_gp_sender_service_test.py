from unittest import TestCase
from mock import Mock, patch
from manage_non_gp_senders.get_non_gp_sender_service import get_sender_by_cipher_and_code
from common.test_assertions.api_assertions import assertApiResponse
from manage_non_gp_senders.log_references import LogReference

SESSION = {'session_key': '1234567890'}


class TestGetNonGpSender(TestCase):
    module = 'manage_non_gp_senders.get_non_gp_sender_service'

    def setUp(self):
        self.context = Mock()
        self.context.function_name = ''

    @patch(f'{module}.log')
    @patch(f'{module}.get_reference_by_cipher_and_key')
    def test_get_sender_by_cipher_and_code_returns_sender_when_found(self, get_reference_mock, log_mock):
        # Arrange
        get_reference_mock.return_value = {'sender': 'a_sender'}
        # Act
        actual = get_sender_by_cipher_and_code({
            'path_parameters': {'cipher': 'AA', 'sender_code': 'ABCDE123'},
            'body': {},
            'session': SESSION
        })
        # Assert
        log_mock.assert_called_once_with(LogReference.MANAGESENDERS200)
        get_reference_mock.assert_called_once_with('AA', 'AA#Sender#ABCDE123')
        assertApiResponse(self, 200, {'sender': 'a_sender'}, actual)

    @patch(f'{module}.log')
    @patch(f'{module}.get_reference_by_cipher_and_key')
    def test_get_sender_by_cipher_and_code_returns_404_when_no_match_found(self, get_reference_mock, log_mock):
        # Arrange
        get_reference_mock.return_value = None
        # Act
        actual = get_sender_by_cipher_and_code({
            'path_parameters': {'cipher': 'AA', 'sender_code': 'ABCDE123'},
            'body': {},
            'session': SESSION
        })
        # Assert
        log_mock.assert_called_once_with({
            'log_reference': LogReference.MANAGESENDERS410,
            'cipher': 'AA',
            'sender_code': 'ABCDE123',
        })
        get_reference_mock.assert_called_once_with('AA', 'AA#Sender#ABCDE123')
        assertApiResponse(self, 404, {'message': 'No sender found'}, actual)
