from unittest import TestCase
from ddt import data, ddt, unpack
from mock import Mock, patch, call
from common.utils import json_return_object
from common.test_assertions.api_assertions import assertApiResponse
from manage_non_gp_senders.log_references import LogReference
from manage_non_gp_senders.manage_non_gp_senders import lambda_handler

SESSION = {'session_key': '1234567890'}


@ddt
class TestManageNonGpSenders(TestCase):
    module = 'manage_non_gp_senders.manage_non_gp_senders'

    def setUp(self):
        self.context = Mock()
        self.context.function_name = ''

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.get_sender_by_cipher_and_code')
    def test_lambda_handler_calls_get_method(self, get_sender_mock, request_info_mock, log_mock):
        # Arrange
        request_info_mock.return_value = get_request_info('AA', 'ABCDE123')
        get_sender_mock.return_value = json_return_object(200, {'sender': 'a_sender'})
        # Act
        actual_response = lambda_handler.__wrapped__({}, {})
        # Assert
        log_mock.assert_called_once_with({
            'log_reference': LogReference.MANAGESENDERS001,
            'request_keys': [],
        })
        get_sender_mock.assert_called_once_with(
            {'path_parameters': {'cipher': 'AA', 'sender_code': 'ABCDE123'}, 'body': {}, 'session': SESSION})
        assertApiResponse(self, 200, {'sender': 'a_sender'}, actual_response)

    @unpack
    @data(
        ('activate', True),
        ('deactivate', False)
    )
    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    @patch(f'{module}.change_sender_activation')
    def test_lambda_handler_calls_activate_method(
        self, path_part, activate_arg, change_sender_activation_mock, request_info_mock, log_mock
    ):
        # Arrange
        request_body = {
            'crm_number': '123456',
            'comments': 'This is a comment'
        }
        request_info_mock.return_value = get_request_info('AA', 'ABCDE123', request_body, 'POST', path_part)
        change_sender_activation_mock.return_value = json_return_object(200)
        # Act
        actual_response = lambda_handler.__wrapped__({}, {})
        # Assert
        log_mock.assert_called_once_with({
            'log_reference': LogReference.MANAGESENDERS001,
            'request_keys': ['crm_number', 'comments'],
        })
        change_sender_activation_mock.assert_called_once_with(
            activate_arg,
            {'path_parameters': {'cipher': 'AA', 'sender_code': 'ABCDE123'}, 'body': request_body, 'session': SESSION})
        assertApiResponse(self, 200, {}, actual_response)

    @patch(f'{module}.log')
    @patch(f'{module}.get_api_request_info_from_event')
    def test_lambda_handler_handles_invalid_method(self, request_info_mock, log_mock):
        # Arrange
        request_info_mock.return_value = {
            'path_parameters': {'participant_id': '123'},
            'body': {},
            'session': SESSION,
        }, 'INVALID', '/api/sender'
        # Act
        actual_response = lambda_handler.__wrapped__({}, {})
        # Assert
        log_mock.assert_has_calls([
            call({
                'log_reference': LogReference.MANAGESENDERS001,
                'request_keys': [],
            }),
            call(LogReference.MANAGESENDERS400)
        ])
        assertApiResponse(self, 400, {'message': 'unrecognised method/resource combination'}, actual_response)


def get_request_info(cipher, sender_code, body={}, method='GET', path_part=None):
    path = '/api/sender/{cipher}/{sender_code}' + (f'/{path_part}' if path_part else '')
    return {
        'path_parameters': {'cipher': cipher, 'sender_code': sender_code},
        'body': body,
        'session': SESSION
    }, method, path
