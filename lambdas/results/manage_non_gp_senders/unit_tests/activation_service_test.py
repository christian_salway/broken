from unittest import TestCase
from mock import Mock, call, patch
from ddt import ddt, data, unpack
from common.test_assertions.api_assertions import assertApiResponse
from common.utils.dynamodb_access.table_names import TableNames
from manage_non_gp_senders.log_references import LogReference
from manage_non_gp_senders.activation_service import can_update_state, change_sender_activation, update_activation

SESSION = {
    'session_key': '1234567890',
    "user_data": {
        "first_name": "First",
        "last_name": "Last"
    },
}

SENDER = {
    'source_cipher': 'AA',
    'key': 'AA#Sender#ABCDE123',
    'active': True,
    'reference_id': 'ABCDE123',
}


@ddt
class TestNonGpSenderActivation(TestCase):
    module = 'manage_non_gp_senders.activation_service'

    def setUp(self):
        self.context = Mock()
        self.context.function_name = ''

    @data(True, False)
    @patch(f'{module}.can_update_state')
    @patch(f'{module}.update_activation')
    @patch(f'{module}.log')
    @patch(f'{module}.get_reference_by_cipher_and_key')
    def test_change_sender_activation(
        self, new_activation, get_reference_mock, log_mock, update_activation_mock, can_update_mock
    ):
        # Arrange
        sender = SENDER.copy()
        sender['active'] = not new_activation
        get_reference_mock.return_value = sender
        can_update_mock.return_value = None
        # Act
        body = {
            'crm_number': '123456',
            'comments': 'This is a comment'
        }
        actual = change_sender_activation(new_activation, {
            'path_parameters': {'cipher': 'AA', 'sender_code': 'ABCDE123'},
            'body': body,
            'session': SESSION
        })
        # Assert
        assertApiResponse(self, 200, {}, actual)
        log_mock.assert_called_once_with(LogReference.MANAGESENDERS200)
        get_reference_mock.assert_called_once_with('AA', 'AA#Sender#ABCDE123')
        update_activation_mock.assert_called_once_with(body, SESSION, sender, new_activation)
        can_update_mock.assert_called_once_with(sender, new_activation)

    @data(True, False)
    @patch(f'{module}.can_update_state')
    @patch(f'{module}.update_activation')
    @patch(f'{module}.log')
    @patch(f'{module}.get_reference_by_cipher_and_key')
    def test_incorrect_sender_state(
        self, new_activation, get_reference_mock, log_mock, update_activation_mock, can_update_mock
    ):
        # Arrange
        sender = SENDER.copy()
        sender['active'] = new_activation
        get_reference_mock.return_value = sender
        validation_error = 'This is an error message'
        can_update_mock.return_value = validation_error
        # Act
        body = {
            'crm_number': '123456',
            'comments': 'This is a comment'
        }
        actual = change_sender_activation(new_activation, {
            'path_parameters': {'cipher': 'AA', 'sender_code': 'ABCDE123'},
            'body': body,
            'session': SESSION
        })
        # Assert
        assertApiResponse(self, 409, {'message': validation_error}, actual)
        log_mock.assert_has_calls([
            call(LogReference.MANAGESENDERS200),
        ])
        get_reference_mock.assert_called_once_with('AA', 'AA#Sender#ABCDE123')
        update_activation_mock.assert_not_called()
        can_update_mock.assert_called_once_with(sender, new_activation)

    @patch(f'{module}.can_update_state')
    @patch(f'{module}.update_activation')
    @patch(f'{module}.log')
    @patch(f'{module}.get_reference_by_cipher_and_key')
    def test_sender_not_found(
        self, get_reference_mock, log_mock, update_activation_mock, can_update_mock
    ):
        # Arrange
        get_reference_mock.return_value = None
        # Act
        actual = change_sender_activation(False, {
            'path_parameters': {'cipher': 'AA', 'sender_code': 'ABCDE123'},
            'body': {},
            'session': SESSION
        })
        # Assert
        assertApiResponse(self, 404, {'message': 'No sender found'}, actual)
        log_mock.assert_has_calls([
            call({
                'log_reference': LogReference.MANAGESENDERS410,
                'cipher': 'AA',
                'sender_code': 'ABCDE123',
            }),
        ])
        get_reference_mock.assert_called_once_with('AA', 'AA#Sender#ABCDE123')
        update_activation_mock.assert_not_called()
        can_update_mock.assert_not_called()

    @unpack
    @data(
        (True, False, "2", None, None),
        (False, True, "2", None, None),
        (True, True, "2", 'Sender is already in the requested state', {
            'log_reference': LogReference.MANAGESENDERS420,
            'cipher': SENDER['source_cipher'],
            'sender_code': SENDER['reference_id'],
            'activate': True,
        }),
        (False, False, "2", 'Sender is already in the requested state', {
            'log_reference': LogReference.MANAGESENDERS420,
            'cipher': SENDER['source_cipher'],
            'sender_code': SENDER['reference_id'],
            'activate': False,
        }),
        (False, True, "1", 'Cannot edit sender with this source type', {
            'log_reference': LogReference.MANAGESENDERS421,
            'cipher': SENDER['source_cipher'],
            'sender_code': SENDER['reference_id'],
            'source_type': "1",
        }),
        (False, True, "8", 'Cannot edit sender with this source type', {
            'log_reference': LogReference.MANAGESENDERS421,
            'cipher': SENDER['source_cipher'],
            'sender_code': SENDER['reference_id'],
            'source_type': "8",
        }),
    )
    @patch(f'{module}.log')
    def test_can_update_state(self, sender_activation, new_activation, source_type, expected, expected_log, log_mock):
        # Arrange
        sender = SENDER.copy()
        sender['active'] = sender_activation
        sender['reference_subtype'] = source_type
        # Act
        actual = can_update_state(sender, new_activation)
        # Assert
        self.assertEqual(actual, expected)
        if expected_log:
            log_mock.assert_called_once_with(expected_log)

    @data(True, False)
    @patch(f'{module}.generate_crm_list')
    @patch(f'{module}.build_update_query')
    @patch(f'{module}.dynamodb_update_item')
    def test_update_activation_with_crm(self, activation, update_item_mock, build_query_mock, generate_crm_mock):
        # Arrange
        sender = SENDER.copy()
        crm = {'crm_number': '12345678', 'comment': 'This is a comment'}
        generate_crm_mock.return_value = [crm]
        build_query_mock.return_value = (
            'SET #active = :active, #crm = :crm',
            {'#active': 'active', '#crm': 'crm'},
            {':active': activation, ':crm': [{'crm_numer': '12345678'}]}
        )
        # Act
        update_activation(crm, SESSION, sender, activation)
        # Assert
        generate_crm_mock.assert_called_once_with(crm, SESSION, [])
        build_query_mock.assert_called_once_with({
            'active': activation,
            'crm': [crm]
        })
        update_item_mock.assert_called_once_with(TableNames.REFERENCE, dict(
            Key={'source_cipher': 'AA', 'key': 'AA#Sender#ABCDE123'},
            UpdateExpression='SET #active = :active, #crm = :crm',
            ExpressionAttributeValues={':active': activation, ':crm': [{'crm_numer': '12345678'}]},
            ExpressionAttributeNames={'#active': 'active', '#crm': 'crm'},
            ReturnValues='NONE'
        ))

    @data(True, False)
    @patch(f'{module}.generate_crm_list')
    @patch(f'{module}.build_update_query')
    @patch(f'{module}.dynamodb_update_item')
    def test_update_activation_no_crm(self, activation, update_item_mock, build_query_mock, generate_crm_mock):
        # Arrange
        sender = SENDER.copy()
        build_query_mock.return_value = (
            'SET #active = :active',
            {'#active': 'active'},
            {':active': activation}
        )
        # Act
        update_activation({}, SESSION, sender, activation)
        # Assert
        generate_crm_mock.assert_not_called()
        build_query_mock.assert_called_once_with({
            'active': activation,
        })
        update_item_mock.assert_called_once_with(TableNames.REFERENCE, dict(
            Key={'source_cipher': 'AA', 'key': 'AA#Sender#ABCDE123'},
            UpdateExpression='SET #active = :active',
            ExpressionAttributeValues={':active': activation},
            ExpressionAttributeNames={'#active': 'active'},
            ReturnValues='NONE'
        ))
