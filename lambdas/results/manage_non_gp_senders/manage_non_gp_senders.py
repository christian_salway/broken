from functools import partial
from common.log import log
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.api_utils import get_api_request_info_from_event
from common.utils import json_return_message
from manage_non_gp_senders.get_non_gp_sender_service import get_sender_by_cipher_and_code
from manage_non_gp_senders.log_references import LogReference
from manage_non_gp_senders.activation_service import change_sender_activation


@lambda_entry_point
def lambda_handler(event, _):

    BASE_URL = '/api/sender'
    REQUEST_FUNCTIONS = {
        ('GET', BASE_URL + '/{cipher}/{sender_code}'): get_sender_by_cipher_and_code,
        ('POST', BASE_URL + '/{cipher}/{sender_code}/deactivate'): partial(change_sender_activation, False),
        ('POST', BASE_URL + '/{cipher}/{sender_code}/activate'): partial(change_sender_activation, True),
    }

    event_data, http_method, resource = get_api_request_info_from_event(event)
    request_body = event_data.get('body', {})

    log({
        'log_reference': LogReference.MANAGESENDERS001,
        'request_keys': list(request_body.keys()),
    })

    response = REQUEST_FUNCTIONS.get((http_method, resource), bad_request)(event_data)
    return response


def bad_request(_):
    log(LogReference.MANAGESENDERS400)
    return json_return_message(400, 'unrecognised method/resource combination')
