from unittest import TestCase
from mock import Mock, patch
from manage_non_gp_senders.manage_non_gp_senders import lambda_handler
from common.test_assertions.api_assertions import assertApiResponse
from manage_non_gp_senders.component_tests.test_helpers import build_event, mock_env_vars

reference_table_mock = Mock()


@patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
       Mock(return_value=reference_table_mock))
@patch('os.environ', mock_env_vars)
class TestGetNonGpSender(TestCase):
    module = 'manage_non_gp_senders.manage_non_gp_senders'

    def setUp(self):
        reference_table_mock.reset_mock()
        self.context = Mock()
        self.context.function_name = ''

    def test_can_get_sender_when_sender_exists(self):
        # Arrange
        reference_table_mock.get_item.return_value = {'Item': {'sender': 'a_sender'}}
        event = build_event('AA', 'ABCDE123', '12345678', 'session123', '/api/sender/{cipher}/{sender_code}')
        # Act
        actual_response = lambda_handler(event, self.context)
        # Assert
        assertApiResponse(self, 200, {'sender': 'a_sender'}, actual_response)

    def test_returns_404_when_sender_does_not_exist(self):
        # Arrange
        reference_table_mock.get_item.return_value = {}
        event = build_event('AA', 'ABCDE123', '12345678', 'session123', '/api/sender/{cipher}/{sender_code}')
        # Act
        actual_response = lambda_handler(event, self.context)
        # Assert
        assertApiResponse(self, 404, {'message': 'No sender found'}, actual_response)
