import json


mock_env_vars = {
    'DYNAMODB_REFERENCE': 'REFERENCE_TABLE',
    'AUDIT_QUEUE_URL': 'the_audit_queue_url',
}


def build_event(
            cipher,
            sender_code,
            user_uid,
            session_id,
            resource,
            body={},
            method='GET'):
    return {
        'httpMethod': method,
        'resource': resource,
        'pathParameters': {'cipher': cipher, 'sender_code': sender_code},
        'headers': {
            'request_id': 'requestID',
        },
        'body': json.dumps(body),
        'requestContext': {
            'authorizer': {
                'principalId': 'blah',
                'session': json.dumps({
                    "session_id": session_id,
                    "user_data": {
                        "nhsid_useruid": user_uid,
                        "first_name": "firsty",
                        "last_name": "lasty"
                    },
                    "selected_role": {
                        "workgroups": ['cervicalscreening'],
                        "role_id": "test_role",
                        "organisation_code": "test_organisation_code"
                    }
                })
            }
        }
    }
