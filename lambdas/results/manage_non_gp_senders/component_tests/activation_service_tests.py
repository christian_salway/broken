from datetime import datetime, timezone
from unittest import TestCase
from ddt import data, ddt, unpack
from mock import Mock, patch
from manage_non_gp_senders.manage_non_gp_senders import lambda_handler
from common.test_assertions.api_assertions import assertApiResponse
from manage_non_gp_senders.component_tests.test_helpers import build_event, mock_env_vars

reference_table_mock = Mock()

SENDER = {
    'source_cipher': 'AA',
    'key': 'AA#Sender#ABCDE123',
    'active': False,
    'reference_id': 'ABCDE123',
    'reference_subtype': "2",
}
REQUEST_BODY = {
    'crm_number': "12345678",
    'comments': 'This is a comment',
}
example_date = datetime(2020, 10, 18, 13, 48, 8, tzinfo=timezone.utc)
datetime_mock = Mock(wraps=datetime)
datetime_mock.now.return_value = example_date


@ddt
@patch('common.utils.datetime', datetime_mock)
@patch('common.utils.dynamodb_access.operations.get_dynamodb_table',
       Mock(return_value=reference_table_mock))
@patch('os.environ', mock_env_vars)
class TestGetNonGpSender(TestCase):
    module = 'manage_non_gp_senders.manage_non_gp_senders'

    def setUp(self):
        reference_table_mock.reset_mock()
        self.context = Mock()
        self.context.function_name = ''

    @data(True, False)
    def test_can_change_sender_activation(self, new_activation):
        # Arrange
        endpoint = 'activate' if new_activation else 'deactivate'
        sender = SENDER.copy()
        sender['active'] = not new_activation
        reference_table_mock.get_item.return_value = {'Item': sender}
        event = build_event(
            'AA',
            'ABCDE123',
            '12345678',
            'session123',
            f'/api/sender/{{cipher}}/{{sender_code}}/{endpoint}',
            body=REQUEST_BODY,
            method='POST',
        )
        # Act
        actual_response = lambda_handler(event, self.context)
        # Assert
        assertApiResponse(self, 200, {}, actual_response)
        reference_table_mock.update_item.assert_called_once_with(
            Key={'source_cipher': 'AA', 'key': 'AA#Sender#ABCDE123'},
            UpdateExpression='SET #active = :active, #crm = :crm',
            ExpressionAttributeValues={
                ':active': new_activation,
                ':crm': [{
                    'crm_number': '12345678',
                    'comments': 'This is a comment',
                    'first_name': 'firsty',
                    'last_name': 'lasty',
                    'timestamp': '2020-10-18T13:48:08.000000+00:00'
                }]
            },
            ExpressionAttributeNames={'#active': 'active', '#crm': 'crm'},
            ReturnValues='NONE'
        )

    @data(True, False)
    def test_returns_404_when_sender_does_not_exist(self, new_activation):
        # Arrange
        endpoint = 'activate' if new_activation else 'deactivate'
        reference_table_mock.get_item.return_value = {}
        event = build_event(
            'AA',
            'ABCDE123',
            '12345678',
            'session123',
            f'/api/sender/{{cipher}}/{{sender_code}}/{endpoint}',
            body=REQUEST_BODY,
            method='POST',
        )
        # Act
        actual_response = lambda_handler(event, self.context)
        # Assert
        assertApiResponse(self, 404, {'message': 'No sender found'}, actual_response)

    @unpack
    @data(
        (True, True, "2", 'Sender is already in the requested state'),
        (False, False, "2", 'Sender is already in the requested state'),
        (True, False, "1", 'Cannot edit sender with this source type'),
        (True, False, "8", 'Cannot edit sender with this source type'),
        (False, True, "1", 'Cannot edit sender with this source type'),
        (False, True, "8", 'Cannot edit sender with this source type'),
    )
    def test_returns_409_when_sender_has_bad_state(self, new_activation, current_activation, source_type, message):
        # Arrange
        endpoint = 'activate' if new_activation else 'deactivate'
        event = build_event(
            'AA',
            'ABCDE123',
            '12345678',
            'session123',
            f'/api/sender/{{cipher}}/{{sender_code}}/{endpoint}',
            body=REQUEST_BODY,
            method='POST',
        )
        sender = SENDER.copy()
        sender['active'] = current_activation
        sender['reference_subtype'] = source_type
        reference_table_mock.get_item.return_value = {'Item': sender}
        # Act
        actual_response = lambda_handler(event, self.context)
        # Assert
        assertApiResponse(self, 409, {'message': message}, actual_response)
        reference_table_mock.update_item.assert_not_called()
