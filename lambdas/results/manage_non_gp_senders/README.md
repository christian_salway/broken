The entry-point for all api requests for managing or viewing non-gp sender codes.

Currently this will accept a get request for a single cipher/sender code, will return the corresponding record if it exists.