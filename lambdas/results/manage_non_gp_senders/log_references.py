import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    MANAGESENDERS001 = (logging.INFO, 'Received request to manage a non-gp sender')
    MANAGESENDERS200 = (logging.INFO, 'Found sender')
    MANAGESENDERS400 = (logging.ERROR, 'Requested method/resource not supported')
    MANAGESENDERS410 = (logging.WARNING, 'Sender not found for cipher/sender code combination')
    MANAGESENDERS420 = (logging.WARNING, 'Sender already in requested state')
    MANAGESENDERS421 = (logging.WARNING, 'Sender with invalid source type')
    MANAGESENDERS430 = (logging.WARNING, 'Missing arguments')
