import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    IDENDUP0001 = (logging.INFO, 'Received request to identify duplicate results')
    IDENDUP0002 = (logging.INFO, 'Attempting pre match validation')
    IDENDUP0003 = (logging.WARNING, 'Record failed pre-match validation')
    IDENDUP0004 = (logging.INFO,
                   'Duplicate slide number found with differing demographics. Continuing with participant matching')
    IDENDUP0005 = (logging.INFO, 'Queuing result for auto-matching')

    IDENDUPERR0001 = (logging.ERROR, 'Unable to find the result record')
    IDENDUPERR0002 = (logging.INFO, 'Result has already been matched')
