This lambda is triggered by messages placed on the results_to_identify_duplicates SQS queue.
It retrieves the result record and checks that it has not been matched already.

It performs pre-match validation of the result, i.e. checks if the slide number is a duplicate. If it is an exact duplicate (same demographics) it is rejected outright and processing goes no further. If it is not an exact duplicate then the results it duplicates are attached to the result as a list and the result is marked as rejected, for resolution manually.

If the result has not been rejected outright then a message is placed on the results_to_match SQS queue.