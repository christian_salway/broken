import json
import os
from unittest.case import TestCase

from common.test_mocks.mock_events import \
    sqs_mock_event
from identify_duplicate_results.log_references import LogReference
from mock import Mock, call, patch

with patch('boto3.resource') as resource_mock, patch('boto3.client') as client_mock:
    from identify_duplicate_results.identify_duplicate_results import \
        lambda_handler


path = 'identify_duplicate_results.identify_duplicate_results'

mock_sqs_event = sqs_mock_event({
    "internal_id": "QWERTY123",
    "result_id": "540407ef-2578-4cf2-82c8-0ab735330267",
    "received_time": "2020-01030T09:52:12.345"
})

sqs_match_result_queue_mock = Mock()
sqs_match_result_queue_mock.return_value = 'match_queue_url'


@patch(f'{path}.sqs_match_result_queue', sqs_match_result_queue_mock)
@patch(f'{path}.identify_duplicate_slide_numbers')
@patch(f'{path}.log')
@patch(f'{path}.get_result_record')
@patch(f'{path}.send_new_message')
class TestIdentifyDuplicates(TestCase):

    def test_exception_raised_when_unable_to_find_result_record(
            self, sqs_mock, get_result_record_mock, log_mock,
            identify_duplicate_slide_numbers_mock):

        get_result_record_mock.return_value = None

        with self.assertRaises(Exception):
            lambda_handler.__wrapped__(mock_sqs_event, {})

        get_result_record_mock.assert_called()

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.IDENDUP0001}),
            call({'log_reference': LogReference.IDENDUPERR0001,
                  'result_id': '540407ef-2578-4cf2-82c8-0ab735330267',
                  'received_time': '2020-01030T09:52:12.345'}),
        ])

    def test_lambda_exits_when_result_already_matched(
            self, sqs_mock, get_result_record_mock,
            log_mock, identify_duplicate_slide_numbers_mock):
        result_record = {
            'matched_time': 'something'
        }
        get_result_record_mock.return_value = result_record

        response = lambda_handler.__wrapped__(mock_sqs_event, {})

        self.assertEqual(None, response)
        log_mock.assert_has_calls([
            call({'log_reference': LogReference.IDENDUP0001}),
            call({'log_reference': LogReference.IDENDUPERR0002})
        ])

        sqs_mock.assert_not_called()

    def test_identify_duplicate_results_completes_successfully(
            self, sqs_mock, get_result_record_mock,
            log_mock, identify_duplicate_slide_numbers_mock):

        result_record = json.loads(load_result_record('example_record_1.json'))

        identify_duplicate_slide_numbers_mock.return_value = result_record
        get_result_record_mock.return_value = result_record

        lambda_handler.__wrapped__(mock_sqs_event, {})

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.IDENDUP0001}),
            call({'log_reference': LogReference.IDENDUP0002})
        ])

        sqs_mock.assert_called_with(
            'match_queue_url', {
                'internal_id': 'QWERTY123',
                'result_id': '540407ef-2578-4cf2-82c8-0ab735330267',
                'received_time': '2020-01030T09:52:12.345'
            }
        )

    def test_result_processing_continues_when_slide_number_duplicated(
            self, sqs_mock, get_result_record_mock,
            log_mock, identify_duplicate_slide_numbers_mock):
        result_record = json.loads(load_result_record('example_record_1.json'))

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        get_result_record_mock.return_value = result_record
        result_record['duplicates'] = ['duplicate_id']
        identify_duplicate_slide_numbers_mock.return_value = result_record

        lambda_handler.__wrapped__(mock_sqs_event, {})

        get_result_record_mock.assert_called_with(result_id, received_time)

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.IDENDUP0002}),
            call({'log_reference': LogReference.IDENDUP0004, 'duplicates': ['duplicate_id']})
        ])

        sqs_mock.assert_called_with(
            'match_queue_url', {
                'internal_id': 'QWERTY123',
                'result_id': '540407ef-2578-4cf2-82c8-0ab735330267',
                'received_time': '2020-01030T09:52:12.345'
            }
        )

    def test_result_rejected_when_exact_duplicate_of_existing_result(
            self, sqs_mock, get_result_record_mock,
            log_mock, identify_duplicate_slide_numbers_mock):
        result_record = json.loads(load_result_record('example_record_1.json'))

        result_id = "540407ef-2578-4cf2-82c8-0ab735330267"
        received_time = "2020-01030T09:52:12.345"

        get_result_record_mock.return_value = result_record
        result_record['rejected_reason'] = 'a reason'
        identify_duplicate_slide_numbers_mock.return_value = result_record

        lambda_handler.__wrapped__(mock_sqs_event, {})

        get_result_record_mock.assert_called_with(result_id, received_time)

        log_mock.assert_has_calls([
            call({'log_reference': LogReference.IDENDUP0002}),
            call({'log_reference': LogReference.IDENDUP0003, 'reason': 'a reason'})
        ])

        sqs_mock.assert_not_called()


def load_result_record(name):
    current_path = os.path.dirname(os.path.realpath(__file__))
    full_file_path = os.path.join(current_path, 'test_data', name)
    return open(full_file_path).read()


def mock_matched_participant():
    return {
        'match_strategy': 0,
        'participant': {
            'participant_id': '0'
        }
    }
