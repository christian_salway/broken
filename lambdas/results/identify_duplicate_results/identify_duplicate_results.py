import json
import os

from common.log import log
from common.utils.lambda_wrappers import \
    lambda_entry_point
from common.utils.match_result_utils import (
    get_result_record, identify_duplicate_slide_numbers,
    result_matched_already)
from common.utils.sqs_utils import send_new_message
from identify_duplicate_results.identify_duplicate_results_service import \
    result_rejected_outright
from identify_duplicate_results.log_references import LogReference


def sqs_match_result_queue():
    return os.environ.get('SQS_QUEUE_URL')


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.IDENDUP0001})

    message = event['Records'][0]
    message_information = json.loads(message['body'])

    result_id = message_information['result_id']
    received_time = message_information['received_time']
    internal_id = message_information['internal_id']

    result_record = get_result_record(result_id, received_time)

    if not result_record:
        log({'log_reference': LogReference.IDENDUPERR0001,
             'result_id': result_id,
             'received_time': received_time})
        raise Exception('Result not found')

    if result_matched_already(result_record):
        log({'log_reference': LogReference.IDENDUPERR0002})
        return

    log({'log_reference': LogReference.IDENDUP0002})
    validated_record = identify_duplicate_slide_numbers(result_record)

    if result_rejected_outright(validated_record):
        log({'log_reference': LogReference.IDENDUP0003,
             'reason': validated_record['rejected_reason']})
        return

    duplicates = validated_record.get('duplicates')
    if duplicates:
        # Duplicate slide number found but with differing demographics.
        log({'log_reference': LogReference.IDENDUP0004, 'duplicates': duplicates})

    log({'log_reference': LogReference.IDENDUP0005})
    send_new_message(sqs_match_result_queue(), {
        'result_id': result_id,
        'received_time': received_time,
        'internal_id': internal_id
    })
