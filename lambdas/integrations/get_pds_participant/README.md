THIS LAMBDA IS A PROOF OF CONCEPT
It will need to be improved for actual use in the system

This lambda returns a participant from PDS with the nhs number provided.
It makes the request to the PDS FHIR service. If the user has not yet authenticated with the service this session the lambda will fail. If the access token has expired, the lambda will refresh the token before fetching the participant.

Request:
`api/participants/pds-participant/{nhs_number}`

Response:
```
{
    "data": {
        "nhs_number": "9000000009", 
        "first_name": "Jane", 
        "last_name": "Smith", 
        "title": "Mrs", 
        "date_of_birth": "2010-10-22", 
        "address": [
            "1 Trevelyan Square", 
            "Boar Lane", 
            "City Centre", 
            "Leeds", 
            "West Yorkshire"
        ], 
        "postcode": "LS1 6AE"
    }
}
```

