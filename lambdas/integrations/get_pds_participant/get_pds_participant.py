import os
import json
import requests
from time import time
from common.utils.lambda_wrappers import lambda_entry_point, api_lambda
from common.log import log
from common.utils import json_return_message, json_return_data
from common.utils.session_utils import update_session, get_session_from_lambda_event
from common.utils.secrets_manager_utils import get_secret
from .log_references import LogReference
from uuid import uuid4

PDS_FHIR_BASE_URL = os.environ.get('PDS_FHIR_BASE_URL')
PDS_FHIR_AUTH_URL = os.environ.get('PDS_FHIR_AUTH_URL')
PDSFHIR_CLIENT_SECRET_ID = os.environ.get('PDSFHIR_CLIENT_SECRET_ID')
ENVIRONMENT = os.environ.get('ENVIRONMENT_NAME')


@lambda_entry_point
@api_lambda
def lambda_handler(event, context):
    log({'log_reference': LogReference.PDSSEARCH0001})
    session = get_session_from_lambda_event(event)
    session_token = session['session_token']

    path_parameters = event.get('pathParameters')
    nhs_number = path_parameters.get('nhs_number') if path_parameters else None
    if not nhs_number:
        log({'log_reference': LogReference.PDSSEARCH0002})
        return json_return_message(400, 'Required parameter nhs_number not supplied')

    now = time()
    if not session.get('pds_access_token') or session['pds_reauthenticate_timestamp'] < now:
        log({'log_reference': LogReference.PDSSEARCH0003})
        return json_return_message(400, 'User not authenticated with PDS')

    if session['pds_access_token_expiry'] < now:
        log({'log_reference': LogReference.PDSSEARCH0004})
        session['pds_access_token'] = refresh_pds_access_token(session_token, session['pds_refresh_token'])

    log({'log_reference': LogReference.PDSSEARCH0008})
    headers = {
        'Authorization': f'Bearer {session["pds_access_token"]}',
        'NHSD-Session-URID': session['selected_role']['role_id'],
        'X-Request-ID': str(uuid4())
    }
    url = f'{PDS_FHIR_BASE_URL}/Patient/{nhs_number}'
    response = requests.get(url, headers=headers)

    if response.status_code != 200:
        log({'log_reference': LogReference.PDSSEARCH0009, 'response': response.json()})
        return json_return_message(
            500,
            f'Failed to retrieve PDS participant with NHS number {nhs_number}'
        )

    log({'log_reference': LogReference.PDSSEARCH0010})
    pds_participant = format_pds_participant(response.json())
    return json_return_data(200, pds_participant)


def refresh_pds_access_token(session_token, refresh_token):
    log({'log_reference': LogReference.PDSSEARCH0005})
    client_secrets = json.loads(get_secret(PDSFHIR_CLIENT_SECRET_ID, secret_name_is_key=False))

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    parameters = {
        'client_secret': client_secrets['client_secret'],
        'client_id': client_secrets['client_id'],
        'grant_type': 'refresh_token',
        'refresh_token': refresh_token
    }

    log({'log_reference': LogReference.PDSSEARCH0006})
    url = f'{PDS_FHIR_AUTH_URL}/token'
    response = requests.post(url, headers=headers, data=parameters)

    if response.status_code != 200:
        raise Exception('Could not refresh PDS access token')

    log({'log_reference': LogReference.PDSSEARCH0007})
    response_body = response.json()
    now = int(time())
    refreshed_token = response_body['access_token']
    update_session(
        session_token,
        update_attributes={
            'pds_access_token': refreshed_token,
            'pds_access_token_expiry': now + int(response_body['expires_in']),
            'pds_refresh_token': response_body['refresh_token']
        }
    )
    return refreshed_token


def format_pds_participant(response):
    name = response.get('name', [{}])[0]
    address = response.get('address', [{}])[0]
    return {
        'nhs_number': response.get('id'),
        'first_name': name.get('given', [None])[0],
        'last_name': name.get('family'),
        'title': name.get('prefix', [None])[0],
        'date_of_birth': response.get('birthDate'),
        'address': address.get('line'),
        'postcode': address.get('postalCode')
    }
