from time import time
from unittest import TestCase
from unittest.mock import patch, Mock, MagicMock
from uuid import uuid4


@patch('os.environ', {'SESSION_TABLE_NAME': 'sessions-table'})
class TestGetPdsParticipant(TestCase):
    mock_env_vars = {
        'PDS_FHIR_BASE_URL': 'the_pds_base_url',
        'PDS_FHIR_AUTH_URL': 'the_pds_auth_url',
        'PDSFHIR_CLIENT_SECRET_ID': 'the_client_secret_id',
        'ENVIRONMENT': 'the_env'
    }
    EXAMPLE_UUID = '98f1f2c2-fd40-4e75-9d36-d171479db19d'

    time_mock = Mock(wraps=time)
    time_mock.return_value = 1601392160
    uuid_mock = Mock(wraps=uuid4)
    uuid_mock.return_value = EXAMPLE_UUID
    post_response_object = MagicMock()
    post_response_object.status_code = 200
    post_response_json = {
        'access_token': 'the_refreshed_access_token',
        'expires_in': '500', 'refresh_token': 'the_new_refresh_token'}
    post_response_object.json.return_value = post_response_json
    get_response_object = MagicMock()
    get_response_object.status_code = 200
    get_response_json = {
        'id': '9999999999',
        'name': [{'given': ['firsty'], 'family': 'lasty', 'prefix': ['HRH']}],
        'birthDate': '01/04/2020',
        'address': [{'line': '1 Road', 'postalCode': 'AB1  2CD'}]
    }
    get_response_object.json.return_value = get_response_json

    @classmethod
    @patch('boto3.session.Session')
    @patch('boto3.resource')
    @patch('time.time', time_mock)
    @patch('os.environ', mock_env_vars)
    @patch('uuid.uuid4', uuid_mock)
    def setUpClass(cls, boto3_resource, boto3_session, *args):
        cls.log_patcher = patch('get_pds_participant.get_pds_participant.log')

        # Setup database mock
        session_table_mock = Mock()
        session_table_mock.update_item.return_value = {'Attributes': {'a_key': 'a_value'}}
        boto3_table_mock = Mock()
        boto3_table_mock.Table.return_value = session_table_mock
        boto3_resource.return_value = boto3_table_mock
        cls.session_table_mock = session_table_mock

        # Setup secrets manager mock
        session_client_mock = Mock()
        session_client_mock.get_secret_value.return_value = {
            'SecretString': '{"client_secret": "the_secret", "client_id": "the_client"}'
        }
        boto3_session_mock = Mock()
        boto3_session_mock.client.return_value = session_client_mock
        boto3_session.return_value = boto3_session_mock
        cls.boto3_session_mock = boto3_session_mock
        cls.session_client_mock = session_client_mock

        # Setup lambda module
        import get_pds_participant.get_pds_participant as _get_pds_participant
        global get_pds_participant_module
        get_pds_participant_module = _get_pds_participant

    def setUp(self):
        self.session_table_mock.reset_mock()
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('requests.get')
    @patch('requests.post')
    def test_get_pds_participant_including_refreshing_access_token(self, post_request_mock, get_request_mock):
        # Setup requests mocks
        post_request_mock.return_value = self.post_response_object
        get_request_mock.return_value = self.get_response_object

        # Setup the event and the function invocation
        event = _build_event()
        context = Mock()
        context.function_name = ''

        expected_response = {
            'body': '{"data": {"nhs_number": "9999999999", "first_name": "firsty", "last_name": "lasty", '
                    '"title": "HRH", "date_of_birth": "01/04/2020", "address": "1 Road", "postcode": "AB1  2CD"}}',
            'headers': {'Content-Type': 'application/json', 'Strict-Transport-Security': 'max-age=1576800',
                        'X-Content-Type-Options': 'nosniff'},
            'isBase64Encoded': False,
            'statusCode': 200
        }

        actual_response = get_pds_participant_module.lambda_handler(event, context)

        # Assert client secret was correctly looked up from secrets manager
        self.boto3_session_mock.client.assert_called_with(
            service_name='secretsmanager',
            region_name='eu-west-2',
            endpoint_url='https://secretsmanager.eu-west-2.amazonaws.com')
        self.session_client_mock.get_secret_value.assert_called_with(SecretId='the_client_secret_id')

        # Assert new access token requests from PDS token endpoint
        post_request_mock.assert_called_with('the_pds_auth_url/token',
                                             headers={'Content-Type': 'application/x-www-form-urlencoded'},
                                             data={
                                                'client_secret': 'the_secret',
                                                'client_id': 'the_client',
                                                'grant_type': 'refresh_token',
                                                'refresh_token': 'the_refresh_token'
                                             })

        # Assert user session is updated
        self.session_table_mock.update_item.assert_called_with(
            Key={'session_token': 'the_session_token'},
            UpdateExpression='SET #pds_access_token = :pds_access_token, #pds_access_token_expiry = :pds_access_token_expiry, #pds_refresh_token = :pds_refresh_token, #expires = :expires',  
            ExpressionAttributeValues={':pds_access_token': 'the_refreshed_access_token',
                                       ':pds_access_token_expiry': 1601392660,
                                       ':pds_refresh_token': 'the_new_refresh_token', ':expires': 1601392760},
            ExpressionAttributeNames={'#pds_access_token': 'pds_access_token',
                                      '#pds_access_token_expiry': 'pds_access_token_expiry',
                                      '#pds_refresh_token': 'pds_refresh_token', '#expires': 'expires'},
            ReturnValues='ALL_NEW'
        )

        # Assert GET PDS participant request sent with new access token
        get_request_mock.assert_called_with(
            'the_pds_base_url/Patient/9999999999',
            headers={
                'Authorization': 'Bearer the_refreshed_access_token',
                'NHSD-Session-URID': 'test',
                'X-Request-ID': self.EXAMPLE_UUID
            })

        # Assert lambda response
        self.assertDictEqual(expected_response, actual_response)


def _build_event():
    return {
        'pathParameters': {
            'nhs_number': '9999999999'
        },
        'headers': {
            'request_id': 'requestID',
        },
        'requestContext': {
            'authorizer': {
                'principalId': 'blah',
                'session': "{\"user_data\": {\"nhsid_useruid\": \"user_uid\"}, \"session_id\": \"the_session_id\", "
                           "\"session_token\": \"the_session_token\", \"pds_access_token\": \"the_access_token\", "
                           "\"pds_reauthenticate_timestamp\": 1601392160, \"pds_access_token_expiry\": 1601392159, "
                           "\"pds_refresh_token\": \"the_refresh_token\", \"selected_role\": {\"role_id\": \"test\"}}"
            }
        }
    }
