import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    PDSSEARCH0001 = (logging.INFO, 'Received request to get participant from PDS.')
    PDSSEARCH0002 = (logging.ERROR, 'NHS number missing from request.')
    PDSSEARCH0003 = (logging.INFO, 'User not authenticated with PDS.')
    PDSSEARCH0004 = (logging.INFO, 'PDS access token expired. Refreshing access token.')
    PDSSEARCH0005 = (logging.INFO, 'Getting PDS client secret.')
    PDSSEARCH0006 = (logging.INFO, 'Sending POST request to PDS auth token url.')
    PDSSEARCH0007 = (logging.INFO, 'Updating user session with PDS access token.')
    PDSSEARCH0008 = (logging.INFO, 'Sending GET request to PDS patient endpoint.')
    PDSSEARCH0009 = (logging.ERROR, 'Failed to retrieve PDS participant.')
    PDSSEARCH0010 = (logging.INFO, 'PDS participant found.')
