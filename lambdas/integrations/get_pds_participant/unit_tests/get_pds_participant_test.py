import unittest

from mock import patch, Mock, MagicMock

mock_env_vars = {
    'PDS_FHIR_BASE_URL': 'the_pds_base_url',
    'PDS_FHIR_AUTH_URL': 'the_pds_auth_url',
    'PDSFHIR_CLIENT_SECRET_ID': 'the_client_secret_id',
    'ENVIRONMENT': 'the_env'
}

module = 'get_pds_participant.get_pds_participant'


@patch(f'{module}.log', Mock())
class TestGetPdsParticipant(unittest.TestCase):

    @patch('os.environ', mock_env_vars)
    @patch('boto3.resource')
    def setUp(self, *args):
        # Setup lambda module
        from get_pds_participant.get_pds_participant import lambda_handler, format_pds_participant, \
            refresh_pds_access_token
        self.unwrapped_handler = lambda_handler.__wrapped__.__wrapped__
        self.format_pds_participant = format_pds_participant
        self.refresh_pds_access_token = refresh_pds_access_token

    @patch(f'{module}.get_session_from_lambda_event')
    def test_get_pds_participant_returns_bad_request_if_nhs_number_missing(self, get_session_mock):
        get_session_mock.return_value = {'session_token': 'the_token'}
        event = {'pathParameters': None}
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Required parameter nhs_number not supplied"}',
            'isBase64Encoded': False
        }
        actual_response = self.unwrapped_handler(event, {})
        self.assertDictEqual(expected_response, actual_response)

    @patch(f'{module}.time')
    @patch(f'{module}.get_session_from_lambda_event')
    def test_get_pds_participant_returns_bad_request_when_pds_access_token_missing(self, get_session_mock, time_mock):
        get_session_mock.return_value = {'session_token': 'the_token'}
        time_mock.return_value = 1601392160
        event = {'pathParameters': {'nhs_number': '9999999999'}}
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "User not authenticated with PDS"}',
            'isBase64Encoded': False
        }
        actual_response = self.unwrapped_handler(event, {})
        self.assertDictEqual(expected_response, actual_response)

    @patch(f'{module}.time')
    @patch(f'{module}.get_session_from_lambda_event')
    def test_get_pds_participant_returns_bad_request_when_pds_reauthentication_expiry_in_the_past(
            self, get_session_mock, time_mock):
        get_session_mock.return_value = {'session_token': 'the_token', 'pds_access_token': 'pds_token',
                                         'pds_reauthenticate_timestamp': 1601392159}
        time_mock.return_value = 1601392160
        event = {'pathParameters': {'nhs_number': '9999999999'}}
        expected_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "User not authenticated with PDS"}',
            'isBase64Encoded': False
        }
        actual_response = self.unwrapped_handler(event, {})
        self.assertDictEqual(expected_response, actual_response)

    @patch(f'{module}.uuid4')
    @patch(f'{module}.requests.get')
    @patch(f'{module}.refresh_pds_access_token')
    @patch(f'{module}.time')
    @patch(f'{module}.get_session_from_lambda_event')
    def test_get_pds_participant_refreshes_access_token_if_it_has_expired_and_uses_this_in_get_request(
            self, get_session_mock, time_mock, refresh_token_mock, get_request_mock, uuid_mock):
        get_session_mock.return_value = {
            'session_token': 'the_token',
            'pds_access_token': 'pds_token',
            'pds_refresh_token': 'refresh_token',
            'pds_reauthenticate_timestamp': 1601392160,
            'pds_access_token_expiry': 1601392159,
            'selected_role': {'role_id': 'test'},
        }
        time_mock.return_value = 1601392160
        uuid_mock.return_value = "test_uuid"
        refresh_token_mock.return_value = 'the_refreshed_access_token'
        event = {'pathParameters': {'nhs_number': '9999999999'}}
        self.unwrapped_handler(event, {})
        refresh_token_mock.assert_called_with('the_token', 'refresh_token')
        get_request_mock.assert_called_with(
            'the_pds_base_url/Patient/9999999999',
            headers={
                'Authorization': 'Bearer the_refreshed_access_token',
                'NHSD-Session-URID': 'test',
                'X-Request-ID': 'test_uuid'
            })

    @patch(f'{module}.uuid4')
    @patch(f'{module}.requests.get')
    @patch(f'{module}.refresh_pds_access_token')
    @patch(f'{module}.time')
    @patch(f'{module}.get_session_from_lambda_event')
    def test_get_pds_participant_does_not_refresh_access_token_if_it_has_not_expired(
            self, get_session_mock, time_mock, refresh_token_mock, get_request_mock, uuid_mock):
        get_session_mock.return_value = {
            'session_token': 'the_token',
            'pds_access_token': 'old_access_token',
            'pds_refresh_token': 'refresh_token',
            'pds_reauthenticate_timestamp': 1601392160,
            'pds_access_token_expiry': 1601392160,
            'selected_role': {'role_id': 'test'},
        }
        time_mock.return_value = 1601392160
        uuid_mock.return_value = 'test_uuid'
        event = {'pathParameters': {'nhs_number': '9999999999'}}
        self.unwrapped_handler(event, {})
        refresh_token_mock.assert_not_called()
        get_request_mock.assert_called_with(
            'the_pds_base_url/Patient/9999999999',
            headers={
                'Authorization': 'Bearer old_access_token',
                'NHSD-Session-URID': 'test',
                'X-Request-ID': 'test_uuid'
            }
            )

    @patch(f'{module}.uuid4')
    @patch(f'{module}.requests.get')
    @patch(f'{module}.time')
    @patch(f'{module}.get_session_from_lambda_event')
    def test_get_pds_participant_returns_500_if_error_getting_participant_from_pds(
            self, get_session_mock, time_mock, get_request_mock, uuid_mock):
        get_session_mock.return_value = {
            'session_token': 'the_token',
            'pds_access_token': 'pds_token',
            'selected_role': {'role_id': 'test'},
            'pds_refresh_token': 'refresh_token',
            'pds_reauthenticate_timestamp': 1601392160,
            'pds_access_token_expiry': 1601392160
        }
        time_mock.return_value = 1601392160
        uuid_mock.return_value = "test_uuid"
        event = {'pathParameters': {'nhs_number': '9999999999'}}
        response_object = MagicMock()
        response_object.status_code = 404
        get_request_mock.return_value = response_object
        expected_response = {
            'statusCode': 500,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Failed to retrieve PDS participant with NHS number 9999999999"}',
            'isBase64Encoded': False
        }
        actual_response = self.unwrapped_handler(event, {})
        self.assertDictEqual(expected_response, actual_response)
        get_request_mock.assert_called_with(
            'the_pds_base_url/Patient/9999999999',
            headers={
                'Authorization': 'Bearer pds_token',
                'NHSD-Session-URID': 'test',
                'X-Request-ID': 'test_uuid'
            })

    @patch(f'{module}.uuid4')
    @patch(f'{module}.format_pds_participant')
    @patch(f'{module}.requests.get')
    @patch(f'{module}.time')
    @patch(f'{module}.get_session_from_lambda_event')
    def test_get_pds_participant_returns_200_if_successfully_get_participant_from_pds(
            self, get_session_mock, time_mock, get_request_mock, format_mock, uuid_mock):
        get_session_mock.return_value = {
            'session_token': 'the_token',
            'selected_role': {'role_id': 'test'},
            'pds_access_token': 'pds_token',
            'pds_refresh_token': 'refresh_token',
            'pds_reauthenticate_timestamp': 1601392160,
            'pds_access_token_expiry': 1601392160
        }
        time_mock.return_value = 1601392160
        uuid_mock.return_value = 'test_uuid'
        event = {'pathParameters': {'nhs_number': '9999999999'}}
        response_object = MagicMock()
        response_object.status_code = 200
        response_json = {'a_key': 'a_value'}
        response_object.json.return_value = response_json
        get_request_mock.return_value = response_object
        format_mock.return_value = {'formatted_key': 'formatted_value'}
        expected_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"data": {"formatted_key": "formatted_value"}}',
            'isBase64Encoded': False
        }
        actual_response = self.unwrapped_handler(event, {})
        self.assertDictEqual(expected_response, actual_response)
        get_request_mock.assert_called_with(
            'the_pds_base_url/Patient/9999999999',
            headers={
                'Authorization': 'Bearer pds_token',
                'NHSD-Session-URID': 'test',
                'X-Request-ID': 'test_uuid'
            })

    @patch(f'{module}.requests.post')
    @patch(f'{module}.time')
    @patch(f'{module}.update_session')
    @patch(f'{module}.get_secret')
    def test_refresh_pds_access_token_raises_exception_if_request_to_authorize_with_pds_fails(
            self, get_secret_mock, update_session_mock, time_mock, post_request_mock):
        time_mock.return_value = 1601392160
        get_secret_mock.return_value = '{"client_secret": "the_secret", "client_id": "the_client"}'
        response_object = MagicMock()
        response_object.status_code = 400
        post_request_mock.return_value = response_object

        with self.assertRaises(Exception) as context:
            self.refresh_pds_access_token('the_session_token', 'the_refresh_token')

        self.assertEqual('Could not refresh PDS access token', context.exception.args[0])
        post_request_mock.assert_called_with('the_pds_auth_url/token',
                                             headers={'Content-Type': 'application/x-www-form-urlencoded'},
                                             data={
                                                'client_secret': 'the_secret',
                                                'client_id': 'the_client',
                                                'grant_type': 'refresh_token',
                                                'refresh_token': 'the_refresh_token'
                                             })
        update_session_mock.assert_not_called()

    @patch(f'{module}.requests.post')
    @patch(f'{module}.time')
    @patch(f'{module}.update_session')
    @patch(f'{module}.get_secret')
    def test_refresh_pds_access_token_updates_session_when_request_to_refresh_token_succeeds(
            self, get_secret_mock, update_session_mock, time_mock, post_request_mock):
        time_mock.return_value = 1601392160
        get_secret_mock.return_value = '{"client_secret": "the_secret", "client_id": "the_client"}'
        response_object = MagicMock()
        response_object.status_code = 200
        response_json = {
            'access_token': 'the_access_token', 'expires_in': '500',
            'refresh_token': 'the_new_refresh_token'}
        response_object.json.return_value = response_json
        post_request_mock.return_value = response_object

        refreshed_token = self.refresh_pds_access_token('the_session_token', 'the_refresh_token')
        self.assertEqual('the_access_token', refreshed_token)

        post_request_mock.assert_called_with('the_pds_auth_url/token',
                                             headers={'Content-Type': 'application/x-www-form-urlencoded'},
                                             data={
                                                'client_secret': 'the_secret',
                                                'client_id': 'the_client',
                                                'grant_type': 'refresh_token',
                                                'refresh_token': 'the_refresh_token'
                                             })
        update_session_mock.assert_called_with('the_session_token',
                                               update_attributes={'pds_access_token': 'the_access_token',
                                                                  'pds_access_token_expiry': 1601392660,
                                                                  'pds_refresh_token': 'the_new_refresh_token'})

    def test_format_pds_participant(self):
        pds_response_json = {
            'id': '9999999999',
            'name': [{'given': ['firsty'], 'family': 'lasty', 'prefix': ['HRH']}],
            'birthDate': '01/04/2020',
            'address': [{'line': '1 Road', 'postalCode': 'AB1  2CD'}]
        }
        expected_converted_json = {
            'nhs_number': '9999999999',
            'first_name': 'firsty',
            'last_name': 'lasty',
            'title': 'HRH',
            'date_of_birth': '01/04/2020',
            'address': '1 Road',
            'postcode': 'AB1  2CD'
        }
        actual_converted_json = self.format_pds_participant(pds_response_json)
        self.assertDictEqual(expected_converted_json, actual_converted_json)

    def test_format_pds_participant_if_response_is_empty(self):
        expected_converted_json = {
            'nhs_number': None,
            'first_name': None,
            'last_name': None,
            'title': None,
            'date_of_birth': None,
            'address': None,
            'postcode': None
        }
        actual_converted_json = self.format_pds_participant({})
        self.assertDictEqual(expected_converted_json, actual_converted_json)
