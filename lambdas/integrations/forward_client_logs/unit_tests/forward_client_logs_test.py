from mock import patch
from unittest.case import TestCase

from forward_client_logs.log_references import LogReference
with patch('common.log.log') as mock_log:
    from forward_client_logs.forward_client_logs import lambda_handler


@patch('forward_client_logs.forward_client_logs.log')
class TestForwardClientLogs(TestCase):

    def setUp(self):
        super(TestForwardClientLogs, self).setUp()
        self.unwrapped_handler = lambda_handler.__wrapped__
        self.expected_happy_response = {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{}',
            'isBase64Encoded': False
        }
        self.expected_error_response = {
            'statusCode': 400,
            'headers': {'Content-Type': 'application/json',
                        'X-Content-Type-Options': 'nosniff',
                        'Strict-Transport-Security': 'max-age=1576800'},
            'body': '{"message": "Missing request body."}',
            'isBase64Encoded': False
        }
        self.missing_ip = 'IP address not found'

    def test_returns_200_and_logs_expected_data(self, mock_logger):

        lambda_proxy_payload = {
            'body': '{"a": "b", "c": "d"}',
            'headers': {'X-Forwarded-For': '12345', 'X-Su-Waa': '236'},
            'info': 'seven'
        }

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        self.assertDictEqual(self.expected_happy_response, actual_response)
        mock_logger.assert_called_with({'log_reference': LogReference.FRWD0001,
                                        'event': {"a": "b", "c": "d"},
                                        'ip_address': '12345'})

    def test_payload_without_headers(self, mock_logger):
        lambda_proxy_payload = {
            'body': '{"a": "b", "c": "d"}',
            'info': 'seven'
        }

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        self.assertDictEqual(self.expected_happy_response, actual_response)
        mock_logger.assert_called_with({'log_reference': LogReference.FRWD0001,
                                        'event': {"a": "b", "c": "d"},
                                        'ip_address': self.missing_ip})

    def test_payload_without_IP(self, mock_logger):
        lambda_proxy_payload = {
            'body': '{"a": "b", "c": "d"}',
            'headers': {'X-Su-Waa': '236'},
            'info': 'seven'
        }

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        self.assertDictEqual(self.expected_happy_response, actual_response)
        mock_logger.assert_called_with({'log_reference': LogReference.FRWD0001,
                                        'event': {"a": "b", "c": "d"},
                                        'ip_address': self.missing_ip})

    def test_payload_without_body_throws_error(self, mock_logger):
        lambda_proxy_payload = {
            'headers': {'X-Forwarded-For': '321', 'X-Su-Waa': '236'},
            'info': 'seven'
        }

        actual_response = self.unwrapped_handler(lambda_proxy_payload, {})

        self.assertDictEqual(self.expected_error_response, actual_response)
        mock_logger.assert_called_with({'log_reference': LogReference.FRWD0002,
                                        'Error': 'Missing request body.',
                                        'ip_address': '321'})
