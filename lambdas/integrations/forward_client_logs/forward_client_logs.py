import json
from common.log import log
from common.utils import json_return_message, json_return_object
from common.utils.lambda_wrappers import lambda_entry_point
from forward_client_logs.log_references import LogReference


@lambda_entry_point
def lambda_handler(event, context):

    ip_address = str(event.get('headers', {}).get('X-Forwarded-For', 'IP address not found'))
    try:
        log_message = json.loads(event['body'])
    except Exception:
        error_message = 'Missing request body.'
        log({'log_reference': LogReference.FRWD0002, 'Error': error_message, 'ip_address': ip_address})
        return json_return_message(400, error_message)

    log({'log_reference': LogReference.FRWD0001, 'event': log_message, 'ip_address': ip_address})
    return json_return_object(200)
