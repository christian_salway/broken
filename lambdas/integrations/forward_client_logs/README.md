This lambda receives logs from the web app's logger and logs them out to CloudWatch.
In the future, we will likely skip this and instead post the logs to Splunk.