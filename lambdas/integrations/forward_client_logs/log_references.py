import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    FRWD0001 = (logging.INFO, 'Received request')
    FRWD0002 = (logging.ERROR, 'Failed to forward')
