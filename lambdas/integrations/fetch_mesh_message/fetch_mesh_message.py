import json
import os
import tempfile

from common.utils.mesh.mesh_client_utils import get_mesh_client, get_message_contents_by_message_id
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.secrets_manager_utils import get_secret_by_key
from common.log import log, get_internal_id
from fetch_mesh_message.log_references import LogReference
from common.utils.s3_utils import put_object
from common.exceptions import InvalidMailboxIdException


RESULTS_FILE_BUCKET = os.environ.get('RESULTS_FILE_BUCKET')
DEMOGRAPHICS_FILE_BUCKET = os.environ.get('DEMOGRAPHICS_FILE_BUCKET')
RESULT_FILE_MAILBOX_IDS = os.environ.get('RESULT_FILE_MAILBOX_IDS')
COHORT_FILE_MAILBOX_IDS = os.environ.get('COHORT_FILE_MAILBOX_IDS')
ENVIRONMENT_NAME = os.environ.get('ENVIRONMENT_NAME')
MESH_HOST = os.environ.get('MESH_HOST')
MESH_ENVIRONMENT = os.environ.get('MESH_ENVIRONMENT')
AWS_ACCOUNT_ID = os.environ.get('AWS_ACCOUNT_ID')


@lambda_entry_point
def lambda_handler(event, context):
    sqs_message = json.loads(event['Records'][0]['body'])
    log({'log_reference': LogReference.FETCHMESH0001})

    cloudwatch_event_rule = sqs_message['cloudwatch_event_rule']
    message_id = sqs_message['message_id']

    log({'log_reference': LogReference.FETCHMESH0002, 'cloudwatch_event_rule': cloudwatch_event_rule})
    mesh_mailbox_creds = json.loads(get_secret_by_key('mesh_mailbox_credentials',
                                    extract_mailbox_secret_key(cloudwatch_event_rule)))

    mailbox_id = mesh_mailbox_creds['mailbox_id']
    if mailbox_id == "[ENV]":
        mailbox_id = f'{AWS_ACCOUNT_ID}-{ENVIRONMENT_NAME}'

    with tempfile.TemporaryDirectory() as temp_dir_name:
        with get_mesh_client(mailbox_id, mesh_mailbox_creds['password'], temp_dir_name) as mesh_client:
            log({'log_reference': LogReference.FETCHMESH0003, 'mailbox_id': mailbox_id, 'message_id': message_id})
            file_name, from_mailbox_id,  file_contents = get_message_contents_by_message_id(message_id, mesh_client)
            log({'log_reference': LogReference.FETCHMESH0004, 'mailbox_id': mailbox_id,
                 'from_mailbox_id': from_mailbox_id, 'message_id': message_id, 'message_length': len(file_contents)})

            if not is_valid_from_mailbox(cloudwatch_event_rule, from_mailbox_id):
                log({'log_reference': LogReference.FETCHMESH0009})
                mesh_client.close()
                raise InvalidMailboxIdException(from_mailbox_id)

            target_bucket = get_s3_bucket_name_from_cloudwatch_event_rule(cloudwatch_event_rule)
            upload_file_to_s3(target_bucket, file_contents, file_name)

            log({'log_reference': LogReference.FETCHMESH0007})
            mesh_client.acknowledge_message(message_id)
            log({'log_reference': LogReference.FETCHMESH0008})
            mesh_client.close()


def get_s3_bucket_name_from_cloudwatch_event_rule(cloudwatch_event_rule):
    if 'check-results' in cloudwatch_event_rule:
        return RESULTS_FILE_BUCKET
    elif 'check-demographics' in cloudwatch_event_rule:
        return DEMOGRAPHICS_FILE_BUCKET
    else:
        raise Exception(f'Unknown cloudwatch event rule {cloudwatch_event_rule}. '
                        f'Cannot determine S3 upload destination.')


def upload_file_to_s3(target_bucket, file_content, file_name):
    log({'log_reference': LogReference.FETCHMESH0005, 'target_bucket': target_bucket, 'file_name': file_name})
    meta_data = {
        'internal_id': get_internal_id()
    }
    put_object(target_bucket, file_content, file_name, meta_data=meta_data)
    log({'log_reference': LogReference.FETCHMESH0006})


def is_valid_from_mailbox(cloudwatch_event_rule, from_mailbox_id):
    valid_mesh_mailbox_ids = None
    if 'results' in cloudwatch_event_rule:
        valid_mesh_mailbox_ids = RESULT_FILE_MAILBOX_IDS.split(',')
    elif 'demographics' in cloudwatch_event_rule:
        valid_mesh_mailbox_ids = COHORT_FILE_MAILBOX_IDS.split(',')

    if valid_mesh_mailbox_ids and from_mailbox_id not in valid_mesh_mailbox_ids:
        log({'log_reference': LogReference.FETCHMESHERR0001, 'mailbox_id': from_mailbox_id,
             'valid_ids': str(valid_mesh_mailbox_ids)})
        return False

    return True


def extract_mailbox_secret_key(cloudwatch_event_rule):
    return cloudwatch_event_rule.replace(ENVIRONMENT_NAME, MESH_ENVIRONMENT)
