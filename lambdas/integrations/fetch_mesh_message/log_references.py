import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    FETCHMESH0001 = (logging.INFO, 'SQS message deserialised successfully')
    FETCHMESH0002 = (logging.INFO, 'Retrieving mailbox credentials')
    FETCHMESH0003 = (logging.INFO, 'Retrieving message from mailbox')
    FETCHMESH0004 = (logging.INFO, 'Message retrieved from MESH')
    FETCHMESH0005 = (logging.INFO, 'Uploading message content to S3 bucket')
    FETCHMESH0006 = (logging.INFO, 'Message content uploaded to S3 successfully')
    FETCHMESH0007 = (logging.INFO, 'Sending MESH message acknowledgement to MESH server')
    FETCHMESH0008 = (logging.INFO, 'Successfully sent MESH message acknowledgement. Closing MESH client.')
    FETCHMESH0009 = (logging.INFO, 'Closing mesh client before raising invalid mailbox exception')
    FETCHMESHERR0001 = (logging.ERROR, "Sender's Mailbox Id is invalid, ignoring message")
