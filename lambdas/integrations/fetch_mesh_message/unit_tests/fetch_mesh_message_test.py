from unittest import TestCase
from mock import patch, call, Mock
from ddt import ddt, data, unpack

from fetch_mesh_message.log_references import LogReference
from common.exceptions import InvalidMailboxIdException

with patch('boto3.resource') as resource_mock:
    import fetch_mesh_message.fetch_mesh_message as environment
    from fetch_mesh_message.fetch_mesh_message import (
        lambda_handler,
        get_s3_bucket_name_from_cloudwatch_event_rule,
        upload_file_to_s3)

environment.MESH_HOST = 'the_mesh_host'
environment.RESULTS_FILE_BUCKET = 'the_results_file_bucket'
environment.DEMOGRAPHICS_FILE_BUCKET = 'the_demographics_file_bucket'
environment.RESULT_FILE_MAILBOX_IDS = 'the_mailbox_id'
environment.COHORT_FILE_MAILBOX_IDS = 'the_mailbox_id'
environment.ENVIRONMENT_NAME = 'test'
environment.MESH_ENVIRONMENT = 'int_test'
environment.AWS_ACCOUNT_ID = '123'
module = 'fetch_mesh_message.fetch_mesh_message'


@ddt
class TestFetchMeshMessageLambdaHandler(TestCase):

    def setUp(self):
        self.unwrapped_handler = lambda_handler.__wrapped__

    @unpack
    @data(('the_mailbox_id', 'the_mailbox_id'),
          ('[ENV]', f'{environment.AWS_ACCOUNT_ID}-{environment.ENVIRONMENT_NAME}'))
    @patch(f'{module}.upload_file_to_s3')
    @patch(f'{module}.get_s3_bucket_name_from_cloudwatch_event_rule')
    @patch(f'{module}.get_message_contents_by_message_id')
    @patch(f'{module}.get_mesh_client')
    @patch(f'{module}.tempfile')
    @patch(f'{module}.get_secret_by_key')
    @patch(f'{module}.log')
    def test_lambda_handler_successfully_retrieves_mesh_message_and_uploads_contents_to_s3(
            self, secret_mailbox_id, expected_mailbox_id,
            log_mock, get_secret_by_key_mock, tempfile_mock, get_client_mock,
            get_message_mock, get_bucket_mock, upload_mock):
        sqs_event = {'Records': [{'body': '{"cloudwatch_event_rule": "the_rule", "message_id": "the_message_id"}'}]}
        get_secret_by_key_mock.return_value = f'{{"mailbox_id": "{secret_mailbox_id}", "password": "the_password"}}'
        tempfile_mock.TemporaryDirectory.return_value.__enter__.return_value = 'the_temp_dir_name'
        mesh_client_mock = Mock()
        get_client_mock.return_value.__enter__.return_value = mesh_client_mock
        get_message_mock.return_value = 'the_file_name', 'the_sender', 'the_file_contents'
        get_bucket_mock.return_value = 'the_s3_bucket'
        self.unwrapped_handler(sqs_event, {})

        get_secret_by_key_mock.assert_called_once_with('mesh_mailbox_credentials', 'the_rule')
        get_client_mock.assert_called_once_with(expected_mailbox_id, 'the_password', 'the_temp_dir_name')
        get_message_mock.assert_called_once_with('the_message_id', mesh_client_mock)
        get_bucket_mock.assert_called_once_with('the_rule')
        upload_mock.assert_called_once_with('the_s3_bucket', 'the_file_contents', 'the_file_name')
        mesh_client_mock.acknowledge_message.assert_called_once_with('the_message_id')
        mesh_client_mock.close.assert_called_once()
        tempfile_mock.TemporaryDirectory.return_value.__exit__.assert_called_once()

        expected_log_calls = [
            call({'log_reference': LogReference.FETCHMESH0001}),
            call({'log_reference': LogReference.FETCHMESH0002, 'cloudwatch_event_rule': 'the_rule'}),
            call({'log_reference': LogReference.FETCHMESH0003,
                  'mailbox_id': expected_mailbox_id, 'message_id': 'the_message_id'}),
            call({'log_reference': LogReference.FETCHMESH0004, 'mailbox_id': expected_mailbox_id,
                  'from_mailbox_id': 'the_sender', 'message_id': 'the_message_id', 'message_length': 17}),
            call({'log_reference': LogReference.FETCHMESH0007}),
            call({'log_reference': LogReference.FETCHMESH0008})
        ]
        log_mock.assert_has_calls(expected_log_calls)

    @unpack
    @data(('the_mailbox_id', 'the_mailbox_id', "['the_mailbox_id']"),
          ('[ENV]', f'{environment.AWS_ACCOUNT_ID}-{environment.ENVIRONMENT_NAME}', "['the_mailbox_id']"))
    @patch(f'{module}.upload_file_to_s3')
    @patch(f'{module}.get_s3_bucket_name_from_cloudwatch_event_rule')
    @patch(f'{module}.get_message_contents_by_message_id')
    @patch(f'{module}.get_mesh_client')
    @patch(f'{module}.tempfile')
    @patch(f'{module}.get_secret_by_key')
    @patch(f'{module}.log')
    def test_lambda_handler_retrieves_message_and_throws_exception_if_sender_mailbox_is_invalid(
            self, secret_mailbox_id, expected_mailbox_id, valid_mailbox_ids,
            log_mock, get_secret_by_key_mock, tempfile_mock, get_client_mock,
            get_message_mock, get_bucket_mock, upload_mock):
        sqs_event = {'Records': [{'body': '{"cloudwatch_event_rule": "results_rule", "message_id": "the_message_id"}'}]}
        get_secret_by_key_mock.return_value = f'{{"mailbox_id": "{secret_mailbox_id}", "password": "the_password"}}'
        tempfile_mock.TemporaryDirectory.return_value.__enter__.return_value = 'the_temp_dir_name'
        mesh_client_mock = Mock()
        get_client_mock.return_value.__enter__.return_value = mesh_client_mock
        get_message_mock.return_value = 'the_file_name', 'the_sender_mailbox_id', 'the_file_contents'
        get_bucket_mock.return_value = 'the_s3_bucket'

        with self.assertRaises(InvalidMailboxIdException) as context:
            self.unwrapped_handler(sqs_event, {})

        self.assertEqual(
            "Sender's mailbox id [the_sender_mailbox_id] is not in the "
            "list of the expected mailboxes to receive results from", context.exception.args[0])

        get_secret_by_key_mock.assert_called_once_with('mesh_mailbox_credentials', 'results_rule')
        get_client_mock.assert_called_once_with(expected_mailbox_id, 'the_password', 'the_temp_dir_name')
        get_message_mock.assert_called_once_with('the_message_id', mesh_client_mock)
        mesh_client_mock.close.assert_called_once()
        get_bucket_mock.assert_not_called()
        upload_mock.assert_not_called()
        mesh_client_mock.acknowledge_message.assert_not_called()
        tempfile_mock.TemporaryDirectory.return_value.__exit__.assert_called_once()

        expected_log_calls = [
            call({'log_reference': LogReference.FETCHMESH0001}),
            call({'log_reference': LogReference.FETCHMESH0002, 'cloudwatch_event_rule': 'results_rule'}),
            call({'log_reference': LogReference.FETCHMESH0003, 'mailbox_id': expected_mailbox_id,
                  'message_id': 'the_message_id'}),
            call({'log_reference': LogReference.FETCHMESH0004, 'mailbox_id': expected_mailbox_id,
                  'from_mailbox_id': 'the_sender_mailbox_id', 'message_id': 'the_message_id', 'message_length': 17}),
            call({'log_reference': LogReference.FETCHMESHERR0001, 'mailbox_id': 'the_sender_mailbox_id',
                  'valid_ids': valid_mailbox_ids}),
            call({'log_reference': LogReference.FETCHMESH0009})
        ]
        log_mock.assert_has_calls(expected_log_calls)


class TestFetchMeshMessageHelperFunctions(TestCase):

    def test_get_s3_bucket_name_from_cloudwatch_event_rule_returns_results_bucket_for_check_results_trigger(self):
        cloudwatch_event_rule = 'my-environment-check-results-trigger'
        expected_s3_bucket = 'the_results_file_bucket'
        actual_s3_bucket = get_s3_bucket_name_from_cloudwatch_event_rule(cloudwatch_event_rule)
        self.assertEqual(expected_s3_bucket, actual_s3_bucket)

    def test_get_s3_bucket_name_from_cloudwatch_event_rule_returns_demographics_bucket_for_demographics_trigger(self):
        cloudwatch_event_rule = 'my-environment-check-demographics-trigger'
        expected_s3_bucket = 'the_demographics_file_bucket'
        actual_s3_bucket = get_s3_bucket_name_from_cloudwatch_event_rule(cloudwatch_event_rule)
        self.assertEqual(expected_s3_bucket, actual_s3_bucket)

    def test_get_s3_bucket_name_from_cloudwatch_event_rule_raises_exception_for_unknown_trigger(self):
        cloudwatch_event_rule = 'my-environment-made-up-trigger'
        expected_exception_message = 'Unknown cloudwatch event rule my-environment-made-up-trigger. ' \
                                     'Cannot determine S3 upload destination.'

        with self.assertRaises(Exception) as context:
            get_s3_bucket_name_from_cloudwatch_event_rule(cloudwatch_event_rule)

        actual_exception_message = context.exception.args[0]
        self.assertEqual(expected_exception_message, actual_exception_message)

    @patch(f'{module}.put_object')
    @patch(f'{module}.log')
    @patch(f'{module}.get_internal_id')
    def test_upload_file_to_s3_calls_put_object_and_logs_the_request_information_correctly(
            self, internal_id_mock, log_mock, put_object_mock):
        target_bucket = 'the_bucket'
        file_name = 'the_file_name'
        file_contents = 'the_file_contents'
        internal_id = 'internal_id'
        internal_id_mock.return_value = internal_id
        upload_file_to_s3(target_bucket, file_contents, file_name)

        put_object_mock.assert_called_with(
            target_bucket,
            file_contents, file_name,
            meta_data={'internal_id': internal_id}
        )

        expected_log_calls = [
            call({'log_reference': LogReference.FETCHMESH0005, 'target_bucket': target_bucket, 'file_name': file_name}),
            call({'log_reference': LogReference.FETCHMESH0006})
        ]

        log_mock.assert_has_calls(expected_log_calls)
