This lambda is triggered when there are messages on the `pending_mesh_messages` SQS Queue and will be triggered for each message on the queue. 

A message consists of a message id and a rule name. 

The message id is used to download a MESH message from the a MESH mailbox. 

The mailbox id and password to download the message are retrieved from AWS Secrets Manager using the rule name as the key. 

Once downloaded, we store the message in an S3 bucket and then we acknowledge the message on MESH. 

The rule name is used to determine which S3 bucket to store retrieved messages in:
* check-results-trigger = results-data bucket
* check-demographics-trigger (not yet implemented) = demographics-data bucket
* Any other rule name throws an exception