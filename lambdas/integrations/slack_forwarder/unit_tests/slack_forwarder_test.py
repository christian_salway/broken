from unittest.case import TestCase
from requests.exceptions import HTTPError
import requests
from mock import patch, Mock
with patch('boto3.resource') as resource_mock, patch('os.environ', {'FAILURE_QUEUE': 'FAILURE_QUEUE_URL'}):
    import slack_forwarder.slack_forwarder as slack_forwarder


@patch('slack_forwarder.slack_forwarder.get_approximate_number_of_messages')
class TestSlackForwarder(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.log_patcher = patch('common.log.log')

    def setUp(self):
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('slack_forwarder.slack_forwarder.post_message_to_slack')
    def test_slack_forwarder_sends_message_when_messages_exist(self, slack_message_post_mock, message_approximator_mock): 
        # Setup the function invocation
        context = Mock()
        context.function_name = ''

        message_approximator_mock.return_value = 1
        slack_forwarder.lambda_handler({}, context)

        expected_call = [
            {'type': 'section', 'text': {'type': 'mrkdwn', 'text': '*DLQ URL*: FAILURE_QUEUE_URL'}},
            {'type': 'section', 'text': {'type': 'mrkdwn', 'text': '*Number of messages*: 1'}}
        ]
        slack_message_post_mock.assert_called_with(expected_call)

    @patch('slack_forwarder.slack_forwarder.post_message_to_slack')
    def test_slack_forwarder_does_not_send_message_when_no_messages_exist(self, slack_message_post_mock, message_approximator_mock): 
        # Setup the function invocation
        context = Mock()
        context.function_name = ''
        message_approximator_mock.return_value = 0

        slack_forwarder.lambda_handler({}, context)

        slack_message_post_mock.assert_not_called

    @patch('requests.post')
    def test_slack_forwarder_errors_when_httperror_is_thrown(self, mock_request, message_approximator_mock):
        # Setup the function invocation
        context = Mock()
        context.function_name = ''
        message_approximator_mock.return_value = 1

        mock_resp = requests.models.Response()
        mock_resp.status_code = 500
        mock_resp.reason = 'Something has gone wrong'
        mock_request.return_value = mock_resp

        with self.assertRaises(HTTPError) as error:
            slack_forwarder.lambda_handler({}, context)

        self.assertEqual('500 Server Error: Something has gone wrong for url: None', error.exception.args[0])
