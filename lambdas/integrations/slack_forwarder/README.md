This lambda function is controlled via the CloudWatch rule defined as `check_failed_lambda_invocations_dlq`.
It is configured to run every 5 minutes. If the queue contains any errors, it posts a notification to the `automated_notifications` channel
where it can be actioned.