import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    LOGSLA0001 = (logging.INFO, 'Attempting to send slack notification')
    LOGSLA0002 = (logging.INFO, 'Slack notification sent successfully')
    LOGSLAERR0001 = (logging.ERROR, 'Non-200 status code recieved from slack')
