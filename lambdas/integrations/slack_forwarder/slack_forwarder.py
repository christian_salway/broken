import os
import json
import requests
import boto3
from requests.exceptions import HTTPError
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from slack_forwarder.log_references import LogReference

SLACK_WEBHOOK_URL = 'https://hooks.slack.com/services/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'
FAILURE_QUEUE = os.environ['FAILURE_QUEUE']


@lambda_entry_point
def lambda_handler(event, context):
    messages = get_approximate_number_of_messages()
    if messages > 0:
        slack_message = format_slack_message(messages)
        log({'log_reference': LogReference.LOGSLA0001, 'number_of_messages': messages})
        try:
            post_message_to_slack(slack_message)
        except HTTPError as err:
            log({'log_reference': LogReference.LOGSLAERR0001, 'error': str(err)})
            raise err
        log({'log_reference': LogReference.LOGSLA0002})


def get_approximate_number_of_messages():
    sqs_client = boto3.client('sqs')
    response = sqs_client.get_queue_attributes(
        QueueUrl=FAILURE_QUEUE,
        AttributeNames=['ApproximateNumberOfMessages'])
    queue_attributes = response['Attributes']
    messages = queue_attributes.get('ApproximateNumberOfMessages', 0)
    return int(messages)


def format_slack_message(number_of_messages):
    return [
        {
            'type': 'section',
            'text': {
                'type': 'mrkdwn',
                'text': f'*DLQ URL*: {FAILURE_QUEUE}'
            }
        },
        {
            'type': 'section',
            'text': {
                'type': 'mrkdwn',
                'text': f'*Number of messages*: {number_of_messages}'
            }
        }
    ]


def post_message_to_slack(message):
    payload = {
        'channel': 'automated-notifications',
        'username': 'AWS DLQ Error: Data transport failure',
        'blocks': message,
        'icon_url': 'https://uxwing.com/wp-content/themes/uxwing/download/10-brands-and-social-media/aws.png'
    }
    response = requests.post(SLACK_WEBHOOK_URL, json.dumps(payload))
    response.raise_for_status()
