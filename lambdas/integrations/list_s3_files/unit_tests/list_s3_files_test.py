from unittest import TestCase
from mock import patch, Mock, MagicMock, call
from common.test_mocks.mock_events import sns_mock_event


class TestListS3Files(TestCase):
    queue_url = 'sqs_queue_url'
    bucket_url = 's3_bucket'
    mock_env_vars = {
        'SQS_QUEUE_URL': queue_url,
        'S3_BUCKET': bucket_url
    }

    @patch('os.environ', mock_env_vars)
    @patch('boto3.resource', MagicMock())
    @patch('boto3.client', MagicMock())
    def setUp(self):
        import list_s3_files.list_s3_files as _list_s3_files
        self.list_s3_files = _list_s3_files
        self.log_patcher = patch('common.log.logger')
        self.log_patcher.start()
        self.context = Mock()
        self.context.function_name = 'list_s3_files_test'

    def tearDown(self):
        self.log_patcher.stop()

    @patch('list_s3_files.list_s3_files.log')
    @patch('list_s3_files.list_s3_files.send_new_message')
    @patch('list_s3_files.list_s3_files.read_bucket')
    def test_lambda_get_files_from_s3_bucket(self,
                                             s3_client_mock,
                                             sqs_client_mock,
                                             logger_mock):
        # when
        self.list_s3_files.lambda_handler(sns_mock_event({'message': 'some message'}), self.context)
        # then
        s3_client_mock.assert_called()
        s3_client_mock.assert_called_with(self.bucket_url)

    @patch('list_s3_files.list_s3_files.log')
    @patch('list_s3_files.list_s3_files.send_new_message')
    @patch('list_s3_files.list_s3_files.read_bucket')
    @patch('list_s3_files.list_s3_files.get_file_contents_from_bucket')
    def test_lambda_handler_should_send_sqs_message_for_all_files(self,
                                                                  get_s3_file_contents_mock,
                                                                  s3_client_mock,
                                                                  sqs_client_mock,
                                                                  logger_mock):
        # given
        file_list = {"Contents": [{"Key": 'file1', 'LastModified': '2020/1/1'},
                                  {"Key": 'file2', 'LastModified': '2020/1/1'}]}

        expected_sqs_assert = [
            call(self.queue_url, {
                'name': 'file1',
                'bucket': 's3_bucket',
                'last_modified': '2020/1/1',
                'internal_id': 1
            }),
            call(self.queue_url, {
                'name': 'file2',
                'bucket': 's3_bucket',
                'last_modified': '2020/1/1',
                'internal_id': 2
            })
        ]
        sqs_client_mock.return_value = True
        s3_client_mock.return_value = file_list
        get_s3_file_contents_mock.side_effect = [
            {
                'body': 'data',
                'meta_data': {
                    'internal_id': 1
                }
            },
            {
                'body': 'data',
                'meta_data': {
                    'internal_id': 2
                }
            }
        ]
        # when
        sqs_client_mock.return_value = True
        s3_client_mock.return_value = file_list
        self.list_s3_files.lambda_handler(sns_mock_event({'message': 'some message'}), self.context)

        # then
        sqs_client_mock.assert_has_calls(expected_sqs_assert)

    @patch('list_s3_files.list_s3_files.log')
    @patch('list_s3_files.list_s3_files.send_new_message')
    @patch('list_s3_files.list_s3_files.read_bucket')
    def test_lambda_handler_should_not_send_sqs_message_when_no_files_found(self,
                                                                            s3_client_mock,
                                                                            sqs_client_mock,
                                                                            logger_mock):

        # when
        sqs_client_mock.return_value = True
        s3_client_mock.return_value = {}
        self.list_s3_files.lambda_handler(sns_mock_event({'message': 'some message'}), self.context)
        # then
        sqs_client_mock.assert_not_called()
