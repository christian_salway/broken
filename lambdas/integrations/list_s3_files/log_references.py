import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    LIST1 = (logging.INFO, 'Reading S3 Bucket to look for files')
    LIST2 = (logging.INFO, 'No files found in S3 Bucket')
    LIST3 = (logging.INFO, 'Found files in bucket')
    LIST4 = (logging.INFO, 'Sending file to SQS')
    LIST5 = (logging.ERROR, 'Unable to read files from S3 bucket')
