import os
from common.log import log
from list_s3_files.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.s3_utils import read_bucket, get_file_contents_from_bucket
from common.utils.sqs_utils import send_new_message

SQS_QUEUE_URL = os.environ.get('SQS_QUEUE_URL')
S3_BUCKET = os.environ.get('S3_BUCKET')


@lambda_entry_point
def lambda_handler(event, context):
    try:
        log({'log_reference': LogReference.LIST1, 'event': event})
        files_in_bucket = read_bucket(S3_BUCKET)

        if 'Contents' not in files_in_bucket:
            log({'log_reference': LogReference.LIST2})
            return

        contents = files_in_bucket['Contents']
        log({'log_reference': LogReference.LIST3, 'file_count': len(contents)})

        for file in contents:
            file_meta_data = get_file_contents_from_bucket(S3_BUCKET, file['Key'])['meta_data']
            internal_id = file_meta_data['internal_id']
            log({'log_reference': LogReference.LIST4, 'file': file['Key'], 'object_internal_id': internal_id})
            message = {
                "name": file['Key'],
                "bucket": S3_BUCKET,
                "last_modified": file['LastModified'],
                "internal_id": internal_id
            }
            send_new_message(SQS_QUEUE_URL, message)
    except Exception as e:
        log({'log_reference': LogReference.LIST5, 'error': str(e)})
        raise e
