# List S3 Files

- [List S3 Files](#list-s3-files)
  - [Process](#process)
  - [Architecture](#architecture)

## Process

>Previous step is: NONE

>Next step is [decode_edifact_result](../../results/split_results_file/README.md)

- Lambda is subscribed to an SNS Topic.
- Lambda retrieves a list of files in an S3 bucket.
- For each item found, it enqueues a message on an SQS queue.
- Each enqueued message includes:
  - Reference to the file (name, path, bucket).
  - The file's LastModified attribute.

## Architecture

See the technical overview for more information: https://nhsd-confluence.digital.nhs.uk/display/CSP/Results+-+Technical+Overview
