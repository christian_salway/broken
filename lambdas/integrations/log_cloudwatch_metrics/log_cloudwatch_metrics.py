import concurrent
from datetime import datetime, timedelta, timezone
from functools import partial
from common.log import log
from log_cloudwatch_metrics.cloudwatch_client import initialise_cloudwatch_client
from log_cloudwatch_metrics.concurrent_executor import get_executor
from log_cloudwatch_metrics.log_references import LogReference
from common.utils.lambda_wrappers import lambda_entry_point
from log_cloudwatch_metrics.sqs_metrics import get_sqs_metrics
from log_cloudwatch_metrics.s3_metrics import get_s3_metrics
from log_cloudwatch_metrics.waf_metrics import get_waf_metrics
from log_cloudwatch_metrics.sns_metrics import get_sns_metrics
from log_cloudwatch_metrics.cloudwatch_events_metrics import get_cloudwatch_events_metrics
from log_cloudwatch_metrics.dynamodb_metrics import get_dynamodb_metrics
from log_cloudwatch_metrics.lambda_metrics import get_lambda_metrics


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.METRICS_1})
    initialise_cloudwatch_client()
    forward_metrics()


def forward_metrics():
    end_time = datetime.now(timezone.utc)
    start_time = end_time - timedelta(minutes=10)
    metric_functions = [
        get_sqs_metrics,
        get_s3_metrics,
        get_waf_metrics,
        get_sns_metrics,
        get_cloudwatch_events_metrics,
        get_dynamodb_metrics,
        get_lambda_metrics,
    ]
    tasks = [get_executor().submit(partial(function, start_time, end_time))
             for function in metric_functions]
    results, _pending = concurrent.futures.wait(tasks)
    exceptions = [result.exception() for result in results if isinstance(result.exception(), Exception)]
    if exceptions:
        log({'log_reference': LogReference.METRICS_2, 'exceptions': str(exceptions)})
