import concurrent
from functools import partial
import os
from boto3 import client
from botocore.client import Config
from common.log import log
from log_cloudwatch_metrics.concurrent_executor import get_executor
from log_cloudwatch_metrics.filter_resources import filter_resource
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics.exceptions import AsyncExceptions
from log_cloudwatch_metrics.cloudwatch_client import query_cloudwatch_metric, get_metric_from_response

# Rather than accessing this global directly use the get_cloudwatch_events_client() function which aids in testability
_CLOUDWATCH_EVENTS_CLIENT = None


def get_environment():
    return os.environ.get('ENVIRONMENT_NAME')


def get_cloudwatch_events_client():
    global _CLOUDWATCH_EVENTS_CLIENT
    if not _CLOUDWATCH_EVENTS_CLIENT:
        _CLOUDWATCH_EVENTS_CLIENT = client('events', config=Config(retries={'max_attempts': 3}))
    return _CLOUDWATCH_EVENTS_CLIENT


def get_cloudwatch_events_metrics(start_time, end_time):
    # Can add more metrics in here if needed
    metrics = [
        'FailedInvocations',
        'Invocations',
    ]
    rule_list = get_rules()
    tasks = []
    for rule in rule_list:
        log({'log_reference': LogReference.METRIC_CLOUDEVENTS_1, 'rule_name': rule})
        tasks.extend([
            get_executor().submit(partial(get_metric, rule, start_time, end_time, metric))
            for metric in metrics
        ])
    results, _pending = concurrent.futures.wait(tasks)
    exceptions = [result.exception() for result in results if isinstance(result.exception(), Exception)]
    if exceptions:
        raise AsyncExceptions('Errors getting Cloudwatch Event metrics', exceptions)


def get_metric(rule_name, start_time, end_time, metric_name):
    dimensions = [{'Name': 'RuleName', 'Value': rule_name}]
    response = query_cloudwatch_metric(start_time, end_time, 'AWS/Events', metric_name, dimensions, ['Sum'])
    metrics = get_metric_from_response(response)
    if metrics:
        log({'log_reference': LogReference.METRIC_CLOUDEVENTS_2,
             'timestamp': metrics['Timestamp'].isoformat(),
             'rule_name': rule_name,
             metric_name: metrics['Sum'],
             'statistic': 'Sum'})
    else:
        log({'log_reference': LogReference.METRIC_CLOUDEVENTS_3,
             'rule_name': rule_name,
             'metric_name': metric_name,
             'statistic': 'Sum'})


def get_rules():
    response = get_cloudwatch_events_client().list_rules()
    rule_objects = response.get('Rules', [])
    next_token = response.get('NextToken')
    while next_token:
        response = get_cloudwatch_events_client().list_rules(NextToken=next_token)
        rule_objects.extend(response.get('Rules', []))
        next_token = response.get('NextToken')
    rule_names = [rule['Name'] for rule in rule_objects]
    filtered_rules = [rule for rule in rule_names if filter_resource(get_environment(), rule)]
    return filtered_rules
