import concurrent
from functools import partial
import os
from boto3 import client
from botocore.client import Config
from common.log import log
from log_cloudwatch_metrics.concurrent_executor import get_executor
from log_cloudwatch_metrics.filter_resources import filter_resource
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics.exceptions import AsyncExceptions
from log_cloudwatch_metrics.cloudwatch_client import query_cloudwatch_metric, get_metric_from_response

# Rather than accessing this global directly use the get_sqs_client() function which aids in testability
_SQS_CLIENT = None


def get_environment():
    return os.environ.get('ENVIRONMENT_NAME')


def get_sqs_client():
    global _SQS_CLIENT
    if not _SQS_CLIENT:
        _SQS_CLIENT = client('sqs', config=Config(retries={'max_attempts': 3}))
    return _SQS_CLIENT


def get_sqs_metrics(start_time, end_time):
    metrics = [
        'NumberOfMessagesReceived',
        'ApproximateNumberOfMessagesVisible',
    ]
    queues_list = get_queues_list()
    tasks = []
    for queue_url in queues_list:
        log({'log_reference': LogReference.METRIC_SQS_1, 'queue_url': queue_url})
        queue_name = queue_url.split('/')[4]
        # run these functions on the executor
        tasks.extend([
            get_executor().submit(partial(get_metric, queue_name, metric, start_time, end_time))
            for metric in metrics
        ])
    results, _pending = concurrent.futures.wait(tasks)
    exceptions = [result.exception() for result in results if isinstance(result.exception(), Exception)]
    if exceptions:
        raise AsyncExceptions('Errors getting SQS metrics', exceptions)


def get_metric(queue_name, metric_name, start_time, end_time):
    dimensions = [{'Name': 'QueueName', 'Value': queue_name}]
    response = query_cloudwatch_metric(start_time, end_time, "AWS/SQS", metric_name,
                                       dimensions, ['Sum'])
    metrics = get_metric_from_response(response)
    if metrics:
        log({'log_reference': LogReference.METRIC_SQS_2,
             'timestamp': metrics['Timestamp'].isoformat(),
             'queue_name': queue_name,
             metric_name: metrics['Sum'],
            'statistic': 'Sum', })
    else:
        log({'log_reference': LogReference.METRIC_SQS_3,
             'queue_name': queue_name,
             'metric_name': metric_name,
            'statistic': 'Sum', })


def get_queues_list():
    response = get_sqs_client().list_queues(
        QueueNamePrefix=get_environment(),
        MaxResults=1000
    )

    filtered_queue_list = [
        queue_url for queue_url in response['QueueUrls']
        if filter_resource(get_environment(), queue_url)
    ]

    return filtered_queue_list
