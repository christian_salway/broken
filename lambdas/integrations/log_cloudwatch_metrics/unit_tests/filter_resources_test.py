from unittest import TestCase
from ddt import ddt, data, unpack
from log_cloudwatch_metrics.filter_resources import filter_resource


@ddt
class TestFilterResource(TestCase):

    @unpack
    @data(
        ('master', 'master-resource'),
        ('master-stubbed', 'master-stubbed-resource'),
        ('cerss-0000', 'cerss-0000-resource'))
    def test_filter_resources_returns_true_when_correct_env(self, environment, resource):
        result = filter_resource(environment, resource)

        self.assertTrue(result)

    @unpack
    @data(
        ('master', 'master-stubbed-resource'),
        ('master-stubbed', 'master-resource'))
    def test_filter_resources_returns_false_when_not_correct_env(self, environment, resource):
        result = filter_resource(environment, resource)

        self.assertFalse(result)
