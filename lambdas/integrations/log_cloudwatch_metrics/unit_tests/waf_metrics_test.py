from unittest import TestCase
from mock import Mock, MagicMock, patch, call
from datetime import datetime, timezone
from log_cloudwatch_metrics.log_references import LogReference
from ddt import ddt, data, unpack


@patch('common.log.logger', MagicMock())
@patch('boto3.client', MagicMock())
@patch('os.environ', {'ENVIRONMENT_NAME': 'test'})
@ddt
class TestWAFCloudWatchMetrics(TestCase):

    @patch('boto3.client')
    def setUp(self, boto3_client):
        # Setup module imports
        import log_cloudwatch_metrics.waf_metrics as _waf_metrics
        self.waf_metrics_module = _waf_metrics

        # Setup logger
        self.log_patcher = patch('common.log.log')

        # Setup cloudwatch_mock and sqs_client mock
        sqs_client_mock = Mock()
        cloudwatch_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: \
            sqs_client_mock if args and args[0] == 'sqs' else \
            cloudwatch_mock if args and args[0] == 'cloudwatch' else Mock()

        # Reset and start all used mocks
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('log_cloudwatch_metrics.waf_metrics.query_cloudwatch_metric')
    @patch('log_cloudwatch_metrics.waf_metrics.log')
    def test_waf_get_metrics_logs_when_metrics_exist(self,
                                                     log_mock,
                                                     query_cloudwatch_metric_mock):
        # Arrange
        timestamp = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        self.waf_metrics_module.get_waf_metrics(start_time, end_time)

        # Assert
        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_WAF_1}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_2,
                'timestamp': timestamp.isoformat(),
                'rule': 'ALL',
                'BlockedRequests': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_2,
                'timestamp': timestamp.isoformat(),
                'rule': 'ALL',
                'AllowedRequests': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_2,
                'timestamp': timestamp.isoformat(),
                'rule': 'ALL',
                'CountedRequests': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_2,
                'timestamp': timestamp.isoformat(),
                'rule': 'ALL',
                'PassedRequests': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_2,
                'timestamp': timestamp.isoformat(),
                'rule': 'test-allow-uk-traffic-metric',
                'BlockedRequests': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_2,
                'timestamp': timestamp.isoformat(),
                'rule': 'test-ip-address-hard-limit-metric',
                'BlockedRequests': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_2,
                'timestamp': timestamp.isoformat(),
                'rule': 'test-ip-address-soft-limit-metric',
                'CountedRequests': 5.0,
                'statistic': 'Sum'}
            )
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

        query_cloudwatch_metric_mock.assert_has_calls(get_expected_metric_calls(start_time, end_time), any_order=True)

    @patch('log_cloudwatch_metrics.waf_metrics.query_cloudwatch_metric')
    @patch('log_cloudwatch_metrics.waf_metrics.log')
    def test_waf_no_log_results_when_metrics_do_not_exist(self,
                                                          log_mock,
                                                          query_cloudwatch_metric_mock):
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        query_cloudwatch_metric_mock.side_effect = blank_metric_response_side_effect
        self.waf_metrics_module.get_waf_metrics(start_time, end_time)

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_WAF_1}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_3,
                'rule': 'ALL',
                'metric_name': 'BlockedRequests',
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_3,
                'rule': 'ALL',
                'metric_name': 'AllowedRequests',
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_3,
                'rule': 'ALL',
                'metric_name': 'CountedRequests',
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_3,
                'rule': 'ALL',
                'metric_name': 'PassedRequests',
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_3,
                'rule': 'test-allow-uk-traffic-metric',
                'metric_name': 'BlockedRequests',
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_3,
                'rule': 'test-ip-address-hard-limit-metric',
                'metric_name': 'BlockedRequests',
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_WAF_3,
                'rule': 'test-ip-address-soft-limit-metric',
                'metric_name': 'CountedRequests',
                'statistic': 'Sum'}
            )
        ]

        log_mock.assert_has_calls(expected_log_calls, any_order=True)

        query_cloudwatch_metric_mock.assert_has_calls(get_expected_metric_calls(start_time, end_time), any_order=True)

    @unpack
    @data(
        ({}, None),
        (
            {'Datapoints': [{'Timestamp': datetime(2019, 12, 31, 23, 50), 'Sum': 5.0, 'Unit': 'Count'}]},
            {'Timestamp': datetime(2019, 12, 31, 23, 50), 'Sum': 5.0, 'Unit': 'Count'}
        )
    )
    def test_get_metric_from_response_returns_correct_response(self, metrics, expected_response):
        response = self.waf_metrics_module.get_metric_from_response(metrics)
        self.assertEqual(response, expected_response)


def metric_response_side_effect(*args, **kwargs):
    return {
        'Metrics': [
            {
                'Namespace': 'AWS/WAFV2',
                'Dimensions': [
                    {
                        'Name': 'WebACL',
                        'Value': 'master-waf-v2'
                    },
                    {
                        'Name': 'Region',
                        'Value': 'eu-west-2'
                    },
                    {
                        'Name': 'Rule',
                        'Value': 'ALL'
                    }
                ],
                'MetricName': args[0]
            }
        ],
        'Datapoints': [{'Timestamp': datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc), 'Sum': 5.0, 'Unit': 'Count'}],
        'NextToken': 'xxx'
    }


def blank_metric_response_side_effect(*args, **kwargs):
    return {
        'Metrics': [
            {
                'Namespace': 'AWS/WAFV2',
                'Dimensions': [
                    {
                        'Name': 'WebACL',
                        'Value': 'master-waf-v2'
                    },
                    {
                        'Name': 'Region',
                        'Value': 'eu-west-2'
                    },
                    {
                        'Name': 'Rule',
                        'Value': 'ALL'
                    }
                ],
                'MetricName': args[0]
            }
        ],
        'Datapoints': [],
        'NextToken': 'xxx'
    }


def get_expected_metric_calls(start_time, end_time):
    expected_metric_calls = [
        call(
            start_time,
            end_time,
            'AWS/WAFV2',
            'BlockedRequests',
            [{'Name': 'WebACL', 'Value': 'test-waf-v2'}, {'Name': 'Region', 'Value': 'eu-west-2'},
             {'Name': 'Rule', 'Value': 'ALL'}],
            ['Sum']
        ),
        call(
            start_time,
            end_time,
            'AWS/WAFV2',
            'AllowedRequests',
            [{'Name': 'WebACL', 'Value': 'test-waf-v2'}, {'Name': 'Region', 'Value': 'eu-west-2'},
             {'Name': 'Rule', 'Value': 'ALL'}],
            ['Sum']
        ),
        call(
            start_time,
            end_time,
            'AWS/WAFV2',
            'CountedRequests',
            [{'Name': 'WebACL', 'Value': 'test-waf-v2'}, {'Name': 'Region', 'Value': 'eu-west-2'},
             {'Name': 'Rule', 'Value': 'ALL'}],
            ['Sum']
        ),
        call(
            start_time,
            end_time,
            'AWS/WAFV2',
            'PassedRequests',
            [{'Name': 'WebACL', 'Value': 'test-waf-v2'}, {'Name': 'Region', 'Value': 'eu-west-2'},
             {'Name': 'Rule', 'Value': 'ALL'}],
            ['Sum']
        ),
        call(
            start_time,
            end_time,
            'AWS/WAFV2',
            'BlockedRequests',
            [{'Name': 'WebACL', 'Value': 'test-waf-v2'}, {'Name': 'Region', 'Value': 'eu-west-2'},
             {'Name': 'Rule', 'Value': 'test-allow-uk-traffic-metric'}],
            ['Sum']
        ),
        call(
            start_time,
            end_time,
            'AWS/WAFV2',
            'BlockedRequests',
            [{'Name': 'WebACL', 'Value': 'test-waf-v2'}, {'Name': 'Region', 'Value': 'eu-west-2'},
             {'Name': 'Rule', 'Value': 'test-ip-address-hard-limit-metric'}],
            ['Sum']
        ),
        call(
            start_time,
            end_time,
            'AWS/WAFV2',
            'CountedRequests',
            [{'Name': 'WebACL', 'Value': 'test-waf-v2'}, {'Name': 'Region', 'Value': 'eu-west-2'},
             {'Name': 'Rule', 'Value': 'test-ip-address-soft-limit-metric'}],
            ['Sum']
        ),
    ]

    return expected_metric_calls
