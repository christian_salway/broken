from unittest import TestCase
from mock import Mock, patch, MagicMock, call
from datetime import datetime, timezone
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics import s3_metrics


boto3_client = MagicMock()


@patch('common.log.logger', MagicMock())
@patch('boto3.client', boto3_client)
@patch('log_cloudwatch_metrics.s3_metrics.log')
@patch('log_cloudwatch_metrics.s3_metrics.query_cloudwatch_metric')
@patch('os.environ', {'ENVIRONMENT_NAME': 'test'})
class TestS3CloudWatchMetrics(TestCase):
    def setUp(self):
        # Setup logger
        self.log_patcher = patch('common.log.log')

        # Setup cloudwatch_mock and s3_client mock
        s3_client_mock = Mock()
        cloudwatch_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: \
            s3_client_mock if args and args[0] == 's3' else \
            cloudwatch_mock if args and args[0] == 'cloudwatch' else Mock()

        # Reset and start all used mocks
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('log_cloudwatch_metrics.s3_metrics.get_buckets')
    def test_s3_get_metrics_logs_when_metrics_exist(self,
                                                    get_buckets_mock,
                                                    query_cloudwatch_metric_mock,
                                                    log_mock):
        # Arrange
        timestamp = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_buckets_mock.return_value = ['test']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        s3_metrics.get_s3_metrics(start_time, end_time)

        # Assert
        get_buckets_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_S3_1,
                'bucket_name': 'test'}
            ),
            call({
                'log_reference': LogReference.METRIC_S3_2,
                'timestamp': timestamp.isoformat(),
                'bucket_name': 'test',
                'Metric4xxErrors': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_S3_2,
                'timestamp': timestamp.isoformat(),
                'bucket_name': 'test',
                'Metric5xxErrors': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_S3_2,
                'timestamp': timestamp.isoformat(),
                'bucket_name': 'test',
                'MetricTotalRequestLatency': 19.0,
                'statistic': 'p99'}
            )
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

        expected_metric_calls = [
            call(
                start_time,
                end_time,
                "AWS/S3",
                '4xxErrors',
                [{"Name": "BucketName", "Value": 'test'},
                 {"Name": "FilterId", "Value": "EntireBucket"}],
                ['Sum'],
                extended=False
            ),
            call(
                start_time,
                end_time,
                "AWS/S3",
                '5xxErrors',
                [{"Name": "BucketName", "Value": 'test'},
                 {"Name": "FilterId", "Value": "EntireBucket"}],
                ['Sum'],
                extended=False
            ),
            call(
                start_time,
                end_time,
                "AWS/S3",
                'TotalRequestLatency',
                [{"Name": "BucketName", "Value": 'test'},
                 {"Name": "FilterId", "Value": "EntireBucket"}],
                ['p99'],
                extended=True
            )
        ]
        query_cloudwatch_metric_mock.assert_has_calls(expected_metric_calls, any_order=True)

    @patch('log_cloudwatch_metrics.s3_metrics.get_buckets')
    def test_lambda_should_not_log_data_if_no_values_returned(self,
                                                              get_buckets_mock,
                                                              query_cloudwatch_metric_mock,
                                                              log_mock):
        # Arrange
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_buckets_mock.return_value = ['none']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        s3_metrics.get_s3_metrics(start_time, end_time)

        # Assert
        get_buckets_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_S3_1,
                'bucket_name': 'none'}),
            call({
                'log_reference': LogReference.METRIC_S3_3,
                'bucket_name': 'none',
                'metric_name': '4xxErrors',
                'statistic': 'Sum'}),
            call({
                'log_reference': LogReference.METRIC_S3_3,
                'bucket_name': 'none',
                'metric_name': '5xxErrors',
                'statistic': 'Sum'}),
            call({
                'log_reference': LogReference.METRIC_S3_3,
                'bucket_name': 'none',
                'metric_name': 'TotalRequestLatency',
                'statistic': 'p99'}),
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

    def test_get_visible_messages_metrics_do_not_break_if_no_datapoints(self, query_cloudwatch_metric_mock, _):
        query_cloudwatch_metric_mock.return_value = {'Datapoints': []}
        try:
            s3_metrics.get_metric('', '', '', '', '', '')
        except Exception:
            self.fail("get_metric() raised an exception")

    @patch('log_cloudwatch_metrics.s3_metrics.get_s3_client')
    @patch('log_cloudwatch_metrics.s3_metrics.filter_resource')
    def test_get_buckets(self, filter_resource_mock, s3_mock, _, __):
        # Arrange
        list_buckets_mock = Mock()
        list_buckets_mock.list_buckets.side_effect = [
            {
                'Buckets': [
                    {
                        'Name': 'test1'
                    },
                    {
                        'Name': 'test2'
                    },
                    {
                        'Name': 'another-env-test3'
                    },
                ],
            }
        ]
        s3_mock.return_value = list_buckets_mock
        filter_resource_mock.side_effect = [True, True, False]
        # Act
        buckets = s3_metrics.get_buckets()
        # Assert
        self.assertEqual(buckets, ['test1', 'test2'])
        list_buckets_mock.list_buckets.assert_called_once()
        filter_resource_mock.assert_has_calls([
            call('test', 'test1'),
            call('test', 'test2'),
            call('test', 'another-env-test3')
        ])


def metric_response_side_effect(*args, **kwargs):
    datapoints = [] if args[4][0]['Value'] == 'none' else [
        {
            'Timestamp': datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc),
            'Sum': 5.0, 'Unit': 'Count',
            'ExtendedStatistics': {'p99': 19.0}
        }
    ]
    return {
        "Metrics": [
            {
                "Namespace": "AWS/S3",
                "Dimensions": [
                    {
                        "Name": "BucketName",
                        "Value": "test"
                    }
                ],
                "MetricName": "4XXErrors"
            }
        ],
        'Datapoints': datapoints,
        "NextToken": None
    }
