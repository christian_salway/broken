from unittest import TestCase
from mock import Mock, patch, MagicMock, call
from datetime import datetime, timezone
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics import sns_metrics


boto3_client = MagicMock()


@patch('common.log.logger', MagicMock())
@patch('boto3.client', boto3_client)
@patch('log_cloudwatch_metrics.sns_metrics.log')
@patch('log_cloudwatch_metrics.sns_metrics.query_cloudwatch_metric')
@patch('os.environ', {'ENVIRONMENT_NAME': 'test'})
class TestSNSCloudWatchMetrics(TestCase):
    def setUp(self):
        # Setup logger
        self.log_patcher = patch('common.log.log')

        # Setup cloudwatch_mock and sns_client mock
        sns_client_mock = Mock()
        cloudwatch_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: \
            sns_client_mock if args and args[0] == 'sns' else \
            cloudwatch_mock if args and args[0] == 'cloudwatch' else Mock()

        # Reset and start all used mocks
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('log_cloudwatch_metrics.sns_metrics.get_topics')
    def test_sns_get_metrics_logs_when_metrics_exist(self,
                                                     get_topics_mock,
                                                     query_cloudwatch_metric_mock,
                                                     log_mock):
        # Arrange
        timestamp = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_topics_mock.return_value = ['test']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        sns_metrics.get_sns_metrics(start_time, end_time)

        # Assert
        get_topics_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_SNS_1,
                'topic_name': 'test'}
            ),
            call({
                'log_reference': LogReference.METRIC_SNS_2,
                'timestamp': timestamp.isoformat(),
                'topic_name': 'test',
                'NumberOfNotificationsDelivered': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_SNS_2,
                'timestamp': timestamp.isoformat(),
                'topic_name': 'test',
                'NumberOfNotificationsFailed': 5.0,
                'statistic': 'Sum'}
            )
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

        expected_metric_calls = [
            call(
                start_time,
                end_time,
                'AWS/SNS',
                'NumberOfNotificationsDelivered',
                [{'Name': 'TopicName', 'Value': 'test'}],
                ['Sum'],
            ),
            call(
                start_time,
                end_time,
                'AWS/SNS',
                'NumberOfNotificationsFailed',
                [{'Name': 'TopicName', 'Value': 'test'}],
                ['Sum'],
            )
        ]
        query_cloudwatch_metric_mock.assert_has_calls(expected_metric_calls, any_order=True)

    @patch('log_cloudwatch_metrics.sns_metrics.get_topics')
    def test_lambda_should_not_log_data_if_no_values_returned(self,
                                                              get_topics_mock,
                                                              query_cloudwatch_metric_mock,
                                                              log_mock):
        # Arrange
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_topics_mock.return_value = ['none']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        sns_metrics.get_sns_metrics(start_time, end_time)

        # Assert
        get_topics_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_SNS_1,
                'topic_name': 'none'}),
            call({
                'log_reference': LogReference.METRIC_SNS_3,
                'topic_name': 'none',
                'metric_name': 'NumberOfNotificationsDelivered',
                'statistic': 'Sum'}),
            call({
                'log_reference': LogReference.METRIC_SNS_3,
                'topic_name': 'none',
                'metric_name': 'NumberOfNotificationsFailed',
                'statistic': 'Sum'}),
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

    def test_get_visible_messages_metrics_do_not_break_if_no_datapoints(self, query_cloudwatch_metric_mock, _):
        query_cloudwatch_metric_mock.return_value = {'Datapoints': []}
        try:
            sns_metrics.get_metric('', '', '', '')
        except Exception:
            self.fail("get_metric() raised an exception")

    @patch('log_cloudwatch_metrics.sns_metrics.get_sns_client')
    @patch('log_cloudwatch_metrics.sns_metrics.filter_resource')
    def test_get_topics(self, filter_resource_mock, sns_mock, _, __):
        # Arrange
        list_topics_mock = Mock()
        list_topics_mock.list_topics.side_effect = [
            {
                'Topics': [
                    {
                        'TopicArn': 'arn:aws:sns:eu-west-2:000000000000:test1'
                    },
                    {
                        'TopicArn': 'arn:aws:sns:eu-west-2:000000000000:test2'
                    },
                ],
                'NextToken': 'my_next_token'
            },
            {
                'Topics': [
                    {
                        'TopicArn': 'arn:aws:sns:eu-west-2:000000000000:test3'
                    },
                    {
                        'TopicArn': 'arn:aws:sns:eu-west-2:000000000000:another_environment'
                    },
                ],
            },
        ]
        sns_mock.return_value = list_topics_mock
        filter_resource_mock.side_effect = [True, True, True, False]
        # Act
        topics = sns_metrics.get_topics()
        # Assert
        self.assertEqual(topics, ['test1', 'test2', 'test3'])
        list_topics_mock.list_topics.assert_has_calls([call(), call(NextToken='my_next_token')])
        filter_resource_mock.assert_has_calls([
            call('test', 'test1'),
            call('test', 'test2'),
            call('test', 'test3'),
            call('test', 'another_environment')
        ])


def metric_response_side_effect(*args, **kwargs):
    datapoints = [] if args[4][0]['Value'] == 'none' else [
        {
            'Timestamp': datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc),
            'Sum': 5.0, 'Unit': 'Count'
        }
    ]
    return {
        "Metrics": [
            {
                "Namespace": "AWS/SNS",
                "Dimensions": [
                    {
                        "Name": "TopicName",
                        "Value": "test"
                    }
                ],
                "MetricName": args[3]
            }
        ],
        'Datapoints': datapoints,
        "NextToken": None
    }
