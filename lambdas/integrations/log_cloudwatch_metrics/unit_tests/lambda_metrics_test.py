from unittest import TestCase
from mock import Mock, patch, MagicMock, call
from datetime import datetime, timezone
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics import lambda_metrics


boto3_client = MagicMock()


@patch('common.log.logger', MagicMock())
@patch('boto3.client', boto3_client)
@patch('log_cloudwatch_metrics.lambda_metrics.log')
@patch('log_cloudwatch_metrics.lambda_metrics.query_cloudwatch_metric')
@patch('os.environ', {'ENVIRONMENT_NAME': 'test'})
class TestLambdaCloudWatchMetrics(TestCase):
    def setUp(self):
        # Setup logger
        self.log_patcher = patch('common.log.log')

        # Setup cloudwatch_mock and lambda_client mock
        lambda_client_mock = Mock()
        cloudwatch_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: \
            lambda_client_mock if args and args[0] == 'lambda' else \
            cloudwatch_mock if args and args[0] == 'cloudwatch' else Mock()

        # Reset and start all used mocks
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('log_cloudwatch_metrics.lambda_metrics.get_lambdas')
    def test_lambda_get_metrics_logs_when_metrics_exist(self,
                                                        get_functions_mock,
                                                        query_cloudwatch_metric_mock,
                                                        log_mock):
        # Arrange
        timestamp = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_functions_mock.return_value = ['test']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        lambda_metrics.get_lambda_metrics(start_time, end_time)

        # Assert
        get_functions_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_LAMBDA_1,
                'metric_function_name': 'test'}
            ),
            call({
                'log_reference': LogReference.METRIC_LAMBDA_2,
                'timestamp': timestamp.isoformat(),
                'metric_function_name': 'test',
                'Duration': 5.0,
                'statistic': 'Maximum'}
            ),
            call({
                'log_reference': LogReference.METRIC_LAMBDA_2,
                'timestamp': timestamp.isoformat(),
                'metric_function_name': 'test',
                'Errors': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_LAMBDA_2,
                'timestamp': timestamp.isoformat(),
                'metric_function_name': 'test',
                'Throttles': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_LAMBDA_2,
                'timestamp': timestamp.isoformat(),
                'metric_function_name': 'test',
                'Invocations': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_LAMBDA_2,
                'timestamp': timestamp.isoformat(),
                'metric_function_name': 'test',
                'DeadLetterErrors': 5.0,
                'statistic': 'Sum'}
            ),
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

        expected_metric_calls = [
            call(
                start_time,
                end_time,
                'AWS/Lambda',
                'Duration',
                [{"Name": "FunctionName", "Value": 'test'}],
                ['Maximum'],
            ),
            call(
                start_time,
                end_time,
                'AWS/Lambda',
                'Errors',
                [{"Name": "FunctionName", "Value": 'test'}],
                ['Sum'],
            ),
            call(
                start_time,
                end_time,
                'AWS/Lambda',
                'Throttles',
                [{"Name": "FunctionName", "Value": 'test'}],
                ['Sum'],
            ),
            call(
                start_time,
                end_time,
                'AWS/Lambda',
                'Invocations',
                [{"Name": "FunctionName", "Value": 'test'}],
                ['Sum'],
            ),
            call(
                start_time,
                end_time,
                'AWS/Lambda',
                'DeadLetterErrors',
                [{"Name": "FunctionName", "Value": 'test'}],
                ['Sum'],
            ),
        ]
        query_cloudwatch_metric_mock.assert_has_calls(expected_metric_calls, any_order=True)

    @patch('log_cloudwatch_metrics.lambda_metrics.get_lambdas')
    def test_lambda_should_not_log_data_if_no_values_returned(self,
                                                              get_functions_mock,
                                                              query_cloudwatch_metric_mock,
                                                              log_mock):
        # Arrange
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_functions_mock.return_value = ['none']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        lambda_metrics.get_lambda_metrics(start_time, end_time)

        # Assert
        get_functions_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_LAMBDA_1,
                'metric_function_name': 'none'}),
            call({
                'log_reference': LogReference.METRIC_LAMBDA_3,
                'metric_function_name': 'none',
                'metric_name': 'Duration',
                'statistic': 'Maximum'}),
            call({
                'log_reference': LogReference.METRIC_LAMBDA_3,
                'metric_function_name': 'none',
                'metric_name': 'Errors',
                'statistic': 'Sum'}),
            call({
                'log_reference': LogReference.METRIC_LAMBDA_3,
                'metric_function_name': 'none',
                'metric_name': 'Throttles',
                'statistic': 'Sum'}),
            call({
                'log_reference': LogReference.METRIC_LAMBDA_3,
                'metric_function_name': 'none',
                'metric_name': 'Invocations',
                'statistic': 'Sum'}),
            call({
                'log_reference': LogReference.METRIC_LAMBDA_3,
                'metric_function_name': 'none',
                'metric_name': 'DeadLetterErrors',
                'statistic': 'Sum'}),
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

    def test_get_visible_messages_metrics_do_not_break_if_no_datapoints(self, query_cloudwatch_metric_mock, _):
        query_cloudwatch_metric_mock.return_value = {'Datapoints': []}
        try:
            lambda_metrics.get_metric('', '', '', '', '')
        except Exception:
            self.fail("get_metric() raised an exception")

    @patch('log_cloudwatch_metrics.lambda_metrics.get_lambda_client')
    @patch('log_cloudwatch_metrics.lambda_metrics.filter_resource')
    def test_get_lambdas(self, filter_resource_mock, lambda_mock, _, __):
        # Arrange
        list_functions_mock = Mock()
        list_functions_mock.list_functions.side_effect = [
            {
                'Functions': [
                    {
                        'FunctionName': 'test1'
                    },
                    {
                        'FunctionName': 'test2'
                    },
                ],
                'NextMarker': 'my_next_token'
            },
            {
                'Functions': [
                    {
                        'FunctionName': 'test3'
                    },
                    {
                        'FunctionName': 'another_environment'
                    },
                ],
            },
        ]
        lambda_mock.return_value = list_functions_mock
        filter_resource_mock.side_effect = [True, True, True, False]
        # Act
        functions = lambda_metrics.get_lambdas()
        # Assert
        self.assertEqual(functions, ['test1', 'test2', 'test3'])
        list_functions_mock.list_functions.assert_has_calls([
            call(MaxItems=50),
            call(MaxItems=50, Marker='my_next_token')
        ])
        filter_resource_mock.assert_has_calls([
            call('test', 'test1'),
            call('test', 'test2'),
            call('test', 'test3'),
            call('test', 'another_environment')
        ])


def metric_response_side_effect(*args, **kwargs):
    datapoints = [] if args[4][0]['Value'] == 'none' else [
        {
            'Timestamp': datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc),
            'Maximum': 5.0,
            'Sum': 5.0,
            'Unit': 'Count',
        }
    ]
    return {
        "Metrics": [
            {
                "Namespace": "AWS/Lambda",
                "Dimensions": [
                    {
                        "Name": "FunctionName",
                        "Value": "test"
                    }
                ],
                "MetricName": "Duration"
            }
        ],
        'Datapoints': datapoints,
        "NextToken": None
    }
