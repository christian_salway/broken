from unittest import TestCase
from mock import Mock, patch, MagicMock, call
from datetime import datetime, timezone
from log_cloudwatch_metrics.log_references import LogReference


@patch('common.log.logger', MagicMock())
@patch('boto3.client', MagicMock())
@patch('os.environ', {'ENVIRONMENT_NAME': 'test'})
@patch('log_cloudwatch_metrics.sqs_metrics.query_cloudwatch_metric')
class TestSQSCloudWatchMetrics(TestCase):

    @patch('boto3.client')
    def setUp(self, boto3_client):
        # Setup module imports
        import log_cloudwatch_metrics.sqs_metrics as _sqs_metrics
        self.sqs_metrics_module = _sqs_metrics

        # Setup logger
        self.log_patcher = patch('common.log.log')

        # Setup cloudwatch_mock and sqs_client mock
        sqs_client_mock = Mock()
        cloudwatch_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: \
            sqs_client_mock if args and args[0] == 'sqs' else \
            cloudwatch_mock if args and args[0] == 'cloudwatch' else Mock()

        # Reset and start all used mocks
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('log_cloudwatch_metrics.sqs_metrics.log')
    @patch('log_cloudwatch_metrics.sqs_metrics.get_queues_list')
    def test_sqs_get_metrics_logs_when_metrics_exist(self,
                                                     get_queues_list_mock,
                                                     log_mock,
                                                     query_cloudwatch_metric_mock
                                                     ):
        timestamp = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_queues_list_mock.return_value = ['https://test.amazonaws.com/test/test-queue']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect
        self.sqs_metrics_module.get_sqs_metrics(start_time, end_time)

        get_queues_list_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_SQS_1,
                'queue_url': 'https://test.amazonaws.com/test/test-queue'}
            ),
            call({
                'log_reference': LogReference.METRIC_SQS_2,
                'timestamp': timestamp.isoformat(),
                'queue_name': 'test-queue',
                'ApproximateNumberOfMessagesVisible': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_SQS_2,
                'timestamp': timestamp.isoformat(),
                'queue_name': 'test-queue',
                'NumberOfMessagesReceived': 5.0,
                'statistic': 'Sum'}
            )
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

        expected_metric_calls = [
            call(
                start_time,
                end_time,
                'AWS/SQS',
                'ApproximateNumberOfMessagesVisible',
                [{'Name': 'QueueName', 'Value': 'test-queue'}],
                ['Sum']
            ),
            call(
                start_time,
                end_time,
                'AWS/SQS',
                'NumberOfMessagesReceived',
                [{'Name': 'QueueName', 'Value': 'test-queue'}],
                ['Sum']
            )
        ]
        query_cloudwatch_metric_mock.assert_has_calls(expected_metric_calls, any_order=True)

    @patch('log_cloudwatch_metrics.sqs_metrics.log')
    @patch('log_cloudwatch_metrics.sqs_metrics.get_queues_list')
    def test_lambda_should_not_log_data_if_no_values_returned(self,
                                                              get_queues_list_mock,
                                                              log_mock,
                                                              query_cloudwatch_metric_mock,
                                                              ):
        # Arrange
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_queues_list_mock.return_value = ['https://test.amazonaws.com/test/none']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        self.sqs_metrics_module.get_sqs_metrics(start_time, end_time)

        # Assert
        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_SQS_1,
                'queue_url': 'https://test.amazonaws.com/test/none'}
            ),
            call({
                'log_reference': LogReference.METRIC_SQS_3,
                'queue_name': 'none',
                'metric_name': 'NumberOfMessagesReceived',
                'statistic': 'Sum'
            }),
            call({
                'log_reference': LogReference.METRIC_SQS_3,
                'queue_name': 'none',
                'metric_name': 'ApproximateNumberOfMessagesVisible',
                'statistic': 'Sum'
            })
        ]

        expected_metric_calls = [
            call(
                start_time,
                end_time,
                'AWS/SQS',
                'ApproximateNumberOfMessagesVisible',
                [{'Name': 'QueueName', 'Value': 'none'}],
                ['Sum']
            ),
            call(
                start_time,
                end_time,
                'AWS/SQS',
                'NumberOfMessagesReceived',
                [{'Name': 'QueueName', 'Value': 'none'}],
                ['Sum']
            )
        ]

        get_queues_list_mock.assert_called_once()
        query_cloudwatch_metric_mock.assert_has_calls(expected_metric_calls, any_order=True)
        log_mock.assert_has_calls(expected_log_calls)

    def test_get_metric_do_not_break_if_no_datapoints(self, query_cloudwatch_metric_mock):
        query_cloudwatch_metric_mock.return_value = {'Datapoints': []}
        try:
            self.sqs_metrics_module.get_metric('', '', '', '')
        except Exception:
            self.fail("get_metric() raised an exception")

    @patch('log_cloudwatch_metrics.sqs_metrics.get_sqs_client')
    @patch('log_cloudwatch_metrics.sqs_metrics.filter_resource')
    def test_get_queues_list(self, filter_resource_mock, sqs_mock, _):
        # Arrange
        list_queues_mock = Mock()
        list_queues_mock.list_queues.side_effect = [
            {
                'QueueUrls': [
                    'test-queue-1',
                    'test-queue-2',
                    'test-queue-3',
                    'another-env-queue-4',
                ]
            }
        ]
        sqs_mock.return_value = list_queues_mock
        filter_resource_mock.side_effect = [True, True, True, False]
        # Act
        queues = self.sqs_metrics_module.get_queues_list()
        # Assert
        self.assertEqual(queues, ['test-queue-1', 'test-queue-2', 'test-queue-3'])
        list_queues_mock.list_queues.assert_has_calls([call(QueueNamePrefix='test', MaxResults=1000)])
        filter_resource_mock.assert_has_calls([
            call('test', 'test-queue-1'),
            call('test', 'test-queue-2'),
            call('test', 'test-queue-3'),
            call('test', 'another-env-queue-4')
        ])


def metric_response_side_effect(*args, **kwargs):
    datapoints = [] if args[4][0]['Value'] == 'none' else [
        {
            'Timestamp': datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc),
            'Sum': 5.0, 'Unit': 'Count'
        }
    ]
    return {
        "Metrics": [
            {
                "Namespace": "AWS/SQS",
                "Dimensions": [
                    {
                        "Name": "QueueName",
                        "Value": "test"
                    }
                ],
                "MetricName": "NumberOfMessagesReceived"
            }
        ],
        'Datapoints': datapoints,
        "NextToken": "xxx"
    }
