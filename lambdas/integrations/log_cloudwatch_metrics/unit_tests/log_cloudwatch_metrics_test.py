import concurrent
from unittest import TestCase
from mock import patch, call
from log_cloudwatch_metrics.log_cloudwatch_metrics import forward_metrics
from log_cloudwatch_metrics.log_references import LogReference


class TestLogCloudwatchMetrics(TestCase):
    def setUp(self):
        # Setup logger
        self.log_patcher = patch('common.log.log')

        # Reset and start all used mocks
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.log')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_sqs_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_s3_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_waf_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_sns_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_cloudwatch_events_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_dynamodb_metrics')
    def gather_metrics_test(
        self, dynamodb_metrics_mock, cloudwatch_events_metrics_mock,
        sns_metrics_mock, waf_metrics_mock, s3_mock, sqs_metrics_mock, log_mock
    ):
        # Arrange
        sns_metrics_mock.return_value = concurrent.futures.Future()
        cloudwatch_events_metrics_mock.return_value = concurrent.futures.Future()
        waf_metrics_mock.return_value = concurrent.futures.Future()
        s3_mock.return_value = concurrent.futures.Future()
        sqs_metrics_mock.return_value = concurrent.futures.Future()
        dynamodb_metrics_mock.return_value = concurrent.futures.Future()
        # Act
        forward_metrics()
        # Assert
        sqs_metrics_mock.assert_called_once()
        waf_metrics_mock.assert_called_once()
        sns_metrics_mock.assert_called_once()
        cloudwatch_events_metrics_mock.assert_called_once()
        dynamodb_metrics_mock.assert_called_once()

    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.log')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_sqs_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_s3_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_lambda_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_waf_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_sns_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_cloudwatch_events_metrics')
    @patch('log_cloudwatch_metrics.log_cloudwatch_metrics.get_dynamodb_metrics')
    def gather_metrics_errors_test(
        self, dynamodb_metrics_mock, cloudwatch_events_metrics_mock, sns_metrics_mock,
        waf_metrics_mock, lambda_metrics_mock, s3_mock, sqs_metrics_mock, log_mock
    ):
        # Arrange
        sns_metrics_mock.return_value = concurrent.futures.Future()
        cloudwatch_events_metrics_mock.return_value = concurrent.futures.Future()
        s3_mock.return_value = concurrent.futures.Future()
        sqs_metrics_mock.return_value = concurrent.futures.Future()
        dynamodb_metrics_mock.return_value = concurrent.futures.Future()
        lambda_metrics_mock.return_value = concurrent.futures.Future()

        def mock_async_error(start_time, end_time):
            raise ValueError('Something went wrong!')
        waf_metrics_mock.side_effect = mock_async_error
        # Act
        forward_metrics()
        # Assert
        sqs_metrics_mock.assert_called_once()
        waf_metrics_mock.assert_called_once()
        cloudwatch_events_metrics_mock.assert_called_once()
        dynamodb_metrics_mock.assert_called_once()
        lambda_metrics_mock.assert_called_once()
        expected_log_calls = [
            call({
                'log_reference': LogReference.METRICS_2,
                'exceptions': str([ValueError('Something went wrong!')])
            })
        ]
        log_mock.assert_called_once()
        log_mock.assert_has_calls(expected_log_calls)
