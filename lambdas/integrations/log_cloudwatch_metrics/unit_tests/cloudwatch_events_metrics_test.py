from unittest import TestCase
from mock import Mock, patch, MagicMock, call
from datetime import datetime, timezone
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics import cloudwatch_events_metrics


boto3_client = MagicMock()


@patch('common.log.logger', MagicMock())
@patch('boto3.client', boto3_client)
@patch('log_cloudwatch_metrics.cloudwatch_events_metrics.log')
@patch('log_cloudwatch_metrics.cloudwatch_events_metrics.query_cloudwatch_metric')
@patch('os.environ', {'ENVIRONMENT_NAME': 'test'})
class TestCloudWatchEventsMetrics(TestCase):
    def setUp(self):
        # Setup logger
        self.log_patcher = patch('common.log.log')

        # Setup cloudwatch_mock and cloudwatch_events_client mock
        cloudwatch_events_client_mock = Mock()
        cloudwatch_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: \
            cloudwatch_events_client_mock if args and args[0] == 'events' else \
            cloudwatch_mock if args and args[0] == 'cloudwatch' else Mock()

        # Reset and start all used mocks
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('log_cloudwatch_metrics.cloudwatch_events_metrics.get_rules')
    def test_cloudwatch_events_get_metric_logs_when_metrics_exist(self,
                                                                  get_rules_mock,
                                                                  query_cloudwatch_metric_mock,
                                                                  log_mock):
        # Arrange
        timestamp = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_rules_mock.return_value = ['test']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        cloudwatch_events_metrics.get_cloudwatch_events_metrics(start_time, end_time)

        # Assert
        get_rules_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_CLOUDEVENTS_1,
                'rule_name': 'test'}
            ),
            call({
                'log_reference': LogReference.METRIC_CLOUDEVENTS_2,
                'timestamp': timestamp.isoformat(),
                'rule_name': 'test',
                'FailedInvocations': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_CLOUDEVENTS_2,
                'timestamp': timestamp.isoformat(),
                'rule_name': 'test',
                'Invocations': 5.0,
                'statistic': 'Sum'}
            )
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

        expected_metric_calls = [
            call(
                start_time,
                end_time,
                'AWS/Events',
                'FailedInvocations',
                [{'Name': 'RuleName', 'Value': 'test'}],
                ['Sum'],
            ),
            call(
                start_time,
                end_time,
                'AWS/Events',
                'Invocations',
                [{'Name': 'RuleName', 'Value': 'test'}],
                ['Sum'],
            ),
        ]
        query_cloudwatch_metric_mock.assert_has_calls(expected_metric_calls, any_order=True)

    @patch('log_cloudwatch_metrics.cloudwatch_events_metrics.get_rules')
    def test_lambda_should_not_log_data_if_no_values_returned(self,
                                                              get_rules_mock,
                                                              query_cloudwatch_metric_mock,
                                                              log_mock):
        # Arrange
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_rules_mock.return_value = ['none']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        cloudwatch_events_metrics.get_cloudwatch_events_metrics(start_time, end_time)

        # Assert
        get_rules_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_CLOUDEVENTS_1,
                'rule_name': 'none'}
            ),
            call({
                'log_reference': LogReference.METRIC_CLOUDEVENTS_3,
                'rule_name': 'none',
                'metric_name': 'FailedInvocations',
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_CLOUDEVENTS_3,
                'rule_name': 'none',
                'metric_name': 'Invocations',
                'statistic': 'Sum'}
            )
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

    def test_get_metrics_does_not_break_if_no_datapoints(self, query_cloudwatch_metric_mock, _):
        query_cloudwatch_metric_mock.return_value = {'Datapoints': []}
        try:
            cloudwatch_events_metrics.get_metric('', '', '', '')
        except Exception:
            self.fail("get_metric() raised an exception")

    @patch('log_cloudwatch_metrics.cloudwatch_events_metrics.get_cloudwatch_events_client')
    @patch('log_cloudwatch_metrics.cloudwatch_events_metrics.filter_resource')
    def test_get_rules(self, filter_resource_mock, cloudwatch_events_mock, _, __):
        # Arrange
        list_rules_mock = Mock()
        list_rules_mock.list_rules.side_effect = [
            {
                'Rules': [
                    {
                        'Name': 'test-env-TestEvent1'
                    },
                    {
                        'Name': 'test-env-TestEvent2'
                    }
                ],
                'NextToken': 'my_next_token'
            },
            {
                'Rules': [
                    {
                        'Name': 'test-env-TestEvent3'
                    },
                    {
                        'Name': 'another-env-TestEvent4'
                    }
                ],
            }
        ]

        cloudwatch_events_mock.return_value = list_rules_mock
        filter_resource_mock.side_effect = [True, True, True, False]
        # Act
        rules = cloudwatch_events_metrics.get_rules()
        # Assert
        self.assertEqual(rules, ['test-env-TestEvent1', 'test-env-TestEvent2', 'test-env-TestEvent3'])
        list_rules_mock.list_rules.assert_has_calls([call(), call(NextToken='my_next_token')])
        filter_resource_mock.assert_has_calls([
            call('test', 'test-env-TestEvent1'),
            call('test', 'test-env-TestEvent2'),
            call('test', 'test-env-TestEvent3'),
            call('test', 'another-env-TestEvent4')
        ])


def metric_response_side_effect(*args, **kwargs):
    datapoints = [] if args[4][0]['Value'] == 'none' else [
        {
            'Timestamp': datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc),
            'Sum': 5.0, 'Unit': 'Count'
        }
    ]

    return {
        "Metrics": [
            {
                "Namespace": "AWS/Events",
                "Dimensions": [
                    {
                        "Name": "RuleName",
                        "Value": "test"
                    }
                ],
                "MetricName": "FailedInvocations"
            }
        ],
        'Datapoints': datapoints,
        "NextToken": None
    }
