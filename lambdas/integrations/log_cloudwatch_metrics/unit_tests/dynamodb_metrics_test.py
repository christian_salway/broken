from unittest import TestCase
from mock import Mock, patch, MagicMock, call
from datetime import datetime, timezone
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics import dynamodb_metrics


boto3_client = MagicMock()


@patch('common.log.logger', MagicMock())
@patch('boto3.client', boto3_client)
@patch('log_cloudwatch_metrics.dynamodb_metrics.log')
@patch('log_cloudwatch_metrics.dynamodb_metrics.query_cloudwatch_metric')
@patch('os.environ', {'ENVIRONMENT_NAME': 'test'})
class TestDynamoDBMetrics(TestCase):
    def setUp(self):
        # Setup logger
        self.log_patcher = patch('common.log.log')

        # Setup cloudwatch_mock and dynamodb_client mock
        cloudwatch_mock = Mock()
        boto3_client.side_effect = lambda *args, **kwargs: \
            cloudwatch_mock if args and args[0] == 'cloudwatch' else Mock()

        # Reset and start all used mocks
        self.log_patcher.start()

    def tearDown(self):
        self.log_patcher.stop()

    @patch('log_cloudwatch_metrics.dynamodb_metrics.get_tables')
    @patch('log_cloudwatch_metrics.dynamodb_metrics.get_gsi_names_for_table')
    def test_dynamodb_get_metric_logs_when_metrics_exist(
            self, get_gsi_mock, get_tables_mock, query_cloudwatch_metric_mock, log_mock):
        # Arrange
        timestamp = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_gsi_mock.return_value = ['test_gsi']
        get_tables_mock.return_value = ['test_table']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        dynamodb_metrics.get_dynamodb_metrics(start_time, end_time)

        # Assert
        get_tables_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_1,
                'table_name': 'test_table'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_4,
                'table_name': 'test_table',
                'gsi_name': 'test_gsi'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_2,
                'timestamp': timestamp.isoformat(),
                'table_name': 'test_table',
                'SystemErrors': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_2,
                'timestamp': timestamp.isoformat(),
                'table_name': 'test_table',
                'ThrottledRequests': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_2,
                'timestamp': timestamp.isoformat(),
                'table_name': 'test_table',
                'ReadThrottleEvents': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_2,
                'timestamp': timestamp.isoformat(),
                'table_name': 'test_table',
                'WriteThrottleEvents': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_5,
                'timestamp': timestamp.isoformat(),
                'gsi_name': 'test_gsi',
                'table_name': 'test_table',
                'ReadThrottleEvents': 5.0,
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_5,
                'timestamp': timestamp.isoformat(),
                'table_name': 'test_table',
                'gsi_name': 'test_gsi',
                'WriteThrottleEvents': 5.0,
                'statistic': 'Sum'}
            )
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

        expected_metric_calls = [
            call(
                start_time,
                end_time,
                'AWS/DynamoDB',
                'SystemErrors',
                [{'Name': 'TableName', 'Value': 'test_table'}],
                ['Sum']
            ),
            call(
                start_time,
                end_time,
                'AWS/DynamoDB',
                'ReadThrottleEvents',
                [{'Name': 'TableName', 'Value': 'test_table'}],
                ['Sum']
            ),
            call(
                start_time,
                end_time,
                'AWS/DynamoDB',
                'WriteThrottleEvents',
                [{'Name': 'TableName', 'Value': 'test_table'}],
                ['Sum']
            ),
            call(
                start_time,
                end_time,
                'AWS/DynamoDB',
                'ThrottledRequests',
                [{'Name': 'TableName', 'Value': 'test_table'}],
                ['Sum']
            ),
            call(
                start_time,
                end_time,
                'AWS/DynamoDB',
                'ReadThrottleEvents',
                [{'Name': 'TableName', 'Value': 'test_table'},
                 {'Name': 'GlobalSecondaryIndexName', 'Value': 'test_gsi'}],
                ['Sum']
            ),
            call(
                start_time,
                end_time,
                'AWS/DynamoDB',
                'WriteThrottleEvents',
                [{'Name': 'TableName', 'Value': 'test_table'},
                 {'Name': 'GlobalSecondaryIndexName', 'Value': 'test_gsi'}],
                ['Sum']
            )
        ]
        query_cloudwatch_metric_mock.assert_has_calls(expected_metric_calls, any_order=True)

    @patch('log_cloudwatch_metrics.dynamodb_metrics.get_tables')
    @patch('log_cloudwatch_metrics.dynamodb_metrics.get_gsi_names_for_table')
    def test_dynamodb_get_metric_logs_raises_exception_on_error(
            self, get_gsi_mock, get_tables_mock, query_cloudwatch_metric_mock, log_mock):
        # Arrange
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_gsi_mock.return_value = ['test_gsi']
        get_tables_mock.return_value = ['test_table']
        query_cloudwatch_metric_mock.side_effect = Exception('Boom')

        # Act
        try:
            dynamodb_metrics.get_dynamodb_metrics(start_time, end_time)
        except Exception:

            # Assert
            get_tables_mock.assert_called_once()

            expected_log_calls = [
                call({
                    'log_reference': LogReference.METRIC_DYNAMODB_1,
                    'table_name': 'test_table'}
                ),
                call({
                    'log_reference': LogReference.METRIC_DYNAMODB_4,
                    'table_name': 'test_table',
                    'gsi_name': 'test_gsi'}
                )
            ]
            log_mock.assert_has_calls(expected_log_calls, any_order=True)

            expected_metric_calls = [
                call(
                    start_time,
                    end_time,
                    'AWS/DynamoDB',
                    'SystemErrors',
                    [{'Name': 'TableName', 'Value': 'test_table'}],
                    ['Sum']
                ),
                call(
                    start_time,
                    end_time,
                    'AWS/DynamoDB',
                    'ReadThrottleEvents',
                    [{'Name': 'TableName', 'Value': 'test_table'}],
                    ['Sum']
                ),
                call(
                    start_time,
                    end_time,
                    'AWS/DynamoDB',
                    'WriteThrottleEvents',
                    [{'Name': 'TableName', 'Value': 'test_table'}],
                    ['Sum']
                ),
                call(
                    start_time,
                    end_time,
                    'AWS/DynamoDB',
                    'ThrottledRequests',
                    [{'Name': 'TableName', 'Value': 'test_table'}],
                    ['Sum']
                ),
                call(
                    start_time,
                    end_time,
                    'AWS/DynamoDB',
                    'ReadThrottleEvents',
                    [{'Name': 'TableName', 'Value': 'test_table'},
                     {'Name': 'GlobalSecondaryIndexName', 'Value': 'test_gsi'}],
                    ['Sum']
                ),
                call(
                    start_time,
                    end_time,
                    'AWS/DynamoDB',
                    'WriteThrottleEvents',
                    [{'Name': 'TableName', 'Value': 'test_table'},
                     {'Name': 'GlobalSecondaryIndexName', 'Value': 'test_gsi'}],
                    ['Sum']
                )
            ]
            query_cloudwatch_metric_mock.assert_has_calls(expected_metric_calls, any_order=True)
            self.assertRaises(Exception)

    @patch('log_cloudwatch_metrics.dynamodb_metrics.get_tables')
    @patch('log_cloudwatch_metrics.dynamodb_metrics.get_gsi_names_for_table')
    def test_lambda_should_not_log_data_if_no_values_returned(self,
                                                              get_gsi_mock,
                                                              get_tables_mock,
                                                              query_cloudwatch_metric_mock,
                                                              log_mock):
        # Arrange
        start_time = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        end_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        get_gsi_mock.return_value = []
        get_tables_mock.return_value = ['none']
        query_cloudwatch_metric_mock.side_effect = metric_response_side_effect

        # Act
        dynamodb_metrics.get_dynamodb_metrics(start_time, end_time)

        # Assert
        get_tables_mock.assert_called_once()

        expected_log_calls = [
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_1,
                'table_name': 'none'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_3,
                'table_name': 'none',
                'metric_name': 'SystemErrors',
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_3,
                'table_name': 'none',
                'metric_name': 'ReadThrottleEvents',
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_3,
                'table_name': 'none',
                'metric_name': 'WriteThrottleEvents',
                'statistic': 'Sum'}
            ),
            call({
                'log_reference': LogReference.METRIC_DYNAMODB_3,
                'table_name': 'none',
                'metric_name': 'ThrottledRequests',
                'statistic': 'Sum'}
            )
        ]
        log_mock.assert_has_calls(expected_log_calls, any_order=True)

    def test_get_metrics_for_table(self, query_cloudwatch_metric_mock, log_mock):
        timestamp = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        query_cloudwatch_metric_mock.return_value = {'Datapoints': [
            {
                'Timestamp': timestamp,
                'Sum': 5.0, 'Unit': 'Sum'
            }
        ]}
        dynamodb_metrics.get_metric_for_table('test-table', 'start-time', 'end-time', 'test-metric')

        query_cloudwatch_metric_mock.assert_called_with('start-time', 'end-time', 'AWS/DynamoDB', 'test-metric',
                                                        [{'Name': 'TableName', 'Value': 'test-table'}], ['Sum'])
        log_mock.assert_called_with({
            'log_reference': LogReference.METRIC_DYNAMODB_2,
            'timestamp': timestamp.isoformat(),
            'table_name': 'test-table',
            'test-metric': 5.0,
            'statistic': 'Sum'}
        )

    def test_get_metrics_for_table_does_not_break_if_no_datapoints(self, query_cloudwatch_metric_mock, log_mock):
        query_cloudwatch_metric_mock.return_value = {'Datapoints': []}
        try:
            dynamodb_metrics.get_metric_for_table('test-table', 'start-time', 'end-time', 'test-metric')
            query_cloudwatch_metric_mock.assert_called_with('start-time', 'end-time', 'AWS/DynamoDB', 'test-metric',
                                                            [{'Name': 'TableName', 'Value': 'test-table'}], ['Sum'])
            log_mock.assert_called_with({
                'log_reference': LogReference.METRIC_DYNAMODB_3,
                'table_name': 'test-table',
                'metric_name': 'test-metric',
                'statistic': 'Sum'}
            )
        except Exception:
            self.fail("get_metric_for_table() raised an exception")

    def test_get_metrics_for_gsi(self, query_cloudwatch_metric_mock, log_mock):
        timestamp = datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc)
        query_cloudwatch_metric_mock.return_value = {'Datapoints': [
            {
                'Timestamp': timestamp,
                'Sum': 5.0, 'Unit': 'Sum'
            }
        ]}
        dynamodb_metrics.get_metric_for_gsi('test-table', 'test-gsi', 'start-time', 'end-time', 'test-metric')

        query_cloudwatch_metric_mock.assert_called_with('start-time', 'end-time', 'AWS/DynamoDB', 'test-metric',
                                                        [{'Name': 'TableName', 'Value': 'test-table'},
                                                         {'Name': 'GlobalSecondaryIndexName', 'Value': 'test-gsi'}],
                                                        ['Sum'])

        log_mock.assert_called_with({
            'log_reference': LogReference.METRIC_DYNAMODB_5,
            'timestamp': timestamp.isoformat(),
            'table_name': 'test-table',
            'gsi_name': 'test-gsi',
            'test-metric': 5.0,
            'statistic': 'Sum'}
        )

    def test_get_metrics_for_gsi_does_not_break_if_no_datapoints(self, query_cloudwatch_metric_mock, log_mock):
        query_cloudwatch_metric_mock.return_value = {'Datapoints': []}
        try:
            dynamodb_metrics.get_metric_for_gsi('test-table', 'test-gsi', 'start-time', 'end-time', 'test-metric')
            query_cloudwatch_metric_mock.assert_called_with('start-time', 'end-time', 'AWS/DynamoDB', 'test-metric',
                                                            [{'Name': 'TableName', 'Value': 'test-table'},
                                                             {'Name': 'GlobalSecondaryIndexName', 'Value': 'test-gsi'}],
                                                            ['Sum'])
            log_mock.assert_called_with({
                'log_reference': LogReference.METRIC_DYNAMODB_6,
                'table_name': 'test-table',
                'gsi_name': 'test-gsi',
                'metric_name': 'test-metric',
                'statistic': 'Sum'}
            )
        except Exception:
            self.fail("get_metric_for_gsi() raised an exception")

    @patch('log_cloudwatch_metrics.dynamodb_metrics.list_dynamodb_tables')
    @patch('log_cloudwatch_metrics.dynamodb_metrics.filter_resource')
    def test_get_tables(self, filter_resource_mock, list_tables_mock, _, __):
        # Arrange
        list_tables_mock.return_value = [
            'test-table-1',
            'test-table-2',
            'test-table-3',
            'another-env-test-table-4'
        ]

        filter_resource_mock.side_effect = [True, True, True, False]
        # Act
        tables = dynamodb_metrics.get_tables()
        # Assert
        self.assertEqual(tables, ['test-table-1', 'test-table-2', 'test-table-3'])
        list_tables_mock.assert_called_once()
        filter_resource_mock.assert_has_calls([
            call('test', 'test-table-1'),
            call('test', 'test-table-2'),
            call('test', 'test-table-3'),
            call('test', 'another-env-test-table-4')
        ])

    @patch('log_cloudwatch_metrics.dynamodb_metrics.describe_dynamodb_table')
    def test_get_gsi_names_for_table(self, describe_table_mock, query_cloudwatch_metric_mock, log_mock):
        # Arrange
        describe_table_mock.return_value = {
            'TableName': 'test-table',
            'GlobalSecondaryIndexes': [
                {
                    'IndexName': 'test-gsi-1',
                },
                {
                    'IndexName': 'test-gsi-2',
                }
            ]
        }

        # Act
        result = dynamodb_metrics.get_gsi_names_for_table('test-table')
        # Assert
        self.assertEqual(result, ['test-gsi-1', 'test-gsi-2'])
        describe_table_mock.assert_called_once_with('test-table')

    @patch('log_cloudwatch_metrics.dynamodb_metrics.describe_dynamodb_table')
    def test_get_gsi_names_for_table_no_gsis(self, describe_table_mock, query_cloudwatch_metric_mock, log_mock):
        # Arrange

        describe_table_mock.return_value = {
            'TableName': 'test-table',
        }

        # Act
        result = dynamodb_metrics.get_gsi_names_for_table('test-table')
        # Assert
        self.assertEqual(result, [])
        describe_table_mock.assert_called_once_with('test-table')

    @patch('log_cloudwatch_metrics.dynamodb_metrics.get_metric_for_gsi')
    @patch('log_cloudwatch_metrics.dynamodb_metrics.get_gsi_names_for_table')
    def test_get_all_metrics_for_gsis(self, get_gsi_mock, get_metric_for_gsi_mock, _, log_mock):
        # Arrange
        get_gsi_mock.return_value = ['test-gsi']

        # Act
        dynamodb_metrics.get_all_metrics_for_gsis('test-table', 'start-time', 'end-time')
        # Assert
        get_gsi_mock.assert_called_once_with('test-table')
        get_metric_for_gsi_mock.assert_has_calls([
            call('test-table', 'test-gsi', 'start-time', 'end-time', 'ReadThrottleEvents'),
            call('test-table', 'test-gsi', 'start-time', 'end-time', 'WriteThrottleEvents'),
        ], any_order=True)

    @patch('log_cloudwatch_metrics.dynamodb_metrics.get_metric_for_gsi')
    @patch('log_cloudwatch_metrics.dynamodb_metrics.get_gsi_names_for_table')
    def test_get_all_metrics_for_gsis_raises_exception_on_error(
            self, get_gsi_mock, get_metric_for_gsi_mock, _, log_mock):
        # Arrange
        get_gsi_mock.return_value = ['test-gsi']
        get_metric_for_gsi_mock.side_effect = Exception('Boom')
        # Act
        try:
            dynamodb_metrics.get_all_metrics_for_gsis('test-table', 'start-time', 'end-time')
        except Exception:
            # Assert
            get_gsi_mock.assert_called_once_with('test-table')
            get_metric_for_gsi_mock.assert_called_with('test-table',
                                                       'test-gsi',
                                                       'start-time',
                                                       'end-time',
                                                       'WriteThrottleEvents')
            self.assertRaises(Exception)


def metric_response_side_effect(*args, **kwargs):
    datapoints = [] if args[4][0]['Value'] == 'none' else [
        {
            'Timestamp': datetime(2019, 12, 31, 23, 50, tzinfo=timezone.utc),
            'Sum': 5.0, 'Unit': 'Sum'
        }
    ]

    return {
        "Metrics": [
            {
                "Namespace": "AWS/Events",
                "Dimensions": [
                    {
                        "Name": "TableName",
                        "Value": "test"
                    }
                ],
                "MetricName": "test"
            }
        ],
        'Datapoints': datapoints,
        "NextToken": None
    }
