import concurrent
from functools import partial
import os
from common.log import log
from log_cloudwatch_metrics.concurrent_executor import get_executor
from log_cloudwatch_metrics.filter_resources import filter_resource
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics.exceptions import AsyncExceptions
from log_cloudwatch_metrics.cloudwatch_client import query_cloudwatch_metric, get_metric_from_response
from common.utils.dynamodb_access.operations import describe_dynamodb_table, list_dynamodb_tables


def get_environment():
    return os.environ.get('ENVIRONMENT_NAME')


def get_dynamodb_metrics(start_time, end_time):
    # Can add more metrics in here if needed
    table_metrics = [
        'SystemErrors',
        'ReadThrottleEvents',
        'WriteThrottleEvents',
        'ThrottledRequests',
    ]
    table_list = get_tables()
    tasks = []
    for table in table_list:
        log({'log_reference': LogReference.METRIC_DYNAMODB_1, 'table_name': table})
        tasks.append(
            get_executor().submit(partial(get_all_metrics_for_gsis, table, start_time, end_time))
        )
        tasks.extend([
            get_executor().submit(partial(get_metric_for_table, table, start_time, end_time, metric))
            for metric in table_metrics
        ])
    results, _pending = concurrent.futures.wait(tasks)
    exceptions = [result.exception() for result in results if isinstance(result.exception(), Exception)]
    if exceptions:
        raise AsyncExceptions('Errors getting DynamoDB Table metrics', exceptions)


def get_all_metrics_for_gsis(table, start_time, end_time):
    # Can add more metrics in here if needed
    gsi_metrics = [
        'ReadThrottleEvents',
        'WriteThrottleEvents'
    ]
    tasks = []
    gsi_names = get_gsi_names_for_table(table)
    for gsi in gsi_names:
        log({'log_reference': LogReference.METRIC_DYNAMODB_4, 'table_name': table, 'gsi_name': gsi})
        tasks.extend([
            get_executor().submit(partial(get_metric_for_gsi, table, gsi, start_time, end_time, metric))
            for metric in gsi_metrics
        ])
    results, _pending = concurrent.futures.wait(tasks)
    exceptions = [result.exception() for result in results if isinstance(result.exception(), Exception)]
    if exceptions:
        raise AsyncExceptions('Errors getting DynamoDB GSI metrics', exceptions)


def get_metric_for_table(table_name, start_time, end_time, metric_name):
    dimensions = [{'Name': 'TableName', 'Value': table_name}]
    response = query_cloudwatch_metric(start_time, end_time, 'AWS/DynamoDB', metric_name, dimensions, ['Sum'])
    metrics = get_metric_from_response(response)
    if metrics:
        log({'log_reference': LogReference.METRIC_DYNAMODB_2,
             'timestamp': metrics['Timestamp'].isoformat(),
             'table_name': table_name,
             metric_name: metrics['Sum'],
             'statistic': 'Sum'})
    else:
        log({'log_reference': LogReference.METRIC_DYNAMODB_3,
             'table_name': table_name,
             'metric_name': metric_name,
             'statistic': 'Sum'})


def get_metric_for_gsi(table_name, gsi_name, start_time, end_time, metric_name):
    dimensions = [{'Name': 'TableName', 'Value': table_name},
                  {'Name': 'GlobalSecondaryIndexName', 'Value': gsi_name}]
    response = query_cloudwatch_metric(start_time, end_time, 'AWS/DynamoDB', metric_name, dimensions, ['Sum'])
    metrics = get_metric_from_response(response)
    if metrics:
        log({'log_reference': LogReference.METRIC_DYNAMODB_5,
             'timestamp': metrics['Timestamp'].isoformat(),
             'table_name': table_name,
             'gsi_name': gsi_name,
             metric_name: metrics['Sum'],
             'statistic': 'Sum'})
    else:
        log({'log_reference': LogReference.METRIC_DYNAMODB_6,
             'table_name': table_name,
             'gsi_name': gsi_name,
             'metric_name': metric_name,
             'statistic': 'Sum'})


def get_tables():
    table_names = list_dynamodb_tables()
    return [table for table in table_names if filter_resource(get_environment(), table)]


def get_gsi_names_for_table(table_name):
    table_data = describe_dynamodb_table(table_name)
    gsi_objects = table_data.get('GlobalSecondaryIndexes', [])
    return [gsi['IndexName'] for gsi in gsi_objects]
