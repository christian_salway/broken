import traceback


class AsyncExceptions(Exception):
    def __init__(self, message, errors):
        super().__init__()
        self.errors = errors
        self.message = message

    def __str__(self):
        return f'{self.message}: {[str(e) for e in self.errors]}'

    def __repr__(self):
        stack_traces = ["".join(traceback.TracebackException.from_exception(err).format()) for err in self.errors]
        return f'AsyncExceptions({self.message}: {stack_traces})'
