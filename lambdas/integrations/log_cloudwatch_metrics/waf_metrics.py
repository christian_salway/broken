import os
import concurrent
from common.log import log
from functools import partial
from log_cloudwatch_metrics.concurrent_executor import get_executor
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics.exceptions import AsyncExceptions
from log_cloudwatch_metrics.cloudwatch_client import query_cloudwatch_metric, get_metric_from_response


def get_environment():
    return os.environ.get('ENVIRONMENT_NAME')
# Rather than accessing this global directly use the get_sqs_client() function which aids in testability


def get_waf_metrics(start_time, end_time):
    metrics = [
        ('BlockedRequests', "ALL"),
        ('AllowedRequests', "ALL"),
        ('CountedRequests', "ALL"),
        ('PassedRequests', "ALL"),
        ('BlockedRequests', f"{get_environment()}-allow-uk-traffic-metric"),
        ('BlockedRequests', f"{get_environment()}-ip-address-hard-limit-metric"),
        ('CountedRequests', f"{get_environment()}-ip-address-soft-limit-metric"),
    ]
    log({'log_reference': LogReference.METRIC_WAF_1})
    results, _incomplete = concurrent.futures.wait(
        [get_executor().submit(partial(get_metric, metric[0], start_time, end_time, metric[1]))
         for metric in metrics]
    )
    exceptions = [result.exception() for result in results if isinstance(result.exception(), Exception)]
    if exceptions:
        raise AsyncExceptions('Errors getting WAF metrics', exceptions)


def get_metric(metric_name, start_time, end_time, rule):
    dimensions = [
        {'Name': 'WebACL', 'Value': f'{get_environment()}-waf-v2'},
        {'Name': 'Region', 'Value': 'eu-west-2'},
        {'Name': 'Rule', 'Value': rule}
    ]
    response = query_cloudwatch_metric(start_time, end_time, 'AWS/WAFV2', metric_name, dimensions, ['Sum'])
    metric = get_metric_from_response(response)
    if metric:
        log({'log_reference': LogReference.METRIC_WAF_2,
             'timestamp': metric['Timestamp'].isoformat(),
             'rule': rule,
             metric_name: metric['Sum'],
            'statistic': 'Sum', })
    else:
        log({'log_reference': LogReference.METRIC_WAF_3,
             'rule': rule,
             'metric_name': metric_name,
            'statistic': 'Sum', })
