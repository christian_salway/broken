environment_suffixes = ['stubbed', 'smartcard', 'ci-analytics']


def filter_resource(environment_name, resource_name):
    """
    This will return True if the resource should be included and False if not
    It is to try and remove things like MR resources showing up in feature branches for example.
    """
    excluded_environment_names = tuple(f'{environment_name}-{suffix}' for suffix in environment_suffixes)
    return (resource_name.startswith(environment_name) and
            not resource_name.startswith(excluded_environment_names))
