from boto3 import client
from botocore.client import Config

_CLOUDWATCH_CLIENT = None


def get_cloudwatch_client():
    global _CLOUDWATCH_CLIENT
    return _CLOUDWATCH_CLIENT


# We need to initialise the client separately as boto client creation is not threadsafe in some circumstances
def initialise_cloudwatch_client():
    global _CLOUDWATCH_CLIENT
    if not _CLOUDWATCH_CLIENT:
        _CLOUDWATCH_CLIENT = client('cloudwatch', config=Config(retries={'max_attempts': 5, 'mode': 'standard'}))


def query_cloudwatch_metric(start_time, end_time, namespace, metric, dimensions, statistics, extended=False):
    args = {
        "Period": 600,
        "StartTime": start_time,
        "EndTime": end_time,
        "MetricName": metric,
        "Namespace": namespace,
        "Dimensions": dimensions
    }

    if extended:
        args.update({'ExtendedStatistics': statistics})
    else:
        args.update({'Statistics': statistics})

    return get_cloudwatch_client().get_metric_statistics(**args)


def get_metric_from_response(metric_response):
    if metric_response.get('Datapoints'):
        return metric_response['Datapoints'][-1]
