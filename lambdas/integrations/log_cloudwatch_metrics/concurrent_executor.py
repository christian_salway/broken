import concurrent.futures

_EXECUTOR = None


def get_executor():
    global _EXECUTOR
    if not _EXECUTOR:
        _EXECUTOR = concurrent.futures.ThreadPoolExecutor(
            max_workers=150,
        )
    return _EXECUTOR
