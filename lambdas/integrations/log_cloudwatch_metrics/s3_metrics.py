import concurrent
from functools import partial
import os
from boto3 import client
from botocore.client import Config
from common.log import log
from log_cloudwatch_metrics.filter_resources import filter_resource
from log_cloudwatch_metrics.concurrent_executor import get_executor
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics.exceptions import AsyncExceptions
from log_cloudwatch_metrics.cloudwatch_client import query_cloudwatch_metric, get_metric_from_response

# Rather than accessing this global directly use the get_s3_client() function which aids in testability
_S3_CLIENT = None


def get_environment():
    return os.environ.get('ENVIRONMENT_NAME')


def get_s3_client():
    global _S3_CLIENT
    if not _S3_CLIENT:
        _S3_CLIENT = client('s3', config=Config(retries={'max_attempts': 3}))
    return _S3_CLIENT


def get_s3_metrics(start_time, end_time):
    metrics = [
        ('4xxErrors', 'Sum', False),
        ('5xxErrors', 'Sum', False),
        ('TotalRequestLatency', 'p99', True),
    ]
    buckets_list = get_buckets()
    tasks = []
    for bucket_name in buckets_list:
        log({'log_reference': LogReference.METRIC_S3_1, 'bucket_name': bucket_name})
        tasks.extend([
            get_executor().submit(
                partial(get_metric, bucket_name, start_time, end_time, metric[0], metric[1], metric[2])
            ) for metric in metrics
        ])
    results, _pending = concurrent.futures.wait(tasks)
    exceptions = [result.exception() for result in results if isinstance(result.exception(), Exception)]
    if exceptions:
        raise AsyncExceptions('Errors getting S3 metrics', exceptions)


def get_metric(bucket_name, start_time, end_time, metric_name, statistic, extended):
    dimensions = [
        {"Name": "BucketName", "Value": bucket_name},
        {"Name": "FilterId", "Value": "EntireBucket"},
    ]
    response = query_cloudwatch_metric(start_time, end_time, "AWS/S3", metric_name,
                                       dimensions, [statistic], extended=extended)
    metrics = get_metric_from_response(response)
    if metrics:
        # Prepend with 'Metric' as splunk does not like field names that start with a number
        if extended:
            log({
                'log_reference': LogReference.METRIC_S3_2,
                'timestamp': metrics['Timestamp'].isoformat(),
                'bucket_name': bucket_name,
                f'Metric{metric_name}': metrics['ExtendedStatistics'][statistic],
                'statistic': statistic,
            })
        else:
            log({
                'log_reference': LogReference.METRIC_S3_2,
                'timestamp': metrics['Timestamp'].isoformat(),
                'bucket_name': bucket_name,
                f'Metric{metric_name}': metrics[statistic],
                'statistic': statistic,
            })
    else:
        log({
            'log_reference': LogReference.METRIC_S3_3,
            'bucket_name': bucket_name,
            'metric_name': metric_name,
            'statistic': statistic,
        })


def get_buckets():
    response = get_s3_client().list_buckets()
    filtered_buckets = [
        bucket['Name'] for bucket in response['Buckets']
        if filter_resource(get_environment(), bucket['Name'])
    ]
    return filtered_buckets
