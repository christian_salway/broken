import concurrent
from functools import partial
import os
from boto3 import client
from botocore.client import Config
from common.log import log
from log_cloudwatch_metrics.concurrent_executor import get_executor
from log_cloudwatch_metrics.filter_resources import filter_resource
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics.exceptions import AsyncExceptions
from log_cloudwatch_metrics.cloudwatch_client import query_cloudwatch_metric, get_metric_from_response

# Rather than accessing this global directly use the get_lambda_client() function which aids in testability
_LAMBDA_CLIENT = None


def get_environment():
    return os.environ.get('ENVIRONMENT_NAME')


def get_lambda_client():
    global _LAMBDA_CLIENT
    if not _LAMBDA_CLIENT:
        _LAMBDA_CLIENT = client('lambda', config=Config(retries={'max_attempts': 3}))
    return _LAMBDA_CLIENT


def get_lambda_metrics(start_time, end_time):
    metrics = [
        ('Duration', 'Maximum'),
        ('Errors', 'Sum'),
        ('Throttles', 'Sum'),
        ('Invocations', 'Sum'),
        ('DeadLetterErrors', 'Sum'),
    ]
    functions_list = get_lambdas()
    tasks = []
    for function_name in functions_list:
        log({'log_reference': LogReference.METRIC_LAMBDA_1, 'metric_function_name': function_name})
        tasks.extend([
            get_executor().submit(
                partial(get_metric, function_name, start_time, end_time, metric[0], metric[1])
            ) for metric in metrics
        ])
    results, _pending = concurrent.futures.wait(tasks)
    exceptions = [result.exception() for result in results if isinstance(result.exception(), Exception)]
    if exceptions:
        raise AsyncExceptions('Errors getting lambda metrics', exceptions)


def get_metric(function_name, start_time, end_time, metric_name, statistic):
    dimensions = [{"Name": "FunctionName", "Value": function_name}]
    response = query_cloudwatch_metric(start_time, end_time, "AWS/Lambda", metric_name, dimensions, [statistic])
    metrics = get_metric_from_response(response)
    if metrics:
        log({
            'log_reference': LogReference.METRIC_LAMBDA_2,
            'timestamp': metrics['Timestamp'].isoformat(),
            'metric_function_name': function_name,
            metric_name: metrics[statistic],
            'statistic': statistic,
        })
    else:
        log({
            'log_reference': LogReference.METRIC_LAMBDA_3,
            'metric_function_name': function_name,
            'metric_name': metric_name,
            'statistic': statistic,
        })


def get_lambdas():
    response = get_lambda_client().list_functions(MaxItems=50)
    function_objects = response.get('Functions', [])
    next_token = response.get('NextMarker')
    while next_token:
        response = get_lambda_client().list_functions(MaxItems=50, Marker=next_token)
        function_objects.extend(response.get('Functions', []))
        next_token = response.get('NextMarker')
    function_names = [function['FunctionName'] for function in function_objects]
    filtered_functions = [function for function in function_names if filter_resource(get_environment(), function)]
    return filtered_functions
