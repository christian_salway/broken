import concurrent
from functools import partial
import os
from boto3 import client
from botocore.client import Config
from common.log import log
from log_cloudwatch_metrics.concurrent_executor import get_executor
from log_cloudwatch_metrics.filter_resources import filter_resource
from log_cloudwatch_metrics.log_references import LogReference
from log_cloudwatch_metrics.exceptions import AsyncExceptions
from log_cloudwatch_metrics.cloudwatch_client import query_cloudwatch_metric, get_metric_from_response


# Rather than accessing this global directly use the get_sns_client() function which aids in testability
_SNS_CLIENT = None


def get_environment():
    return os.environ.get('ENVIRONMENT_NAME')


def get_sns_client():
    global _SNS_CLIENT
    if not _SNS_CLIENT:
        _SNS_CLIENT = client('sns', config=Config(retries={'max_attempts': 3}))
    return _SNS_CLIENT


def get_sns_metrics(start_time, end_time):
    metrics = [
        'NumberOfNotificationsDelivered',
        'NumberOfNotificationsFailed',
    ]
    topics_list = get_topics()
    tasks = []
    for topic_name in topics_list:
        log({'log_reference': LogReference.METRIC_SNS_1, 'topic_name': topic_name})
        tasks.extend([
            get_executor().submit(partial(get_metric, topic_name, start_time, end_time, metric))
            for metric in metrics
        ])
    results, _pending = concurrent.futures.wait(tasks)
    exceptions = [result.exception() for result in results if isinstance(result.exception(), Exception)]
    if exceptions:
        raise AsyncExceptions('Errors getting SNS metrics', exceptions)


def get_metric(topic_name, start_time, end_time, metric_name):
    dimensions = [{'Name': 'TopicName', 'Value': topic_name}]
    response = query_cloudwatch_metric(start_time, end_time, 'AWS/SNS', metric_name, dimensions, ['Sum'])
    metrics = get_metric_from_response(response)
    if metrics:
        log({
            'log_reference': LogReference.METRIC_SNS_2,
            'timestamp': metrics['Timestamp'].isoformat(),
            'topic_name': topic_name,
            metric_name: metrics['Sum'],
            'statistic': 'Sum',
        })
    else:
        log({
            'log_reference': LogReference.METRIC_SNS_3,
            'topic_name': topic_name,
            'metric_name': metric_name,
            'statistic': 'Sum',
        })


def get_topics():
    response = get_sns_client().list_topics()
    topic_objects = response.get('Topics', [])
    next_token = response.get('NextToken')
    while next_token:
        response = get_sns_client().list_topics(NextToken=next_token)
        topic_objects.extend(response.get('Topics', []))
        next_token = response.get('NextToken')
    topic_names = [topic['TopicArn'].split(':')[-1] for topic in topic_objects]
    filtered_topics = [topic for topic in topic_names if filter_resource(get_environment(), topic)]
    return filtered_topics
