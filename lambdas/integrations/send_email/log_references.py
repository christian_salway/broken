import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    SENDEMAIL0001 = (logging.ERROR, 'Could not retrieve NHSMail credentials from AWS Secrets Manager')
    SENDEMAIL0002 = (logging.INFO, 'Retrieved NHSMail credentials from AWS Secrets Manager')
    SENDEMAIL0003 = (logging.INFO, 'Connected to Exchange account and cached configuration')
    SENDEMAIL0004 = (logging.ERROR, 'Could not connect to Exchange account')
    SENDEMAIL0005 = (logging.ERROR, 'Could not send email')
    SENDEMAIL0006 = (logging.INFO, 'Successfully sent email')
    SENDEMAIL0007 = (logging.INFO, 'Using cached configuration object')
    SENDEMAIL0008 = (logging.INFO, 'No cached configuration, autodiscovering new data')
    SENDEMAIL0009 = (logging.INFO, 'Successfully closed session')
