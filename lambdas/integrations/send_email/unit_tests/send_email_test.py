from unittest.case import TestCase
import json
from mock import patch, Mock, call
from send_email.send_email import lambda_handler, get_account
from send_email.log_references import LogReference

module = 'send_email.send_email'


class TestSendEmail(TestCase):
    expected_return_value = {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': json.dumps('Hello from send_email!'),
        'isBase64Encoded': True
    }

    @patch(f'{module}.log')
    @patch(f'{module}.get_secret')
    @patch(f'{module}.get_account')
    @patch(f'{module}.Message')
    @patch(f'{module}.Credentials')
    @patch(f'{module}.Mailbox')
    @patch(f'{module}.HTMLBody')
    @patch(f'{module}.audit', Mock())
    @patch(f'{module}.NHSMAIL_CREDENTIALS', None)
    @patch(f'{module}.ACCOUNT', None)
    def test_send_happy_path(
        self, html_mock, mailbox_mock, credentials_mock, message_mock, account_mock, secret_mock, log_mock
    ):
        # Given
        event = {
            'Records': [{
                'body': json.dumps({
                    'email_addresses': ['x', 'y'],
                    'subject': 'spujb',
                    'body': 'message'
                })
            }]
        }
        secret_mock.return_value = json.dumps({
            'username': 'user',
            'password': 'password'
        })
        account = Mock()
        account_mock.return_value = account
        mailbox_mock.return_value = 'recipient'
        credentials_mock.return_value = 'credentials'
        message = Mock()
        message_mock.return_value = message
        message_body = Mock()
        html_mock.return_value = message_body

        # When
        return_value = lambda_handler.__wrapped__(event, {})

        # Then
        secret_mock.assert_called_with('nhsmail_credentials', secret_name_is_key=False)
        credentials_mock.assert_called_with('user', 'password')
        account_mock.assert_called_with('credentials')
        mailbox_mock.assert_has_calls([call(email_address='x'), call(email_address='y')])
        html_mock.assert_called_with('<html><body>message</body></html>')
        message_mock.assert_called_with(
            account=account,
            subject='spujb',
            body=message_body,
            to_recipients=['recipient', 'recipient']
        )
        message.send.assert_called_once()

        expected_logs = [
            call({'log_reference': LogReference.SENDEMAIL0002, 'username': 'user'}),
            call({'log_reference': LogReference.SENDEMAIL0003}),
            call({'log_reference': LogReference.SENDEMAIL0006, 'amount': 1})
        ]
        log_mock.assert_has_calls(expected_logs)

        self.assertEqual(return_value, self.expected_return_value)

    @patch(f'{module}.log')
    @patch(f'{module}.get_secret')
    @patch(f'{module}.get_account')
    @patch(f'{module}.Message')
    @patch(f'{module}.Credentials')
    @patch(f'{module}.Mailbox')
    @patch(f'{module}.HTMLBody')
    @patch(f'{module}.NHSMAIL_CREDENTIALS', None)
    def test_error_retrieving_credentials(
        self, html_mock, mailbox_mock, credentials_mock, message_mock, account_mock, secret_mock, log_mock
    ):
        # Given
        event = {
            'Records': [{
                'body': json.dumps({
                    'email_addresses': ['x', 'y'],
                    'subject': 'spujb',
                    'body': 'message'
                })
            }]
        }
        secret_mock.side_effect = Exception('oh no')
        account = Mock()
        account_mock.return_value = account
        mailbox_mock.return_value = 'recipient'
        credentials_mock.return_value = 'credentials'
        message = Mock()
        message_mock.return_value = message
        message_body = Mock()
        html_mock.return_value = message_body

        # When
        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(event, {})

        # Then
        secret_mock.assert_called_with('nhsmail_credentials', secret_name_is_key=False)
        credentials_mock.assert_not_called()
        account_mock.assert_not_called()
        mailbox_mock.assert_not_called()
        html_mock.assert_not_called()
        message_mock.assert_not_called()
        message.send.assert_not_called()

        expected_logs = [call({'log_reference': LogReference.SENDEMAIL0001, 'error': 'oh no'})]
        log_mock.assert_has_calls(expected_logs)
        self.assertEqual(context.exception.args[0], 'oh no')

    @patch(f'{module}.log')
    @patch(f'{module}.get_secret')
    @patch(f'{module}.get_account')
    @patch(f'{module}.Message')
    @patch(f'{module}.Credentials')
    @patch(f'{module}.Mailbox')
    @patch(f'{module}.HTMLBody')
    @patch(f'{module}.NHSMAIL_CREDENTIALS', None)
    def test_error_getting_account(
        self, html_mock, mailbox_mock, credentials_mock, message_mock, account_mock, secret_mock, log_mock
    ):
        # Given
        event = {
            'email_addresses': ['x', 'y'],
            'subject': 'spujb',
            'message': 'message'
        }
        secret_mock.return_value = json.dumps({
            'username': 'user',
            'password': 'password'
        })
        credentials_mock.return_value = 'credentials'
        account_mock.side_effect = Exception('gasp')

        # When
        with self.assertRaises(Exception) as context:
            lambda_handler.__wrapped__(event, {})

        # Then
        secret_mock.assert_called_with('nhsmail_credentials', secret_name_is_key=False)
        credentials_mock.assert_called_with('user', 'password')
        account_mock.assert_called_with('credentials')
        mailbox_mock.assert_not_called()
        html_mock.assert_not_called()
        message_mock.assert_not_called()

        expected_logs = [
            call({'log_reference': LogReference.SENDEMAIL0002, 'username': 'user'}),
            call({'log_reference': LogReference.SENDEMAIL0004, 'error': 'gasp'})
        ]
        log_mock.assert_has_calls(expected_logs)

        self.assertEqual(context.exception.args[0], 'gasp')

    @patch(f'{module}.log')
    @patch(f'{module}.get_secret')
    @patch(f'{module}.get_account')
    @patch(f'{module}.Message')
    @patch(f'{module}.Credentials')
    @patch(f'{module}.Mailbox')
    @patch(f'{module}.HTMLBody')
    @patch(f'{module}.send_new_message')
    @patch(f'{module}.audit', Mock())
    @patch(f'{module}.NHSMAIL_CREDENTIALS', None)
    @patch(f'{module}.FAILED_EMAIL_QUEUE', 'test_failure_queue')
    def test_error_sending_message(
        self, sqs_mock, html_mock, mailbox_mock, credentials_mock, message_mock, account_mock, secret_mock, log_mock
    ):
        # Given
        event = {
            'Records': [{
                'body': json.dumps({
                    'email_addresses': ['x', 'y'],
                    'subject': 'spujb',
                    'body': 'message'
                })
            }]
        }
        secret_mock.return_value = json.dumps({
            'username': 'user',
            'password': 'password'
        })
        account = Mock()
        account_mock.return_value = account
        mailbox_mock.return_value = 'recipient'
        credentials_mock.return_value = 'credentials'
        message = Mock()
        message_mock.return_value = message
        message_body = Mock()
        html_mock.return_value = message_body
        message.send.side_effect = Exception('tragedy')

        # When
        lambda_handler.__wrapped__(event, {})

        # Then
        secret_mock.assert_called_with('nhsmail_credentials', secret_name_is_key=False)
        credentials_mock.assert_called_with('user', 'password')
        account_mock.assert_called_with('credentials')
        sqs_mock.assert_called_with('test_failure_queue', json.loads(event['Records'][0]['body']))
        mailbox_mock.assert_has_calls([call(email_address='x'), call(email_address='y')])
        html_mock.assert_called_with('<html><body>message</body></html>')
        message_mock.assert_called_with(
            account=account,
            subject='spujb',
            body=message_body,
            to_recipients=['recipient', 'recipient']
        )
        message.send.assert_called_once()

        expected_logs = [
            call({'log_reference': LogReference.SENDEMAIL0002, 'username': 'user'}),
            call({'log_reference': LogReference.SENDEMAIL0003}),
            call({'log_reference': LogReference.SENDEMAIL0005, 'error': 'tragedy'})
        ]
        log_mock.assert_has_calls(expected_logs)

    @patch(f'{module}.EWS_URL', 'url')
    @patch(f'{module}.EWS_AUTH_TYPE', 'type')
    @patch(f'{module}.PRIMARY_SMTP_ADDRESS', 'smtp')
    @patch(f'{module}.EWS_SERVER_VERSION', 'server')
    @patch(f'{module}.Configuration')
    @patch(f'{module}.Account')
    def test_happy_path_get_account(self, account_mock, configuration_mock):
        # Given
        configuration = 'eeeeeeeeeeeeee'
        configuration_mock.return_value = configuration
        account_return_value = Mock()
        account_return_value.protocol.service_endpoint = 'blah'
        account_return_value.protocol.auth_type = 'wah'
        account_return_value.primary_smtp_address = 'eh'
        account_return_value.version = 'deg'
        account_mock.return_value = account_return_value

        # When
        account = get_account({})

        # Then
        configuration_mock.assert_called_with(
            service_endpoint='url',
            credentials={},
            auth_type='type',
            version='server'
        )
        account_mock.assert_called_with(
            primary_smtp_address='smtp',
            config=configuration,
            autodiscover=False
        )
        self.assertEqual(account, account_return_value)

    @patch(f'{module}.EWS_URL', 'url')
    @patch(f'{module}.EWS_AUTH_TYPE', 'type')
    @patch(f'{module}.PRIMARY_SMTP_ADDRESS', 'smtp')
    @patch(f'{module}.EWS_SERVER_VERSION', None)
    @patch(f'{module}.Account')
    def test_happy_path_get_account_missing_config(self, account_mock):
        # Given
        account_return_value = Mock()
        account_return_value.protocol.service_endpoint = 'blah'
        account_return_value.protocol.auth_type = 'wah'
        account_return_value.primary_smtp_address = 'eh'
        account_return_value.version = 'deg'
        account_mock.return_value = account_return_value
        event = Mock()
        event.username = 'user'

        # When
        account = get_account(event)

        # Then
        account_mock.assert_called_with(
            primary_smtp_address='user',
            credentials=event,
            autodiscover=True
        )
        self.assertEqual(account, account_return_value)
