This lambda, upon receiving an event, sends an email. The message body, subject, and recipients are supplied in the event.

* Currently only directly messaging is implemented, but CC/BCC'ing is supported by exchangelib and easy enough to introduce if needed.
* Credentials are retrieved from AWS Secrets Manager. Different accounts exist for dev and prod environments.