import json
import os
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.sqs_utils import send_new_message
from common.utils.secrets_manager_utils import get_secret
from common.log import log
from send_email.log_references import LogReference
from common.utils.audit_utils import audit, AuditActions, AuditUsers
from exchangelib import Account, Configuration, Credentials, HTMLBody, Mailbox, Message, autodiscover
from exchangelib.protocol import close_connections

NHSMAIL_CREDENTIALS = None
EWS_URL = None
EWS_AUTH_TYPE = None
PRIMARY_SMTP_ADDRESS = None
EWS_SERVER_VERSION = None
ACCOUNT = None
FAILED_EMAIL_QUEUE = os.environ.get('FAILED_EMAIL_QUEUE_URL')
autodiscover.AutodiscoverProtocol.TIMEOUT = 600


@lambda_entry_point
def lambda_handler(event, context):
    global NHSMAIL_CREDENTIALS, EWS_URL, EWS_AUTH_TYPE, PRIMARY_SMTP_ADDRESS, EWS_SERVER_VERSION, ACCOUNT
    close_connections()
    try:
        NHSMAIL_CREDENTIALS = NHSMAIL_CREDENTIALS or json.loads(
            get_secret('nhsmail_credentials', secret_name_is_key=False)
        )
        username, password = NHSMAIL_CREDENTIALS['username'], NHSMAIL_CREDENTIALS['password']
    except Exception as e:
        log({'log_reference': LogReference.SENDEMAIL0001, 'error': str(e)})
        raise e

    log({'log_reference': LogReference.SENDEMAIL0002, 'username': NHSMAIL_CREDENTIALS['username']})
    credentials = Credentials(username, password)

    try:
        account = ACCOUNT if ACCOUNT else get_account(credentials)
        ACCOUNT = account
        log({'log_reference': LogReference.SENDEMAIL0003})
    except Exception as e:
        log({'log_reference': LogReference.SENDEMAIL0004, 'error': str(e)})
        raise e
    for record in event['Records']:
        try:
            payload = json.loads(record['body'])
            recipients = [Mailbox(email_address=address) for address in payload['email_addresses']]
            message = Message(
                account=account,
                subject=payload['subject'],
                body=HTMLBody(f'<html><body>{payload["body"]}</body></html>'),
                to_recipients=recipients
            )
            message.send()
        except Exception as e:
            log({'log_reference': LogReference.SENDEMAIL0005, 'error': str(e)})
            send_new_message(FAILED_EMAIL_QUEUE, payload)

    log({'log_reference': LogReference.SENDEMAIL0006, 'amount': len(event['Records'])})
    close_connections()
    audit(AuditActions.SEND_EMAIL, user=AuditUsers.SYSTEM)
    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': json.dumps('Hello from send_email!'),
        'isBase64Encoded': True
    }


def get_account(credentials):
    global EWS_URL, EWS_AUTH_TYPE, PRIMARY_SMTP_ADDRESS, EWS_SERVER_VERSION

    # Attempting to use cached configuration first
    if all([EWS_URL, EWS_AUTH_TYPE, PRIMARY_SMTP_ADDRESS, EWS_SERVER_VERSION]):
        cached_config = Configuration(
            service_endpoint=EWS_URL,
            credentials=credentials,
            auth_type=EWS_AUTH_TYPE,
            version=EWS_SERVER_VERSION
        )
        account = Account(
            primary_smtp_address=PRIMARY_SMTP_ADDRESS,
            config=cached_config,
            autodiscover=False
        )
    else:
        account = Account(primary_smtp_address=credentials.username, credentials=credentials, autodiscover=True)

    EWS_URL = account.protocol.service_endpoint
    EWS_AUTH_TYPE = account.protocol.auth_type
    PRIMARY_SMTP_ADDRESS = account.primary_smtp_address
    EWS_SERVER_VERSION = account.version

    return account
