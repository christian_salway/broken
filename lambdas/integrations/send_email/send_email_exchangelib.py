import json
from exchangelib import Account, Configuration, Credentials, HTMLBody, Mailbox, Message, FaultTolerance
from exchangelib.autodiscover import Autodiscovery
from send_email.log_references import LogReference
from common.log import log

EWS_URL = None
EWS_AUTH_TYPE = None
PRIMARY_SMTP_ADDRESS = None
EWS_SERVER_VERSION = None


def send_exchange_email(username, password, event):
    log({'log_reference': LogReference.SENDEMAIL0002, 'username': username})
    Autodiscovery.INITIAL_RETRY_POLICY = FaultTolerance(max_wait=600)
    credentials = Credentials(username, password)
    try:
        account = get_account(credentials)
        log({'log_reference': LogReference.SENDEMAIL0003})
    except Exception as e:
        log({'log_reference': LogReference.SENDEMAIL0004, 'error': str(e)})

    try:
        recipients = [Mailbox(email_address=x) for x in event['email_addresses']]
        message = Message(
            account=account,
            subject=event['subject'],
            body=HTMLBody(f'<html><body>{event["message"]}</body></html>'),
            to_recipients=recipients
        )

        message.send()
    except Exception as e:
        log({'log_reference': LogReference.SENDEMAIL0005, 'error': str(e)})
        raise e
    else:
        log({'log_reference': LogReference.SENDEMAIL0006})
        return {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json'},
            'body': json.dumps('Hello from send_email!'),
            'isBase64Encoded': True
        }


def get_account(credentials):
    global EWS_URL, EWS_AUTH_TYPE, PRIMARY_SMTP_ADDRESS, EWS_SERVER_VERSION

    # Attempting to use cached configuration first
    if all([EWS_URL, EWS_AUTH_TYPE, PRIMARY_SMTP_ADDRESS, EWS_SERVER_VERSION]):
        log({'log_reference': LogReference.SENDEMAIL0007})
        cached_config = Configuration(
            service_endpoint=EWS_URL,
            credentials=credentials,
            auth_type=EWS_AUTH_TYPE,
            version=EWS_SERVER_VERSION
        )
        account = Account(
            primary_smtp_address=PRIMARY_SMTP_ADDRESS,
            config=cached_config,
            autodiscover=False
        )
    else:
        log({'log_reference': LogReference.SENDEMAIL0008})
        account = Account(primary_smtp_address=credentials.username, credentials=credentials, autodiscover=True)

    EWS_URL = account.protocol.service_endpoint
    EWS_AUTH_TYPE = account.protocol.auth_type
    PRIMARY_SMTP_ADDRESS = account.primary_smtp_address
    EWS_SERVER_VERSION = account.version

    return account
