import unittest
import socket
import os
import json
from mock import call, patch, Mock
from http import HTTPStatus
from ddt import ddt, data
from log_forwarding.log_references import LogReference

SPLUNK_URL = "test:8088"

os.environ['SPLUNK_URL'] = SPLUNK_URL
os.environ['SPLUNK_TOKEN'] = "test"
os.environ['SPLUNK_INDEX'] = "test"
os.environ['SPLUNK_VERIFY_SSL'] = "FALSE"
os.environ['ENVIRONMENT_NAME'] = "Test"

with patch('boto3.resource') as resource_mock:
    from log_forwarding.log_forwarding import lambda_handler, convert_non_lambda_log_data, convert_lambda_log_data
    from common.test_mocks.mock_response import MockResponse
    import log_forwarding.log_forwarding as builder

TEST_VALID_PAYLOAD_NON_LAMBDA_LOGS = {
    "awslogs": {
        "data": "H4sIAAAAAAAAAHWPwQqCQBCGX0Xm7EFtK+smZBEUgXoLCdMhFtKV3akI8d0bLYmibvPPN3wz00CJxmQnTO41whwWQRIctmEcB6sQbFC3CjW3XW8kxpOpP+OC22d1Wml1qZkQGtoMsScxaczKN3plG8zlaHIta5KqWsozoTYw3/djzwhpLwivWFGHGpAFe7DL68JlBUk+l7KSN7tCOEJ4M3/qOI49vMHj+zCKdlFqLaU2ZHV2a4Ct/an0/ivdX8oYc1UVX860fQDQiMdxRQEAAA=="  
    }
}
TEST_AWS_PAYLOAD_LAMBDA_LOGS = {
    'awslogs': {
        'data': b'H4sIAOWaEV8C/52P0U7CMBSGX6VpvGRm7dqx7W6ESUiYGja8MYQUdiRNNoZtpyLh3W2HGmO8wbv2//6efueIG9BabKE87AEnCI/TMl3lWVGkkwwPEG5fd6AcIDRgPBxGsT04ULfbiWq7vWO1aNaVWNlote2zMy+MAtG4ggFtZt+Bpbpb642SeyPb3Y2sDShte4998XzHy/OQ7AV2podHLCs3DFwyrYibY6T1N6JxGoQxnzEaR0Pf9y373Mw9Kcp0XqI5PHe2Pa0SRDec2r7wNgFlHgf+5MVVSDwWcBLSNScREPRgraxfgq5maZkVJT4N0C8JeoHEPLu/+4fFuFPC9Br0OiCo0Wgk6xqqH4D4vstzaFp1QIV8hwRxQlE+Qrl4+8oXGuyfUWjjPzYJLtgkux1fugY+LU8fdUNJ420CAAA='  
    }
}

TEST_MIXED_PAYLOAD = {
    "awslogs": {
        "data": "H4sIACFIDF8C/62RXWvCMBiF/0oIu7Qjn9UWvChYRXAObPBmSEltJoV+uDY6hvO/7411u9h2I9td3nNOTp4kJ1yZrtM7o972BocITyIVpQ9xkkSzGA8Qbl5r0zqDMi6kPxwFsHBG2exmbXPYO6/UVZbrFKR0d9F6P7Gt0ZULWNPZxZcAbnfIum1b7G3R1NOitKbtIPd0CfYz3vQl8dHU9mKecJG7MuOUeU5djy2A3+rKYVAhiBAsGA0JIeBdb+a2JCpaKbQyLwdIz/MQsa1kkNfeljPhSSOfvSD3qSe4pD7LJB0ZitZABXwhultEKk4UPg/QNwh2A4SLjRlhxCPUY1xRFnIRSv++f1r0jkpoLcfz5fQRBlMfi7apKzgnrTVsdW8D+rVxrNz4Wf+TjP8HmS8FZ/QvZIj9wiZuYIuXk1s/Dp835w/DqEgQ2QIAAA=="  
    }
}
TEST_INVALID_PAYLOAD_1 = {
    "somekey": 42
}
TEST_INVALID_PAYLOAD_2 = {
    "awslogs": {
        "somekey": 42
    }
}

CONVERTED_AWS_DATA = 'time=2015-08-24T19:03:07+00:00 | level=INFO | aws_log_type=START | ' + \
    'aws_request_id=2c52144a-c324-5e5f-9d61-435162b518e1\n' + \
    'time=2015-08-24T19:03:07+00:00 | level=INFO | aws_log_type=REPORT | ' + \
    'aws_request_id=2c52144a-c324-5e5f-9d61-435162b518e1 | ' + \
    'duration="2.31 ms" | billed_duration="100 ms" | memory_size="512 MB" | max_memory_used="86 MB"\n' + \
    'time=2015-08-24T19:03:07+00:00 | level=INFO | aws_log_type=END | aws_request_id=2c52144a-c324-5e5f-9d61-435162b518e1' 

CONVERTED_MIXED_DATA = 'time=2020-01-23T12:34:56.123456 | level=INFO | aws_log_type=START | ' + \
    'aws_request_id=2c52144a-c324-5e5f-9d61-435162b518e1\n' + \
    'time=2020-01-23T12:34:56.123456 | level=INFO | environment_name=test | message=Test message\n' + \
    'time=2020-01-23T12:34:56.654321 | level=INFO | environment_name=test | message=Test message 2\n' + \
    'time=2020-01-23T12:34:56.654321 | level=INFO | aws_log_type=END | ' + \
    'aws_request_id=2c52144a-c324-5e5f-9d61-435162b518e1'

CONVERTED_MIXED_EDGE_DATA = 'time=2015-08-24T19:03:07+00:00 | level=INFO | aws_log_type=START | ' + \
    'aws_request_id=2c52144a-c324-5e5f-9d61-435162b518e1\n' + \
    'time=2015-08-24T19:03:07+00:00 | Exception\ntime=2015-08-24T19:03:07+00:00 | level=INFO | ' + \
    'aws_log_type=REPORT | aws_request_id=2c52144a-c324-5e5f-9d61-435162b518e1 | duration="2.31 ms" | ' + \
    'billed_duration="100 ms" | memory_size="512 MB" | max_memory_used="86 MB"'


def mock_post_fail(*args, **kwargs):
    return MockResponse({}, HTTPStatus.INTERNAL_SERVER_ERROR)


def mock_post_succeed(*args, **kwargs):
    if kwargs['url'] == f'{SPLUNK_URL}/services/collector/raw':
        return MockResponse({"ackId": 123456}, HTTPStatus.OK)
    elif kwargs['url'] == f'{SPLUNK_URL}/services/collector/ack?channel=FA4F81AF-2E21-4F8E-A217-69053C865501':
        return MockResponse({"acks": {'123456': True}}, HTTPStatus.OK)
    raise ValueError('Unknown request url in mock')


@patch.object(builder, 'socket', Mock(wraps=socket))
@ddt
class TestLogForwarding(unittest.TestCase):

    def setUp(self):
        os.environ['SPLUNK_USE_ACK'] = "TRUE"
        self.unwrapped_handler = lambda_handler.__wrapped__

    @patch('log_forwarding.log_forwarding.log')
    @patch('log_forwarding.log_forwarding.requests.Session.post', side_effect=mock_post_fail)
    def test_log_created_and_exception_raised_when_non_200_response_code(self, mock_reponse, mock_logger):
        with self.assertRaises(Exception):
            self.unwrapped_handler(TEST_VALID_PAYLOAD_NON_LAMBDA_LOGS, {})

        mock_logger.assert_called_with({'log_reference': LogReference.LOGFOR0002, 'lines_to_process': 2})

    @patch('log_forwarding.log_forwarding.log')
    @patch('log_forwarding.log_forwarding.requests.Session.post', side_effect=mock_post_succeed)
    def test_payload_sent_log_when_200_response_code(self, mock_response, mock_logger):
        self.unwrapped_handler(TEST_VALID_PAYLOAD_NON_LAMBDA_LOGS, {})

        mock_logger.assert_called_with({'log_reference': LogReference.LOGFOR0001, 'lines_processed': 2})

    @patch('log_forwarding.log_forwarding.log')
    @patch('log_forwarding.log_forwarding.requests.Session.post', side_effect=mock_post_succeed)
    def test_aws_logs_converted_and_use_datetime(self, mock_response, mock_logger):

        builder.HOST_NAME = 'hostname'
        self.unwrapped_handler(TEST_AWS_PAYLOAD_LAMBDA_LOGS, {})

        mock_response.assert_has_calls([
            call(
                url='test:8088/services/collector/raw',
                data=CONVERTED_AWS_DATA,
                params={'host': 'hostname', 'index': 'test', 'source': 'lambda_log_group', 'sourcetype': 'screening:aws:cloudwatch_logs'},  
                headers={'Authorization': 'Splunk test', 'X-Splunk-Request-Channel': 'FA4F81AF-2E21-4F8E-A217-69053C865501'},  
                verify=False
            ),
            call(
                url='test:8088/services/collector/ack?channel=FA4F81AF-2E21-4F8E-A217-69053C865501',
                data='{"acks": [123456]}',
                headers={
                    'Authorization': 'Splunk test',
                },
                verify=False
            ),
        ])

    @patch('time.sleep')
    @patch('log_forwarding.log_forwarding.log')
    @patch('log_forwarding.log_forwarding.requests.Session.post', side_effect=[
        MockResponse({"ackId": 123456}, HTTPStatus.OK),
        MockResponse({"acks": {'123456': False}}, HTTPStatus.OK),
        MockResponse({"acks": {'123456': True}}, HTTPStatus.OK),
    ])
    def test_splunk_ack_partial_failure(self, mock_response, mock_logger, mock_time):

        builder.HOST_NAME = 'hostname'
        self.unwrapped_handler(TEST_AWS_PAYLOAD_LAMBDA_LOGS, {})

        mock_response.assert_has_calls([
            call(
                url='test:8088/services/collector/raw',
                data=CONVERTED_AWS_DATA,
                params={'host': 'hostname', 'index': 'test', 'source': 'lambda_log_group', 'sourcetype': 'screening:aws:cloudwatch_logs'},  
                headers={'Authorization': 'Splunk test', 'X-Splunk-Request-Channel': 'FA4F81AF-2E21-4F8E-A217-69053C865501'},  
                verify=False
            ),
            *[call(
                url='test:8088/services/collector/ack?channel=FA4F81AF-2E21-4F8E-A217-69053C865501',
                data='{"acks": [123456]}',
                headers={
                    'Authorization': 'Splunk test',
                },
                verify=False
            ) for _ in range(2)],
        ])
        mock_time.assert_has_calls([call(10) for _ in range(1)])

    @patch('time.sleep')
    @patch('log_forwarding.log_forwarding.log')
    @patch('log_forwarding.log_forwarding.requests.Session.post', side_effect=[
        MockResponse({"ackId": 123456}, HTTPStatus.OK),
        *[MockResponse({"acks": {'123456': False}}, HTTPStatus.OK) for _ in range(30)],
    ])
    def test_splunk_ack_failure(self, mock_response, mock_logger, mock_time):

        builder.HOST_NAME = 'hostname'
        with self.assertRaises(Exception) as context:
            self.unwrapped_handler(TEST_AWS_PAYLOAD_LAMBDA_LOGS, {})

        self.assertEqual(str(context.exception), LogReference.LOGFOR0005.message)
        mock_response.assert_has_calls([
            call(
                url='test:8088/services/collector/raw',
                data=CONVERTED_AWS_DATA,
                params={'host': 'hostname', 'index': 'test', 'source': 'lambda_log_group', 'sourcetype': 'screening:aws:cloudwatch_logs'},  
                headers={'Authorization': 'Splunk test', 'X-Splunk-Request-Channel': 'FA4F81AF-2E21-4F8E-A217-69053C865501'},  
                verify=False
            ),
            *[call(
                url='test:8088/services/collector/ack?channel=FA4F81AF-2E21-4F8E-A217-69053C865501',
                data='{"acks": [123456]}',
                headers={
                    'Authorization': 'Splunk test',
                },
                verify=False
            ) for _ in range(30)],
        ])
        mock_time.assert_has_calls([call(10) for _ in range(30)])
        mock_logger.assert_called_with({'log_reference': LogReference.LOGFOR0005, 'ack_id': 123456})

    @patch('log_forwarding.log_forwarding.log')
    @patch('log_forwarding.log_forwarding.requests.Session.post', side_effect=mock_post_succeed)
    def test_aws_logs_use_app_log_dates(self, mock_response, mock_logger):

        builder.HOST_NAME = 'hostname'
        self.unwrapped_handler(TEST_MIXED_PAYLOAD, {})

        mock_response.assert_has_calls([
            call(
                url='test:8088/services/collector/raw',
                data=CONVERTED_MIXED_DATA,
                params={'host': 'hostname', 'index': 'test', 'source': 'lambda_log_group', 'sourcetype': 'screening:aws:cloudwatch_logs'},  
                headers={'Authorization': 'Splunk test', 'X-Splunk-Request-Channel': 'FA4F81AF-2E21-4F8E-A217-69053C865501'},  
                verify=False
            ),
            call(
                url='test:8088/services/collector/ack?channel=FA4F81AF-2E21-4F8E-A217-69053C865501',
                data='{"acks": [123456]}',
                headers={
                    'Authorization': 'Splunk test',
                },
                verify=False
            ),
        ])

    @patch('log_forwarding.log_forwarding.log')
    @patch('log_forwarding.log_forwarding.requests.Session.post', side_effect=mock_post_succeed)
    def test_aws_log_edge_case_where_line_length_caused_exception(self, mock_response, mock_logger):
        aws_log = {
            'logEvents': [
                {
                    'id': 'eventId1',
                    'message': 'START RequestId: 2c52144a-c324-5e5f-9d61-435162b518e1 Version: $LATEST',
                    'timestamp': 1440442987000
                },
                {
                    'id': 'eventId2',
                    'message': 'Exception',  # This used to cause an exception due to the message being too short
                    'timestamp': 1440442987000
                },
                {
                    'id': 'eventId3',
                    'message': 'REPORT RequestId: 2c52144a-c324-5e5f-9d61-435162b518e1 Duration: 2.31 ms Billed Duration: 100 ms Memory Size: 512 MB Max Memory Used: 86 MB',  
                    'timestamp': 1440442987000
                }
            ],
            'logGroup': 'lambda_log_group',
            'logStream': 'testLogStream',
            'messageType': 'DATA_MESSAGE',
            'owner': '123456789123',
            'subscriptionFilters': ['testFilter']
        }

        builder.HOST_NAME = 'hostname'
        self.unwrapped_handler(_build_cloud_watch_test_event(aws_log), {})

        mock_response.assert_has_calls([
            call(
                url='test:8088/services/collector/raw',
                data=CONVERTED_MIXED_EDGE_DATA,
                params={'index': 'test', 'sourcetype': 'screening:aws:cloudwatch_logs', 'source': 'lambda_log_group', 'host': 'hostname'},  
                headers={'Authorization': 'Splunk test', 'X-Splunk-Request-Channel': 'FA4F81AF-2E21-4F8E-A217-69053C865501'},  
                verify=False
            ),
            call(
                url='test:8088/services/collector/ack?channel=FA4F81AF-2E21-4F8E-A217-69053C865501',
                data='{"acks": [123456]}',
                headers={
                    'Authorization': 'Splunk test',
                },
                verify=False
            ),
        ])

    def test_convert_non_lambda_log_data_formats_json__messages(self):
        log_data = [
            {
                'message': json.dumps({'k1': 'v1', 'k2': 'v2'})
            },
            {
                'message': json.dumps({'k11': 'v11', 'k21': 'v21'})
            }
        ]

        expected_splunk_logs = [
            'k1=v1 | k2=v2',
            'k11=v11 | k21=v21'
        ]

        self.assertEqual(expected_splunk_logs, convert_non_lambda_log_data(log_data))

    def test_convert_non_lambda_log_data_formats_non_json_messages(self):
        log_data = [
            {
                'message': 'time=123 something other thing'
            }
        ]

        expected_splunk_logs = [
            'time=123 something other thing'
        ]

        self.assertEqual(expected_splunk_logs, convert_non_lambda_log_data(log_data))

    @patch('log_forwarding.log_forwarding.log')
    @patch('log_forwarding.log_forwarding.requests.Session.post', side_effect=mock_post_succeed)
    @patch('log_forwarding.log_forwarding.convert_lambda_log_data')
    @patch('log_forwarding.log_forwarding.convert_non_lambda_log_data')
    def test_lambda_handler_calls_convert_lambda_log_data_for_lambda_logs(self, convert_non_lambda_log_mock,
                                                                          convert_lambda_log_mock, mock_logger, *args):
        convert_lambda_log_mock.return_value = [[], 0]
        self.unwrapped_handler(TEST_AWS_PAYLOAD_LAMBDA_LOGS, {})

        convert_lambda_log_mock.assert_called()
        convert_non_lambda_log_mock.assert_not_called()

    @data(TEST_INVALID_PAYLOAD_1, TEST_INVALID_PAYLOAD_2)
    @patch('log_forwarding.log_forwarding.log')
    @patch('log_forwarding.log_forwarding.requests.Session.post', side_effect=mock_post_succeed)
    @patch('log_forwarding.log_forwarding.convert_lambda_log_data')
    @patch('log_forwarding.log_forwarding.convert_non_lambda_log_data')
    def test_lambda_handler_ignores_invalid_events(self, payload, convert_non_lambda_log_mock,
                                                   convert_lambda_log_mock, post_mock, mock_logger, *args):
        self.unwrapped_handler(payload, {})

        convert_lambda_log_mock.assert_not_called()
        convert_non_lambda_log_mock.assert_not_called()
        mock_logger.assert_called_with(
            {'log_reference': LogReference.LOGFOR0006, 'event': payload}
        )

    @patch('log_forwarding.log_forwarding.log')
    @patch('log_forwarding.log_forwarding.requests.Session.post', side_effect=mock_post_succeed)
    @patch('log_forwarding.log_forwarding.convert_lambda_log_data')
    @patch('log_forwarding.log_forwarding.convert_non_lambda_log_data')
    def test_lambda_handler_calls_convert_non_lambda_log_data_for_lambda_logs(
            self, convert_non_lambda_log_mock, convert_lambda_log_mock, mock_logger, *args):
        self.unwrapped_handler(TEST_VALID_PAYLOAD_NON_LAMBDA_LOGS, {})

        convert_non_lambda_log_mock.assert_called()
        convert_lambda_log_mock.assert_not_called()

    def test_convert_lambda_log_data_sensitive_data(self):
        log_data = [
            {'id': 'eventId1',
                'timestamp': 1440442987000,
                'message': 'START RequestId: 2c52144a-c324-5e5f-9d61-435162b518e1 Version: $LATEST'},
            {'id': 'eventId2',
                'timestamp': 1440442987000,
                'message': 'time=2020-01-23T12:34:56.123456 | level=INFO | environment_name=test | message=Test message'},  
            {'id': 'eventId3',
                'timestamp': 1440442987000,
                'message': 'time=2020-01-23T12:34:56.654321 | level=INFO | environment_name=test | message=Test message 2 | sensitive_data=true'}, 
            {'id': 'eventId4',
                'timestamp': 1440442987000,
                'message': 'END RequestId: 2c52144a-c324-5e5f-9d61-435162b518e1'}
        ]

        expected_splunk_logs = [
            'time=2020-01-23T12:34:56.123456 | level=INFO | aws_log_type=START | aws_request_id=2c52144a-c324-5e5f-9d61-435162b518e1', 
            'time=2020-01-23T12:34:56.123456 | level=INFO | environment_name=test | message=Test message',
            'time=2020-01-23T12:34:56.654321 | level=INFO | aws_log_type=END | aws_request_id=2c52144a-c324-5e5f-9d61-435162b518e1'  
        ]

        actual_splunk_logs, lines_not_forwarded = convert_lambda_log_data(log_data)

        self.assertEqual(1, lines_not_forwarded)
        self.assertEqual(expected_splunk_logs, actual_splunk_logs)


def _build_cloud_watch_test_event(log_data):
    """
    used to build encoded and compresed events for unit tests, keep it as utils for future maintenance of this unittests
    """
    import gzip
    import json
    import base64

    data_string = json.dumps(log_data)
    compressed_data = gzip.compress(data_string.encode())
    encoded_data = base64.b64encode(compressed_data)
    return {
        "awslogs": {
            "data": encoded_data
        }
    }
