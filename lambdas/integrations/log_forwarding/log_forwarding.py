import base64
import zlib
import json
import os
import requests
import socket
import time
from datetime import datetime, timezone
from urllib3.exceptions import InsecureRequestWarning
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
from distutils import util
from common.utils.lambda_wrappers import lambda_entry_point
from log_forwarding.log_references import LogReference
from common.log import log


SPLUNK_URL = os.environ.get('SPLUNK_URL')
SPLUNK_TOKEN = os.environ.get('SPLUNK_TOKEN')
SPLUNK_INDEX = os.environ.get('SPLUNK_INDEX')
SPLUNK_VERIFY_SSL = bool(util.strtobool(os.environ.get('SPLUNK_VERIFY_SSL')))
ENVIRONMENT_NAME = os.environ.get('ENVIRONMENT_NAME')
HOST_NAME = socket.gethostname()
SOURCE_TYPE = 'screening:aws:cloudwatch_logs'
NEW_LINE_DELIMITER = '\n'
LOG_ENDPOINT = '/services/collector/raw'
ACK_ENDPOINT = '/services/collector/ack'


if not SPLUNK_VERIFY_SSL:
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


def get_use_splunk_ack():
    return bool(util.strtobool(os.environ.get('SPLUNK_USE_ACK')))


def get_first_log_time(log_data):
    for log_in_progress in log_data:
        log_line = log_in_progress['message']
        if log_line.startswith('time='):
            return log_line[5:31]
    # if no times in logs, return first timestamp
    timestamp_in_seconds = log_data[0]['timestamp']/1000.0
    timestr = datetime.fromtimestamp(timestamp_in_seconds, tz=timezone.utc).isoformat()
    return timestr


def convert_aws_log(log_line):
    log_type = log_line[:3]
    log_data = log_line.split()
    if len(log_data) > 2:
        preamble = f'level=INFO | aws_log_type={log_data[0]} | aws_request_id={log_data[2]}'
        if log_type == 'STA' or log_type == 'END':
            return preamble
        elif log_type == 'REP':
            return preamble + f' | duration="{log_data[4]} {log_data[5]}" | ' + \
                f'billed_duration="{log_data[8]} {log_data[9]}" | ' + \
                f'memory_size="{log_data[12]} {log_data[13]}" | ' + \
                f'max_memory_used="{log_data[17]} {log_data[18]}"'
    return log_line


def convert_lambda_log_data(log_data):
    splunk_logs = []
    lines_not_forwarded = 0

    # Not using timestamp as this is the received time at cloudwatch and causes out of order issues
    recent_time = get_first_log_time(log_data)
    for log_in_progress in log_data:
        log_line = log_in_progress['message']

        if log_line.startswith('time='):
            recent_time = log_line[5:31]
        else:
            log_line = f'time={recent_time} | ' + convert_aws_log(log_in_progress['message'])

        # TODO Instead of dropping this, add to a new audit splunk index (CERSS-1551)
        if 'sensitive_data=true' in log_line.split(' | '):
            lines_not_forwarded += 1
            continue

        splunk_logs.append(log_line)

    return splunk_logs, lines_not_forwarded


def convert_non_lambda_log_data(log_data):
    splunk_logs = []

    for log_in_progress in log_data:
        log_line = log_in_progress['message']
        try:
            message_json = json.loads(log_in_progress['message'])
            log_line = ' | '.join([f'{k}={v}' for k, v in message_json.items()])
        except ValueError:
            log_line = log_in_progress['message']
        splunk_logs.append(log_line)

    return splunk_logs


@lambda_entry_point
def lambda_handler(event, context):
    if 'awslogs' in event and 'data' in event['awslogs'] and event['awslogs']['data']:
        data = event['awslogs']['data']
    else:
        log({'log_reference': LogReference.LOGFOR0006, 'event': event})
        return

    log_data = json.loads(
        zlib.decompress(base64.b64decode(data), 16 + zlib.MAX_WBITS).decode('utf-8')
    )

    lines_not_forwarded = 0

    splunk_params = {
        'index': SPLUNK_INDEX,
        'sourcetype': SOURCE_TYPE,
        'source': log_data['logGroup'],
        'host': HOST_NAME
    }

    if 'lambda' in log_data['logGroup']:
        splunk_logs_with_times, lines_not_forwarded = convert_lambda_log_data(log_data['logEvents'])
    else:
        splunk_logs_with_times = convert_non_lambda_log_data(log_data['logEvents'])

    splunk_logs_str = NEW_LINE_DELIMITER.join(splunk_logs_with_times)

    retry_strategy = Retry(
        total=3,
        status_forcelist=[429, 500, 502, 503, 504]
    )
    session = requests.Session()
    session.mount('https://', HTTPAdapter(max_retries=retry_strategy))

    headers = {'Authorization': f'Splunk {SPLUNK_TOKEN}'}
    channel = 'FA4F81AF-2E21-4F8E-A217-69053C865501'

    if get_use_splunk_ack():
        # This is simply a random GUID used by Splunk to differentiate log forwarder
        # requests from those appearing from other sources
        headers['X-Splunk-Request-Channel'] = channel

    response = session.post(
        url=(f'{SPLUNK_URL}{LOG_ENDPOINT}'),
        data=splunk_logs_str,
        params=splunk_params,
        headers=headers,
        verify=SPLUNK_VERIFY_SSL
    )

    if lines_not_forwarded:
        log({'log_reference': LogReference.LOGFOR0003, 'lines_not_forwarded': lines_not_forwarded})

    if response.status_code != 200:
        log({'log_reference': LogReference.LOGFOR0002, 'lines_to_process': len(splunk_logs_with_times)})
        response.raise_for_status()

    acked = False
    ack_id = None
    if get_use_splunk_ack():
        response_body = response.json()
        ack_id = response_body['ackId']

        retries = 30
        retry_time_seconds = 10
        for _i in range(retries):
            acked = indexer_ack(session, ack_id, channel)
            if acked:
                break
            time.sleep(retry_time_seconds)

    if get_use_splunk_ack() and not acked:
        log({'log_reference': LogReference.LOGFOR0005, 'ack_id': ack_id})
        raise Exception(LogReference.LOGFOR0005.message)
    else:
        log({'log_reference': LogReference.LOGFOR0001, 'lines_processed': len(splunk_logs_with_times)})


def indexer_ack(session, ack_id, channel):
    url = f'{SPLUNK_URL}{ACK_ENDPOINT}?channel={channel}'
    body = {'acks': [ack_id]}
    response = session.post(
        url=url,
        data=json.dumps(body),
        headers={
            'Authorization': f'Splunk {SPLUNK_TOKEN}',
        },
        verify=SPLUNK_VERIFY_SSL
    )
    if response.status_code != 200:
        log({'log_reference': LogReference.LOGFOR0004, 'ack_id': ack_id})
        response.raise_for_status()
    return response.json()['acks'][str(ack_id)]
