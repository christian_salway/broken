import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    LOGFOR0001 = (logging.INFO, 'Payload sent successfully')
    LOGFOR0002 = (logging.WARNING, 'Non-200 status code received from payload transmission')
    LOGFOR0003 = (logging.INFO, 'PID in log, not forwarding some lines to Splunk')
    LOGFOR0004 = (logging.INFO, 'Failed to check ack on Splunk log')
    LOGFOR0005 = (logging.ERROR, 'No ack after timeout')
    LOGFOR0006 = (logging.WARNING, 'No log data to forward')
