import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    SENDMESHMESSAGE0001 = (logging.INFO,  'Retrieved MESH message properties from SQS queue')
    SENDMESHMESSAGE0002 = (logging.INFO,  'Retrieved file from Incoming S3 bucket')
    SENDMESHMESSAGE0003 = (logging.INFO,  'Instantiated MESH client using Mailbox ID, Password & Shared Key')
    SENDMESHMESSAGE0004 = (logging.ERROR, 'Missing information in SQS Message')
    SENDMESHMESSAGE0005 = (logging.ERROR, 'Failed preparations to retrieve file to send')
    SENDMESHMESSAGE0006 = (logging.ERROR, 'Failed to get a new Mesh Connection')
    SENDMESHMESSAGE0007 = (logging.INFO,  'MESH client message sent')
    SENDMESHMESSAGE0008 = (logging.ERROR, 'MESH client message sending failed')
    SENDMESHMESSAGE0009 = (logging.INFO,  'Uploading file to Completed S3 bucket')
    SENDMESHMESSAGE0010 = (logging.INFO,  'File uploaded to Completed S3 bucket')
    SENDMESHMESSAGE0011 = (logging.ERROR, 'Upload to Completed S3 bucket failed')
    SENDMESHMESSAGE0012 = (logging.INFO, 'Deleting file from Incoming S3 bucket')
    SENDMESHMESSAGE0013 = (logging.INFO, 'File deleted from Incoming S3 bucket')
    SENDMESHMESSAGE0014 = (logging.ERROR, 'Delete from Incoming S3 bucket failed')
