import json
import os
import tempfile

from common.utils.s3_utils import get_file_contents_from_bucket, put_object, delete_object_from_bucket
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from send_mesh_message.log_references import LogReference
from common.utils.secrets_manager_utils import get_secret_by_key
from common.utils.mesh.mesh_client_utils import get_mesh_client
from common.utils.sqs_utils import read_message_from_queue

ENVIRONMENT_NAME = os.environ.get('ENVIRONMENT_NAME')
ACCOUNT_ID = os.environ.get('ACCOUNT_ID')
COMPLETED_BUCKET = os.environ.get('COMPLETED_BUCKET')
MESH_ENVIRONMENT = os.environ.get('MESH_ENVIRONMENT')
MESH_HOST = os.environ.get('MESH_HOST')


@lambda_entry_point
def lambda_handler(event, context):
    try:
        file_name, from_mailbox_id, mesh_recipient, workflow_id, bucket_name = get_mesh_message_properties(event)
        full_bucket_name = f'{ENVIRONMENT_NAME}-{ACCOUNT_ID}-{bucket_name}'
    except Exception as e:
        log({'log_reference': LogReference.SENDMESHMESSAGE0004, 'add_exception_info': True})
        raise e

    try:
        file_contents, encoded_file_contents = get_file_contents(full_bucket_name, file_name)
    except Exception as e:
        log({'log_reference': LogReference.SENDMESHMESSAGE0005, 'add_exception_info': True})
        raise e

    with tempfile.TemporaryDirectory() as temp_dir_name:
        try:
            mailbox_password = get_secret_by_key('mesh_mailbox_credentials', from_mailbox_id)
            mesh_client = get_mesh_client(from_mailbox_id, mailbox_password, temp_dir_name)
            log({'log_reference': LogReference.SENDMESHMESSAGE0003})
        except Exception as e:
            log({'log_reference': LogReference.SENDMESHMESSAGE0006, 'add_exception_info': True})
            raise e

        try:
            message_id = mesh_client.send_message(mesh_recipient, encoded_file_contents, workflow_id=workflow_id)
            log({'log_reference': LogReference.SENDMESHMESSAGE0007, 'id_response': message_id})
            mesh_client.close()
        except Exception as e:
            log({'log_reference': LogReference.SENDMESHMESSAGE0008, 'add_exception_info': True})
            raise e

    try:
        log({'log_reference': LogReference.SENDMESHMESSAGE0009, 'target_bucket': COMPLETED_BUCKET})
        put_object(COMPLETED_BUCKET, file_contents, file_name)
        log({'log_reference': LogReference.SENDMESHMESSAGE0010})
    except Exception as e:
        log({'log_reference': LogReference.SENDMESHMESSAGE0011, 'add_exception_info': True})
        raise e

    try:
        log({'log_reference': LogReference.SENDMESHMESSAGE0012, 'target_bucket': full_bucket_name})
        delete_object_from_bucket(full_bucket_name, file_name)
        log({'log_reference': LogReference.SENDMESHMESSAGE0013})
    except Exception as e:
        log({'log_reference': LogReference.SENDMESHMESSAGE0014, 'add_exception_info': True})
        raise e


def get_mesh_message_properties(event):
    event_json = read_message_from_queue(event)
    mesh_message_properties = json.loads(event_json)
    log({'log_reference': LogReference.SENDMESHMESSAGE0001, 'mesh_message_properties': mesh_message_properties})
    file_name = mesh_message_properties['object_key']
    from_mailbox_id = mesh_message_properties['from_mailbox']
    mesh_recipient = mesh_message_properties['to_mailbox']
    workflow_id = mesh_message_properties['workflow_id']
    bucket_name = mesh_message_properties['bucket_name']
    return file_name, from_mailbox_id, mesh_recipient, workflow_id, bucket_name


def get_file_contents(full_bucket_name, file_name):
    file_response = get_file_contents_from_bucket(full_bucket_name, file_name)
    file_contents = file_response['body']
    log({'log_reference': LogReference.SENDMESHMESSAGE0002})
    encoded_file_contents = file_contents.encode('utf-8')
    return file_contents, encoded_file_contents
