This lambda, upon receiving an event, sends a MESH message containing a file retrieved from a specified bucket.
The mailbox the message is sent from, mailbox message is to be sent to, workflow ID, specified bucket name and filename are supplied in the event.

The lambda function fetches a file (we expect these to be either a demographics or results file) from the specified bucket. It initialises a MESH client and sends a message using the from/to mailboxes and sends the encoded file on accordingly.

If successful the file is moved to a completed bucket, otherwise an exception is raised on the lambda.

* Bucket name is provided in the event for flexibility.
* MESH credentials and mailbox credentials are retrieved from AWS Secrets Manager.