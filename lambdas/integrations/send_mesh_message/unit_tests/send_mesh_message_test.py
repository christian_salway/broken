from unittest import TestCase
from mock import patch, Mock, call
from send_mesh_message.log_references import LogReference

with patch('boto3.client') as client_mock:
    import send_mesh_message.send_mesh_message as environment
    from send_mesh_message.send_mesh_message import lambda_handler

environment.ENVIRONMENT_NAME = 'sp-env'
environment.ACCOUNT_ID = '1234'
environment.COMPLETED_BUCKET = 'completed_bucket'
environment.MESH_ENVIRONMENT = 'mesh_env'
environment.MESH_HOST = 'mesh_host'

FROM_MAILBOX_ID = 'from_id'
TO_MAILBOX_ID = 'to_id'
MAILBOX_PASSWORD = 'pword'
DATA_BUCKET = 'data_bucket'
FILE_KEY = 'filename'
WORKFLOW_ID = '1'


@patch('send_mesh_message.send_mesh_message.log')
@patch('send_mesh_message.send_mesh_message.get_file_contents_from_bucket')
@patch('send_mesh_message.send_mesh_message.get_secret_by_key')
@patch('send_mesh_message.send_mesh_message.get_mesh_client')
@patch('send_mesh_message.send_mesh_message.put_object')
@patch('send_mesh_message.send_mesh_message.delete_object_from_bucket')
class TestSendMeshMessage(TestCase):
    def setUp(self):
        self.unwrapped_function = lambda_handler.__wrapped__

    @patch('send_mesh_message.send_mesh_message.tempfile')
    def test_calls_functions_and_logs_progress(
        self,
        tempfile_mock,
        delete_object_mock,
        put_object_mock,
        get_mesh_client_mock,
        get_secret_by_key_mock,
        get_file_contents_from_bucket,
        log_mock
    ):
        results_file = 'results'
        encoded_results_file = results_file.encode('utf-8')
        get_secret_by_key_mock.return_value = MAILBOX_PASSWORD
        get_file_contents_from_bucket.return_value = {
            'body': results_file,
            'internal_id': 'internal_id'
        }
        tempfile_mock.TemporaryDirectory.return_value.__enter__.return_value = 'the_temp_dir_name'
        mesh_client_fake = Mock()
        mesh_client_fake.send_message.return_value = 'response'
        get_mesh_client_mock.return_value = mesh_client_fake

        message = f'''{{"from_mailbox": "{FROM_MAILBOX_ID}", "to_mailbox": "{TO_MAILBOX_ID}",
                        "workflow_id": "{WORKFLOW_ID}", "bucket_name": "{DATA_BUCKET}","object_key": "{FILE_KEY}"}}'''
        self.unwrapped_function({'Records': [{'body': message}]}, '')

        mesh_message_properties = {
            'from_mailbox': FROM_MAILBOX_ID,
            'to_mailbox': TO_MAILBOX_ID,
            'workflow_id': WORKFLOW_ID,
            'bucket_name': DATA_BUCKET,
            'object_key': FILE_KEY
        }

        get_file_contents_from_bucket.assert_called_once_with('sp-env-1234-data_bucket', FILE_KEY)
        get_mesh_client_mock.assert_called_once_with(FROM_MAILBOX_ID, MAILBOX_PASSWORD, 'the_temp_dir_name')
        mesh_client_fake.send_message.assert_called_once_with(
            TO_MAILBOX_ID, encoded_results_file, workflow_id=WORKFLOW_ID
        )
        mesh_client_fake.close.assert_called_once()
        put_object_mock.assert_called_once_with('completed_bucket', results_file, 'filename')
        tempfile_mock.TemporaryDirectory.return_value.__exit__.assert_called_once()
        log_mock.assert_has_calls([
            call
            ({'log_reference': LogReference.SENDMESHMESSAGE0001,  'mesh_message_properties': mesh_message_properties}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0002}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0003}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0007, 'id_response': 'response'}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0009, 'target_bucket': 'completed_bucket'}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0010}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0012, 'target_bucket': 'sp-env-1234-data_bucket'}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0013})
        ])

    def test_message_is_missing_properties_throws_exception(
        self,
        delete_object_mock,
        put_object_mock,
        get_mesh_client_mock,
        get_secret_by_key_mock,
        get_file_contents_from_bucket,
        log_mock
    ):
        results_file = 'results'

        get_file_contents_from_bucket.return_value = results_file

        message = f'''{{"from_mailbox": "{FROM_MAILBOX_ID}", "to_mailbox": "{TO_MAILBOX_ID}",
                        "workflow_id": "{WORKFLOW_ID}", "bucket_name": "{DATA_BUCKET}"}}'''

        with self.assertRaises(Exception) as context:
            self.unwrapped_function({'Records': [{'body': message}]}, '')

        expected_exception_message = "object_key"

        self.assertEqual(expected_exception_message, context.exception.args[0])

        mesh_message_properties = {
            'from_mailbox': FROM_MAILBOX_ID,
            'to_mailbox': TO_MAILBOX_ID,
            'workflow_id': WORKFLOW_ID,
            'bucket_name': DATA_BUCKET,
        }

        get_file_contents_from_bucket.assert_not_called()
        log_mock.assert_has_calls([
            call
            (
                {'log_reference': LogReference.SENDMESHMESSAGE0001,
                 'mesh_message_properties': mesh_message_properties}
            ),
            call({'log_reference': LogReference.SENDMESHMESSAGE0004, 'add_exception_info': True})
        ])

    def test_fails_to_retrieve_file_throws_exception(
        self,
        delete_object_mock,
        put_object_mock,
        get_mesh_client_mock,
        get_secret_by_key_mock,
        get_file_contents_from_bucket,
        log_mock
    ):
        get_file_contents_from_bucket.side_effect = Exception('could not read file')

        expected_exception_message = "could not read file"

        message = f'''{{"from_mailbox": "{FROM_MAILBOX_ID}", "to_mailbox": "{TO_MAILBOX_ID}",
                        "workflow_id": "{WORKFLOW_ID}", "bucket_name": "{DATA_BUCKET}","object_key": "{FILE_KEY}"}}'''

        with self.assertRaises(Exception) as context:
            self.unwrapped_function({'Records': [{'body': message}]}, '')
        self.assertEqual(expected_exception_message, context.exception.args[0])

        mesh_message_properties = {
            'from_mailbox': FROM_MAILBOX_ID,
            'to_mailbox': TO_MAILBOX_ID,
            'workflow_id': WORKFLOW_ID,
            'bucket_name': DATA_BUCKET,
            'object_key': FILE_KEY
        }

        log_mock.assert_has_calls([
            call
            (
                {'log_reference': LogReference.SENDMESHMESSAGE0001,
                 'mesh_message_properties': mesh_message_properties}
            ),
            call({'log_reference': LogReference.SENDMESHMESSAGE0005, 'add_exception_info': True})
        ])

    @patch('send_mesh_message.send_mesh_message.tempfile')
    def test_fail_to_retrieve_mesh_client_secrets_throws_exception(
        self,
        tempfile_mock,
        delete_object_mock,
        put_object_mock,
        get_mesh_client_mock,
        get_secret_by_key_mock,
        get_file_contents_from_bucket,
        log_mock
    ):
        results_file = 'results'

        get_file_contents_from_bucket.return_value = {
            'body': results_file,
            'internal_id': 'internal_id'
        }

        get_secret_by_key_mock.side_effect = Exception("secret not found")

        message = f'''{{"from_mailbox": "{FROM_MAILBOX_ID}", "to_mailbox": "{TO_MAILBOX_ID}",
                        "workflow_id": "{WORKFLOW_ID}", "bucket_name": "{DATA_BUCKET}","object_key": "{FILE_KEY}"}}'''
        with self.assertRaises(Exception) as context:
            self.unwrapped_function({'Records': [{'body': message}]}, '')

        mesh_message_properties = {
            'from_mailbox': FROM_MAILBOX_ID,
            'to_mailbox': TO_MAILBOX_ID,
            'workflow_id': WORKFLOW_ID,
            'bucket_name': DATA_BUCKET,
            'object_key': FILE_KEY
        }

        self.assertEqual("secret not found", context.exception.args[0])

        get_file_contents_from_bucket.assert_called_once_with('sp-env-1234-data_bucket', FILE_KEY)
        tempfile_mock.TemporaryDirectory.return_value.__exit__.assert_called_once()
        log_mock.assert_has_calls([
            call
            (
                {'log_reference': LogReference.SENDMESHMESSAGE0001,
                 'mesh_message_properties': mesh_message_properties}
            ),
            call({'log_reference': LogReference.SENDMESHMESSAGE0002}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0006, 'add_exception_info': True})
        ])

    def test_message_send_fail_throws_exception(
        self,
        delete_object_mock,
        put_object_mock,
        get_mesh_client_mock,
        get_secret_by_key_mock,
        get_file_contents_from_bucket,
        log_mock
    ):
        mesh_client_fake = Mock()
        mesh_client_fake.send_message = Mock()
        mesh_client_fake.send_message.side_effect = Exception('cannot send message')
        get_mesh_client_mock.return_value = mesh_client_fake

        results_file = 'results'

        get_file_contents_from_bucket.return_value = {
            'body': results_file,
            'internal_id': 'internal_id'
        }

        message = f'''{{"from_mailbox": "{FROM_MAILBOX_ID}", "to_mailbox": "{TO_MAILBOX_ID}",
                        "workflow_id": "{WORKFLOW_ID}", "bucket_name": "{DATA_BUCKET}","object_key": "{FILE_KEY}"}}'''
        with self.assertRaises(Exception) as context:
            self.unwrapped_function({'Records': [{'body': message}]}, '')

        mesh_message_properties = {
            'from_mailbox': FROM_MAILBOX_ID,
            'to_mailbox': TO_MAILBOX_ID,
            'workflow_id': WORKFLOW_ID,
            'bucket_name': DATA_BUCKET,
            'object_key': FILE_KEY
        }

        expected_exception_message = "cannot send message"

        self.assertEqual(expected_exception_message, context.exception.args[0])

        get_file_contents_from_bucket.assert_called_once_with('sp-env-1234-data_bucket', FILE_KEY)
        log_mock.assert_has_calls([
            call
            (
                {'log_reference': LogReference.SENDMESHMESSAGE0001,
                 'mesh_message_properties': mesh_message_properties}
            ),
            call({'log_reference': LogReference.SENDMESHMESSAGE0002}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0003}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0008, 'add_exception_info': True})
        ])

    def test_fails_to_copy_file_throws_exception(
        self,
        delete_object_mock,
        put_object_mock,
        get_mesh_client_mock,
        get_secret_by_key_mock,
        get_file_contents_from_bucket,
        log_mock
    ):
        results_file = 'results'
        encoded_results_file = results_file.encode('utf-8')
        mesh_message_response = 'response'

        get_file_contents_from_bucket.return_value = {
            'body': results_file,
            'internal_id': 'internal_id'
        }

        mesh_client_fake = Mock()
        mesh_client_fake.send_message = Mock()
        mesh_client_fake.send_message.return_value = mesh_message_response
        get_mesh_client_mock.return_value = mesh_client_fake

        put_object_mock.side_effect = Exception('did not copy file')

        message = f'''{{"from_mailbox": "{FROM_MAILBOX_ID}", "to_mailbox": "{TO_MAILBOX_ID}",
                        "workflow_id": "{WORKFLOW_ID}", "bucket_name": "{DATA_BUCKET}","object_key": "{FILE_KEY}"}}'''

        with self.assertRaises(Exception) as context:
            self.unwrapped_function({'Records': [{'body': message}]}, '')

        expected_exception_message = "did not copy file"

        self.assertEqual(expected_exception_message, context.exception.args[0])

        mesh_message_properties = {
            'from_mailbox': FROM_MAILBOX_ID,
            'to_mailbox': TO_MAILBOX_ID,
            'workflow_id': WORKFLOW_ID,
            'bucket_name': DATA_BUCKET,
            'object_key': FILE_KEY
        }

        get_file_contents_from_bucket.assert_called_once_with('sp-env-1234-data_bucket', FILE_KEY)
        mesh_client_fake.send_message.assert_called_once_with(
            TO_MAILBOX_ID, encoded_results_file, workflow_id=WORKFLOW_ID
        )
        log_mock.assert_has_calls([
            call
            (
                {'log_reference': LogReference.SENDMESHMESSAGE0001,
                 'mesh_message_properties': mesh_message_properties}
            ),
            call({'log_reference': LogReference.SENDMESHMESSAGE0002}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0003}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0007, 'id_response': mesh_message_response}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0009, 'target_bucket': 'completed_bucket'}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0011, 'add_exception_info': True})
        ])

    def test_fails_to_delete_file_throws_exception(
        self,
        delete_object_mock,
        put_object_mock,
        get_mesh_client_mock,
        get_secret_by_key_mock,
        get_file_contents_from_bucket,
        log_mock
    ):
        results_file = 'results'
        encoded_results_file = results_file.encode('utf-8')
        mesh_message_response = 'response'

        get_file_contents_from_bucket.return_value = {
            'body': results_file,
            'internal_id': 'internal_id'
        }

        mesh_client_fake = Mock()
        mesh_client_fake.send_message = Mock()
        mesh_client_fake.send_message.return_value = mesh_message_response
        get_mesh_client_mock.return_value = mesh_client_fake

        delete_object_mock.side_effect = Exception('did not delete file')

        message = f'''{{"from_mailbox": "{FROM_MAILBOX_ID}", "to_mailbox": "{TO_MAILBOX_ID}",
                        "workflow_id": "{WORKFLOW_ID}", "bucket_name": "{DATA_BUCKET}","object_key": "{FILE_KEY}"}}'''

        with self.assertRaises(Exception) as context:
            self.unwrapped_function({'Records': [{'body': message}]}, '')

        expected_exception_message = "did not delete file"

        self.assertEqual(expected_exception_message, context.exception.args[0])

        mesh_message_properties = {
            'from_mailbox': FROM_MAILBOX_ID,
            'to_mailbox': TO_MAILBOX_ID,
            'workflow_id': WORKFLOW_ID,
            'bucket_name': DATA_BUCKET,
            'object_key': FILE_KEY
        }

        get_file_contents_from_bucket.assert_called_once_with('sp-env-1234-data_bucket', FILE_KEY)
        mesh_client_fake.send_message.assert_called_once_with(
            TO_MAILBOX_ID, encoded_results_file, workflow_id=WORKFLOW_ID
        )
        log_mock.assert_has_calls([
            call
            (
                {'log_reference': LogReference.SENDMESHMESSAGE0001,
                 'mesh_message_properties': mesh_message_properties}
            ),
            call({'log_reference': LogReference.SENDMESHMESSAGE0002}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0003}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0007, 'id_response': mesh_message_response}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0009, 'target_bucket': 'completed_bucket'}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0010}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0012, 'target_bucket': 'sp-env-1234-data_bucket'}),
            call({'log_reference': LogReference.SENDMESHMESSAGE0014, 'add_exception_info': True})
        ])
