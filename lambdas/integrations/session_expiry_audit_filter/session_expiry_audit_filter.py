from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.audit_utils import AuditActions, audit
from common.log import log
from session_expiry_audit_filter.log_references import LogReference
from datetime import datetime, timezone


@lambda_entry_point
def lambda_handler(event, context):
    records = event['Records']
    for record in records:
        if not sender_is_ttl_expiry(record):
            log({'log_reference': LogReference.SESSIONFILTER0003})
            continue

        dynamodb_record = record['dynamodb']['OldImage']
        if not dynamodb_record.get('user_data'):
            log({'log_reference': LogReference.SESSIONFILTER0002})
            continue

        user_data = dynamodb_record['user_data']['M']
        session = {
            'session_id': dynamodb_record['session_id']['S'],
            'user_data': {
                'nhsid_useruid': user_data['nhsid_useruid']['S']
            }
        }

        selected_role = dynamodb_record.get('selected_role', {}).get('M')
        if selected_role:
            session['selected_role'] = {
                'role_id': selected_role.get('role_id', {}).get('S'),
                'organisation_code': selected_role.get('organisation_code', {}).get('S'),
            }

        audit(session=session, action=AuditActions.SESSION_TIMEOUT, timestamp=get_timestamp(dynamodb_record))
        log({'log_reference': LogReference.SESSIONFILTER0001})


def sender_is_ttl_expiry(record):
    sender = record.get('userIdentity')
    if sender and sender['type'] == 'Service' and sender['principalId'] == 'dynamodb.amazonaws.com':
        return True
    return False


def get_timestamp(record):
    expiry_time = record['expires']['N']
    return datetime.utcfromtimestamp(int(expiry_time)).astimezone(timezone.utc).isoformat(timespec='microseconds')
