from unittest import TestCase
from session_expiry_audit_filter.session_expiry_audit_filter import lambda_handler, sender_is_ttl_expiry, get_timestamp
from common.utils.audit_utils import AuditActions
from unittest.mock import patch, Mock

iso_timestamp = '2020-03-13T17:02:34.000000+00:00'


@patch('session_expiry_audit_filter.session_expiry_audit_filter.audit')
@patch('session_expiry_audit_filter.session_expiry_audit_filter.log', Mock())
class TestSessionExpiryAuditFilter(TestCase):

    def setUp(self):
        self.handler = lambda_handler.__wrapped__
        self.event = {'Records': [{
            'dynamodb': {'OldImage': {
                'session_id': {'S': 'session id'},
                'user_data': {'M': {
                    'nhsid_useruid': {'S': 'user id'}
                }}
            }}
        }]}
        self.user_identity = {
            'userIdentity': {
                'type': 'Service',
                'principalId': 'dynamodb.amazonaws.com'
            }
        }
        self.timestamp = {
            'expires': {'N': 1584118954}
        }
        self.session = {
            'session_id': 'session id',
            'user_data': {
                'nhsid_useruid': 'user id'
            }
        }
        self.action = AuditActions.SESSION_TIMEOUT

    def test_sender_is_ttl_expiry_returns_true_when_userIdentity_correct(self, audit_mock):
        user_identity = self.user_identity
        assert sender_is_ttl_expiry(user_identity) is True

    def test_sender_is_ttl_expiry_returns_false_when_no_userIdentity_provided(self, audit_mock):
        assert sender_is_ttl_expiry({}) is False

    def test_sender_is_ttl_expiry_returns_false_when_type_incorrect(self, audit_mock):
        user_identity = self.user_identity
        user_identity['userIdentity']['type'] = 'blah'
        assert sender_is_ttl_expiry(user_identity) is False

    def test_sender_is_ttl_expiry_returns_false_when_pricipalId_incorrect(self, audit_mock):
        user_identity = self.user_identity
        user_identity['userIdentity']['principalId'] = 'blah'
        assert sender_is_ttl_expiry(user_identity) is False

    def test_get_timestamp_correctly_formats_numeric_epoch_timestamp(self, audit_mock):
        assert get_timestamp(self.timestamp) == iso_timestamp

    def test_get_timestamp_correctly_formats_stringified_epoch_timestamp(self, audit_mock):
        timestamp = {
            'expires': {'N': '1584118954'}
        }
        assert get_timestamp(timestamp) == iso_timestamp

    @patch('session_expiry_audit_filter.session_expiry_audit_filter.sender_is_ttl_expiry', Mock(return_value=True))
    @patch('session_expiry_audit_filter.session_expiry_audit_filter.get_timestamp', Mock(return_value=iso_timestamp))
    def test_expiry_event_passes_correct_fields_to_audit(self, audit_mock):
        self.handler(self.event, {})
        audit_mock.assert_called_once_with(session=self.session, action=self.action, timestamp=iso_timestamp)

    def test_expiry_event_passes_correct_fields_to_audit_unmocked(self, audit_mock):
        event = self.event
        event['Records'][0].update(self.user_identity)
        event['Records'][0]['dynamodb']['OldImage'].update(self.timestamp)
        self.handler(self.event, {})
        audit_mock.assert_called_once_with(session=self.session, action=self.action, timestamp=iso_timestamp)

    @patch('session_expiry_audit_filter.session_expiry_audit_filter.sender_is_ttl_expiry', Mock(return_value=True))
    @patch('session_expiry_audit_filter.session_expiry_audit_filter.get_timestamp', Mock(return_value=iso_timestamp))
    def test_multiple_expiry_records_are_all_passed_to_audit(self, audit_mock):
        event = {'Records': [
            self.event['Records'][0], self.event['Records'][0]
        ]}
        self.handler(event, {})
        self.assertEqual(audit_mock.call_count, 2)

    @patch('session_expiry_audit_filter.session_expiry_audit_filter.sender_is_ttl_expiry', Mock(return_value=True))
    @patch('session_expiry_audit_filter.session_expiry_audit_filter.get_timestamp', Mock(return_value=iso_timestamp))
    def test_expiry_event_with_selected_role_passes_correct_fields_to_audit(self, audit_mock):
        event = self.event
        event['Records'][0]['dynamodb']['OldImage']['selected_role'] = {'M': {
            'role_id': {'S': 'role id'}, 'organisation_code': {'S': 'org code'}
        }}
        session = self.session
        session['selected_role'] = {'role_id': 'role id', 'organisation_code': 'org code'}
        self.handler(event, {})
        audit_mock.assert_called_once_with(session=session, action=self.action, timestamp=iso_timestamp)

    @patch('session_expiry_audit_filter.session_expiry_audit_filter.sender_is_ttl_expiry', Mock(return_value=True))
    @patch('session_expiry_audit_filter.session_expiry_audit_filter.get_timestamp', Mock(return_value=iso_timestamp))
    def test_expiry_event_with_no_user_data_is_ignored(self, audit_mock):
        event = self.event
        event['Records'][0]['dynamodb']['OldImage']['user_data'] = {}
        audit_mock.assert_not_called()

    @patch('session_expiry_audit_filter.session_expiry_audit_filter.sender_is_ttl_expiry', Mock(return_value=False))
    @patch('session_expiry_audit_filter.session_expiry_audit_filter.get_timestamp', Mock(return_value=iso_timestamp))
    def test_event_with_incorrect_userIdentity_is_ignored(self, audit_mock):
        audit_mock.assert_not_called()
