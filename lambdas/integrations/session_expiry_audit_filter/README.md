This lambda processes events in the stream from the sessions dynamodb table.
If the event is a session expiry, the lambda forwards the information to the audit lambda via an audit SQS.