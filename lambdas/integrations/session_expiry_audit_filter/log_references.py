import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    SESSIONFILTER0001 = (logging.INFO, 'Audit added to queue for expiry event.')
    SESSIONFILTER0002 = (logging.INFO, 'No event added to queue for expiry of session with no user data.')
    SESSIONFILTER0003 = (logging.INFO, 'No event added to queue for non-expiry event.')
