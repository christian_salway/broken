import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):

    MSHCHK0001 = (logging.INFO, 'Received request to check MESH mailbox')
    MSHCHK0002 = (logging.INFO, 'Rule name determined from ARN')
    MSHCHK0003 = (logging.INFO, 'Mailbox Credentials successfully obtained from AWS')
    MSHCHK0004 = (logging.INFO, 'Mailbox has unacknowledged messages. Closing mesh client')
    MSHCHK0006 = (logging.INFO, 'Mailbox has no unacknowledged messages. Closing mesh client')
    MSHCHK0007 = (logging.INFO, 'Checking mailbox for unacknowledged messages')
    MSHCHK0008 = (logging.INFO, 'Closed mesh client')
    MSHCHK0009 = (logging.INFO, 'Sending message to pending_mesh_downloads SQS queue')
    MSHCHKERR0001 = (logging.ERROR, 'Failed to get Mailbox Credentials from AWS')
    MSHCHKERR0002 = (logging.ERROR, 'Rule ARN is not in the correct format')
