from unittest.case import TestCase
from mock import patch, call, Mock, MagicMock
from ddt import ddt, data, unpack
from common.exceptions import (
    MeshCredentialsNotFoundException, InvalidEventRuleException)
from check_mesh_mailbox.log_references import LogReference
with patch('boto3.resource'):
    import check_mesh_mailbox.check_mesh_mailbox as environment
    from check_mesh_mailbox.check_mesh_mailbox import (
        lambda_handler,
        extract_mailbox_secret_key,
        get_mailbox_credentials_using_event_rule,
        get_pending_messages_list,
        extract_cloudwatch_event_arn_from_event,
        extract_cloudwatch_event_rule_from_arn,
        send_mesh_messages_to_sqs)

environment.MESH_HOST = 'the_host'
environment.MESH_MESSAGE_SQS_QUEUE_URL = 'the_queue_url'
environment.ENVIRONMENT_NAME = 'test'
environment.AWS_ACCOUNT_ID = '123'
module = 'check_mesh_mailbox.check_mesh_mailbox'


class TestCheckMeshMailboxLambdaHandler(TestCase):

    def setUp(self):
        self.log_patcher = patch(f'{module}.log', MagicMock()).start()
        self.unwrapped_handler = lambda_handler.__wrapped__

    @patch(f'{module}.send_mesh_messages_to_sqs')
    @patch(f'{module}.get_pending_messages_list')
    @patch(f'{module}.get_mailbox_credentials_using_event_rule')
    @patch(f'{module}.extract_cloudwatch_event_rule_from_arn')
    @patch(f'{module}.extract_cloudwatch_event_arn_from_event')
    def test_lambda_handler_does_not_send_any_sqs_messages_if_no_messages_pending(
            self, get_arn_mock, get_rule_mock, get_creds_mock, get_messages_mock, send_message_mock):
        event = {'Records': [{'Sns': {'Message': '{"resources": ["the_event_arn"]}'}}]}
        get_arn_mock.return_value = 'the_arn'
        get_rule_mock.return_value = 'the_rule'
        get_creds_mock.return_value = {'mailbox_id': 'the_mailbox_id', 'password': 'the_password'}
        get_messages_mock.return_value = []

        expected_response = {'statusCode': 200}

        actual_response = self.unwrapped_handler(event, {})

        self.assertDictEqual(expected_response, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.MSHCHK0001})]
        self.log_patcher.assert_has_calls(expected_log_calls)

        get_arn_mock.assert_called_with(event)
        get_rule_mock.assert_called_with('the_arn')
        get_creds_mock.assert_called_with('the_rule')
        get_messages_mock.assert_called_with('the_mailbox_id', 'the_password')
        send_message_mock.assert_not_called()

    @patch(f'{module}.send_mesh_messages_to_sqs')
    @patch(f'{module}.get_pending_messages_list')
    @patch(f'{module}.get_mailbox_credentials_using_event_rule')
    @patch(f'{module}.extract_cloudwatch_event_rule_from_arn')
    @patch(f'{module}.extract_cloudwatch_event_arn_from_event')
    def test_lambda_handler_sends_sqs_message_for_each_pending_message(
            self, get_arn_mock, get_rule_mock, get_creds_mock, get_messages_mock, send_message_mock):
        event = {'Records': [{'Sns': {'Message': '{"resources": ["the_event_arn"]}'}}]}
        get_arn_mock.return_value = 'the_arn'
        get_rule_mock.return_value = 'the_rule'
        get_creds_mock.return_value = {'mailbox_id': 'the_mailbox_id', 'password': 'the_password'}
        get_messages_mock.return_value = ['message_1', 'message_2']

        expected_response = {'statusCode': 200}

        actual_response = self.unwrapped_handler(event, {})

        self.assertDictEqual(expected_response, actual_response)

        expected_log_calls = [call({'log_reference': LogReference.MSHCHK0001})]
        self.log_patcher.assert_has_calls(expected_log_calls)

        get_arn_mock.assert_called_with(event)
        get_rule_mock.assert_called_with('the_arn')
        get_creds_mock.assert_called_with('the_rule')
        get_messages_mock.assert_called_with('the_mailbox_id', 'the_password')
        send_message_mock.assert_called_with(['message_1', 'message_2'], 'the_rule')


@ddt
class TestCheckMeshMailboxHelperFunctions(TestCase):

    def setUp(self):
        self.log_patcher = patch(f'{module}.log', MagicMock()).start()

    @patch(f'{module}.get_secret_by_key')
    @patch(f'{module}.MESH_ENVIRONMENT', 'stub')
    @patch(f'{module}.ENVIRONMENT_NAME', 'sp-123')
    def test_get_mailbox_credentials_using_event_rule_throws_exception_if_no_credentials_found(
            self, get_secret_by_key_mock):
        cloudwatch_event_rule = 'the_event_rule'
        get_secret_by_key_mock.return_value = ''

        with self.assertRaises(MeshCredentialsNotFoundException):
            get_mailbox_credentials_using_event_rule(cloudwatch_event_rule)

        expected_log_calls = [call({'log_reference': LogReference.MSHCHKERR0001})]
        self.log_patcher.assert_has_calls(expected_log_calls)
        get_secret_by_key_mock.assert_called_with('mesh_mailbox_credentials', cloudwatch_event_rule)

    @patch(f'{module}.get_secret_by_key')
    @patch(f'{module}.MESH_ENVIRONMENT', 'stub')
    @patch(f'{module}.ENVIRONMENT_NAME', 'sp-123')
    def test_get_mailbox_credentials_using_event_rule_returns_credentials(self, get_secret_by_key_mock):
        cloudwatch_event_rule = 'the_event_rule'
        get_secret_by_key_mock.return_value = '{"key_1": "value_1", "key_2": "value_2"}'
        expected_credentials = {'key_1': 'value_1', 'key_2': 'value_2'}

        actual_credentials = get_mailbox_credentials_using_event_rule(cloudwatch_event_rule)

        self.assertDictEqual(expected_credentials, actual_credentials)

        expected_log_calls = [call({'log_reference': LogReference.MSHCHK0003})]

        self.log_patcher.assert_has_calls(expected_log_calls)
        get_secret_by_key_mock.assert_called_with('mesh_mailbox_credentials', cloudwatch_event_rule)

    @unpack
    @data(('the_mailbox_id', 'the_mailbox_id'),
          ('[ENV]', f'{environment.AWS_ACCOUNT_ID}-{environment.ENVIRONMENT_NAME}'))
    @patch(f'{module}.get_mesh_client')
    @patch(f'{module}.tempfile')
    def test_get_pending_messages_list_uses_mailbox_id_for_environment_if_required(
            self, secret_mailbox_id, expected_mailbox_id,
            tempdir_mock, get_mesh_client_mock):

        tempdir_mock.TemporaryDirectory.return_value.__enter__.return_value = 'the_temp_dir_name'

        mailbox_id = secret_mailbox_id
        password = 'the_password'
        mesh_client = Mock()
        mesh_client.list_messages.return_value = []
        get_mesh_client_mock.return_value.__enter__.return_value = mesh_client

        get_pending_messages_list(mailbox_id, password)

        get_mesh_client_mock.assert_called_with(expected_mailbox_id, password, 'the_temp_dir_name')

    @patch(f'{module}.get_mesh_client')
    @patch(f'{module}.tempfile')
    def test_get_pending_messages_list_logs_if_there_are_no_pending_messages(
            self, tempdir_mock, get_mesh_client_mock):

        tempdir_mock.TemporaryDirectory.return_value.__enter__.return_value = 'the_temp_dir_name'

        mailbox_id = 'the_mailbox_id'
        password = 'the_password'

        mesh_client = Mock()
        mesh_client.list_messages.return_value = []
        get_mesh_client_mock.return_value.__enter__.return_value = mesh_client

        actual_messages = get_pending_messages_list(mailbox_id, password)

        self.assertEqual([], actual_messages)

        get_mesh_client_mock.assert_called_with(mailbox_id, password, 'the_temp_dir_name')
        mesh_client.list_messages.assert_called_once()
        tempdir_mock.TemporaryDirectory.return_value.__exit__.assert_called_once()
        mesh_client.close.assert_called_once()

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.MSHCHK0007, 'mailbox_id': 'the_mailbox_id'}),
            call({'log_reference': LogReference.MSHCHK0006}),
            call({'log_reference': LogReference.MSHCHK0008})])

    @patch(f'{module}.get_mesh_client')
    @patch(f'{module}.tempfile')
    def test_get_pending_messages_list_logs_if_there_are_pending_messages(
            self, tempdir_mock, get_mesh_client_mock):

        tempdir_mock.TemporaryDirectory.return_value.__enter__.return_value = 'the_temp_dir_name'

        mailbox_id = 'the_mailbox_id'
        password = 'the_password'
        mesh_client = Mock()
        mesh_client.list_messages.return_value = ['a_message']
        get_mesh_client_mock.return_value.__enter__.return_value = mesh_client

        actual_messages = get_pending_messages_list(mailbox_id, password)

        self.assertEqual(['a_message'], actual_messages)

        get_mesh_client_mock.assert_called_with(mailbox_id, password, 'the_temp_dir_name')
        mesh_client.list_messages.assert_called_once()
        tempdir_mock.TemporaryDirectory.return_value.__exit__.assert_called_once()
        mesh_client.close.assert_called_once()

        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.MSHCHK0007, 'mailbox_id': 'the_mailbox_id'}),
            call({'log_reference': LogReference.MSHCHK0004, 'number_of_messages': 1}),
            call({'log_reference': LogReference.MSHCHK0008})])

    def test_extract_cloudwatch_event_arn_from_event_returns_arn(self):
        event = {'Records': [{'Sns': {'Message': '{"resources": ["the_event_arn"]}'}}]}
        expected_arn = "the_event_arn"
        actual_arn = extract_cloudwatch_event_arn_from_event(event)
        self.assertEqual(expected_arn, actual_arn)

    def test_successful_extract_cloudwatch_event_rule_from_arn(self):
        event_rule = extract_cloudwatch_event_rule_from_arn('arn:aws:events:eu-west-2:092130162833:rule/a_new_rule')
        self.assertEqual("a_new_rule", event_rule)
        expected_log_calls = [call({'log_reference': LogReference.MSHCHK0002, 'rule_name': 'a_new_rule'})]
        self.log_patcher.assert_has_calls(expected_log_calls)

    def test_failed_extract_cloudwatch_event_rule_from_arn_when_rule_name_empty(self):
        self.assertRaises(
            InvalidEventRuleException, extract_cloudwatch_event_rule_from_arn,
            'arn:aws:events:eu-west-2:092130162833:rule/'
        )
        expected_log_calls = [call({'log_reference': LogReference.MSHCHKERR0002,
                                    'arn': 'arn:aws:events:eu-west-2:092130162833:rule/'})]
        self.log_patcher.assert_has_calls(expected_log_calls)

    def test_failed_extract_cloudwatch_event_rule_from_arn_when_rule_name_missing(self):
        self.assertRaises(
            InvalidEventRuleException, extract_cloudwatch_event_rule_from_arn,
            'arn:aws:events:eu-west-2:092130162833:stuff'
        )
        expected_log_calls = [call({'log_reference': LogReference.MSHCHKERR0002,
                                    'arn': 'arn:aws:events:eu-west-2:092130162833:stuff'})]
        self.log_patcher.assert_has_calls(expected_log_calls)

    def test_failed_extract_cloudwatch_event_rule_from_arn_when_arn_format_invalid(self):
        self.assertRaises(
            InvalidEventRuleException, extract_cloudwatch_event_rule_from_arn,
            'invalid_arn'
        )
        expected_log_calls = [call({'log_reference': LogReference.MSHCHKERR0002, 'arn': 'invalid_arn'})]
        self.log_patcher.assert_has_calls(expected_log_calls)

    @patch(f'{module}.boto3')
    @patch(f'{module}.get_internal_id', Mock(return_value='internal_id'))
    def test_send_mesh_messages_to_sqs_sends_to_sqs_in_batches(self, boto3_mock):
        sqs_results = {'Success': [], 'Failed': []}
        mock_sqs = Mock()
        mock_sqs.send_message_batch.return_value = sqs_results
        messages = [f'message_{i}' for i in range(11)]
        cloudwatch_event_rule = 'the_rule'
        boto3_mock.client.return_value = mock_sqs

        send_mesh_messages_to_sqs(messages, cloudwatch_event_rule)

        expected_calls = [
            call(Entries=[
                {'Id': 'message_0', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_0"}'},
                {'Id': 'message_1', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_1"}'},
                {'Id': 'message_2', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_2"}'},
                {'Id': 'message_3', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_3"}'},
                {'Id': 'message_4', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_4"}'},
                {'Id': 'message_5', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_5"}'},
                {'Id': 'message_6', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_6"}'},
                {'Id': 'message_7', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_7"}'},
                {'Id': 'message_8', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_8"}'},
                {'Id': 'message_9', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_9"}'}
            ],
                QueueUrl='the_queue_url'),
            call(Entries=[
                {'Id': 'message_10', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_10"}'}
            ],
                QueueUrl='the_queue_url'
            )
        ]

        actual_calls = mock_sqs.send_message_batch.call_args_list

        self.assertEqual(expected_calls, actual_calls)

    @patch(f'{module}.boto3')
    @patch(f'{module}.get_internal_id', Mock(return_value='internal_id'))
    def test_send_mesh_messages_to_sqs_throws_exception_if_sending_fails(self, boto3_mock):
        sqs_results = {'Success': [], 'Failed': ['message_1_failed']}
        mock_sqs = Mock()
        mock_sqs.send_message_batch.return_value = sqs_results
        messages = ['message_1']
        cloudwatch_event_rule = 'the_rule'
        boto3_mock.client.return_value = mock_sqs

        with self.assertRaises(Exception) as context:
            send_mesh_messages_to_sqs(messages, cloudwatch_event_rule)

        self.assertEqual('Unable to send batch to SQS', context.exception.args[0])

        expected_calls = [
            call(Entries=[
                {'Id': 'message_1', 'MessageBody':
                    '{"internal_id": "internal_id", "cloudwatch_event_rule": "the_rule", "message_id": "message_1"}'}
            ], QueueUrl='the_queue_url')
        ]

        actual_calls = mock_sqs.send_message_batch.call_args_list

        self.assertEqual(expected_calls, actual_calls)
        self.log_patcher.assert_called_once_with({
            'log_reference': LogReference.MSHCHK0009,
            'message_id': 'message_1'
        })

    @patch(f'{module}.MESH_ENVIRONMENT', 'int')
    @patch(f'{module}.ENVIRONMENT_NAME', 'sp-123')
    def test_extract_mailbox_secret_key_returns_int_when_mesh_environment_is_set_to_int(
            self):
        event_rule = 'sp-123-a-rule'
        parsed_key = extract_mailbox_secret_key(event_rule)
        self.assertEqual(parsed_key, 'int-a-rule')

    @patch(f'{module}.MESH_ENVIRONMENT', 'stub')
    @patch(f'{module}.ENVIRONMENT_NAME', 'sp-123')
    def test_extract_mailbox_secret_key_returns_default_when_mesh_environment_is_set_to_stub(
            self):
        event_rule = 'sp-123-a-rule'
        parsed_key = extract_mailbox_secret_key(event_rule)
        self.assertEqual(parsed_key, 'stub-a-rule')
