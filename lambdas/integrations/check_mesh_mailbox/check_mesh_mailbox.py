import json
import os
import tempfile

import boto3
from botocore.client import Config

from common.exceptions import (
    MeshCredentialsNotFoundException, InvalidEventRuleException)
from common.log import log, get_internal_id
from common.utils.mesh.mesh_client_utils import get_mesh_client
from common.utils import chunk_list
from common.utils.lambda_wrappers import lambda_entry_point
from common.utils.secrets_manager_utils import get_secret_by_key
from check_mesh_mailbox.log_references import LogReference

BATCH_SIZE = 10
ENVIRONMENT_NAME = os.environ.get('ENVIRONMENT_NAME')
AWS_ACCOUNT_ID = os.environ.get('AWS_ACCOUNT_ID')
MESH_HOST = os.environ.get('MESH_HOST')
MESH_ENVIRONMENT = os.environ.get('MESH_ENVIRONMENT')
MESH_MESSAGE_SQS_QUEUE_URL = os.environ.get('MESH_MESSAGE_SQS_QUEUE_URL')


@lambda_entry_point
def lambda_handler(event, context):
    log({'log_reference': LogReference.MSHCHK0001})

    cloudwatch_event_arn = extract_cloudwatch_event_arn_from_event(event)
    cloudwatch_event_rule = extract_cloudwatch_event_rule_from_arn(cloudwatch_event_arn)
    mailbox_creds = get_mailbox_credentials_using_event_rule(cloudwatch_event_rule)

    messages_list = get_pending_messages_list(mailbox_creds['mailbox_id'], mailbox_creds['password'])

    if messages_list:
        send_mesh_messages_to_sqs(messages_list, cloudwatch_event_rule)

    return {'statusCode': 200}


def get_mailbox_credentials_using_event_rule(cloudwatch_event_rule):
    mailbox_creds = get_secret_by_key('mesh_mailbox_credentials', extract_mailbox_secret_key(cloudwatch_event_rule))

    if (mailbox_creds):
        log({'log_reference': LogReference.MSHCHK0003})
    else:
        log({'log_reference': LogReference.MSHCHKERR0001})
        raise MeshCredentialsNotFoundException()

    return json.loads(mailbox_creds)


def get_pending_messages_list(mailbox_id, password):
    if mailbox_id == "[ENV]":
        mailbox_id = f'{AWS_ACCOUNT_ID}-{ENVIRONMENT_NAME}'
    with tempfile.TemporaryDirectory() as temp_dir_name:
        with get_mesh_client(mailbox_id, password, temp_dir_name) as mesh_client:
            log({'log_reference': LogReference.MSHCHK0007, 'mailbox_id': mailbox_id})
            messages_list = mesh_client.list_messages()
            if (messages_list):
                log({'log_reference': LogReference.MSHCHK0004, 'number_of_messages': len(messages_list)})
            else:
                log({'log_reference': LogReference.MSHCHK0006})
            mesh_client.close()

        log({'log_reference': LogReference.MSHCHK0008})

    return messages_list


def extract_cloudwatch_event_arn_from_event(event):
    sns = event['Records'][0]['Sns']
    message = json.loads(sns['Message'])
    return message['resources'][0]


def extract_cloudwatch_event_rule_from_arn(arn):
    arn_sections = arn.split(':')

    if(len(arn_sections) != 6):
        log({'log_reference': LogReference.MSHCHKERR0002, 'arn': arn})
        raise InvalidEventRuleException()

    rule_name_section = arn_sections[5]  # index of rule name section within the ARN

    if not rule_name_section.startswith('rule/'):
        log({'log_reference': LogReference.MSHCHKERR0002, 'arn': arn})
        raise InvalidEventRuleException()

    rule_name = rule_name_section.replace('rule/', '')

    if rule_name:
        log({'log_reference': LogReference.MSHCHK0002, 'rule_name': rule_name})
        return rule_name
    else:
        log({'log_reference': LogReference.MSHCHKERR0002, 'arn': arn})
        raise InvalidEventRuleException()


def send_mesh_messages_to_sqs(messages_list, cloudwatch_event_rule):
    client = boto3.client('sqs', config=Config(retries={'max_attempts': 3}))
    internal_id = get_internal_id()
    messages_chunks = chunk_list(messages_list, BATCH_SIZE)

    for chunk in messages_chunks:

        entries = []
        for message in chunk:
            message_to_send = {
                'internal_id': internal_id,
                'cloudwatch_event_rule': cloudwatch_event_rule,
                'message_id': message
            }
            formatted_message_to_send = {
                'Id': message,
                'MessageBody': json.dumps(message_to_send)
            }
            log({'log_reference': LogReference.MSHCHK0009, 'message_id': message})
            entries.append(formatted_message_to_send)

        responses = client.send_message_batch(
            QueueUrl=MESH_MESSAGE_SQS_QUEUE_URL,
            Entries=entries
        )

        if responses.get('Failed', []):
            raise Exception('Unable to send batch to SQS')


def extract_mailbox_secret_key(cloudwatch_event_rule):
    return cloudwatch_event_rule.replace(ENVIRONMENT_NAME, MESH_ENVIRONMENT)
