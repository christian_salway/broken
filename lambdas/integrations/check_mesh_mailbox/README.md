Lambda, that checks MESH mailboxes for waiting messages, and sends an SQS message related to each waiting message.

[JIRA](https://nhsd-jira.digital.nhs.uk/browse/SP-560)
[Confluence](https://nhsd-confluence.digital.nhs.uk/display/CSP/MESH+Receive+Processing#MESHReceiveProcessing-MailboxcheckLambda)
