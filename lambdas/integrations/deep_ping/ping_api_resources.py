import boto3
import requests
import uuid
from common.utils.deep_ping import DEEP_PING_PREFIX
from common.log import log
from deep_ping.log_references import LogReference
from time import time
from common.utils.dynamodb_access.operations import dynamodb_put_item, dynamodb_delete_item


def ping_api_resources(environment_name, environment_domain):
    test_user = build_test_user()
    # create a deep ping session
    dynamodb_put_item(f'{environment_name}-sessions', test_user)
    log({'log_reference': LogReference.DP0002})
    resources = enumerate_endpoints(environment_name)
    ping_endpoints(environment_domain, resources, test_user['session_token'])
    # deletes the above deep ping session
    dynamodb_delete_item(f'{environment_name}-sessions', {'session_token': test_user['session_token']})


def build_test_user():
    current_time_in_seconds = int(time())
    ten_minutes_in_seconds = 600

    session_id = str(uuid.uuid4())
    session_token = str(uuid.uuid4())
    expires = current_time_in_seconds + ten_minutes_in_seconds

    return {
        "session_token": session_token,
        "nhsid_subject_id": "deep_ping",
        "expires": expires,
        "reauthenticate_timestamp": expires,
        "selected_role": {
            "role_name": "deep_ping",
            "role_id": "deep_ping"
        },
        "user_data": {
            "nhsid_useruid": "deep_ping",
        },
        "session_id": f'{DEEP_PING_PREFIX}_{session_id}',
        "nhsid_session_id": "deep_ping",
        "is_current_session": True,
        "nhsid_useruid": "deep_ping"
    }


def get_api_for_environment(apis, environment_name):
    for api in apis:
        if api.get('tags', {}).get('environment_name') == environment_name:
            return api


def ping_endpoints(domain_url, api_endpoints, session_token):
    ignore_list = ['/static/media/{item+}']
    for endpoint in api_endpoints:
        path = endpoint["path"]
        if path in ignore_list:
            continue
        url = f'https://{domain_url}{path}'
        methods = endpoint.get('resourceMethods', {})
        for method in methods.keys():
            if method == 'OPTIONS':
                continue
            ping(url, method, session_token)


def ping(url, method, session_token):
    allowed_methods = ['GET', 'POST', 'PUT', 'DELETE', 'PATCH']
    if method not in allowed_methods:
        log({'log_reference': LogReference.DP0011, 'method': method})
        return
    uuid4 = str(uuid.uuid4())
    request_id = f'{DEEP_PING_PREFIX}_{uuid4}'
    headers = {
        'request_id': request_id,
        'Cookie': f'sp_session_cookie="{session_token}"'
    }
    log({
        'log_reference': LogReference.DP0003,
        'url': url,
        'request_id': request_id
    })
    response = make_request(method, url, headers)
    log({
        'log_reference': LogReference.DP0004,
        'response_code': response.status_code,
        'request_id': request_id
    })


def make_request(method, url, headers):
    try:
        return getattr(requests, method.lower())(url=url, headers=headers, timeout=5)
    except Exception:
        log({'log_reference': LogReference.DP0012, 'add_exception_info': True})


def enumerate_endpoints(environment_name):
    log({'log_reference': LogReference.DP0005})
    client = boto3.client('apigateway')
    apis = client.get_rest_apis(limit=500).get('items', [])
    api = get_api_for_environment(apis, environment_name)
    if not api:
        raise Exception(f'Unable to find api gateway for environment {environment_name}')

    log({'log_reference': LogReference.DP0006, 'api_id': api["id"]})
    return client.get_resources(restApiId=api['id']).get('items', [])
