The purpose of this lambda is to run periodic checks against the API Gateway endpoints and backends lambdas in order to determine the health of the service by uncovering any deployment/configuration errors, or highlighting outages in areas of the system with lower traffic so that we are aware of them before any users of the system

This lambda is intended to be run immediately after deployment, and periodically after that

It creates a deep_ping user in the sessions table, assumes the deep_ping role, and hits each of the API endpoints for its environment with a request_id prefixed with 'deep_ping'

The lambdas that handle these requests will identify that these are deep ping requests and will handle them differently, so as to avoid returning data to the deep ping lambda, modifying production data, or creating unwanted noise in the logs/monitoring - see the lambda handler in common