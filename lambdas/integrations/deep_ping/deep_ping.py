import os
from common.utils.lambda_wrappers import lambda_entry_point
from common.log import log
from deep_ping.log_references import LogReference
from deep_ping.ping_api_resources import ping_api_resources
from deep_ping.ping_lambdas import ping_lambdas


@lambda_entry_point
def lambda_handler(event, context):
    config = get_config_from_environment()
    environment_name = config['environment_name']
    environment_domain = config['environment_domain']
    log({'log_reference': LogReference.DP0001})
    ping_api_resources(environment_name, environment_domain)
    ping_lambdas()


def get_config_from_environment():
    return {
        'environment_name': os.getenv('ENVIRONMENT_NAME'),
        'environment_domain': os.getenv('ENVIRONMENT_DOMAIN'),
    }
