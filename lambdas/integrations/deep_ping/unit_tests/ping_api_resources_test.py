from unittest.case import TestCase
from mock import patch, Mock, call, MagicMock
from common.test_mocks.mock_response import MockResponse
from deep_ping.log_references import LogReference

module = 'deep_ping.ping_api_resources'

endpoints = [{'id': '0rx6di', 'parentId': '3blimo', 'pathPart': 'post-match-validator', 'path': '/api/csas/results/{sending_lab}/check/{check_result_id}/post-match-validator', 'resourceMethods': {'OPTIONS': {}, 'POST': {}}}, {'id': '12iqni', 'parentId': 'b1ppcc', 'pathPart': 'backchannel-logout', 'path': '/auth/backchannel-logout', 'resourceMethods': {'POST': {}}}, {'id': '1ekgdp', 'parentId': 'xchf1x', 'pathPart': 'noaction', 'path': '/api/participants/{participant_id}/noaction', 'resourceMethods': {'OPTIONS': {}, 'PUT': {}}}, {'id': '36flya', 'parentId': 'a0lz48', 'pathPart': '{organisation_code}', 'path': '/api/organisation-supplemental/{organisation_code}', 'resourceMethods': {'GET': {}, 'OPTIONS': {}}}, {'id': '3blimo', 'parentId': '6hxmuh', 'pathPart': '{check_result_id}', 'path': '/api/csas/results/{sending_lab}/check/{check_result_id}'}, {'id': '3tlbsj', 'parentId': 'y9r9oa', 'pathPart': 'csas', 'path': '/api/csas'}, {'id': '3uxp5f', 'parentId': '94x8ed', 'pathPart': 'accept-legitimate-relations', 'path': '/api/users/accept-legitimate-relations', 'resourceMethods': {'OPTIONS': {}, 'POST': {}}}, {'id': '3we0to', 'parentId': 'xchf1x', 'pathPart': 'audit', 'path': '/api/participants/{participant_id}/audit', 'resourceMethods': {'GET': {}, 'OPTIONS': {}}}, {'id': '41z690', 'parentId': 'up7u5b', 'pathPart': 'media', 'path': '/static/media'}, {'id': '48ivym', 'parentId': 'xchf1x', 'pathPart': 'accept', 'path': '/api/participants/{participant_id}/accept', 'resourceMethods': {'OPTIONS': {}, 'PUT': {}}}, {'id': '65e463', 'parentId': 'sa9uoq', 'pathPart': '{process_result_id}', 'path': '/api/csas/results/{sending_lab}/process/{process_result_id}', 'resourceMethods': {'GET': {}}}, {'id': '6hxmuh', 'parentId': '9fmvvd', 'pathPart': 'check', 'path': '/api/csas/results/{sending_lab}/check'}, 
             {'id': '6ssduo', 'parentId': 'xchf1x', 'pathPart': 'results', 'path': '/api/participants/{participant_id}/results', 'resourceMethods': {'GET': {}, 'OPTIONS': {}, 'POST': {}}}, {'id': '6wbbnu', 'parentId': '41z690', 'pathPart': '{item+}', 'path': '/static/media/{item+}', 'resourceMethods': {'GET': {}}}, {'id': '7f8qfs', 'parentId': 'skyqi7', 'pathPart': '{participant_id}', 'path': '/api/gp-notifications/{participant_id}'}, {'id': '89jaz0', 'parentId': 'b1ppcc', 'pathPart': 'check-session', 'path': '/auth/check-session', 'resourceMethods': {'GET': {}}}, {'id': '94x8ed', 'parentId': 'y9r9oa', 'pathPart': 'users', 'path': '/api/users', 'resourceMethods': {'GET': {}, 'OPTIONS': {}}}, {'id': '9fmvvd', 'parentId': 'hpsplg', 'pathPart': '{sending_lab}', 'path': '/api/csas/results/{sending_lab}', 'resourceMethods': {'GET': {}}}, {'id': 'a0lz48', 'parentId': 'y9r9oa', 'pathPart': 'organisation-supplemental', 'path': '/api/organisation-supplemental', 'resourceMethods': {'OPTIONS': {}, 'POST': {}}}, {'id': 'b1ppcc', 'parentId': 'bpsyfmu70d', 'pathPart': 'auth', 'path': '/auth', 'resourceMethods': {'OPTIONS': {}}}, {'id': 'bpsyfmu70d', 'path': '/', 'resourceMethods': {'GET': {}}}, {'id': 'brq897', 'parentId': 'y9r9oa', 'pathPart': 'clientlogs', 'path': '/api/clientlogs', 'resourceMethods': {'OPTIONS': {}, 'POST': {}}}, {'id': 'ds13tw', 'parentId': 'xchf1x', 'pathPart': 'reinstate', 'path': '/api/participants/{participant_id}/reinstate', 'resourceMethods': {'GET': {}, 'OPTIONS': {}, 'PUT': {}}}, {'id': 'eyi53k', 'parentId': '7f8qfs', 'pathPart': '{sort_key}', 'path': '/api/gp-notifications/{participant_id}/{sort_key}'}, {'id': 'fwi0vh', 'parentId': 'b1ppcc', 'pathPart': 'logout', 'path': '/auth/logout', 'resourceMethods': {'GET': {}, 'OPTIONS': {}}}] 
invalid_method_endpoint = [{'id': '0rx6di', 'parentId': '3blimo', 'pathPart': 'path', 'path': '/api/path', 'resourceMethods': {'OPTIONS': {}, 'INVALID_METHOD': {}}}] 


class TestPingApiResources(TestCase):

    @patch('boto3.client', Mock())
    def setUp(self):
        self.environment_name = 'env_name'
        self.environment_domain = 'env_domain'

        self.create_patchers()
        self.mock_responses()
        from deep_ping.ping_api_resources import ping_api_resources
        self.ping_api_resources = ping_api_resources

    def tearDown(self):
        patch.stopall()

    def create_patchers(self):
        self.log_patcher = patch(f'{module}.log').start()
        self.dynamodb_put_item_mock = patch(f'{module}.dynamodb_put_item').start()
        self.dynamodb_delete_item_mock = patch(f'{module}.dynamodb_delete_item').start()
        self.enumerate_endpoints_patcher = patch(f'{module}.enumerate_endpoints').start()
        self.make_request_patcher = patch(f'{module}.make_request').start()
        self.uuid_patcher = patch(f'{module}.uuid').start()
        self.time_patcher = patch(f'{module}.time').start()

    def mock_responses(self):
        self.enumerate_endpoints_patcher.return_value = endpoints
        mock_response = MockResponse({})
        self.make_request_patcher.return_value = mock_response
        self.uuid_patcher.uuid4.return_value = 'uuid'
        self.time_patcher.return_value = '123456789'

    def act(self):
        self.ping_api_resources(self.environment_name, self.environment_domain)

    def test_calls_api_endpoints(self):
        expected_cookie = 'sp_session_cookie="uuid"'
        expected_request_id = 'deep_ping_uuid'
        expected_headers = {'request_id': expected_request_id, 'Cookie': expected_cookie}
        expected_session = {
            'session_token': 'uuid',
            'nhsid_subject_id': 'deep_ping',
            'expires': 123457389,
            'reauthenticate_timestamp': 123457389,
            'selected_role': {'role_name': 'deep_ping', 'role_id': 'deep_ping'},
            'user_data': {'nhsid_useruid': 'deep_ping'},
            'session_id': 'deep_ping_uuid',
            'nhsid_session_id': 'deep_ping',
            'is_current_session': True,
            'nhsid_useruid': 'deep_ping'
        }

        self.act()
        self.make_request_patcher.assert_has_calls([
            call(
                'POST', 'https://env_domain/api/csas/results/{sending_lab}/check/{check_result_id}/post-match-validator', expected_headers), 
            call('POST', 'https://env_domain/auth/backchannel-logout', expected_headers), 
            call('PUT', 'https://env_domain/api/participants/{participant_id}/noaction', expected_headers),
            call('GET', 'https://env_domain/api/organisation-supplemental/{organisation_code}', expected_headers),
            call('POST', 'https://env_domain/api/users/accept-legitimate-relations', expected_headers),
            call('GET', 'https://env_domain/api/participants/{participant_id}/audit', expected_headers),
            call('PUT', 'https://env_domain/api/participants/{participant_id}/accept', expected_headers),
            call(
                'GET', 'https://env_domain/api/csas/results/{sending_lab}/process/{process_result_id}', expected_headers), 
            call('GET', 'https://env_domain/api/participants/{participant_id}/results', expected_headers),
            call('POST', 'https://env_domain/api/participants/{participant_id}/results', expected_headers),
            call('GET', 'https://env_domain/auth/check-session', expected_headers),
            call('GET', 'https://env_domain/api/users', expected_headers),
            call('GET', 'https://env_domain/api/csas/results/{sending_lab}', expected_headers),
            call('POST', 'https://env_domain/api/organisation-supplemental', expected_headers),
            call('GET', 'https://env_domain/', expected_headers),
            call('POST', 'https://env_domain/api/clientlogs', expected_headers),
            call('GET', 'https://env_domain/api/participants/{participant_id}/reinstate', expected_headers),
            call('PUT', 'https://env_domain/api/participants/{participant_id}/reinstate', expected_headers),
            call('GET', 'https://env_domain/auth/logout', expected_headers)])

        self.dynamodb_put_item_mock.assert_called_once_with(f'{self.environment_name}-sessions', expected_session)
        self.dynamodb_delete_item_mock.assert_called_once_with(
            f'{self.environment_name}-sessions', {'session_token': 'uuid'})

    def test_logs_error_if_method_not_allowed(self):
        # Arrange
        self.enumerate_endpoints_patcher.return_value = invalid_method_endpoint
        # Act
        self.act()
        # Assert
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.DP0002}),
            call({'log_reference': LogReference.DP0011, 'method': 'INVALID_METHOD'})
        ])


class TestMakeRequest(TestCase):

    def setUp(self):
        self.requests_patcher = patch(f'{module}.requests').start()
        self.log_patcher = patch(f'{module}.log').start()

        from deep_ping.ping_api_resources import make_request
        self.make_request = make_request

    def test_logs_error_if_exception_thrown(self):
        # Arrange
        self.requests_patcher.get.side_effect = Exception('no')
        # Act
        self.make_request('get', 'url', {})
        # Assert
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.DP0012, 'add_exception_info': True})
        ])

    def test_makes_request(self):
        # Act
        self.make_request('get', 'url', {})
        # Assert
        self.requests_patcher.get.assert_called_with(url='url', headers={}, timeout=5)


class TestEnumerateEndpoints(TestCase):

    def setUp(self):
        self.mock_client = MagicMock()
        self.client_patcher = patch('boto3.client').start()
        self.log_patcher = patch(f'{module}.log').start()
        self.client_patcher.return_value = self.mock_client

        from deep_ping.ping_api_resources import enumerate_endpoints
        self.enumerate_endpoints = enumerate_endpoints

    def act(self):
        return self.enumerate_endpoints('environment_name')

    def test_fetches_resources_for_environment_api(self):
        # Arrange
        expected_response = ['resources']
        self.mock_client.get_rest_apis.return_value = {'items': [
            {'id': '08mnn61b33', 'tags': {'Name': 'WRONG_ENVIRONMENT', 'environment_name': 'WRONG_ENVIRONMENT'}},
            {'id': '86f5urlt1k', 'tags': {'Name': 'CORRECT_ENVIRONMENT', 'environment_name': 'environment_name'}},
            {'id': '15zq0j0kl1', 'tags': {'Name': 'WRONG_ENVIRONMENT', 'environment_name': 'WRONG_ENVIRONMENT_2'}},
        ]}
        self.mock_client.get_resources.return_value = {'items': expected_response}
        # Act
        response = self.act()
        # Assert
        self.assertEqual(expected_response, response)
        self.mock_client.get_resources.assert_called_once_with(restApiId='86f5urlt1k')

    def test_raises_exception_if_no_api_found(self):
        # Arrange
        expected_exception_message = 'Unable to find api gateway for environment environment_name'
        self.mock_client.get_rest_apis.return_value = {'items': [
            {'id': '08mnn61b33', 'tags': {'Name': 'WRONG_ENVIRONMENT', 'environment_name': 'WRONG_ENVIRONMENT'}},
            {'id': '15zq0j0kl1', 'tags': {'Name': 'WRONG_ENVIRONMENT', 'environment_name': 'WRONG_ENVIRONMENT_2'}},
        ]}
        with self.assertRaises(Exception) as context:
            # Act
            self.act()
        # Assert
        self.assertEqual(expected_exception_message, context.exception.args[0])
