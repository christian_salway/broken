from unittest.case import TestCase
from mock import patch, Mock

module = 'deep_ping.deep_ping'

endpoints = [{'id': '0rx6di', 'parentId': '3blimo', 'pathPart': 'post-match-validator', 'path': '/api/csas/results/{sending_lab}/check/{check_result_id}/post-match-validator', 'resourceMethods': {'OPTIONS': {}, 'POST': {}}}, {'id': '12iqni', 'parentId': 'b1ppcc', 'pathPart': 'backchannel-logout', 'path': '/auth/backchannel-logout', 'resourceMethods': {'POST': {}}}, {'id': '1ekgdp', 'parentId': 'xchf1x', 'pathPart': 'noaction', 'path': '/api/participants/{participant_id}/noaction', 'resourceMethods': {'OPTIONS': {}, 'PUT': {}}}, {'id': '36flya', 'parentId': 'a0lz48', 'pathPart': '{organisation_code}', 'path': '/api/organisation-supplemental/{organisation_code}', 'resourceMethods': {'GET': {}, 'OPTIONS': {}}}, {'id': '3blimo', 'parentId': '6hxmuh', 'pathPart': '{check_result_id}', 'path': '/api/csas/results/{sending_lab}/check/{check_result_id}'}, {'id': '3tlbsj', 'parentId': 'y9r9oa', 'pathPart': 'csas', 'path': '/api/csas'}, {'id': '3uxp5f', 'parentId': '94x8ed', 'pathPart': 'accept-legitimate-relations', 'path': '/api/users/accept-legitimate-relations', 'resourceMethods': {'OPTIONS': {}, 'POST': {}}}, {'id': '3we0to', 'parentId': 'xchf1x', 'pathPart': 'audit', 'path': '/api/participants/{participant_id}/audit', 'resourceMethods': {'GET': {}, 'OPTIONS': {}}}, {'id': '41z690', 'parentId': 'up7u5b', 'pathPart': 'media', 'path': '/static/media'}, {'id': '48ivym', 'parentId': 'xchf1x', 'pathPart': 'accept', 'path': '/api/participants/{participant_id}/accept', 'resourceMethods': {'OPTIONS': {}, 'PUT': {}}}, {'id': '65e463', 'parentId': 'sa9uoq', 'pathPart': '{process_result_id}', 'path': '/api/csas/results/{sending_lab}/process/{process_result_id}', 'resourceMethods': {'GET': {}}}, {'id': '6hxmuh', 'parentId': '9fmvvd', 'pathPart': 'check', 'path': '/api/csas/results/{sending_lab}/check'}, 
             {'id': '6ssduo', 'parentId': 'xchf1x', 'pathPart': 'results', 'path': '/api/participants/{participant_id}/results', 'resourceMethods': {'GET': {}, 'OPTIONS': {}, 'POST': {}}}, {'id': '6wbbnu', 'parentId': '41z690', 'pathPart': '{item+}', 'path': '/static/media/{item+}', 'resourceMethods': {'GET': {}}}, {'id': '7f8qfs', 'parentId': 'skyqi7', 'pathPart': '{participant_id}', 'path': '/api/gp-notifications/{participant_id}'}, {'id': '89jaz0', 'parentId': 'b1ppcc', 'pathPart': 'check-session', 'path': '/auth/check-session', 'resourceMethods': {'GET': {}}}, {'id': '94x8ed', 'parentId': 'y9r9oa', 'pathPart': 'users', 'path': '/api/users', 'resourceMethods': {'GET': {}, 'OPTIONS': {}}}, {'id': '9fmvvd', 'parentId': 'hpsplg', 'pathPart': '{sending_lab}', 'path': '/api/csas/results/{sending_lab}', 'resourceMethods': {'GET': {}}}, {'id': 'a0lz48', 'parentId': 'y9r9oa', 'pathPart': 'organisation-supplemental', 'path': '/api/organisation-supplemental', 'resourceMethods': {'OPTIONS': {}, 'POST': {}}}, {'id': 'b1ppcc', 'parentId': 'bpsyfmu70d', 'pathPart': 'auth', 'path': '/auth', 'resourceMethods': {'OPTIONS': {}}}, {'id': 'bpsyfmu70d', 'path': '/', 'resourceMethods': {'GET': {}}}, {'id': 'brq897', 'parentId': 'y9r9oa', 'pathPart': 'clientlogs', 'path': '/api/clientlogs', 'resourceMethods': {'OPTIONS': {}, 'POST': {}}}, {'id': 'ds13tw', 'parentId': 'xchf1x', 'pathPart': 'reinstate', 'path': '/api/participants/{participant_id}/reinstate', 'resourceMethods': {'GET': {}, 'OPTIONS': {}, 'PUT': {}}}, {'id': 'eyi53k', 'parentId': '7f8qfs', 'pathPart': '{sort_key}', 'path': '/api/gp-notifications/{participant_id}/{sort_key}'}, {'id': 'fwi0vh', 'parentId': 'b1ppcc', 'pathPart': 'logout', 'path': '/auth/logout', 'resourceMethods': {'GET': {}, 'OPTIONS': {}}}] 


class TestPingApiResources(TestCase):

    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    def setUp(self):
        self.event = {}
        context = Mock()
        context.function_name = ''
        self.context = context

        self.create_patchers()
        self.mock_responses()
        from deep_ping.deep_ping import lambda_handler
        self.lambda_handler = lambda_handler

    def tearDown(self):
        patch.stopall()

    def create_patchers(self):
        self.log_patcher = patch('common.log.log').start()
        self.get_config_from_environment_patcher = patch(f'{module}.get_config_from_environment').start()
        self.ping_api_resources = patch(f'{module}.ping_api_resources').start()
        self.ping_lambdas = patch(f'{module}.ping_lambdas').start()

    def mock_responses(self):
        self.get_config_from_environment_patcher.return_value = {
            'environment_name': 'env_name',
            'environment_domain': 'env_domain',
        }

    def act(self):
        self.lambda_handler(self.event, self.context)

    def test_pings_api_endpoints_and_lambdas(self):
        self.act()

        self.get_config_from_environment_patcher.assert_called_once()
        self.ping_api_resources.assert_called_once_with('env_name', 'env_domain')
        self.ping_lambdas.assert_called_once()
