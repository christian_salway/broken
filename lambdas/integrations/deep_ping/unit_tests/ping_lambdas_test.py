from unittest.case import TestCase
from deep_ping.log_references import LogReference
from mock import patch, Mock, call, MagicMock

module = 'deep_ping.ping_lambdas'


class TestPingLambdas(TestCase):

    @patch('boto3.resource', Mock())
    @patch('boto3.client', Mock())
    def setUp(self):
        self.create_patchers()
        self.mock_responses()
        from deep_ping.ping_lambdas import ping_lambdas
        self.ping_lambdas = ping_lambdas

    def tearDown(self):
        patch.stopall()

    def create_patchers(self):
        self.log_patcher = patch('deep_ping.ping_lambdas.log').start()
        self.get_lambda_targets_patcher = patch(f'{module}.get_lambda_targets').start()
        self.invoke_lambda_patcher = patch(f'{module}.invoke_lambda').start()

    def mock_responses(self):
        self.get_lambda_targets_patcher.return_value = [
            'target_lambda_1', 'target_lambda_2', 'target_lambda_3'
        ]

    def act(self):
        self.ping_lambdas()

    def test_invokes_lambdas(self):
        self.act()

        self.invoke_lambda_patcher.assert_has_calls([
            call('target_lambda_1'),
            call('target_lambda_2'),
            call('target_lambda_3')
        ])


class TestInvokeLambda(TestCase):

    @patch('boto3.resource', Mock())
    def setUp(self):
        self.mock_client = MagicMock()
        self.client_patcher = patch('boto3.client').start()
        self.log_patcher = patch('deep_ping.ping_lambdas.log').start()
        self.client_patcher.return_value = self.mock_client
        from deep_ping.ping_lambdas import invoke_lambda
        self.invoke_lambda = invoke_lambda

    def act(self):
        return self.invoke_lambda('lambda_arn')

    def tearDown(self):
        patch.stopall()

    def test_calls_invoke_on_lambda_client(self):
        # Act
        self.act()
        # Assert
        self.mock_client.invoke.assert_has_calls([
            call(FunctionName='lambda_arn', InvocationType='Event', Payload='{"message": "deep_ping"}')
        ])

    def test_catches_and_logs_exceptions(self):
        # Arrange
        self.mock_client.invoke.side_effect = Exception('☹️')
        # Act
        self.act()
        # Assert
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.DP0008, 'lambda_arn': 'lambda_arn'}),
            call({'log_reference': LogReference.DP0010, 'add_exception_info': True})
        ])

    def test_logs_400_response_code_from_lambda(self):
        self.mock_client.invoke.return_value = {
            'StatusCode': 400
        }
        # Act
        self.act()
        # Assert
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.DP0008, 'lambda_arn': 'lambda_arn'}),
            call({'log_reference': LogReference.DP0014, 'lambda_arn': 'lambda_arn'})
        ])

    def test_logs_function_error_response_code_from_lambda(self):
        self.mock_client.invoke.return_value = {
            'FunctionError': 'Error raised by lambda invoked'
        }
        # Act
        self.act()
        # Assert
        self.log_patcher.assert_has_calls([
            call({'log_reference': LogReference.DP0008, 'lambda_arn': 'lambda_arn'}),
            call({'log_reference': LogReference.DP0013, 'lambda_arn': 'lambda_arn',
                  'function_error': 'Error raised by lambda invoked'})
        ])
