import os
import boto3
import json
from common.log import log
from common.utils.deep_ping import DEEP_PING_MESSAGE_BODY
from deep_ping.log_references import LogReference


def invoke_lambda(lambda_arn):
    try:
        lambda_client = boto3.client('lambda')
        log({'log_reference': LogReference.DP0008, 'lambda_arn': lambda_arn})
        response = lambda_client.invoke(
            FunctionName=lambda_arn,
            InvocationType='Event',
            Payload=json.dumps(DEEP_PING_MESSAGE_BODY))

        function_error = response.get('FunctionError')
        status_code = response.get('StatusCode')

        # Handle successful responses that include error behaviors
        if function_error:
            log({'log_reference': LogReference.DP0013, 'lambda_arn': lambda_arn,
                 'function_error': function_error})
        elif status_code == 400:
            log({'log_reference': LogReference.DP0014, 'lambda_arn': lambda_arn})
        else:
            log({'log_reference': LogReference.DP0009, 'lambda_arn': lambda_arn})
    except Exception:
        log({'log_reference': LogReference.DP0010, 'add_exception_info': True})


def get_lambda_targets():
    return [os.getenv(x) for x in os.environ if x.startswith('TARGET_LAMBDA_ARN_')]


def ping_lambdas():
    log({'log_reference': LogReference.DP0007})
    targets_arns = get_lambda_targets()
    for target_arn in targets_arns:
        invoke_lambda(target_arn)
