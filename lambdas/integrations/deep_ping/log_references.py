import logging

from common.log_references import BaseLogReference


class LogReference(BaseLogReference):
    DP0001 = (logging.INFO, 'Creating deep ping user session')
    DP0002 = (logging.INFO, 'Created deep ping user session')
    DP0003 = (logging.INFO, 'Sending test request to URL')
    DP0004 = (logging.INFO, 'Received response from URL')
    DP0005 = (logging.INFO, 'Finding API gateway for environment')
    DP0006 = (logging.INFO, 'Enumerating API endpoints for API Gateway')
    DP0007 = (logging.INFO, 'Invoking ETL lambdas')
    DP0008 = (logging.INFO, 'Invoking lambda')
    DP0009 = (logging.INFO, 'Successfully invoked lambda')
    DP0010 = (logging.ERROR, 'Failed to invoke lambda')
    DP0011 = (logging.ERROR, 'Invalid API gateway resource method')
    DP0012 = (logging.ERROR, 'Error making API request')
    DP0013 = (logging.WARNING), 'Invoked function threw a FunctionError'
    DP0014 = (logging.WARNING), 'Invoked function returned 400 '
