lambdas() {
sed -i.bak '/### BEGIN/,${q;}' ./cicd/lambdas.yml && rm -rf ./cicd/lambdas.yml.bak
find ./lambdas -path '*unit_tests' -type d | while read line; do
JOB_NAME=$(dirname $line | cut -c 3-)
cat <<EOM >> ./cicd/lambdas.yml

####
.${JOB_NAME}:
  variables:
    LAMBDA_PATH: ${JOB_NAME}

${JOB_NAME} [linting]:
  extends: [.lambda_lint, .${JOB_NAME}]

${JOB_NAME} [auditing]:
  extends: [.lambda_audit, .${JOB_NAME}]

${JOB_NAME} [testing]:
  extends: [.lambda_test, .${JOB_NAME}]

${JOB_NAME} [deploying]:
  extends: [.lambda_deploy, .${JOB_NAME}]
  rules:
    - if: '\$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - \${LAMBDA_PATH}/*
        - lambdas/common/**/*
  needs:
    - ${JOB_NAME} [linting]
    - ${JOB_NAME} [auditing]
    - ${JOB_NAME} [testing]

${JOB_NAME} [manual]:
  extends: [.lambda_deploy, .${JOB_NAME}]
  rules:
    - if: '\$CI_PIPELINE_SOURCE == "web" && \$CI_COMMIT_BRANCH != \$CI_DEFAULT_BRANCH'
      allow_failure: true
      when: manual
EOM
done
}

"$@"