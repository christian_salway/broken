from setuptools import setup, find_packages

setup(
    name='flake8_plugins',
    version='1.0.3',
    description='Python flake8 plugins',

    package_dir={'': 'plugins/'},
    packages=find_packages(
        where='plugins/'
    ),

    install_requires=['flake8'],

    entry_points={
        'flake8.extension': [
            'NHS1 = flake8_datetime_checker.datetime_now:UTCNowToNowChecker',
            'NHS2 = flake8_log_warn_checker.log_warn:LogWarningChecker',
            'NHS3 = flake8_ddb_access_checker.ddb_checker:DynamoDBCallChecker',
        ],
    },
)