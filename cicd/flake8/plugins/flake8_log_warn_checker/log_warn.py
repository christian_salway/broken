import ast

__version__ = '0.2.0'

LOGGING_WARN_LEVEL_ERROR = 'NHS201 You should use logging.WARNING instead of logging.WARN'


class LogWarningVisitor(ast.NodeVisitor):

    def __init__(self):
        self.errors = []

    def visit_Attribute(self, attr):

        if attr.attr == 'WARN':

            self.errors.append(
                (
                    attr.lineno,
                    attr.col_offset,
                    LOGGING_WARN_LEVEL_ERROR
                )
            )

        self.generic_visit(attr)


class LogWarningChecker:
    options = None
    name = 'logging.WARN checker'
    version = __version__

    def __init__(self, tree):
        self.tree = tree

    def run(self):
        visitor = LogWarningVisitor()
        visitor.visit(self.tree)

        for error in visitor.errors:
            yield (*error, LogWarningChecker)
