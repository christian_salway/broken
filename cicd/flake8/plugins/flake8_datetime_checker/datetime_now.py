import ast

CHECKER_NAME = "UTCNowToNow"
DATETIME_NAMES = ["datetime"]
DATETIME_NOW_ERROR = "NHS101 datetime.utcnow is forbidden, use datetime.now(timzone.utc) instead"
TIMEZONE_ARG_ERROR = "NHS102 timezone.utc missing in datetime.now() call, correct args missing"
TOO_MANY_ARGS_ERROR = "NHS103 datetime.now() called with too many args or kwargs"

__version__ = "0.2.0"


class DatetimeVisitor(ast.NodeVisitor):

    def __init__(self):
        self.errors = []

    def visit_Call(self, node):

        is_utcnow_call = getattr(node.func, "attr", None) == "utcnow"
        is_now_call = getattr(node.func, "attr", None) == "now"

        if is_utcnow_call:
            utcnow_call_expr = getattr(node.func, "value", None)
            utcnow_call_expr_expr = getattr(utcnow_call_expr, "value", None)

            # In the case of datetime.utcnow() the utcnow_call_expr should contain a name object which has the id datetime
            is_datetime = getattr(utcnow_call_expr, "id", None) == "datetime"

            has_datetime_attr = getattr(utcnow_call_expr, "attr", None) == "datetime"
            has_deep_datetime_name = getattr(utcnow_call_expr_expr, "id", None) == "datetime"
            is_datetime_datetime = has_datetime_attr and has_deep_datetime_name

            if is_datetime or is_datetime_datetime:
                self.errors.append(
                    (
                        node.func.lineno,
                        node.func.col_offset,
                        DATETIME_NOW_ERROR
                    )
                )

        if is_now_call:
            now_call_expr = getattr(node.func, "value", None)
            now_call_expr_expr = getattr(now_call_expr, "value", None)

            is_datetime = getattr(now_call_expr, "id", None) == "datetime"

            has_datetime_attr = getattr(now_call_expr, "attr", None) == "datetime"
            has_deep_datetime_name = getattr(now_call_expr_expr, "id", None) == "datetime"
            is_datetime_datetime = has_datetime_attr and has_deep_datetime_name

            if is_datetime or is_datetime_datetime:
                datetime_now_args = getattr(node, "args", [])
                datetime_now_kwargs = getattr(node, "keywords", [])

                if len(datetime_now_args) > 1 or len(datetime_now_kwargs) > 1:
                    self.errors.append(
                        (
                            node.func.lineno,
                            node.func.col_offset,
                            TOO_MANY_ARGS_ERROR
                        )
                    )

                is_timezone_arg = False
                is_utc_arg = False
                is_timezone_kwarg = False
                is_utc_kwarg = False

                for arg in datetime_now_args:
                    is_timezone_arg = getattr(arg, "id", False) == "timezone"
                    is_utc_arg = getattr(arg, "attr", False) == 'utc'

                for kwarg in datetime_now_kwargs:
                    kwarg_value = getattr(kwarg, "value", None)
                    timezone_value = getattr(kwarg_value, "value", None)
                    is_timezone_kwarg = getattr(timezone_value, "id", None) == "timezone"
                    is_utc_kwarg = getattr(kwarg_value, "attr", None) == 'utc'

                if not (is_timezone_arg or is_utc_arg) and not (is_timezone_kwarg or is_utc_kwarg):
                    self.errors.append(
                        (
                            node.func.lineno,
                            node.func.col_offset,
                            TIMEZONE_ARG_ERROR
                        )
                    )

        self.generic_visit(node)


class UTCNowToNowChecker:
    options = None
    name = CHECKER_NAME
    version = __version__

    def __init__(self, tree):
        self.tree = tree

    def run(self):
        visitor = DatetimeVisitor()
        visitor.visit(self.tree)

        for error in visitor.errors:
            yield (*error, UTCNowToNowChecker)
