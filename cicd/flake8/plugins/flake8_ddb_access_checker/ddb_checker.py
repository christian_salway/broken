import ast

CHECKER_NAME = "DDBResourceClientChecker"

RESOURCE_WRAPPER_IN_USE_ERROR = "NHS301 get_dynamodb_resource in use outside common/utils/dynamodb_access/"
BOTO3_RESOURCE_IN_USE_ERROR = "NHS302 boto3.resource('dynamodb') in use outside common/utils/dynamodb_access/"
RESOURCE_IN_USE_ERROR = "NHS303 resource('dynamodb') in use outside common/utils/dynamodb_access/"

CLIENT_WRAPPER_IN_USE_ERROR = "NHS304 get_dynamodb_client in use outside common/utils/dynamodb_access/"
BOTO3_CLIENT_IN_USE_ERROR = "NHS305 boto3.client('dynamodb') in use outside common/utils/dynamodb_access/"
CLIENT_IN_USE_ERROR = "NHS306 client('dynamodb') in use outside common/utils/dynamodb_access/"

errors = {
    "get_dynamodb_resource": RESOURCE_WRAPPER_IN_USE_ERROR,
    "boto3.resource": BOTO3_RESOURCE_IN_USE_ERROR,
    "resource": RESOURCE_IN_USE_ERROR,

    "get_dynamodb_client": CLIENT_WRAPPER_IN_USE_ERROR,
    "boto3.client": BOTO3_CLIENT_IN_USE_ERROR,
    "client": CLIENT_IN_USE_ERROR
}

__version__ = "0.0.1"


class DynamoDBVisitor(ast.NodeVisitor):

    def __init__(self):
        self.errors = []

    def visit_Call(self, node):
        # Wrapper function check
        func_name = getattr(node.func, "id", None)

        if func_name is not None:
            if func_name in ["get_dynamodb_resource", "get_dynamodb_client"]:
                self.errors.append(
                    (
                        node.func.lineno,
                        node.func.col_offset,
                        errors[func_name]
                    )
                )

        # boto3.resource/client check
        attr = getattr(node.func, "attr", None)
        if attr in ["resource", "client"] and getattr(node.func.value, "id", None) == "boto3":
            arg = getattr(node.args[0], "value", None)

            if arg == "dynamodb":
                self.errors.append(
                    (
                        node.func.lineno,
                        node.func.col_offset,
                        errors[f"boto3.{attr}"]
                    )
                )

        # resource/client check
        if func_name is not None:
            if func_name in ["resource", "client"]:
                arg = getattr(node.args[0], 'value')

                if arg == "dynamodb":
                    self.errors.append(
                        (
                            node.func.lineno,
                            node.func.col_offset,
                            errors[func_name]
                        )
                    )

        self.generic_visit(node)


class DynamoDBCallChecker:
    options = None
    name = CHECKER_NAME
    version = __version__

    def __init__(self, tree):
        self.tree = tree

    def run(self):
        visitor = DynamoDBVisitor()
        visitor.visit(self.tree)

        for error in visitor.errors:
            yield (*error, DynamoDBCallChecker)
