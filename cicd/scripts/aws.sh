#!/bin/bash -e

if [ "${DEBUG_OUTPUT}" = "true" ]; then
    set -x
fi

aws_version() {
    aws --version
}

# usage: aws_assume_role account_id role_arn
aws_assume_role() {
    # save current credentials for reuse
    if [[ -n "$AWS_ACCESS_KEY_ID" ]]; then export AWS_ACCESS_KEY_ID_="$AWS_ACCESS_KEY_ID"; fi
    if [[ -n "$AWS_SECRET_ACCESS_KEY" ]]; then export AWS_SECRET_ACCESS_KEY_="$AWS_SECRET_ACCESS_KEY"; fi

    # pass in as environment variables or parameters
    AWS_ACCOUNT_ID="${AWS_ACCOUNT_ID:-$1}"
    AWS_ASSUME_ROLE="${AWS_ASSUME_ROLE:-$2}"

    echo "Assuming role '${AWS_ASSUME_ROLE}' in account '${AWS_ACCOUNT_ID}'..."
    AWS_CREDENTIALS=$(aws sts assume-role \
        --role-arn "arn:aws:iam::${AWS_ACCOUNT_ID}:role/${AWS_ASSUME_ROLE}" \
        --role-session-name "${AWS_SESSION_NAME:-GitlabRunner}" \
        --duration-seconds ${AWS_SESSION_DURATION:-3600} | jq '.Credentials')
    export AWS_ACCESS_KEY_ID=$(echo ${AWS_CREDENTIALS} | jq -r '.AccessKeyId')
    export AWS_SECRET_ACCESS_KEY=$(echo ${AWS_CREDENTIALS} | jq -r '.SecretAccessKey')
    export AWS_SESSION_TOKEN=$(echo ${AWS_CREDENTIALS} | jq -r '.SessionToken')
}
