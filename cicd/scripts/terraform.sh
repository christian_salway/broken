#!/bin/bash -e

if [ "${DEBUG_OUTPUT}" = "true" ]; then
    set -x
fi

terraform_version() {
    terraform --version
}

# Usage: [TF_STATE_NAME="value"] [TF_HTTP_...="value";] terraform_configure
terraform_configure() {
    local TF_STATE_NAME="${TF_STATE_NAME:-${CI_COMMIT_REF_SLUG}}"

    # https://www.terraform.io/docs/language/settings/backends/http.html
    export TF_HTTP_ADDRESS="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}"
    export TF_HTTP_USERNAME="${TF_USERNAME:-gitlab-ci-token}"
    export TF_HTTP_PASSWORD="${TF_PASSWORD:-${CI_JOB_TOKEN}}"
    export TF_HTTP_UPDATE_METHOD="${TF_HTTP_UPDATE_METHOD:-POST}"
    export TF_HTTP_LOCK_ADDRESS="${TF_HTTP_ADDRESS}/lock"
    export TF_HTTP_LOCK_METHOD="${TF_HTTP_LOCK_METHOD:-POST}"
    export TF_HTTP_UNLOCK_ADDRESS="${TF_HTTP_ADDRESS}/lock"
    export TF_HTTP_UNLOCK_METHOD="${TF_HTTP_UNLOCK_METHOD:-DELETE}.."
    export TF_HTTP_RETRY_MAX="${TF_HTTP_RETRY_MAX:-2}"
    export TF_HTTP_RETRY_WAIT_MIN="${TF_HTTP_RETRY_WAIT_MIN:-5}"
    export TF_HTTP_RETRY_WAIT_MAX="${TF_HTTP_RETRY_WAIT_MAX:-30}"

    # https://www.terraform.io/docs/cli/config/environment-variables.html
    export TF_IN_AUTOMATION=1
    export TF_INPUT=0
}

# Usage: [TF_STATE_NAME="value"] terraform_init
terraform_init() {
    terraform_configure
    terraform init -reconfigure ${@}
}

# Usage: terraform_fmt
terraform_fmt() {
    terraform fmt -check -diff -recursive
}

# Usage: [TF_INIT_OPTS="-option=value"] terraform_validate [-json]
terraform_validate() {
    terraform_init -backend=false ${TF_INIT_OPTS}
    terraform validate ${@}
}

# Usage: [TF_STATE_NAME="value"] [TF_INIT_OPTS="-option=value"] terraform_plan [-destroy|-refresh-only] [-refresh=false] [-var-file=FILENAME] [-out=FILENAME]
terraform_plan() {
    terraform_init ${TF_INIT_OPTS}
    terraform plan ${@}
}

# Usage: [TF_STATE_NAME="value"] [TF_INIT_OPTS="-option=value"] terraform_plan_json [-refresh=false] [-var-file=FILENAME]
terraform_plan_json() {
    JQ_PLAN='
    ([.resource_changes[]?.change.actions?] | flatten) | {
        "create":(map(select(.=="create")) | length),
        "update":(map(select(.=="update")) | length),
        "delete":(map(select(.=="delete")) | length)}
    '
    terraform_init ${TF_INIT_OPTS}
    terraform plan -out="tfplan.bin" ${@}
    terraform show -json "tfplan.bin" | jq -r "${JQ_PLAN}" > "tfplan.json"
}

# Usage: [TF_STATE_NAME="state"] [TF_INIT_OPTS="-option=value"] terraform_apply [-refresh=false] [-compact-warnings] [-var-file=FILENAME] [PLAN FILE]
terraform_apply() {
    terraform_init ${TF_INIT_OPTS}
    terraform apply -auto-approve ${@}
}

# Usage: [TF_STATE_NAME="state"] [TF_INIT_OPTS="-option=value"] terraform_destroy [-compact-warnings] [PLAN FILE]
terraform_destroy() {
    terraform_init ${TF_INIT_OPTS}
    terraform destroy -auto-approve ${@}
}
